#!/system/bin/sh
su
mount -o rw,remount /system
cd /system/priv-app/WingsPlay
cp /storage/sdcard0/Download/app-debug.apk Play.apk
chmod 644 Play.apk

am force-stop com.wings.play
pm install -r Play.apk
am start -n "com.wings.play/com.wings.play.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
