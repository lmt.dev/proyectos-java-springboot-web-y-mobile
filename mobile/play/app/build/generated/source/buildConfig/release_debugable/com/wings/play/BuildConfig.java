/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.wings.play;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.wings.play";
  public static final String BUILD_TYPE = "release_debugable";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "1.0.3";
}
