package com.wings.play.views;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by seba on 22/10/17.
 */

public class Preferences {

    private static final String fileKey  = "com.wings.play.KKGLDODS";

    public static final String TOKEN_SENT = "com.wings.play.TOKEN_SENT";
    public static final String TOKEN = "com.wings.play.TOKEN";
    public static final String ANDROID_ID = "com.wings.play.ANDROID_ID";

    public static SharedPreferences getPreferences(Context context){
        return context.getSharedPreferences(fileKey, Context.MODE_PRIVATE);
    }

}
