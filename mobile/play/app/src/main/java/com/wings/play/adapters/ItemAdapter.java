package com.wings.play.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.wings.play.domain.Item;
import com.wings.play.views.ItemView;

import java.util.List;

/**
 * Created by seba on 21/10/17.
 */

public class ItemAdapter extends BaseAdapter {

    private final List<Item> items;

    private Context mContext;

    public ItemAdapter(Context c, List<Item> items) {
        this.mContext = c;
        this.items = items;
    }

    public void addItems(List<Item> items){
        this.items.addAll(items);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemView itemView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            itemView = new ItemView(mContext,null);
            itemView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            itemView.setPadding(5, 5, 5, 5);

        } else {
            itemView = (ItemView) convertView;
        }

        Item item = items.get(position);
        itemView.setItemId(item.getId());
        itemView.setRating(item.getStats().getPrating().getAvg().floatValue());
        itemView.setDownloads(item.getStats().getPdownloads());
        String img = item.getIcon();
        /*if(img == null || img.isEmpty()){
            img = item.getIcon();
        }*/
        itemView.setImage(img);
        itemView.setName(item.getName());

        return itemView;
    }
}
