
package com.wings.play.domain.apkfile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvInfo {

    @SerializedName("infections")
    @Expose
    private List<Object> infections = null;
    @SerializedName("name")
    @Expose
    private String name;

    public List<Object> getInfections() {
        return infections;
    }

    public void setInfections(List<Object> infections) {
        this.infections = infections;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
