package com.wings.play.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by seba on 21/10/17.
 */

public class ItemsResponse {

    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("datalist")
    @Expose
    private Datalist datalist;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public Datalist getDatalist() {
        return datalist;
    }

    public void setDatalist(Datalist datalist) {
        this.datalist = datalist;
    }

}
