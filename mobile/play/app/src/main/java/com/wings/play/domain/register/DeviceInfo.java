package com.wings.play.domain.register;

import android.content.res.Resources;

import com.wings.play.BuildConfig;

/**
 * Created by seba on 22/10/17.
 */

public class DeviceInfo {

    private String androidId;
    private String token;
    private String os = System.getProperty("os.version");
    private String device = android.os.Build.DEVICE;
    private String model = android.os.Build.MODEL;
    private String product = android.os.Build.PRODUCT;
    private int versionCode = BuildConfig.VERSION_CODE;
    private String versionName = BuildConfig.VERSION_NAME;
    private String locale = Resources.getSystem().getConfiguration().locale.toString();

    public DeviceInfo(String token, String androidId) {
        this.token = token;
        this.androidId = androidId;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
