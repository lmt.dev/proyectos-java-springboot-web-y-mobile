
package com.wings.play.domain.apkfile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Scanned {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("av_info")
    @Expose
    private List<AvInfo> avInfo = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<AvInfo> getAvInfo() {
        return avInfo;
    }

    public void setAvInfo(List<AvInfo> avInfo) {
        this.avInfo = avInfo;
    }

}
