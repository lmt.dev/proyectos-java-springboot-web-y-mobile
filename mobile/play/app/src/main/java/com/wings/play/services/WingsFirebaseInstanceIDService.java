package com.wings.play.services;

import android.provider.Settings.Secure;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.wings.play.R;
import com.wings.play.domain.register.DeviceInfo;
import com.wings.play.utils.ApiUtils;

/**
 * Created by seba on 21/10/17.
 */
public class WingsFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "WingsFirebaseInstanceID";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        String androidId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        DeviceInfo info = new DeviceInfo(token, androidId);

        String url = getString(R.string.api_url);
        ApiUtils.sendRegistrationToServer(getApplicationContext(), url, info);
    }
}
