package com.wings.play;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wings.play.adapters.ItemAdapter;
import com.wings.play.domain.Datalist;
import com.wings.play.domain.Item;
import com.wings.play.domain.ItemsResponse;
import com.wings.play.domain.register.DeviceInfo;
import com.wings.play.utils.ApiUtils;
import com.wings.play.views.ItemView;
import com.wings.play.views.Preferences;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {


    private Context mContext;
    private ProgressBar pBar;
    private View netError;
    private View retryButton;

    private TextView netErrorText;
    private GridView maincontent;

    private int page = 0;

    private boolean isSearch = false;
    private String searchQuery = "";

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 9;

    private Set<Integer> alreadyLoaded = new HashSet<>();

    private final int MY_PERMISSIONS_REQUEST = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        pBar = (ProgressBar) findViewById(R.id.main_pbar);
        netError = findViewById(R.id.net_error);
        retryButton = findViewById(R.id.net_error_retry);
        netErrorText = (TextView) findViewById(R.id.net_error_text);

        maincontent = (GridView) findViewById(R.id.main_content);


        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });

        mContext = getApplicationContext();
        SharedPreferences pref = Preferences.getPreferences(MainActivity.this);
        boolean sent = pref.getBoolean(Preferences.TOKEN_SENT, false);
        if (!sent) {
            String token = pref.getString(Preferences.TOKEN, null);
            String andId = pref.getString(Preferences.ANDROID_ID, null);
            if (token != null && andId != null) {
                DeviceInfo info = new DeviceInfo(token, andId);
                String url = getString(R.string.api_url);
                ApiUtils.sendRegistrationToServer(MainActivity.this, url, info);
            }
        }

        //checkPermissions();
        start();
    }


    @Override
    protected void onResume() {
        super.onResume();
        maincontent.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view,
                                 int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        page++;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    loading = true;
                    if (isSearch) {
                        search(searchQuery);
                    } else {
                        start();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //blank, not required in your case
            }
        });
    }

    private void start() {

        if (isSearch) {

            maincontent.setAdapter(null);
            alreadyLoaded.clear();

            searchQuery = "";
            page = 0;
            previousTotal = 0;
            maincontent.setAdapter(null);
        }

        isSearch = false;
        if (alreadyLoaded.contains(page)) {
            return;
        }

        pBar.setVisibility(View.VISIBLE);
        netError.setVisibility(View.GONE);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        String url = getString(R.string.api_url);
        Call<ItemsResponse> call = ApiUtils.getApiClient(url).getFrontPage(page);
        try {
            call.enqueue(new Callback<ItemsResponse>() {
                @Override
                public void onResponse(final Call<ItemsResponse> call, Response<ItemsResponse> response) {
                    if (response.isSuccessful()) {
                        alreadyLoaded.add(page);
                        loadItems(response.body());
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                        pBar.setVisibility(View.GONE);
                        netError.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<ItemsResponse> call, Throwable t) {
                    if (t instanceof IOException) {
                        netErrorText.setText(getString(R.string.net_error_no_internet));
                    } else if (t instanceof SocketTimeoutException) {
                        netErrorText.setText(getString(R.string.net_error_timeout));
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                    }
                    pBar.setVisibility(View.GONE);
                    netError.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception ex) {
            netErrorText.setText(getString(R.string.net_error_generic));
            pBar.setVisibility(View.GONE);
            netError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        alreadyLoaded.clear();
    }

    private void loadItems(ItemsResponse itemsResponse) {

        Datalist dataList = itemsResponse.getDatalist();
        List<Item> items = dataList.getItems();
        if (mContext.getResources().getBoolean(R.bool.is_landscape)) {
            maincontent.setNumColumns(5);
            visibleThreshold = 15;
        } else {
            maincontent.setNumColumns(3);
            visibleThreshold = 9;
        }

        if (maincontent.getAdapter() == null) {
            ItemAdapter adapter = new ItemAdapter(mContext, items);
            maincontent.setAdapter(adapter);
        } else {
            ((ItemAdapter) maincontent.getAdapter()).addItems(items);
        }

        maincontent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Integer itemId = ((ItemView) view).getItemId();
                openDetail(itemId);
            }
        });

        loading = false;
        pBar.setVisibility(View.GONE);
    }

    private void openDetail(Integer id) {

        Intent i = new Intent(MainActivity.this, DetailActivity.class);
        i.putExtra("apk_id", id);
        startActivity(i);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                start();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSearch = true;
                searchQuery = searchView.getQuery().toString();
                page = 0;
                previousTotal = 0;
                alreadyLoaded.clear();
                maincontent.setAdapter(null);
                searchView.clearFocus();
                search(searchQuery);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            if (drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
            }
        }
        return true;
    }

    private void search(String query) {

        if (alreadyLoaded.contains(page)) {
            return;
        }

        pBar.setVisibility(View.VISIBLE);
        netError.setVisibility(View.GONE);

        String url = getString(R.string.api_url);
        Call<ItemsResponse> call = ApiUtils.getApiClient(url).getAppSearch(URLEncoder.encode(query), page);
        try {
            call.enqueue(new Callback<ItemsResponse>() {
                @Override
                public void onResponse(final Call<ItemsResponse> call, Response<ItemsResponse> response) {
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    }

                    if (response.isSuccessful()) {
                        alreadyLoaded.add(page);
                        loadItems(response.body());
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                        pBar.setVisibility(View.GONE);
                        netError.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<ItemsResponse> call, Throwable t) {
                    if (t instanceof IOException) {
                        netErrorText.setText(getString(R.string.net_error_no_internet));
                    } else if (t instanceof SocketTimeoutException) {
                        netErrorText.setText(getString(R.string.net_error_timeout));
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                    }
                    pBar.setVisibility(View.GONE);
                    netError.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception ex) {
            netErrorText.setText(getString(R.string.net_error_generic));
            pBar.setVisibility(View.GONE);
            netError.setVisibility(View.VISIBLE);
        }
    }

}
