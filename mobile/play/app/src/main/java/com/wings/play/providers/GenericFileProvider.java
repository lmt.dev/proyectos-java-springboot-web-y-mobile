package com.wings.play.providers;


import android.support.v4.content.FileProvider;

public class GenericFileProvider extends FileProvider {

    public static final String PROVIDER_NAME = "com.wings.play.fileprovider";
}