
package com.wings.play.domain.apkfile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hardware {

    @SerializedName("sdk")
    @Expose
    private Integer sdk;
    @SerializedName("screen")
    @Expose
    private String screen;
    @SerializedName("gles")
    @Expose
    private Integer gles;
    @SerializedName("cpus")
    @Expose
    private List<String> cpus = null;
    @SerializedName("densities")
    @Expose
    private List<Object> densities = null;
    @SerializedName("dependencies")
    @Expose
    private List<Object> dependencies = null;

    public Integer getSdk() {
        return sdk;
    }

    public void setSdk(Integer sdk) {
        this.sdk = sdk;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public Integer getGles() {
        return gles;
    }

    public void setGles(Integer gles) {
        this.gles = gles;
    }

    public List<String> getCpus() {
        return cpus;
    }

    public void setCpus(List<String> cpus) {
        this.cpus = cpus;
    }

    public List<Object> getDensities() {
        return densities;
    }

    public void setDensities(List<Object> densities) {
        this.densities = densities;
    }

    public List<Object> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Object> dependencies) {
        this.dependencies = dependencies;
    }

}
