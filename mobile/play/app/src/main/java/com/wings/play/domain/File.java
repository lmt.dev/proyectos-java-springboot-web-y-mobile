
package com.wings.play.domain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.wings.play.domain.apkfile.Flags;
import com.wings.play.domain.apkfile.Hardware;
import com.wings.play.domain.apkfile.Malware;
import com.wings.play.domain.apkfile.Signature;

public class File {

    @SerializedName("vername")
    @Expose
    private String vername;
    @SerializedName("vercode")
    @Expose
    private Integer vercode;
    @SerializedName("md5sum")
    @Expose
    private String md5sum;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("path_alt")
    @Expose
    private String pathAlt;
    @SerializedName("filesize")
    @Expose
    private Long filesize;
    @SerializedName("signature")
    @Expose
    private Signature signature;
    @SerializedName("hardware")
    @Expose
    private Hardware hardware;
    @SerializedName("malware")
    @Expose
    private Malware malware;
    @SerializedName("flags")
    @Expose
    private Flags flags;
    @SerializedName("used_features")
    @Expose
    private List<String> usedFeatures = null;
    @SerializedName("used_permissions")
    @Expose
    private List<String> usedPermissions = null;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;

    public String getVername() {
        return vername;
    }

    public void setVername(String vername) {
        this.vername = vername;
    }

    public Integer getVercode() {
        return vercode;
    }

    public void setVercode(Integer vercode) {
        this.vercode = vercode;
    }

    public String getMd5sum() {
        return md5sum;
    }

    public void setMd5sum(String md5sum) {
        this.md5sum = md5sum;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathAlt() {
        return pathAlt;
    }

    public void setPathAlt(String pathAlt) {
        this.pathAlt = pathAlt;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    public Hardware getHardware() {
        return hardware;
    }

    public void setHardware(Hardware hardware) {
        this.hardware = hardware;
    }

    public Malware getMalware() {
        return malware;
    }

    public void setMalware(Malware malware) {
        this.malware = malware;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    public List<String> getUsedFeatures() {
        return usedFeatures;
    }

    public void setUsedFeatures(List<String> usedFeatures) {
        this.usedFeatures = usedFeatures;
    }

    public List<String> getUsedPermissions() {
        return usedPermissions;
    }

    public void setUsedPermissions(List<String> usedPermissions) {
        this.usedPermissions = usedPermissions;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }
}
