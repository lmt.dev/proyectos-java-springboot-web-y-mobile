package com.wings.play.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wings.play.R;

/**
 * Created by seba on 24/10/17.
 */

public class PermissionView extends LinearLayout {


    private TextView title;
    private TextView desc;
    private ImageView icon;

    public PermissionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.view_permission_layout, this, true);

        title = (TextView) layout.findViewById(R.id.permission_detail_title);
        desc = (TextView) layout.findViewById(R.id.permission_detail_desc);
        icon = (ImageView) layout.findViewById(R.id.permission_detail_icon);

    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setDesc(String desc) {
        this.desc.setText(desc);
    }

    public void setIcon(Drawable icon) {
        this.icon.setImageDrawable(icon);
    }

    public void setIcon(int iconResource) {
        this.icon.setImageResource(iconResource);
    }
}
