
package com.wings.play.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urls {

    @SerializedName("w")
    @Expose
    private String w;
    @SerializedName("m")
    @Expose
    private String m;

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

}
