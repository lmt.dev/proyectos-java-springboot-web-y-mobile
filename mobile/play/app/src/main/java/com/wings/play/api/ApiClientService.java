package com.wings.play.api;

import com.wings.play.domain.ItemsResponse;
import com.wings.play.domain.ItemDetail;
import com.wings.play.domain.register.DeviceInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by seba on 21/10/17.
 */

public interface ApiClientService {

    @GET("apps/get/front-page")
    Call<ItemsResponse> getFrontPage(@Query("page") int page);

    @GET("apps/get/{id}")
    Call<ItemDetail> getAppDetail(@Path("id") Integer id);

    @GET("apps/search")
    Call<ItemsResponse> getAppSearch(@Query("query") String query,@Query("page") int page);

    @POST("device/register")
    Call<Void> postRegisterDevice(@Body DeviceInfo info);

}