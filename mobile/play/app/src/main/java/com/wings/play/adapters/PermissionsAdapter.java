package com.wings.play.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.wings.play.R;
import com.wings.play.views.PermissionView;

import java.util.List;

/**
 * Created by seba on 24/10/17.
 */

public class PermissionsAdapter extends BaseAdapter {


    private List<PermissionInfo> items;
    private Context context;
    private PackageManager pm;

    public PermissionsAdapter(Context context, List<PermissionInfo> items) {
        this.context = context;
        this.items = items;
        this.pm = context.getPackageManager();
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PermissionInfo info = items.get(position);
        String title = info.loadLabel(pm).toString();

        String desc = "";
        CharSequence cdesc = info.loadDescription(pm);
        if (cdesc != null) {
            desc = cdesc.toString();
        }

        PermissionView pView;//ItemView itemView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            pView = new PermissionView(context, null);
            pView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            pView.setPadding(5, 5, 5, 5);
        } else {
            pView = (PermissionView) convertView;
        }

        try {

            Drawable icon = null;
            if (info.icon != 0) {
                icon = info.loadIcon(pm);
            }

            if (icon == null) {
                String group = info.group;
                if (group != null) {
                    try {
                        PermissionGroupInfo groupInfo = pm.getPermissionGroupInfo(group, PackageManager.GET_META_DATA);
                        icon = pm.getResourcesForApplication(info.packageName).getDrawable(groupInfo.icon);
                    }catch (Exception e){

                    }
                }

                if (icon == null) {
                    if (info.name.contains("WIFI") || info.name.contains("NETWORK") || info.name.contains("INTERNET")) {
                        icon = context.getResources().getDrawable(R.drawable.ic_perm_scan_wifi_black_24dp);
                    }

                    if (info.name.contains("BLUETOOTH")) {
                        icon = context.getResources().getDrawable(R.drawable.ic_bluetooth_black_24dp);
                    }
                }

                if (icon == null) {
                    icon = context.getResources().getDrawable(R.drawable.ic_perm_device_information_black_24dp);
                }
            }
            pView.setIcon(icon);

        } catch (Exception e) {
            Log.d("ICON","ERROR ", e);
            pView.setIcon(R.drawable.ic_info_outline_black_24dp);
        }

        pView.setDesc(desc);
        pView.setTitle(title);

        return pView;
    }
}
