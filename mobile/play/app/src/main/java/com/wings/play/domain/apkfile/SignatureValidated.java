
package com.wings.play.domain.apkfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignatureValidated {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("signature_from")
    @Expose
    private String signatureFrom;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSignatureFrom() {
        return signatureFrom;
    }

    public void setSignatureFrom(String signatureFrom) {
        this.signatureFrom = signatureFrom;
    }

}
