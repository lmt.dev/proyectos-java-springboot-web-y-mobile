package com.wings.play;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wings.play.adapters.PermissionsAdapter;
import com.wings.play.domain.ItemDetail;
import com.wings.play.services.InstallAppService;
import com.wings.play.utils.ApiUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    private TextView appName;
    private TextView description;
    private TextView size;
    private TextView downloads;

    private ImageView icon;
    private ImageView header;
    private ProgressBar pBar;
    private Button install;
    private RatingBar rating;
    private View retryButton;


    private View installPanel;
    private View detailPanel;
    private View netError;
    private TextView netErrorText;
    private List<PermissionInfo> items = new LinkedList<>();

    private Integer appId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        appId = getIntent().getIntExtra("apk_id", 1);

        appName = (TextView) findViewById(R.id.detail_app_name);
        description = (TextView) findViewById(R.id.detail_description);
        size = (TextView) findViewById(R.id.detail_size);
        downloads = (TextView) findViewById(R.id.detail_downloads);

        icon = (ImageView) findViewById(R.id.detail_icon);
        header = (ImageView) findViewById(R.id.detail_header_image);
        install = (Button) findViewById(R.id.detail_install);
        pBar = (ProgressBar) findViewById(R.id.main_pbar);
        rating = (RatingBar) findViewById(R.id.detail_rating);
        installPanel = findViewById(R.id.detail_install_panel);
        detailPanel = findViewById(R.id.detail_detail_panel);
        netError = findViewById(R.id.net_error);
        retryButton = findViewById(R.id.net_error_retry);
        netErrorText = (TextView) findViewById(R.id.net_error_text);

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });

        install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPermissionsDialog();
            }
        });

        start();

    }

    private void start() {

        installPanel.setVisibility(View.VISIBLE);
        detailPanel.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.VISIBLE);
        netError.setVisibility(View.GONE);

        String url = getString(R.string.api_url);
        Call<ItemDetail> call = ApiUtils.getApiClient(url).getAppDetail(appId);

        try {
            call.enqueue(new Callback<ItemDetail>() {
                @Override
                public void onResponse(final Call<ItemDetail> call, Response<ItemDetail> response) {
                    if (response.isSuccessful()) {
                        loadItem(response.body());
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                        pBar.setVisibility(View.GONE);
                        installPanel.setVisibility(View.GONE);
                        detailPanel.setVisibility(View.GONE);
                        netError.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<ItemDetail> call, Throwable t) {
                    if (t instanceof IOException) {
                        netErrorText.setText(getString(R.string.net_error_no_internet));
                    } else if (t instanceof SocketTimeoutException) {
                        netErrorText.setText(getString(R.string.net_error_timeout));
                    } else {
                        netErrorText.setText(getString(R.string.net_error_generic));
                    }
                    installPanel.setVisibility(View.GONE);
                    pBar.setVisibility(View.GONE);
                    detailPanel.setVisibility(View.GONE);
                    netError.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception ex) {
            netErrorText.setText(getString(R.string.net_error_generic));
            pBar.setVisibility(View.GONE);
            installPanel.setVisibility(View.GONE);
            detailPanel.setVisibility(View.GONE);
            netError.setVisibility(View.VISIBLE);
        }
    }

    private void loadItem(ItemDetail item) {

        String head = item.getData().getGraphic();
        Picasso.with(DetailActivity.this).load(head).into(header);

        String ico = item.getData().getIcon();
        Picasso.with(DetailActivity.this).load(ico).into(icon);

        appName.setText(item.getData().getName());
        String panned = item.getData().getMedia().getDescription();
        description.setText(Html.fromHtml(panned));
        rating.setRating(item.getData().getStats().getPrating().getAvg().floatValue());
        size.setText(humanReadableByteCount(item.getData().getSize()));
        downloads.setText(parseDownloads(item.getData().getStats().getPdownloads()));
        pBar.setVisibility(View.GONE);

        List<String> permissions = item.getData().getFile().getUsedPermissions();
        loadPermissions(permissions);

    }

    private void loadPermissions(List<String> permissions) {
        try {
            for (String perm : permissions) {
                PermissionInfo info = getPackageManager().getPermissionInfo(perm, PackageManager.GET_META_DATA);
                items.add(info);
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
    }


    private void showPermissionsDialog() {

        LinearLayout layout = (LinearLayout) LayoutInflater.from(DetailActivity.this).inflate(R.layout.dialog_permissions_layout, null);
        ListView list = (ListView) layout.findViewById(R.id.permissions_list_view);
        View noPerm = layout.findViewById(R.id.permission_not_required);

        if (items.isEmpty()) {
            list.setVisibility(View.GONE);
            noPerm.setVisibility(View.VISIBLE);
        }

        list.setAdapter(new PermissionsAdapter(DetailActivity.this, items));

        ((TextView) layout.findViewById(R.id.permission_app_name)).setText(appName.getText());
        ((ImageView) layout.findViewById(R.id.permission_app_icon)).setImageDrawable(icon.getDrawable());


        ((ImageView) layout.findViewById(R.id.permission_app_icon)).setImageDrawable(icon.getDrawable());

        new AlertDialog.Builder(DetailActivity.this)
                .setView(layout)
                .setPositiveButton(getString(R.string.install), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(DetailActivity.this, InstallAppService.class);
                        i.putExtra("app_id", appId);
                        startService(i);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null)
                .show();
    }

    public static String humanReadableByteCount(long bytes) {
        int unit = 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = ("KMGTPE").charAt(exp - 1) + "";
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private String parseDownloads(int downloads) {
        if (downloads < 1000) {
            return "" + downloads;
        } else if (downloads < 1000000) {
            return (downloads / 1000) + " k";
        } else {
            return (downloads / 1000000) + " M";
        }
    }
}
