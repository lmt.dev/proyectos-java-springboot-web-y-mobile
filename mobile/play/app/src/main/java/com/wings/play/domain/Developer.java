
package com.wings.play.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Developer {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("website")
    @Expose
    private Object website;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("privacy")
    @Expose
    private Object privacy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getWebsite() {
        return website;
    }

    public void setWebsite(Object website) {
        this.website = website;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Object privacy) {
        this.privacy = privacy;
    }

}
