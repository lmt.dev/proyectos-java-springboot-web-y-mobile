
package com.wings.play.domain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Media {

    @SerializedName("keywords")
    @Expose
    private List<String> keywords = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("news")
    @Expose
    private String news;
    @SerializedName("videos")
    @Expose
    private List<Object> videos = null;
    @SerializedName("screenshots")
    @Expose
    private List<Screenshot> screenshots = null;

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public List<Object> getVideos() {
        return videos;
    }

    public void setVideos(List<Object> videos) {
        this.videos = videos;
    }

    public List<Screenshot> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(List<Screenshot> screenshots) {
        this.screenshots = screenshots;
    }

}
