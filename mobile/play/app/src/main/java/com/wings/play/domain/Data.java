
package com.wings.play.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.wings.play.domain.store.Store;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("uname")
    @Expose
    private String uname;
    @SerializedName("size")
    @Expose
    private Long size;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("graphic")
    @Expose
    private String graphic;
    @SerializedName("added")
    @Expose
    private String added;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("main_package")
    @Expose
    private Object mainPackage;
    @SerializedName("developer")
    @Expose
    private Developer developer;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("file")
    @Expose
    private File file;
    @SerializedName("media")
    @Expose
    private Media media;
    @SerializedName("urls")
    @Expose
    private Urls urls;
    @SerializedName("stats")
    @Expose
    private Stats stats;
    @SerializedName("obb")
    @Expose
    private Object obb;
    @SerializedName("pay")
    @Expose
    private Object pay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGraphic() {
        return graphic;
    }

    public void setGraphic(String graphic) {
        this.graphic = graphic;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Object getMainPackage() {
        return mainPackage;
    }

    public void setMainPackage(Object mainPackage) {
        this.mainPackage = mainPackage;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Object getObb() {
        return obb;
    }

    public void setObb(Object obb) {
        this.obb = obb;
    }

    public Object getPay() {
        return pay;
    }

    public void setPay(Object pay) {
        this.pay = pay;
    }

}
