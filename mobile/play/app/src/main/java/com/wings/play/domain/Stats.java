
package com.wings.play.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stats {

    @SerializedName("downloads")
    @Expose
    private Integer downloads;
    @SerializedName("pdownloads")
    @Expose
    private Integer pdownloads;
    @SerializedName("rating")
    @Expose
    private Rating rating;
    @SerializedName("prating")
    @Expose
    private Prating prating;

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public Integer getPdownloads() {
        return pdownloads;
    }

    public void setPdownloads(Integer pdownloads) {
        this.pdownloads = pdownloads;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public Prating getPrating() {
        return prating;
    }

    public void setPrating(Prating prating) {
        this.prating = prating;
    }

}
