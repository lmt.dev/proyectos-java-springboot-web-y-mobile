#!/bin/bash

ADB_SH="$ADB shell"
ADB="adb"
app_package="com.wings.play"
dir_app_name="WingsPlay"
MAIN_ACTIVITY="MainActivity"
apk_target_sys="/storage/sdcard0/Download"

apk_host="./app/build/outputs/apk/app-debug.apk"

# Delete previous APK
rm -f $apk_host

# Compile the APK: you can adapt this for production build, flavors, etc.
./gradlew assembleDebug || exit -1 # exit on failure
$ADB push $apk_host $apk_target_sys

$ADB_SH install_play.sh

