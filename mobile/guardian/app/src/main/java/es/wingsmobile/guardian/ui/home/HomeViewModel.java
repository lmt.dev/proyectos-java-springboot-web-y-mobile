package es.wingsmobile.guardian.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import es.wingsmobile.guardian.db.DatabaseHelper;
import es.wingsmobile.guardian.domain.Record;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<List<Record>> eventRecords;
    private DatabaseHelper db;


    public HomeViewModel(DatabaseHelper db) {
        this.db = db;
        mText = new MutableLiveData<>();
        eventRecords = new MutableLiveData<>();
        mText.setValue("Wings Guardian 1.4");
        eventRecords.setValue(getData());
    }


    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<List<Record>> getEventRecords(){
        return eventRecords;
    }
    public void refreshData(){
        eventRecords.setValue(getData());
    }

    private List<Record> getData() {
        List<Record> events = db.getEvents();
        if(events==null){
            events = new LinkedList<>();
        }
        return events;
    }


    public void setDataBaseHelper(DatabaseHelper db) {
        this.db = db;
    }
}