package es.wingsmobile.guardian.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by seba on 19/06/17.
 */

public class FormatUtil {


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdfToDisplay = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

    public static String fromDateToDisplay(Date date){
        return sdfToDisplay.format(date);
    }

    public static String fromDateToString(Date date){
        return sdf.format(date);
    }

    public static Date fromStringToDate(String date){
        Date ret = new Date();
        try {
            ret = sdf.parse(date);
        }catch (Exception e){}
        return ret;
    }


    public static String formatDuration(long duration){
        String ret = "00:00:00";
        if(duration > 0l){
            ret = String.format(Locale.ENGLISH, "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(duration),
                    TimeUnit.MILLISECONDS.toMinutes(duration) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                    TimeUnit.MILLISECONDS.toSeconds(duration) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        }
        return ret;
    }
}
