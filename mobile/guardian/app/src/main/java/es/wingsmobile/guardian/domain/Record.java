package es.wingsmobile.guardian.domain;

import java.util.Date;

import es.wingsmobile.guardian.util.FormatUtil;

/**
 * Created by seba on 19/06/17.
 */

public class Record {

    private String pid;
    private String appName;
    private Date start;
    private Date end;
    private String source;
    private boolean show;
    private String type;
    private String section;
    private boolean recording;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDuration(){
        long mill = end.getTime() - start.getTime();
        return FormatUtil.formatDuration(mill);
    }

    public void setShow(boolean show) {
        this.show = show;
    }
    public boolean isShow() {
        return show;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
    }
    public boolean isRecording() {
        return recording;
    }

    public void setType(String type){this.type=type;}
    public String getType(){return type;}

    public void setSection(String section){this.section=section;}
    public String getSection(){return  section;}
}
