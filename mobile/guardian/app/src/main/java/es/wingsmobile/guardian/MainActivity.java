package es.wingsmobile.guardian;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.navigation.NavigationView;
import es.wingsmobile.guardian.db.DatabaseHelper;
import es.wingsmobile.guardian.domain.Config;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer ;
    private Toolbar toolbar;

    private Switch sw;
    private Switch locked;
    private Switch swTrackMic;
    private Switch swTrackCam;
    private Switch swTrackLoc;
    private DatabaseHelper db;
    private Switch checkDataWeek;
    private Switch checkDataRow;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();

        final NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                //toolbar.setTitle(destination.getLabel());
                toolbar.setTitle(null);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.locked_switch:
                        locked.setChecked(!locked.isChecked());
                        break;
                    case R.id.clear_log:
                        item.setChecked(false);
                        db.clearLogs();
                        if(navController.getCurrentDestination().getId() == R.id.nav_home){
                            navController.navigate(R.id.nav_home);
                        }
                        break;
                    case (R.id.servicerun_switch):
                        sw.setChecked(!sw.isChecked());
                        sw.callOnClick();
                        break;
                    case (R.id.trackMic):
                        swTrackMic.setChecked(!swTrackMic.isChecked());
                        swTrackMic.callOnClick();
                        break;
                    case (R.id.trackCam):
                        swTrackCam.setChecked(!swTrackCam.isChecked());
                        swTrackCam.callOnClick();
                        break;
                    case (R.id.trackLoc):
                        swTrackLoc.setChecked(!swTrackLoc.isChecked());
                        swTrackLoc.callOnClick();
                        break;
                    case (R.id.checkData):
                        break;
                    case (R.id.checkDataWeek):
                        checkDataWeek.setChecked(!checkDataWeek.isChecked());
                        checkDataWeek.callOnClick();
                        break;
                    case (R.id.checkDataRow):
                        checkDataRow.setChecked(!checkDataRow.isChecked());
                        checkDataRow.callOnClick();
                        break;
                    default:
                        item.setChecked(true);
                        navController.navigate(item.getItemId());
                        drawer.closeDrawer(GravityCompat.START,true);
                        break;
                }
                return false;
            }
        });

        sw = (Switch) navigationView.getMenu().findItem(R.id.servicerun_switch).getActionView();
        locked = (Switch) navigationView.getMenu().findItem(R.id.locked_switch).getActionView();
        swTrackMic = (Switch) navigationView.getMenu().findItem(R.id.trackMic).getActionView();
        swTrackCam = (Switch) navigationView.getMenu().findItem(R.id.trackCam).getActionView();
        swTrackLoc = (Switch) navigationView.getMenu().findItem(R.id.trackLoc).getActionView();
        checkDataWeek = (Switch) navigationView.getMenu().findItem(R.id.checkDataWeek).getActionView();
        checkDataRow = (Switch) navigationView.getMenu().findItem(R.id.checkDataRow).getActionView();

        TextView txtVersion = findViewById(R.id.txtVersion);
        txtVersion.setText(BuildConfig.VERSION_NAME);

        final Intent alarmIntent = new Intent(this, CheckService.class);
        //alarmIntent.setFlags(Service.START_STICKY);
        sw.setChecked(isMyServiceRunning(CheckService.class));

        final Config config = db.getConfig();
        if (config.get("start_on_boot") == null) {
            config.put("only_sleep", "false");
            config.put("start_on_boot", "true");
            db.saveConfig(config);
        }

        locked.setChecked(config.get("only_sleep") != null && config.get("only_sleep").equals("true"));

        final String strackMic = config.get("trackMic");
        if (strackMic != null && strackMic.equals("true")) {
            swTrackMic.setChecked(true);
        } else {
            swTrackMic.setChecked(false);
        }

        final String strackCam = config.get("trackCam");
        if (strackCam != null && strackCam.equals("true")) {
            swTrackCam.setChecked(true);
        } else {
            swTrackCam.setChecked(false);
        }

        final String strackLoc = config.get("trackLoc");
        if (strackLoc != null && strackLoc.equals("true")) {
            swTrackLoc.setChecked(true);
        } else {
            swTrackLoc.setChecked(false);
        }

        final String sCheckWeek = config.get("checkWeek");
        if (sCheckWeek != null && sCheckWeek.equals("true")) {
            checkDataWeek.setChecked(true);
        } else {
            checkDataWeek.setChecked(false);
        }

        final String sCheckRows = config.get("checkRows");
        if (sCheckRows != null && sCheckRows.equals("true")) {
            checkDataRow.setChecked(true);
        } else {
            checkDataRow.setChecked(false);
        }

        sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sw.isChecked()) {
                    startService(alarmIntent);
                } else {
                    stopService(alarmIntent);
                }
            }
        });

        locked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("only_sleep", locked.isChecked() + "");
                db.saveConfig(config);

            }
        });

        swTrackMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("trackMic", String.valueOf(swTrackMic.isChecked()));
                db.saveConfig(config);
            }
        });

        swTrackCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("trackCam", String.valueOf(swTrackCam.isChecked()));
                db.saveConfig(config);
            }
        });

        swTrackLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("trackLoc", String.valueOf(swTrackLoc.isChecked()));
                db.saveConfig(config);
            }
        });

        checkDataWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("checkWeek", String.valueOf(checkDataWeek.isChecked()));
                db.saveConfig(config);
            }
        });

        checkDataRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.put("checkRows", String.valueOf(checkDataRow.isChecked()));
                db.saveConfig(config);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mybutton:
                drawer.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
