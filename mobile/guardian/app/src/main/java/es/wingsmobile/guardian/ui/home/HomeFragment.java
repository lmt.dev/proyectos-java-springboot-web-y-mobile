package es.wingsmobile.guardian.ui.home;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import es.wingsmobile.guardian.R;
import es.wingsmobile.guardian.db.DatabaseHelper;
import es.wingsmobile.guardian.domain.Record;
import es.wingsmobile.guardian.util.FormatUtil;

public class HomeFragment extends Fragment {

    public static final String TAG = "HomeFragment";
    private HomeViewModel homeViewModel;
    private LinearLayout event_container;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel = new ViewModelProvider(this, new HomeViewModelFactory(getContext())).get(HomeViewModel.class);
        homeViewModel.setDataBaseHelper(new DatabaseHelper(getContext()));
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        //final TextView textView = root.findViewById(R.id.text_home);
        mSwipeRefreshLayout = root.findViewById(R.id.swiperefresh_items);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homeViewModel.refreshData();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        event_container = root.findViewById(R.id.event_container);
        homeViewModel.getEventRecords().observe(getViewLifecycleOwner(), new Observer<List<Record>>() {
            @Override
            public void onChanged(final List<Record> records) {
                loadData(records);
            }
        });
        return root;
    }

    private void loadData(List<Record> records) {
        event_container.removeAllViews();
        event_container.removeAllViewsInLayout();

        LayoutInflater inf = getLayoutInflater();
        boolean vIsTableHover = false;
        for (Record r : records) {
            ConstraintLayout item = (ConstraintLayout) inf.inflate(R.layout.item_event_layout, null);
            if (!vIsTableHover){
                vIsTableHover=true;
            }
            else {
                item.setBackgroundColor(getResources().getColor(R.color.design_default_color_background));
                vIsTableHover=false;
            }
            TextView t = item.findViewById(R.id.txt_appName);
            String appName = r.getAppName();
            t.setText(appName);
            boolean isAppDeleted = false;
            try {
                appName = appName.replaceAll(":search", "");
                ApplicationInfo ai = getContext().getPackageManager().getApplicationInfo(appName, 0);
                t.setText(getContext().getPackageManager().getApplicationLabel(ai));
            } catch (Exception e) {
                isAppDeleted = true;
                Log.d("TEST", "loadEvents: error ", e);
            }

            try {
                Drawable drawable = getContext().getPackageManager().getApplicationIcon(appName);
                ImageView v = (ImageView) item.findViewById(R.id.app_icon);
                v.setImageDrawable(drawable);
            } catch (Exception e) {
                ImageView v = (ImageView) item.findViewById(R.id.app_icon);
                v.setImageResource(R.drawable.ic_delete_sw);
            }

            ((TextView) item.findViewById(R.id.txt_duration)).setText(r.getDuration());
            try {
                String typeRecord = r.getType();
                String sectionRecord = r.getSection().toUpperCase().trim();
                if (typeRecord != null) {
                    Drawable drawable;
                    switch (typeRecord) {
                        case "MICROPHONE":
                            drawable = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_microphone_png, null);
                            ImageView v = (ImageView) item.findViewById(R.id.app_icon_type);
                            v.setImageDrawable(drawable);

                            if (r.isRecording()){
                                item.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                            }

                            break;
                        case "CAMERA":
                            drawable = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_camera_png, null);
                            ImageView v2 = (ImageView) item.findViewById(R.id.app_icon_type);
                            v2.setImageDrawable(drawable);
                            break;
                        case "LOCATION":
                            drawable = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_location_png, getContext().getTheme());
                            ImageView v3 = (ImageView) item.findViewById(R.id.app_icon_type);
                            v3.setImageDrawable(drawable);
                            break;
                    }
                    boolean isPrecision = false;
                    boolean isDevice = false;
                    switch (sectionRecord) {
                        case "GPS":
                            final TextView tprovgps = (TextView) item.findViewById(R.id.txt_duration);
                            tprovgps.setText(getString(R.string.provgps));
                            isPrecision = true;
                            break;

                        case "PASSIVE":
                            final TextView tprovpassive = (TextView) item.findViewById(R.id.txt_duration);
                            tprovpassive.setText(getString(R.string.provpassive));
                            isPrecision = true;
                            break;

                        case "NETWORK":
                            final TextView tprovnetwork = (TextView) item.findViewById(R.id.txt_duration);
                            tprovnetwork.setText(getString(R.string.provnetwork));
                            isPrecision = true;
                            break;

                        case "FUSED":
                            final TextView tprovfused = (TextView) item.findViewById(R.id.txt_duration);
                            tprovfused.setText(getString(R.string.provfused));
                            isPrecision = true;
                            break;

                        case "FRONTAL":
                        case "NORMAL":
                            final TextView tDevice1 = (TextView) item.findViewById(R.id.txt_duration);
                            tDevice1.setText(sectionRecord);
                            isDevice = true;
                            break;
                    }

                    if (isPrecision) {
                        TextView tSection = item.findViewById(R.id.txt_durationOrSection);
                        tSection.setText(getString(R.string.precision) + ":");
                    }

                    if (isDevice) {
                        TextView tSection = item.findViewById(R.id.txt_durationOrSection);
                        tSection.setText(getString(R.string.device) + ":");
                    }
                } else {
                    Log.d("r NULL", "V");
                }

            } catch (Exception e) {
                Log.d("ERROR SETEANDO TIPO", "Error Seteando TIPO ", e);
            }

            //((TextView) item.findViewById(R.id.txt_source)).setText(r.getSource());
            ((TextView) item.findViewById(R.id.txt_date)).setText(FormatUtil.fromDateToDisplay(r.getStart()));

            final String finalAppName = appName;
            final boolean finalIsAppDeleted = isAppDeleted;
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalIsAppDeleted) {
                        Toast toast = Toast.makeText(getContext().getApplicationContext(), getString(R.string.messageNoMenu), Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        displaySettings(finalAppName);
                    }
                }
            });

            event_container.addView(item);
        }

    }

    public void displaySettings(String packageName) {
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + packageName));
        this.startActivity(i);
        Toast toast = Toast.makeText(getContext().getApplicationContext(), getString(R.string.messageClickInPermission), Toast.LENGTH_SHORT);
        toast.show();
    }

}
