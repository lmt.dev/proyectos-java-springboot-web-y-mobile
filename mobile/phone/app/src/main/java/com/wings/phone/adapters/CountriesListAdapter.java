package com.wings.phone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wings.phone.R;

public class CountriesListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    public CountriesListAdapter(Context context, String[] values) {
        super(context, R.layout.country_list_item, values);
        this.context = context;
        this.values = values;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.country_list_item, parent, false);

        String[] g = values[position].split(",");
        TextView textView = row.findViewById(R.id.txtViewCountryName);
        textView.setText(g[0].trim());

        String pngName = g[1].trim().toLowerCase();
        ImageView imageView = row.findViewById(R.id.imgViewFlag);
        imageView.setImageResource(context.getResources().getIdentifier("drawable/flag_" + pngName, null, context.getPackageName()));

        return row;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.country_list_item, parent, false);
        TextView textView = rowView.findViewById(R.id.txtViewCountryName);
        ImageView imageView = rowView.findViewById(R.id.imgViewFlag);

        String[] g = values[position].split(",");
        textView.setText(g[0].trim());

        String pngName = g[1].trim().toLowerCase();
        imageView.setImageResource(context.getResources().getIdentifier("drawable/flag_" + pngName, null, context.getPackageName()));
        return rowView;
    }*/

}
