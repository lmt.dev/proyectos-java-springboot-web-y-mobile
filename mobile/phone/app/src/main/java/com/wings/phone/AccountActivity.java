package com.wings.phone;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wings.phone.fragments.AcountDetailsFragment;
import com.wings.phone.fragments.AcountRegisterFragment;
import com.wings.phone.util.Settings;

public class AccountActivity extends AppCompatActivity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account);
        getSupportActionBar().setTitle(getString(R.string.account));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Settings.isRegistered()) {
            ((TextView) findViewById(R.id.account_title)).setText(R.string.account_register_details);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.account_register_container, new AcountDetailsFragment(), AcountDetailsFragment.TAG)
                    .commit();
        } else {

            ((TextView) findViewById(R.id.account_title)).setText(R.string.account_register_title);

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.account_register_container, new AcountRegisterFragment(), AcountDetailsFragment.TAG)
                    .commit();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     *
     * @param view
     */
    public void showPrivacy(View view) {
        String url = getString(R.string.privacy_policy_privacy_url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    /**
     *
     * @param view
     */
    public void showSales(View view) {
        String url = getString(R.string.privacy_policy_sales_url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
