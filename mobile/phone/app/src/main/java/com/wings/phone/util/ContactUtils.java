package com.wings.phone.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;

import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;


public class ContactUtils {


    public static ArrayList newArrayContactsWihtinfo(Context context){
        ArrayList<Contact> contactsArr = new ArrayList<>();
        Map<String, Contact> contactsMap = new HashMap<String, Contact>();
        Map<String, String> numbersMap = new HashMap<String, String>();




        String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID,
                ContactsContract.RawContacts.ACCOUNT_TYPE,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                 };


        String selectionFields =  ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?";
        String[] selectionArgs = new String[]{"com.google", "Local Phone Account", "USIM Account"};

        Cursor cursor =  context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                selectionFields,
                selectionArgs,
                ContactsContract.Contacts.DISPLAY_NAME
        );
        cursor.move(-1);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String contactId = cursor.getString(1);
            String rawContactId = cursor.getString(2);
            String accountType = cursor.getString(3);
            String displayName = cursor.getString(4);
            String photoUri = cursor.getString(5);
            String number = cursor.getString(6);


            Contact contacto_temp;

            if (contactsMap.containsKey(displayName)){
                contacto_temp = contactsMap.get(displayName);
            } else {
                contacto_temp = new Contact();
                contacto_temp.setName(displayName);
                contacto_temp.setId(Integer.parseInt(contactId));
                contacto_temp.setRawId(Integer.parseInt(rawContactId));
                contacto_temp.setPhotoUri(photoUri);
            }
            if(!numbersMap.containsKey(number)){
                Number n = new Number();
                n.setInPhoneAccount(true);
                n.setRawNumber(number);
                try {
                    n.setNumber(PhoneNumberUtils.formatNumberToE164(number, com.wings.phone.util.Settings.getCountry()).replaceAll("\\+", ""));
                } catch (Exception e) {
                }

                try {
                    n.setFormattedNumber(PhoneNumberUtils.formatNumber(number, com.wings.phone.util.Settings.getCountry()));
                } catch (Exception e) {
                }
                contacto_temp.addNumber(n);
                if(n.getFormattedNumber()==null){
                    continue; //ignorar numero
                }
                numbersMap.put(number,displayName);
            }




            contactsMap.put(displayName,contacto_temp);




        }
        cursor.close();



        SortedSet<String> keys = new TreeSet<>(contactsMap.keySet());
        for (String key : keys) {
            Contact c = contactsMap.get(key);
            contactsArr.add(c);
        }



        return contactsArr;
    }

    public static List<Contact> getFavContats(Context context){
        List<Contact> result = new LinkedList<>();
        Map<String, Contact> contactsMap = new HashMap<String, Contact>();
        final String[] projection = new String[]{
                ContactsContract.PhoneLookup._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.PHOTO_URI,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID,
                ContactsContract.PhoneLookup.STARRED
        };
        final String selection ="(" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " != ? ) AND " +ContactsContract.PhoneLookup.STARRED + " = ?";
        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                projection,
                selection,
                new String[]{"0","1"},
                ContactsContract.PhoneLookup.DISPLAY_NAME);

        while (cursor != null && cursor.moveToNext()) {
            Contact contacto_temp;
            int id = cursor.getInt(0); // contact ID
            String name = cursor.getString(1); // contact name
            String nameRawConactId = cursor.getString(4);
            String photoUri = null;
            try {
                photoUri = cursor.getString(2);
            } catch (Exception ex) {
            }
            String data1 = null;
            String data4 = null;

            try {
                data1 = cursor.getString(3);
            } catch (Exception ex) {
            }
            if (cursor.getColumnCount() > 5) {
                try {
                    data4 = cursor.getString(5);
                } catch (Exception ex) {
                }

            }

            if (contactsMap.containsKey(name)){
                contacto_temp = contactsMap.get(name);
            } else {
                contacto_temp = new Contact();
                contacto_temp.setName(name);
                contacto_temp.setId(id);
                contacto_temp.setRawId(Integer.parseInt(nameRawConactId));
                contacto_temp.setPhotoUri(photoUri);
            }

            result.add(contacto_temp);

        }

        assert cursor != null;
        cursor.close();

        return result;
    }


    public static ArrayList<Contact> getContactsArray(Context context,String[] GROUP_PROJECTION, String SELECTION, String[] ARGS, Uri URI) {
        Map<String, Contact> contactsMap = new HashMap<String, Contact>();
        ArrayList<Contact> contactsArr = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(
                URI,
                GROUP_PROJECTION,
                SELECTION,
                ARGS,
                ContactsContract.PhoneLookup.DISPLAY_NAME);
        Contact contacto_temp;
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(0); // contact ID
            String name = cursor.getString(1); // contact name
            String nameRawConactId = cursor.getString(4);
            String photoUri = null;
            try{ photoUri = cursor.getString(2); } catch (Exception ex) {}
            String data1 = null;
            String data4 = null;

            try{ data1 = cursor.getString(3); } catch (Exception ex) {}
            if(cursor.getColumnCount()>5){
                try{ data4 = cursor.getString(5); } catch (Exception ex) {}
            }
            //try{ data4 = cursor.getString(5); } catch (Exception ex) {}

            // add info to existing list if this contact-id was already found, or create a new list in case it's new
            //String key = id + " - " + name;
            if (contactsMap.containsKey(name)){
                contacto_temp = contactsMap.get(name);
            } else {
                contacto_temp = new Contact();
                contacto_temp.setName(name);
                contacto_temp.setId(id);
                contacto_temp.setRawId(Integer.parseInt(nameRawConactId));
                contacto_temp.setPhotoUri(photoUri);
            }
            Number number = new Number();

            if(data4!=null){
                number.setNumber(data4);
                number.setFormattedNumber(PhoneNumberUtils.formatNumber(data4, com.wings.phone.util.Settings.getCountry()));
                if(number.getNumber()!=null){
                    contacto_temp.addNumber(number);
                }

            }else if(data1!=null){
                number.setNumber(data1);
                if(number.getNumber()!=null){
                    contacto_temp.addNumber(number);
                }
            }

            if(contactsMap.containsKey(name)){
                Contact contact = contactsMap.get(name);
                contact.addNumber(number);
                contactsMap.put(name,contact);
            }else{
                contactsMap.put(name, contacto_temp);

            }

        }

        assert cursor != null;
        cursor.close();

        Object[] keys = contactsMap.keySet().toArray();
        Arrays.sort(keys);

        for(Object k : keys){
            Contact contacto = contactsMap.get(k);
            contactsArr.add(contacto);
        }






        return contactsArr;
    }



    public static void readContacts(Context context){
        List<Contact> contactos = new LinkedList<>();
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                Contact contact = new Contact();
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //System.out.println("name : " + name + ", ID : " + id);
                    contact.setId(Integer.parseInt(id));
                    contact.setName(name);

                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Number number = new Number();
                        number.setRawNumber(phone);
                        number.setFormattedNumber(PhoneNumberUtils.formatNumber(phone, Settings.getCountry()));
                        contact.addNumber(number);
                    }
                    pCur.close();


                }
                contactos.add(contact);
            }

        }
    }

    public static void dumpCursor(Cursor cursor){

        if (cursor != null) {
            int startPos = cursor.getPosition();
            int index = 0;

            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                String linea = "";
                for(String columName : cursor.getColumnNames()){
                    String value = null;
                    switch (cursor.getType(cursor.getColumnIndex(columName))){
                        case Cursor.FIELD_TYPE_NULL:
                            break;
                        case Cursor.FIELD_TYPE_INTEGER:
                            value = String.valueOf(cursor.getInt(cursor.getColumnIndex(columName)));
                            break;
                        case Cursor.FIELD_TYPE_STRING:
                            value = cursor.getString(cursor.getColumnIndex(columName));
                            break;
                        case Cursor.FIELD_TYPE_FLOAT:
                            value = String.valueOf(cursor.getFloat(cursor.getColumnIndex(columName)));
                            break;
                        case Cursor.FIELD_TYPE_BLOB:
                            break;
                    }
                    if(value!=null){
                        String l = columName + "=" + value ;
                        linea = linea + l + " ";
                    }

                }
                //escribir linea
                System.out.println(linea + "\n");
                index=index+1;
            }
            cursor.moveToPosition(startPos);
        }


    }



    public static void dumpContacts(Context context) {




        String[] projection = new String[]{"_id", "display_name", "has_phone_number", "name_raw_contact_id"};

        //CONTACTS
        System.out.println("---------------CONTACTS-----------------------------------------------");
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null);

        //DatabaseUtils.dumpCursor(cursor);
        dumpCursor(cursor);


        projection = new String[]{"_id", "display_name", "raw_contact_id", "account_type_and_data_set", "mimetype", "name_raw_contact_id"};
        //PhoneContent
        System.out.println("---------------PHONE-----------------------------------------------");
        Cursor cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                null,
                null,
                null);

        //DatabaseUtils.dumpCursor(cursorPhone);
        dumpCursor(cursorPhone);



        //RAW
        projection = new String[]{"_id", "account_type", "contact_id", "display_name", "deleted"};

        System.out.println("---------------RAW-----------------------------------------------");
        Cursor cursorRaw = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI,
                null,
                null,
                null,
                null);

        //DatabaseUtils.dumpCursor(cursorRaw);
        dumpCursor(cursorRaw);


        //DATA
        projection = new String[]{"_id", "account_type", "raw_contact_id", "contact_id", "display_name", "mimetype", "data1", "data2", "data3"};
        System.out.println("---------------DATA-----------------------------------------------");
        Cursor cursorData = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                null,
                null,
                null);

        //DatabaseUtils.dumpCursor(cursorData);
        dumpCursor(cursorData);



    }
}
