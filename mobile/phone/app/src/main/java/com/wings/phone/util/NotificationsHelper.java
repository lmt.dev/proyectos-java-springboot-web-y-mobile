package com.wings.phone.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.wings.phone.CallActivity;
import com.wings.phone.MainActivity;
import com.wings.phone.R;
import com.wings.phone.telecom.CallBuddy;

import java.io.InputStream;
import java.net.URLEncoder;

/**
 * Created by seba on 11/04/17.
 */

public class NotificationsHelper {


    public static Notification getMissNotif(Context context, String number) {

        Intent resultIntent = new Intent(context, MainActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        Intent dismissIntent = new Intent(context, MainActivity.class);
        dismissIntent.setAction("ACTION_DISMISS");
        PendingIntent piDismiss = PendingIntent.getActivity(context, 0, dismissIntent, 0);

        PendingIntent piCall = null;
        if (number != null) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + URLEncoder.encode(number)));
            piCall = PendingIntent.getActivity(context, 0, callIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            number = context.getString(android.R.string.unknownName);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_phone_missed_black_24dp)
                .setContentTitle(context.getString(R.string.missed_call))
                .setContentIntent(resultPendingIntent)
                .setContentText(context.getString(R.string.missed_call_click))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(context.getString(R.string.missed_call_text, number)))
                .addAction(R.drawable.ic_clear_black_24dp, context.getString(R.string.dismiss), piDismiss)
                .addAction(R.drawable.ic_phone_black_24dp, context.getString(R.string.call), piCall);

        return mBuilder.build();
    }



    public static Notification getCurrentNotif(Context context, CallBuddy buddy, String number) {

        Intent resultIntent = new Intent(context, CallActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent hangUpIntent = new Intent(context, CallActivity.class);
        hangUpIntent.setAction("ACTION_HANGUP");
        hangUpIntent.setClass(context, CallActivity.class);
        PendingIntent piHangUp = PendingIntent.getActivity(context, 0, hangUpIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*Intent muteIntent = new Intent(context, CallActivity.class);
        muteIntent.setAction("ACTION_MUTE");
        hangUpIntent.setClass(context, CallActivity.class);
        PendingIntent piMute = PendingIntent.getActivity(context, 0, muteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent speakerIntent = new Intent(context, CallActivity.class);
        speakerIntent.setAction("ACTION_SPEAKER");
        hangUpIntent.setClass(context, CallActivity.class);
        PendingIntent piSpeaker = PendingIntent.getActivity(context, 0, speakerIntent, PendingIntent.FLAG_UPDATE_CURRENT);*/

        Bitmap bitmap;
        if (buddy.getPhotoUri() != null) {
            Uri uri = Uri.parse(buddy.getPhotoUri());
            bitmap = getContactBitmapFromURI(context, uri);
        } else {
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.baseline_person_black_24);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("WINGSPHONE", "Wings Phone", NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("Canal de notificaciones de Wings Phone");
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationManager.createNotificationChannel(channel);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "WINGSPHONE")
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_phone_black_24dp)
                .setContentTitle(context.getString(R.string.current_call))
                .setLargeIcon(bitmap)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setChannelId("WINGSPHONE")
                .setOngoing(true)
                .setCategory(Notification.CATEGORY_CALL)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("" + buddy.getName() + "  " + (number != null ? number : "")))
                .addAction(R.drawable.ic_call_end_black_24dp, context.getString(R.string.hang_up), piHangUp);
                /*.addAction(R.drawable.ic_mic_off_black_24dp, "Mute", piMute)
                .addAction(R.drawable.ic_volume_off_black_24dp, "Speaker", piSpeaker);*/

        Notification ret = mBuilder.build();
        ret.flags = Notification.FLAG_NO_CLEAR;
        return ret;
    }

    private static Bitmap getBitmap(int color, Context context) {
        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.logo_wings_transparent, null);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);

        //set round background on lolipop+ and square before
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Paint p = new Paint();
            p.setAntiAlias(true);
            p.setColor(ContextCompat.getColor(context, color));
            canvas.drawCircle(canvas.getHeight() / 2, canvas.getHeight() / 2, canvas.getHeight() / 2, p);
        } else {
            canvas.drawColor(ContextCompat.getColor(context, color));
        }
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap getContactBitmapFromURI(Context context, Uri uri) {
        Bitmap ret = null;
        try {
            InputStream input = context.getContentResolver().openInputStream(uri);
            if (input == null) {
                return null;
            }
            ret = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
        }
        return ret;
    }

    public static Notification showWorkingNotification(Context context) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "WINGSPHONE")
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_sync_black_24dp)
                .setContentTitle("Updating")
                .setContentText("Updating provisioning settings");
        return mBuilder.build();
    }

    public static void showSecretNotification(Context context, String title, String description) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "WINGSPHONE")
                .setSmallIcon(R.drawable.ic_logo_wings_color)
                .setContentTitle(title)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setContentText(description)
                .setSound(defaultSoundUri);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = 1;
        createPublicChannel(notificationManager);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private static void createSecretChannel(NotificationManager notificationManager) {
        NotificationChannel channel = new NotificationChannel("WINGSPHONE", "Wings Phone", NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("Canal de notificaciones de Wings Phone");
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
    }

    private static void createPublicChannel(NotificationManager notificationManager) {
        NotificationChannel channel = new NotificationChannel("WINGSPHONE", "Wings Phone", NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("Canal de notificaciones de Wings Phone");
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
    }

}
