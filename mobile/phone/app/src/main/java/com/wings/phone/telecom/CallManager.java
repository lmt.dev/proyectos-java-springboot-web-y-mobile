package com.wings.phone.telecom;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.provider.CallLog;
import android.telecom.Call;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.wings.phone.CallActivity;
import com.wings.phone.R;
import com.wings.phone.data.DataProvider;
import com.wings.phone.telecom.sip.SipListener;
import com.wings.phone.telecom.sip.SipManager;
import com.wings.phone.util.WingsConst;

import org.linphone.core.Core;
import org.linphone.core.MediaEncryption;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import timber.log.Timber;

public class CallManager {



    private SipListener sipListener = new SipListener() {
        @Override
        public void onRegistrationStateChanged(CallWrapper wrapper) {
            listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");
        }

        @Override
        public void onCallStateChanged(org.linphone.core.Call call, org.linphone.core.Call.State cstate, String message) {



            if (!message.equals("Call released")) {
                CallWrapper wrapper = callMap.get(getUsername(call));

                if (wrapper != null) {
                    translateFromSipToWingsStatus(wrapper);

                    if (wrapper.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE) && wrapper.getActivationTime() == 0) {
                        wrapper.setActivationTime(System.currentTimeMillis());
                    }
                    listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");

                    if (wrapper.getStatus().equals(WingsConst.CALL_STATUS_DISCONNECTED)) {
                        removeCall(getUsername(call));//sip
                    }
                }
            }
        }

        @Override
        public void onEncryptionChanged(org.linphone.core.Call call, boolean on, String authenticationToken) {
            CallWrapper wrapper = callMap.get(getUsername(call));
            if (on) {
                wrapper.setSecret(authenticationToken);
            }
            listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");
        }

        @Override
        public void onCallCreated(Core core, org.linphone.core.Call call) {

            //System.out.println("CAXXMANAGER call created");

            if (regTimeoutTimer != null) {
                regTimeoutTimer.cancel();
            }

            CallWrapper wrapper = callMap.get(getUsername(call));
            if (wrapper == null) {
                CallBuddy buddy = DataProvider.getCallBuddy(context, getUsername(call));

                wrapper = new CallWrapper(call);
                wrapper.setCallBuddy(buddy);
                wrapper.setDirection(WingsConst.CALL_DIRECTION_INCOMING);

                MediaEncryption me = call.getCore().getMediaEncryption();
                boolean secure = me.name().equals("ZRTP");
                wrapper.setType(secure ? WingsConst.CALL_TYPE_SECURE : WingsConst.CALL_TYPE_FREE);
                translateFromSipToWingsStatus(wrapper);

                addCall(wrapper);

                Intent intent = new Intent(context, CallActivity.class);
                intent.setAction(WingsConst.INTENT_WINGS_INCOMING);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                //intent.putExtra("body", body);
                context.startActivity(intent);

            } else {
                listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");
            }

        }

        @Override
        public void prepareTimeout() {
            if (callList.get(0).getDirection() == WingsConst.CALL_DIRECTION_INCOMING) {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        CallWrapper wrapper = callList.get(0);
                        wrapper.setStatus(WingsConst.CALL_STATUS_DISCONNECTED);
                        listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");
                        removeCall(wrapper.getCallBuddy().getCleanNumber());
                    }
                };
                regTimeoutTimer = new Timer();
                regTimeoutTimer.schedule(task, 10000);
            }
        }

        @Override
        public void onStatsUpdate(String stats) {
            listener.onStatsUpdate(stats);
        }

        private String getUsername(org.linphone.core.Call call) {
            return call.getRemoteAddress().getUsername()
                    .replaceAll("int", "")
                    .replaceAll("sec", "");
        }

    };

    private static CallManager instance;
    private Context context;

    private List<CallWrapper> callList = new ArrayList<>();
    private Map<String, CallWrapper> callMap = new HashMap<>();
    private CallWrapper activeCall;
    private CallWrapperListener listener;
    private SipManager sipManager;
    private Timer regTimeoutTimer;
    private boolean inConference = false;
    private boolean doHoldFix = false;

    public static synchronized CallManager getInstance(@Nullable Context context) {
        if (instance == null) {
            instance = new CallManager();
        }
        instance.context = context;
        return instance;
    }

    private CallManager() {
    }

    public void terminate() {

        if (!callList.isEmpty()) {
            callList.clear();
        }

        if (!callMap.isEmpty()) {
            callMap.clear();
        }

        if (sipManager != null) {
            sipManager.destroy();
        }

    }

    public void sipRegister(boolean useZrtp) {
        sipManager = SipManager.getInstance(context, sipListener);
        sipManager.register(useZrtp);
    }

    public void makeSipCall(CallBuddy buddy) {

        CallWrapper wrapper = new CallWrapper();
        wrapper.setCallBuddy(buddy);
        wrapper.setSip(true);
        wrapper.setDirection(WingsConst.CALL_DIRECTION_OUTGOING);

        wrapper.setType(WingsConst.CALL_TYPE_FREE);
        if (buddy.getSipUri().contains("sec")) {
            wrapper.setType(WingsConst.CALL_TYPE_SECURE);
        } else if (buddy.getSipUri().contains("int")) {
            wrapper.setType(WingsConst.CALL_TYPE_WINGS);
        }
        addCall(wrapper);

        sipManager = SipManager.getInstance(context, sipListener);
        sipManager.makeCall(wrapper);

        listener.onCallStatusChange(wrapper, wrapper.getStatus(), "");
    }

    public void addCall(final CallWrapper call) {



        callList.add(call);
        if (call.isSip()) {
            callMap.put(call.getCallBuddy().getCleanNumber(), call);
        } else {
            callMap.put(String.valueOf(call.getGsmCall().hashCode()), call);
        }
        activeCall = call;

        //mark all as no current
        for (CallWrapper wr : callList) {
            wr.setCurrent(false);
        }
        //this one as current
        call.setCurrent(true);


        //gsm listener
        if (!call.isSip()) {
            call.addGsmCallback(new Call.Callback() {
                @Override
                public void onStateChanged(Call call1, int state) {
                    if(doHoldFix){
                        doHoldFix=false;
                        if(state == 3){
                            //Timber.d("ignorar hold");
                            state = 4; //active
                            call.setStatus(WingsConst.CALL_STATUS_ACTIVE);
                            call.setCurrent(true);
                            call.unHold();
                        }
                    }else{

                    }
                    if(state == 7){
                        doHoldFix = true;
                    }
                    super.onStateChanged(call1, state);


                    if (call.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE) && call.getActivationTime() == 0) {
                        call.setActivationTime(System.currentTimeMillis());
                    }
                    if (call != null && listener != null ) {
                        listener.onCallStatusChange(call, call.getStatus(), call.getLastReason());

                    }
                }

                @Override
                public void onDetailsChanged(Call call, Call.Details details) {
                    super.onDetailsChanged(call, details);
                }
            });
        }
    }

    public void removeCall(String number) {

        CallWrapper removed = callMap.remove(number);
        callList.remove(removed);

        if (removed == null) {
            return;
        }

        //TODO: revisar esto
        if (context != null && removed.isSip()) {
            try {
                ContentValues values = getCallLog(removed);
                writeCallLog(values);
            } catch (Exception e) {
            }
        }

        if (callList.isEmpty()) {
            //TODO: ver que no queden sip nada mas
            //TODO: temp
            if (sipManager != null) {
                sipManager.destroy();
            }

            activeCall = null;
        } else {
            setActiveCall(callList.get(0));
        }

        if (callList.size() <= 1 && inConference) {
            inConference = false;
        }

        if (callList.isEmpty()) {
            try {
                AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                audioManager.setMicrophoneMute(false);
                audioManager.setSpeakerphoneOn(false);
            } catch (Exception e) {
            }
        }

        listener.onCallRemoved(removed);
        //activeCall = null;
    }

    public List<CallWrapper> getCallList() {
        return callList;
    }

    public CallWrapper getActiveCall() {
        return activeCall;
    }

    public void setActiveCall(CallWrapper activeCall) {
        for (CallWrapper wrapper : callList) {
            if(wrapper != activeCall){
                wrapper.setCurrent(false);
            }
        }
        activeCall.setCurrent(true);
        activeCall.unHold();
        this.activeCall = activeCall;
    }

    public void answer() {
        if (callList.size() > 1) {
            for (CallWrapper wrapper : callList) {
                if (!wrapper.equals(activeCall)) {
                    wrapper.hold();
                }
            }
        }
        try{
            activeCall.answer();

        }catch (Exception e){

        }
    }

    public void reject() {
        activeCall.decline();
    }

    public void hangUp() {
        if (callList.size() > 1) {
            for (CallWrapper wrapper : callList) {
                if (!wrapper.equals(activeCall)) {
                    wrapper.unHold();
                }
            }
        }
        if (activeCall != null) {
            activeCall.hangUp();
        }
    }

    public boolean isInConference() {
        return inConference;
    }

    public boolean toogleHold() {

        if (activeCall == null) {
            return false;
        }


        if (activeCall.isOnHold()) {
            activeCall.unHold();
        } else {
            activeCall.hold();
        }
        //Timber.e("activeCall.isOnHold(): %s",String.valueOf(activeCall.isOnHold()));
        return activeCall.isOnHold();
    }

    public void setListener(CallWrapperListener listener) {
        this.listener = listener;
    }

    public void translateFromSipToWingsStatus(CallWrapper wrapper) {

        if (wrapper == null) {
            Timber.e("Error getting status wrapper is null");
            return;
        }

        if (wrapper.getSipCall() == null) {
            Timber.e("Error getting status sipCall is null");
            return;
        }

        //TODO: agregar mas estados en las transaltes para marcar el remote ringing,
        //remote holding localholding , etc
        org.linphone.core.Call sipCall = wrapper.getSipCall();
        /*Idle(0),
        IncomingReceived(1),
        OutgoingInit(2),
        OutgoingProgress(3),
        OutgoingRinging(4),
        OutgoingEarlyMedia(5),
        Connected(6),
        StreamsRunning(7),
        Pausing(8),
        Paused(9),
        Resuming(10),
        Referred(11),
        Error(12),
        End(13),
        PausedByRemote(14),
        UpdatedByRemote(15),
        IncomingEarlyMedia(16),
        Updating(17),
        Released(18),
        EarlyUpdatedByRemote(19),
        EarlyUpdating(20);*/

        switch (sipCall.getState().toInt()) {
            case 0:
                wrapper.setStatus(WingsConst.CALL_STATUS_INIT);
                break;
            case 1:
                wrapper.setStatus(WingsConst.CALL_STATUS_RINGING);
                break;
            case 2:
                wrapper.setStatus(WingsConst.CALL_STATUS_CONNECTING);
                break;
            case 3:
                wrapper.setStatus(WingsConst.CALL_STATUS_DIALING);
                break;
            case 4:
                wrapper.setStatus(WingsConst.CALL_STATUS_DIALING);
                break;
            case 5:
                wrapper.setStatus(WingsConst.CALL_STATUS_CONNECTING);
                break;
            case 6:
            case 7:
                wrapper.setStatus(WingsConst.CALL_STATUS_ACTIVE);
                break;
            case 8:
            case 9:
                wrapper.setStatus(WingsConst.CALL_STATUS_HOLDING);
                break;
            case 10:
                break;
            case 11:
                break;
            case 12:
            case 13:
                wrapper.setStatus(WingsConst.CALL_STATUS_DISCONNECTED);
                break;
            case 14:
                wrapper.setStatus(WingsConst.CALL_STATUS_HOLDING);
                break;
            case 15:
                break;
            case 16:
                wrapper.setStatus(WingsConst.CALL_STATUS_RINGING);
                break;
            case 17:
                break;
            case 18:
                wrapper.setStatus(WingsConst.CALL_STATUS_DISCONNECTED);
                break;
            case 19:
                break;
            case 20:
                break;

        }
    }

    private void writeCallLog(ContentValues values) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALL_LOG) == PackageManager.PERMISSION_GRANTED && values.size() != 0) {
            context.getContentResolver().insert(CallLog.Calls.CONTENT_URI, values);
        }
    }

    private ContentValues getCallLog(CallWrapper call) {

        ContentValues values = new ContentValues();

        try {
            values.put(CallLog.Calls.NUMBER, call.getCallBuddy().getCleanNumber());
            values.put(CallLog.Calls.DATE, System.currentTimeMillis());

            try {
                //values.put(CallLog.Calls.DURATION, ((SipCall) call).getDuration());
            } catch (Exception e) {
                values.put(CallLog.Calls.DURATION, 0);
            }

            values.put(CallLog.Calls.TYPE, call.getDirection() == WingsConst.CALL_DIRECTION_INCOMING ? 1 : 2);
            values.put(CallLog.Calls.NEW, 1);
            values.put(CallLog.Calls.CACHED_NAME, call.getCallBuddy().getName());
            values.put(CallLog.Calls.CACHED_NUMBER_TYPE, 0);
            values.put(CallLog.Calls.CACHED_NUMBER_LABEL, "");
            values.put(CallLog.Calls.CACHED_NUMBER_LABEL, "");
            values.put(CallLog.Calls.DATA_USAGE, "");

            String accountId = null;
            switch (call.getType()) {
                case WingsConst.CALL_TYPE_FREE:
                    accountId = WingsConst.ACCOUNT_FREE;
                    break;
                case WingsConst.CALL_TYPE_WINGS:
                    accountId = WingsConst.ACCOUNT_WINGS;
                    break;
                case WingsConst.CALL_TYPE_SECURE:
                    accountId = WingsConst.ACCOUNT_SECURE;
                    break;
                case WingsConst.CALL_TYPE_VIDEO:
                    accountId = WingsConst.ACCOUNT_VIDEO;
                    break;
            }
            values.put(CallLog.Calls.PHONE_ACCOUNT_ID, accountId);

        } catch (Exception e) {
            Timber.e(e);
        }

        return values;
    }

    public void swapActiveCall() {
        int idx = callList.indexOf(activeCall);
        CallWrapper newActive;
        for (CallWrapper c : callList) {
            c.hold();
        }
        if (idx + 1 < callList.size()) {
            newActive = callList.get(idx + 1);
        } else {
            newActive = callList.get(0);
        }

        for (CallWrapper wrapper : callList) {
            wrapper.setCurrent(false);
        }
        activeCall = newActive;
        activeCall.setCurrent(true);
        activeCall.unHold();

    }

    public boolean canMakeConference() {

        //if there's more than 1 call
        if (callList.size() < 2) {
            return false;
        }

        int sips = 0;
        for (CallWrapper call : callList) {
            sips += call.isSip() ? 1 : 0;
        }
        //if all call are not sip, or all are sip
        return sips == 0 || sips == callList.size();

    }

    /**
     * Transmit audio media from call1 to call2 and from call2 to call1
     *
     * @param call1
     * @param call2
     */
    public int makeConference(CallWrapper call1, CallWrapper call2) {

        if (call1 == null || call2 == null) {
            return R.string.conference_not_available;
        }

        int ret = 0;
        if (call1.isSip() && call2.isSip()) {
            try {
                sipManager.createConference(call1.getSipCall(), call2.getSipCall());
                inConference = true;
                ret = R.string.conference_created;
            } catch (Exception e) {
                Timber.e(e);
                ret = R.string.conference_cannot_make;
                inConference = false;
            }
        } else if (!call1.isSip() && !call2.isSip()) {

            try {
                call1.getGsmCall().conference(call2.getGsmCall());
                call1.getGsmCall().mergeConference();

                inConference = true;
                ret = R.string.conference_created;
            } catch (Exception e) {
                Timber.e(e);
                ret = R.string.conference_cannot_make;
                inConference = false;
            }
        }
        return ret;
    }

    /**
     * Stop Transmit audio media from call1 to call2 and from call2 to call1
     *
     * @param call1
     * @param call2
     */
    public int removeConference(CallWrapper call1, CallWrapper call2) {

        if (call1 == null || call2 == null) {
            inConference = false;
            return R.string.conference_not_available;
        }

        int ret = 0;
        if (call1.isSip() && call2.isSip()) {

            try {
                sipManager.removeConference(call1.getSipCall(), call2.getSipCall());
                inConference = false;
                ret = R.string.conference_destroyed;
            } catch (Exception e) {
                Timber.e(e);
                ret = R.string.conference_cannot_make;
            }

        } else if (!call1.isSip() && !call2.isSip()) {

            try {

                call1.getGsmCall().splitFromConference();
                call2.getGsmCall().splitFromConference();

                inConference = false;
                ret = R.string.conference_destroyed;
            } catch (Exception e) {
                Timber.e(e);
                ret = R.string.conference_cannot_make;
            }

        }
        return ret;
    }

    public void transferCall(int callId, CallBuddy buddy) {

        for (CallWrapper call : callList) {
            if (call.getId() == callId) {
                String destUri = buddy.getSipUri();
                //String destUri = "sip:" + dest.getNumbers().get(0).getNumberForSip() + "@79.143.183.86";
                //SipManager.getInstance().transferCall((SipCall) call, destUri);
                call.getSipCall().transfer(destUri);
            }
        }
    }

}
