package com.wings.phone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.MainActivity;
import com.wings.phone.R;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.voip.model.contact.Contact;

import java.util.List;
import java.util.Random;

public class FavoritesAdapter extends BaseAdapter {

    private Context context;
    private List<Contact> contacts;
    private MainActivity mainActivity;

    public FavoritesAdapter(Context context, List<Contact> contacts, MainActivity mainActivity){
        this.context = context;
        this.contacts = contacts;
        this.mainActivity = mainActivity;

    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Contact getItem(int position) { return contacts.get(position); }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.view_favorite_item, null);
        }
        Contact current = (Contact)getItem(i);
        ((TextView) view.findViewById(R.id.favorite_name)).setText(current.getName());
        ImageView avatar = (ImageView) view.findViewById(R.id.favorite_avatar);
        TextView avatar_name = view.findViewById(R.id.favorite_name);
        if(current.getPhotoUri()!=null){
            Picasso.get().load(current.getPhotoUri()).transform(new CircleTransform()).into(avatar);
        }else{
            Random r = new Random();
            int a = current.getId();
            String letter = current.getName().substring(0,1);
            TextDrawable drawable = TextDrawable.builder().buildRound(letter, ColorGenerator.MATERIAL.getColor(a));
            avatar.setImageDrawable(drawable);

        }

        avatar_name.setText(current.getName());


        try {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.showContactFiltered(current);
                }
            });
        } catch (Exception e) {
        }

        return view;
    }

    public void setContactList(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
