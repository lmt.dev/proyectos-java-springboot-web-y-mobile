package com.wings.phone.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Utility class to enable/disable CallActivity if the call
 * it's the missed call from registration
 */
public class ActivUtils {

    public static void enableGsmCallActivity(Context context) {
        enableActivity(context, ".CallActivity", true);
    }

    public static void disableGsmCallActivity(Context context) {
        enableActivity(context, ".CallActivity", false);
    }

    public static boolean isGsmActivityEnabled(Context context) {
        PackageManager pm = context.getPackageManager();
        ComponentName compName = new ComponentName(context.getPackageName(), context.getPackageName() + ".CallActivity");
        return pm.getComponentEnabledSetting(compName) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
    }

    private static void enableActivity(Context context, String activity, boolean flag) {

        int enabled;
        if (flag) {
            enabled = PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
        } else {
            enabled = PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
        }

        PackageManager pm = context.getPackageManager();
        ComponentName compName = new ComponentName(context.getPackageName(), context.getPackageName() + activity);
        pm.setComponentEnabledSetting(compName, enabled, PackageManager.DONT_KILL_APP);

    }

}
