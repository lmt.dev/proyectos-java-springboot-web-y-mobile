package com.wings.phone.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.wings.phone.AddCallActivity;
import com.wings.phone.R;
import com.wings.phone.TransferCallActivity;
import com.wings.phone.gsm.MakeGsmCallHelper;
import com.wings.phone.util.InternetCheck;
import com.wings.phone.util.NetworkUtils;
import com.wings.phone.util.StringUtils;
import com.wings.phone.util.WingsConst;

import timber.log.Timber;

public class DetailClickListener implements View.OnClickListener {

    private String number;
    private Context context;
    private Integer transferFromId;

    public DetailClickListener(String number, Context context) {
        this.number = number;
        this.context = context;
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.detail_call_gsm:
                callGsm();
                break;
            case R.id.detail_call_free:
                callFree();
                break;
            case R.id.detail_call_wings:
                callWings();
                break;
            case R.id.detail_call_secure:
                callSecure();
                break;
                /*case R.id.detail_call_video:
                    callVideo();
                    break;
                case R.id.detail_call_sms:
                    callSms();
                    break;*/
        }

    }

    private void callGsm() {

        MakeGsmCallHelper.makeIntent(context, number);

        if (context instanceof AddCallActivity) {
            ((AddCallActivity) context).finish();
        }

    }

    private void callFree() {

        if (NetworkUtils.isOnline(context)) {
            NetworkUtils.hasInternet(new InternetCheck.Consumer() {
                @Override
                public void accept(Boolean internet) {
                    if (internet) {

                        Uri uri = Uri.parse("wings:" + StringUtils.cleanUpNumber(number));
                        Intent intent = new Intent(WingsConst.INTENT_WINGS_FREE_CALL, uri);
                        if (transferFromId != null && transferFromId > 0) {
                            intent.putExtra("transfer_from_id", transferFromId);
                        }
                        context.startActivity(intent);
                        if (context instanceof AddCallActivity) {
                            ((AddCallActivity) context).finish();
                        }

                        /*if (context instanceof TransferCallActivity) {
                            ((TransferCallActivity) context).finish();
                        }*/

                    } else {
                        NetworkUtils.showNoInternetDialog(context);
                    }
                }
            });
        } else {
            NetworkUtils.showNoInternetDialog(context);
        }
    }

    private void callWings() {

        if (NetworkUtils.isOnline(context)) {
            NetworkUtils.hasInternet(new InternetCheck.Consumer() {
                @Override
                public void accept(Boolean internet) {
                    if (internet) {
                        Uri uri = Uri.parse("wings:" + StringUtils.cleanUpNumber(number));
                        Intent intent = new Intent(WingsConst.INTENT_WINGS_WINGS_CALL, uri);

                        if (transferFromId != null && transferFromId > 0) {
                            intent.putExtra("transfer_from_id", transferFromId);
                        }

                        context.startActivity(intent);

                        if (context instanceof AddCallActivity) {
                            ((AddCallActivity) context).finish();
                        }

                        /*if (context instanceof TransferCallActivity) {
                            ((TransferCallActivity) context).finish();
                        }*/

                    } else {
                        NetworkUtils.showNoInternetDialog(context);
                    }
                }
            });
        } else {
            NetworkUtils.showNoInternetDialog(context);
        }
    }

    private void callSecure() {

        if (NetworkUtils.isOnline(context)) {
            NetworkUtils.hasInternet(new InternetCheck.Consumer() {
                @Override
                public void accept(Boolean internet) {
                    if (internet) {
                        Uri uri = Uri.parse("wings:" + StringUtils.cleanUpNumber(number));
                        Intent intent = new Intent(WingsConst.INTENT_WINGS_SECURE_CALL, uri);

                        if (transferFromId != null && transferFromId > 0) {
                            intent.putExtra("transfer_from_id", transferFromId);
                        }

                        context.startActivity(intent);

                        if (context instanceof AddCallActivity) {
                            ((AddCallActivity) context).finish();
                        }
                    } else {
                        NetworkUtils.showNoInternetDialog(context);
                    }
                }
            });
        } else {
            NetworkUtils.showNoInternetDialog(context);
        }

    }

    private void callVideo() {

        Uri uri = Uri.parse("wings:" + StringUtils.cleanUpNumber(number));
        Intent intent = new Intent(WingsConst.INTENT_WINGS_VIDEO_CALL, uri);
        context.startActivity(intent);

    }

    private void callSms() {
        Toast.makeText(context, "Call SMS " + number, Toast.LENGTH_SHORT).show();
    }

    public void setTransferFromId(Integer transferFromId) {
        this.transferFromId = transferFromId;
    }
}
