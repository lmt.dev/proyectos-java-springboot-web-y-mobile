package com.wings.phone.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.R;
import com.wings.phone.gsm.MakeGsmCallHelper;
import com.wings.phone.model.CallLog;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.phone.util.NumberUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.WingsConst;

import java.util.List;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

public class HistoryAdapter extends BaseAdapter {

    private Context context;
    private List<CallLog> calls;

    private int lastColor = 0;
    private String lastName = "";

    public HistoryAdapter(Context context, List<CallLog> calls){
        this.context = context;
        this.calls = calls;

    }

    @Override
    public int getCount() {
        return calls.size();
    }

    @Override
    public CallLog getItem(int position) { return calls.get(position); }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = LayoutInflater.from(context).inflate(R.layout.view_history_item, null);
        CallLog current = (CallLog)getItem(i);
        ((TextView) view.findViewById(R.id.when)).setText(when(current.getDate()));
        ((TextView) view.findViewById(R.id.via)).setText(Settings.getAccountMap().get(current.getAccountId()));
        ((TextView) view.findViewById(R.id.history_name)).setText(current.getName());

        view.findViewById(R.id.incoming_type).setVisibility(View.GONE);
        view.findViewById(R.id.outgoing_type).setVisibility(View.GONE);
        view.findViewById(R.id.rejected_type).setVisibility(View.GONE);


        switch (current.getType()) {
            case android.provider.CallLog.Calls.INCOMING_TYPE:
                view.findViewById(R.id.incoming_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.OUTGOING_TYPE:
                view.findViewById(R.id.outgoing_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.VOICEMAIL_TYPE:
                break;
            case android.provider.CallLog.Calls.REJECTED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.BLOCKED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
            case android.provider.CallLog.Calls.ANSWERED_EXTERNALLY_TYPE:
                break;
            case android.provider.CallLog.Calls.MISSED_TYPE:
                view.findViewById(R.id.rejected_type).setVisibility(View.VISIBLE);
                break;
        }

        ImageView avatar = (ImageView) view.findViewById(R.id.history_avatar);
        if(current.getPhotoUri() != null){
            
            Timber.d("IMPORTANTE!!!!" + current.getPhotoUri());
            
            try {
                Picasso.get().load(current.getPhotoUri()).transform(new CircleTransform()).into(avatar);
            }catch (Exception e){
            }
        }else{
            int colorInt;
            if(!current.getName().equals(lastName)){
                colorInt = current.getId();
                lastName = current.getName();
                lastColor = colorInt;
            }else{
                colorInt = lastColor;
            }
            int color = ColorGenerator.MATERIAL.getColor(colorInt);
            TextDrawable drawable = TextDrawable.builder().buildRound(current.getName().substring(0,1), color);
            avatar.setImageDrawable(drawable);

        }
        ImageView callIcon = (ImageView) view.findViewById(R.id.call_icon);
        callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current.getAccountId() != null) {
                    MakeGsmCallHelper.makeIntentWithProvider(context, current.getNumber(), current.getAccountId());
                } else {
                    MakeGsmCallHelper.makeIntent(context, current.getNumber());
                }
            }

        });

        if (current.getAccountId() != null && current.getAccountId().startsWith("ACCOUNT")) {
            switch (current.getAccountId()) {
                case WingsConst.ACCOUNT_WINGS:
                    callIcon.setImageResource(R.drawable.ic_call_wings2);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorBlue));
                    break;
                case WingsConst.ACCOUNT_SECURE:
                    callIcon.setImageResource(R.drawable.baseline_lock_24);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorBlue));
                    break;
                default:
                    callIcon.setImageResource(R.drawable.ic_call_free2);
                    callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPink));
                    break;
            }
        } else {
            callIcon.setImageResource(R.drawable.ic_call_gsm2);
            callIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorGreen));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //3 opciones ,
                //1 nombre, mensaje
                //2 solo numero , muestra all
                //3 desconocido, no muestra nada
                View groupAdd = v.findViewById(R.id.groupAddContact);
                View groupAppend = v.findViewById(R.id.groupAppendContact);
                View groupSendSMS = v.findViewById(R.id.groupSendSMS);
                View groupCopy = v.findViewById(R.id.groupCopy);

                if (!current.getStatus().isVisible()) {

                    current.getStatus().setVisible(true);
                    switch (current.getStatus().getStatus()) {
                        case 1:
                            groupSendSMS.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            groupSendSMS.setVisibility(View.VISIBLE);
                            groupAdd.setVisibility(View.VISIBLE);
                            groupAppend.setVisibility(View.VISIBLE);
                            groupCopy.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            break;
                    }

                } else {
                    hideAll(v);
                    current.getStatus().setVisible(false);
                }

                //expand actions listeners
                View containerAdd = v.findViewById(R.id.containerAdd);
                View containerAppend = v.findViewById(R.id.containerAppend);
                View containerSendSMS = v.findViewById(R.id.containerSendSMS);
                View containerCopy = v.findViewById(R.id.containerCopy);

                containerAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (current.getNumber() != null) {
                            String formatted = NumberUtils.getFormattedNumber(context, current.getNumber());
                            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                            intent.putExtra(ContactsContract.Intents.Insert.PHONE, formatted);
                            context.startActivity(intent);
                        }
                    }
                });

                containerAppend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (current.getNumber() != null) {
                            String formatted = NumberUtils.getFormattedNumber(context, current.getNumber());
                            Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                            intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                            intent.putExtra(ContactsContract.Intents.Insert.PHONE, formatted);
                            context.startActivity(intent);
                        }
                    }
                });
                containerCopy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (current.getNumber() != null) {
                            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("Numero", current.getNumber());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(context, R.string.clipboard_copied, Toast.LENGTH_LONG).show();
                        }
                    }
                });

                containerSendSMS.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", current.getNumber(), null)));
                    }
                });
            }
        });

        return view;
    }

    private String when(long date) {
        String ret;
        long diff = System.currentTimeMillis() - date;
        if (diff < 60000) {
            long seconds = TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);
            if (seconds <= 1) {
                ret = context.getString(R.string.second_ago, 1);
            } else {
                ret = context.getString(R.string.seconds_ago, seconds);
            }
        } else if (diff < 3600000) {
            long minutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
            if (minutes <= 1) {
                ret = context.getString(R.string.minute_ago, 1);
            } else {
                ret = context.getString(R.string.minutes_ago, minutes);
            }
        } else if (diff < 86400000) {
            long hours = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
            if (hours <= 1) {
                ret = context.getString(R.string.hour_ago, 1);
            } else {
                ret = context.getString(R.string.hours_ago, hours);
            }
        } else if (diff < 2592000000L) {
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (days <= 1) {
                ret = context.getString(R.string.day_ago, 1);
            } else {
                ret = context.getString(R.string.days_ago, days);
            }
        } else {
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            int months = (int) (days / 30);
            if (months <= 1) {
                ret = context.getString(R.string.month_ago);
            } else {
                ret = context.getString(R.string.months_ago, months);
            }
        }
        return ret;
    }

    private void hideAll(View v) {
        View groupAdd = v.findViewById(R.id.groupAddContact);
        View groupAppend = v.findViewById(R.id.groupAppendContact);
        View groupSendSMS = v.findViewById(R.id.groupSendSMS);
        View groupCopy = v.findViewById(R.id.groupCopy);

        groupAdd.setVisibility(View.GONE);
        groupAppend.setVisibility(View.GONE);
        groupSendSMS.setVisibility(View.GONE);
        groupCopy.setVisibility(View.GONE);
    }

    public void setCallLogList(List<CallLog> callLogs) {
        this.calls = callLogs;
    }
}
