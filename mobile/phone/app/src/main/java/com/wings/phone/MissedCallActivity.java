package com.wings.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.data.CallLogManager;
import com.wings.phone.data.sync.ContactsManager;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.telecom.CallWrapper;
import com.wings.phone.telecom.CallWrapperListener;
import com.wings.phone.util.ActivUtils;
import com.wings.phone.util.Settings;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.requests.RegisterRequest;
import com.wings.voip.model.requests.RegisterResponse;
import com.wings.voip.model.requests.RequestMissedCallResponse;

import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MissedCallActivity extends AppCompatActivity {

    private class CallWrapperListenerImpl implements CallWrapperListener {
        @Override
        public void onCallRemoved(final CallWrapper call) {
            validateHangUpCall();
        }

        @Override
        public void onCallStatusChange(final CallWrapper call, String callStatus, String lastReason) {
        }

        @Override
        public void onStatsUpdate(final String stats) {
        }
    }

    private String myNumber;
    private String offSet;
    private String callingNumber;
    private String verifiedCallingNumber;
    private String callId;
    private String smsCode;

    private String opMnc;
    private String opMcc;

    private String apiUrl;
    private String appId;
    private String token;

    private CallManager callManager;
    private AudioManager mode;
    private TextView logView;
    private TextView timerView;
    private ProgressBar progressBar;

    private TextView textSmsError;
    private Button btnCancel;
    private Button btnValidate;
    private EditText edtCode;

    private BroadcastReceiver smsReceiver;
    private CountDownTimer missedCallTimer;
    private CountDownTimer smsRequestTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_validation);

        logView = findViewById(R.id.missed_call_log);
        timerView = findViewById(R.id.missed_call_timer);
        btnCancel = findViewById(R.id.btnCancel);
        btnValidate = findViewById(R.id.btnValidate);
        edtCode = findViewById(R.id.edtCode);
        textSmsError = findViewById(R.id.textSmsError);
        progressBar = findViewById(R.id.progressBar);
        try {
            myNumber = getIntent().getExtras().getString("myNumber");
        } catch (Exception ex) {
        }

        if (myNumber == null) {
            finish();
        }

        apiUrl = getString(R.string.settings_api_url);
        appId = getString(R.string.settings_api_app_id);
        token = getString(R.string.settings_api_app_token);

        //clean up settings
        Settings.logout(MissedCallActivity.this);
        Settings.init(MissedCallActivity.this);

        requestMissedCall();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanUpMissedCall();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void requestMissedCall() {

        //disable CallActivity, only this activity should receive gsm calls
        setGsmEnabled(false);

        //timer if call never arrives
        missedCallTimer = new CountDownTimer(40000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                appendLogTimer(" " + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                missedCallResponseError();
            }
        };
        missedCallTimer.start();

        //mute
        try {
            mode = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            if (mode.getRingerMode() != AudioManager.RINGER_MODE_VIBRATE) {
                mode.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
            }
        } catch (Exception ex) {
        }

        //setup callManager
        callManager = CallManager.getInstance(getApplicationContext());
        callManager.setListener(new CallWrapperListenerImpl());

        appendLog(R.string.missed_call_requesting);
        //Request missed call
        Call<RequestMissedCallResponse> call = ApiClientFactory.getClient(apiUrl).requestMissedCall(appId, token, myNumber);
        try {
            call.enqueue(new Callback<RequestMissedCallResponse>() {
                @Override
                public void onResponse(Call<RequestMissedCallResponse> call, Response<RequestMissedCallResponse> response) {

                    appendLog(R.string.missed_call_requested);
                    if (response.body() != null && response.body().getStatus() != null && response.body().getStatus().equals("success")) {
                        offSet = response.body().getOffSet();
                        callId = response.body().getCallId();
                        missedCallResponseSuccess();
                    } else {
                        Timber.e("Error on response missed call");
                        missedCallResponseError();
                    }
                }

                @Override
                public void onFailure(Call<RequestMissedCallResponse> call, Throwable t) {
                    Timber.e(t, "Error on response missed call");
                    missedCallResponseError();
                }
            });
        } catch (Exception ex) {
            Timber.e(ex, "Error on response missed call");
            missedCallResponseError();
        }
    }


    private void missedCallResponseSuccess() {

        if (missedCallTimer != null) {
            missedCallTimer.cancel();
            missedCallTimer = null;
        }

        appendLog(R.string.missed_call_waiting);
        //add this point we wait for incoming call
        new Thread(new Runnable() {

            int maxIterations = 500;
            int iterations = 0;

            @Override
            public void run() {
                while (callManager.getActiveCall() == null) {
                    try {
                        Thread.sleep(100);

                        iterations++;
                        if (iterations >= maxIterations) {
                            break;
                        }

                        if (iterations % 10 == 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    appendLogTimer(" " + ((maxIterations - iterations) / 10));
                                }
                            });
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        delayed();
                    }
                });
            }
        }).start();

    }

    private void delayed() {

        appendLog(R.string.missed_call_ready);
        CallWrapper call = callManager.getActiveCall();

        if (call == null) {
            requestSmsValidation();
            return;
        }

        if (call.getCallBuddy() == null) {
            requestSmsValidation();
            return;
        }

        String number = call.getCallBuddy().getNumber();
        if (number != null) {
            callingNumber = number;
        }

        if (callingNumber != null && callingNumber.contains(offSet)) {
            verifiedCallingNumber = number;
            offSet = null;
        }

        call.hangUp();
        //after this goes to listener
    }


    private void validateHangUpCall() {

        //appendLog("Call hang up...");
        //get operator values
        Map<String, String> vals = CallLogManager.getCallLogFromRegister(MissedCallActivity.this, callingNumber);
        opMcc = vals.get("mcc");
        opMnc = vals.get("mnc");

        //clean up
        cleanUpMissedCall();

        //appendLog("Waiting for internet...");
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
        }

        if (verifiedCallingNumber != null) {
            appendLog(R.string.missed_call_verify);
            verify(verifiedCallingNumber);
        } else {
            appendLog(R.string.missed_call_failed);
            requestSmsValidation();
        }
    }

    private void missedCallResponseError() {
        //start sms validation
        cleanUpMissedCall();
        requestSmsValidation();
    }

    private void verify(String callingNumber) {

        String mnc = callingNumber.substring(callingNumber.length() - 4);
        Call<RequestMissedCallResponse> call = ApiClientFactory.getClient(apiUrl).verifyNumber(appId, token, myNumber, callId, mnc);
        try {
            call.enqueue(new Callback<RequestMissedCallResponse>() {
                @Override
                public void onResponse(Call<RequestMissedCallResponse> call, Response<RequestMissedCallResponse> response) {
                    if (response.body() != null && response.body().getStatus().equals("success")) {
                        register(myNumber);
                    } else {
                        missedCallResponseError();
                    }
                }

                @Override
                public void onFailure(Call<RequestMissedCallResponse> call, Throwable t) {
                    Timber.e(t, "Error on verify call");
                    missedCallResponseError();
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error registering client");
            missedCallResponseError();
        }
    }

    private void requestSmsValidation() {

        if (missedCallTimer != null) {
            missedCallTimer.cancel();
            missedCallTimer = null;
        }

        appendLog(R.string.missed_call_sms_request);
        //register sms receiver
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                appendLog(R.string.missed_call_sms_received);
                try {
                    Bundle data = intent.getExtras();
                    Object[] pdus = (Object[]) data.get("pdus");
                    for (int i = 0; i < pdus.length; i++) {

                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i], SmsMessage.FORMAT_3GPP);
                        String body = smsMessage.getDisplayMessageBody();
                        //Check the sender to filter messages which we require to read

                        if ((smsCode != null && body.contains(smsCode)) || (callingNumber != null && body.contains(callingNumber))) {
                            appendLog(R.string.missed_call_sms_verified);
                            register(myNumber);
                        } else {
                            appendLog(R.string.missed_call_sms_error);
                        }

                    }
                } catch (Exception e) {
                    Timber.e(e);
                    returnFailure();
                }
            }
        };
        registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

        smsRequestTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                appendLogTimer(" " + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                showButtonsAndEnterCode();
            }
        };
        smsRequestTimer.start();

        //appendLog("Waiting for internet...");
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
        }

        String lang = Locale.getDefault().getLanguage().toLowerCase();
        Call<String> call = ApiClientFactory.getClient(apiUrl).requestSmsValidation(appId, token, myNumber, lang);
        try {
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    if (response.body() != null) {
                        appendLog(R.string.missed_call_sms_request_response);
                        smsCode = response.body();
                    } else {
                        returnFailure();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    //error.setText(getString(R.string.account_register_error));
                    Timber.e(t, "Error on verify call ");
                    returnFailure();
                }
            });
        } catch (Exception e) {
            Timber.e(e, "Error registering client");
            returnFailure();
        }
    }


    private void register(final String number) {

        String apiUrl = getString(R.string.settings_api_url);
        String appId = getString(R.string.settings_api_app_id);
        String appToken = getString(R.string.settings_api_app_token);

        Settings.init(MissedCallActivity.this);

        //data to send
        String login = number;
        String token = Settings.getToken();
        String pushToken = Settings.getPushToken(MissedCallActivity.this);
        String instanceId = Settings.getInstanceId();
        String deviceName = Settings.getDeviceName();
        String deviceModel = Settings.getDeviceModel();
        String serialNumber = Settings.getSerialNumber();
        String deviceId = Settings.getDeviceId();
        String udid = Settings.getUdid();
        String imei1 = Settings.getImei1();
        String imei2 = Settings.getImei2();
        String countryPrefix = getCountryDialCode();
        String countryIso = Settings.getCountry();
        String versionCode = String.valueOf(Settings.getVersionCode());
        String versionName = Settings.getVersionName();
        String locale = Locale.getDefault().toString();

        final RegisterRequest req = new RegisterRequest(login, token, pushToken, instanceId, deviceName, deviceModel, serialNumber, deviceId, udid, imei1, imei2, countryPrefix, countryIso, versionCode, versionName);
        req.setLocale(locale);
        req.setMcc(opMcc);
        req.setMnc(opMnc);

        Call<RegisterResponse> call = ApiClientFactory.getClient(apiUrl).register(appId, appToken, req);
        try {
            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {

                    if (response.body() != null) {

                        //at this point number must be validated
                        Settings.setRegistered(number);
                        AccountStatus status = response.body().getAccountStatus();
                        Settings.updateStatus(status, MissedCallActivity.this);
                        Settings.setProvisioning(response.body().getProvisioning(), MissedCallActivity.this);
                        ContactsManager.createWingsAccount(MissedCallActivity.this);

                        returnSuccess();

                    } else {
                        returnFailure();
                    }

                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    returnFailure();
                }
            });
        } catch (Exception e) {
            returnFailure();
        }
    }


    private void showButtonsAndEnterCode() {

        progressBar.setVisibility(View.GONE);
        timerView.setVisibility(View.GONE);
        logView.setVisibility(View.GONE);

        btnCancel.setVisibility(View.VISIBLE);
        btnValidate.setVisibility(View.VISIBLE);
        edtCode.setVisibility(View.VISIBLE);
        textSmsError.setVisibility(View.VISIBLE);


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnFailure();
            }
        });

        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enterCode = edtCode.getText().toString();
                if (enterCode != null && enterCode.equals(smsCode)) {
                    textSmsError.setText("");
                    btnValidate.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                    appendLog(R.string.missed_call_sms_verified);
                    register(myNumber);
                } else {
                    textSmsError.setText(R.string.sms_wrong_code);
                }
            }
        });
    }

    private void returnSuccess() {
        if (smsReceiver != null) {
            try {
                unregisterReceiver(smsReceiver);
            } catch (Exception e) {
            }
        }

        if (smsRequestTimer != null) {
            smsRequestTimer.cancel();
            smsRequestTimer = null;
        }

        cleanUpMissedCall();
        Intent data = new Intent();
        data.setData(Uri.parse(myNumber));
        setResult(RESULT_OK, data);
        finish();
    }

    private void returnFailure() {

        if (smsReceiver != null) {
            try {
                unregisterReceiver(smsReceiver);
            } catch (Exception e) {
            }
        }

        if (smsRequestTimer != null) {
            try {
                smsRequestTimer.cancel();
                smsRequestTimer = null;
            } catch (Exception ex) {
            }
        }

        cleanUpMissedCall();
        Intent data = new Intent();
        setResult(RESULT_FIRST_USER, data);
        finish();
    }

    /*****************************************************/
    private void cleanUpMissedCall() {

        //disable CallActivity, only this activity should receive gsm calls
        setGsmEnabled(true);

        //delete missed call record
        try {
            CallLogManager.deleteCallLog(MissedCallActivity.this, callingNumber);
        } catch (Exception e) {
        }

        if (mode != null) {
            try {
                //unmute
                mode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            } catch (Exception ex) {
            }
        }
    }

    private void setGsmEnabled(boolean flag) {
        if (flag) {
            ActivUtils.enableGsmCallActivity(MissedCallActivity.this);
        } else {
            ActivUtils.disableGsmCallActivity(MissedCallActivity.this);
        }
    }

    private String getCountryDialCode() {

        String contryDialCode = "+34";
        TelephonyManager telephonyMngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryId = telephonyMngr.getSimCountryIso().toUpperCase();

        String[] arrCountryCode = this.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrCountryCode.length; i++) {
            String[] arrDial = arrCountryCode[i].split(",");
            if (arrDial[1].trim().equals(countryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }


    private void appendLogTimer(final String log) {
        timerView.setText(log);
    }

    private void appendLog(int resourceId) {
        try {
            String log = getString(resourceId);
            appendLogString(log);
        } catch (Exception e) {
            appendLogString("");
        }
    }

    private void appendLogString(final String log) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logView.setText(log);
            }
        });
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String text = log + System.lineSeparator();
                text += logView.getText();
                logView.setText(text);
            }
        });*/

    }

}
