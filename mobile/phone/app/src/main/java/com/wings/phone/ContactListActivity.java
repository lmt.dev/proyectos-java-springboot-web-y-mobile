package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;

import java.util.ArrayList;

import static com.wings.phone.data.sync.ContactsManager.getContactLookpu;
import static com.wings.phone.util.ContactUtils.getContactsArray;

public class ContactListActivity extends AppCompatActivity {

    private ListView list;
    private ArrayList<Contact> contactsArrALL;
    private ContactAdapter contactAdapter;

    public String formated;
    public String number;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_search, menu);

        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) search.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }


    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_contacts);
        formated = getIntent().getStringExtra("formated");
        number = getIntent().getStringExtra("number");
        if(formated==null){
            formated=number;
        }

        list = (ListView) findViewById(R.id.contacts);
        list.setFastScrollEnabled(true);

        if(contactsArrALL==null) {
            String[] projection = new String[]{
                    ContactsContract.PhoneLookup.CONTACT_ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME,
                    ContactsContract.PhoneLookup.PHOTO_URI,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER,
                    ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID,
                    ContactsContract.Data.DATA4,
            };
            String selection = "has_phone_number = ?";
            String[] args = new String[]{"1"};
            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            contactsArrALL = getContactsArray(this, projection, null, null, uri);
        }

        contactAdapter =  new ContactAdapter(this, contactsArrALL);
        list.setAdapter(contactAdapter);


    }



    public class ContactAdapter extends ArrayAdapter<Contact> implements Filterable {

        ArrayList<Contact> contacts;
        ArrayList<Contact> contactsAll;
        private  class ViewHolder {
            ImageView avatar;
            TextView nameView;
        }

        public ContactAdapter(Context context, ArrayList<Contact> contacts) {
            super(context, R.layout.view_contact_item, contacts);
            this.contacts = contacts;
            this.contactsAll = contacts;
        }
        @Override

        public View getView(int position, View convertView, ViewGroup parent) {
            Contact current = getItem(position);
            ViewHolder viewHolder; // view lookup cache stored in tag
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.view_contact_item, parent, false);
                viewHolder.avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
                viewHolder.nameView = (TextView) convertView.findViewById(R.id.contact_name);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //((LinearLayout)convertView.findViewById(R.id.contact_detail_container)).setVisibility(View.GONE);
            ((LinearLayout)convertView.findViewById(R.id.contact_detail_container)).setVisibility(View.INVISIBLE);

            String pic = current.getPhotoUri();
            if (pic == null) {
                TextDrawable drawable = TextDrawable.builder().buildRound(current.getName().substring(0, 1), ColorGenerator.MATERIAL.getColor(current.getRawId()));
                viewHolder.avatar.setImageDrawable(drawable);
            } else {
                //Picasso.with(context).load(pic).resize(50,50).transform(new CircleTransform()).into(avatar);
                Picasso.get().load(pic).resize(50, 50).transform(new CircleTransform()).into(viewHolder.avatar);
            }

            viewHolder.nameView.setText(current.getName());


            convertView.setOnClickListener(v -> {
                String lk = getContactLookpu(getContext(), String.valueOf(current.getId()));
                Uri selectedContactUri = ContactsContract.Contacts.getLookupUri(current.getId(), lk);

                Intent editIntent = new Intent(Intent.ACTION_EDIT);
                editIntent.setDataAndType(selectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                editIntent.putExtra(ContactsContract.Intents.Insert.PHONE, formated);
                editIntent.putExtra("finishActivityOnSaveCompleted", true);
                getContext().startActivity(editIntent);

            });



            return convertView;

        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @SuppressWarnings("unchecked")
                @Override
                //protected FilterResults performFiltering  {
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                            contacts = contactsAll;
                        }
                    ArrayList<Contact> filteredList = new ArrayList<>();
                    for (Contact row : contactsAll)
                    {

                        if(row.getName().toLowerCase().contains(charSequence.toString().toLowerCase())){
                            filteredList.add(row);
                            continue;
                        }
                        for(Number n : row.getNumbers()){
                            try{
                                if(n.getNumber().contains(charSequence)){
                                    filteredList.add(row);
                                    break;
                                }else if(n.getNumber().contains(charSequence)){
                                    filteredList.add(row);
                                    break;
                                }
                            }catch (Exception ex){}
                        }
                    }

                    contacts = filteredList;

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filteredList;

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contacts = (ArrayList<Contact>) filterResults.values;

                    // refresh the list with filtered data
                    notifyDataSetChanged();
                }
            };
        }

        @Override
        public int getCount() {
            try{
                return contacts.size(); //returns total of items in the list
            }catch (Exception ex){
                return 0;
            }
        }

        @Override
        public Contact getItem(int position) {
            return contacts.get(position); //returns list item at the specified position
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

    }
}
