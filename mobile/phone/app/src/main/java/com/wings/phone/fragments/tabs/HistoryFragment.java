package com.wings.phone.fragments.tabs;


import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.wings.phone.R;
import com.wings.phone.adapters.HistoryAdapter;
import com.wings.phone.model.CallLog;
import com.wings.phone.util.Settings;
import com.wings.phone.viewmodels.HistoryViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by seba on 26/04/17.
 */

public class HistoryFragment extends Fragment {

    public static final String TAG = "HistoryFragment";

    private ListView lview;
    private LinearLayout layout;
    private HistoryViewModel historyViewModel;
    private HistoryAdapter historyAdapter;
    private List<CallLog> callLogs;

    public static Fragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }
    public HistoryFragment(){

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(callLogs==null){
            callLogs=new LinkedList<>();
        }
        historyAdapter = new HistoryAdapter(getContext(),callLogs);
        historyViewModel = new ViewModelProvider(this, new ViewModelFactory(requireActivity())).get(HistoryViewModel.class);
    }



    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //CallLogManager.markMissedCallsAsRead(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        layout = (LinearLayout) inflater.inflate(R.layout.fragment_tab_history, null);
        lview = layout.findViewById(R.id.list_history_tab);

        historyViewModel.getCallLogtListMutableLiveData().observe(getViewLifecycleOwner(), new Observer<List<CallLog>>() {
            @Override
            public void onChanged(List<CallLog> callLogs) {
                historyAdapter.setCallLogList(callLogs);
                historyAdapter.notifyDataSetChanged();

                if (callLogs.size() == 0) {
                    layout.findViewById(R.id.no_calllog).setVisibility(View.VISIBLE);
                    lview.setVisibility(View.GONE);
                }else{
                    layout.findViewById(R.id.no_calllog).setVisibility(View.GONE);
                    lview.setVisibility(View.VISIBLE);
                }


            }

        });

        lview.setAdapter(historyAdapter);


        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        //String mustUpdate = Settings.getPreference("UI."+TAG+".update","false",getContext());
        //if(Boolean.valueOf(mustUpdate)){
        refreshUI();
        //    Settings.setPreference("UI."+TAG+".update","false",getContext());
        //}
    }



    public void refreshUI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                historyViewModel.refresh();
            }
        }).run();
    }

    static class ViewModelFactory implements ViewModelProvider.Factory {
        private Application application;
        ViewModelFactory(Context context) {
            application = (Application) context.getApplicationContext();
        }
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull Class<T> modelClass) {
            return (T) new HistoryViewModel(application);
        }
    }


}
