package com.wings.phone.adapters;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayout;
import com.wings.phone.fragments.tabs.ContactsFragment;
import com.wings.phone.fragments.tabs.FavoritesFragment;
import com.wings.phone.fragments.tabs.HistoryFragment;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStateAdapter {

    //private ArrayList<Fragment> arrayList = new ArrayList<>();
    private int currentTab = 0;
    private TabLayout tabLayout;
    private Fragment favFragment;
    private Fragment hisFragment;
    private Fragment conFragment;


    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                if(favFragment==null) {
                    favFragment = FavoritesFragment.newInstance();
                }
                return favFragment;
            case 1:
                if(hisFragment==null) {
                    hisFragment = HistoryFragment.newInstance();
                }
                return hisFragment;
            case 2:
                if(conFragment==null) {
                    conFragment = ContactsFragment.newInstance();
                }
                return conFragment;

        }
        return null;
    }

    private Fragment fav() {
        if(favFragment==null){
            favFragment = FavoritesFragment.newInstance();
        }
        return favFragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }




    //public void addFragment(Fragment fragment) {
    //    arrayList.add(fragment);
    //}

    /*@Override
    public int getItemCount() {
        return arrayList.size();
    }*/



/*    public void setTabLayout(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
    }*/
}