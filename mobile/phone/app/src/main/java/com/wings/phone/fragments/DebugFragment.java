package com.wings.phone.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.wings.phone.R;

public class DebugFragment extends Fragment {

    public final static String TAG = "DebugFragment";
    private TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, null);
        textView = view.findViewById(R.id.call_debug_text);
        return view;
    }

    public TextView getTextView() {
        return textView;
    }
}
