package com.wings.phone.adapters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.AddCallActivity;
import com.wings.phone.CallActivity;
import com.wings.phone.ContactListActivity;
import com.wings.phone.R;
import com.wings.phone.data.sync.ContactsManager;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.phone.util.NumberUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import timber.log.Timber;

import static com.wings.phone.data.sync.ContactsManager.getContactLookpu;
import static com.wings.phone.util.ContactUtils.getContactsArray;


/**
 */

public class ContactsAdapter extends BaseAdapter implements StickyListHeadersAdapter, Filterable {
    private boolean clearOnEmptyFilter = true;
    private LayoutInflater inflater;
    private ArrayList<Contact> contactsArrALL;
    private ArrayList<Contact> contactsArr;
    private final String abc = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ-1234567890";
    private Context context;
    private Integer transferFromId;
    private String filterQuery;
    private Map<String, View> contactsMap;
    private ColorGenerator generator;
    private boolean showExtraButtons = false;


    public ContactsAdapter(Context context, int layout, boolean showContactsAtStart, String qry){
        this.contactsArr = new ArrayList<Contact>();
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.filterQuery = qry;
        this.contactsMap = new HashMap<String, View>();
        this.generator = ColorGenerator.MATERIAL;
        if(showContactsAtStart){
            //setContactsArr();
            clearOnEmptyFilter = false;

        }else{
            setContactsArrWhitNumbers();
        }

    }

    public void setShowExtraButtons(boolean set){
        this.showExtraButtons = set;
    }



    public void setContactsArr(List<Contact> contacts){
/*        if(contactsArrALL==null){
            final String[] GROUP_PROJECTION = new String[]{
                    ContactsContract.PhoneLookup._ID,
                    ContactsContract.PhoneLookup.DISPLAY_NAME,
                    ContactsContract.PhoneLookup.PHOTO_URI,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER,
                    ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID
            };
            final String SELECTION ="(" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " != ? )";
            contactsArrALL = getContactsArray(context, GROUP_PROJECTION,SELECTION,new String[]{"0"},ContactsContract.Contacts.CONTENT_URI);
            contactsArr = contactsArrALL;

        }*/
        this.contactsArr = (ArrayList<Contact>) contacts;
        if(contactsArrALL==null){contactsArrALL=contactsArr;}
    }

    public void setContactsArrWhitNumbers(){

            if(contactsArrALL==null){
                String[] projection = new String[]{
                        ContactsContract.PhoneLookup.CONTACT_ID,
                        ContactsContract.PhoneLookup.DISPLAY_NAME,
                        ContactsContract.PhoneLookup.PHOTO_URI,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER,
                        ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID,
                        ContactsContract.Data.DATA4,
                };
                String selection = "has_phone_number = ?";
                String[] args = new String[]{"1"};
                Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

                contactsArrALL = getContactsArray(context,projection,selection,args,uri);

            }
        }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.view_contact_list_header, null);
        }

        String headerString = "...";
        try {
            headerString = abc.charAt((int) getHeaderId(position)) + "";
        } catch (Exception e) {
            Timber.d("Error creating header view");
        }
        ((TextView) convertView).setText(headerString);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        Contact current = (Contact)getItem(position);

        String ch = "-";
        try {
            String tmp = current.getName();
            ch = tmp.substring(0, 1).toUpperCase();
        } catch (Exception e) {
            Timber.e(e);
        }
        return abc.indexOf(ch);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            //protected FilterResults performFiltering  {
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    if(clearOnEmptyFilter){
                        contactsArr.clear();
                    }else {
                        contactsArr = contactsArrALL;
                    }

                } else {
                    ArrayList<Contact> filteredList = new ArrayList<>();
                    for (Contact row : contactsArrALL)
                    {

                        if(row.getName().toLowerCase().contains(charSequence.toString().toLowerCase())){
                            filteredList.add(row);
                            continue;
                    }
                        for(Number n : row.getNumbers()){
                            try{
                                if(n.getNumber().contains(charSequence)){
                                    filteredList.add(row);
                                    break;
                                }else if(n.getNumber().contains(charSequence)){
                                    filteredList.add(row);
                                    break;
                                }
                            }catch (Exception ex){}
                        }
                    }
                    Number n = new Number();
                    n.setNumber(charString);
                    n.setRawNumber(charString);
                    List<Number> numbers = new LinkedList<>();
                    numbers.add(n);

                    contactsArr = filteredList;
                    if(showExtraButtons){
                        Contact boton_temp = new Contact();
                        boton_temp.setName("Crear nuevo contacto");
                        boton_temp.setNumbers(numbers);
                        filteredList.add(boton_temp);
                        boton_temp = new Contact();
                        boton_temp.setName("Agregar a contacto");
                        boton_temp.setNumbers(numbers);
                        filteredList.add(boton_temp);
                        boton_temp = new Contact();
                        boton_temp.setName("Enviar SMS");
                        boton_temp.setNumbers(numbers);
                        filteredList.add(boton_temp);
                    }

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactsArr;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactsArr = (ArrayList<Contact>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        try{
        return contactsArr.size(); //returns total of items in the list
        }catch (Exception ex){
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return contactsArr.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // inflate the layout for each list row
        if (view == null || true) {
            view = LayoutInflater.from(context).inflate(R.layout.view_contact_item, null);
        }
        final Contact current = (Contact)getItem(i);
        final String contactId = String.valueOf(current.getId());
        final String name = current.getName();
        final String pic;
        if(!contactsMap.containsKey(contactId)){
            contactsMap.put(contactId,view);
        }
        final LinearLayout cont = (LinearLayout) view.findViewById(R.id.contact_detail_container);
        cont.setVisibility(View.GONE);
        TextView nameView = ((TextView) view.findViewById(R.id.contact_name));
        View.OnClickListener expandClickListener;
        View.OnLongClickListener contactEditionLongClickListener;

        switch (name) {
            case "Crear nuevo contacto": {

                nameView.setText(context.getString(R.string.add_new_contact));
                int resId = R.drawable.baseline_person_add_24;
                Resources resources = context.getResources();
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId));
                ImageView avatar = (ImageView) view.findViewById(R.id.contact_avatar);

                avatar.setImageURI(uri);
                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) avatar.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                avatar.setLayoutParams(params);
                avatar.setPadding(20, 20, 0, 0);


                expandClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String number = current.getNumbers().get(0).getRawNumber();
                        String formatted = NumberUtils.getFormattedNumber(context, number);
                        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                        intent.putExtra(ContactsContract.Intents.Insert.PHONE, formatted);
                        intent.putExtra("finishActivityOnSaveCompleted", true);
                        context.startActivity(intent);
                    }
                };

                break;
            }
            case "Agregar a contacto": {
                nameView.setText(context.getString(R.string.add_to_existing_contact));

                int resId = R.drawable.baseline_person_24;
                Resources resources = context.getResources();
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId));
                ImageView avatar = (ImageView) view.findViewById(R.id.contact_avatar);

                avatar.setImageURI(uri);

                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) avatar.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                avatar.setLayoutParams(params);
                avatar.setPadding(20, 20, 0, 0);

                expandClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String number = current.getNumbers().get(0).getRawNumber();
                        String formatted = NumberUtils.getFormattedNumber(context, number);


                        Intent myIntent = new Intent(context, ContactListActivity.class);
                        myIntent.putExtra("number", number);
                        myIntent.putExtra("formatted", formatted);
                        context.startActivity(myIntent);





/*                        List<Account> google = Arrays.asList(AccountManager.get(context).getAccountsByType("com.google"));
                        List<Account> local = Arrays.asList(AccountManager.get(context).getAccountsByType("Local Phone Account"));
                        List<Account> usim = Arrays.asList(AccountManager.get(context).getAccountsByType("USIM Account"));



                        List<Account> accounts = new ArrayList<>();
                        accounts.addAll(google);
                        accounts.addAll(local);
                        accounts.addAll(usim);


                        Intent intent = new Intent(Intent.ACTION_EDIT);
                        intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                        intent.putExtra(ContactsContract.Intents.Insert.PHONE, formatted);
                        //intent.putExtra(ContactsContract.Data.ACCOUNT_TYPE_AND_DATA_SET, "com.google");



                        context.startActivity(intent);*/
                    }
                };

                break;
            }
            case "Enviar SMS": {
                nameView.setText(context.getString(R.string.send_sms));

                int resId = R.drawable.baseline_message_24;
                Resources resources = context.getResources();
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId));
                ImageView avatar = (ImageView) view.findViewById(R.id.contact_avatar);

                avatar.setImageURI(uri);

                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) avatar.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                avatar.setLayoutParams(params);
                avatar.setPadding(20, 20, 0, 0);
                expandClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String number = current.getNumbers().get(0).getRawNumber();
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));

                    }
                };

                break;
            }
            default: {
                //contacto corriente
                nameView.setText(name);
                pic = current.getPhotoUri();
                expandClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //expand
                        if (cont.getVisibility() == View.VISIBLE) {
                            cont.setVisibility(View.GONE);
                            //cont.removeAllViews();
                        } else {
                            loadDetail(cont, current);
                            cont.setVisibility(View.VISIBLE);
                            //final Rect rect = new Rect(0, 0, cont.getWidth(), cont.getHeight());
                            //((View)cont.getParent()).scrollTo(((View)cont.getParent()).getScrollX(),((View)cont.getParent()).getScrollY());
                        }
                    }
                };

                //ImageView callView = ((ImageView) view.findViewById(R.id.contact_call));
                //callView.setOnClickListener(expandClickListener);

                contactEditionLongClickListener = v -> {
                    //

                    String lk = getContactLookpu(context, String.valueOf(current.getId()));
                    Uri selectedContactUri = ContactsContract.Contacts.getLookupUri(current.getId(), lk);

                    // Creates a new Intent to edit a contact
                    Intent editIntent = new Intent(Intent.ACTION_EDIT);
                    /*
                     * Sets the contact URI to edit, and the data type that the
                     * Intent must match
                     */
                    editIntent.setDataAndType(selectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                    //
                    editIntent.putExtra("finishActivityOnSaveCompleted", true);
                    context.startActivity(editIntent);

                    return true;
                };


                nameView.setOnLongClickListener(contactEditionLongClickListener);
                view.setOnLongClickListener(contactEditionLongClickListener);


                ImageView avatar = (ImageView) view.findViewById(R.id.contact_avatar);
                avatar.setOnClickListener(expandClickListener);

                avatar.setOnLongClickListener(contactEditionLongClickListener);

                try {
                    if (pic == null) {

                        TextDrawable drawable = TextDrawable.builder().buildRound(name.substring(0, 1), generator.getColor(current.getRawId()));
                        avatar.setImageDrawable(drawable);
                    } else {
                        //Picasso.with(context).load(pic).resize(50,50).transform(new CircleTransform()).into(avatar);
                        Picasso.get().load(pic).resize(50, 50).transform(new CircleTransform()).into(avatar);
                    }


                } catch (Exception e) {
                    Timber.e(e, "Error creating avatar");
                }
                break;
            }
        }

        nameView.setOnClickListener(expandClickListener);
        view.setOnClickListener(expandClickListener);


        return view;
    }



    private String GetNumber(View v) {
        TextView textNumber = v.findViewById(R.id.dial_number);
        EditText textNumberEdit = v.findViewById(R.id.dial_number_edit);
        String n = "";
        try { n = textNumber.getText().toString().trim(); } catch (Exception e) {}
        if(n==""){
            try { n = textNumber.getText().toString().trim(); } catch (Exception e) {}
        }
        return n;
    }

    private void loadDetail(LinearLayout cont, Contact c) {



        cont.removeAllViews();
        List<Number> numbers = ContactsManager.getContactNumbers(context, c.getId().toString(),c.getRawId().toString(),1);
        if(numbers.size()< 1){
            numbers = ContactsManager.getContactNumbers(context, c.getId().toString(),c.getRawId().toString(),0);
        }
        for (Number n : numbers) {
            addDetail(cont, n, n.getLabel());
        }

    }


    private void addDetail(LinearLayout layout, Number number, String label) {
        if(label==null){
            label="";
        }

        View detail = inflater.inflate(R.layout.view_contact_detail_item, null);

        String n = number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber();

        ((TextView) detail.findViewById(R.id.detail_call_label)).setText(label + " " + n);

        TextView operatorNameView = detail.findViewById(R.id.detail_call_operator_name);
        String operatorName = Settings.getOperatorDefault();
        operatorName = StringUtils.capitalize(operatorName.toLowerCase(),new char[]{' '});

        if(operatorName.length() >= 10){
            operatorNameView.setTextSize(11.0f);
        }

        operatorNameView.setText(operatorName);

        DetailClickListener listener = new DetailClickListener(number.getRawNumber(), context);
        if (transferFromId != null && transferFromId > 0) {
            listener.setTransferFromId(transferFromId);
        }

        detail.findViewById(R.id.detail_call_gsm).setOnClickListener(listener);

        if (Settings.isRegistered()) {

            if (number.isHasMimeFree()) {
                detail.findViewById(R.id.detail_call_free).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_free));
            }

            if (number.isHasMimeSecure() && Settings.getImSecure(context)) {
                detail.findViewById(R.id.detail_call_secure).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_secure));
            }

            if (number.isHasMimeWings()) {
                detail.findViewById(R.id.detail_call_wings).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_wings));
            }
            //detail.findViewById(R.id.detail_call_video).setOnClickListener(listener);
        } else {

            disableButton(detail.findViewById(R.id.detail_call_free));
            disableButton(detail.findViewById(R.id.detail_call_wings));
            disableButton(detail.findViewById(R.id.detail_call_secure));

        }
        //detail.findViewById(R.id.detail_call_sms).setOnClickListener(listener);
        //detail.findViewById(R.id.detail_call_other).setOnClickListener(listener);
        layout.addView(detail);

    }


    private void disableButton(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof ImageView) {
                    ((ImageView) child).setColorFilter(0xFFDDDDDD, PorterDuff.Mode.SRC_IN);
                }
            }
        }
    }


    public void setFilterQuery(String filterQuery) {
        this.filterQuery = filterQuery;
    }
}
