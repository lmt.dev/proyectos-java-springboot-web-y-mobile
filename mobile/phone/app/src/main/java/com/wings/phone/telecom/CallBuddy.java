package com.wings.phone.telecom;

import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;

/**
 * User data on single call
 */
public class CallBuddy {

    private String id;
    private String name;
    private String photoUri;
    private String number;
    private String formattedNumber;
    private String numberLabel;
    private String sipUri;
    private String mimeFromContact;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setFormattedNumber(String formattedNumber) {
        this.formattedNumber = formattedNumber;
    }

    public String getFormattedNumber() {
        return formattedNumber;
    }

    public void setNumberLabel(String numberLabel) {
        this.numberLabel = numberLabel;
    }

    public String getNumberLabel() {
        return numberLabel;
    }

    public String getSipUri() {
        return sipUri;
    }

    public String getCleanNumber() {
        return StringUtils.cleanUpNumber(number);
    }

    public void buildFreeURI() {
        sipUri = "sip:" + StringUtils.cleanUpNumber(number) + "@" + Settings.getProvisioning("sip.sipServer", "sip.wingsmobile.net");
    }

    public void buildWingsURI() {
        sipUri = "sip:int" + StringUtils.cleanUpNumber(number) + "@" + Settings.getProvisioning("sip.sipServer", "sip.wingsmobile.net");
    }

    public void buildSecureURI() {
        sipUri = "sip:sec" + StringUtils.cleanUpNumber(number) + "@" + Settings.getProvisioning("sip.sipServer", "sip.wingsmobile.net");
    }

    public String getMimeFromContact() {
        return mimeFromContact;
    }

    public void setMimeFromContact(String mimeFromContact) {
        this.mimeFromContact = mimeFromContact;
    }
}
