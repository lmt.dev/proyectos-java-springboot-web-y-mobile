package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.wings.phone.adapters.SelectableHistoryAdapter;
import com.wings.phone.data.CallLogManager;
import com.wings.phone.fragments.tabs.HistoryFragment;
import com.wings.phone.model.CallLog;
import com.wings.phone.model.DataRepository;
import com.wings.phone.util.Settings;
import com.wings.phone.viewmodels.HistoryViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CallHistoryActivity extends AppCompatActivity {

    private ListView list;
    private HistoryViewModel selectableHistoryViewModel;
    private SelectableHistoryAdapter historyAdapter;
    private List<CallLog> callLogs;





    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_call_history);

        list = findViewById(R.id.list_history);
        if(callLogs == null){
            callLogs = new LinkedList<>();
        }
        historyAdapter = new SelectableHistoryAdapter(getApplicationContext(),callLogs);
        list.setFastScrollEnabled(true);
        list.setAdapter(historyAdapter);
        selectableHistoryViewModel = new ViewModelProvider(this, new ViewModelFactory(getApplicationContext())).get(HistoryViewModel.class);
        selectableHistoryViewModel.getCallLogtListMutableLiveData().observe(this, new Observer<List<CallLog>>() {
            @Override
            public void onChanged(List<CallLog> callLogs) {
                //new AsyncAdapterInit(callLogs).execute();
                historyAdapter.setCallLogList(callLogs);
                historyAdapter.notifyDataSetChanged();
            }

        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        new AsyncAdapterInit().execute();



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_log, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_delete_selected) {
            Set<Integer> ids = historyAdapter.getSelectedIds();
            CallLogManager.deleteCallLog(CallHistoryActivity.this, ids);
            selectableHistoryViewModel.refresh();

        } else if (item.getItemId() == R.id.action_delete_all) {
            new AlertDialog.Builder(CallHistoryActivity.this)
                    .setTitle(R.string.delete_all)
                    .setMessage(R.string.delete_all_confirm)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            CallLogManager.deleteAllCallLog(CallHistoryActivity.this);
                            selectableHistoryViewModel.refresh();

                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        }
        Settings.setPreference("UI."+HistoryFragment.TAG+".update","true",getApplicationContext());
        return super.onOptionsItemSelected(item);
    }


    static class ViewModelFactory implements ViewModelProvider.Factory {
        private Application application;
        ViewModelFactory(Context context) {
            application = (Application) context.getApplicationContext();

        }
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull Class<T> modelClass) {
            return (T) new HistoryViewModel(application);
        }
    }
    private class AsyncAdapterInit extends AsyncTask<String, String, SelectableHistoryAdapter> {


        @Override
        protected SelectableHistoryAdapter doInBackground(String... strings) {
            selectableHistoryViewModel.refresh();
            return null;
        }

        @Override
        protected void onPostExecute(SelectableHistoryAdapter historyAdapter) {
            //list.setFastScrollEnabled(true);
            //list.setAdapter(historyAdapter);
        }
    }
}
