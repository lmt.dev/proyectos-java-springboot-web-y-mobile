package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.res.ResourcesCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.data.DataProvider;
import com.wings.phone.fragments.DebugFragment;
import com.wings.phone.fragments.DialpadFragment;
import com.wings.phone.gsm.MakeGsmCallHelper;
import com.wings.phone.sensor.CallActivityHelper;
import com.wings.phone.telecom.CallBuddy;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.telecom.CallWrapper;
import com.wings.phone.telecom.CallWrapperListener;
import com.wings.phone.telecom.DtmfCallback;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.phone.util.OnOneOffClickListener;
import com.wings.phone.util.WingsConst;
import com.wings.phone.views.GlowPadViewListenerStub;

import net.frakbot.glowpadbackport.GlowPadView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import timber.log.Timber;


public class CallActivity extends AppCompatActivity {

    private LinearLayout detailContainer;
    private View viewCallAnswer;
    private View viewCallHangUp;
    private GlowPadView viewCallGlowPad;
    private ImageView viewCallMute;
    private ImageView viewCallSpeaker;
    private ImageView viewCallHold;
    private ImageView viewCallDialpad;
    private ImageView viewCallTransfer;
    private ImageView viewCallAdd;
    private ImageView viewCallMerge;
    private ImageView viewCallSwap;
    private ImageView viewCallDebug;

    private CallManager callManager;
    private LayoutInflater inflater;
    private Map<String, View> viewCallMap = new HashMap<>();
    private CallActivityHelper helper;

    private Timer pingTimer;
    private Timer callStatusTimer;
    private boolean dialPadShowing = false;
    private boolean canSwap = true;

    private boolean debugShow = false;
    private boolean secureMessagePlayed = false;
    private DebugFragment debugFrag;
    final DialpadFragment dialpadFragment = new DialpadFragment();
    private LinearLayout viewCallButtons1;
    private LinearLayout viewCallButtons2;
    private OnOneOffClickListener viewCallHoldClickListener;


    private class CallWrapperListenerImpl implements CallWrapperListener {
        @Override
        public void onCallRemoved(final CallWrapper call) {

            if (callManager.getCallList().size() == 0) {
                //to remove notifications if screen is background
                helper.cancelNotifications();
                terminate();
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View callDetail = viewCallMap.remove(call.getCallBuddy().getCleanNumber());

                        //refresh all views to change small of normal detail
                        detailContainer.removeAllViews();
                        List<CallWrapper> calls = callManager.getCallList();
                        for (final CallWrapper call : calls) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    addCallToView(call);
                                    updateViewForActiveCall(); //onCallRemoved

                                }
                            });
                        }
                        //onCallStatusChange(callManager.getActiveCall(), callManager.getActiveCall().getStatus(), "");
                    }
                });
            }

        }

        @Override
        public void onCallStatusChange(final CallWrapper call, String callStatus, String lastReason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallStatus(call);
                    updateViewForActiveCall();//onCallStatusChange

                }
            });
        }

        @Override
        public void onStatsUpdate(final String stats) {
            if (debugFrag != null && debugFrag.getTextView() != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        debugFrag.getTextView().setText(stats);
                    }
                });
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_call);
        
        //
        viewCallHoldClickListener = new OnOneOffClickListener() {
            @Override
            public void onSingleClick(View v) {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        callManager.toogleHold();
                    }
                });

                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        //boolean status = callManager.toogleHold();
                        *//*
                        if (status) {
                            viewCallHold.setImageResource(R.drawable.ic_phone_paused_black_24dp);
                        } else {
                            viewCallHold.setImageResource(R.drawable.ic_pause_black_24dp);
                        }
                         *//*
                    }
                });*/
            }
        };
        //

        bindViews();

        inflater = LayoutInflater.from(CallActivity.this);
        callManager = CallManager.getInstance(getApplicationContext());
        callManager.setListener(new CallWrapperListenerImpl());

        //create helper
        helper = new CallActivityHelper(CallActivity.this);
        callStatusTimer = new Timer();

        //grab action for later use
        String action = getIntent().getAction();

        int transferCallId = 0;
        if (getIntent().hasExtra("transfer_from_id")) {
            transferCallId = getIntent().getIntExtra("transfer_from_id", 0);
            action = WingsConst.INTENT_WINGS_TRANSFER_CALL;
        }

        if (action != null) {

            Bundle extras = getIntent().getExtras();
            CallBuddy buddy = new CallBuddy();
            if (extras != null && extras.getString("third_party_action") != null) {

                buddy = DataProvider.getCallBuddy(getApplicationContext(), getIntent().getData());

                if (buddy.getMimeFromContact().contains("int")) {
                    action = WingsConst.INTENT_WINGS_WINGS_CALL;
                } else if (buddy.getMimeFromContact().contains("sec")) {
                    action = WingsConst.INTENT_WINGS_SECURE_CALL;
                } else {
                    action = WingsConst.INTENT_WINGS_FREE_CALL;
                }

            } else {
                Uri callee = getIntent().getData();
                if (callee != null) {
                    String number = callee.toString().replaceAll("wings:", "");
                    buddy = DataProvider.getCallBuddy(getApplicationContext(), number);
                }
            }

            switch (action) {
                case WingsConst.INTENT_WINGS_SECURE_CALL:
                    buddy.buildSecureURI();
                    callManager.makeSipCall(buddy);
                    break;
                case WingsConst.INTENT_WINGS_WINGS_CALL:
                    buddy.buildWingsURI();
                    callManager.makeSipCall(buddy);
                    break;
                case WingsConst.INTENT_WINGS_FREE_CALL:
                    buddy.buildFreeURI();
                    callManager.makeSipCall(buddy);
                    break;
                case WingsConst.INTENT_WINGS_INCOMING:
                    //se registra desde el push ahora
                    break;
                case WingsConst.INTENT_WINGS_VIDEO_CALL:
                    //TODO:
                    break;
                case WingsConst.INTENT_WINGS_TRANSFER_CALL:
                    if (transferCallId != 0) {

                        final Integer finalTransferCallId = transferCallId;
                        final CallBuddy budToTransfer = buddy;

                        buddy.buildFreeURI();
                        //buddy.buildSecureURI();
                        //buddy.buildWingsURI();

                        callManager.transferCall(finalTransferCallId, budToTransfer);
                    }
                    break;
                case WingsConst.INTENT_WINGS_GSM_CALL_ADDED:

                    //check if has default sim
                    if (getIntent().hasExtra("account_selected")) {
                        TelecomManager telecomManager = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                        @SuppressLint("MissingPermission") final List<PhoneAccountHandle> handles = telecomManager.getCallCapablePhoneAccounts();
                        AlertDialog.Builder builder = new AlertDialog.Builder(CallActivity.this);
                        MakeGsmCallHelper.PhoneAccountAdapter adapter = new MakeGsmCallHelper.PhoneAccountAdapter(CallActivity.this, handles);
                        adapter.setTelecomManager(telecomManager);
                        builder
                                .setTitle(R.string.helper_choose_sim)
                                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        CallManager manager = CallManager.getInstance(CallActivity.this);
                                        manager.getActiveCall().getGsmCall().phoneAccountSelected(handles.get(which), false);
                                    }
                                });
                        builder.create().show();
                    }
                    break;
            }
        }
        helper.onCreate();

        if (action != null && action.equals("ACTION_HANGUP")) {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callManager.hangUp();
                    }
                });
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        helper.onResume();

        detailContainer.removeAllViews();
        List<CallWrapper> calls = callManager.getCallList();
        for (final CallWrapper call : calls) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    addCallToView(call);
                    updateViewForActiveCall(); //onResume
                }
            });
        }

        if (callManager.getCallList().isEmpty()) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        helper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        helper.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
        if (dialPadShowing) {
            dialPadShowing = false;
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                    .remove(dialpadFragment)
                    .commit();
        } else {
            super.onBackPressed();
        }
    }

    private void terminate() {
        callManager.terminate();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 500);
            }
        });
    }

    private void terminateNow() {
        callManager.terminate();
        finish();
    }

    private void bindViews() {
        detailContainer = findViewById(R.id.caller_detail_container);
        detailContainer.removeAllViews();

        viewCallGlowPad = findViewById(R.id.call_glow_pad);
        viewCallAnswer = findViewById(R.id.call_answer);
        viewCallHangUp = findViewById(R.id.call_hang_up);
        viewCallMute = findViewById(R.id.call_mute);
        viewCallSpeaker = findViewById(R.id.call_speaker);
        viewCallHold = findViewById(R.id.call_hold);
        viewCallDialpad = findViewById(R.id.call_dialPad);
        viewCallDebug = findViewById(R.id.call_debug);


        viewCallTransfer = findViewById(R.id.call_transfer);
        viewCallAdd = findViewById(R.id.call_add);
        viewCallMerge = findViewById(R.id.call_merge);
        viewCallSwap = findViewById(R.id.call_swap);

        viewCallButtons1 = findViewById(R.id.call_call_buttons1);
        viewCallButtons2 = findViewById(R.id.call_call_buttons2);

        viewCallGlowPad.setVisibility(View.GONE);
        viewCallHangUp.setVisibility(View.GONE);
        viewCallAnswer.setVisibility(View.GONE);

        //viewCallButtons1.setVisibility(View.GONE);
        viewCallButtons2.setVisibility(View.GONE);


    }

    private void addCallToView(final CallWrapper call) {

        View detailView;
        if (callManager.getCallList().size() > 2) {
            detailView = inflater.inflate(R.layout.view_call_detail_small, null);
        } else {
            detailView = inflater.inflate(R.layout.view_call_detail, null);
        }

        ImageView callType = detailView.findViewById(R.id.call_detail_type);
        final ImageView callPhoto = detailView.findViewById(R.id.call_detail_photo);
        TextView callName = detailView.findViewById(R.id.call_detail_name);
        TextView callNumber = detailView.findViewById(R.id.call_detail_number);
        TextView callStatus = detailView.findViewById(R.id.call_detail_status);
        TextView callSecret = detailView.findViewById(R.id.call_detail_secret);
        callSecret.setVisibility(View.GONE);


        callPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callManager.getActiveCall() != call){
                    if(canSwap && !callManager.isInConference()){
                        canSwap = false;
                        callManager.setActiveCall(call);
                        canSwap = true;
                    }
                }
            }
        });

        switch (call.getType()) {
            case WingsConst.CALL_TYPE_GSM:
                callType.setImageResource(R.drawable.ic_call_gsm2);
                break;
            case WingsConst.CALL_TYPE_FREE:
                callType.setImageResource(R.drawable.ic_call_free2);
                break;
            case WingsConst.CALL_TYPE_WINGS:
                callType.setImageResource(R.drawable.ic_call_wings2);
                break;
            case WingsConst.CALL_TYPE_SECURE:
                callType.setImageResource(R.drawable.outline_lock_open_black_24);
                break;
            case WingsConst.CALL_TYPE_VIDEO:
                callType.setImageResource(R.drawable.baseline_videocam_24);
                break;
        }

        if (call.getCallBuddy().getPhotoUri() != null) {
            final String uri = call.getCallBuddy().getPhotoUri();
            final String name = call.getCallBuddy().getName();
            if(uri!=null){
                Picasso.get().load(uri).transform(new CircleTransform()).into(callPhoto);
            }else{
                TextDrawable drawable = TextDrawable.builder().buildRound(name.substring(0,1), ColorGenerator.MATERIAL.getColor(call.getCallBuddy().getId()));
                callPhoto.setImageDrawable(drawable);

            }
        }

        callName.setText(call.getCallBuddy().getName());
        if (call.getCallBuddy().getNumber() != null && !call.getCallBuddy().getNumber().isEmpty()) {
            callNumber.setText(call.getCallBuddy().getFormattedNumber());
        } else {
            callNumber.setText(getString(android.R.string.unknownName));
        }


        int statusId = 0;
        try {
            statusId = WingsConst.getStringIdentifier(CallActivity.this, call.getStatus());
        } catch (Exception e) {
        }

        if (statusId != 0) {
            callStatus.setText(getString(statusId));
        } else {
            callStatus.setText(call.getStatus());
        }

        detailContainer.addView(detailView);
        viewCallMap.put(call.getCallBuddy().getCleanNumber(), detailView);

        if (call.isCurrent()) {
            int green = ResourcesCompat.getColor(getResources(), R.color.colorGreen, null);
            callType.setColorFilter(green);
        } else {
            int white = ResourcesCompat.getColor(getResources(), R.color.colorOff, null);
            callType.setColorFilter(white);
        }

    }

    private void updateViewForActiveCall() {

        final CallWrapper activeCall = callManager.getActiveCall();
        if (activeCall != null) {

            setupCallAudioButtons();
            updateCallStatus(activeCall);

            switch (activeCall.getStatus()) {
                case WingsConst.CALL_STATUS_REGISTERED:

                    //push incoming, already registered, we only set status, no break
                    if (activeCall.isSip() && activeCall.getDirection() == WingsConst.CALL_DIRECTION_INCOMING) {
                        activeCall.setStatus(WingsConst.CALL_STATUS_RINGING);
                        updateCallStatus(activeCall);
                    } else {
                        //else break to avoid glowpad
                        break;
                    }

                case WingsConst.CALL_STATUS_RINGING:

                    viewCallGlowPad.setVisibility(View.VISIBLE);
                    viewCallHangUp.setVisibility(View.GONE);
                    viewCallAnswer.setVisibility(View.GONE);

                    setupGlowpadForActiveCall();

                    if (activeCall.isSip() && activeCall.getDirection() == WingsConst.CALL_DIRECTION_INCOMING) {
                        helper.startExternalRinging();
                    }

                    break;
                case WingsConst.CALL_STATUS_DIALING:

                    viewCallGlowPad.setVisibility(View.GONE);
                    viewCallHangUp.setVisibility(View.VISIBLE);
                    viewCallAnswer.setVisibility(View.GONE);

                    viewCallHangUp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callManager.hangUp();
                                }
                            });
                        }
                    });

                    if (activeCall.isSip()) {
                        //helper.startRinging();
                    }

                    break;
                case WingsConst.CALL_STATUS_DISCONNECTING:
                case WingsConst.CALL_STATUS_CONNECTING:

                    //if (activeCall.isSip() && activeCall.getDirection() == WingsCall.DIRECTION_INCOMING) {
                    helper.stopExternalRinging();
                    //}

                    break;
                case WingsConst.CALL_STATUS_ACTIVE:
                    if(activeCall.isCurrent()){
                        setupCallControlButtons();
                        helper.stopExternalRinging();
                        helper.stopRinging();

                        viewCallButtons1.setVisibility(View.VISIBLE);
                        viewCallButtons2.setVisibility(View.VISIBLE);
                        if(activeCall.getType() == WingsConst.CALL_TYPE_SECURE){
                            viewCallButtons2.setVisibility(View.GONE);
                        }

                        viewCallGlowPad.setVisibility(View.GONE);
                        viewCallHangUp.setVisibility(View.VISIBLE);
                        viewCallAnswer.setVisibility(View.GONE);
                        viewCallHangUp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        activeCall.hangUp();
                                    }
                                });
                            }
                        });

                        //timer for call duration
                        //try{ callStatusTimer.cancel(); callStatusTimer = new Timer();}catch (Exception ex){}
                        callStatusTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                updateCallStatus(activeCall);
                                if(activeCall.getStatus().equals(WingsConst.CALL_STATUS_DISCONNECTED)){
                                    this.cancel();
                                }
                            }
                        }, 500, 1000);

                    }else{
                        Timber.e("Error 22");
                    }




                    break;
                case WingsConst.CALL_STATUS_DISCONNECTED:
                    Timber.e("Disconected "+ activeCall.getId());
                    activeCall.setCurrent(false);

                    updateCallStatus(activeCall);
                    viewCallGlowPad.setVisibility(View.GONE);
                    //viewCallHangUp.setVisibility(View.GONE);
                    viewCallAnswer.setVisibility(View.GONE);

                    if (activeCall.isSip()) {
                        helper.startDisconnect();
                        helper.stopExternalRinging();
                    }

                    break;
                case WingsConst.CALL_STATUS_HOLDING:
                    if (activeCall.isSip()) {
                        helper.stopRinging();
                        helper.startHolding();
                    }
                    updateCallStatus(activeCall);
                    break;

                case WingsConst.CALL_STATUS_REGISTERING:

                    viewCallHangUp.setVisibility(View.VISIBLE);
                    viewCallHangUp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            helper.startDisconnect();
                            terminateNow();
                        }
                    });
                    break;

                case WingsConst.CALL_STATUS_REGISTRATION_FAILED:
                    updateCallStatus(activeCall);
                    callManager.removeCall(activeCall.getCallBuddy().getCleanNumber());
                    terminate();
                    break;
            }


        }
    }




    private void updateCallStatus(final CallWrapper call) {

        //maybe null
        if (call == null ) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View v = viewCallMap.get(call.getCallBuddy().getCleanNumber());

                if (v != null) {

                    //update call status label
                    final TextView callStatus = v.findViewById(R.id.call_detail_status);
                    int statusId = WingsConst.getStringIdentifier(CallActivity.this, call.getStatus());

                    String append = "";
                    //TODO: ver el last status en que momentos vale ponerlo
                    /*if (call.getLastReason() != null) {
                        int lastStatusId = WingsConst.getStringIdentifier(CallActivity.this, call.getLastReason());
                        if (lastStatusId == 0) {
                            append = call.getLastReason();
                        } else {
                            append = getString(lastStatusId);
                        }
                    }*/




                    if (statusId != 0) {
                        callStatus.setText(getString(statusId) + " " + append);
                    } else {
                        callStatus.setText(call.getStatus());
                    }

                    //is is current call, show duration
                    //if (call.getStatus() != null && call.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE) && call.isCurrent()) {
                    if (call.isCurrent() && call.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(call.getActivationTime() == 0){
                                    call.setActivationTime(System.currentTimeMillis());
                                }
                                String timer = callStatus.getText() + " " + call.getDurationString();
                                callStatus.setText(timer);
                            }
                        });
                    }

                    //set crypt message
                    if (call.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE) &&
                            call.getType() == WingsConst.CALL_TYPE_SECURE &&
                            call.getSecret() == null) {
                        callStatus.setText(getString(R.string.ENCRYPTING));
                    }

                    //play secure voice message
                    //TODO: move this to callwrapper
                    if (call.getStatus().equals(WingsConst.CALL_STATUS_ACTIVE) &&
                            call.getType() == WingsConst.CALL_TYPE_SECURE &&
                            call.getSecret() != null &&
                            call.getDurationInSeconds() < 7 &&
                            !secureMessagePlayed) {

                        helper.playSecureMessage();

                        secureMessagePlayed = true;
                    }

                    //show color for call type icon if is current
                    View detailView = viewCallMap.get(call.getCallBuddy().getCleanNumber());
                    ImageView callType = (ImageView) detailView.findViewById(R.id.call_detail_type);
                    if (call.getStatus() == WingsConst.CALL_STATUS_ACTIVE) {
                        int green = ResourcesCompat.getColor(getResources(), R.color.colorGreen, null);
                        callType.setColorFilter(green);
                    } else {
                        int white = ResourcesCompat.getColor(getResources(), R.color.colorOff, null);
                        callType.setColorFilter(white);
                    }


                    //prevent changing icon before it's on hold
                    boolean show = call.decrementHoldCounter() > 0;

                    if(call.isCurrent()){
                        //change icon if call is on hold
                        if (call.getStatus() == WingsConst.CALL_STATUS_HOLDING && call == callManager.getActiveCall()){
                            viewCallHold.setImageResource(R.drawable.ic_phone_paused_black_24dp);
                        } else {
                            viewCallHold.setImageResource(R.drawable.ic_pause_black_24dp);
                        }
                    }


                    //change secure icon and put secret if it's there
                    if (call.getSecret() != null) {
                        callType.setImageResource(R.drawable.baseline_lock_black_24);
                        v.findViewById(R.id.call_detail_secret).setVisibility(View.VISIBLE);
                        ((TextView) v.findViewById(R.id.call_detail_secret)).setText(call.getSecret());
                    }
                }
            }
        });
    }

    private void setupGlowpadForActiveCall() {

        if (pingTimer == null) {
            pingTimer = new Timer(true);
            pingTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewCallGlowPad.ping();
                        }
                    });
                }
            }, 10, 500);
        }

        viewCallGlowPad.setOnTriggerListener(new GlowPadViewListenerStub() {
            @Override
            public void onTrigger(View v, int target) {
                pingTimer.cancel();
                pingTimer = null;
                viewCallGlowPad.setVisibility(View.GONE);

                if (target == 0) {
                    callManager.answer();
                } else {
                    callManager.reject();
                }
                updateViewForActiveCall(); //setupGlowpadForActiveCall
            }
        });
    }

    private void setupCallAudioButtons() {

        //Mute
        viewCallMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean muted = helper.toogleMute();
                if (muted) {
                    viewCallMute.setImageResource(R.drawable.ic_mic_off_black_24dp);
                } else {
                    viewCallMute.setImageResource(R.drawable.ic_mic_black_24dp);
                }
            }
        });

        //Speaker
        viewCallSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean speakers = helper.toogleSpeakers();
                if (speakers) {
                    viewCallSpeaker.setImageResource(R.drawable.ic_volume_up_black_24dp);
                } else {
                    viewCallSpeaker.setImageResource(R.drawable.ic_volume_off_black_24dp);
                }
            }
        });

        //setup current audio status
        if (helper.isMuted()) {
            viewCallMute.setImageResource(R.drawable.ic_mic_off_black_24dp);
        } else {
            viewCallMute.setImageResource(R.drawable.ic_mic_black_24dp);
        }

        if (helper.isOnSpeakers()) {
            viewCallSpeaker.setImageResource(R.drawable.ic_volume_up_black_24dp);
        } else {
            viewCallSpeaker.setImageResource(R.drawable.ic_volume_off_black_24dp);
        }
    }

    private void setupCallControlButtons() {

        if (BuildConfig.BUILD_TYPE == "debug") {
            //debug
            viewCallDebug.setVisibility(View.VISIBLE);
            viewCallDebug.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (debugFrag == null) {
                        debugFrag = new DebugFragment();
                    }

                    if (debugShow) {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                                .remove(debugFrag)
                                .commit();

                        debugShow = false;
                        debugFrag = null;

                    } else {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                                .add(R.id.call_buttons_container, debugFrag, DebugFragment.TAG)
                                .commit();
                        debugShow = true;

                    }
                }
            });
        }



        //Hold

        viewCallHold.setOnClickListener(viewCallHoldClickListener);

        //show dialpad
        viewCallDialpad.setOnClickListener(new OnOneOffClickListener() {
            @Override
            public void onSingleClick(View v) {

                if (!dialPadShowing) {

                    dialPadShowing = true;

                    if (!dialpadFragment.isInitialized()) {
                        DtmfCallback dtmfCallback = new DtmfCallback(CallActivity.this);
                        dtmfCallback.setCall(callManager.getActiveCall());

                        dialpadFragment.init(false, true, false, false,dtmfCallback, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialPadShowing = false;
                                getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                                        .remove(dialpadFragment)
                                        .commit();
                            }
                        });
                    }

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                            .add(R.id.call_buttons_container, dialpadFragment, DialpadFragment.TAG)
                            .commit();
                } else {

                    dialPadShowing = false;
                    getSupportFragmentManager()
                            .beginTransaction()
                            .remove(dialpadFragment)
                            .commit();
                }
            }
        });

        //add call
        viewCallAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CallActivity.this, AddCallActivity.class);
                startActivity(i);
            }
        });

        //transfer
        viewCallTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer id = null;
                if (callManager.getActiveCall() != null) {
                    id = callManager.getActiveCall().getId();
                }

                if (!callManager.getActiveCall().isSip()) {
                    Toast.makeText(CallActivity.this, getString(R.string.helper_cant_transfer_call), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (id != null) {
                    Intent intent = new Intent(CallActivity.this, TransferCallActivity.class);
                    intent.putExtra("transfer_from_id", callManager.getActiveCall().getId());
                    startActivity(intent);
                }

            }
        });

        //merge
        viewCallMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!callManager.canMakeConference()) {
                            Toast.makeText(CallActivity.this, getString(R.string.conference_not_available), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        final List<CallWrapper> calls = callManager.getCallList();
                        int response;
                        if (callManager.isInConference()) {
                            response = callManager.removeConference(calls.get(0), calls.get(1));
                            viewCallMerge.setImageResource(R.drawable.baseline_call_merge_24);
                        } else {
                            response = callManager.makeConference(calls.get(0), calls.get(1));
                            viewCallMerge.setImageResource(R.drawable.baseline_call_split_24);
                        }
                        Toast.makeText(CallActivity.this, getString(response), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


        //swap calls
        viewCallSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //*prevent multiple clicking
                if (canSwap) {
                    canSwap = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            canSwap = true;
                        }
                    }, 1500);
                } else {
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (callManager.getCallList().size() <= 1 || callManager.isInConference()) {
                            Toast.makeText(CallActivity.this, R.string.swap_calls_error, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        callManager.swapActiveCall();
                        detailContainer.removeAllViews();
                        List<CallWrapper> calls = callManager.getCallList();
                        for (CallWrapper call : calls) {
                            addCallToView(call);
                            updateCallStatus(call);
                        }
                        updateViewForActiveCall();//swap calls
                        //canSwap = true;
                    }
                });
            }
        });





    }



}
