package com.wings.phone.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils {

    private static final int MAX_BUFFER_SIZE = 1024 * 1024;

    public static void copyFileFromPackage(final Context context, final int resourceId, final String target) {
        //noinspection OverlyBroadCatchBlock
        try {
            final FileOutputStream outputStream = context.openFileOutput(target, 0);
            final InputStream inputStream = context.getResources().openRawResource(resourceId);
            FileUtils.copyStream(inputStream, outputStream);
            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch (final IOException e) {

        }
    }

    public static void copyStream(final InputStream is, final OutputStream os) throws IOException {
        final byte[] buffer = new byte[MAX_BUFFER_SIZE];
        int length;
        while ((length = is.read(buffer)) != -1) {
            os.write(buffer, 0, length);
        }
    }

    public static File getFilesDirectory() {

        String filesDir = (new StringBuilder()).append(Environment.getExternalStorageDirectory().getAbsolutePath()).append("/").append("Calls").append("/").toString();
        File myDir = new File(filesDir);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        return myDir;
    }
}
