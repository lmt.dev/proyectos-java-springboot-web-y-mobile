package com.wings.phone.gsm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.wings.phone.R;
import com.wings.phone.util.InternetCheck;
import com.wings.phone.util.NetworkUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.phone.util.WingsConst;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class MakeGsmCallHelper {

    //@VisibleForTesting
    //static final String MMI_IMEI_DISPLAY = "*#06#";
    //private static final String MMI_REGULATORY_INFO_DISPLAY = "*#07#";
    //add by liuhuimin for show teeresult 20171226
    //private static final String TEE_CHECK = "*#08#";
    //add by liuhuimin for open mtklog 20180830
    //private static final String OPEN_MTKLOG = "*#3646#";
    //add by liuhuimin for open factory 20180830
    //private static final String OPEN_FACTORY = "*#66#";
    //private static final String MMI_USB_REBOOT_META_SECRET_CODE = "*#*#3641122#*#*";
    //private static final String MMI_WIFI_REBOOT_META_SECRET_CODE = "*#*#3642233#*#*";

    //private static final List<String> codes = Arrays.asList("*#06#", "*#07#", "*#08#", "*#3646#", "*#66#", "*#*#3641122#*#*", "*#*#3642233#*#*");

    /**
     * Method used everywhere else besides history
     *
     * @param context
     * @param number
     */
    public static void makeIntent(final Context context, final String number) {

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        //Map<Integer, List<EmergencyNumber>> EmergencyNumberList = tm.getEmergencyNumberList();
        Map<String, String> EmergencyNumberList = new LinkedHashMap<>();
        //Argentina
        EmergencyNumberList.put("100","Bomberos");
        EmergencyNumberList.put("101","Policia");
        EmergencyNumberList.put("103","Defensa civil");
        EmergencyNumberList.put("105","Emergencia ambiental");
        EmergencyNumberList.put("106","Emergencia nautica");
        EmergencyNumberList.put("107","Emergencia medica");
        EmergencyNumberList.put("135","Asistencia al suicida");
        EmergencyNumberList.put("52751135","Asistencia al suicida");
        EmergencyNumberList.put("142","Personas extraviadas");
        EmergencyNumberList.put("911","Emergencias");
        //Colombia
        EmergencyNumberList.put("112","Policia Nacional");
        EmergencyNumberList.put("144","Defensa civil");
        EmergencyNumberList.put("123","Emergencias");
        EmergencyNumberList.put("132","Cruz Roja");
        //Peru
        EmergencyNumberList.put("113","Emergencia Coronavirus");
        EmergencyNumberList.put("110","policía de carreteras");
        EmergencyNumberList.put("115","");
        EmergencyNumberList.put("116","Bomberos");
        EmergencyNumberList.put("1818","Central Única de Denuncias del Mininter");
        //Brasil
        EmergencyNumberList.put("193","Bomberos");
        EmergencyNumberList.put("199","Defensa Civil");
        EmergencyNumberList.put("197","Policia Civil");
        EmergencyNumberList.put("190","Policia Militar");
        EmergencyNumberList.put("192","Servicio de Urgencias");
        EmergencyNumberList.put("198","Policia Rodoviaria");
        //Mexico
        EmergencyNumberList.put("065","Cruz Roja");
        EmergencyNumberList.put("068","Bomberos");
        EmergencyNumberList.put("060","Policía");
        //Venezuela
        EmergencyNumberList.put("0800558842","Protección civil");
        EmergencyNumberList.put("08005588427","Protección civil");
        EmergencyNumberList.put("02125454545","Servicio de ambulancias metropolitano");
        EmergencyNumberList.put("02125457765","Bomberos metropolitanos");
        EmergencyNumberList.put("02125458053","Bomberos metropolitanos");
        EmergencyNumberList.put("02125450331","Bomberos metropolitanos");
        EmergencyNumberList.put("02125643786","Policía metropolitana");
        EmergencyNumberList.put("02128625871","Policía metropolitana");
        EmergencyNumberList.put("02128625872","Policía metropolitana");
        EmergencyNumberList.put("02125714713","Socorristas de Cruz Roja de la Gran Caracas");
        //Bolivia
        EmergencyNumberList.put("120","Policía Boliviana");
        EmergencyNumberList.put("165","Ambulancia de emergencias");
        EmergencyNumberList.put("160","Ambulancia de emergencias");
        EmergencyNumberList.put("119","Bomberos");
        EmergencyNumberList.put("123","búsqueda y rescate");
        EmergencyNumberList.put("114","Retén de emergencias");
        //España
        EmergencyNumberList.put("062","Guardia Civil");
        EmergencyNumberList.put("112","Emergencias");







        if (PhoneNumberUtils.isEmergencyNumber(number) || number.equals("119") || number.equals("110") || EmergencyNumberList.containsKey(number)) {
            try {
                Intent callIntent = new Intent("com.android.phone.EmergencyDialer.DIAL");
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                callIntent.setData(Uri.parse("tel:" + number)); // < Optional, will prompt number for user
                context.startActivity(callIntent);
                return;
            } catch (Exception e) {

                // in case something went wrong ...
            }
        }

        //secrets codes
        /*if (codes.contains(number)) {
            String secretCode = number.replaceAll("#", "");
            secretCode = secretCode.replaceAll("\\*", "");
            handleSecretCode(context, secretCode);
            //Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode(number)));
            //context.startActivity(intent);
            //Intent intent = new Intent(SECRET_CODE_ACTION, Uri.parse("android_secret_code://" + secretCode));
            //context.sendBroadcast(intent);
            return;
        }

        //for everything that starts with *#*# and ends with #*#*
        if (number.length() > 8 && number.startsWith("*#*#") && number.endsWith("#*#*")) {
            String secretCode = number.replaceAll("#", "");
            secretCode = secretCode.replaceAll("\\*", "");
            handleSecretCode(context, secretCode);
            return;
        }*/

        try {
            //check if the phone has sim
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int state = telephonyManager.getSimState();
            if (state != TelephonyManager.SIM_STATE_READY) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.helper_no_sim));
                builder.setMessage(context.getString(R.string.helper_no_sim_message));
                builder.create().show();
                return;
            }

            //else check if has default sim
            TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
            final PhoneAccountHandle handle = telecomManager.getDefaultOutgoingPhoneAccount("tel");

            if (handle == null) {

                final List<PhoneAccountHandle> handles = telecomManager.getCallCapablePhoneAccounts();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                PhoneAccountAdapter adapter = new PhoneAccountAdapter(context, handles);
                adapter.setTelecomManager(telecomManager);

                builder
                        .setTitle(R.string.helper_choose_sim)
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String numberEnc = Uri.encode(number);
                                Uri uri = Uri.parse("tel:" + numberEnc);
                                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                                intent.putExtra(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, handles.get(which));
                                context.startActivity(intent);

                            }
                        });


                builder.create().show();

            } else {
                String numberEnc = Uri.encode(number);
                Uri uri = Uri.parse("tel:" + numberEnc);
                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                context.startActivity(intent);
            }

        } catch (SecurityException ex) {
            Timber.e(ex);
        }
    }

    /**
     * Method use from history
     *
     * @param context
     * @param number
     * @param provider
     */
    public static void makeIntentWithProvider(final Context context, final String number, final String provider) {

        if (provider.startsWith("ACCOUNT")) {

            if(!Settings.isRegistered()){
                Toast.makeText(context,R.string.register_to_use_services,Toast.LENGTH_LONG).show();
                return;
            }

            if (NetworkUtils.isOnline(context)) {
                NetworkUtils.hasInternet(new InternetCheck.Consumer() {
                    @Override
                    public void accept(Boolean internet) {
                        if (internet) {
                            Uri uri = Uri.parse("wings:" + StringUtils.cleanUpNumber(number));
                            Intent intent = new Intent();
                            switch (provider) {
                                case WingsConst.ACCOUNT_FREE:
                                    intent.setAction(WingsConst.INTENT_WINGS_FREE_CALL);
                                    intent.setData(uri);
                                    break;
                                case WingsConst.ACCOUNT_WINGS:
                                    intent.setAction(WingsConst.INTENT_WINGS_WINGS_CALL);
                                    intent.setData(uri);
                                    break;
                                case WingsConst.ACCOUNT_SECURE:
                                    intent.setAction(WingsConst.INTENT_WINGS_SECURE_CALL);
                                    intent.setData(uri);
                                    break;
                            }
                            context.startActivity(intent);
                        } else {
                            NetworkUtils.showNoInternetDialog(context);
                        }
                    }
                });
            } else {
                NetworkUtils.showNoInternetDialog(context);
            }

        } else {

            try {

                int which = 0;
                List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
                if (subscriptionInfos != null) {
                    for (int i = 0; i < subscriptionInfos.size(); i++) {
                        SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                        if (lsuSubscriptionInfo.getIccId().equals(provider)) {
                            which = i;
                            break;
                        }
                    }
                }

                TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                final List<PhoneAccountHandle> handles = telecomManager.getCallCapablePhoneAccounts();
                String numberEnc = Uri.encode(number);
                Uri uri = Uri.parse("tel:" + numberEnc);
                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                intent.putExtra(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, handles.get(which));
                context.startActivity(intent);

            } catch (SecurityException ex) {
                Timber.e(ex);
            }

        }
    }

    /**
     * Phone Account adapter to choose SIM
     */
    public static class PhoneAccountAdapter extends ArrayAdapter<PhoneAccountHandle> {

        private Context mContext;
        private List<PhoneAccountHandle> handles;
        private TelecomManager telecomManager;

        public PhoneAccountAdapter(@NonNull Context context, List<PhoneAccountHandle> list) {
            super(context, 0, list);
            mContext = context;
            handles = list;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View listItem = convertView;
            if (listItem == null) {
                listItem = LayoutInflater.from(mContext).inflate(R.layout.adapter_phone_account, parent, false);
            }

            PhoneAccountHandle handle = handles.get(position);
            PhoneAccount account = telecomManager.getPhoneAccount(handle);

            ImageView image = (ImageView) listItem.findViewById(R.id.imageView_poster);
            image.setImageIcon(account.getIcon());

            TextView name = (TextView) listItem.findViewById(R.id.textView_name);
            name.setText(account.getLabel());

            return listItem;
        }

        public void setTelecomManager(TelecomManager telecomManager) {
            this.telecomManager = telecomManager;
        }
    }


    /**
     * Handles secret codes to launch arbitrary activities.
     *
     * @param context    the context to use
     * @param secretCode the secret code without the "*#*#" prefix and "#*#*" suffix
     */
    public static void handleSecretCode(Context context, String secretCode) {
        try {
            context.getSystemService(TelephonyManager.class).sendDialerSpecialCode(secretCode);
        } catch (Exception e) {
            Timber.e(e);
        }
    }
}
