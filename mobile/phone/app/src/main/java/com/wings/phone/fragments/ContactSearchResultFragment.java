package com.wings.phone.fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;
import com.wings.phone.R;
import com.wings.phone.adapters.DetailClickListener;
import com.wings.phone.data.sync.ContactsManager;
import com.wings.phone.util.CircleTransform;
import com.wings.phone.util.ColorGenerator;
import com.wings.phone.util.Settings;
import com.wings.phone.util.StringUtils;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;

import java.util.List;


public class ContactSearchResultFragment extends Fragment {


    public static final String TAG = "ContactSearchResultFragment";
    private String contactId;
    private Contact contact;
    private LayoutInflater inflater;

    private final String[] GROUP_PROJECTION = new String[]{
            ContactsContract.PhoneLookup._ID,
            ContactsContract.PhoneLookup.DISPLAY_NAME,
            ContactsContract.PhoneLookup.PHOTO_URI
    };

    public void setContact(Contact contact) {
        this.contact = contact;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_contact_search_result, null);
        this.inflater = inflater;

        if (contactId == null) {
            //return view;
        }

        //Contact c = ContactsManager.getContact(getContext(), contactId);
        Contact c = contact;

        List<Number> numbers = ContactsManager.getContactNumbers(getContext(), c.getId().toString(),c.getRawId().toString(),1);
        if(numbers.size()< 1){
            numbers = ContactsManager.getContactNumbers(getContext(), c.getId().toString(),c.getRawId().toString(),0);
        }
        c.setNumbers(numbers);




        TextView nameView = ((TextView) view.findViewById(R.id.contact_name));
        nameView.setText(c.getName());
        ImageView callView = ((ImageView) view.findViewById(R.id.contact_call));
        callView.setVisibility(View.GONE);

        ImageView avatar = (ImageView) view.findViewById(R.id.contact_avatar);
        if(c.getPhotoUri()!=null){
            Picasso.get().load(c.getPhotoUri()).transform(new CircleTransform()).into(avatar);
        }else{
            TextDrawable drawable = TextDrawable.builder().buildRound(c.getName().substring(0,1), ColorGenerator.MATERIAL.getColor(c.getId()));
            avatar.setImageDrawable(drawable);

        }




        final LinearLayout cont = (LinearLayout) view.findViewById(R.id.contact_detail_container);
        //loadDetail(cont, c);
        cont.removeAllViews();
        for (Number n : c.getNumbers()) {
            addDetail(cont, n, n.getLabel());
        }

        return view;
    }


    private void addDetail(LinearLayout layout, Number number, String label) {
        if(label==null){
            label="";
        }
        View detail = inflater.inflate(R.layout.view_contact_detail_item, null);
        ((TextView) detail.findViewById(R.id.detail_call_label)).setText(label + " " + number.getRawNumber());

        TextView operatorNameView = detail.findViewById(R.id.detail_call_operator_name);
        String operatorName = Settings.getOperatorDefault();
        operatorName = StringUtils.capitalize(operatorName.toLowerCase(), new char[]{' '});

        if (operatorName.length() >= 10) {
            operatorNameView.setTextSize(11.0f);
        }

        operatorNameView.setText(operatorName);

        DetailClickListener listener = new DetailClickListener(number.getRawNumber(), getContext());
        detail.findViewById(R.id.detail_call_gsm).setOnClickListener(listener);

        if (Settings.isRegistered()) {

            if (number.isHasMimeFree()) {
                detail.findViewById(R.id.detail_call_free).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_free));
            }

            if (number.isHasMimeSecure() && Settings.getImSecure(getContext())) {
                detail.findViewById(R.id.detail_call_secure).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_secure));
            }

            if (number.isHasMimeWings()) {
                detail.findViewById(R.id.detail_call_wings).setOnClickListener(listener);
            } else {
                disableButton(detail.findViewById(R.id.detail_call_wings));
            }
            //detail.findViewById(R.id.detail_call_video).setOnClickListener(listener);
        } else {

            disableButton(detail.findViewById(R.id.detail_call_free));
            disableButton(detail.findViewById(R.id.detail_call_wings));
            disableButton(detail.findViewById(R.id.detail_call_secure));

        }
        //detail.findViewById(R.id.detail_call_sms).setOnClickListener(listener);
        //detail.findViewById(R.id.detail_call_other).setOnClickListener(listener);
        layout.addView(detail);

    }


    private void disableButton(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof ImageView) {
                    ((ImageView) child).setColorFilter(0xFFDDDDDD, PorterDuff.Mode.SRC_IN);
                }
            }
        }
    }
}
