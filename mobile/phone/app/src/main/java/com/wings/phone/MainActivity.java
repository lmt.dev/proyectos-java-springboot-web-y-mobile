package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.wings.phone.clients.validation.ApiClientFactory;
import com.wings.phone.fragments.ContactSearchResultFragment;
import com.wings.phone.fragments.DialpadFragment;
import com.wings.phone.fragments.SearchFragment;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.util.DialogUtils;
import com.wings.phone.util.NetworkUtils;
import com.wings.phone.util.Settings;
import com.wings.phone.util.UserUtil;
import com.wings.phone.util.WingsConst;
import com.wings.voip.model.UpdateCheckResponse;
import com.wings.voip.model.WingsBookResponse;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.secondNumber.RoamingFreeResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private View dialerButton;
    private View mainSearch;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    private int currentTab;
    private boolean dialerVisible = false;

    private final int MY_PERMISSIONS_REQUEST = 102;
    private Switch roamingFree;
    private Switch wingsBook;
    private boolean ignoreCheckedStatus = false;
    private boolean ignoreWingsBookCheckedStatus = false;

    /**
     * This is used when the user search for a contact
     * using the search bar, after click
     */
    private boolean filtered = false;

    private TabFragment tabFragment;
    private SearchFragment search;
    private int topMargin;
    private boolean finishActivityOnBack = false;
    private FragmentManager fragmentManager;
    private DialpadFragment diapadFragment;


    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Make to run your application only in portrait mode
        if (Settings.getPreference("config.google_dialer", "false", MainActivity.this).equals("true")) {

            Intent i = new Intent();
            try {
                i.setClassName("com.android.dialer", "com.android.dialer.app.DialtactsActivity");
                startActivity(i);
                finish();
            } catch (Exception e) {

                try {
                    i.setClassName("com.google.android.dialer", "com.android.dialer.main.impl.MainActivity");
                    startActivity(i);
                    finish();
                } catch (Exception ex) {
                }
            }
        }


        setContentView(R.layout.activity_main);
        //dialerButton = findViewById(R.id.dialer_button);
        setupFloatButtons();
        mainSearch = findViewById(R.id.main_search);
        mainSearch.bringToFront();

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);


        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.nav_settings:
                        //Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
                        //startActivity(intent);
                    case R.id.nav_update:
                        update();
                        break;
                    case R.id.nav_account:
                        showAccount();
                        break;
                    case R.id.nav_logout:
                        Settings.logout(MainActivity.this);
                        finish();
                        break;
                    case R.id.nav_help:
                        String url = Settings.getProvisioning("config.helpLink", "https://ayuda.wingsmobile.com");
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        break;
                    case R.id.nav_second_number:
                        Intent intent = new Intent(getApplicationContext(), DidsActivity.class);
                        startActivity(intent);
                        break;
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });

        if (getIntent().getType() != null && getIntent().getType().equals("vnd.android.cursor.dir/calls")) {
            currentTab = 1;
        }
        diapadFragment = new DialpadFragment();
        fragmentManager = getSupportFragmentManager();
        checkDefaultDialer();
        handleDialAction();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {

            Settings.init(MainActivity.this);

            setupFloatButtons();

/*            TabLayout tabLayout = findViewById(R.id.tabs);
            if (currentTab != 0 && tabLayout != null) {
                tabLayout.getTabAt(currentTab).select();
            }*/

            //enable secondnumber menu
            Menu menu = mNavigationView.getMenu();
            MenuItem sec = menu.findItem(R.id.nav_second_number);
            Boolean hasDid = Settings.getHasDid(MainActivity.this);
            if (hasDid != null) {
                sec.setEnabled(hasDid);
            }

            //enable roaming free menu
            final Boolean roamingFreeEnabled = Settings.getRoamingFree(getApplicationContext());
            roamingFree = (Switch) mNavigationView.getMenu().findItem(R.id.nav_roaming_free).getActionView();

            roamingFree.setOnCheckedChangeListener(null);
            roamingFree.setChecked(roamingFreeEnabled != null && roamingFreeEnabled);

            if (Settings.isRegistered()) {
                roamingFree.setEnabled(true);
                roamingFree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

                        if (ignoreCheckedStatus) {
                            ignoreCheckedStatus = false;
                            return;
                        }

                        if (isChecked) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle(getString(R.string.roaming_free))
                                    .setMessage(R.string.roaming_free_terms)
                                    .setPositiveButton(getString(R.string.Continue), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            enableDisableRoamingFree(true);
                                        }
                                    })
                                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ignoreCheckedStatus = true;
                                            roamingFree.setChecked(false);
                                        }
                                    })
                                    .show();
                        } else {
                            enableDisableRoamingFree(false);
                        }
                    }
                });
            } else {
                roamingFree.setEnabled(false);
            }

            //wings book
            wingsBook = (Switch) mNavigationView.getMenu().findItem(R.id.nav_wings_book).getActionView();
            Boolean wingsBookEnabled = Settings.getWingsBook(getApplicationContext());

            wingsBook.setOnCheckedChangeListener(null);
            wingsBook.setChecked(wingsBookEnabled != null && wingsBookEnabled);

            if (Settings.isRegistered()) {
                wingsBook.setEnabled(true);
                wingsBook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

                        if (ignoreWingsBookCheckedStatus) {
                            ignoreWingsBookCheckedStatus = false;
                            return;
                        }

                        if (isChecked) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle(getString(R.string.wings_book))
                                    .setMessage(R.string.wings_book_terms)
                                    .setPositiveButton(getString(R.string.Continue), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            enableDisableWingsBook(true);
                                        }
                                    })
                                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ignoreWingsBookCheckedStatus = true;
                                            wingsBook.setChecked(false);
                                        }
                                    })
                                    .show();
                        } else {
                            enableDisableWingsBook(false);
                        }

                    }
                });
            } else {
                wingsBook.setEnabled(false);
            }

            // hide sim contacts


            MenuItem register = mNavigationView.getMenu().findItem(R.id.nav_account);
            if (!Settings.isRegistered()) {
                register.setTitle(R.string.register);
            } else {
                register.setTitle(R.string.account);
                ((TextView) mNavigationView.getHeaderView(0).findViewById(R.id.user_info_register_title)).setText("");
            }
        } catch (Exception ex) {
            Timber.e(ex);
        }
    }

    private void setupFloatButtons() {
        if (!CallManager.getInstance(MainActivity.this).getCallList().isEmpty()) {

            findViewById(R.id.dialer_button).setVisibility(View.GONE);
            findViewById(R.id.return_to_call).setVisibility(View.VISIBLE);

            Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce_anim);
            findViewById(R.id.return_to_call).startAnimation(bounce);

            findViewById(R.id.return_to_call).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this, CallActivity.class);
                    startActivity(i);
                }
            });
        } else {
            findViewById(R.id.return_to_call).setVisibility(View.GONE);
            if (!dialerVisible) {
                //dialerButton.setVisibility(View.VISIBLE);
                Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce_anim);
                findViewById(R.id.return_to_call).startAnimation(bounce);
                findViewById(R.id.dialer_button).setVisibility(View.VISIBLE);
            }
        }
    }


    public void start() {
        Timber.d("ARN 1");
        if (tabFragment == null) {
            tabFragment = new TabFragment();
            //tabFragment.setCurrentTab(currentTab);
        }
        if (search == null) {
            search = new SearchFragment();
        }
        search.setDrawerLayout(mDrawerLayout);

        fragmentManager
                .beginTransaction()
                .replace(R.id.main_search, search, SearchFragment.TAG)
                .replace(R.id.main_content, tabFragment, TabFragment.TAG)
                .setCustomAnimations(R.animator.slide_down2_animation, R.animator.slide_up_animation)
                .commitNow();

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) findViewById(R.id.main_content).getLayoutParams();
        topMargin = params.topMargin;

        Map userInfo = UserUtil.getUserInfo(MainActivity.this);
        String name = userInfo.get("name").toString();
        //String pic = (userInfo.get("pic") != null) ? userInfo.get("pic").toString() : "";

        View headerLayout = mNavigationView.getHeaderView(0); // 0-index header
        /*AvatarView avatar = (AvatarView) headerLayout.findViewById(R.id.user_info_avatar);
        imlder.loadImage(avatar, pic, name);*/
        TextView username = headerLayout.findViewById(R.id.user_info_username);
        username.setText(name);
        TextView version = headerLayout.findViewById(R.id.user_info_version);
        version.setText(Settings.getVersionName());

        //enable debug options
        String allowDebug = Settings.getPreference("config.allow_debug", "false", MainActivity.this);
        if (allowDebug.equals("true")) {
            mNavigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            mNavigationView.getMenu().findItem(R.id.nav_update).setVisible(true);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkUpdate();
            }
        }, 1500);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleDialAction();
    }

    public void showDialer(View view) {
        showDialerWithNumber(null, null);
    }

    public void showDialerWithNumber(String number, FragmentManager fm) {

        if (diapadFragment == null) {
            diapadFragment = new DialpadFragment();
        }


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialer(v);
            }
        };
        diapadFragment.init(true, false, true, listener);

        if (number != null && !number.isEmpty()) {
            Bundle b = new Bundle();
            b.putString("number", number);
            diapadFragment.setArguments(b);
            mainSearch.setVisibility(View.GONE);
            finishActivityOnBack = true;
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            };
            diapadFragment.hideDialerButtonClickListener = clickListener;


        }


        try {
            hideMainSeach(diapadFragment);
        } catch (Exception ex) {
        }


        dialerVisible = true;
        //dialerButton.setVisibility(View.GONE);
        findViewById(R.id.dialer_button).setVisibility(View.GONE);


    }


    public void hideDialer(View view) {
        if (tabFragment == null) {
            tabFragment = new TabFragment();
        }

        //t.setCurrentTab(currentTab);
        getSupportFragmentManager()
                .beginTransaction()
                //.setCustomAnimations(R.animator.slide_down2_animation, R.animator.slide_down_animation)
                .replace(R.id.main_content, tabFragment, TabFragment.TAG)
                .show(search)
                .commit();
        //showMainSearch();
        dialerVisible = false;
        setupFloatButtons();
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) findViewById(R.id.main_content).getLayoutParams();
        params.topMargin = topMargin;
        //topMargin = 0;


    }

    private void hideMainSeach(DialpadFragment frag) {

        search.getFragmentManager()
                .beginTransaction()
                //.setCustomAnimations(R.animator.slide_up_animation,R.animator.slide_up2_animation)
                .hide(search)
                .replace(R.id.main_content, frag, DialpadFragment.TAG)
                .commitNow();


        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) findViewById(R.id.main_content).getLayoutParams();
        topMargin = params.topMargin;
        params.topMargin = 0;


    }

    private void showMainSearch() {

/*
        if(mainSearch.getVisibility()==View.GONE){
            mainSearch.setVisibility(View.VISIBLE);
        }
*/

        search.getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_down2_animation, R.animator.slide_down_animation)
                .show(search)
                .commit();

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) findViewById(R.id.main_content).getLayoutParams();
        params.topMargin = 140;

        //start();


    }

    public void showContactFiltered(Contact contact) {
        dialerButton = findViewById(R.id.dialer_button);

        try {
            TabFragment fragmentTab = (TabFragment) fragmentManager.findFragmentByTag(TabFragment.TAG);
            currentTab = fragmentTab.getCurrentTab();
        } catch (Exception e) {
        }

        dialerVisible = false;
        //dialerButton.setVisibility(View.VISIBLE);
        findViewById(R.id.dialer_button).setVisibility(View.VISIBLE);

        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce_anim);
        findViewById(R.id.return_to_call).startAnimation(bounce);


        filtered = true;

        ContactSearchResultFragment frag = new ContactSearchResultFragment();
        frag.setContact(contact);

        fragmentManager
                .beginTransaction()
                //.setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                .replace(R.id.main_content, frag, ContactSearchResultFragment.TAG)
                .commitNow();

        ((FloatingActionButton) dialerButton).setImageResource(R.drawable.ic_arrow_back_black_24dp);
        dialerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(finishActivityOnBack){
            finish();
            return;
        }

        if (dialerVisible) {

            hideDialer(null);

        } else if (filtered) {

            //restore dialer
            ((FloatingActionButton) dialerButton).setImageResource(R.drawable.ic_dialpad_black_18dp);
            dialerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialer(null);

                }
            });

            filtered = false;
            hideDialer(null);
            //currentTab = 0;

        } else {
            super.onBackPressed();
        }
    }

    private void checkPermissions() {

        //start();
        List<String> toRequest = new ArrayList<>();
        String[] permissions = WingsConst.permissions;
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                toRequest.add(permission);
            }
        }

        if (!toRequest.isEmpty()) {
            String[] permArray = new String[toRequest.size()];
            ActivityCompat.requestPermissions(MainActivity.this, toRequest.toArray(permArray), MY_PERMISSIONS_REQUEST);

            //finish();
        } else {
            start();
        }
    }


    private void checkDefaultDialer() {

        TelecomManager tm = (TelecomManager) getSystemService(TELECOM_SERVICE);
        String pkg = tm.getDefaultDialerPackage();
        if (pkg == null || !pkg.equals("com.wings.phone")) {
            Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
            intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, MainActivity.this.getPackageName());
            startActivity(intent);
        }


        checkPermissions();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
        Settings.init(MainActivity.this);
        start();
    }


    private void update() {

        if (NetworkUtils.isOnline(MainActivity.this)) {
            Settings.updateProvisioning(getApplicationContext());
        } else {
            NetworkUtils.showNoInternetDialog(MainActivity.this);
        }

    }


    private void showAccount() {
        Intent i = new Intent(MainActivity.this, AccountActivity.class);
        //i.putExtra("url", getString(R.string.privacy_policy_sales_url));
        startActivity(i);
    }


    private void handleDialAction() {
        FragmentManager fm = fragmentManager;
        Intent intent = getIntent();
        if (intent != null && intent.getAction() != null && (intent.getAction().equals("android.intent.action.DIAL") || intent.getAction().equals("android.intent.action.VIEW"))) {
            Uri data = intent.getData();
            if (data != null) {
                String number = data.getEncodedSchemeSpecificPart();
                if (number != null) {
                    showDialerWithNumber(number, fm);
                    finishActivityOnBack = true;
                }
            } else {
                currentTab = 1;
            }
        }
    }

    private void enableDisableRoamingFree(final boolean enable) {

        if (NetworkUtils.getConnectivityStatus(getApplicationContext()) == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED) {
            Toast.makeText(getApplicationContext(), R.string.no_internet_desc, Toast.LENGTH_LONG).show();
            return;
        }

        String title = getString(R.string.roaming_free);
        String message;
        if (enable) {
            message = getString(R.string.roaming_free_enable);
        } else {
            message = getString(R.string.roaming_free_disable);
        }

        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, title, message);

        int mcc = -1;
        int mnc = -1;
        try {
            //get current operator
            TelecomManager telecomManager = (TelecomManager) getApplicationContext().getSystemService(Context.TELECOM_SERVICE);
            final PhoneAccountHandle handle = telecomManager.getDefaultOutgoingPhoneAccount("tel");
            String subscriptionId = handle.getId();
            List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
            if (subscriptionInfos != null) {
                for (int i = 0; i < subscriptionInfos.size(); i++) {
                    SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                    if (lsuSubscriptionInfo.getIccId().equals(subscriptionId)) {
                        mcc = lsuSubscriptionInfo.getMcc();
                        mnc = lsuSubscriptionInfo.getMnc();
                    }
                }
            }
        } catch (SecurityException ex) {
            Toast.makeText(getApplicationContext(), R.string.error_generic, Toast.LENGTH_LONG).show();
        }

        //if success on getting data
        if (mnc != -1 && mcc != -1) {

            String apiUrl = getString(R.string.settings_api_url);
            String appId = getString(R.string.settings_api_app_id);
            String token = getString(R.string.settings_api_app_token);

            Call<RoamingFreeResponse> call = ApiClientFactory.getClient(apiUrl).postActionRoaming(appId, token, Settings.getRegisteredNumber(), String.valueOf(mcc), String.valueOf(mnc), enable);
            try {
                call.enqueue(new Callback<RoamingFreeResponse>() {
                    @Override
                    public void onResponse(Call<RoamingFreeResponse> call, Response<RoamingFreeResponse> response) {

                        if (response.body() != null) {

                            if (response.body().getMessage() != null) {
                                String message = response.body().getMessage();
                                try {
                                    int id = getResources().getIdentifier("com.wings.phone:string/" + response.body().getMessage(), null, null);
                                    message = getString(id);
                                } catch (Exception e) {
                                    Timber.e(e);
                                }
                                dialog.dismiss();
                                ignoreCheckedStatus = true;
                                roamingFree.setChecked(false);
                                DialogUtils.showInfoDialog(MainActivity.this, getString(R.string.roaming_free), message);

                            } else {

                                final boolean responseBoolean = response.body().getEnabled();
                                String dialString = response.body().getDialString();
                                TelephonyManager manager = (TelephonyManager) MainActivity.this.getSystemService(TELEPHONY_SERVICE);
                                manager.sendUssdRequest(dialString, new TelephonyManager.UssdResponseCallback() {
                                    @Override
                                    public void onReceiveUssdResponse(TelephonyManager telephonyManager, String request, CharSequence response) {
                                        super.onReceiveUssdResponse(telephonyManager, request, response);
                                        Settings.setRoamingFree(getApplicationContext(), responseBoolean);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onReceiveUssdResponseFailed(TelephonyManager telephonyManager, String request, int failureCode) {
                                        super.onReceiveUssdResponseFailed(telephonyManager, request, failureCode);
                                        Settings.setRoamingFree(getApplicationContext(), !responseBoolean);
                                        Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                        ignoreCheckedStatus = true;
                                        roamingFree.setChecked(false);
                                    }
                                }, new Handler());

                            }

                        } else {
                            Timber.e("Error on response enableRoaming");
                            Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RoamingFreeResponse> call, Throwable t) {
                        Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                        Timber.e(t);
                    }
                });
            } catch (SecurityException ex) {
                Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                Timber.e(ex);
            }

        } else {
            Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
        }
    }


    private void enableDisableWingsBook(final boolean enable) {

        if (NetworkUtils.getConnectivityStatus(getApplicationContext()) == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED) {
            Toast.makeText(getApplicationContext(), R.string.no_internet_desc, Toast.LENGTH_LONG).show();
            return;
        }

        String title = getString(R.string.wings_book);
        String message;
        if (enable) {
            message = getString(R.string.wings_book_enable);
        } else {
            message = getString(R.string.wings_book_disable);
        }

        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, title, message);

        int mcc = -1;
        int mnc = -1;
        try {
            //get current operator
            TelecomManager telecomManager = (TelecomManager) getApplicationContext().getSystemService(Context.TELECOM_SERVICE);
            final PhoneAccountHandle handle = telecomManager.getDefaultOutgoingPhoneAccount("tel");
            String subscriptionId = handle.getId();
            List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
            if (subscriptionInfos != null) {
                for (int i = 0; i < subscriptionInfos.size(); i++) {
                    SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                    if (lsuSubscriptionInfo.getIccId().equals(subscriptionId)) {
                        mcc = lsuSubscriptionInfo.getMcc();
                        mnc = lsuSubscriptionInfo.getMnc();
                    }
                }
            }
        } catch (SecurityException ex) {
            Toast.makeText(getApplicationContext(), R.string.error_generic, Toast.LENGTH_LONG).show();
        }

        //if success on getting data
        if (mnc != -1 && mcc != -1) {

            String apiUrl = getString(R.string.settings_api_url);
            String appId = getString(R.string.settings_api_app_id);
            String token = getString(R.string.settings_api_app_token);

            Call<WingsBookResponse> call = ApiClientFactory.getClient(apiUrl).postActionWingsBook(appId, token, Settings.getRegisteredNumber(), String.valueOf(mcc), String.valueOf(mnc), enable);
            try {
                call.enqueue(new Callback<WingsBookResponse>() {
                    @Override
                    public void onResponse(Call<WingsBookResponse> call, Response<WingsBookResponse> response) {

                        if (response.body() != null) {

                            if (response.body().getMessage() != null) {
                                String message = response.body().getMessage();
                                try {
                                    int id = getResources().getIdentifier("com.wings.phone:string/" + response.body().getMessage(), null, null);
                                    message = getString(id);
                                } catch (Exception e) {
                                    Timber.e(e);
                                }
                                dialog.dismiss();
                                ignoreWingsBookCheckedStatus = true;
                                wingsBook.setChecked(false);
                                DialogUtils.showInfoDialog(MainActivity.this, getString(R.string.wings_book), message);

                            } else {

                                final boolean responseBoolean = response.body().getEnabled();
                                String dialString = response.body().getDialString();
                                TelephonyManager manager = (TelephonyManager) MainActivity.this.getSystemService(TELEPHONY_SERVICE);
                                manager.sendUssdRequest(dialString, new TelephonyManager.UssdResponseCallback() {
                                    @Override
                                    public void onReceiveUssdResponse(TelephonyManager telephonyManager, String request, CharSequence response) {
                                        super.onReceiveUssdResponse(telephonyManager, request, response);
                                        Settings.setWingsBook(getApplicationContext(), responseBoolean);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onReceiveUssdResponseFailed(TelephonyManager telephonyManager, String request, int failureCode) {
                                        super.onReceiveUssdResponseFailed(telephonyManager, request, failureCode);
                                        Settings.setWingsBook(getApplicationContext(), !responseBoolean);
                                        Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                        ignoreWingsBookCheckedStatus = true;
                                        wingsBook.setChecked(false);
                                    }
                                }, new Handler());
                            }

                        } else {
                            Timber.e("Error on response enableWingsBook");
                            Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<WingsBookResponse> call, Throwable t) {
                        Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                        Timber.e(t);
                    }
                });
            } catch (SecurityException ex) {
                Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
                Timber.e(ex);
            }

        } else {
            Toast.makeText(MainActivity.this, R.string.error_generic, Toast.LENGTH_LONG).show();
        }
    }


    private void checkUpdate() {

        if (NetworkUtils.getConnectivityStatus(getApplicationContext()) == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED) {
            return;
        }

        String version = BuildConfig.VERSION_NAME;
        String model = Build.MODEL;
        String apiUrl = getString(R.string.settings_api_url);

        Call<UpdateCheckResponse> call = ApiClientFactory.getClient(apiUrl).checkUpdate(version, model);
        try {
            call.enqueue(new Callback<UpdateCheckResponse>() {
                @Override
                public void onResponse(Call<UpdateCheckResponse> call, Response<UpdateCheckResponse> response) {
                    if (response.body() != null && response.body().isUpdateAvailable()) {
                        showUpdateDialog(response.body());
                    }
                }

                @Override
                public void onFailure(Call<UpdateCheckResponse> call, Throwable t) {
                    Timber.e(t);
                }
            });
        } catch (Exception e) {
            Timber.e(e);
        }
    }


    private void showUpdateDialog(final UpdateCheckResponse response) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.update_available))
                .setMessage(getString(R.string.update_available_description))
                .setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openPlayActivity(response);
                    }
                })
                .setNegativeButton(getString(R.string.later), null)
                .show();
    }

    private void openPlayActivity(UpdateCheckResponse response) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.getMarketLink())));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.getUrlLink())));
        }
    }


}
