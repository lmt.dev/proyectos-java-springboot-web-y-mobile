package com.wings.phone;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.wings.phone.fragments.tabs.ContactsFragment;


public class AddCallActivity extends AppCompatActivity {


    private ContactsFragment contactFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_search, menu);

        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) search.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactFragment.setFilterString(newText);
                return false;
            }
        });

        return true;
    }





    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_add_call);

        if(contactFragment==null)
        contactFragment = new ContactsFragment();

        ((TextView) findViewById(R.id.activity_title)).setText(R.string.add_new_call);

        ContactsFragment frag = contactFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_up_animation, R.animator.slide_down_animation)
                .replace(R.id.add_call_main_content, frag, ContactsFragment.TAG)
                .commit();
    }

}
