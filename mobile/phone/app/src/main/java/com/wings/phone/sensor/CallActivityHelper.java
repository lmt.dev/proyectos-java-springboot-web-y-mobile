package com.wings.phone.sensor;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.PowerManager;
import android.view.WindowManager;

import com.wings.phone.CallActivity;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.telecom.CallWrapper;
import com.wings.phone.util.NotificationsHelper;

import java.util.List;
import java.util.Locale;

import timber.log.Timber;


public class CallActivityHelper {

    private Context context;
    private AudioManager audioManager;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private SensorEventListener sensorEventListener;
    private ToneGenerator toneGenerator;
    private NotificationManager notifyManager;
    private MediaPlayer mp;

    public CallActivityHelper(Context context) {
        this.context = context;
        notifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        setupSensors();
    }

    public void onCreate() {
        ((CallActivity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    public void onResume() {
        notifyManager.cancelAll();
        //wakeDevice();
        registerProximitySensor();
    }

    public void onPause() {

        CallManager callManager = CallManager.getInstance(context);
        List<CallWrapper> callList = callManager.getCallList();
        if (!callList.isEmpty()) {
            for (CallWrapper call : callList) {
                String number = call.getCallBuddy().getFormattedNumber();
                Notification notif = NotificationsHelper.getCurrentNotif(context, call.getCallBuddy(), number);
                notifyManager.notify(call.getId(), notif);
            }
        }

        //removeWakeDevice();
        unregisterProximitySensor();

    }

    public void onDestroy() {

        try {
            stopRinging();
        } catch (Exception e) {
        }

        List<CallWrapper> callList = CallManager.getInstance(context).getCallList();
        if (callList.isEmpty()) {
            notifyManager.cancelAll();
        }

        ((CallActivity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        //stopExternalRinging();
        turnOnScreen();
    }

    public void cancelNotifications() {
        List<CallWrapper> callList = CallManager.getInstance(context).getCallList();
        if (callList.isEmpty()) {
            notifyManager.cancelAll();
        } else {
            for (CallWrapper call : callList) {
                notifyManager.cancel(call.getId());
            }
        }
    }

    private void setupSensors() {

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        /*if (audioManager.isSpeakerphoneOn()) {
            audioManager.setSpeakerphoneOn(false);
        }
        audioManager.setMicrophoneMute(false);*/

        powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                    if (event.values[0] == 0) {
                        //near
                        turnOffScreen();
                    } else {
                        //far
                        turnOnScreen();
                    }
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
    }

    public void turnOnScreen() {
        if (wakeLock != null) {
            wakeLock.release();
        }

        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wings-phone:lock");
        wakeLock.acquire(1500);
    }

    public void turnOffScreen() {
        if (wakeLock != null) {
            wakeLock.release();
        }
        wakeLock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "wings-phone:lock");
        wakeLock.acquire();
    }

    public void registerProximitySensor() {
        sensorManager.registerListener(sensorEventListener, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterProximitySensor() {
        sensorManager.flush(sensorEventListener);
        sensorManager.unregisterListener(sensorEventListener, proximitySensor);
    }

    @Deprecated
    public void wakeDevice() {

        Timber.e("WAKE ADD");
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();


        ((CallActivity) context).runOnUiThread(new Runnable() {
            public void run() {
                ((CallActivity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                ((CallActivity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        });

    }

    @Deprecated
    public void removeWakeDevice() {

        Timber.e("WAKE REMOVE");
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.reenableKeyguard();

        ((CallActivity) context).runOnUiThread(new Runnable() {
            public void run() {
                ((CallActivity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                ((CallActivity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        });

        /*DevicePolicyManager mgr = (DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        mgr.lockNow();*/

    }


    public void startExternalRinging() {
        Intent startIntent = new Intent(context, RingtonePlayingService.class);
        context.startService(startIntent);
    }

    public void stopExternalRinging() {
        Intent stopIntent = new Intent(context, RingtonePlayingService.class);
        context.stopService(stopIntent);
    }

    /*Tone generator methods */
    @Deprecated
    public void startRinging() {
        if (toneGenerator == null) {
            toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
            toneGenerator.startTone(ToneGenerator.TONE_SUP_RINGTONE);
        }
    }

    public void startHolding() {
        toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
        toneGenerator.startTone(ToneGenerator.TONE_SUP_CALL_WAITING);
    }

    @Deprecated
    public void startBusy() {
        toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
        toneGenerator.startTone(ToneGenerator.TONE_SUP_BUSY);
    }

    public void startDisconnect() {
        toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
        toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_NETWORK_LITE);
    }

    public void stopRinging() {
        try {
            if (toneGenerator != null) {
                toneGenerator.stopTone();
                toneGenerator.release();
                toneGenerator = null;
            }
        } catch (Exception e) {
        }
    }

    @Deprecated
    public boolean changeAudio() {

        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
        audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, maxVolume, 0);
        boolean speaker = audioManager.isSpeakerphoneOn();
        audioManager.setSpeakerphoneOn(!speaker);

        return speaker;
    }

    public boolean toogleSpeakers() {

        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
        audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, maxVolume, 0);
        boolean speaker = audioManager.isSpeakerphoneOn();
        audioManager.setSpeakerphoneOn(!speaker);

        return !speaker;
    }

    public boolean toogleMute() {

        boolean muted = audioManager.isMicrophoneMute();
        audioManager.setMicrophoneMute(!muted);

        return !muted;
    }

    public boolean isMuted() {
        return audioManager.isMicrophoneMute();
    }

    public boolean isOnSpeakers() {
        return audioManager.isSpeakerphoneOn();
    }

    public void resetAudio() {
        audioManager.setSpeakerphoneOn(false);
        audioManager.setMicrophoneMute(false);
    }

    public void playSecureMessage() {

        try {
            String lang = Locale.getDefault().toString();
            String name = "secure_en";
            if (lang.toLowerCase().contains("es")) {
                name = "secure_es";
            } else if (lang.toLowerCase().contains("it")) {
                name = "secure_it";
            }

            int resID = context.getResources().getIdentifier(name, "raw", context.getPackageName());
            mp = MediaPlayer.create(context, resID);
            mp.start();

            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });

        } catch (Exception ex) {
        }

    }
}
