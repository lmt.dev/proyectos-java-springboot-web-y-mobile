package com.wings.phone;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;

import androidx.core.content.ContextCompat;

import com.crashlytics.android.Crashlytics;
import com.wings.phone.telecom.CallManager;
import com.wings.phone.util.ActivUtils;
import com.wings.phone.util.Settings;

import timber.log.Timber;

import static timber.log.Timber.DebugTree;

public class PhoneApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        if (ContextCompat.checkSelfPermission(PhoneApp.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            Settings.init(PhoneApp.this);
        }

        ActivUtils.enableGsmCallActivity(PhoneApp.this);
        CallManager.getInstance(PhoneApp.this);
        Crashlytics.setUserIdentifier(Settings.getUdid());

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                Timber.e(e);
                Crashlytics.logException(e);
            }
        });

    }

    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.DebugTree {
        @Override
        public void i(String message, Object... args) {
            Crashlytics.log(message);
        }

        @Override
        public void i(Throwable t, String message, Object... args) {
            Crashlytics.log(message);
            Crashlytics.logException(t);
        }

        @Override
        public void e(String message, Object... args) {
            Crashlytics.log(message);
        }

        @Override
        public void e(Throwable t, String message, Object... args) {
            Crashlytics.log(message);
            Crashlytics.logException(t);
        }
    }
}
