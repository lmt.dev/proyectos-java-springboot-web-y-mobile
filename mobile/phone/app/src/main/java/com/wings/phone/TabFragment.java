package com.wings.phone;

import android.content.Context;
import android.graphics.ColorFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.wings.phone.adapters.ViewPagerAdapter;
import com.wings.phone.fragments.tabs.ContactsFragment;
import com.wings.phone.fragments.tabs.FavoritesFragment;
import com.wings.phone.fragments.tabs.HistoryFragment;

public class TabFragment extends Fragment {
    public static final String TAG = "TabFragment";

    private int currentTab = 0;
    private TabLayout tabLayout;
    private int[] imageResId = {
            R.drawable.ic_star,
            R.drawable.ic_history_black_24dp,
            R.drawable.ic_people_black_24dp
    };
    private ViewPagerAdapter adapter;
    private ViewPager2 viewPager2;
    private Context context;
    private FavoritesFragment fav;
    private HistoryFragment his;
    private ContactsFragment cont;

    public TabFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        adapter = new ViewPagerAdapter(getChildFragmentManager(),getLifecycle());


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab, null);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager2 = (ViewPager2) view.findViewById(R.id.viewpager);
        //viewPager2.setUserInputEnabled(false);
        //viewPager2.setCurrentItem(0, true);
        viewPager2.setOffscreenPageLimit(1);
        viewPager2.setSaveEnabled(false);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTab = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager2.setAdapter(adapter);
        if(currentTab!=0){
            viewPager2.setCurrentItem(currentTab,false);
        }




        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setIcon(imageResId[position]);

            }
        }).attach();





        return view;
    }
    int getCurrentTab() {
        return currentTab;
    }
    public void setCurrentTab(int currentTab) {
        this.currentTab = currentTab;
        tabLayout.setScrollPosition(currentTab,0f,true);
        viewPager2.setCurrentItem(currentTab,false);
    }


}
