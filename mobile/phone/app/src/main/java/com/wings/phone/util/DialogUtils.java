package com.wings.phone.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogUtils {


    public static void showInfoDialog(Context context, String title, String message) {

        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .show();
    }


    public static void showImeiDialog(Context context) {

        Settings.loadImeis(context);

        String imei1 = Settings.getImei1() != null ? Settings.getImei1() : "";
        String imei2 = Settings.getImei2() != null ? Settings.getImei2() : "";
        final String message = imei1 + "\n\n" + imei2;

        new AlertDialog.Builder(context)
                .setTitle("IMEI")
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .show();
    }

}
