package com.wings.phone.data.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.telephony.PhoneNumberUtils;

import com.wings.phone.R;
import com.wings.phone.util.WingsConst;
import com.wings.voip.model.contact.Contact;
import com.wings.voip.model.contact.Number;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import timber.log.Timber;



public class ContactsManager {


    /**
     * Get a List of all contacts with phone numbers
     * return empty if there's no contacts
     *
     *
     * @param context
     * @return
     */
    public static List<Contact> getAllContacts(Context context) {

        return getAllContactsCustom(context,ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null);
    }

        /**
         * Get a Custom List of all contacts with phone numbers
         * return empty if there's no contacts
         *
         *
         * @param context
         * @param contentUri
         * @param projection
         * @param selection
         * @param args
         * @param order
         * @return
         */
    public static List<Contact> getAllContactsCustom(Context context, Uri contentUri, String[] projection, String selection, String[] args, String order) {

        List<Contact> contacts = new LinkedList<>();

        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(contentUri,
                projection,
                selection,
                args,
                order);

        if (cur.getCount() <= 0) {
            return contacts;
        }

        cur.move(-1);
        int idColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup._ID);
        int idRawColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID);
        int nameColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
        int photoColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI);
        int lastUpdatedColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);
        int sdnColumnIndex = cur.getColumnIndex("is_sdn_contact");
        int inVisibleGroupColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.IN_VISIBLE_GROUP);
        while (cur.moveToNext()) {

            /*int isSdn = cur.getInt(sdnColumnIndex);
            if (isSdn != 0) {
                continue;
            }*/

            Contact contact = new Contact();

            int contactId = cur.getInt(idColumnIndex);
            int contactRawId = cur.getInt(idRawColumnIndex);
            String displayName = cur.getString(nameColumnIndex);
            String photoUri = cur.getString(photoColumnIndex);
            String lastUpdate = cur.getString(lastUpdatedColumnIndex);
            int inVisibleGroup = cur.getInt(inVisibleGroupColumnIndex);

            if(inVisibleGroup==0){
                continue;
            }
            contact.setId(contactId);
            contact.setRawId(contactRawId);
            contact.setName(displayName);
            contact.setPhotoUri(photoUri);
            contact.setLastUpdated(lastUpdate);

            List<Number> numbers = getContactNumbers(context, contact.getId().toString(),contact.getRawId().toString(),-1);
            if (numbers.isEmpty()) {
                continue;
            }

            contact.setNumbers(numbers);
            contacts.add(contact);
        }
        cur.close();

        return contacts;
    }


    /**
     * Get a one contact with phone numbers with contact id
     * return empty if there's no contacts
     *
     * @param context
     * @return
     */
    public static Contact getContact(Context context, String contactId) {

        Contact contact = null;
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null,
                ContactsContract.Contacts._ID + " = ?",
                new String[]{contactId},
                null);

        if (cur.getCount() <= 0) {
            return contact;
        }


        cur.move(-1);
        int idColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup._ID);
        int rawIdColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID);
        int nameColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
        int photoColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI);
        int lastUpdatedColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);
        int sdnColumnIndex = cur.getColumnIndex("is_sdn_contact");

        while (cur.moveToNext()) {

            int isSdn = cur.getInt(sdnColumnIndex);
            if (isSdn != 0) {
                //continue;
            }

            contact = new Contact();

            int intcontactId = cur.getInt(idColumnIndex);
            int intcontactRawId = cur.getInt(rawIdColumnIndex);
            String displayName = cur.getString(nameColumnIndex);
            String photoUri = cur.getString(photoColumnIndex);
            String lastUpdate = cur.getString(lastUpdatedColumnIndex);

            contact.setId(intcontactId);
            contact.setRawId(intcontactRawId);
            contact.setName(displayName);
            contact.setPhotoUri(photoUri);
            contact.setLastUpdated(lastUpdate);

            List<Number> numbers = getContactNumbers(context, contact.getId().toString(),contact.getRawId().toString());
            contact.setNumbers(numbers);

        }
        cur.close();

        return contact;
    }

    public static String getContactLookpu(Context context, String cID){
        String[] prjection = new String[]{
                ContactsContract.Contacts.LOOKUP_KEY
        };
        String selection = ContactsContract.Contacts._ID + " == ?";
        String[] args = new String[]{
                cID
        };
        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                prjection,
                selection,
                args,
                ContactsContract.PhoneLookup.DISPLAY_NAME);
        String result = "";
        while (cursor != null && cursor.moveToNext()) {
            result = cursor.getString(0);
        }
        return result;
    }


    /**
     * getAll contacts numbers
     *
     * @param context
     * @param contactId
     * @return
     */
    public static List<Number> getContactNumbers(Context context, String contactId, String nameRawContactId) {
        return getContactNumbers(context,contactId,nameRawContactId,0);
    }


    /**
     * getAll contacts numbers
     *
     * @param context
     * @param contactId
     * @param nameRawContactId
     * @param wingsAccountOnly
     * @return
     */
    public static List<Number> getContactNumbers(Context context, String contactId, String nameRawContactId, int wingsAccountOnly) {

        List<Number> tempList = new ArrayList<>();

/*        String accountList = "";
        Account[] accs = AccountManager.get(context).getAccounts();
        for (Account acc : accs) {
            accountList += acc.type + ",";
        }*/


        String[] values = new String[]{contactId,nameRawContactId,"com.google", "Local Phone Account", "USIM Account","com.wings.profile.account"};

        String selection = "( "+ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? OR "+ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID + " = ? ) " +
                "AND ( " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? )";



        if(wingsAccountOnly==1){
            selection = "( "+ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? OR "+ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID + " = ? ) " +
                    "AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?";
            values = new String[]{contactId,nameRawContactId,"com.wings.profile.account"};
        }else if(wingsAccountOnly==-1){
            selection = "( "+ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? OR "+ContactsContract.CommonDataKinds.Phone.NAME_RAW_CONTACT_ID + " = ? ) " +
                    "AND ( " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                    ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? OR " +
                    ContactsContract.RawContacts.ACCOUNT_TYPE + " = ? )" ;
            values = new String[]{contactId,nameRawContactId,"com.google", "Local Phone Account", "USIM Account"};
        }else{

        }


        Cursor cursor = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,//projection,
                selection,
                values,
                null);

        int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int accountTypeColumnIndex = cursor.getColumnIndex("account_type");
        int mimeTypeColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.MIMETYPE);
        int sdnColumnIndex = cursor.getColumnIndex("is_sdn_contact");
        //int isVisibleGroupColumnIndex = cursor.getColumnIndex("in_visible_group");
        int labelTypeIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
        int labelTypeTextIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA3);

        cursor.move(-1);
        while (cursor.moveToNext()) {

            try {
                String raw = cursor.getString(numberColumnIndex);
                String accountType = cursor.getString(accountTypeColumnIndex);
                String mimeType = cursor.getString(mimeTypeColumnIndex);
                //int sdn = cursor.getInt(sdnColumnIndex);
                int type = cursor.getInt(labelTypeIndex);
                int vivible = 1;//cursor.getInt(isVisibleGroupColumnIndex);

                /*if (accountList.contains(accountType) || !mimeType.equals("vnd.android.cursor.item/phone_v2")) {
                    continue;
                }*/

                if (vivible != 1) {
                    continue;
                }
                //skip sim numbers
                /*if (sdn == 1) {
                    continue;
                }*/

                Number n = new Number();
                n.setInPhoneAccount(true);
                n.setRawNumber(raw);
                try {
                    n.setNumber(PhoneNumberUtils.formatNumberToE164(raw, com.wings.phone.util.Settings.getCountry()).replaceAll("\\+", ""));
                } catch (Exception e) {
                }

                try {
                    n.setFormattedNumber(PhoneNumberUtils.formatNumber(raw, com.wings.phone.util.Settings.getCountry()));
                } catch (Exception e) {
                }

                //mime types
                if (n.getNumber() != null) {

                    String prefix = "";
                    String data1 = "wings://" + prefix + n.getNumber() + "@sip.wingsmobile.net";
                    Cursor dataCursor = context.getContentResolver().query(
                            Data.CONTENT_URI,
                            null,//projection,
                            "mimetype = ? and data1 = ?",
                            new String[]{WingsConst.MIMETYPE_FREE, data1},
                            null);
                    n.setHasMimeFree(dataCursor.getCount() != 0);

                    prefix = "sec";
                    data1 = "wings://" + prefix + n.getNumber() + "@sip.wingsmobile.net";
                    dataCursor = context.getContentResolver().query(
                            Data.CONTENT_URI,
                            null,//projection,
                            "mimetype = ? and data1 = ?",
                            new String[]{WingsConst.MIMETYPE_SECURE, data1},
                            null);
                    n.setHasMimeSecure(dataCursor.getCount() != 0);

                    prefix = "int";
                    data1 = "wings://" + prefix + n.getNumber() + "@sip.wingsmobile.net";
                    dataCursor = context.getContentResolver().query(
                            Data.CONTENT_URI,
                            null,//projection,
                            "mimetype = ? and data1 = ?",
                            new String[]{WingsConst.MIMETYPE_WINGS, data1},
                            null);
                    n.setHasMimeWings(dataCursor.getCount() != 0);

                }

                //wings account
                if (accountType.equals(WingsConst.ACCOUNT_TYPE) && mimeType.equals("vnd.android.cursor.item/phone_v2")) {
                    n.setInWingsAccount(true);
                }

                //label
                String label;
                if (type == 0) {
                    label = cursor.getString(labelTypeTextIndex);
                } else {
                    label = ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), type, "").toString();
                }
                n.setLabel(label);

                tempList.add(n);

            } catch (Exception e) {
                Timber.e(e, "Error on getContactNumbers");
            }
        }
        cursor.close();

        //tempList = mergeNumbers(tempList);

        return tempList;
    }

    /**
     * Create all free mime types for contact
     *
     * @param context
     * @param contact
     */
    public static void createAllMimeTypes(Context context, Contact contact) {
        ContactsManager.createMimeType(context, contact, WingsConst.MIMETYPE_FREE);
        ContactsManager.createMimeType(context, contact, WingsConst.MIMETYPE_SECURE);
        ContactsManager.createMimeType(context, contact, WingsConst.MIMETYPE_WINGS);
    }




    /**
     * Create particular mime type for contact
     *
     * @param context
     * @param contact
     * @param mimeType
     */
    public static void createMimeType(Context context, Contact contact, String mimeType) {

        //if has no numbers return
        if (contact.getNumbers().isEmpty()) {
            return;
        }


        //set label and prefix
        String label = "";
        String prefix = "";
        switch (mimeType) {
            case WingsConst.MIMETYPE_FREE:
                label = context.getString(R.string.call_detail_free);
                prefix = "";
                break;
            case WingsConst.MIMETYPE_SECURE:
                label = context.getString(R.string.call_detail_secure);
                prefix = "sec";
                break;
            case WingsConst.MIMETYPE_WINGS:
                label = context.getString(R.string.call_detail_wings);
                prefix = "int";
                break;
        }


        for (Number number : contact.getNumbers()) {

            if (number.getNumber().length() < 5 || number.getNumber().contains("*")) {
                continue;
            }
            //insert only the ones
            if (mimeType.equals(WingsConst.MIMETYPE_FREE) && !number.isFree()) {
                continue;
            }

            if (mimeType.equals(WingsConst.MIMETYPE_SECURE) && !number.isSecure()) {
                continue;
            }



            /*//prevent duplicates
            if (mimeType.equals(WingsConst.MIMETYPE_FREE) && number.isHasMimeFree()) {
                continue;
            }
            if (mimeType.equals(WingsConst.MIMETYPE_SECURE) && number.isHasMimeSecure()) {
                continue;
            }
            if (mimeType.equals(WingsConst.MIMETYPE_WINGS) && number.isHasMimeWings()) {
                continue;
            }*/

            if (number.getFormattedNumber() == null) {
                continue;
            }

            String intNumber = PhoneNumberUtils.formatNumber(number.getNumber(), com.wings.phone.util.Settings.getCountry());
            if (intNumber == null) {
                intNumber = number.getNumber();
            }


            ArrayList<ContentProviderOperation> ops = new ArrayList<>();


            String selection = ContactsContract.Data.NAME_RAW_CONTACT_ID + " = ? AND " +
                    ContactsContract.RawContacts.ACCOUNT_TYPE +" = ?";
            String[] values = new String[]{String.valueOf(contact.getRawId()), WingsConst.ACCOUNT_TYPE};


            int rawContactId = getExistentRawContactId(context,selection,values, ContactsContract.Data.CONTENT_URI);
            if(rawContactId==0) {
                ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, WingsConst.ACCOUNT_TYPE)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, WingsConst.ACCOUNT_NAME_SHORT)
                        .build());

                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName())
                        .build());
                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber())
                        .build());

                ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.RawContacts.Data.MIMETYPE, mimeType)
                        .withValue(ContactsContract.RawContacts.Data.DATA1, "wings://" + prefix + number.getNumber() + "@sip.wingsmobile.net")
                        .withValue(ContactsContract.RawContacts.Data.DATA2, number.getNumber())
                        .withValue(ContactsContract.RawContacts.Data.DATA3, label + " " + number.getFormattedNumber())
                        .build());

            }else {


                //insertar o actualizar nombre
                selection =
                        ContactsContract.Data.MIMETYPE + "=? AND " +
                        ContactsContract.Data.RAW_CONTACT_ID + "=? AND " +
                        ContactsContract.RawContacts.ACCOUNT_TYPE + "=?";
                values = new String[]{
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        String.valueOf(rawContactId),
                        WingsConst.ACCOUNT_TYPE
                };
                int existentRawContactId = getExistentRawContactId(context, selection, values, ContactsContract.Data.CONTENT_URI);
                if (existentRawContactId == 0) {
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName())
                            .build());
                } else {
                    ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName())
                            .withSelection(selection,values)
                            .build());
                }

                //insertar o actualizar numero
                selection =
                        ContactsContract.Data.NAME_RAW_CONTACT_ID + "=? AND " +
                        ContactsContract.Data.MIMETYPE + "=? AND " +
                        ContactsContract.RawContacts.ACCOUNT_TYPE + "=? AND (" +
                        ContactsContract.CommonDataKinds.Phone.NUMBER + "=? OR "+
                        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + "=? )"
                ;
                values = new String[]{
                        String.valueOf(contact.getRawId()),
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        WingsConst.ACCOUNT_TYPE,
                        number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber(),
                        number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber()
                };
                existentRawContactId = getExistentRawContactId(context, selection, values, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                if (existentRawContactId == 0) {
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.Data.DATA3, number.getLabel())
                            .withValue(ContactsContract.Data.DATA2, 0)
                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber())
                            .build());
                } else {
                    ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number.getFormattedNumber() != null ? number.getFormattedNumber() : number.getRawNumber())
                            .withValue(ContactsContract.Data.DATA2, 0)
                            .withValue(ContactsContract.Data.DATA3, number.getLabel())
                            .withSelection(selection, values)
                            .build());
                }


                //actualizar mimetype
                selection = ContactsContract.Data.MIMETYPE + "=? AND " + ContactsContract.Data.RAW_CONTACT_ID + "=? AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + "=?";
                values = new String[]{mimeType, String.valueOf(rawContactId), WingsConst.ACCOUNT_TYPE};
                //existentRawContactId = getExistentRawContactId(context, selection, values, ContactsContract.Data.CONTENT_URI);
                if (true) {
                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                            .withValue(ContactsContract.RawContacts.Data.MIMETYPE, mimeType)
                            .withValue(ContactsContract.RawContacts.Data.DATA1, "wings://" + prefix + number.getNumber() + "@sip.wingsmobile.net")
                            .withValue(ContactsContract.RawContacts.Data.DATA2, number.getNumber())
                            .withValue(ContactsContract.RawContacts.Data.DATA3, label + " " + number.getFormattedNumber())
                            .build());
                } else {
                    ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)

                            .withValue(ContactsContract.RawContacts.Data.DATA1, "wings://" + prefix + number.getNumber() + "@sip.wingsmobile.net")
                            .withValue(ContactsContract.RawContacts.Data.DATA2, number.getNumber())
                            .withValue(ContactsContract.RawContacts.Data.DATA3, label + " " + number.getFormattedNumber())
                            .withSelection(selection, values)
                            .build());
                }
            }

            try {
                context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static int getExistentRawContactId(Context context, String selection, String[] values, Uri uri){

        Cursor cur = context.getContentResolver().query(uri,
                null,
                selection,
                values,
                null);
        try {
            if (cur.moveToFirst()) {
                return cur.getInt(cur.getColumnIndex("raw_contact_id"));
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return 0;
    }





    /**
     * @param context
     */
    public static void deleteAllMimeTypes(Context context) {
        ContactsManager.deleteMimeType(context);
        //ContactsManager.deleteMimeType(context, contact, WingsConst.MIMETYPE_SECURE);
        //ContactsManager.deleteMimeType(context, contact, WingsConst.MIMETYPE_WINGS);
    }


    /**
     * Delete particular mime type for contact
     *
     * @param context
     */
    public static void deleteMimeType(Context context) {



        //delete raw contact, this will delete all data asociated
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        Uri deleteUri = ContactsContract.Data.CONTENT_URI.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                .build();
        ops.add(ContentProviderOperation.newDelete(deleteUri)
                .withSelection("account_type= ? ", new String[]{WingsConst.ACCOUNT_TYPE})
                .build());
        Uri deleteUr2i = ContactsContract.Contacts.CONTENT_URI.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                .build();
        ops.add(ContentProviderOperation.newDelete(deleteUr2i)
                .withSelection("account_type= ? ", new String[]{WingsConst.ACCOUNT_TYPE})
                .build());
        Uri deleteUr3i = ContactsContract.RawContacts.CONTENT_URI.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                .build();
        ops.add(ContentProviderOperation.newDelete(deleteUr3i)
                .withSelection("account_type= ? ", new String[]{WingsConst.ACCOUNT_TYPE})
                .build());




        try {
            context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void createWingsAccount(Context context) {

        AccountManager mAccountManager = AccountManager.get(context);
        Account account = new Account(WingsConst.ACCOUNT_NAME_SHORT, WingsConst.ACCOUNT_TYPE);
        mAccountManager.addAccountExplicitly(account, "", null);

        final String AUTHORITY = ContactsContract.AUTHORITY;
        final long SYNC_FREQUENCY = 60 * 60; // 1 hour (seconds)

        ContentResolver.setIsSyncable(account, AUTHORITY, 1);
        ContentResolver.setSyncAutomatically(account, AUTHORITY, true);
        ContentResolver.addPeriodicSync(account, AUTHORITY, new Bundle(), SYNC_FREQUENCY);

    }

    /**
     * Request for sync contacts
     *
     * @param context
     */
    public static void requestSync(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType(WingsConst.ACCOUNT_TYPE);
        if (accounts.length > 0) {
            Account account = accounts[0];
            ContentResolver.requestSync(account, ContactsContract.AUTHORITY, new Bundle());
        }
    }


    /**
     * Get a List of all contacts with phone numbers
     * return empty if there's no contacts
     *
     * @param context
     * @return
     */
   /* public static List<Contact> searchContacts(Context context, String search) {

        List<Contact> contacts = new LinkedList<>();

        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI, Uri.encode(search));

        Cursor cur = context.getContentResolver().query(uri,
                null,
                null,
                null,
                null);

        if (cur.getCount() <= 0) {
            return contacts;
        }





        cur.move(-1);
        int idColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup._ID);
        int rawIdColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.NAME_RAW_CONTACT_ID);
        int nameColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
        int photoColumnIndex = cur.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI);
        int lastUpdatedColumnIndex = cur.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);
        int sdnColumnIndex = cur.getColumnIndex("is_sdn_contact");

        while (cur.moveToNext()) {

            int isSdn = cur.getInt(sdnColumnIndex);
            if (isSdn != 0) {
                continue;
            }

            Contact contact = new Contact();

            int contactId = cur.getInt(idColumnIndex);
            int contactRawId = cur.getInt(rawIdColumnIndex);
            String displayName = cur.getString(nameColumnIndex);
            String photoUri = cur.getString(photoColumnIndex);
            String lastUpdate = cur.getString(lastUpdatedColumnIndex);

            contact.setId(contactId);
            contact.setRawId(contactRawId);
            contact.setName(displayName);
            contact.setPhotoUri(photoUri);
            contact.setLastUpdated(lastUpdate);

            List<Number> numbers = getContactNumbers(context, contact.getId().toString(), contact.getRawId().toString());
            contact.setNumbers(numbers);

            contacts.add(contact);
        }

        cur.close();

        return contacts;
    }*/


 /*   *//**
     * Clean up duplicated numbers PhoneAccount / Wings Account
     * leave only phone account
     *
     * @param numbers
     * @return
     */
    private static List<Number> mergeNumbers(List<Number> numbers) {

        List<Number> toReturn = new ArrayList<>();

        //agrupo por numeros
        Map<String, List<Number>> grouped = new HashMap<>();
        for (Number n : numbers) {
            if (grouped.containsKey(n.getNumber())) {
                grouped.get(n.getNumber()).add(n);
            } else {
                List<Number> groupList = new ArrayList<>();
                groupList.add(n);
                grouped.put(n.getNumber(), groupList);
            }
        }

        //recorro los agrupados
        for (Map.Entry<String, List<Number>> entry : grouped.entrySet()) {

            Number tokeep = null;
            boolean hasFree = false;
            boolean hasWings = false;
            boolean hasSecure = false;

            for (Number n : entry.getValue()) {
                if (n.isInPhoneAccount() && !n.isInWingsAccount()) {
                    tokeep = n;
                    continue;
                }
                if (n.isHasMimeFree() && !hasFree) {
                    hasFree = true;
                }
                if (n.isHasMimeSecure() && !hasSecure) {
                    hasSecure = true;
                }
                if (n.isHasMimeWings() && !hasWings) {
                    hasWings = true;
                }
            }

            if (tokeep != null) {
                tokeep.setHasMimeFree(hasFree);
                tokeep.setHasMimeSecure(hasSecure);
                tokeep.setHasMimeWings(hasWings);
                toReturn.add(tokeep);
            }
        }

        return toReturn;
    }

/*    *//**
     * return a new list with contacts only needed for sync
     *
     * @param lastUpdated
     * @param contacts
     * @return
     *//*
    private static List<Contact> cleanUpSynced(String lastUpdated, List<Contact> contacts) {

        long lastUpdatedLong = Long.valueOf(lastUpdated);
        List<Contact> cleaned = new LinkedList<>();
        for (Contact contact : contacts) {
            long contactLastUpdated = Long.valueOf(contact.getLastUpdated());
            if (contactLastUpdated < lastUpdatedLong) {
                continue;
            }
            cleaned.add(contact);
        }
        return cleaned;
    }*/







}