package com.wings.phone.clients.validation;

import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.DidCityDTO;
import com.wings.voip.model.DidCountryDTO;
import com.wings.voip.model.DidNumberAvailableDTO;
import com.wings.voip.model.GenericResponse;
import com.wings.voip.model.StoreServiceDTO;
import com.wings.voip.model.UpdateCheckResponse;
import com.wings.voip.model.WingsBookResponse;
import com.wings.voip.model.requests.RegisterRequest;
import com.wings.voip.model.requests.RegisterResponse;
import com.wings.voip.model.requests.RequestMissedCallResponse;
import com.wings.voip.model.requests.SyncContactRequest;
import com.wings.voip.model.requests.SyncContactResponse;
import com.wings.voip.model.secondNumber.RoamingFreeResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiClient {

    @GET("verify/request_call")
    Call<RequestMissedCallResponse> requestMissedCall(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("mobile") String mobile);

    /**
     * @param app_id
     * @param accessToken
     * @param mobile
     * @param mcc Request id
     * @param mnc last four digits of the calling number
     * @return
     */
    @POST("verify/confirm")
    Call<RequestMissedCallResponse> verifyNumber(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("mobile") String mobile, @Query("mcc") String mcc, @Query("mnc") String mnc);

    @GET("verify/done")
    Call<Void> done(@Query("callId") String callId);

    @GET("verify/number")
    Call<GenericResponse> verifyNumberValid(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("mobile") String mobile, @Query("countryCode") String countryCode);

    @POST("clients/register/")
    Call<RegisterResponse> register(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Body RegisterRequest req);

    @POST("contacts/sync")
    Call<SyncContactResponse> syncContacts(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Body SyncContactRequest req);

    @GET("provisioning/")
    Call<Map<String, String>> getProvisioning(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login);

    @GET("clients/account/status")
    Call<AccountStatus> getAccountStatus(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("locale") String locale);

    @GET("store/services")
    Call<List<StoreServiceDTO>> getStoreServices(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("locale") String locale);

    @POST("store/services/buy")
    Call<AccountStatus> postStoreServiceBuy(
            @Query("app_id") String app_id,
            @Query("access_token") String accessToken,
            @Query("login") String login,
            @Query("token") String token,
            @Query("serviceId") Long serviceId,
            @Query("extend") boolean extend
    );

    @GET("did/countries")
    Call<List<DidCountryDTO>> getDidCountries(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("locale") String locale);

    @GET("did/cities")
    Call<List<DidCityDTO>> getDidCities(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("locale") String locale, @Query("countryId") Long countryId);

    @GET("did/numbers/available")
    Call<List<DidNumberAvailableDTO>> getDidNumbersAvailable(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("locale") String locale, @Query("countryId") Long countryId, @Query("cityId") Long cityId);

    @POST("did/numbers/setup")
    Call<AccountStatus> postDidNumberBuy(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("token") String token, @Query("didId") String didId, @Query("didNumber") String didNumber, @Query("didRemoteId") String didRemoteId);

    @POST("roaming/action")
    Call<RoamingFreeResponse> postActionRoaming(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("mcc") String mcc, @Query("mnc") String mnc, @Query("enable") boolean enable);

    @POST("book/action")
    Call<WingsBookResponse> postActionWingsBook(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("mcc") String mcc, @Query("mnc") String mnc, @Query("enable") boolean enable);

    @POST("verify/sms")
    Call<String> requestSmsValidation(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Query("login") String login, @Query("lang") String lang);

    @GET("updates/check")
    Call<UpdateCheckResponse> checkUpdate(@Query("version") String version, @Query("model") String model);

    @POST("clients/updateToken")
    Call<GenericResponse> updateToken(@Query("app_id") String app_id, @Query("access_token") String accessToken, @Body RegisterRequest req);

}
