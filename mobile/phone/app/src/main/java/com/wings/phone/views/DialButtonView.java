package com.wings.phone.views;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.wings.phone.R;

/**
 * Created by seba on 26/04/17.
 */

public class DialButtonView {

    private LinearLayout layout;

    public DialButtonView(LinearLayout layout) {
        this.layout = layout;
    }

    public DialButtonView setNumber(String number) {
        ((TextView) layout.findViewById(R.id.button_number)).setText(number);
        return this;
    }

    public DialButtonView setText(String text) {
        TextView tview = (TextView) layout.findViewById(R.id.button_text);
        if(text.length() > 3){
            tview.setTextSize(10f);
        }
        tview.setText(text);
        return this;
    }
}
