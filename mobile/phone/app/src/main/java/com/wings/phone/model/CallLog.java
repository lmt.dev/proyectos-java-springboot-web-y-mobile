package com.wings.phone.model;


import android.content.Context;

import com.wings.phone.util.ExtraButtonStatus;

public class CallLog {
    private String name;
    private String number;
    private int type;
    private long date;
    private int id;
    private String accountId;
    private String cachedName;
    private String photoUri;
    private ExtraButtonStatus status;

    public CallLog(){
        status = new ExtraButtonStatus();
    }

    public void setNumber(String number) {
        this.number = number;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(ExtraButtonStatus status){
        this.status = status;
    }

    public void setType(int type) {
        this.type = type;
    }



    public void setAccountId(String accountId) {
        this.accountId = accountId;

    }

    public void setCachedName(String cachedName) {
        this.cachedName = cachedName;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public long getDate() {
        return date;
    }
    public void setDate(long date) {
        this.date = date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }




    public String getAccountId() {
        return accountId;
    }


    public String getPhotoUri() {
        return photoUri;
    }

    public ExtraButtonStatus getStatus() {
        return status;
    }


    public void process(Context context) {
        name = cachedName;

        if (name == null || name.isEmpty()) {
            name = number;
            status.setStatus(2);
        }

        if (name == null || name.isEmpty()) {
            name = context.getString(android.R.string.unknownName);
            status.setStatus(3);
        }
    }

    public String getCachedName() {
        return cachedName;
    }


}
