package com.wings.phone.telecom.sip;

import com.wings.phone.telecom.CallWrapper;

import org.linphone.core.Call;
import org.linphone.core.Core;

public interface SipListener {

    void onRegistrationStateChanged(CallWrapper wrapper);

    void onCallStateChanged(Call call, Call.State cstate, String message);

    void onEncryptionChanged(Call call, boolean on, String authenticationToken);

    void onCallCreated(Core core, Call call);

    void onStatsUpdate(String stats);

    void prepareTimeout();

}
