package com.wings.phone.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EncryptUtils {

    private static long s = 0x20625438 & 0x42316845 & 0x4232624;

    /**
     *
     * @param src
     * @return
     */
    public static String encrypt(String src) {
        return encrypt(src, false);
    }

    /**
     *
     * @param src
     * @param isHex
     * @return
     */
    public static String encrypt(String src, Boolean isHex) {
        String seed = String.valueOf(s);
        StandardPBEStringEncryptor enc = new StandardPBEStringEncryptor();
        enc.setPassword(seed);
        String result;
        if (isHex) {
            result = stringToHex(enc.encrypt(src));
        } else {
            result = enc.encrypt(src);
        }
        return result;
    }

    /**
     *
     * @param src
     * @return
     */
    public static String decrypt(String src) {
        return decrypt(src, false);
    }

    /**
     *
     * @param src
     * @param isHex
     * @return
     */
    public static String decrypt(String src, Boolean isHex) {
        String seed = String.valueOf(s);
        StandardPBEStringEncryptor enc = new StandardPBEStringEncryptor();
        enc.setPassword(seed);
        String result;
        if (isHex) {
            result = hexToString(enc.decrypt(src));
        } else {
            result = enc.decrypt(src);
        }
        return result;

    }

    /**
     *
     * @param str
     * @return
     */
    public static String stringToHex(String str) {
        StringBuilder sb = new StringBuilder();
        //Converting string to character array
        char ch[] = str.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            String hexString = Integer.toHexString(ch[i]);
            sb.append(hexString);
        }
        String result = sb.toString();
        return result;
    }

    /**
     *
     * @param str
     * @return
     */
    public static String hexToString(String str) {
        String result = new String();
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i = i + 2) {
            String st = "" + charArray[i] + "" + charArray[i + 1];
            char ch = (char) Integer.parseInt(st, 16);
            result = result + ch;
        }
        return result;
    }

}
