package com.wings.phone.util;

import android.content.Context;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;

import com.wings.phone.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

public class NumberUtils {

    private static int MAX = 6999;
    private static int MIN = 6000;

    static Random rnd = new Random();

    public static int getRandomCallId() {
        return rnd.nextInt(1500);
    }

    public static int getSipRandomPort() {
        return rnd.nextInt(MAX + 1 - MIN) + MIN;
    }


    public static String formatMoney(double money) {
        NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        return moneyFormat.format(round(money, 2));
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    /**
     * return formatted number for storage
     *
     * @param context
     * @param number
     * @return
     */
    public static String getFormattedNumber(Context context, String number) {
        TelephonyManager telephonyMngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String iso = telephonyMngr.getSimCountryIso();
        if (iso == null) {
            iso = Settings.getCountry();
        }

        if (iso != null) {
            iso = iso.toUpperCase();
        }

        String countryCode = getCountryDialCode(context);
        String ret = number;
        if (ret.length() <= 10 && ret.length() >= 8 && !ret.contains(countryCode)) {
            ret = countryCode + ret;
        }

        ret = PhoneNumberUtils.formatNumberToE164(ret, iso);
        if (ret == null) {
            ret = number;
        }

        if (ret.length() >= 10 && !ret.contains("+")) {
            ret = "+" + ret;
        }
        return ret;
    }


    /**
     * Get country Area code according to sim inserted
     * if no sim, return +34
     *
     * @param context
     * @return
     */
    public static String getCountryDialCode(Context context) {
        String contryDialCode = "+34";
        TelephonyManager telephonyMngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrCountryCode = context.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrCountryCode.length; i++) {
            String[] arrDial = arrCountryCode[i].split(",");
            if (arrDial[1].trim().equals(countryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }

}
