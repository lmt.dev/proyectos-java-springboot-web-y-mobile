package com.wings.phone.viewmodels;


import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.wings.phone.model.CallLog;
import com.wings.phone.model.DataRepository;

import java.util.List;

public class HistoryViewModel extends AndroidViewModel {


    private MutableLiveData<List<CallLog>> callLogsLiveData;
    private DataRepository repository;

    public HistoryViewModel(Application application) {
        super(application);
        callLogsLiveData = new MutableLiveData<List<CallLog>>();
        repository = new DataRepository(getApplication().getApplicationContext());
    }

    public MutableLiveData<List<CallLog>> getCallLogtListMutableLiveData() {
        return repository.getCallLogLiveData();
    }


    public void refresh() {
        repository.asyncTaskCallLog();
    }



}
