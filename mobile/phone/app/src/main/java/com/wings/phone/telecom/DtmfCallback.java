package com.wings.phone.telecom;

import com.wings.phone.CallActivity;

/**
 * Class used to play DTMF tones over a Wings call (GSM or SIP)
 * Timer schedule it's needed otherwise the tone is played forever
 */
public class DtmfCallback {

    private CallWrapper call;
    private CallActivity activity;

    public DtmfCallback(CallActivity activity) {
        this.activity = activity;
    }

    public void setCall(CallWrapper call) {
        this.call = call;
    }

    public void playDTMF(final char digit) {
        if (call != null) {
            call.playDtmfTone(digit);
        }
    }

}
