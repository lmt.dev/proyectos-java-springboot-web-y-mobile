package com.wings.backoffice;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiClient {

    @POST("login")
    //@FormUrlEncoded
    //@POST("api/v1/login")
    Call<LoginResponse> login(@Body LoginRequest req);
    //Call<LoginResponse> login(@Field("username") String username, @Field("password") String password);
}
