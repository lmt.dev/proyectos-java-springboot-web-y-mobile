package com.wings.backoffice;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private Session session;//global variable



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        session = new Session(this);
        findViewById(R.id.ingresarButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //api login
                String user = ((EditText)findViewById(R.id.user_EditText)).getText().toString();
                String pass = ((EditText)findViewById(R.id.pass_EditText)).getText().toString();

                try {
                    login(user,pass);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private void login(String user, String pass) throws NoSuchAlgorithmException {
        String apiUrl = getString(R.string.api_url_dev);

        LoginRequest req = new LoginRequest();

        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass);


        //String str = bytesToHex(");
        req.setPassword(sha256hex);
        req.setUsername(user);
        Call<LoginResponse> call = ApiClientFactory.getClient(apiUrl).login(req);

        try{

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response) {

                    LoginResponse loginResponse = response.body();
                    assert loginResponse != null;
                    if(loginResponse.isLogin()){
                        session.setusename(loginResponse.getUsername());
                        setContentView(R.layout.activity_main);
                    } else {
                        Toast.makeText(getApplicationContext(),loginResponse.getError(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t)
                {
                    t.printStackTrace();

                }
            });

       }catch (Exception ex){
           Log.e("ERROR",ex.toString());
       }



    }






}
