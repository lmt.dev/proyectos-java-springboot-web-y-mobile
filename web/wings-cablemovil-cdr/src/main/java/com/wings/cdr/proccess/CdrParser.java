/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.cdr.proccess;

import com.wings.cdr.CdrProperties;
import com.wings.cdr.entity.Cdr;
import com.wings.cdr.repo.CdrRepository;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CdrParser {

    private final Logger log = LoggerFactory.getLogger(CdrParser.class);

    @Autowired
    private CdrProperties cdrProperties;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private CdrRepository cdrRepository;

    public void parse() {

        File parsed = new File(cdrProperties.getDestDir() + "/parsed");
        if (!parsed.exists()) {
            parsed.mkdir();
        }

        String charset = "ISO-8859-1";
        File dir = new File(cdrProperties.getDestDir());
        File[] files = dir.listFiles();
        log.info("Start parsing files");
        long i = 0;
        for (File file : files) {

            if (file.isDirectory()) {
                continue;
            }

            try {
                BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));
                String line;
                log.info("Parsing file nro: " + i++);
                while ((line = bf.readLine()) != null) {
                    try {
                        Cdr cdr = parseLine(line);
                        try {
                            cdrRepository.save(cdr);
                        } catch (DataIntegrityViolationException e) {
                        }
                    } catch (ParseException e) {
                        log.error("Error parseando linea", e);
                    }
                }

                File renamed = new File(parsed.getAbsolutePath() + "/" + file.getName());
                file.renameTo(renamed);

            } catch (IOException e) {
                log.error("Error leyendo archivo", e);
            }
        }

        log.info("End parsing files");
    }

    private Cdr parseLine(String line) throws ParseException {

        if (line.trim().length() == 0) {
            throw new ParseException("Empty line", 0);
        }

        String[] data = line.split(";");

        Cdr cdr = new Cdr();
        cdr.setCdrType(data[0]);
        cdr.setClient(data[1]);
        cdr.setOrigin(data[2]);
        cdr.setOriginOperator(data[3]);
        cdr.setDestination(data[4]);
        cdr.setDate(dateFormat.parse(data[5]));
        cdr.setTime(timeFormat.parse(data[6]));
        cdr.setDuration(Long.parseLong(data[7]));
        cdr.setData1(Long.parseLong(data[8]));
        cdr.setData2(Long.parseLong(data[9]));
        cdr.setDestinationOperator(data[10]);
        cdr.setPrice(Double.parseDouble(data[11]));

        return cdr;
    }

}
