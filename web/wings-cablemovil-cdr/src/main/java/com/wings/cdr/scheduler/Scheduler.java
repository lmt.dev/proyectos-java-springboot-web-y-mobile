/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.cdr.scheduler;

import com.wings.cdr.proccess.CdrDownloader;
import com.wings.cdr.proccess.CdrParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class Scheduler {

    @Autowired
    private CdrParser cdrParser;

    @Autowired
    private CdrDownloader cdrDownloader;

    private final long dailyRate = 86400000l;
    private final long every4hours = 14400000l;

    @Scheduled(initialDelay = 10000l, fixedRate = every4hours)
    public void downloadAndParse() {
        cdrDownloader.download();
        cdrParser.parse();
    }
}
