/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.cdr.proccess;

import com.wings.cdr.CdrProperties;
import com.wings.cdr.entity.Config;
import com.wings.cdr.repo.ConfigRepository;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CdrDownloader {

    private final Logger log = LoggerFactory.getLogger(CdrDownloader.class);

    @Autowired
    private CdrProperties cdrProperties;

    @Autowired
    private ConfigRepository configRepository;

    public void download() {

        Config config = configRepository.findByK("file.lastTimestamp");
        if (config == null) {
            config = new Config();
            config.setK("file.lastTimestamp");
            config.setV("0");
            configRepository.save(config);
        }

        long minTime = Long.parseLong(config.getV());
        final long filterTime = minTime;

        try {
            FTPClient client = new FTPClient();
            client.connect(cdrProperties.getFtpServer());

            client.setControlKeepAliveReplyTimeout(5000);
            client.setDefaultTimeout(300000);
            client.setSoTimeout(300000);
            client.setDataTimeout(0);

            client.setFileType(FTP.ASCII_FILE_TYPE);

            boolean login = client.login(cdrProperties.getFtpUser(), cdrProperties.getFtpPassword());
            FTPFileFilter filter = new FTPFileFilter() {
                @Override
                public boolean accept(FTPFile ftpf) {
                    return ftpf.getTimestamp().getTimeInMillis() > filterTime;
                }
            };

            FTPFile[] files = client.listFiles(null, filter);
            log.info("Files to download: " + files.length);
            for (FTPFile file : files) {

                if (minTime < file.getTimestamp().getTimeInMillis()) {
                    minTime = file.getTimestamp().getTimeInMillis();
                }

                File f = new File(cdrProperties.getDestDir() + "/" + file.getName());
                try (FileOutputStream fos = new FileOutputStream(f)) {
                    log.info("Downloading : " + file.getName());
                    client.retrieveFile(file.getName(), fos);
                }
            }

            log.info("New MIN TIME: " + minTime);
            config.setV(String.valueOf(minTime));
            configRepository.save(config);

            client.logout();
            client.disconnect();

        } catch (IOException e) {
            log.error("Error descargando archivos", e);
        }
    }

}
