/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.client;

import com.wings.payments.client.dto.RequestDTO;
import com.wings.payments.client.dto.ResponseDTO;
import java.net.URI;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
public class WingsPay {

    private static String url;

    public static final String ENVIRONMENT_TEST = "TEST";
    public static final String ENVIRONMENT_DEV = "DEV";
    public static final String ENVIRONMENT_PROD = "PROD";

    /**
     * Set the URL for environment
     * <p>
     * TEST: http://local.pagos.wingsmobile.com/
     * <p>
     * DEV: https://dev.pagos.wingsmobile.com/
     * <p>
     * PROD: https://pagos.wingsmobile.com/
     *
     * @param environment
     */
    public static void initEnv(String environment) {
        switch (environment) {
            case ENVIRONMENT_TEST:
                url = "http://local.pagos.wingsmobile.com/";
                break;
            case ENVIRONMENT_DEV:
                url = "https://dev.pagos.wingsmobile.com/";
                break;
            case ENVIRONMENT_PROD:
                url = "https://pagos.wingsmobile.com/";
                break;
        }
    }

    /**
     * @param testing
     */
    public static void init(boolean testing) {
        if (testing) {
            url = "https://dev.pagos.wingsmobile.com/";
        } else {
            url = "https://pagos.wingsmobile.com/";
        }
    }

    public static String createPaymentRequest(RequestDTO dto) {
        try {
            RestTemplate template = new RestTemplate();
            RequestEntity<RequestDTO> requestEntity = RequestEntity
                    .post(new URI(url + "/b"))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .body(dto);

            ResponseEntity<ResponseDTO> resp = template.exchange(requestEntity, ResponseDTO.class);

            if (resp != null && resp.getBody() != null && resp.getBody().getStatus() != null && resp.getBody().getStatus().equals("success")) {
                return url + resp.getBody().getMessage();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
