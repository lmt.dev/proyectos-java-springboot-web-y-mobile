CREATE TABLE MLM.bis_payments_type (
	id INT auto_increment NOT NULL,
	payment_type_name varchar(50) NOT NULL,
	CONSTRAINT bis_payments_type_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('bacs');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('tefpay');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('pagantis');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('free');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('paypal');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('manual');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('tucompra');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('payulatam');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('culqi');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('stripe');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('paymentez');
INSERT INTO MLM.bis_payments_type (payment_type_name ) VALUES ('coms');