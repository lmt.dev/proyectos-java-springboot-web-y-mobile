--Agrego pago en cuotas

INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock, `source`) 
	VALUES ('STRT3C', 'STARTER-3C', '', 1, 83, 8, 27, -2, 'store-co,store-pe,store-ec'),
         ('STRTPRO3C', 'STARTER PRO-3C', '', 1, 183, 18, 100, -2, 'store-co,store-pe,store-ec'),
	 ('CMB3C', 'COMBO-3C', '', 1, 216, 21, 108, -2, 'store-co,store-pe,store-ec'),
	 ('CMBPRO3C', 'COMBO PRO-3C', '', 1, 233, 23, 116, -2, 'store-co,store-pe,store-ec'),
	 ('MINFND3C', 'MINI FOUNDER-3C', '', 1, 333, 33, 166, -2, 'store-co,store-pe,store-ec'),
	 ('GLDFND3C', 'GOLDEN FOUNDER-3C', '', 1, 333, 33, 166, -2, 'store-co,store-pe,store-ec'),
	 ('DMDFND3C', 'DIAMOND FOUNDER-3C', '', 1, 666, 66, 333, -2, 'store-co,store-pe,store-ec');

--nuevos nombres para es, co e it
INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
	VALUES 
        (82, 'STARTER - Pago en 3 cuotas'),
        (82, 'STARTER - Pago en 3 cuotas - RED'),
        (82, 'STARTER - Pago en 3 cuotas - BLACK'),
        (83, 'STARTER PRO - Pago en 3 cuotas'),
        (84, 'COMBO - Pago en 3 cuotas'),
        (84, 'COMBO - Pago en 3 cuotas - RED'),
        (84, 'COMBO - Pago en 3 cuotas - BLACK'),
        (85, 'COMBO PRO - Pago en 3 cuotas'),
        (86, 'Mini FOUNDER - Pago en 3 cuotas'),
        (86, 'Mini Founder - Pago en 3 cuotas'),
        (86, 'Mini FOUNDER - Pago en 3 cuotas - RED'),
        (86, 'Mini FOUNDER - Pago en 3 cuotas - BLACK'),
        (87, 'GOLDEN FOUNDER - Pago en 3 cuotas'),
        (87, 'Golden FOUNDER - Pago en 3 cuotas'),
        (88, 'DIAMOND FOUNDER - Pago en 3 cuotas'),
        (88, 'Diamond FOUNDER - Pago en 3 cuotas'),        
;
        
