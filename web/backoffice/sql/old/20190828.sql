
--add cuotas info
alter table bis_store_items add column installments tinyint(1) default 0;
alter table bis_store_items add column installments_qty int default 0;

--update store items
update bis_store_items set installments = 1 , installments_qty = 3 where id in (82,83,84,85,86,87,88);

--update orders with cuotas
alter table bis_orders add column installment_number int default 0;
update bis_orders set installment_number = 1 where item_id in (82,83,84,85,86,87,88) and message = 'Cuota 1';
update bis_orders set installment_number = 2 where item_id in (82,83,84,85,86,87,88) and message = 'Cuota 2';
update bis_orders set installment_number = 3 where item_id in (82,83,84,85,86,87,88) and message = 'Cuota 3';

--update the ones with other cuotas created
update bis_orders set installment_number = 1 where item_id in (82,83,84,85,86,87,88) and message is null;
