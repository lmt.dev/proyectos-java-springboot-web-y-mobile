CREATE TABLE `bis_temp_ecuador` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_date` TIMESTAMP NOT NULL,  
  `first_name` varchar(250) default NULL,
  `last_name` varchar(250) default NULL,
  `email` varchar(250) default NULL,
  `phone` varchar(250) default NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




INSERT INTO MLM.bis_store_items
(code, name, description, category, price, rank_points, pay_points, stock, source, available_to, enabled, image, final_item_id, reference, price_for_coms)
VALUES('evtecu03profndr', 'Promo Founder 03 de Agosto', 'El paquete Promo FOUNDER 03 de Agosto incluye lo siguiente:
1 Teléfono W3.
1 Teléfono W6.
1 Wings Watch Fashion Line.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.', 1, 808.477576, 80.00, 400.00, -2, 'store-ec,store-int', NULL, 1, NULL, NULL, NULL, NULL);

INSERT INTO MLM.bis_store_items
( code, name, description, category, price, rank_points, pay_points, stock, source, available_to, enabled, image, final_item_id, reference, price_for_coms)
VALUES( 'evtecu03str', 'Promo STARTER 03 de Agosto', 'El paquete Promo STARTER 03 de Agosto incluye lo siguiente:
1 Teléfono W3.
1 Wings Watch Fashion Line.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.', 1, 253.389830, 25.00, 125.00, -2, 'store', NULL, 0, NULL, NULL, NULL, NULL);


INSERT INTO MLM.bis_store_items
( code, name, description, category, price, rank_points, pay_points, stock, source, available_to, enabled, image, final_item_id, reference, price_for_coms)
VALUES( 'evtecu03strpro', 'Promo STARTER PRO 03 de Agosto', 'El paquete Promo STARTER PRO 03 de Agosto incluye lo siguiente:
2 Teléfono W3.
2 Wings Watch Fashion Line.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.', 1, 420.387531, 50.00, 250.00, -2, 'store', NULL, 0, NULL, NULL, NULL, NULL);
