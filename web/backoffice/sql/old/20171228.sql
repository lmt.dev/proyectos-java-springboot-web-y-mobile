drop table bis_order_items;
CREATE TABLE `bis_order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `store_item_option_id` bigint(20) DEFAULT NULL,
  `shipped` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table bis_orders add column shipping_status varchar(100) not null default 'created';
alter table bis_orders add column message text null;
alter table bis_orders add column source varchar(100) not null default 'legacy';

CREATE TABLE `bis_store_items_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_item_id` bigint(20) DEFAULT NULL,
  `ean_code` varchar(200) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '-1',
  `option_name` varchar(200) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

