ALTER TABLE MLM.bis_orders ADD external_id BIGINT NULL ;

DROP TABLE bis_balance;
CREATE TABLE `bis_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,2) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `control` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


