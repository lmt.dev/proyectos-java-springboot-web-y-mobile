drop table `bis_member_snapshots`;
CREATE TABLE `bis_member_snapshots` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) NOT NULL,
  `event_points` decimal(10,2) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `is_founder` tinyint(1) NOT NULL DEFAULT 0,
  `next_rank_id` bigint(20) NOT NULL,
  `next_rank_name` varchar(200) NOT NULL,
  `next_rank_required` bigint(20) NOT NULL,
  `next_rank_requiredsb` bigint(20) NOT NULL,
  `snapshot_date` DATETIME DEFAULT NULL,
  `most_valuable_branch` bigint(20) NOT NULL,
  `speed_bonus` tinyint(1) NOT NULL DEFAULT 0,
  `speed_bonus_end_date` DATETIME DEFAULT NULL,
  `kits_pv` decimal(10,2) default null,
  `products_pv` decimal(10,2) default null,
  `services_pv` decimal(10,2) default null,
  `personal_service_pv` decimal(10,2) default null,
  `sum6040` decimal(10,2) default null,
  `sums_per_branch` text default null,
  `points_per_child` text default null,
  `referers` bigint(20) default 0,
  `product_clients` bigint(20) default 0,
  `comment` varchar(200) default null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table bis_calification add column member_type varchar(30) not null;
alter table bis_members add column calculated_rank_id bigint(20) not null default 1;
