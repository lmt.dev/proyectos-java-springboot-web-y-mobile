-- alter table bis_store_items add column category_name varchar(50) default null;
-- alter table bis_store_items add column sub_category_name varchar(50) default null; --telefono, ordenador, reloj,tablet
-- update bis_store_items set category_name = 'kit' where category = 1;
-- update bis_store_items set category_name = 'product' where category = 2;
-- update bis_store_items set category_name = 'service' where category = 3;
-- update bis_store_items set category_name = 'upgrade' where category = 4;
-- update bis_store_items set category_name = 'store' where category = 5;
-- update bis_store_items set category_name = 'ticket' where category = 6;
-- update bis_store_items set category_name = 'recharge' where category = 7;
-- update bis_store_items set category_name = 'voip' where category = 8;
-- 

-- alter table bis_store_items add column short_description varchar(50) default null; --short description to show in some cases
-- alter table bis_store_items add column status varchar(50) default null; --short description to show in some cases
-- alter table bis_store_items add column visibility varchar(50) default null; --short description to show in some cases
-- alter table bis_store_items add column sales_count bigint(20) default 0; --count to show popularity
-- alter table bis_store_items add column rating integer(1) default 0; --count to show stars
-- 
-- alter table bis_store_items add column single tinyint(1) default 0; --count to show stars
-- alter table bis_store_items add column available_to_complex tinyint(1) default 0; --count to show stars
-- alter table bis_store_items add column countries varchar(250); --count to show stars
-- 
-- alter table bis_store_items modify rank_points decimal(9,2); --allow nulls
-- alter table bis_store_items modify pay_points decimal(9,2); --allow nulls
-- 
-- --describe bis_store_items;
-- 
-- CREATE TABLE `bis_store_item_children` (
--   `id` bigint(20) NOT NULL AUTO_INCREMENT,
--   `parent_id` bigint(20) NOT NULL,
--   `child_id` bigint(20) NOT NULL,
--   `quantity` integer NOT NULL default 0,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 
-- CREATE TABLE `bis_store_item_i18n` (
--   `id` bigint(20) NOT NULL AUTO_INCREMENT,
--   `store_item_id` bigint(20),
--   `country` varchar(10),
--   `name` varchar(255),
--   `description` text,
--   `price` decimal(24,6),
--   `price_for_coms` decimal(24,6),
--   `stock` bigint(20),
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- alter table bis_store_items_options add column default_value tinyint(1) not NULL default 0 comment 'default value if user did no choose one';
-- alter table bis_order_items add column store_item_ean varchar(200);


-- --bis_store_items_options_stock --esta tabla representa un producto individialmente
-- CREATE TABLE `bis_store_items_options_stock` (
--   `id` bigint(20) NOT NULL AUTO_INCREMENT,
--   `store_item_id` bigint(20), --que producto es
--   `store_item_option_id` bigint(20), --que opcion dentro del producto
--   `country` varchar(10), --pais al que esta destinado
--   `item_name` varchar(150), --nombre del producto
--   `sn` varchar(255), --numero de serie
--   `imei1` varchar(255), --imei1 si lo tiene
--   `imei2` varchar(255), -- imei2 si lo tiene
--   `custom1` varchar(255), --campo custom, ponemos algo no contemplado
--   `custom2` varchar(255),--campo custom, ponemos algo no contemplado
--   `custom3` varchar(255),--campo custom, ponemos algo no contemplado
--   `taken` tinyint(1), --si el producto ya esta asociado y enviado a delivery
--   `taken_date` DATETIME, --fecha en la que fue tomado
--   `order_id` BIGINT(20), -- order asociada
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;





--BWN
-- alter table bis_members add column bwn_wallet varchar(255) default null;
--alter table bis_members add column bwn_claim int not null default 0;
-- alter table bis_members add column bwn_sent tinyint(1) not null default 0;
--alter table bis_members modify bwn_claim int default 0;

----------------------------------------------------------------------------------------------

