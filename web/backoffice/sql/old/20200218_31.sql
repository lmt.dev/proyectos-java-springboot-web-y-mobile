CREATE TABLE `MLM`.`bis_topups` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `msisdn` BIGINT NULL,
  `amount` double(24,6) NULL,
  `currency` VARCHAR(100) NULL,
  `provider` VARCHAR(100) NULL,
  `creation_date` datetime NULL,
  `status` VARCHAR(45) NULL,  
PRIMARY KEY (`id`));

alter table bis_topups add column mod_user varchar(150) default null;
alter table bis_topups add column mod_ip varchar(150) default null;
alter table bis_topups add column mod_status varchar(150) default null;
alter table bis_topups add column iccid varchar(150) default null;

