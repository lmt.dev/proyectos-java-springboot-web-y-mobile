
alter table bis_store_items add column available_to varchar(255) default null;
alter table bis_store_items add column enabled tinyint(1) not null default 1;

INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock, `source`, available_to,enabled) 
	VALUES ('STRTN', 'STARTER', '', 1, 249.00, 25.00, 125.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('STRTPRON', 'STARTER PRO', '', 1, 599.00, 60.00, 300.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('CMBN', 'COMBO', '', 1, 649.00, 65.00, 325.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('CMBPRON', 'COMBO PRO', '', 1, 749.00, 75.00, 375.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('MINFNDN', 'MINI FOUNDER', '', 1, 1049.00, 105.00, 525.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('GLDFNDN', 'GOLDEN FOUNDER', '', 1, 1049.00, 105.00, 525.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1),
	 ('DMDFNDN', 'DIAMOND FOUNDER', '', 1, 2099.00, 210.00, 1050.00, -2, 'store-es,store-co,store-it,store-ec,store-pe', NULL,1);


INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
	VALUES (89, 'STARTER'),
	(89, 'STARTER - RED'),
	(89, 'STARTER - BLACK'),
	(90, 'STARTER PRO'),
	(91, 'COMBO'),
	(91, 'COMBO - RED'),
	(91, 'COMBO - BLACK'),
	(92, 'COMBO PRO'),
	(93, 'Mini FOUNDER'),
	(93, 'Mini Founder'),
	(93, 'Mini FOUNDER - RED'),
	(93, 'Mini FOUNDER - BLACK'),
	(94, 'GOLDEN FOUNDER'),
	(94, 'Golden FOUNDER'),
	(95, 'DIAMOND FOUNDER'),
	(95, 'Diamond FOUNDER');

UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 3;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 4;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 5;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 6;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 7;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 8;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 69;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 70;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 71;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 72;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 73;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 74;
UPDATE MLM.bis_store_items SET `enabled` = false WHERE id = 75;


SELECT * FROM bis_store_items where category = 1 and name = 'Mini FOUNDER';
SELECT * FROM bis_store_items;
SELECT * FROM bis_store_item_names;

INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock, `source`, available_to, enabled) 	
	('upgrade-stmfn', 'Upgrade STARTER a Mini FOUNDER', NULL, 4, 800.00, 80.00, 400.00, -1, 'store', '69,89', true),
	('upgrade-stgfn', 'Upgrade STARTER a Golden FOUNDER', NULL, 4, 800.00, 80.00, 400.00, -1, 'store', '69,89', true),
	('upgrade-stdfn', 'Upgrade STARTER a Diamond FOUNDER', NULL, 4, 1850.00, 185.00, 925.00, -1, 'store', '69,89', true),
	('upgrade-sptmfn', 'Upgrade STARTER PRO a Mini FOUNDER', NULL, 4, 450.00, 45.00, 225.00, -1, 'store', '70,90', true),
	('upgrade-sptgfn', 'Upgrade STARTER PRO a Golden FOUNDER', NULL, 4, 450.00, 45.00, 225.00, -1, 'store', '70,90', true),
	('upgrade-sptdfn', 'Upgrade STARTER PRO a Diamond FOUNDER', NULL, 4, 1500.00, 150.00, 750.00, -1, 'store', '70,90', true),
	('upgrade-ctmfn', 'Upgrade COMBO a Mini FOUNDER', NULL, 4, 400.00, 40.00, 200.00, -1, 'store', '71,91', true),
	('upgrade-ctgfn', 'Upgrade COMBO a Golden FOUNDER', NULL, 4, 400.00, 40.00, 200.00, -1, 'store', '71,91', true),
	('upgrade-ctdfn', 'Upgrade COMBO a Diamond FOUNDER', NULL, 4, 1450.00, 145.00, 725.00, -1, 'store', '71,91', true),
	('upgrade-cptmfn', 'Upgrade COMBO PRO a Mini FOUNDER', NULL, 4, 300.00, 30.00, 150.00, -1, 'store', '72,92', true),
	('upgrade-cptgfn', 'Upgrade COMBO PRO a Golden FOUNDER', NULL, 4, 300.00, 30.00, 150.00, -1, 'store', '72,92', true),
	('upgrade-cptdfn', 'Upgrade COMBO PRO a Diamond FOUNDER', NULL, 4, 1350.00, 135.00, 675.00, -1, 'store', '72,92', true),
        ('upgrade-mftdfn', 'Upgrade Mini FOUNDER a Diamond Founder', NULL, 4, 1050.00, 105.00, 525.00, -1, 'store', '73,93', true),
	('upgrade-gftdfn', 'Upgrade Golden FOUNDER a Diamond Founder', NULL, 4, 1050.00, 105.00, 525.00, -1, 'store', '74,94', true);


alter table bis_store_items add column image varchar(255) default null;

alter table bis_store_items add column final_item_id bigint(20) default null;

