DROP TABLE bis_member_services;
CREATE TABLE `bis_member_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) default NULL,
  `store_id` varchar(200) default null,
  `creation_date` TIMESTAMP NOT NULL,
  `status` varchar(50) NOT NULL,  
  `type` varchar(50) NOT NULL,  
  `type_status` varchar(250) DEFAULT NULL,  
  `first_name` varchar(250) default null,
  `last_name` varchar(250) default null,
  `company_name` varchar(250) default null,
  `id_type` varchar(20) default null,
  `id_value` varchar(40) default null,
  `description` varchar(250) default null,
  `email` varchar(250) default null,
  `comment` varchar(250) default null,
  `reference` varchar(50) NOT NULL,  
  `reference_id` bigint(20) default NULL,  
   `personal` tinyint(1) default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE MLM.bis_member_snapshots CHANGE product_clients client_count bigint(20) DEFAULT 0 NULL ;
