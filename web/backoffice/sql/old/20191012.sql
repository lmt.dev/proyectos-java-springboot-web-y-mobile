DROP TABLE `bis_support_messages`;
CREATE TABLE `bis_support_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(150),
  `member_id` bigint(20),
  `description` varchar(500),
  `parent_id` bigint(20),
  `creation_date` DATETIME not null default current_timestamp,
  `status` varchar(150),
  `closed_date` DATETIME,
  `closed_user` varchar(255),
  `closed_message` varchar(500),  
  `assigned_to` varchar(255),
  `country` varchar(20),
  `email` varchar(200),
  `first_name` varchar(200),
  `last_name` varchar(200),
  `username` varchar(200),
  `attachment` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `bis_support_messages_attachment`;
CREATE TABLE `bis_support_messages_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_id` bigint(20),
  `path` varchar(500),
  `code` varchar(500),  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

