alter table bis_events_products add column seats_pool bigint(20) after seats_available;
alter table bis_events add column enable_sync tinyint(1) not null default 0;