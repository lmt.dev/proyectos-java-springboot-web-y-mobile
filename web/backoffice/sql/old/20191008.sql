DROP TABLE `bis_promo_latam`;
CREATE TABLE `bis_promo_latam` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `rank_id` bigint(20) NOT NULL,
  `rank_name` varchar(150),
  `points_needed` decimal(20,6),
  `points_reached` decimal(20,6),
  `percent_reached` integer,
  `qualify` tinyint(1) not null default 0,
  `country` varchar(200),
  `username` varchar(200),
  `first_name` varchar(500),
  `last_name` varchar(500),
  `email` varchar(500),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;