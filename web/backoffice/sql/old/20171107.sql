TRUNCATE TABLE MLM.bis_orders;
TRUNCATE TABLE MLM.bis_differential;
ALTER TABLE MLM.bis_differential ADD receive_rank_id BIGINT NULL ;
ALTER TABLE MLM.bis_differential CHANGE receive_rank_id receive_rank_id BIGINT NULL AFTER receive_user_id ;
ALTER TABLE MLM.bis_differential ADD offer_rank_id BIGINT NULL ;
ALTER TABLE MLM.bis_differential CHANGE offer_rank_id offer_rank_id BIGINT NULL AFTER offer_user_id ;
ALTER TABLE MLM.bis_differential ADD event_id BIGINT NULL ;
ALTER TABLE MLM.bis_differential ADD locked BOOL DEFAULT 0 NOT NULL ;

CREATE TABLE `bis_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '',
  `registration_date` timestamp NOT NULL,
  `activation_date` timestamp NULL,
  `street` varchar(50) ,
  `city` varchar(50) ,
  `state` varchar(50) ,
  `country` varchar(50) ,
  `postal` varchar(50) ,
  `phone` varchar(50) ,
  `sponsor_id` int(11) ,
  `rank_id` int(11)  DEFAULT NULL,
  `initial_rank_id` int(11) DEFAULT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `dni_type` varchar(20) DEFAULT NULL,
  `dni_validated` tinyint(1) NOT NULL DEFAULT '0',
  `founder` tinyint(1) NOT NULL DEFAULT '0',
  `color_selected` tinyint(1) NOT NULL DEFAULT '0',
  `colors` varchar(200) DEFAULT NULL,
  `indirect_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `bis_rank_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `rank_id` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
