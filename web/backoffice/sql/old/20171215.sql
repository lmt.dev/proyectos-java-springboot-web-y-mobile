drop table `bis_invoice_items`;
drop table `bis_invoices`;
create table `bis_invoices`(
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `invoice_number` varchar(100),
    `invoice_date` timestamp null,
    `total` decimal(10,2) null,
    `status` varchar(100) null,
    `file_path` varchar(250) null,
    `sended` tinyint(1) not null default 0,
    `last_sended` timestamp null,
    `client_name` varchar(200) DEFAULT NULL,
    `company_name` varchar(200) DEFAULT NULL,
    `client_address` varchar(200) DEFAULT NULL,
    `client_city` varchar(200) DEFAULT NULL,
    `client_state` varchar(200) DEFAULT NULL,
    `client_dni` varchar(200) DEFAULT NULL,
    `imponible` varchar(200) DEFAULT NULL,
    `iva` varchar(200) DEFAULT NULL,
    `retenciones` varchar(200) DEFAULT NULL,
    `total_text` varchar(200) DEFAULT NULL,
    `order_id` bigint null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bis_invoice_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `concept` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_items_invoices_FK` (`invoice_id`),
  CONSTRAINT `invoice_items_invoices_FK` FOREIGN KEY (`invoice_id`) REFERENCES `bis_invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
