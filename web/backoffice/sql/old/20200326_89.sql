DROP TABLE `bis_country_properties`;
CREATE TABLE `bis_country_properties` (
  `code` char(20) NOT NULL,
  `currency` varchar(50),
  `default_lang` varchar(50),
  `default_locale` varchar(50),
  `exchange_rate` decimal(20,6),
  `flag` varchar(250),
  `sort_order` int not null default 0,
  `available_store` tinyint(1) not null default 0,
  `available_backoffice` tinyint(1) not null default 0,
  `iva1` decimal(20,6),
  `iva2` decimal(20,6),
  `iva3` decimal(20,6),
  `shipping1` decimal(20,6),
  `shipping2` decimal(20,6),
  `shipping3` decimal(20,6),
  `shipping4` decimal(20,6),
  `dnis_list` varchar(500),
  `idts_list` varchar(500),
  `custom1` varchar(500),
  `custom2` varchar(500),
  `custom3` varchar(500),
  `custom4` varchar(500),
  `custom5` varchar(500),
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


