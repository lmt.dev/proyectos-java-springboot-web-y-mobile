DROP TABLE IF EXISTS `bis_store_items`;
CREATE TABLE `bis_store_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,  
  `code` varchar(50) DEFAULT NULL COMMENT 'Sku, code ?',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name of product',
  `description` text COMMENT 'Description',
  `category` int DEFAULT NULL,  
  `price` decimal(9,2) DEFAULT '0.00',
  `rank_points` decimal(9,2) NOT NULL DEFAULT '0.00',
  `pay_points` decimal(9,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `bis_orders`;
CREATE TABLE `bis_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,  
  `creation_date` TIMESTAMP NOT NULL,  
  `activation_date` TIMESTAMP NULL,  
  `modification_date` TIMESTAMP NULL,
  `item_id` bigint(20) NULL DEFAULT NULL,
  `status` varchar(100) NULL,
  `comment` varchar(300) NULL,
  `payment_status` varchar(100) NULL,
  `payment_amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `payment_currency` varchar(100) NULL,
  `payment_type` varchar(100) NULL,
  `payer_email` varchar(100) NULL,
  `first_name` varchar(100) NULL,
  `last_name` varchar(100) NULL,
  `store_id` varchar(100) NULL,
  `member_id` bigint(20) NULL,
  `type` varchar(100) NULL,
  `type_status` varchar(100) NULL,
  `active` BOOL DEFAULT 0 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO bis_store_items (id,code,name,description,category,price,rank_points,pay_points) VALUES 
(1,'W5','W5','TECHNICAL SPECIFICATIONS COLORES: Space Gray / Champagne gold 
CUERPO: Aircraft Alluminium PESO: 169 g 
OS: Android 6.0 
CPU: MT6750T Octa-Core 1.5GHZ 
GPU: Mali-7860 
RAM: 4GB Samsung 
STORAGE: 32GB Samsung, Support Micro SDCard hasta 128GB Fingerprint: Fingerprint en el retro. Dimencion: 150,9mm x 76,2 mmm Spesor: 7,6 mm Ultra plano
CAMERA Primaria: 20MP Sonic Secondaria: 8 MP
DISPLAY Dimencion: 5.5 “ Sharp Full HD 1920*1080 pixel Sensores: Gravidad, Proximidad, luz, huella digital, etc
BATERIA: Built-in, 2700mAh de polímero de litio.

SERVICIOS NATIVOS: GSM: Llamadas Nacionales y/o Internacionales SMS: Nacionales y/o Internacionales SECURE CALL: Llamadas Segura 100% 
SECOND NUMBER: Tu números telefónicos segundarios   
Elije entre más de 60 países 
ROAMING FREE: Tu roaming mundial sin costes
LOW COST WINGS: Llamadas Nacionales y internacionales
FREECALL: Llamadas gratuitas entre usuarios Wings 
FREE VIDEOCALL: Video llamadas gratuitas entre usuarios',2,499.00,50.00,250.00)
,(2,'W2','W2','TECHNICAL SPECIFICATIONS COLORES: Space Gray / Champagne gold CUERPO: aleación de metal Y policarbonato
PESO: 195 g 
OS: Android 6.0 
CPU: MTK6737  1.3GHz, núcleo cuádruple
RAM: 2GB
STORAGE: 16GB, Tarjeta TF de hasta 32GB (no incluida) 
HUELLA DIGITAL TRASERA
CAMERA Primaria: 8.0MP (SW 13.0MP) Secondaria: 2.0MP (SW 5.0MP)
DISPLAY Dimencion: 5.5“ 1280 x 720 (HD 720) pixel 
Panel: Captativo
BATERIA: SUCD 2500mah
SERVICIOS NATIVOS: GSM: Llamadas Nacionales y/o Internacionales
SMS: Nacionales y/o Internacionales
SECURE CALL: Llamadas Segura 100% 
SECOND NUMBER: Tu números telefónicos segundarios   
Elije entre más de 60 países 
ROAMING FREE: Tu roaming mundial sin costes
LOW COST WINGS: Llamadas Nacionales y internacionales
FREECALL: Llamadas gratuitas entre usuarios Wings 
FREE VIDEOCALL: Video llamadas gratuitas entre usuarios',2,199.00,20.00,100.00)
,(3,'0001','STARTER','El paquete STARTER incluye lo siguiente:
1 Teléfono W2.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,249.00,25.00,125.00)
,(4,'0002','STARTER 2','El paquete STARTER 2 incluye lo siguiente:
1 Teléfono W5.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,499.00,50.00,250.00)
,(5,'0003','COMBO','El paquete COMBO incluye lo siguiente:
1 Teléfono W2.
1 Teléfono W5.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,599.00,60.00,300.00)
,(6,'0004','COMBO PRO','El paquete COMBO PRO incluye lo siguiente:
2 Teléfono W5.
1 Teléfono W2.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,999.00,100.00,500.00)
,(7,'0005','COMBO BUSINESS','El paquete COMBO PRO incluye lo siguiente:
2 Teléfono W2.
3 Teléfono W5.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,1399.00,150.00,750.00)
,(8,'0007','COMBO FOUNDER','El paquete COMBO PRO incluye lo siguiente:
3 Teléfono W5.
4 Teléfono W2.
1 Licencia de afiliado Wings.
1 Tienda Online personalizada.',1,1999.00,200.00,1000.00)
,(15,'0000','FREE','Paquete FREE',1,0.00,0.00,0.00)
,(16,'cero500','Tarifa Cero500 - 500MB','Tarifa Cero500 - 500MB',3,2.99,1.00,0.00)
;
INSERT INTO bis_store_items (id,code,name,description,category,price,rank_points,pay_points) VALUES 
(17,'cero1000','Tarifa Cero1000 - 1GB','Tarifa Cero1000 - 1GB',3,4.99,1.00,0.00)
,(18,'ilimitadav','Tarifa Ilimitada Solo Voz','Tarifa Ilimitada Solo Voz',3,9.90,1.00,0.00)
,(19,'ilimitada2','Tarifa Ilimitada 2GB','Tarifa Ilimitada 2GB',3,18.90,3.00,0.00)
,(20,'ilimitada4','Tarifa Ilimitada 4GB','Tarifa Ilimitada 4GB',3,22.90,4.00,0.00)
,(21,'ilimitada10','Tarifa Ilimitada 10GB','Tarifa Ilimitada 10GB',3,24.90,6.00,0.00)
,(22,'combina-2-3','Combina Sin Voz 200 MB','Combina Sin Voz 200 MB',3,4.99,1.00,0.00)
,(23,'combina-2-5','Combina Sin Voz 500 MB','Combina Sin Voz 500 MB',3,6.99,1.00,0.00)
,(24,'combina-2-6','Combina Sin Voz 1GB','Combina Sin Voz 1GB',3,7.99,1.00,0.00)
,(25,'combina-2-10','Combina Sin Voz 2GB','Combina Sin Voz 2GB',3,11.99,2.00,0.00)
,(26,'combina-4-2','Combina - 100min. - Sin datos','Combina - 100min. - Sin datos',3,5.99,1.00,0.00)
;
INSERT INTO bis_store_items (id,code,name,description,category,price,rank_points,pay_points) VALUES 
(27,'combina-4-3','Combina - 100min. - 200Mb','Combina - 100min. - 200Mb',3,6.99,1.00,0.00)
,(28,'combina-4-5','Combina - 100min. - 500MB','Combina - 100min. - 500MB',3,8.99,1.00,0.00)
,(29,'combina-4-6','Combina - 100min. - 1GB','Combina - 100min. - 1GB',3,9.99,1.00,0.00)
,(30,'combina-4-10','Combina - 100min. - 2GB','Combina - 100min. - 2GB',3,10.98,2.00,0.00)
,(31,'combina-7-2','Combina - 200min. - Sin datos','Combina - 200min. - Sin datos',3,8.99,1.00,0.00)
,(32,'combina-7-3','Combina - 200min. - 200Mb','Combina - 200min. - 200Mb',3,9.99,1.00,0.00)
,(33,'combina-7-5','Combina - 200min. - 500MB','Combina - 200min. - 500MB',3,11.99,2.00,0.00)
,(34,'combina-7-6','Combina - 200min. - 1GB','Combina - 200min. - 1GB',3,12.99,2.00,0.00)
,(35,'combina-7-10','Combina - 200min. - 2GB','Combina - 200min. - 2GB',3,16.99,3.00,0.00)
,(36,'combina-9-2','Combina - 300min. - Sin datos','Combina - 300min. - Sin datos',3,10.99,2.00,0.00)
;
INSERT INTO bis_store_items (id,code,name,description,category,price,rank_points,pay_points) VALUES 
(37,'combina-9-3','Combina - 300min. - 200Mb','Combina - 300min. - 200Mb',3,11.99,2.00,0.00)
,(38,'combina-9-5','Combina - 300min. - 500MB','Combina - 300min. - 500MB',3,13.99,2.00,0.00)
,(39,'combina-9-6','Combina - 300min. - 1GB','Combina - 300min. - 1GB',3,14.99,2.00,0.00)
,(40,'combina-9-10','Combina - 300min. - 2GB','Combina - 300min. - 2GB',3,18.99,3.00,0.00)
;