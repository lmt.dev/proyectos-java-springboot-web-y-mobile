CREATE INDEX bis_members_sponsor_id_IDX USING BTREE ON MLM.bis_members (sponsor_id);
CREATE INDEX bis_orders_store_id_IDX USING BTREE ON MLM.bis_orders (store_id);
CREATE INDEX bis_members_registration_date_IDX USING BTREE ON MLM.bis_members (registration_date);
