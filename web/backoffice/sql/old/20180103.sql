alter table bis_orders add column multiple tinyint(1) not null default 0;
alter table bis_orders add column with_prize tinyint(1) not null default 0;
alter table bis_orders add column prize_text varchar(150) null;
alter table bis_order_items add column `name` varchar(200) null;


INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(1, 1, '8436574450033', 'pa_color', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(2, 1, '8436574450040', 'pa_color', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(3, 2, '8436574450019', 'pa_color', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(4, 2, '8436574450026', 'pa_color', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(5, 8, '8436574450026', 'pa_color-w2-01', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(6, 8, '8436574450019', 'pa_color-w2-01', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(7, 8, '8436574450026', 'pa_color-w2-02', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(8, 8, '8436574450019', 'pa_color-w2-02', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(9, 8, '8436574450026', 'pa_color-w2-03', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(10, 8, '8436574450019', 'pa_color-w2-03', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(11, 8, '8436574450026', 'pa_color-w2-04', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(12, 8, '8436574450019', 'pa_color-w2-04', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(13, 8, '8436574450033', 'pa_color-w5-01', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(14, 8, '8436574450040', 'pa_color-w5-01', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(15, 8, '8436574450033', 'pa_color-w5-02', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(16, 8, '8436574450040', 'pa_color-w5-02', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(17, 8, '8436574450033', 'pa_color-w5-03', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(18, 8, '8436574450040', 'pa_color-w5-03', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(19, 3, '8436574450019', 'pa_color', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(20, 3, '8436574450026', 'pa_color', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(21, 4, '8436574450033', 'pa_color', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(22, 4, '8436574450040', 'pa_color', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(23, 5, '8436574450033', 'pa_color-w5-01', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(24, 5, '8436574450040', 'pa_color-w5-01', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(27, 5, '8436574450026', 'pa_color-w2-01', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(28, 5, '8436574450019', 'pa_color-w2-01', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(29, 6, '8436574450033', 'pa_color-w5-02', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(30, 6, '8436574450040', 'pa_color-w5-02', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(31, 6, '8436574450033', 'pa_color-w5-01', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(32, 6, '8436574450040', 'pa_color-w5-01', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(33, 6, '8436574450026', 'pa_color-w2-01', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(34, 6, '8436574450019', 'pa_color-w2-01', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(35, 7, '8436574450026', 'pa_color-w2-01', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(36, 7, '8436574450019', 'pa_color-w2-01', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(37, 7, '8436574450026', 'pa_color-w2-02', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(38, 7, '8436574450019', 'pa_color-w2-02', 'space-gray', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(39, 7, '8436574450033', 'pa_color-w5-01', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(40, 7, '8436574450040', 'pa_color-w5-01', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(41, 7, '8436574450033', 'pa_color-w5-02', 'space-gray', 1, 1000, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(42, 7, '8436574450040', 'pa_color-w5-02', 'champagne-gold', 0, 0, 'color', 'W5');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(43, 7, '8436574450026', 'pa_color-w2-03', 'champagne-gold', 1, 1000, 'color', 'W2');
INSERT INTO MLM.bis_store_items_options
(id, store_item_id, ean_code, name, value, in_stock, stock, option_name, product_name)
VALUES(44, 7, '8436574450019', 'pa_color-w2-03', 'space-gray', 1, 1000, 'color', 'W2');
