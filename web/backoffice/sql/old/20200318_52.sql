create or replace
algorithm = UNDEFINED view `vw_members` as
select
    `bm`.`id` as `id`,
    `o`.`creation_date` as `registration_date`,
    `o`.`activation_date` as `activation_date`,
    `bm`.`country` as `country`,
    concat(`bm`.`first_name`, ' ', `bm`.`last_name`) as `full_name`,
    `o`.`store_title` as `kit`,
    `br`.`name` as `rank_name`,
    (case
        when ((`bm`.`country` = 'ES')
        or (`bm`.`country` = 'CO')
        or (`bm`.`country` = 'PE')
        or (`bm`.`country` = 'EC')) then `bm`.`country`
        else 'OT' end) as `country_final`
from
    ((`bis_members` `bm`
join `bis_orders` `o` on
    ((`bm`.`order_id` = `o`.`id`)))
join `bis_ranks` `br` on
    ((`bm`.`rank_id` = `br`.`id`)))
where
    ((`bm`.`deleted` = 0)
    and (`bm`.`order_id` is not null)
    and (`o`.`deleted` = 0))