create or replace
algorithm = UNDEFINED view `vw_orders_paid_diamond` as
select
    count(1) as `size`,
    'Diamond' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (((`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 1))
    or (`bo`.`installment_number` = 0)
    or (`bo`.`installment_number` is null)))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_paid_golden` as
select
    count(1) as `size`,
    'Golden' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (((`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 1))
    or (`bo`.`installment_number` = 0)
    or (`bo`.`installment_number` is null)))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_first_diamond` as
select
    count(1) as `size`,
    'Diamond' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_first_golden` as
select
    count(1) as `size`,
    'Golden' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_second_diamond` as
select
    count(1) as `size`,
    'Diamond' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0)
    and `bo`.`id` in (
    select
        `o`.`external_id`
    from
        `bis_orders` `o`
    where
        ((`o`.`status` = 'paid')
        and (`o`.`deleted` = 0)
        and (`o`.`installment_number` = 2)
        and (`o`.`item_id` = `bo`.`item_id`))))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_second_golden` as
select
    count(1) as `size`,
    'Golden' as `founder`,
    `bo`.`billing_country` as `billing_country`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0)
    and `bo`.`id` in (
    select
        `o`.`external_id`
    from
        `bis_orders` `o`
    where
        ((`o`.`status` = 'paid')
        and (`o`.`deleted` = 0)
        and (`o`.`installment_number` = 2)
        and (`o`.`item_id` = `bo`.`item_id`))))
group by
    `bo`.`billing_country`
order by
    `bo`.`billing_country` desc;