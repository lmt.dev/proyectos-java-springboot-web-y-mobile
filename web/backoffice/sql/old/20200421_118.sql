DROP TABLE `bis_topups_provider`;
CREATE TABLE `bis_topups_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider` varchar(200),
  `reference` varchar(200),
  `amount` decimal(20,6),
  `currency` varchar(100),
  `creation_date` TIMESTAMP NOT NULL,
  `msisdn` varchar(200),
  `iccid` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE UNIQUE INDEX bis_topups_provider_provider_IDX USING BTREE ON MLM.bis_topups_provider (provider,reference,creation_date,iccid,msisdn);
