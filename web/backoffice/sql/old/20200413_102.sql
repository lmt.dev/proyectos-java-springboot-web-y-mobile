create or replace
algorithm = UNDEFINED view `vw_ordersactives` as
select
    `o`.`id` as `order_Id`,
    (case
        when (`o`.`type` = 'upgrade') then `si`.`final_item_id`
        else `o`.`item_id` end) as `item_id`,
    `o`.`type` as `type`,
    `o`.`store_title` as `store_title`,
    `o`.`billing_country` as `billing_country`,
    `o`.`activation_date` as `activation_date`,
    `o`.`member_id` as `member_id`,
    `o`.`installment_complete` as `installment_complete`
from
    (`bis_orders` `o`
join `bis_store_items` `si` on
    ((`o`.`item_id` = `si`.`id`)))
where
    ((`o`.`status` = 'paid')
    and (`o`.`deleted` = 0)
    and (`o`.`item_id` <> 15)
    and ((`o`.`installment_number` is null)
    or (`o`.`installment_number` in (0,
    1))))
order by
    `o`.`activation_date`;

-- CORRER SCRIPT PARA ACTUALIZAR LAS ORDENES QUE ESTAN PAGADAS Y NO SON EN CUOTAS SI ES QUE HACE FALTA
-- update bis_orders set installment_complete = 1 where deleted = 0 and installment_number  = 0 and status = 'paid';