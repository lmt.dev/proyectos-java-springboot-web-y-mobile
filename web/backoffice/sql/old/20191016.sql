DROP TABLE `bis_tools_viralmedia`;
CREATE TABLE `bis_tools_viralmedia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(150),
  `member_id` bigint(20),
  `taken` tinyint(1) default 0,
  `taken_date` DATETIME default null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

