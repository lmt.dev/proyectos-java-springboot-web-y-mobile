alter table bis_orders add column shipping_name varchar(200);
alter table bis_orders add column shipping_address varchar(200);
alter table bis_orders add column shipping_city varchar(200);
alter table bis_orders add column shipping_state varchar(200);
alter table bis_orders add column shipping_country varchar(200);
alter table bis_orders add column shipping_postal_code varchar(200);
alter table bis_orders add column shipping_extra varchar(500);
alter table bis_orders add column dni_type varchar(50);

alter table bis_order_items add column complex_name varchar(150);
alter table bis_order_items add column simple_name varchar(150);
alter table bis_order_items add column complex_id BIGINT(20);
alter table bis_order_items add column simple_id BIGINT(20);
alter table bis_order_items add column item_option_id BIGINT(20);
alter table bis_order_items add column item_option_selected varchar(150);

alter table bis_store_items modify column short_description varchar(150);

alter table bis_order_items drop column quantity;
alter table bis_order_items add column quantity int not null default 0;