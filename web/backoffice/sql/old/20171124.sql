ALTER TABLE bis_store_items ADD stock BIGINT NULL;
update bis_store_items set stock = -2 where category = 1;
update bis_store_items set stock = -1 where category = 3;

DROP TABLE bis_shipments;
CREATE TABLE `bis_shipments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NULL DEFAULT NULL,
  `sended_date` timestamp NULL DEFAULT NULL,
  `modification_date` timestamp NULL DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `shipment_number` varchar(150) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `shipping_name` varchar(100) DEFAULT NULL,
  `shipping_address` varchar(100) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_postal_code` varchar(100) DEFAULT NULL,
  `shipping_country` varchar(100) DEFAULT NULL,
  `tracking_number` varchar(100) DEFAULT NULL,
  `last_tracking_activity` varchar(200),
  `pdf_path` varchar(200),
  `xml_path` varchar(200),
  `phone_number` varchar(100),
  `email` varchar(100),
  `username` varchar(100),
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `deliverable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bis_shipment_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` bigint(20) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_ean` varchar(50) DEFAULT NULL,
  `item_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;