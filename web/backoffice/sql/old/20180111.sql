alter table bis_orders add column `billing_name` varchar(200);    
alter table bis_orders add column `billing_address` varchar(250);    
alter table bis_orders add column `billing_city` varchar(150);    
alter table bis_orders add column `billing_postal_code` varchar(150);    
alter table bis_orders add column `billing_state` varchar(150);    
alter table bis_orders add column `billing_country` varchar(150);
alter table bis_orders add column `billing_company` varchar(150);
alter table bis_orders add column `invoice_id` bigint default null;

