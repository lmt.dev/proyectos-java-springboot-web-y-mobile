
drop table MLM.bis_calification;
CREATE TABLE MLM.bis_calification (
	id BIGINT NOT NULL AUTO_INCREMENT,
	`date` TIMESTAMP NOT NULL,
	member_id BIGINT NULL,
	event_id BIGINT NULL,
	description varchar(200) NULL,
	points DOUBLE NULL,
	CONSTRAINT bis_calification_PK PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

ALTER TABLE MLM.bis_members ADD initial_rank_id int(11) NULL ;
ALTER TABLE MLM.bis_members CHANGE initial_rank_id initial_rank_id int(11) NULL AFTER rank_id ;
ALTER TABLE MLM.bis_differential MODIFY COLUMN `date` TIMESTAMP NOT NULL ;


