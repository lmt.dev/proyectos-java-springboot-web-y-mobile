ALTER TABLE `MLM`.`bis_orders` 
ADD COLUMN `payment_attachment` VARCHAR(200) NULL DEFAULT NULL AFTER `installment_number`,
ADD COLUMN `payment_ticket_number` VARCHAR(200) NULL DEFAULT NULL AFTER `payment_attachment`;

