drop table bis_store_item_names;
create table bis_store_item_names(
`id` bigint AUTO_INCREMENT,
`item_id` bigint,
`item_name` varchar(200),
 PRIMARY KEY (`id`),
 UNIQUE KEY `bis_store_item_names_UN` (`item_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
-- 	VALUES (1, 'TELÉFONO W5');
-- INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
-- 	VALUES (2, 'TELÉFONO W2');
-- INSERT INTO MLM.bis_store_item_names (item_id, item_name) 
-- 	VALUES (8, 'FOUNDER WINGS + CRUCERO');

INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WPOP', 'Wings POP', NULL, 2, 449.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WPOPPRO', 'Wings POP PRO', NULL, 2, 599.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WBOOK', 'Wings BOOK', NULL, 2, 399.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WBOOKPRO', 'Wings BOOK PRO', NULL, 2, 549.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WONEFOUR', 'Wings ONEFOUR', NULL, 2, 449.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('WONEFOURPRO', 'Wings ONEFOUR PRO', NULL, 2, 599.00, 0.00, 0.00, NULL);
INSERT INTO MLM.bis_store_items (code, `name`, description, category, price, rank_points, pay_points, stock) 
	VALUES ('STARTERx2', '2 TELÉFONOS W2', NULL, 1, 399.00, 0.00, 0.00, NULL);




alter table bis_orders add column deleted tinyint(1) not null default 0;
alter table bis_orders add column store_title varchar(200) default null;


update bis_orders set payment_type = 'pagantis' where payment_type = 'Paga+Tarde';
update bis_orders set payment_type = 'tefpay' where payment_type = 'TefPay';
update bis_orders set payment_type = 'bacs' where payment_type = 'Transferencia Bancaria';
update bis_orders set payment_type = 'tefpay' where payment_type is null and `type` = 'service';
update bis_orders set payment_type = 'manual' where payment_type = 'paid';

alter table bis_members add column company_name varchar(200) default null;
alter table bis_members add column source varchar(100) not null default 'legacy';


