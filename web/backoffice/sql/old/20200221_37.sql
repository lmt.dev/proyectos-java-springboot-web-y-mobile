CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `vw_ordersActives` AS
select
    `o`.`id` AS `order_Id`,
    (case
        when (`o`.`type` = 'upgrade') then `si`.`final_item_id`
        else `o`.`item_id` end) AS `item_id`,
    `o`.`type` AS `type`,
    `o`.`store_title` AS `store_title`,
    `o`.`billing_country` AS `billing_country`,
    `o`.`activation_date` AS `activation_date`,
    `o`.`member_id` AS `member_id`
from
    (`bis_orders` `o`
join `bis_store_items` `si` on
    ((`o`.`item_id` = `si`.`id`)))
where
    ((`o`.`status` = 'paid')
    and (`o`.`deleted` = 0)
    and (`o`.`item_id` <> 15)
    and (isnull(`o`.`installment_number`)
    or (`o`.`installment_number` in (0,
    1))))
order by
    `o`.`activation_date`;
