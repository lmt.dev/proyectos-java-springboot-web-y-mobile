CREATE OR REPLACE
    ALGORITHM = UNDEFINED VIEW `MLM`.`vw_members` AS
select
    `bm`.`id` AS `id`,
    `bm`.`registration_date` AS `registration_date`,
    `o`.`activation_date` AS `activation_date`,
    `bm`.`country` AS `country`,
    concat(`bm`.`first_name`, ' ', `bm`.`last_name`) AS `full_name`,
    `o`.`store_title` AS `kit`,
    `br`.`name` AS `rank_name`,
    (case
         when ((`bm`.`country` = 'ES')
             or (`bm`.`country` = 'CO')
             or (`bm`.`country` = 'PE')
             or (`bm`.`country` = 'EC')) then `bm`.`country`
         else 'OT' end) AS `country_final`
from
    ((`MLM`.`bis_members` `bm`
        join `MLM`.`bis_orders` `o` on
            ((`bm`.`order_id` = `o`.`id`)))
        join `MLM`.`bis_ranks` `br` on
        ((`bm`.`rank_id` = `br`.`id`)))
where
    ((`bm`.`deleted` = 0)
        and (`bm`.`order_id` is not null)
        and (`o`.`deleted` = 0));