/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lucas
 * Created: 28-abr-2020
 */

create or replace
algorithm = UNDEFINED view `vw_orders_paid_diamond` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Diamond' as `Founder`,
    'Complete' as 'Mark'
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (((`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 1))
    or (`bo`.`installment_number` = 0)
    or (`bo`.`installment_number` is null)))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_paid_golden` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Golden' as `Founder`,
    'Complete' as 'Mark'
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (((`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 1))
    or (`bo`.`installment_number` = 0)
    or (`bo`.`installment_number` is null)))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_first_diamond` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Diamond' as `Founder`,
    'First' as 'Mark'
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_first_golden` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Golden' as `Founder`,
    'First' as 'Mark'
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_second_diamond` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Diamond' as `Founder`,
    'Second' as `Mark`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (8,
    54,
    55,
    56,
    57,
    58,
    59,
    98,
    75,
    88,
    95,
    199,
    211,
    319,
    320,
    321,
    322,
    323,
    324,
    104,
    101,
    107,
    108,
    109,
    112,
    115))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0)
    and `bo`.`id` in (
    select
        `o`.`external_id`
    from
        `bis_orders` `o`
    where
        ((`o`.`status` = 'paid')
        and (`o`.`deleted` = 0)
        and (`o`.`installment_number` = 2)
        and (`o`.`item_id` = `bo`.`item_id`))))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_orders_second_golden` as
select
    `bo`.`id` as `id`,
    `bo`.`creation_date` as `creation_date`,
    `bo`.`activation_date` as `activation_date`,
    `bo`.`item_id` as `item_id`,
    `bo`.`status` as `status`,
    `bo`.`payment_status` as `payment_status`,
    `bo`.`payer_email` as `payer_email`,
    `bo`.`billing_name` as `client`,
    `bo`.`member_id` as `member_id`,
    `bo`.`store_title` as `store_title`,
    `bo`.`billing_country` as `billing_country`,
    'Golden' as `Founder`,
    'Second' as `Mark`
from
    `bis_orders` `bo`
where
    ((`bo`.`item_id` in (68,
    73,
    74,
    86,
    87,
    93,
    94,
    60,
    61,
    62,
    63,
    96,
    97,
    99,
    100,
    102,
    103,
    105,
    106,
    110,
    111,
    113,
    114,
    130,
    139,
    275,
    276,
    129,
    138,
    198,
    210,
    264,
    265,
    266,
    267,
    302,
    303,
    304,
    305,
    306,
    307,
    309,
    310,
    311,
    312,
    314,
    316,
    317,
    318))
    and (`bo`.`status` = 'paid')
    and (`bo`.`deleted` = 0)
    and (`bo`.`installment_number` = 1)
    and (`bo`.`installment_complete` = 0)
    and `bo`.`id` in (
    select
        `o`.`external_id`
    from
        `bis_orders` `o`
    where
        ((`o`.`status` = 'paid')
        and (`o`.`deleted` = 0)
        and (`o`.`installment_number` = 2)
        and (`o`.`item_id` = `bo`.`item_id`))))
group by
    `bo`.`billing_country`
order by
    `bo`.`activation_date` desc;

create or replace
algorithm = UNDEFINED view `vw_report_founder` as
select
    `vopd`.`id` as `id`,
    `vopd`.`creation_date` as `creation_date`,
    `vopd`.`activation_date` as `activation_date`,
    `vopd`.`item_id` as `item_id`,
    `vopd`.`status` as `status`,
    `vopd`.`payment_status` as `payment_status`,
    `vopd`.`payer_email` as `payer_email`,
    `vopd`.`client` as `client`,
    `vopd`.`member_id` as `member_id`,
    `vopd`.`store_title` as `store_title`,
    `vopd`.`billing_country` as `billing_country`,
    `vopd`.`Founder` as `Founder`,
    `vopd`.`Mark` as `Mark`
from
    `vw_orders_paid_diamond` `vopd`
union
select
    `vopg`.`id` as `id`,
    `vopg`.`creation_date` as `creation_date`,
    `vopg`.`activation_date` as `activation_date`,
    `vopg`.`item_id` as `item_id`,
    `vopg`.`status` as `status`,
    `vopg`.`payment_status` as `payment_status`,
    `vopg`.`payer_email` as `payer_email`,
    `vopg`.`client` as `client`,
    `vopg`.`member_id` as `member_id`,
    `vopg`.`store_title` as `store_title`,
    `vopg`.`billing_country` as `billing_country`,
    `vopg`.`Founder` as `Founder`,
    `vopg`.`Mark` as `Mark`
from
    `vw_orders_paid_golden` `vopg`
union
select
    `vofd`.`id` as `id`,
    `vofd`.`creation_date` as `creation_date`,
    `vofd`.`activation_date` as `activation_date`,
    `vofd`.`item_id` as `item_id`,
    `vofd`.`status` as `status`,
    `vofd`.`payment_status` as `payment_status`,
    `vofd`.`payer_email` as `payer_email`,
    `vofd`.`client` as `client`,
    `vofd`.`member_id` as `member_id`,
    `vofd`.`store_title` as `store_title`,
    `vofd`.`billing_country` as `billing_country`,
    `vofd`.`Founder` as `Founder`,
    `vofd`.`Mark` as `Mark`
from
    `vw_orders_first_diamond` `vofd`
union
select
    `vofg`.`id` as `id`,
    `vofg`.`creation_date` as `creation_date`,
    `vofg`.`activation_date` as `activation_date`,
    `vofg`.`item_id` as `item_id`,
    `vofg`.`status` as `status`,
    `vofg`.`payment_status` as `payment_status`,
    `vofg`.`payer_email` as `payer_email`,
    `vofg`.`client` as `client`,
    `vofg`.`member_id` as `member_id`,
    `vofg`.`store_title` as `store_title`,
    `vofg`.`billing_country` as `billing_country`,
    `vofg`.`Founder` as `Founder`,
    `vofg`.`Mark` as `Mark`
from
    `vw_orders_first_golden` `vofg`
union
select
    `vosd`.`id` as `id`,
    `vosd`.`creation_date` as `creation_date`,
    `vosd`.`activation_date` as `activation_date`,
    `vosd`.`item_id` as `item_id`,
    `vosd`.`status` as `status`,
    `vosd`.`payment_status` as `payment_status`,
    `vosd`.`payer_email` as `payer_email`,
    `vosd`.`client` as `client`,
    `vosd`.`member_id` as `member_id`,
    `vosd`.`store_title` as `store_title`,
    `vosd`.`billing_country` as `billing_country`,
    `vosd`.`Founder` as `Founder`,
    `vosd`.`Mark` as `Mark`
from
    `vw_orders_second_diamond` `vosd`
union
select
    `vosg`.`id` as `id`,
    `vosg`.`creation_date` as `creation_date`,
    `vosg`.`activation_date` as `activation_date`,
    `vosg`.`item_id` as `item_id`,
    `vosg`.`status` as `status`,
    `vosg`.`payment_status` as `payment_status`,
    `vosg`.`payer_email` as `payer_email`,
    `vosg`.`client` as `client`,
    `vosg`.`member_id` as `member_id`,
    `vosg`.`store_title` as `store_title`,
    `vosg`.`billing_country` as `billing_country`,
    `vosg`.`Founder` as `Founder`,
    `vosg`.`Mark` as `Mark`
from
    `vw_orders_second_golden` `vosg`;