CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `vw_members` AS
select
    `bm`.`id` AS `id`,
    `bm`.`registration_date` AS `registration_date`,
    `o`.`activation_date` AS `activation_date`,
    `bm`.`country` AS `country`,
    (case
        when ((`bm`.`country` = 'ES')
        or (`bm`.`country` = 'CO')
        or (`bm`.`country` = 'PE')
        or (`bm`.`country` = 'EC')) then `bm`.`country`
        else 'OT' end) AS `country_final`
from
    (`bis_members` `bm`
join `bis_orders` `o` on
    ((`bm`.`order_id` = `o`.`id`)))
where
    ((`bm`.`deleted` = 0)
    and (`bm`.`order_id` is not null)
    and (`o`.`deleted` = 0))
