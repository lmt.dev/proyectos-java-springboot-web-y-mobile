alter table bis_balance add column order_id bigint(20) default null;

DROP TABLE `bis_withdrawals`;
CREATE TABLE `bis_withdrawals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_date` TIMESTAMP NOT NULL,
  `completed_date` DATETIME DEFAULT NULL,
  `completed` tinyint(1) default 0,
  `status` varchar(50) NOT NULL,
  `status_description` varchar(250) DEFAULT NULL,
  `member_id` bigint(20) default NULL,
  `account_name` varchar(250) default null,
  `account_number` varchar(250) default null,
  `processor` varchar(50) default null,
  `amount` decimal(10,2) default null,
  `money_base` decimal(10,2) default null,
  `money_tax` decimal(10,2) default null,
  `money_retention` decimal(10,2) default null,
  `money_payable` decimal(10,2) default null,
  `request_user` varchar(250) default null,
  `request_description` varchar(250) default null,
  `completed_user` varchar(250) default null,
  `completed_description` varchar(250) default null,
  `iprf` tinyint(1) default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table bis_speed_bonus add column exclude_client_count tinyint(1) default 0;