CREATE UNIQUE INDEX bis_member_sims_iccid_IDX USING BTREE ON bis_member_sims (iccid);
alter table bis_member_sims add column transfer_member_id bigint(20) default null;
alter table bis_member_sims add column transfer_member_name varchar(500) default null;


update bis_member_sims s set s.member_id = (select member_id from bis_orders o where o.id = s.order_id and o.member_id is not null) where s.member_id is null;
