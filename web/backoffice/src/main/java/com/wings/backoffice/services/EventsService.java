/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemI18n;
import com.wings.backoffice.mlm.domain.events.Event;
import com.wings.backoffice.mlm.domain.events.EventExtra;
import com.wings.backoffice.mlm.domain.events.EventHost;
import com.wings.backoffice.mlm.domain.events.EventHostPermission;
import com.wings.backoffice.mlm.domain.events.EventInfo;
import com.wings.backoffice.mlm.domain.events.EventProduct;
import com.wings.backoffice.mlm.domain.events.EventSeat;
import com.wings.backoffice.mlm.domain.events.EventSeatsPool;
import com.wings.backoffice.mlm.domain.events.EventTicket;
import com.wings.backoffice.mlm.domain.events.EventTicketAssignation;
import com.wings.backoffice.mlm.domain.events.EventTicketInfo;
import com.wings.backoffice.mlm.domain.events.EventsProductPurchase;
import com.wings.backoffice.mlm.domain.events.enums.EventProductPurchaseStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventProductStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventTicketStatus;
import com.wings.backoffice.mlm.enums.OrderPaymentStatus;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.ShipmentStatus;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.StoreItemI18nRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.mlm.repos.events.EventExtrasRepository;
import com.wings.backoffice.mlm.repos.events.EventHostsPermissionRepository;
import com.wings.backoffice.mlm.repos.events.EventHostsRepository;
import com.wings.backoffice.mlm.repos.events.EventInfoRepository;
import com.wings.backoffice.mlm.repos.events.EventProductPurchaseRepository;
import com.wings.backoffice.mlm.repos.events.EventProductRepository;
import com.wings.backoffice.mlm.repos.events.EventSeatRepository;
import com.wings.backoffice.mlm.repos.events.EventSeatsPoolRepository;
import com.wings.backoffice.mlm.repos.events.EventTicketsAssignationRepository;
import com.wings.backoffice.mlm.repos.events.EventTicketsInfoRepository;
import com.wings.backoffice.mlm.repos.events.EventTicketsRepository;
import com.wings.backoffice.mlm.repos.events.EventsRepository;
import com.wings.backoffice.mlm.specs.EventsSpecs;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author seba
 */
@Service
public class EventsService {

    @Autowired
    private Environment environment;

    @Autowired
    private EventHostsPermissionRepository eventHostsPermissionRepository;
    @Autowired
    private EventInfoRepository eventInfoRepository;
    @Autowired
    private EventProductRepository eventProductRepository;
    @Autowired
    private EventSeatsPoolRepository eventSeatsPoolRepository;
    @Autowired
    private EventTicketsInfoRepository eventTicketsInfoRepository;
    @Autowired
    private EventHostsRepository eventHostsRepository;
    @Autowired
    private EventProductPurchaseRepository eventProductPurchaseRepository;
    @Autowired
    private EventSeatRepository eventSeatRepository;
    @Autowired
    private EventsRepository eventsRepository;
    @Autowired
    private EventTicketsRepository eventTicketsRepository;
    @Autowired
    private EventExtrasRepository eventExtrasRepository;
    @Autowired
    private EventTicketsAssignationRepository eventTicketsAssignationRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CountriesService countriesService;
    @Autowired
    private StoreItemI18nRepository storeItemI18nRepository;

    private final Logger log = LoggerFactory.getLogger(EventsService.class);

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param status
     * @return
     */
    public Page<Event> searchEvents(int page, int size, String q, String status) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));
        Page<Event> items = eventsRepository.findAll(EventsSpecs.eventSearch(q, status), pr);
        return items;
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param status
     * @param eventId
     * @return
     */
    public Page<EventTicket> searchTickets(int page, int size, String q, String status, Long eventId) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        Page<EventTicket> items;
        if (status == null || status.equals("all")) {
            items = eventTicketsRepository.findAllByEventId(eventId, pr);
        } else {
            items = eventTicketsRepository.findAllByEventIdAndStatus(eventId, status, pr);
        }
        return items;
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param status
     * @param eventId
     * @return
     */
    public Page<EventSeat> searchSeats(int page, int size, String q, String status, Long eventId) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        Page<EventSeat> items = eventSeatRepository.findAllByEventId(eventId, pr);
        return items;
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param status
     * @param eventId
     * @return
     */
    public Page<EventsProductPurchase> searchPurchases(int page, int size, String q, String status, Long eventId) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        Page<EventsProductPurchase> items = eventProductPurchaseRepository.findByEventId(eventId, pr);
        return items;
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param status
     * @param eventId
     * @return
     */
    public Page<EventProduct> searchProducts(int page, int size, String q, String status, Long eventId) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        Page<EventProduct> items = eventProductRepository.findAllByEventId(eventId, pr);
        return items;
    }

    /**
     *
     * @param eventId
     * @return
     */
    public Event getEvent(Long eventId) {
        return eventsRepository.getOne(eventId);
    }

    /**
     *
     * @return
     */
    public List<Event> getEventsPublished() {
        return eventsRepository.findByStatus(EventStatus.published.name());
    }

    /**
     *
     * @param shortName
     * @return
     */
    public Event createEvent(String shortName) {
        Event e = new Event();
        e.setCreationDate(new Date());
        e.setShortName(shortName);
        e.setStatus(EventStatus.created.name());
        eventsRepository.save(e);
        return e;
    }

    /**
     *
     * @param eventId
     * @return
     */
    public Map<String, Long> getStats(Long eventId) {
        long tickeCount = eventTicketsRepository.countByEventId(eventId);
        long seatsCount = eventSeatRepository.countByEventId(eventId);
        long productsCount = eventProductRepository.countByEventId(eventId);
        long productsPurchasesCount = eventProductPurchaseRepository.countByEventId(eventId);
        long usersCount = eventHostsPermissionRepository.countByEventId(eventId);
        Map<String, Long> stats = new HashMap<>();
        stats.put("tickets", tickeCount);
        stats.put("seats", seatsCount);
        stats.put("products", productsCount);
        stats.put("purchases", productsPurchasesCount);
        stats.put("users", usersCount);
        return stats;
    }

    /**
     *
     * @param eventId
     * @return
     */
    public Map<String, String> getStats2(Long eventId) {

        Event event = getEvent(eventId);
        Date creationDate = event.getCreationDate();
        Date eventDate = event.getEventDate();

        long remainig = 0;
        String remainingDays = "";
        if (eventDate != null) {
            //diferencia de dias
            long dfc = DateUtils.getDiffDays(creationDate, eventDate);
            long dfn = DateUtils.getDiffDays(new Date(), eventDate);
            if (dfc > 0) {
                remainig = (((dfn * 100l) / dfc) - 100) * -1;
                remainingDays = dfn + " dias";
                if (dfn == 1) {
                    remainingDays = dfn + " día";
                }
            }
        }

        Map<String, String> stats = new HashMap<>();
        stats.put("remaining", String.valueOf(remainig));
        stats.put("remainingDays", remainingDays);

        int totalTickets = eventTicketsRepository.countByEventId(eventId);
        List<String> asList = Arrays.asList(EventTicketStatus.created.name());
        int totalSell = eventTicketsRepository.countByEventIdAndStatusNotIn(eventId, asList);
        int totalReg = eventTicketsRepository.countByEventIdAndStatus(eventId, EventTicketStatus.reclaimed.name());
        int totalAssi = eventTicketsRepository.countByEventIdAndStatus(eventId, EventTicketStatus.completed.name());

        long sells = 0;
        long regs = 0;
        long ass = 0;
        if (totalTickets > 0) {
            sells = (totalSell * 100) / totalTickets;
            regs = (totalReg * 100) / totalTickets;
            ass = (totalAssi * 100) / totalTickets;
        }

        stats.put("sells", String.valueOf(sells));
        stats.put("sellsText", totalSell + "/" + totalTickets);
        stats.put("regs", String.valueOf(regs));
        stats.put("regsText", totalReg + "/" + totalTickets);
        stats.put("ass", String.valueOf(ass));
        stats.put("assText", totalAssi + "/" + totalTickets);

        return stats;
    }

    /**
     * check if event is publicable, return null if event can be published
     *
     * @param eventId
     * @return
     */
    public String checkPublishAvailability(Long eventId) {

        Event e = getEvent(eventId);
        //verificamos que tenga tickets creados
        Map<String, Long> stats = getStats(eventId);
        if (stats.get("tickets") <= 0) {
            return "El evento no tiene tickets creados";
        }

        //verificamos que tenga productos creados
        if (stats.get("products") <= 0) {
            return "El evento no tiene productos creados";
        }

        if (e.getSeatsNumbers() && stats.get("seats") <= 0) {
            return "El evento no tiene asientos creados, sin embargo tiene habilitada la opción Numeracion de asientos";
        }

        if (e.getDate() == null || e.getDate().isEmpty()) {
            return "El evento no tiene fecha";
        }

        if (e.getHour() == null || e.getHour().isEmpty()) {
            return "El evento no tiene hora";
        }

        if (e.getName() == null || e.getName().isEmpty()) {
            return "El evento no tiene nombre";
        }

        if (e.getPlace() == null || e.getPlace().isEmpty()) {
            return "El evento no tiene lugar";
        }

        if (e.getPlaceAddress() == null || e.getPlaceAddress().isEmpty()) {
            return "El evento no tiene dirección";
        }

        /*if (e.getAvailableCountries() == null || e.getAvailableCountries().isEmpty()) {
            return "El evento no tiene paises definidos";
        }*/
        if (e.getTicketNumbersFrom() == null || e.getTicketNumbersFrom() <= 0) {
            return "El evento no tiene definido la cantidad de tickets";
        }

        return null;
    }

    /**
     *
     * @param e
     */
    public void saveEvent(Event e) {
        eventsRepository.save(e);
    }

    /**
     * Creates tickets for this event, deletes previous
     *
     * @param e
     * @throws Exception
     */
    public void regenerateTickets(Event e) throws Exception {

        //si se publico, alpiste
        if (!e.getStatus().equals(EventStatus.created.name())) {
            throw new Exception("No se pueden regenerar los tickets, el evento ya se publicó");
        }

        eventTicketsRepository.deleteByEventId(e.getId());

        int cant = e.getTicketNumbersFrom();
        Date creationDate = new Date();

        //basico, de inicio a fin 100 a 200 etc
        List<EventTicket> toSave = new LinkedList<>();
        int prefixSize = 3;
        if (e.getId().toString().length() > 3) {
            prefixSize = e.getId().toString().length() + 1;
        }

        String prefixPattern = "%0" + prefixSize + "d";
        String suffixPattern = "%05d";
        String prefix = String.format(prefixPattern, e.getId());

        for (int i = 1; i < cant + 1; i++) {
            String suffix = String.format(suffixPattern, i);
            EventTicket et = new EventTicket();
            et.setEventId(e.getId());
            et.setCreationDate(creationDate);
            et.setInviteCode(StringUtils.generateRandomHash(prefix + "-" + suffix));
            et.setTicketCode(StringUtils.generateRandomHash(prefix + "-" + suffix + "1"));
            et.setStatus(EventTicketStatus.created.name());
            et.setTicketNumber(prefix + "-" + suffix);
            toSave.add(et);
        }

        eventTicketsRepository.save(toSave);
    }

    /**
     * Genera Numeros de asientos para este pool en particular
     *
     * @param pool
     */
    public void generateEventSeats(EventSeatsPool pool) {

        String pattern = pool.getPattern();
        int qty = pool.getSeatsQuantity();

        char[] chars = pattern.toCharArray();
        int pounds = 0;
        String toReplace = "";
        for (char aChar : chars) {
            if (aChar == '#') {
                pounds++;
                toReplace += "#";
            }
        }

        List<EventSeat> seatsToSave = new LinkedList<>();
        String format = "%0" + pounds + "d";
        Date creationDate = new Date();
        for (int i = 1; i < qty + 1; i++) {

            EventSeat seat = new EventSeat();
            seat.setCreationDate(creationDate);
            seat.setEventId(pool.getEventId());
            seat.setPoolId(pool.getId());
            String seatNumber = pattern.replaceAll(toReplace, String.format(format, i));
            seat.setSeatNumber(seatNumber);
            seat.setTaken(Boolean.FALSE);
            seat.setTakenDate(null);

            seatsToSave.add(seat);
        }

        eventSeatRepository.save(seatsToSave);
    }

    /**
     *
     * @param e
     * @param i
     */
    public void saveEventInfo(Event e, EventInfo i) {
        i.setEventId(e.getId());
        eventInfoRepository.save(i);
    }

    /**
     *
     * @param e
     * @param ex
     */
    public void saveEventExtra(Event e, EventExtra ex) {
        ex.setEventId(e.getId());
        eventExtrasRepository.save(ex);
    }

    /**
     *
     * @param pool
     */
    public void saveSeatsPool(EventSeatsPool pool) {
        eventSeatsPoolRepository.save(pool);
        generateEventSeats(pool);
    }

    /**
     *
     * @param product
     */
    public void saveProduct(EventProduct product) {

        Event e = getEvent(product.getEventId());
        List<String> countries = countriesService.getEnabledCountries();

        StoreItem item = new StoreItem();
        item.setAvailableTo(null);
        item.setCategory(5);
        item.setCategoryName("ticket");
        item.setCode(product.getShortName());
        item.setDescription("Entradas evento " + e.getName() + " cantidad: " + product.getQuantity());
        item.setEnabled(false);
        item.setFinalItemId(null);
        String image = getSiteUrl() + "/open/event/" + e.getImage();
        item.setImage(image);
        item.setName(product.getName());
        item.setPayPoints(0.0);
        item.setRankPoints(0.0);
        item.setSource("store-int");
        item.setPrice(product.getPrice());
        item.setPriceForComs(product.getDiscountPrice());
        item.setCountries(countries.toString());

        //save store item
        storeItemsRepository.save(item);

        //add i18n
        List<StoreItemI18n> i18ns = new ArrayList<>();
        for (String country : countries) {
            StoreItemI18n i18n = new StoreItemI18n();
            i18n.setCountry(country);
            i18n.setStoreItemId(item.getId());
            i18n.setPrice(product.getPrice());
            i18ns.add(i18n);
        }
        storeItemI18nRepository.save(i18ns);

        product.setStoreItemId(item.getId());
        eventProductRepository.save(product);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventSeatsPool> getSeatsPool(Long eventId) {
        return eventSeatsPoolRepository.findByEventId(eventId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventInfo> getEventInfos(Long eventId) {
        return eventInfoRepository.findByEventId(eventId);
    }

    /**
     *
     * @param ticketId
     * @return
     */
    public List<EventTicketInfo> getEventTicketInfo(Long ticketId) {
        return eventTicketsInfoRepository.findByTicketId(ticketId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventExtra> getEventExtras(Long eventId) {
        return eventExtrasRepository.findByEventId(eventId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventTicket> getEventTickets(Long eventId) {
        return eventTicketsRepository.findByEventId(eventId);
    }

    /**
     *
     * @param eventId
     * @param reclaimed
     * @return
     */
    public List<EventTicket> getEventTicketsReclaimedAfter(Long eventId, Date reclaimed) {
        return eventTicketsRepository.findByEventIdAndReclaimDateGreaterThan(eventId, reclaimed);
    }

    /**
     *
     * @param ticketCode
     * @return
     */
    public EventTicket getEventTicket(String ticketCode) {
        return eventTicketsRepository.findByTicketCode(ticketCode);
    }

    /**
     *
     * @param ticketNumber
     * @return
     */
    public EventTicket getEventTicketByTicketNumber(String ticketNumber) {
        return eventTicketsRepository.findByTicketNumber(ticketNumber);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public EventTicket getLastEventTicket(Long eventId) {
        return eventTicketsRepository.findFirstByEventIdOrderByIdDesc(eventId);
    }

    /**
     *
     * @param eventId
     * @param cant
     */
    public void addMoreTicketsToEvent(Long eventId, int cant) {

        EventTicket last = getLastEventTicket(eventId);

        String ticketNumber = last.getTicketNumber();
        int lastTicket = Integer.parseInt(ticketNumber.split("-")[1]);

        Date creationDate = new Date();
        //basico, de inicio a fin 100 a 200 etc
        List<EventTicket> toSave = new LinkedList<>();
        String prefixPattern = "%0" + 3 + "d";
        String suffixPattern = "%05d";
        String prefix = String.format(prefixPattern, eventId);

        for (int i = 1; i < cant + 1; i++) {
            String suffix = String.format(suffixPattern, lastTicket + i);
            EventTicket et = new EventTicket();
            et.setEventId(eventId);
            et.setCreationDate(creationDate);
            et.setInviteCode(StringUtils.generateRandomHash(prefix + "-" + suffix));
            et.setTicketCode(StringUtils.generateRandomHash(prefix + "-" + suffix + "1"));
            et.setStatus(EventTicketStatus.created.name());
            et.setTicketNumber(prefix + "-" + suffix);
            toSave.add(et);
        }

        eventTicketsRepository.save(toSave);

    }

    /**
     *
     * @param eventId
     * @param status
     * @return
     */
    public List<EventTicket> getEventTicketByStatus(Long eventId, String status) {
        return eventTicketsRepository.findByEventIdAndStatus(eventId, status);
    }

    /**
     *
     * @param ticketId
     * @return
     */
    public EventTicket getEventTicketById(Long ticketId) {
        return eventTicketsRepository.findOne(ticketId);
    }

    /**
     *
     * @param inviteCode
     * @return
     */
    public EventTicket getEventTicketByInviteCode(String inviteCode) {
        return eventTicketsRepository.findByInviteCode(inviteCode);
    }

    /**
     *
     * @param email
     * @param eventId
     * @return
     */
    public EventTicket getEventTicketByEmail(String email, Long eventId) {
        return eventTicketsRepository.findByEmailAndEventId(email, eventId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventProduct> getAllProductsForEvent(Long eventId) {
        return eventProductRepository.findByEventId(eventId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventProduct> getGiftsForEvent(Long eventId) {
        String status = EventProductStatus.internal.name();
        return eventProductRepository.findByEventIdAndStatus(eventId, status);
    }

    /**
     *
     * @param productId
     * @return
     */
    public EventProduct getProduct(Long productId) {
        return eventProductRepository.findOne(productId);
    }

    /**
     *
     * @param storeItemId
     * @return
     */
    public EventProduct getEventProductByStoreId(Long storeItemId) {
        return eventProductRepository.findByStoreItemId(storeItemId);
    }

    /**
     *
     * @param memberId
     * @param status
     * @return
     */
    public List<EventsProductPurchase> getEventPurchasesForMember(Long memberId, List<String> status) {
        return eventProductPurchaseRepository.findByMemberIdAndStatusInOrderByIdDesc(memberId, status);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventsProductPurchase> getEventPurchasesForEvent(Long eventId) {
        return eventProductPurchaseRepository.findByEventId(eventId);
    }

    /**
     *
     * @param productId
     */
    public void deleteProduct(Long productId) {
        EventProduct product = eventProductRepository.findOne(productId);
        storeItemsRepository.delete(product.getStoreItemId());
        eventProductRepository.delete(product);
    }

    /**
     *
     * @param eventExtraId
     */
    public void deleteEventExtra(Long eventExtraId) {
        eventExtrasRepository.delete(eventExtraId);
    }

    /**
     *
     * @param eventInfoId
     */
    public void deleteEventInfo(Long eventInfoId) {
        eventInfoRepository.delete(eventInfoId);
    }

    /**
     *
     * @param eventId
     */
    public void deleteEvent(Long eventId) {

        Event e = getEvent(eventId);

        //delete tickets and all related info
        //TODO: verificar los tickets info que no se borran
        eventTicketsRepository.deleteByEventId(eventId);
        eventExtrasRepository.deleteByEventId(eventId);
        eventInfoRepository.deleteByEventId(eventId);
        eventHostsPermissionRepository.deleteByEventId(eventId);
        eventProductPurchaseRepository.deleteByEventId(eventId);
        eventProductRepository.deleteByEventId(eventId);

        //delete pools and seats
        deleteEventSeatsPoolForEvent(eventId);

        //delete tickets
        eventTicketsRepository.deleteByEventId(eventId);

        //finally delete event
        eventsRepository.delete(e);
    }

    /**
     *
     * @param eventId
     */
    public void deleteEventSeatsPoolForEvent(Long eventId) {
        List<EventSeatsPool> pools = eventSeatsPoolRepository.findByEventId(eventId);
        pools.stream().map((pool) -> {
            eventSeatRepository.deleteByPoolId(pool.getId());
            return pool;
        }).forEachOrdered((pool) -> {
            eventSeatsPoolRepository.delete(pool);
        });
    }

    /**
     *
     * @param eventSeatsPoolId
     */
    public void deleteEventSeatsPool(Long eventSeatsPoolId) {
        EventSeatsPool pool = eventSeatsPoolRepository.findOne(eventSeatsPoolId);
        eventSeatRepository.deleteByPoolId(eventSeatsPoolId);
        eventSeatsPoolRepository.delete(pool);
    }

    /**
     *
     * @param e
     * @return
     */
    public EventTicket getMockupTicket(Event e) {
        EventTicket ticket = new EventTicket();
        ticket.setAddress("Calle principal 237 oficina1 A");
        ticket.setAttendanceDate(null);
        ticket.setCity("Córdoba");
        ticket.setCountry("Argentina");
        ticket.setCreationDate(new Date());
        ticket.setEmail("test.email@wingsmobile.es");
        ticket.setEventId(e.getId());
        ticket.setEvent(e);
        ticket.setInviteCode("1f9846aa4c039d350a934bbe10a2e8e14103c7bdf8b3cbd66d9262a0ef89ad5f");
        ticket.setFirstName("Juan Alberto Sanchez");
        ticket.setLastName("Perez Mendoza Roncalbo");
        ticket.setReclaimDate(new Date());
        ticket.setReleaseDate(null);
        ticket.setSeatNumber("Fila 1 - 00564");
        ticket.setSeatNumberId(null);
        ticket.setState("Córdoba");
        ticket.setStatus(EventTicketStatus.reclaimed.name());
        ticket.setTicketCode("1a7a250ee28fc85082c80d3b16061563db596ff7855bc5db29f3b818d6ce3d97");
        ticket.setTicketNumber("002-001365");
        ticket.setZip("5000");
        return ticket;
    }

    /**
     *
     * @param eventId
     * @return
     */
    public EventProduct getLastInsertedProduct(Long eventId) {
        return eventProductRepository.findFirstByEventIdOrderByIdDesc(eventId);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public EventInfo getLastInsertedInfo(Long eventId) {
        return eventInfoRepository.findFirstByEventIdOrderByIdDesc(eventId);
    }

    /**
     *
     * @param ticket
     * @param template
     * @return
     */
    public String getPrintableTicket(EventTicket ticket, String template) {
        String url = getSiteUrl();
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("ticket", ticket);
        context.setVariable("url", url);
        return engine.process("templates/" + template, context);
    }

    /**
     * Create a event product purchase
     *
     * @param member
     * @param product
     * @return
     */
    public EventsProductPurchase createProductPurchase(Member member, EventProduct product) {

        //find previous pending
        List<EventsProductPurchase> previous
                = eventProductPurchaseRepository.findByMemberIdAndEventProductIdAndStatus(
                        member.getId(),
                        product.getId(),
                        EventProductPurchaseStatus.pending.name());

        EventsProductPurchase purchase;
        if (!previous.isEmpty()) {
            purchase = previous.get(0);
        } else {
            purchase = new EventsProductPurchase();

            Date creationDate = new Date();
            purchase.setFirstName(member.getFirstName());
            purchase.setLastName(member.getLastName());
            purchase.setAddress(member.getStreet());
            purchase.setCity(member.getCity());
            purchase.setCountry(member.getCountry());
            purchase.setState(member.getState());
            purchase.setZip(member.getPostal());
            purchase.setEmail(member.getEmail());
            purchase.setDni(member.getDni());

            purchase.setAmountPaid(null);
            purchase.setCreationDate(creationDate);
            purchase.setEventId(product.getEventId());
            purchase.setEventProductId(product.getId());
            purchase.setMemberId(member.getId());
            purchase.setProcessor("undef");
            purchase.setStatus(EventProductPurchaseStatus.pending.name());

            eventProductPurchaseRepository.save(purchase);
        }

        Order o;
        if (purchase.getOrderId() == null) {

            o = new Order();

            o.setActivationDate(null);
            o.setActive(false);
            o.setBillingAddress(member.getStreet());
            o.setBillingCity(member.getCity());
            o.setBillingCompany(null);
            o.setBillingCountry(member.getCountry());
            o.setBillingName(member.getFirstName() + " " + member.getLastName());
            o.setBillingPostalCode(member.getPostal());
            o.setBillingState(member.getState());
            o.setComment("Buy tickets from internal store");
            o.setCreationDate(new Date());
            o.setDeleted(false);
            o.setDni(member.getDni());
            o.setExternalId(purchase.getId());
            o.setFirstName(member.getFirstName());

            Event e = getEvent(product.getEventId());
            String image = getSiteUrl() + "/open/event/" + e.getImage();
            o.setImage(image);

            o.setItemId(product.getStoreItemId());
            o.setItems(null);
            o.setLastName(member.getLastName());
            o.setMemberId(member.getId());
            o.setMessage(null);
            o.setModificationDate(null);
            o.setMultiple(false);
            o.setPayerEmail(member.getEmail());
            o.setPaymentAmount(product.getPrice());
            if (product.isMemberDiscount()) {
                o.setPriceForComs(product.getDiscountPrice());
            } else {
                o.setPriceForComs(null);
            }

            o.setPaymentCurrency(CountryUtils.getCurrencyForCountry(member.getCountry()));
            o.setPaymentStatus(OrderPaymentStatus.pending.name());
            o.setPaymentType("undef");
            o.setPrizeText(null);
            o.setOrderItem(null);
            o.setShippingStatus(ShipmentStatus.created.name());
            o.setSource("internal-store");
            o.setStatus(OrderStatus.pending.name());
            o.setStoreId(null);
            o.setStoreTitle(product.getName());

            o.setType(OrderType.ticket.name());
            o.setTypeStatus(OrderStatus.pending.name());
            o.setWithPrize(false);

            ordersService.save(o);

            //save to store order id
            purchase.setOrderId(o.getId());
            eventProductPurchaseRepository.save(purchase);
        }

        return purchase;
    }

    /**
     *
     * @param order
     */
    public void fulfillEventProductPurchase(Order order) {

        double amountPaid;
        if (order.getPaymentType().equals("coms") && order.getPriceForComs() != null) {
            amountPaid = order.getPriceForComs();
        } else {
            amountPaid = order.getPaymentAmount();
        }

        //HAY QUE CREAR LA ORDEN PURCHASE
        //update purchase status
        EventsProductPurchase purchase = eventProductPurchaseRepository.findByOrderId(order.getId());
        purchase.setProcessor(order.getPaymentType());
        purchase.setAmountPaid(amountPaid);
        purchase.setStatus(EventProductPurchaseStatus.paid.name());
        purchase.setPaymentDate(new Date());
        eventProductPurchaseRepository.save(purchase);

        //getEvent
        Event e = getEvent(purchase.getEventId());

        //getProduct
        EventProduct prod = eventProductRepository.getOne(purchase.getEventProductId());

        //how many tickets and seats?
        int cantTickets = prod.getQuantity();
        PageRequest req = new PageRequest(0, cantTickets);

        //get pools an exclude privates
        List<EventSeatsPool> pools = eventSeatsPoolRepository.findByEventIdAndPrivatePool(e.getId(), Boolean.FALSE);
        List<Long> poolIds = new ArrayList<>();
        pools.forEach((pool) -> {
            poolIds.add(pool.getId());
        });

        //get available seats and take them
        Date takenDate = new Date();
        List<EventSeat> seats = null;
        if (prod.isSeatsAvailable() && e.getSeatsNumbers()) {

            Page<EventSeat> seatsPage = eventSeatRepository.findAllByEventIdAndTakenAndPoolIdIn(e.getId(), Boolean.FALSE, poolIds, req);
            seats = seatsPage.getContent();
            for (EventSeat seat : seats) {
                seat.setTaken(Boolean.TRUE);
                seat.setTakenDate(takenDate);
            }
            eventSeatRepository.save(seats);
        }

        int index = 0;
        //get available tickets ans assign them
        Page<EventTicket> ticketsPage = eventTicketsRepository.findByEventIdAndStatus(prod.getEventId(), EventTicketStatus.created.name(), req);
        List<EventTicket> tickets = ticketsPage.getContent();
        for (EventTicket ticket : tickets) {
            ticket.setStatus(EventTicketStatus.assigned.name());
            ticket.setStatusMessage("Ticket assigned by system order " + order.getId());
            ticket.setStatusAudit(
                    DateUtils.formatNormalDateTime(new Date())
                    + " "
                    + ticket.getStatus()
                    + System.lineSeparator()
                    + ((ticket.getStatusAudit() != null) ? ticket.getStatusAudit() : ""));

            //is we have seats put them to the ticket
            if (seats != null) {
                try {
                    EventSeat seat = seats.get(index);
                    ticket.setSeatNumber(seat.getSeatNumber());
                    ticket.setSeatNumberId(seat.getId());
                } catch (Exception ex) {
                    //just in case we have less seats
                }
                index++;
            }
        }
        eventTicketsRepository.save(tickets);

        //save assignation
        List<EventTicketAssignation> toAssign = new LinkedList<>();
        tickets.stream().map((ticket) -> {
            EventTicketAssignation asign = new EventTicketAssignation();
            asign.setPurchaseId(purchase.getId());
            asign.setTicketId(ticket.getId());
            return asign;
        }).forEachOrdered((asign) -> {
            toAssign.add(asign);
        });
        eventTicketsAssignationRepository.save(toAssign);

        checkEventSoldOut(e.getId());

    }

    /**
     * No orders here, only fake purchases
     *
     * @param members
     * @param product
     */
    public void giftProductToMembers(List<Member> members, EventProduct product) {

        Event e = getEvent(product.getEventId());

        //get pools an exclude privates, if product has pool id set it
        List<Long> poolIds = new ArrayList<>();
        if (product.getSeatsPool() != null) {
            poolIds.add(product.getSeatsPool());
        } else {
            List<EventSeatsPool> pools = eventSeatsPoolRepository.findByEventIdAndPrivatePool(e.getId(), Boolean.FALSE);
            pools.forEach((pool) -> {
                poolIds.add(pool.getId());
            });
        }

        List<EventsProductPurchase> toSave = new LinkedList<>();
        for (Member member : members) {

            //create purchases            
            EventsProductPurchase purchase = new EventsProductPurchase();

            Date creationDate = new Date();
            purchase.setFirstName(member.getFirstName());
            purchase.setLastName(member.getLastName());
            purchase.setAddress(member.getStreet());
            purchase.setCity(member.getCity());
            purchase.setCountry(member.getCountry());
            purchase.setState(member.getState());
            purchase.setZip(member.getPostal());
            purchase.setEmail(member.getEmail());
            purchase.setDni(member.getDni());

            purchase.setAmountPaid(null);
            purchase.setCreationDate(creationDate);
            purchase.setEventId(product.getEventId());
            purchase.setEventProductId(product.getId());
            purchase.setMemberId(member.getId());
            purchase.setProcessor("gift");
            purchase.setStatus(EventProductPurchaseStatus.paid.name());
            purchase.setPaymentDate(creationDate);

            toSave.add(purchase);

        }

        //guardo
        eventProductPurchaseRepository.save(toSave);
        String message = "Ticket assigned by system - Auto";
        try {
            AdminUser admin = AuthUtils.getAdminUser();
            if (admin != null) {
                message = "Ticket assigned by gift by " + admin.getUsername();
            }
        } catch (Exception ex) {
        }

        //assigno los tickets
        for (EventsProductPurchase purchase : toSave) {
            //how many tickets and seats?
            int cantTickets = product.getQuantity();
            PageRequest req = new PageRequest(0, cantTickets);

            //get available seats and take them
            Date takenDate = new Date();
            List<EventSeat> seats = null;
            if (product.isSeatsAvailable() && e.getSeatsNumbers()) {

                Page<EventSeat> seatsPage = eventSeatRepository.findAllByEventIdAndTakenAndPoolIdIn(e.getId(), Boolean.FALSE, poolIds, req);
                seats = seatsPage.getContent();

                if (seats.isEmpty()) {
                    log.error("Seats not found to assign to " + purchase.getEmail() + " " + product.getName());
                }

                for (EventSeat seat : seats) {
                    seat.setTaken(Boolean.TRUE);
                    seat.setTakenDate(takenDate);
                }
                eventSeatRepository.save(seats);
            }

            int index = 0;
            //get available tickets ans assign them
            Page<EventTicket> ticketsPage = eventTicketsRepository.findByEventIdAndStatus(product.getEventId(), EventTicketStatus.created.name(), req);
            List<EventTicket> tickets = ticketsPage.getContent();
            for (EventTicket ticket : tickets) {
                ticket.setStatus(EventTicketStatus.assigned.name());
                ticket.setStatusMessage(message);
                ticket.setStatusAudit(
                        DateUtils.formatNormalDateTime(new Date())
                        + " "
                        + ticket.getStatus()
                        + System.lineSeparator()
                        + ((ticket.getStatusAudit() != null) ? ticket.getStatusAudit() : ""));

                //if we have seats put them to the ticket
                if (seats != null) {
                    try {
                        EventSeat seat = seats.get(index);
                        ticket.setSeatNumber(seat.getSeatNumber());
                        ticket.setSeatNumberId(seat.getId());
                    } catch (Exception ex) {
                        //just in case we have less seats
                    }
                    index++;
                }
            }
            eventTicketsRepository.save(tickets);

            //save assignation
            List<EventTicketAssignation> toAssign = new LinkedList<>();
            tickets.stream().map((ticket) -> {
                EventTicketAssignation asign = new EventTicketAssignation();
                asign.setPurchaseId(purchase.getId());
                asign.setTicketId(ticket.getId());
                return asign;
            }).forEachOrdered((asign) -> {
                toAssign.add(asign);
            });
            eventTicketsAssignationRepository.save(toAssign);

        }

        checkEventSoldOut(e.getId());

    }

    /**
     *
     * @param eventId
     */
    public void checkEventSoldOut(Long eventId) {
        Event e = getEvent(eventId);
        if (e.isSoldOut()) {
            return;
        }
        List<String> asList = Arrays.asList(EventTicketStatus.created.name());
        int totalSell = eventTicketsRepository.countByEventIdAndStatusNotIn(eventId, asList);
        boolean soldOut = (e.getTicketNumbersFrom() - totalSell) <= 10;
        if (soldOut) {
            e.setSoldOut(soldOut);
            saveEvent(e);
        }
    }

    /**
     *
     * @param order
     */
    public void deleteEventProductPurchase(Order order) {

        String adminUser = "system";
        try {
            AdminUser admin = AuthUtils.getAdminUser();
            adminUser = admin.getUsername();
        } catch (Exception e) {
        }

        //update purchase status
        EventsProductPurchase purchase = eventProductPurchaseRepository.findByOrderId(order.getId());

        //contenedores de ids
        List<Long> ticketsIds = new ArrayList<>();
        List<Long> seatsIds = new ArrayList<>();

        //busco los tickets a liberar
        List<EventTicketAssignation> toRelease = eventTicketsAssignationRepository.findByPurchaseId(purchase.getId());
        toRelease.forEach((eventTicketAssignation) -> {
            ticketsIds.add(eventTicketAssignation.getTicketId());
        });

        //elimino las asignaciones
        eventTicketsAssignationRepository.delete(toRelease);

        //Libero los tickets
        List<EventTicket> tickets = eventTicketsRepository.findAll(ticketsIds);
        for (EventTicket ticket : tickets) {

            if (ticket.getSeatNumberId() != null) {
                seatsIds.add(ticket.getSeatNumberId());
            }

            ticket.setStatus(EventTicketStatus.created.name());
            ticket.setStatusMessage("Ticket released by " + adminUser + "  order " + order.getId());
            ticket.setStatusAudit(
                    DateUtils.formatNormalDateTime(new Date())
                    + " "
                    + ticket.getStatus()
                    + System.lineSeparator()
                    + ((ticket.getStatusAudit() != null) ? ticket.getStatusAudit() : ""));
            ticket.setSeatNumber(null);
            ticket.setSeatNumberId(null);
        }
        eventTicketsRepository.save(tickets);

        //libero los asientos si hay
        List<EventSeat> seats = eventSeatRepository.findAll(seatsIds);
        seats.forEach((seat) -> {
            seat.setTaken(Boolean.FALSE);
            seat.setTakenDate(null);
        });
        eventSeatRepository.save(seats);

        //marco la compra como eliminada y guardo
        purchase.setStatus(EventProductPurchaseStatus.deleted.name());
        eventProductPurchaseRepository.save(purchase);

    }

    /**
     *
     * @param purchasesIds
     * @return
     */
    public List<EventTicketAssignation> getAssignationTickets(List<Long> purchasesIds) {
        return eventTicketsAssignationRepository.findByPurchaseIdIn(purchasesIds);
    }

    /**
     *
     * @param q
     * @param page
     * @param size
     * @param ticketIds
     * @return
     */
    public Page<EventTicket> searchTicketsIn(String q, int page, int size, List<Long> ticketIds) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        return eventTicketsRepository.findByIdIn(ticketIds, pr);
    }

    /**
     *
     * @param ticket
     */
    public void saveEventTicket(EventTicket ticket) {
        eventTicketsRepository.save(ticket);
    }

    /**
     *
     * @param tickets
     */
    public void saveEventTickets(List<EventTicket> tickets) {
        eventTicketsRepository.save(tickets);
    }

    /**
     *
     * @param infos
     */
    public void saveEventTicketInfo(List<EventTicketInfo> infos) {
        eventTicketsInfoRepository.save(infos);
    }

    /**
     *
     * @return
     */
    public List<EventHost> getAllEventUsers() {
        return eventHostsRepository.findAll();
    }

    /**
     *
     * @param excludeIds
     * @return
     */
    public List<EventHost> getAllEventUsersNotIn(List<Long> excludeIds) {
        return eventHostsRepository.findByIdNotIn(excludeIds);
    }

    /**
     *
     * @param q
     * @param page
     * @param size
     * @return
     */
    public Page<EventHost> searchEventUsers(String q, int page, int size) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);
        return eventHostsRepository.findAll(pr);
    }

    /**
     *
     * @param host
     */
    public void saveHost(EventHost host) {
        eventHostsRepository.save(host);
    }

    /**
     *
     * @param hostPermission
     */
    public void saveHostPermission(EventHostPermission hostPermission) {
        eventHostsPermissionRepository.save(hostPermission);
    }

    /**
     *
     * @param host
     */
    public void deleteHost(EventHost host) {
        eventHostsRepository.delete(host);
    }

    /**
     *
     * @param email
     * @return
     */
    public EventHost getHostByEmail(String email) {
        return eventHostsRepository.findByEmail(email);
    }

    /**
     *
     * @param hostID
     * @return
     */
    public EventHost getHost(Long hostID) {
        return eventHostsRepository.findOne(hostID);
    }

    /**
     *
     * @param eventId
     * @return
     */
    public List<EventHostPermission> getEventHosts(Long eventId) {
        return eventHostsPermissionRepository.findByEventId(eventId);
    }

    /**
     * Return member username for register afiliate
     *
     * @param ticketId
     * @return
     */
    public String getTicketSponsor(Long ticketId) {
        EventTicketAssignation assignation = eventTicketsAssignationRepository.findByTicketId(ticketId);
        EventsProductPurchase purchase = eventProductPurchaseRepository.findOne(assignation.getPurchaseId());
        Member m = membersRepository.findOne(purchase.getMemberId());
        return m.getUsername();
    }

    /**
     *
     */
    @Scheduled(initialDelay = 12000, fixedRate = 60000)
    public void sendEmail() {

        boolean send = false;
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("dev") || profile.contains("default")) {
                send = false;
            }
        }

        if (send) {
            String preBody = " Puedes acceder a tu entrada desde los siguientes enlaces <a href=\"URL\">Version Móvil</a> , <a href=\"URL2\">Version Escritorio</a>";
            List<EventTicket> tickets = eventTicketsRepository.findFirst10ByStatusAndEmailSent(EventTicketStatus.reclaimed.name(), false);
            for (EventTicket ticket : tickets) {

                //solo si el evento esta publicado
                if (ticket.getEvent().getStatus().equals(EventStatus.published.name())) {

                    String subject = "Entrada evento " + ticket.getEvent().getName();
                    String url = "https://beta.wingsmobile.net/open/events/tickets/" + ticket.getTicketCode() + "/mobile";
                    String url2 = "https://beta.wingsmobile.net/open/events/tickets/" + ticket.getTicketCode() + "/desktop";
                    String body = emailService.getGenericEmail(ticket.getFullName(), "Entrada Evento " + ticket.getEvent().getName(), preBody.replaceAll("URL", url).replaceAll("URL2", url2));
                    boolean sent = emailService.sendMail("Eventos<contratacion@wingsmobile.com>", ticket.getEmail(), subject, body, null);
                    ticket.setEmailSent(sent);

                } else {
                    //si el evento no esta publicado lo marco como enviado asi no vuelve a aparecer
                    ticket.setEmailSent(true);
                }
                eventTicketsRepository.save(tickets);

            }
        }
    }

    /**
     *
     * @return
     */
    private String getSiteUrl() {
        String url = "https://beta.wingsmobile.net";
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("dev") || profile.contains("default")) {
                url = "http://localhost:8099";
            }
            if (profile.contains("desa")) {
                url = "http://dev.backoffice.wingsmobile.com";
            }
        }
        return url;
    }

    /**
     *
     */
    //@Scheduled(initialDelay = 60000, fixedRate = 3600000)
    public void processLatamEventorders() {

        Event e = getEvent(48l);
        if (!e.isEnableSync()) {
            return;
        }

        //get products involved
        //List<Long> storeItemids = Arrays.asList(190l, 195l, 196l, 197l, 198l, 199l, 200l, 201l, 202l, 203l, 204l, 206l, 207l, 208l, 209l, 210l, 211l, 239l, 240l, 241l, 242l);
        List<Long> storeItemids = Arrays.asList(271l, 272l, 273l);
        Map<Long, List<Long>> productsPerStoreItem = new HashMap<>();

        //create map product < list of ticket products>
        for (Long storeItemid : storeItemids) {
            List<Long> includes = new ArrayList<>();
            switch (storeItemid.intValue()) {
                case 271: //gral
                    includes.add(101l); //general
                    break;
                case 272: //vip
                    includes.add(102l); //vip
                    break;
                case 273: //promo
                    includes.add(103l); //5 gral
                    includes.add(102l); //1 vip
                    break;
            }
            productsPerStoreItem.put(storeItemid, includes);
        }

        //create map product < list of ticket products>
        /*for (Long storeItemid : storeItemids) {
            List<Long> includes = new ArrayList<>();
            switch (storeItemid.intValue()) {
                case 271: //STARTER
                    includes.add(73l); //2 segundo anillo
                    break;
                case 195: //STARTER PRO
                    includes.add(74l); //4 segundo anillo
                    break;
                case 196: //COMBO
                    //includes.add(78l); //3 platea
                    includes.add(71l); //2 tercer anillo
                    includes.add(73l); //2 segundo anillo
                    break;
                case 197: //COMBO PRO
                    //includes.add(79l); //4 platea
                    includes.add(71l); //2 tercer anillo
                    includes.add(73l); //2 segundo anillo
                    break;
                case 198: //GOLDEN 
                    //includes.add(79l); //4 platea
                    //includes.add(83l); //2 platea VIP
                    includes.add(71l); //2 tercer anillo
                    includes.add(73l); //2 segundo anillo
                    break;
                case 199: //DIAMOND
                    //includes.add(80l); //10 platea
                    //includes.add(84l); //4 platea VIP
                    includes.add(71l); //2 tercer anillo
                    includes.add(73l); //2 segundo anillo
                    break;
                case 200: //SMARTWATCH
                    includes.add(71l); //2 tercer anillo
                    includes.add(71l); //2 tercer anillo
                    break;
                case 201: //10+1 
                    includes.add(75l); //11 segundo anillo
                    break;
                case 202: //10+1
                    //includes.add(81l); //11 platea
                    break;
                case 203: //10+1
                    //includes.add(85l); //11 platea VIP
                    break;
                case 204: //10+1
                    //includes.add(86l); //11 ZONA VIP
                    break;
                case 206: //STARTER 3C
                    includes.add(72l); //1 segundo anillo
                    break;
                case 207: //STARTER PRO 3C
                    includes.add(72l); //1 segundo anillo
                    break;
                case 208: //COMBO 3C
                    //includes.add(76l); //1 platea
                    includes.add(72l); //1 segundo anillo
                    includes.add(71l); //2 tercer anillo
                    break;
                case 209: //COMBO PRO 3C
                    //includes.add(76l); //1 platea
                    includes.add(72l); //1 segundo anillo
                    includes.add(71l); //2 tercer anillo
                    break;
                case 210: //GOLDEN 3C
                    //includes.add(76l); //1 platea
                    //includes.add(82l); //1 platea VIP
                    includes.add(72l); //1 segundo anillo
                    includes.add(71l); //2 tercer anillo
                    break;
                case 211: //DIAMOND 3C
                    //includes.add(77l); //2 platea
                    //includes.add(83l); //2 platea VIP
                    includes.add(71l); //2 tercer anillo
                    includes.add(73l); //2 segundo anillo
                    break;
                case 239: //2  
                    includes.add(73l); //2 segundo anillo
                    break;
                case 240: //2
                    //includes.add(77l); //2 platea
                    //includes.add(71l); //2 tercer anillo
                    //includes.add(73l); //2 segundo anillo
                    break;
                case 241: //2
                    //includes.add(83l); //2 platea VIP
                    break;
                case 242: //2
                    //includes.add(89l); //2 ZONA VIP
                    break;

            }
            productsPerStoreItem.put(storeItemid, includes);
        }*/
        //busco todas las ordenes que tengan los ids de latam event 
        //pueden ser kits o productos
        List<Order> orders = ordersService.findByStatusAndItemIdInAndTypeStatusAndDeleted(OrderStatus.paid.name(), storeItemids, OrderStatus.paid.name(), Boolean.FALSE);
        log.error("Latam EVENT processing " + orders.size());
        for (Order order : orders) {

            if (order.getInstallmentNumber() != null && order.getInstallmentNumber() != 1) {
                continue;
            }

            //get member from database, else create one
            Member m;
            Long memberId = order.getMemberId();
            if (memberId != null) {
                m = membersRepository.findOne(memberId);
            } else {
                m = new Member();
                m.setFirstName(order.getFirstName());
                m.setLastName(order.getLastName());
                m.setStreet(order.getBillingAddress());
                m.setCity(order.getBillingCity());
                m.setState(order.getBillingState());
                m.setPostal(order.getBillingPostalCode());
                m.setCountry(order.getBillingCountry());
                m.setEmail(order.getPayerEmail());
                m.setDni(order.getDni());
                m.setId(-1l);
            }

            List<Long> eventProductsids = productsPerStoreItem.get(order.getItemId());
            List<EventProduct> eventProducts = eventProductRepository.findAll(eventProductsids);
            List<Member> members = new ArrayList<>();
            members.add(m);
            eventProducts.forEach((eventProduct) -> {
                giftProductToMembers(members, eventProduct);
            });

            order.setTypeStatus("ticket-done");
            ordersService.save(order);
        }

        log.error("END Latam EVENT processing ");
    }

    /**
     *
     */
}
