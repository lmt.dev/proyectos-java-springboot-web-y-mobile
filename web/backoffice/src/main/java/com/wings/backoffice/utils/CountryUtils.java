/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.wings.backoffice.services.CountriesService;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CountryUtils {

    private static Map<String, Locale> LOCALES_MAP;
    private static Map<String, String> CURRENCIES_MAP;

    private static CountriesService countriesService;

    @Autowired
    private CountriesService countriesServiceNonStatic;

    @PostConstruct
    private void init() {
        countriesService = countriesServiceNonStatic;
    }

    /**
     * Returns locale for country or ES if locale could not be found
     *
     * @param iso2
     * @return
     */
    public static Locale getLocaleForCountry(String iso2) {
        if (iso2 == null) {
            iso2 = "ES";
        }
        setupLocales();
        if (LOCALES_MAP.containsKey(iso2.toUpperCase())) {
            return LOCALES_MAP.get(iso2.toUpperCase());
        } else {
            return LOCALES_MAP.get("ES");
        }
    }

    /**
     * return currency iso for country
     *
     * @param iso2
     * @return
     */
    public static String getCurrencyForCountry(String iso2) {
        if (iso2 == null) {
            iso2 = "ES";
        }
        setupCurrencies();
        if (CURRENCIES_MAP.containsKey(iso2.toUpperCase())) {
            return CURRENCIES_MAP.get(iso2.toUpperCase());
        } else {
            return CURRENCIES_MAP.get("ES");
        }
    }

    /**
     *
     */
    private static void setupLocales() {
        if (LOCALES_MAP == null) {
            LOCALES_MAP = new HashMap<>();
            LOCALES_MAP.putAll(countriesService.getLocalesMap());
        }
    }

    private static void setupCurrencies() {
        if (CURRENCIES_MAP == null) {
            CURRENCIES_MAP = new HashMap<>();
            CURRENCIES_MAP.putAll(countriesService.getCurrenciesMap());
        }
    }
}
