/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.ecuador;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class EcuadorShipmentWrapper {

    @JsonProperty("ID_TIPO_LOGISTICA")
    private Integer idTipoLogistica;
    @JsonProperty("DETALLE_ENVIO_1")
    private String detalleEnvio1;
    @JsonProperty("DETALLE_ENVIO_2")
    private String detalleEnvio2;
    @JsonProperty("DETALLE_ENVIO_3")
    private String detalleEnvio3;
    @JsonProperty("ID_CIUDAD_ORIGEN")
    private Integer idCiudadOrigen;
    @JsonProperty("ID_CIUDAD_DESTINO")
    private Integer idCiudadDestino;
    @JsonProperty("ID_DESTINATARIO_NE_CL")
    private String idDestinatario;
    @JsonProperty("RAZON_SOCIAL_DESTI_NE")
    private String razonSocialDestinatario;
    @JsonProperty("NOMBRE_DESTINATARIO_NE")
    private String nombreDestinatario;
    @JsonProperty("APELLIDO_DESTINATAR_NE")
    private String apellidoDestinatario;
    @JsonProperty("DIRECCION1_DESTINAT_NE")
    private String direccionDestinatario;
    @JsonProperty("SECTOR_DESTINAT_NE")
    private String sectorDestinatario;
    @JsonProperty("TELEFONO1_DESTINAT_NE")
    private String telefonoDestinatario;
    @JsonProperty("TELEFONO2_DESTINAT_NE")
    private String telefonoDestinatario2;
    @JsonProperty("CODIGO_POSTAL_DEST_NE")
    private String codigoPostal;
    @JsonProperty("ID_REMITENTE_CL")
    private String idRemitente;
    @JsonProperty("RAZON_SOCIAL_REMITE")
    private String razonSocialRemitente;
    @JsonProperty("NOMBRE_REMITENTE")
    private String nombreRemitente;
    @JsonProperty("APELLIDO_REMITE")
    private String apellidoRemitente;
    @JsonProperty("DIRECCION1_REMITE")
    private String direccionRemitente;
    @JsonProperty("SECTOR_REMITE")
    private String sectorRemitente;
    @JsonProperty("TELEFONO1_REMITE")
    private String telefonoRemitente;
    @JsonProperty("TELEFONO2_REMITE")
    private String telefonoRemitente2;
    @JsonProperty("CODIGO_POSTAL_REMI")
    private String codigoPostalRemitente;
    @JsonProperty("ID_PRODUCTO")
    private Integer idProducto;
    @JsonProperty("CONTENIDO")
    private String contenido;
    @JsonProperty("NUMERO_PIEZAS")
    private Integer numeroPiezas;
    @JsonProperty("VALOR_MERCANCIA")
    private Integer valorMercancia;
    @JsonProperty("VALOR_ASEGURADO")
    private Integer valorAsegurado;
    @JsonProperty("LARGO")
    private Integer largo;
    @JsonProperty("ANCHO")
    private Integer ancho;
    @JsonProperty("ALTO")
    private Integer alto;
    @JsonProperty("PESO_FISICO")
    private Integer pesoFisico;
    @JsonProperty("LOGIN_CREACION")
    private String loginCreacion;
    @JsonProperty("PASSWORD")
    private String password;

    public Integer getIdTipoLogistica() {
        return idTipoLogistica;
    }

    public void setIdTipoLogistica(Integer idTipoLogistica) {
        this.idTipoLogistica = idTipoLogistica;
    }

    public String getDetalleEnvio1() {
        return detalleEnvio1;
    }

    public void setDetalleEnvio1(String detalleEnvio1) {
        this.detalleEnvio1 = detalleEnvio1;
    }

    public String getDetalleEnvio2() {
        return detalleEnvio2;
    }

    public void setDetalleEnvio2(String detalleEnvio2) {
        this.detalleEnvio2 = detalleEnvio2;
    }

    public String getDetalleEnvio3() {
        return detalleEnvio3;
    }

    public void setDetalleEnvio3(String detalleEnvio3) {
        this.detalleEnvio3 = detalleEnvio3;
    }

    public Integer getIdCiudadOrigen() {
        return idCiudadOrigen;
    }

    public void setIdCiudadOrigen(Integer idCiudadOrigen) {
        this.idCiudadOrigen = idCiudadOrigen;
    }

    public Integer getIdCiudadDestino() {
        return idCiudadDestino;
    }

    public void setIdCiudadDestino(Integer idCiudadDestino) {
        this.idCiudadDestino = idCiudadDestino;
    }

    public String getIdDestinatario() {
        return idDestinatario;
    }

    public void setIdDestinatario(String idDestinatario) {
        this.idDestinatario = idDestinatario;
    }

    public String getRazonSocialDestinatario() {
        return razonSocialDestinatario;
    }

    public void setRazonSocialDestinatario(String razonSocialDestinatario) {
        this.razonSocialDestinatario = razonSocialDestinatario;
    }

    public String getNombreDestinatario() {
        return nombreDestinatario;
    }

    public void setNombreDestinatario(String nombreDestinatario) {
        this.nombreDestinatario = nombreDestinatario;
    }

    public String getApellidoDestinatario() {
        return apellidoDestinatario;
    }

    public void setApellidoDestinatario(String apellidoDestinatario) {
        this.apellidoDestinatario = apellidoDestinatario;
    }

    public String getDireccionDestinatario() {
        return direccionDestinatario;
    }

    public void setDireccionDestinatario(String direccionDestinatario) {
        this.direccionDestinatario = direccionDestinatario;
    }

    public String getSectorDestinatario() {
        return sectorDestinatario;
    }

    public void setSectorDestinatario(String sectorDestinatario) {
        this.sectorDestinatario = sectorDestinatario;
    }

    public String getTelefonoDestinatario() {
        return telefonoDestinatario;
    }

    public void setTelefonoDestinatario(String telefonoDestinatario) {
        this.telefonoDestinatario = telefonoDestinatario;
    }

    public String getTelefonoDestinatario2() {
        return telefonoDestinatario2;
    }

    public void setTelefonoDestinatario2(String telefonoDestinatario2) {
        this.telefonoDestinatario2 = telefonoDestinatario2;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getIdRemitente() {
        return idRemitente;
    }

    public void setIdRemitente(String idRemitente) {
        this.idRemitente = idRemitente;
    }

    public String getRazonSocialRemitente() {
        return razonSocialRemitente;
    }

    public void setRazonSocialRemitente(String razonSocialRemitente) {
        this.razonSocialRemitente = razonSocialRemitente;
    }

    public String getNombreRemitente() {
        return nombreRemitente;
    }

    public void setNombreRemitente(String nombreRemitente) {
        this.nombreRemitente = nombreRemitente;
    }

    public String getApellidoRemitente() {
        return apellidoRemitente;
    }

    public void setApellidoRemitente(String apellidoRemitente) {
        this.apellidoRemitente = apellidoRemitente;
    }

    public String getDireccionRemitente() {
        return direccionRemitente;
    }

    public void setDireccionRemitente(String direccionRemitente) {
        this.direccionRemitente = direccionRemitente;
    }

    public String getSectorRemitente() {
        return sectorRemitente;
    }

    public void setSectorRemitente(String sectorRemitente) {
        this.sectorRemitente = sectorRemitente;
    }

    public String getTelefonoRemitente() {
        return telefonoRemitente;
    }

    public void setTelefonoRemitente(String telefonoRemitente) {
        this.telefonoRemitente = telefonoRemitente;
    }

    public String getTelefonoRemitente2() {
        return telefonoRemitente2;
    }

    public void setTelefonoRemitente2(String telefonoRemitente2) {
        this.telefonoRemitente2 = telefonoRemitente2;
    }

    public String getCodigoPostalRemitente() {
        return codigoPostalRemitente;
    }

    public void setCodigoPostalRemitente(String codigoPostalRemitente) {
        this.codigoPostalRemitente = codigoPostalRemitente;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Integer getNumeroPiezas() {
        return numeroPiezas;
    }

    public void setNumeroPiezas(Integer numeroPiezas) {
        this.numeroPiezas = numeroPiezas;
    }

    public Integer getValorMercancia() {
        return valorMercancia;
    }

    public void setValorMercancia(Integer valorMercancia) {
        this.valorMercancia = valorMercancia;
    }

    public Integer getValorAsegurado() {
        return valorAsegurado;
    }

    public void setValorAsegurado(Integer valorAsegurado) {
        this.valorAsegurado = valorAsegurado;
    }

    public Integer getLargo() {
        return largo;
    }

    public void setLargo(Integer largo) {
        this.largo = largo;
    }

    public Integer getAncho() {
        return ancho;
    }

    public void setAncho(Integer ancho) {
        this.ancho = ancho;
    }

    public Integer getAlto() {
        return alto;
    }

    public void setAlto(Integer alto) {
        this.alto = alto;
    }

    public Integer getPesoFisico() {
        return pesoFisico;
    }

    public void setPesoFisico(Integer pesoFisico) {
        this.pesoFisico = pesoFisico;
    }

    public String getLoginCreacion() {
        return loginCreacion;
    }

    public void setLoginCreacion(String loginCreacion) {
        this.loginCreacion = loginCreacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
