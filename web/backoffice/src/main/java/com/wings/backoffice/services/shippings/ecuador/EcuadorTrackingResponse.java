/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.ecuador;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class EcuadorTrackingResponse {

    public List<EcuadorTrackingResponseItem> items = new ArrayList<>();

    public List<EcuadorTrackingResponseItem> getItems() {
        return items;
    }

    public void setItems(List<EcuadorTrackingResponseItem> items) {
        this.items = items;
    }

}
