/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class WingsAdsService {

    private final String auth = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEuNjc6ODAwMFwvYXBpXC91c2VyXC9sb2dpbiIsImlhdCI6MTU4OTM4MDIxMSwiZXhwIjoxNTg5MzgzODExLCJuYmYiOjE1ODkzODAyMTEsImp0aSI6ImI4SE9zY0IyUm1SVE9hNHYiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.o7Yb0mmCL8FpgxAIDoxnf1uPz1yzPcqGFfxStnhz7";
    private final String url = "https://dashboard.wingsads.com/brain/api/external/on-buy/username";

    private final Logger log = LoggerFactory.getLogger(WingsAdsService.class);

    /**
     * Returns spondor username if found, null if not found
     *
     * @param email
     * @return
     */
    public String validateEmail(String email) {

        String ret = null;
        try {
            //Si necesito loguear uso esto
            //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
            //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
            RestTemplate template = new RestTemplate();

            Map<String, String> re = new HashMap<>();
            re.put("email", email);

            RequestEntity<Map<String, String>> requestEntity = RequestEntity
                    .post(new URI(url))
                    .header("Auth-Api", auth)
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .body(re);

            ParameterizedTypeReference<Map<String, String>> responseType = new ParameterizedTypeReference<Map<String, String>>() {
            };
            ResponseEntity<Map<String, String>> response = template.exchange(requestEntity, responseType);
            Map res = response.getBody();

            if (res != null && res.get("message") != null && res.get("message").equals("username find")) {
                ret = (String) res.get("result");
            }

        } catch (Exception e) {
            log.error("Error validate email " + email, e);
        }

        return ret;
    }

}
