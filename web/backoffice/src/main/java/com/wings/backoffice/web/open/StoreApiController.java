/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.open;

import com.wings.backoffice.store.domain.OrderWraper;
import com.wings.backoffice.store.enums.PostStatus;
import com.wings.backoffice.store.services.StoreService;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class StoreApiController {

    @Autowired
    private StoreService storeService;

    private final Logger log = LoggerFactory.getLogger(StoreApiController.class);

    @ResponseBody
    @RequestMapping(value = {"/open/store/orders"})
    public ResponseEntity<String> order(Model model, HttpServletRequest request, @RequestBody OrderWraper wraper) {

        log.error("----------------------STORE CALLBACK---------------------");
        /*log.error("Headers:");
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String nextElement = headers.nextElement();
            log.error("\t" + nextElement + " " + request.getHeader(nextElement));
        }*/

        try {
            if (wraper.getStatus().equals("wc-on-hold")
                    || wraper.getStatus().equals("wc-processing")
                    || wraper.getStatus().equals("on-hold")
                    || wraper.getStatus().equals("processing")
                    || wraper.getStatus().equals("wc-completed")
                    || wraper.getStatus().equals("completed")
                    
                    ) {
                
                new Thread(() -> {
                    storeService.processOrder(wraper.getNumber());
                }).start();
            }
        } catch (Exception e) {
            log.error("Error en wraper");
        }
        log.error("----------------------STORE CALLBACK END---------------------");

        return ResponseEntity.ok().build();
    }

}
