/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.NumberUtils;
import java.util.Locale;

/**
 *
 * @author seba
 */
public class DifferentialSums {

    private Double quantityPay;
    private Double quantityRank;
    private Locale locale;

    public DifferentialSums(Double quantityRank, Double quantityPay) {

        this.quantityPay = quantityPay;
        this.quantityRank = quantityRank;

        if (this.quantityPay == null) {
            this.quantityPay = 0.0;
        }

        if (this.quantityRank == null) {
            this.quantityRank = 0.0;
        }
    }

    public Double getQuantityPay() {
        return quantityPay;
    }

    public void setQuantityPay(Double quantityPay) {
        this.quantityPay = quantityPay;
    }

    public Double getQuantityRank() {
        return quantityRank;
    }

    public void setQuantityRank(Double quantityRank) {
        this.quantityRank = quantityRank;
    }

    public String getQuantityPayText() {
        if (locale != null) {
            return NumberUtils.formatMoney(quantityPay, locale);
        }
        return NumberUtils.formatMoney(quantityPay);
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

}
