/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.promo;

import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface PromoLatamRepository extends JpaRepository<PromoLatam, Long>, JpaSpecificationExecutor<PromoLatam> {

    PromoLatam findByMemberId(Long memberId);

}
