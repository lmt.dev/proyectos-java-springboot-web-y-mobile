/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

/**
 *
 * @author seba
 */
public class InvoiceUtils {

    public static String getRandomString() {
        return getRandomString(10);
    }

    public static String getRandomString(Integer number) {

        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                .build();
        String string = generator.generate(number).toUpperCase();

        return string;
    }

    public static String getNumberRandomString(Integer number) {
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', '9')
                .filteredBy(CharacterPredicates.DIGITS)
                .build();

        return generator.generate(number).toUpperCase();
    }

    public static String getWithdrawalInvoiceNumber(Long invoiceId, int year) {
        return "A" + String.format("%08d", invoiceId) + "-" + year;
    }

    public static String getProductInvoiceNumber(Long invoiceId, int year) {
        return "P" + String.format("%08d", invoiceId) + "-" + year;
    }

    public static String getStoreInvoiceNumber(Long invoiceId, int year) {
        return "T" + String.format("%08d", invoiceId) + "-" + year;
    }

    public static String getKitInvoiceNumber(Long invoiceId, int year) {
        return "K" + String.format("%08d", invoiceId) + "-" + year;
    }

    public static String getShipmentId(Long shipmentId) {
        return "201" + String.format("%08d", shipmentId);
    }

    public static String getInvoiceNumber(Long invoiceNumber, String country) {
        if (country.equals("PE")) {
            return "001-" + String.format("%08d", invoiceNumber);
        }
        return "" + String.format("%08d", invoiceNumber);
    }
}
