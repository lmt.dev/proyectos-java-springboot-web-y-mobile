package com.wings.backoffice.web.api.store;

import com.wings.api.response.CategoryResponse;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.SettingsService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lucas
 */
@Controller
@RequestMapping(value = "api/v1/")
public class CategoryApiController {

    @Autowired
    private SettingsService settingsService;
    @Autowired
    private ApiAuthService apiAuthService;

    @RequestMapping(value = "categories", method = RequestMethod.GET)
    public ResponseEntity<CategoryResponse> getCategories(HttpServletRequest request) {

        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        CategoryResponse response = new CategoryResponse();
        response.setCategories(settingsService.getCategories());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
