/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bsc_products")
public class Product implements Serializable{

    private static final long serialVersionUID = -6699685478241756319L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "idcategory")
    private Long idCategory;
    @Column(name = "productcode")
    private String productCode;
    private String img;
    private String imgDetail1;
    private String imgDetail2;
    private String imgDetail3;
    private String name;
    private String description;
    private String shipping;
    private Double price;
    private Double priceOffer;
    private Double rankPoints;
    private Double payPoints;
    private Double volume;
    private Integer stock;
    private String location;
    private String download;
    private Integer ordering;
    private Integer visible;
    private Integer pack;
    private Integer speed;
    private Integer frontstore;
    private Integer backstore;
    private String plugin;
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImgDetail1() {
        return imgDetail1;
    }

    public void setImgDetail1(String imgDetail1) {
        this.imgDetail1 = imgDetail1;
    }

    public String getImgDetail2() {
        return imgDetail2;
    }

    public void setImgDetail2(String imgDetail2) {
        this.imgDetail2 = imgDetail2;
    }

    public String getImgDetail3() {
        return imgDetail3;
    }

    public void setImgDetail3(String imgDetail3) {
        this.imgDetail3 = imgDetail3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceOffer() {
        return priceOffer;
    }

    public void setPriceOffer(Double priceOffer) {
        this.priceOffer = priceOffer;
    }

    public Double getRankPoints() {
        return rankPoints;
    }

    public void setRankPoints(Double rankPoints) {
        this.rankPoints = rankPoints;
    }

    public Double getPayPoints() {
        return payPoints;
    }

    public void setPayPoints(Double payPoints) {
        this.payPoints = payPoints;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getPack() {
        return pack;
    }

    public void setPack(Integer pack) {
        this.pack = pack;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getFrontstore() {
        return frontstore;
    }

    public void setFrontstore(Integer frontstore) {
        this.frontstore = frontstore;
    }

    public Integer getBackstore() {
        return backstore;
    }

    public void setBackstore(Integer backstore) {
        this.backstore = backstore;
    }

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    
    
    

    
}
