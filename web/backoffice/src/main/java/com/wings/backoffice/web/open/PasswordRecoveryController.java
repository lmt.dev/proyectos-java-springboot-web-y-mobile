/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.open;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Token;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.services.EmailService;
import com.wings.backoffice.services.TokenService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class PasswordRecoveryController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = {"/open/recover/send"}, method = RequestMethod.POST)
    public String getRecover(RedirectAttributes redirectAttributes, HttpServletRequest request) {

        String email = request.getParameter("email");
        Member member = membersRepository.findFirstByEmail(email);

        if (member == null) {
            redirectAttributes.addFlashAttribute("message", "Dirección de correo no encontrada");
            return "redirect:/login#signup";
        }

        Token t = tokenService.generateRecoverPasswordToken(member);

        String site = String.format("%s://%s:%d/", request.getScheme(), request.getServerName(), request.getServerPort());
        StringBuilder body = new StringBuilder();
        body.append("Para recuperar tu contraseña, haz click en el siguiente enlace<br/>");
        body.append("<a href=\"").append(site).append("open/recover/").append(t.getToken()).append("\">Recuperar</a><br/>");
        String content = emailService.getGenericEmail("", "Recuperar contraseña - Wings Mobile", body.toString());
        emailService.sendMail("Oficina Virtual<contratacion@wingsmobile.com>", member.getEmail(), "Recuperar contraseña - Wings Mobile", content, null);

        redirectAttributes.addFlashAttribute("message", "Enlace enviado!, revisa tu correo para recuperar tu contraseña");
        return "redirect:/login#signup";
    }

    @RequestMapping(value = {"/open/recover/{token}"}, method = RequestMethod.GET)
    public String getRecover(Model model, HttpServletRequest request, @PathVariable("token") String token) {
        model.addAttribute("token", token);
        return "open/recover-password";
    }

    @RequestMapping(value = {"/open/recover"}, method = RequestMethod.POST)
    public String postRecover(RedirectAttributes redirectAttributes, HttpServletRequest request) {

        String token = request.getParameter("token");
        String newPassword1 = request.getParameter("newPassword1");
        String newPassword2 = request.getParameter("newPassword2");
        String message = null;
        boolean error = false;

        if (token == null) {
            error = true;
            message = "token.invalid";
            redirectAttributes.addAttribute("token", token);
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/open/recover/{token}";
        }

        Token t = tokenService.findByToken(token);
        if (t == null || !t.getActive()) {
            error = true;
            message = "token.invalid";
            redirectAttributes.addAttribute("token", token);
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/open/recover/{token}";
        }

        if (tokenService.isTokenExpired(t)) {
            message = "token.expired";
            error = true;
            redirectAttributes.addAttribute("token", token);
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/open/recover/{token}";
        }

        if (newPassword1 == null || newPassword1.trim().isEmpty() || newPassword2 == null || newPassword2.trim().isEmpty()) {
            message = "password.invalid";
            error = true;
            redirectAttributes.addAttribute("token", token);
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/open/recover/{token}";
        }

        if (!newPassword1.equals(newPassword2)) {
            message = "password.dontmatch";
            error = true;
            redirectAttributes.addAttribute("token", token);
            redirectAttributes.addFlashAttribute("error", error);
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/open/recover/{token}";
        }

        if (!error) {

            Member member = membersRepository.findOne(t.getReferenceId());
            if (member == null || member.getDeleted()) {
                message = "member.invalid";
                error = true;
            } else {
                tokenService.setTokenInactive(t);
                member.setPassword(DigestUtils.sha256Hex(newPassword1));
                membersRepository.save(member);
                message = "reset.success";
            }
        }

        redirectAttributes.addFlashAttribute("error", error);
        redirectAttributes.addFlashAttribute("message", message);
        redirectAttributes.addAttribute("token", token);

        return "redirect:/open/recover/{token}";
    }
}
