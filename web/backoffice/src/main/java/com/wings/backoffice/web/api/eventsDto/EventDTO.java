/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api.eventsDto;

import java.util.Date;

/**
 *
 * @author seba
 */
public class EventDTO {

    private Long eventId;
    private String eventName;
    private String eventShortName;
    private Date eventDate;
    private String eventStatus;

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventShortName() {
        return eventShortName;
    }

    public void setEventShortName(String eventShortName) {
        this.eventShortName = eventShortName;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    
    
}
