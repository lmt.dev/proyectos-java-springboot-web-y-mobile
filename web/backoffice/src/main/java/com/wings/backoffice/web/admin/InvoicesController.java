/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.repos.InvoiceRepository;
import com.wings.backoffice.mlm.specs.InvoiceSpecs;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.InvoiceService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author seba
 */
@Controller
public class InvoicesController {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private CountriesService countriesService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private final Logger log = LoggerFactory.getLogger(InvoicesController.class);

    @RequestMapping(value = {"admin/financial/invoices"}, method = RequestMethod.GET)
    public String getBalances(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String country = request.getParameter("country");

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "invoiceDate", "invoiceNumber"));
        Page<Invoice> items = invoiceRepository.findAll(InvoiceSpecs.search(q, f, t, type, null, country), pr);
        //BalanceSums sums = balanceDAO.find(q, f, t, type, null, null);

        PageWrapper<Invoice> pageWrapper = new PageWrapper<>(items, "admin/financial/invoices");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        //model.addAttribute("total", sums);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("type", type);

        return "admin/financial/invoices";
    }

    @RequestMapping(value = {"admin/financial/invoices/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getInvoicePdf(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Invoice i = invoiceRepository.findOne(id);
        if (i == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (i.getFilePath() == null || i.getFilePath().trim().isEmpty()) {
            String path = invoiceService.generateInvoicePDF(i);
            i.setFilePath(path);
            invoiceRepository.save(i);
        }

        File pdfFile = new File(i.getFilePath().trim());
        if (!pdfFile.exists()) {
            String path = invoiceService.generateInvoicePDF(i);
            i.setFilePath(path);
            invoiceRepository.save(i);
        }

        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + i.getInvoiceNumber() + ".pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"admin/financial/invoices/edit/{id}"}, method = RequestMethod.GET)
    public String getEditInvoice(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        Invoice invoice = invoiceRepository.findOne(id);
        model.addAttribute("invoice", invoice);
        model.addAttribute("postUrl", "admin/financial/invoices/edit/" + id);
        return "admin/financial/invoice-edit";
    }

    @RequestMapping(value = {"admin/financial/invoices/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEditInvoice(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Invoice invoice = invoiceRepository.findOne(id);

        invoice.setClientName(request.getParameter("clientName"));
        invoice.setCompanyName(request.getParameter("companyName"));
        invoice.setClientAddress(request.getParameter("clientAddress"));
        invoice.setClientCity(request.getParameter("clientCity"));
        invoice.setClientState(request.getParameter("clientState"));
        invoice.setClientDni(request.getParameter("dni"));

        invoiceRepository.save(invoice);
        invoiceService.generateInvoicePDF(invoice);

        return ResponseEntity.ok("Factura modificada, pdf regenerado");
    }

    /*@ResponseBody
    @RequestMapping(value = {"admin/orders/invoice/send/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendEmail(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        String ret;
        try {

            Order o = ordersRepository.findOne(orderId);
            if (o == null) {
                ret = "No se encontro la orden";
            } else {

                Invoice i = o.getInvoice();
                if (i == null) {
                    ret = "No se encontró la factura";
                } else {

                    String email = o.getPayerEmail();

                    List<File> attachements = new ArrayList<>();
                    attachements.add(new File(i.getFilePath()));
                    String body = emailService.getGenericEmail(o.getBillingName(), "Factura compra " + o.getStoreTitle(),
                            "<p>Adjuntamos la factura de su pedido.</p><br/>"
                            + "<br/>");

                    boolean success = emailService.sendMail("Ventas<ventas@wingsmobile.es>", email, "Factura compra " + o.getStoreTitle(), body, attachements);

                    if (success) {
                        i.setSended(true);
                        ret = "La factura se envio correctamente";
                    } else {
                        ret = "Ocurrió un error al enviar la factura";
                        return ResponseEntity.badRequest().body(ret);
                    }

                }
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }*/
    @ResponseBody
    @RequestMapping(value = {"admin/financial/invoices/generate"}, method = RequestMethod.POST)
    public ResponseEntity<String> invoiceGenerate(Model model, HttpServletRequest request) {
        String ret;
        try {
            ret = invoiceService.generateInvoices();
        } catch (Exception e) {
            log.error("Error generating invoices", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

//    @RequestMapping(value = {"/open/invoices/{invoiceNumber}"}, method = RequestMethod.GET)
//    public ResponseEntity<InputStreamResource> getInvoicePdfOpen(Model model, HttpServletRequest request, @PathVariable("invoiceNumber") String invoiceNumber) {
//
//        Invoice i = invoiceRepository.findByInvoiceNumberAndClientCountry(invoiceNumber, "PE");
//        if (i == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//        if (i.getFilePath() == null || i.getFilePath().trim().isEmpty()) {
//            String path = invoiceService.generateInvoicePDF(i);
//            i.setFilePath(path);
//            invoiceRepository.save(i);
//        }
//
//        File pdfFile = new File(i.getFilePath().trim());
//        if (!pdfFile.exists()) {
//            String path = invoiceService.generateInvoicePDF(i);
//            i.setFilePath(path);
//            invoiceRepository.save(i);
//        }
//
//        FileInputStream fis;
//        try {
//            fis = new FileInputStream(pdfFile);
//        } catch (FileNotFoundException e) {
//            return ResponseEntity.notFound().build();
//        }
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.parseMediaType("application/pdf"));
//        headers.add("Access-Control-Allow-Origin", "*");
//        headers.add("Content-Disposition", "filename=" + i.getInvoiceNumber() + ".pdf");
//        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
//        headers.add("Pragma", "no-cache");
//        headers.add("Expires", "0");
//
//        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
//    }
    @RequestMapping(value = {"/open/invoices/{token}"}, method = RequestMethod.GET)
    public String getInvoiceHtmlOpen(Model model, HttpServletRequest request, @PathVariable("token") String token) {

        Invoice i = invoiceRepository.findByToken(token);
        //Invoice i = invoiceRepository.findOne(14575l);

        model.addAttribute("officeName", "Wings Mobile Perú S.A.C.");
        model.addAttribute("officeAddress", "Mz. D1 Lote 2 Urb. Santa Patricia 1ra Etapa");
        model.addAttribute("officeIdt", "RUC: 20603544898");
        model.addAttribute("officeCountry", " La Molina, Lima, Perú");
        model.addAttribute("invoice", i);

        Long orderId = i.getOrderId();
        Order order = ordersService.getOrder(orderId);
        model.addAttribute("orderItems", order.getItems());

        return "frags/invoice/invoice";
    }

    @RequestMapping(value = {"/open/invoices/pdf/{token}"}, method = RequestMethod.GET)
    public void getInvoicePdfOpen(Model model, HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("token") String token) {

        Invoice i = invoiceRepository.findByToken(token);

        //return "frags/invoice/invoice";
        String template = "frags/invoice/invoice";
        FileOutputStream os = null;
        try {
            File outputFile = new File("/var/wings/invoices_bo/invoices/" + i.getToken() + ".pdf");

            String processedHtml = getParsedInvoice(template, i);
            os = new FileOutputStream(outputFile);
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(processedHtml);
            renderer.layout();
            renderer.createPDF(os, true);
            renderer.finishPDF();
            os.close();

            FileInputStream fis = new FileInputStream(outputFile);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            response.setContentLengthLong(outputFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=" + i.getToken() + ".pdf");

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public String getParsedInvoice(String template, Invoice invoice) {

        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();

        context.setVariable("officeName", "Wings Mobile Perú S.A.C.");
        context.setVariable("officeAddress", "Mz. D1 Lote 2 Urb. Santa Patricia 1ra Etapa");
        context.setVariable("officeIdt", "RUC: 20603544898");
        context.setVariable("officeCountry", " La Molina, Lima, Perú");
        context.setVariable("invoice", invoice);

        Long orderId = invoice.getOrderId();
        Order order = ordersService.getOrder(orderId);
        context.setVariable("orderItems", order.getItems());

        return engine.process("templates/" + template, context);
    }
}
