/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.events.Event;
import com.wings.backoffice.mlm.domain.events.EventExtra;
import com.wings.backoffice.mlm.domain.events.EventHost;
import com.wings.backoffice.mlm.domain.events.EventHostPermission;
import com.wings.backoffice.mlm.domain.events.EventInfo;
import com.wings.backoffice.mlm.domain.events.EventProduct;
import com.wings.backoffice.mlm.domain.events.EventSeat;
import com.wings.backoffice.mlm.domain.events.EventSeatsPool;
import com.wings.backoffice.mlm.domain.events.EventTicket;
import com.wings.backoffice.mlm.domain.events.EventTicketInfo;
import com.wings.backoffice.mlm.domain.events.EventsProductPurchase;
import com.wings.backoffice.mlm.domain.events.enums.EventStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventTicketStatus;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.ShipmentStatus;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.mlm.domain.events.BisTempEcuador;
import com.wings.backoffice.mlm.repos.BisTempEcuadorRepository;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.PaymentsService;
import com.wings.backoffice.store.services.StoreService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.QrUtils;
import com.wings.backoffice.utils.StringUtils;
import com.wings.backoffice.utils.UrlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author seba
 */
@Controller
public class EventsController {

    @Autowired
    private EventsService eventsService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private StoreService storeService;
    @Autowired
    private PaymentsService paymentsService;
    @Autowired
    private OrdersService ordersService;

    private final Logger log = LoggerFactory.getLogger(EventsController.class);

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/events/list"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<Event> items = eventsService.searchEvents(page, size, q, status);
        PageWrapper<Event> pageWrapper = new PageWrapper<>(items, "admin/events/list");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);

        return "admin/events/event-list";
    }

    /**
     * Post to create an event
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/events/create"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEvent(Model model, HttpServletRequest request) {
        String eventName = request.getParameter("eventName");
        try {
            eventsService.createEvent(eventName);
        } catch (Exception e) {
            log.error("Error creating event ", e);
            return ResponseEntity.badRequest().body("Ocurrio un error al crear el evento!");

        }
        return ResponseEntity.ok("Evento creado exitosamente!");
    }

    /**
     * Post to publish an event
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/event/publish/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEvent(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String res = eventsService.checkPublishAvailability(eventId);
        if (res != null) {
            return ResponseEntity.badRequest().body(res);
        }

        try {
            Event e = eventsService.getEvent(eventId);
            e.setStatus(EventStatus.published.name());
            eventsService.saveEvent(e);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Ocurrio un error al publicar el evento!");
        }
        return ResponseEntity.ok("Evento publicado exitosamente!");
    }

    /**
     * Post to publish an event
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/event/finalize/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEventFinalize(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        try {
            Event e = eventsService.getEvent(eventId);

            long diff = DateUtils.getDiffDays(e.getEventDate());
            if (diff < 1) {
                return ResponseEntity.badRequest().body("El evento no ha finalizado todavia!");
            }

            e.setStatus(EventStatus.finalized.name());
            eventsService.saveEvent(e);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Ocurrio un error al publicar el evento!");
        }
        return ResponseEntity.ok("Evento finalizado exitosamente!");
    }

    /**
     * get to view 1 event
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/view/{id}"}, method = RequestMethod.GET)
    public String getEvent(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {
        Event e = eventsService.getEvent(eventId);
        model.addAttribute("event", e);

        //get countries
        List<String> availableCountries = new ArrayList<>();
        String ac = e.getAvailableCountries();
        if (ac != null) {
            availableCountries.addAll(Arrays.asList(ac.split(",")));
        }
        model.addAttribute("countries", availableCountries);

        //TODO: generar estadisticas de barras de progreso
        Map<String, Long> stats = eventsService.getStats(eventId);
        model.addAttribute("stats", stats);

        //generar estadisticas de barras de progreso
        Map<String, String> stats2 = eventsService.getStats2(eventId);
        model.addAttribute("stats2", stats2);

        EventTicket ticket = eventsService.getMockupTicket(e);
        model.addAttribute("ticket", ticket);

        //set in session
        request.getSession().setAttribute("eventId", eventId);

        //TODO: accion de publicar
        return "admin/events/event-view";
    }

    /**
     * get to edit 1 event
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/edit/{id}"}, method = RequestMethod.GET)
    public String getEditEvent(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {
        Event e = eventsService.getEvent(eventId);
        model.addAttribute("event", e);

        List<EventInfo> infos = eventsService.getEventInfos(eventId);
        model.addAttribute("infos", infos);

        List<EventExtra> extras = eventsService.getEventExtras(eventId);
        model.addAttribute("extras", extras);

        return "admin/events/event-edit";
    }

    /**
     *
     * @param request
     * @param section
     * @param eventId
     * @param logo
     * @param back
     * @return
     */
    @RequestMapping(value = {"/admin/events/edit/{section}/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEditEvent(HttpServletRequest request,
            @PathVariable("section") String section,
            @PathVariable("id") Long eventId,
            @RequestParam(value = "logoFile", required = false) MultipartFile logo,
            @RequestParam(value = "backFile", required = false) MultipartFile back
    ) {

        /*AdminUser admin = AuthUtils.getAdminUser();
        if (!admin.isIsMasterAdmin()) {
            return ResponseEntity.badRequest().body("Unauthorized");
        }*/
        Event e = eventsService.getEvent(eventId);

        //section general
        if (section.equals("general")) {

            String shortName = request.getParameter("shortName");
            String name = request.getParameter("name");
            String eDate = request.getParameter("eventDate");
            String date = request.getParameter("date");
            String hour = request.getParameter("hour");
            String place = request.getParameter("place");
            String placeAddress = request.getParameter("placeAddress");
            String availableCountries = request.getParameter("availableCountries");
            String generateRegister = request.getParameter("generateRegister");
            String disclaimer = request.getParameter("disclaimer");
            String ticketNumbersFrom = request.getParameter("ticketNumbersFrom");
            String seatsNumbers = request.getParameter("seatsNumbers");

            Date eventDate;
            try {
                eventDate = DateUtils.fromBrowserStringToDate(eDate);
            } catch (Exception ex) {
                return ResponseEntity.badRequest().body("Ingrese una fecha válida!");
            }

            int tfrom = 0;
            try {
                tfrom = Integer.parseInt(ticketNumbersFrom);
            } catch (NumberFormatException ex) {
            }

            e.setShortName(shortName);
            e.setName(name);
            e.setEventDate(eventDate);
            e.setDate(date);
            e.setHour(hour);
            e.setPlace(place);
            e.setPlaceAddress(placeAddress);
            e.setAvailableCountries(availableCountries);
            e.setGenerateRegister(generateRegister != null && generateRegister.equals("on"));
            e.setDisclaimer(disclaimer);
            e.setSeatsNumbers(seatsNumbers != null && seatsNumbers.equals("on"));
            e.setTicketNumbersFrom(tfrom);

            eventsService.saveEvent(e);

            return ResponseEntity.ok("Evento editado correctamente!");
        }

        //section reginfo
        if (section.equals("reginfo")) {

            String infoName = request.getParameter("infoName");
            String infoType = request.getParameter("infoType");
            String infoEnum = request.getParameter("infoEnum");
            String infoMandatory = request.getParameter("infoMandatory");

            EventInfo i = new EventInfo();
            i.setName(infoName);
            i.setType(infoType);
            i.setEnumValue(infoEnum);
            i.setMandatory(infoMandatory != null && infoMandatory.equals("on"));

            eventsService.saveEventInfo(e, i);

            return ResponseEntity.ok("Informacion añadida correctamente!");
        }

        //section reginfo
        if (section.equals("tsconfig")) {

            String ticketNumbersFrom = request.getParameter("ticketNumbersFrom");
            String ticketNumbersTo = request.getParameter("ticketNumbersTo");
            String ticketNumbersPattern = request.getParameter("ticketNumbersPattern");
            String seatsNumbers = request.getParameter("seatsNumbers");

            int tfrom = Integer.parseInt(ticketNumbersFrom);
            int tto = Integer.parseInt(ticketNumbersTo);

            e.setTicketNumbersFrom(tfrom);
            e.setTicketNumbersTo(tto);
            e.setTicketNumbersPattern(ticketNumbersPattern);
            e.setSeatsNumbers(seatsNumbers != null && seatsNumbers.equals("on"));

            eventsService.saveEvent(e);

            return ResponseEntity.ok("Evento editado correctamente!");

        }

        //section reginfo
        if (section.equals("styles")) {

            if (logo != null && !logo.isEmpty()) {
                try {
                    String suffix = DigestUtils.sha256Hex("logo" + e.getId());
                    String fileName = "/var/wings/bo_media/events_content/design/" + suffix;
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                    stream.write(logo.getBytes());
                    e.setImage(suffix);
                } catch (Exception ex) {
                }
            }

            if (back != null && !back.isEmpty()) {
                try {
                    String suffix = DigestUtils.sha256Hex("back" + e.getId());
                    String fileName = "/var/wings/bo_media/events_content/design/" + suffix;
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                    stream.write(back.getBytes());
                    e.setTicketBackground(suffix);
                } catch (Exception ex) {
                }
            }

            String ticketColor = request.getParameter("ticketColor");
            String textColor = request.getParameter("textColor");
            String showName = request.getParameter("showName");
            String showDate = request.getParameter("showDate");
            String showHour = request.getParameter("showHour");
            String showPlace = request.getParameter("showPlace");
            String showPlaceAddress = request.getParameter("showPlaceAddress");

            e.setShowDate(showDate != null && showDate.equals("on"));
            e.setShowHour(showHour != null && showHour.equals("on"));
            e.setShowName(showName != null && showName.equals("on"));
            e.setShowPlace(showPlace != null && showPlace.equals("on"));
            e.setShowPlaceAddress(showPlaceAddress != null && showPlaceAddress.equals("on"));

            e.setTicketColor(ticketColor);
            e.setTextColor(textColor);

            eventsService.saveEvent(e);

            return ResponseEntity.ok("Evento editado correctamente!");
        }

        //section reginfo
        if (section.equals("extras")) {
            String infoExtra = request.getParameter("infoExtra");

            if (infoExtra == null || infoExtra.isEmpty()) {
                return ResponseEntity.badRequest().body("No se modifico el evento!");
            }

            EventExtra extra = new EventExtra();
            extra.setExtra(infoExtra);
            extra.setEventId(e.getId());
            eventsService.saveEventExtra(e, extra);

            return ResponseEntity.ok("Evento editado correctamente!");
        }

        //section reginfo
        if (section.equals("seats")) {

            String poolName = request.getParameter("poolName");
            String poolSeatsQty = request.getParameter("poolSeatsQty");
            String poolPattern = request.getParameter("poolPattern");
            String poolPrivate = request.getParameter("poolPrivate");

            if (poolName == null || poolName.isEmpty()) {
                return ResponseEntity.badRequest().body("El nombre no puede estar vacio");
            }

            if (poolPattern == null || poolPattern.isEmpty()) {
                return ResponseEntity.badRequest().body("El patron no puede estar vacio");
            }

            int seats;
            try {
                seats = Integer.parseInt(poolSeatsQty);
            } catch (NumberFormatException ex) {
                return ResponseEntity.badRequest().body("El nro de asientos no es válido");
            }

            EventSeatsPool pool = new EventSeatsPool();
            pool.setCreationDate(new Date());
            pool.setEventId(e.getId());
            pool.setName(poolName);
            pool.setPattern(poolPattern);
            pool.setPrivatePool(poolPrivate != null && poolPrivate.equals("on"));
            pool.setSeatsQuantity(seats);

            eventsService.saveSeatsPool(pool);

            return ResponseEntity.ok("Evento editado correctamente!");
        }

        //section products
        if (section.equals("products")) {

            String shortName = request.getParameter("productShortName");
            String name = request.getParameter("productName");
            String price = request.getParameter("productPrice");
            String qty = request.getParameter("productQuantity");
            String country = request.getParameter("productCountry");
            String discount = request.getParameter("productDiscount");
            String discountPrice = request.getParameter("productDiscountPrice");
            String status = request.getParameter("productStatus");
            String seatsAvailable = request.getParameter("productSeatsAvailable");
            String poolId = request.getParameter("poolId");

            if (poolId != null && poolId.equals("-1")) {
                poolId = null;
            }

            if (shortName == null || shortName.isEmpty()) {
                return ResponseEntity.badRequest().body("El nombre corto no puede estar vacio");
            }

            if (name == null || name.isEmpty()) {
                return ResponseEntity.badRequest().body("El nombre no puede estar vacio");
            }

            /*if (country == null || country.isEmpty()) {
                return ResponseEntity.badRequest().body("El pais no es válido");
            }*/
            int quantity;
            try {
                quantity = Integer.parseInt(qty);
            } catch (NumberFormatException ex) {
                return ResponseEntity.badRequest().body("El nro de no es válido");
            }

            double productPrice;
            try {
                productPrice = Double.parseDouble(price);
            } catch (NumberFormatException ex) {
                return ResponseEntity.badRequest().body("El precio no es válido");
            }

            /*boolean isDiscount = discount != null && discount.equals("on");
            Double discountProductPrice = null;
            if (isDiscount) {
                try {
                    discountProductPrice = Double.parseDouble(discountPrice);
                } catch (NumberFormatException ex) {
                    return ResponseEntity.badRequest().body("El precio de descuento no es válido");
                }
            }*/
            EventProduct p = new EventProduct();
            p.setCountry("all");
            //p.setDiscountPrice(discountProductPrice);
            p.setEventId(e.getId());
            //p.setMemberDiscount(isDiscount);
            p.setName(name);
            p.setShortName(shortName);
            p.setPrice(productPrice);
            p.setQuantity(quantity);
            p.setStatus(status);
            p.setSeatsAvailable(seatsAvailable != null && seatsAvailable.equals("on"));
            p.setSeatsPool(poolId != null ? Long.parseLong(poolId) : null);

            //save product and create store item
            eventsService.saveProduct(p);

            return ResponseEntity.ok("Producto añadido correctamente!");
        }

        //section delete-logo
        if (section.equals("delete-logo")) {
            e.setImage(null);
            eventsService.saveEvent(e);
            return ResponseEntity.ok("Imagen eliminada correctamente!");
        }

        //section delete-logo
        if (section.equals("delete-back")) {
            e.setTicketBackground(null);
            eventsService.saveEvent(e);
            return ResponseEntity.ok("Imagen eliminada correctamente!");
        }

        return ResponseEntity.badRequest().body("Ocurrio un error al editar el evento");
    }

    /**
     * Post for delete and event
     *
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteEvent(HttpServletRequest request, @PathVariable("id") Long eventId) {
        try {
            eventsService.deleteEvent(eventId);
            return ResponseEntity.ok("Evento eliminado!");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error eliminado evento!");
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/tickets/{id}"}, method = RequestMethod.GET)
    public String getTickets(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<EventTicket> items = eventsService.searchTickets(page, size, q, status, eventId);
        PageWrapper<EventTicket> pageWrapper = new PageWrapper<>(items, "/admin/events/tickets/" + eventId);

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        return "admin/events/event-tickets";
    }

    @RequestMapping(value = {"/admin/events/tickets/{id}"}, method = RequestMethod.POST)
    public String postGenerateTickets(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {
        Event event = eventsService.getEvent(eventId);
        try {
            eventsService.regenerateTickets(event);
        } catch (Exception e) {
            log.error("Error generando tickets", e);
        }
        return "redirect:/#admin/events/tickets/" + eventId;
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/seats/{id}"}, method = RequestMethod.GET)
    public String getSeats(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<EventSeat> items = eventsService.searchSeats(page, size, q, status, eventId);
        PageWrapper<EventSeat> pageWrapper = new PageWrapper<>(items, "/admin/events/seats/" + eventId);

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        List<EventSeatsPool> pools = eventsService.getSeatsPool(eventId);
        model.addAttribute("pools", pools);

        return "admin/events/event-seats";
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/products/{id}"}, method = RequestMethod.GET)
    public String getProducts(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<EventProduct> items = eventsService.searchProducts(page, size, q, status, eventId);
        PageWrapper<EventProduct> pageWrapper = new PageWrapper<>(items, "/admin/events/products/" + eventId);

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);

        List<EventSeatsPool> pools = eventsService.getSeatsPool(eventId);
        model.addAttribute("pools", pools);

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        return "admin/events/event-products";
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/store/{id}"}, method = RequestMethod.GET)
    public String getStore(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        List<EventProduct> prods = eventsService.getAllProductsForEvent(eventId);
        model.addAttribute("prods", prods);

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        return "admin/events/event-store";
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/purchases/{id}"}, method = RequestMethod.GET)
    public String getPurchases(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String q = request.getParameter("q");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<EventsProductPurchase> items = eventsService.searchPurchases(page, size, q, status, eventId);
        PageWrapper<EventsProductPurchase> pageWrapper = new PageWrapper<>(items, "/admin/events/purchases/" + eventId);

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", ssize);

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        return "admin/events/event-purchases";
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/gifts/{id}"}, method = RequestMethod.GET)
    public String getGifts(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {
        List<EventProduct> gifts = eventsService.getGiftsForEvent(eventId);
        model.addAttribute("gifts", gifts);
        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);
        return "admin/events/event-gifts";
    }

    /**
     *
     * @param model
     * @param request
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/gifts/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postGifts(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        String ids = request.getParameter("membersIds");
        String productId = request.getParameter("product");

        List<String> stringids = Arrays.asList(ids.split(","));
        List<Long> longIds = new LinkedList<>();
        for (String stringid : stringids) {
            try {
                longIds.add(Long.parseLong(stringid));
            } catch (Exception e) {
            }
        }

        List<Member> members = membersService.getMembers(longIds);
        EventProduct product = eventsService.getProduct(Long.parseLong(productId));
        try {
            eventsService.giftProductToMembers(members, product);
            return ResponseEntity.ok("Regalos asignados");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Ocurrió un error asignando regalos, " + e.getMessage());
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param productId
     * @return
     */
    @RequestMapping(value = {"/admin/events/products/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteProduct(Model model, HttpServletRequest request, @PathVariable("id") Long productId) {
        try {
            eventsService.deleteProduct(productId);
            return ResponseEntity.ok("Producto eliminado correctamente");
        } catch (Exception e) {
            log.error("Error generando tickets", e);
        }
        return ResponseEntity.badRequest().body("Error eliminado producto");
    }

    /**
     *
     * @param model
     * @param request
     * @param extraId
     * @return
     */
    @RequestMapping(value = {"/admin/events/extra/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteExtra(Model model, HttpServletRequest request, @PathVariable("id") Long extraId) {
        try {
            eventsService.deleteEventExtra(extraId);
            return ResponseEntity.ok("Extra eliminado correctamente");
        } catch (Exception e) {
            log.error("Error eliminando extra", e);
        }
        return ResponseEntity.badRequest().body("Error eliminado extra");
    }

    /**
     *
     * @param model
     * @param request
     * @param infoId
     * @return
     */
    @RequestMapping(value = {"/admin/events/info/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteInfo(Model model, HttpServletRequest request, @PathVariable("id") Long infoId) {
        try {
            eventsService.deleteEventInfo(infoId);
            return ResponseEntity.ok("Información eliminado correctamente");
        } catch (Exception e) {
            log.error("Error eliminando info", e);
        }
        return ResponseEntity.badRequest().body("Error eliminado info");
    }

    /**
     *
     * @param model
     * @param request
     * @param poolId
     * @return
     */
    @RequestMapping(value = {"/admin/events/seats/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteSeatsPool(Model model, HttpServletRequest request, @PathVariable("id") Long poolId) {
        try {
            eventsService.deleteEventSeatsPool(poolId);
            return ResponseEntity.ok("Bloque eliminado correctamente");
        } catch (Exception e) {
            log.error("Error generando tickets", e);
        }
        return ResponseEntity.badRequest().body("Error eliminado bloque de asientos");
    }

    /**
     * get the last inserted product for this event as a table row
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/products/last/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getLastinsertedProductAsRow(@PathVariable("id") Long eventId) {

        EventProduct p = eventsService.getLastInsertedProduct(eventId);

        StringBuilder b = new StringBuilder();
        b.append("<tr>")
                .append("<td>").append(p.getShortName()).append("</td>")
                .append("<td>").append(p.getName()).append("</td>")
                .append("<td>").append(p.getQuantity()).append("</td>")
                .append("<td>").append(p.getPriceText()).append("</td>")
                .append("<td>").append(p.isSeatsAvailable()).append("</td>")
                .append("<td>").append(p.getStatus()).append("</td>")
                .append("<td>&nbsp;</td>");

        return ResponseEntity.ok(b.toString());
    }

    /**
     * get the last inserted event info for this event as a table row
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/info/last/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getLastinsertedInfoAsRow(@PathVariable("id") Long eventId) {

        EventInfo i = eventsService.getLastInsertedInfo(eventId);

        StringBuilder b = new StringBuilder();
        b.append("<tr>")
                .append("<td>").append(i.getName()).append("</td>")
                .append("<td>").append(i.getType()).append("</td>")
                .append("<td>").append(i.getEnumValue()).append("</td>")
                .append("<td>").append(i.getMandatory()).append("</td>")
                .append("<td>&nbsp;</td>");

        return ResponseEntity.ok(b.toString());
    }

    /**
     * get the images of the event
     *
     * @param eventId
     * @return
     */
    @RequestMapping(value = {"/admin/events/media/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> postGetMedia(@PathVariable("id") Long eventId) {
        Event e = eventsService.getEvent(eventId);
        Map<String, String> ret = new HashMap<>();
        ret.put("logo", e.getImage());
        ret.put("back", e.getTicketBackground());
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(value = {"admin/events/users/{id}"}, method = {RequestMethod.GET})
    public String getEventUsers(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        List<EventHostPermission> perms = eventsService.getEventHosts(eventId);
        model.addAttribute("hosts", perms);

        return "admin/events/event-users";
    }

    @RequestMapping(value = {"admin/events/users/{id}/add"}, method = {RequestMethod.GET})
    public String getAddEventUsers(Model model, HttpServletRequest request, @PathVariable("id") Long eventId) {

        Event event = eventsService.getEvent(eventId);
        model.addAttribute("event", event);

        List<EventHostPermission> perms = eventsService.getEventHosts(eventId);
        List<Long> alreadyIds = new ArrayList<>();
        perms.forEach((perm) -> {
            alreadyIds.add(perm.getHostId());
        });

        List<EventHost> availables;
        if (alreadyIds.isEmpty()) {
            availables = eventsService.getAllEventUsers();
        } else {
            availables = eventsService.getAllEventUsersNotIn(alreadyIds);
        }

        model.addAttribute("availables", availables);

        return "admin/events/event-users-add";
    }

    @RequestMapping(value = {"admin/events/users/{id}/add"}, method = {RequestMethod.POST})
    public String postAddEventUsers(Model model, HttpServletRequest request, @PathVariable("id") Long eventId, RedirectAttributes redirectAttributes) {

        String hostId = request.getParameter("user");
        if (hostId == null) {
            redirectAttributes.addFlashAttribute("message", "Error asignado usuario");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/admin#admin/events/users/" + eventId;
        }

        EventHostPermission perm = new EventHostPermission();
        perm.setHostId(Long.parseLong(hostId));
        perm.setEventId(eventId);
        eventsService.saveHostPermission(perm);

        redirectAttributes.addFlashAttribute("message", "Usuario asignado");
        redirectAttributes.addFlashAttribute("messageType", "success");

        return "redirect:/admin#admin/events/users/" + eventId;
    }

    @RequestMapping(value = {"/admin/events/tickets/addmore/{id}"}, method = {RequestMethod.POST})
    public String postAddMoreTickets(Model model, HttpServletRequest request, @PathVariable("id") Long eventId, RedirectAttributes redirectAttributes) {
        String scant = request.getParameter("add-tickets-count");
        int cant = Integer.parseInt(scant);
        eventsService.addMoreTicketsToEvent(eventId, cant);
        return "redirect:/admin#admin/events/tickets/" + eventId;
    }

    /**
     * get medias for event, open to use on any store
     *
     * @param model
     * @param request
     * @param response
     * @param token
     */
    @RequestMapping(value = {"/open/event/{token}"}, method = RequestMethod.GET)
    public void openGetEventContent(Model model, HttpServletRequest request, HttpServletResponse response, @PathVariable("token") String token) {
        try {

            if (token == null || token.isEmpty() || token.equals("null")) {
                //throw new Exception("not found");
                return;
            }

            String dir = "/var/wings/bo_media/events_content/design/" + token;
            File ret = new File(dir);

            FileInputStream fis;
            try {
                fis = new FileInputStream(ret);
            } catch (FileNotFoundException e) {
                return;
            }

            response.setContentType(MediaType.IMAGE_JPEG.getType());
            try {
                response.setContentLengthLong(ret.length());
            } catch (Exception e) {
            }
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + ret.getName() + "\"");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (IOException e) {
            log.error("Error getting image", e);
        }
    }

    /**
     * @param response
     * @param token
     */
    @RequestMapping(value = {"/open/events/media/{token}"}, method = RequestMethod.GET)
    public void openGetQrCode(HttpServletResponse response, @PathVariable("token") String token) {
        try {
            File f = QrUtils.generateQrCode(token);
            FileInputStream fis = new FileInputStream(f);
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
            response.setContentLengthLong(f.length());
            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();
        } catch (Exception e) {
        }
    }

    @RequestMapping(value = {"/open/events/tickets/{token}/{client}"}, method = RequestMethod.GET)
    public String openGetTicketPage(Model model, HttpServletRequest request, HttpServletResponse response, @PathVariable("token") String token, @PathVariable(value = "client", required = false) String client) {

        EventTicket ticket;
        if (token.equals("example")) {
            Long eventId = (Long) request.getSession().getAttribute("eventId");
            if (eventId == null) {
                return "error/error";
            }
            ticket = eventsService.getMockupTicket(eventsService.getEvent(eventId));
        } else {
            ticket = eventsService.getEventTicket(token);
        }

        model.addAttribute("ticket", ticket);
        model.addAttribute("url", "");

        if (client != null && client.equals("mobile")) {
            return "admin/events/ticket-mobile";
        } else {
            return "admin/events/ticket-desktop";
        }
    }

    @RequestMapping(value = {"/open/events/tickets/print/{token}/{client}"}, method = RequestMethod.GET)
    public void openPrintTicket(Model model, HttpServletRequest request, HttpServletResponse response,
            @PathVariable("token") String token,
            @PathVariable(value = "client", required = false) String client) {

        EventTicket ticket;
        if (token.equals("example")) {
            Long eventId = (Long) request.getSession().getAttribute("eventId");
            if (eventId == null) {
                return;
            }
            ticket = eventsService.getMockupTicket(eventsService.getEvent(eventId));
        } else {
            ticket = eventsService.getEventTicket(token);
        }

        if (ticket == null) {
            return;
        }

        model.addAttribute("ticket", ticket);

        String template;
        if (client != null && client.equals("mobile")) {
            template = "admin/events/ticket-mobile";
        } else {
            template = "admin/events/ticket-desktop";
        }

        FileOutputStream os = null;
        try {
            File outputFile = new File("/var/wings/bo_media/events_content/tickets/" + ticket.getTicketCode() + client + ".pdf");
            if (!outputFile.exists()) {
                String processedHtml = eventsService.getPrintableTicket(ticket, template);
                os = new FileOutputStream(outputFile);
                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocumentFromString(processedHtml);
                renderer.layout();
                renderer.createPDF(os, false);
                renderer.finishPDF();
                os.close();
            }

            FileInputStream fis = new FileInputStream(outputFile);
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            response.setContentLengthLong(outputFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=" + ticket.getTicketCode() + ".pdf");

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param token
     * @return
     */
    @RequestMapping(value = {"/eventos/{token}"}, method = RequestMethod.GET)
    public String getRegister(Model model, HttpServletRequest request, @PathVariable(value = "token", required = false) String token) {

        EventTicket ticket = eventsService.getEventTicketByInviteCode(token);
        model.addAttribute("ticket", ticket);

        //si el ticket no existe o todavia no se le asigno a nadie
        if (ticket == null || ticket.getStatus().equals(EventTicketStatus.created.name())) {
            return "events/error";
        }

        //ya esta completo, voy a imprimir
        if (ticket.getStatus().equals(EventTicketStatus.reclaimed.name())) {
            return "events/completed";
        }

        boolean disabled = false;
        if (disabled) {
            model.addAttribute("message", "Registro finalizado");
            return "events/error";
        }

        if (ticket.getSeatNumber() == null || (ticket.getSeatNumber() != null && ticket.getSeatNumber().startsWith("Platea"))) {
            return "events/error";
        }

        //si el ticket ya esta cerrado, mostrar finish page de ticket
        //update status and set audit
        String ip = AuthUtils.getIp(request);
        ticket.setStatus(EventTicketStatus.visited.name());
        ticket.setStatusAudit(DateUtils.formatNormalDateTime(new Date())
                + " "
                + ticket.getStatus()
                + " - "
                + ip
                + System.lineSeparator()
                + ((ticket.getStatusAudit() != null) ? ticket.getStatusAudit() : ""));
        eventsService.saveEventTicket(ticket);

        List<EventInfo> infos = eventsService.getEventInfos(ticket.getEventId());
        model.addAttribute("infos", infos);

        return "events/index";
    }

    /**
     *
     * @param model
     * @param request
     * @param token
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/eventos/{token}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postRegister(Model model, HttpServletRequest request, @PathVariable("token") String token, RedirectAttributes redirectAttributes) {

        boolean disabled = false;
        if (disabled) {
            return ResponseEntity.badRequest().body("El ticket no es válido");
        }

        EventTicket ticket = eventsService.getEventTicketByInviteCode(token);
        if (ticket == null) {
            return ResponseEntity.badRequest().body("El ticket no es válido");
        }

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String country = request.getParameter("country");
        String zip = request.getParameter("zip");

        if (firstName == null || firstName.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El nombre no es válido");
        }

        if (lastName == null || lastName.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El apellido no es válido");
        }

        if (email == null || email.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El correo electrónico no es válido");
        }

        if (address == null || address.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El domicilio no es válido");
        }

        if (city == null || city.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("La ciudad no es válida");
        }

        if (state == null || state.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("La provincia/estado no es válida");
        }

        if (country == null || country.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El país no es válido");
        }

        if (zip == null || zip.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("La código postal no es válido");
        }

        List<EventInfo> infos = eventsService.getEventInfos(ticket.getEventId());
        List<EventTicketInfo> toSave = new LinkedList<>();
        model.addAttribute("infos", infos);
        for (EventInfo info : infos) {
            String infoValue = request.getParameter(info.getName());

            if (info.getMandatory() && (infoValue == null || infoValue.trim().isEmpty())) {
                return ResponseEntity.badRequest().body("El campo " + info.getName() + " es obligatorio.");
            }

            EventTicketInfo eti = new EventTicketInfo();
            eti.setEventInfoId(info.getId());
            eti.setInfoName(info.getName());
            eti.setInfoValue(infoValue);
            eti.setTicketId(ticket.getId());
            toSave.add(eti);
        }

        try {

            ticket.setFirstName(firstName);
            ticket.setLastName(lastName);
            ticket.setEmail(email);
            ticket.setAddress(address);
            ticket.setCity(city);
            ticket.setState(state);
            ticket.setCountry(country);
            ticket.setZip(zip);

            String ip = AuthUtils.getIp(request);
            ticket.setStatus(EventTicketStatus.reclaimed.name());
            ticket.setReclaimDate(new Date());
            ticket.setStatusAudit(DateUtils.formatNormalDateTime(new Date())
                    + " "
                    + ticket.getStatus()
                    + " - "
                    + ip
                    + System.lineSeparator()
                    + ((ticket.getStatusAudit() != null) ? ticket.getStatusAudit() : ""));

            eventsService.saveEventTicket(ticket);

            if (!toSave.isEmpty()) {
                eventsService.saveEventTicketInfo(toSave);
            }

        } catch (Exception e) {
            log.error("Error registering ticket", e);
            return ResponseEntity.badRequest().body("Ocurrió un error inesperado, por favor intente nuevamente mas tarde.");
        }

        return ResponseEntity.ok("Registro completado con éxito.");
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/eventos/register"}, method = RequestMethod.GET)
    public String getRegisterIndex(Model model, HttpServletRequest request) {

        StoreItem storeItem = storeItemsRepository.findOne(127l);
        StoreItem storeItem2 = storeItemsRepository.findOne(128l);
        StoreItem storeItem3 = storeItemsRepository.findOne(129l);

        List<StoreItem> items = new ArrayList<>();
        items.add(storeItem);
        items.add(storeItem2);
        items.add(storeItem3);
        model.addAttribute("items", items);

        return "events/register";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/eventos/register"}, method = RequestMethod.POST)
    public ResponseEntity<String> postRegisterIndex(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String email = request.getParameter("email");
        String ticketNumber = request.getParameter("ticketNumber");
        String productId = request.getParameter("productId");

        if (email == null || email.isEmpty()) {
            return ResponseEntity.badRequest().body("Dirección de correo no válida.");
        }

        if (ticketNumber == null || ticketNumber.isEmpty()) {
            return ResponseEntity.badRequest().body("Número de entrada no válido.");
        }

        EventTicket ticket = eventsService.getEventTicketByTicketNumber(ticketNumber);
        if (ticket == null) {
            return ResponseEntity.badRequest().body("Número de entrada no válido.");
        }

        if (!ticket.getEmail().equals(email)) {
            return ResponseEntity.badRequest().body("Dirección de correo no válida.");
        }

        if (productId == null || productId.isEmpty()) {
            return ResponseEntity.badRequest().body("Por favor seleccione un kit de afiliación.");
        }

        if (ticket.getMemberId() != null) {
            return ResponseEntity.badRequest().body("Entrada de afiliado.");
        }

        StoreItem storeItem = storeItemsRepository.findOne(Long.parseLong(productId));
        List<String> types = Arrays.asList(OrderType.kit.name());
        Order already = ordersService.findFirstByExternalIdAndTypeInAndSource(ticket.getId(), types, "internal-event");
        if (already != null) {
            return ResponseEntity.badRequest().body("Entrada ya utilizada.");
        }

        //creo la orden
        Order o = new Order();

        o.setActivationDate(null);
        o.setCreationDate(new Date());
        o.setExternalId(ticket.getId());
        o.setFirstName(StringUtils.capitalize(ticket.getFirstName()));
        o.setLastName(StringUtils.capitalize(ticket.getLastName()));
        o.setPayerEmail(StringUtils.toLowerCase(ticket.getEmail()));
        o.setPaymentAmount(NumberUtils.convertMoneyFromEuros(storeItem.getPrice(), new Locale("es", "CO")));
        o.setPaymentCurrency("COP");
        o.setPaymentType("undef");
        o.setStoreTitle(storeItem.getName());
        o.setType(OrderType.kit.name());
        o.setItemId(storeItem.getId());
        o.setOrderItem(storeItem);
        o.setShippingStatus(ShipmentStatus.created.name());
        o.setMultiple(false);
        o.setSource("internal-event");
        o.setPrizeText(null);
        o.setWithPrize(false);

        o.setBillingCompany(null);
        o.setBillingAddress(StringUtils.capitalize(ticket.getAddress()));
        o.setBillingCity(StringUtils.capitalize(ticket.getCity()));
        o.setBillingCountry("CO");
        o.setBillingName(ticket.getFullName());
        o.setBillingPostalCode(ticket.getZip());
        o.setBillingState(ticket.getState());

        o.setDeleted(false);
        o.setStoreId(eventsService.getTicketSponsor(ticket.getId()));

        o.setPaymentStatus(OrderStatus.pending.name());
        o.setStatus(OrderStatus.pending.name());

        o = storeService.saveOrder(o);
        storeService.createMember(o);

        //create payment and redirect to URL
        String url = paymentsService.createPayment(o);

        if (url != null) {
            return ResponseEntity.ok(url);
        }

        return ResponseEntity.badRequest().body("");
    }

    //EVENTO ECUADOR BORRAR
    /**
     * ****************************************************
     */
    @Autowired
    private BisTempEcuadorRepository bter;
    private final boolean register = false;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/eventos/registerec"}, method = RequestMethod.GET)
    public String getRegisterIndexEC(Model model, HttpServletRequest request) {
        if (register) {
            return "events/registerec";
        } else {

            StoreItem storeItem = storeItemsRepository.findOne(136l);
            StoreItem storeItem2 = storeItemsRepository.findOne(137l);
            StoreItem storeItem3 = storeItemsRepository.findOne(138l);

            List<StoreItem> items = new ArrayList<>();
            items.add(storeItem);
            items.add(storeItem2);
            items.add(storeItem3);
            model.addAttribute("items", items);

            return "events/registerec_afiliate";
        }
    }

    private long pseudoId = 104000l;

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/eventos/registerec"}, method = RequestMethod.POST)
    public ResponseEntity<String> postRegisterIndexEc(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        if (register) {
            String nombre = request.getParameter("AAA");
            String apellido = request.getParameter("BBB");
            String email = request.getParameter("CCC");
            String telefono = request.getParameter("DDD");

            if (nombre == null || nombre.isEmpty()) {
                return ResponseEntity.badRequest().body("Nombre no válido.");
            }

            if (apellido == null || apellido.isEmpty()) {
                return ResponseEntity.badRequest().body("Apellido no válido.");
            }

            if (email == null || email.isEmpty()) {
                return ResponseEntity.badRequest().body("Dirección de correo no válida.");
            }

            if (telefono == null || telefono.isEmpty()) {
                return ResponseEntity.badRequest().body("Telefono no válido.");
            }

            BisTempEcuador b = new BisTempEcuador();

            b.setCreationDate(new Date());
            b.setEmail(email);
            b.setFirstName(email);
            b.setLastName(apellido);
            b.setPhone(telefono);

            bter.save(b);

            return ResponseEntity.ok("Registro almacenado");
        } else {

            String nombre = request.getParameter("firstName");
            String apellido = request.getParameter("lastName");
            String email = request.getParameter("email");
            String telefono = request.getParameter("phone");
            String productId = request.getParameter("productId");

            String sponsor = request.getParameter("sponsor");
            String address = request.getParameter("address");
            String city = request.getParameter("city");
            String state = request.getParameter("state");
            String zip = request.getParameter("country");

            if (sponsor == null || sponsor.trim().isEmpty()) {
                return ResponseEntity.badRequest().body("Patrocinador no válido.");
            }

            Member m = membersService.findByUsername(sponsor);
            if (m == null) {
                return ResponseEntity.badRequest().body("Patrocinador no encontrado.");
            }

            if (email == null || email.isEmpty()) {
                return ResponseEntity.badRequest().body("Dirección de correo no válida.");
            }

            /*List<BisTempEcuador> temps = bter.findByEmail(email);
            if (temps.isEmpty()) {
                return ResponseEntity.badRequest().body("Dirección de correo no registrada.");
            }
            BisTempEcuador tempToUse = temps.get(0);*/
            if (productId == null || productId.isEmpty()) {
                return ResponseEntity.badRequest().body("Por favor seleccione un kit de afiliación.");
            }

            StoreItem storeItem = storeItemsRepository.findOne(Long.parseLong(productId));
            List<String> types = Arrays.asList(OrderType.kit.name());

            try {
                EventTicket ticket = eventsService.getEventTicketByEmail(email, 3l);
                if (ticket != null) {
                    sponsor = eventsService.getTicketSponsor(ticket.getId());
                }
            } catch (Exception e) {
            }

            //creo la orden
            Order o = new Order();

            o.setActivationDate(null);
            o.setCreationDate(new Date());
            o.setExternalId(pseudoId++);
            o.setFirstName(StringUtils.capitalize(nombre));
            o.setLastName(StringUtils.capitalize(apellido));
            o.setPayerEmail(StringUtils.toLowerCase(email));
            o.setPaymentAmount(NumberUtils.convertMoneyFromEuros(storeItem.getPrice(), new Locale("es", "EC")));
            o.setPaymentCurrency("USD");
            o.setPaymentType("undef");
            o.setStoreTitle(storeItem.getName());
            o.setType(OrderType.kit.name());
            o.setItemId(storeItem.getId());
            o.setOrderItem(storeItem);
            o.setShippingStatus(ShipmentStatus.created.name());
            o.setMultiple(false);
            o.setSource("internal-event-ec-03");
            o.setPrizeText(null);
            o.setWithPrize(false);

            o.setBillingCompany(null);
            o.setBillingAddress(StringUtils.capitalize(address));
            o.setBillingCity(StringUtils.capitalize(city));
            o.setBillingCountry("EC");
            o.setBillingName(nombre + " " + apellido);
            o.setBillingPostalCode(zip);
            o.setBillingState(state);

            o.setDeleted(false);
            o.setStoreId(sponsor);

            o.setPaymentStatus(OrderStatus.pending.name());
            o.setStatus(OrderStatus.pending.name());

            o = storeService.saveOrder(o);
            storeService.createMember(o);

            //create payment and redirect to URL
            String url = paymentsService.createPayment(o);

            if (url != null) {
                return ResponseEntity.ok(url);
            }

            return ResponseEntity.badRequest().body("Ocurrió un error al afiliarse");

            //return ResponseEntity.ok("Registro almacenado");
        }

    }

}
