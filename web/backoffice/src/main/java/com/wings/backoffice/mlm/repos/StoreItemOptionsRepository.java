/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItemOption;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface StoreItemOptionsRepository extends JpaRepository<StoreItemOption, Long> {

    List<StoreItemOption> findByStoreItemId(Long storeItemId);

}
