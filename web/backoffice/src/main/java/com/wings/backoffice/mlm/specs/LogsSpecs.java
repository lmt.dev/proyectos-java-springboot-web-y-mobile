/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Log;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class LogsSpecs {

    public static Specification<Log> search(String q, Date from, Date to, String type, String comment) {

        return (Root<Log> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                try {
                    Long id = Long.parseLong(q);
                    predicates.add(
                            builder.or(
                                    builder.equal(root.get("id"), id),
                                    builder.equal(root.get("referenceId"), id)
                            )
                    );

                } catch (Exception e) {
                    String search = "%" + q + "%";
                    predicates.add(
                            builder.or(
                                    builder.like(root.get("message"), search)
                            )
                    );
                }
            }

            if (from != null) {
                Date withHours = DateUtils.getFirstHourOfDay(from);
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), withHours));
            }

            if (to != null) {
                Date withHour = DateUtils.getLastHourOfDay(to);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), withHour));
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            if (comment != null) {
                predicates.add(builder.equal(root.get("comment"), comment));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
