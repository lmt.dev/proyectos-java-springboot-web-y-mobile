/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.dao.CountryDto;
import com.wings.backoffice.mlm.dao.OrderDto;
import com.wings.backoffice.mlm.dao.ReportDto;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import com.wings.backoffice.mlm.domain.bis.orders.MembersView;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.ProductsView;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.MembersViewRepository;
import com.wings.backoffice.mlm.repos.OrdersViewRepository;
import com.wings.backoffice.mlm.repos.ProductsViewRepository;
import com.wings.backoffice.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ReportService {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private ProductsViewRepository productsViewRepository;
    @Autowired
    private OrdersViewRepository ordersViewRepository;
    @Autowired
    private MembersViewRepository membersViewRepository;

    private final Logger log = LoggerFactory.getLogger(ReportService.class);
    private final Map<String, Object> cache = new HashMap<>();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    /**
     *
     * @param country
     * @return
     */
    @Deprecated
    public Map<String, Object> getDeliveriesPerCountry(String country) {

        //devuelve
        Map<String, Object> ret = new LinkedHashMap<>();
        try {

            //remove report more than 1 day
            String cacheKey = country + "getDeliveriesPerCountry";
            if (cache.containsKey(cacheKey)) {

                ret = (Map) cache.get(cacheKey);
                String fecha = (String) ret.get("Fecha reporte");
                Date date = DateUtils.fromNormalStringToDate(fecha);
                Date todaynight = DateUtils.getLastHourOfDay(new Date());
                long diffDays = DateUtils.getDiffDays(date, todaynight);

                if (diffDays >= 1) {
                    cache.remove(cacheKey);
                } else {
                    return ret;
                }
            }

            //member id<W3,cantitad>
            Map<Long, Map<String, Integer>> products = new HashMap<>();
            Map<String, Map<String, Integer>> productsPerMember = new HashMap<>();
            Map<Long, Map<String, Integer>> productsPerMemberById = new HashMap<>();
            //memberid,orderId
            Map<Long, Long> memberOrders = new HashMap<>();

            //cantidad de kits
            Map<String, Integer> kitsCant = new HashMap<>();

            List<Member> members;
            if (country.equals("all")) {
                members = membersRepository.findAll();
            } else {
                members = membersRepository.findByCountry(country);
            }

            //List<Member> members = new ArrayList<>();
            //members.add(membersRepository.findOne(867l));
            //armo la lista de ordenes a entregar
            for (Member member : members) {

                //si esta eliminado, continuo
                if (member.getDeleted()) {
                    continue;
                }

                //si es free continuo
                if (member.getOrder() == null || member.getOrder().getItemId() == 15) {
                    continue;
                }

                //si tiene orden eliminada
                if (member.getOrder() == null || member.getOrder().isDeleted()) {
                    continue;
                }

                //si tiene el kit de afiliacion pagado
                if (member.getOrder().getStatus().equals(OrderStatus.paid.name())) {

                    //si debe alguna cuota, lo salto
                    boolean todoPago = true;
                    List<Order> cuotas = ordersService.getCuotasOrder(member);
                    for (Order cuota : cuotas) {
                        if (!cuota.getStatus().equals(OrderStatus.paid.name())) {
                            todoPago = false;
                            break;
                        }
                    }

                    if (!todoPago) {
                        continue;
                    }

                    //pongo el kit de afiliacion si tiene todo pago
                    memberOrders.put(member.getId(), member.getOrderId());

                    //get member upgrades, 
                    List<Order> upgrades = ordersService.getMemberUpgrades(member);
                    for (Order upgrade : upgrades) {
                        if (!upgrade.getStatus().equals(OrderStatus.paid.name())) {
                            todoPago = false;
                            break;
                        }
                    }

                    //si tiene algun upgrade, y no esta pago
                    if (!todoPago) {
                        memberOrders.remove(member.getId());
                        continue;
                    }

                    //si tiene mas de un upgrade pago, lo salto
                    if (upgrades.size() > 1) {
                        log.error("Revisar afiliado, mas de un upgrade pago " + member.getId() + " " + member);
                        continue;
                    }

                    //pongo el kit de afiliacion si tiene todo pago
                    if (!upgrades.isEmpty()) {
                        memberOrders.put(member.getId(), upgrades.get(0).getId());
                    }
                }
            }

            //saco esto, para que cumpla todo lo W6
            //recorro las ordenes y verifico que no hay shippings
            /*List<Long> toRemove = new ArrayList<>();
        for (Map.Entry<Long, Long> entry : memberOrders.entrySet()) {
            Long key = entry.getKey();
            Long value = entry.getValue();
            List<Shipment> ships = shipmentsRepository.findByOrderIdAndDeleted(value, false);
            if (!ships.isEmpty()) {
                toRemove.add(key);
            }
        }*/
            //saco las ordenes que tienen shipping
            /*for (Long memberId : toRemove) {
            memberOrders.remove(memberId);
        }*/
            //finalmente saco la cantidad de productos a entregar        
            for (Map.Entry<Long, Long> entry : memberOrders.entrySet()) {
                Long memberId = entry.getKey();
                Long orderId = entry.getValue();

                Order order = ordersService.getOrder(orderId);

                //si la orden esta eliminada, continuo
                if (order.isDeleted()) {
                    continue;
                }

                long itemId = order.getItemId();

                String kitName = order.getOrderItem().getName();
                if (kitsCant.containsKey(kitName)) {
                    int cantKit = kitsCant.get(kitName);
                    cantKit++;
                    kitsCant.put(kitName, cantKit);
                } else {
                    kitsCant.put(kitName, 1);
                }

                Map<String, Integer> prods;
                if (!products.containsKey(memberId)) {
                    prods = new HashMap<>();
                    products.put(memberId, prods);
                    productsPerMember.put(order.getMember().toString(), prods);

                    productsPerMemberById.put(order.getId(), prods);

                } else {
                    prods = products.get(memberId);
                }

                //si es un upgrade, pongo el id del upgrade final
                if (order.getOrderItem().getFinalItemId() != null) {
                    itemId = order.getOrderItem().getFinalItemId();
                }

                //starter
                if (itemId == 3 || itemId == 69 || itemId == 89 || itemId == 82 || itemId == 206 || itemId == 190 || itemId == 127) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }
                }

                //starterx2
                if (itemId == 41 || itemId == 128) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }
                }

                //starter PRO y STARTER 2 viejo
                if (itemId == 4 || itemId == 70 || itemId == 90 || itemId == 83 || itemId == 207 || itemId == 195) {
                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }
                }

                //combo
                if (itemId == 71 || itemId == 91 || itemId == 84 || itemId == 208 || itemId == 196) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR");
                        cantW3++;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //combo pro / combo viejo 5
                if (itemId == 5 || itemId == 72 || itemId == 92 || itemId == 85 || itemId == 209 || itemId == 197 || itemId == 129) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }
                }

                //combo pro viejo 
                if (itemId == 6) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }
                }

                //combo bussines viejo 
                if (itemId == 7) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        cantW3++;
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 3);
                    }
                }

                //combo founder viejo 
                if (itemId == 8) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        cantW3++;
                        cantW3++;
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 4);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        cantW3++;
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 3);
                    }
                }

                //mini founder
                if (itemId == 68 || itemId == 73 || itemId == 93 || itemId == 86) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR");
                        cantW3++;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //golden founder
                if (itemId == 74 || itemId == 94 || itemId == 87 || itemId == 210 || itemId == 198) {

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR");
                        cantW3++;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //diamond founder
                if (itemId == 75 || itemId == 95 || itemId == 88 || itemId == 211 || itemId == 199) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3");
                        cantW3++;
                        cantW3++;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR");
                        cantW3++;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //Promos / peru - ecuador
                if (itemId == 65 || itemId == 67) {
                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6");
                        cantW3++;
                        cantW3++;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR");
                        cantW3++;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //Wings Store Base
                if (itemId == 50) {
                    if (prods.containsKey("WINGS STORE BASE")) {
                        int cantW3 = prods.get("WINGS STORE BASE");
                        cantW3++;
                        prods.put("WINGS STORE BASE", cantW3);
                    } else {
                        prods.put("WINGS STORE BASE", 1);
                    }
                }

                //Wings Store CORNER LIGTH
                if (itemId == 51) {
                    if (prods.containsKey("WINGS STORE CORNER LIGHT")) {
                        int cantW3 = prods.get("WINGS STORE CORNER LIGHT");
                        cantW3++;
                        prods.put("WINGS STORE CORNER LIGHT", cantW3);
                    } else {
                        prods.put("WINGS STORE CORNER LIGHT", 1);
                    }
                }

                //Wings Store CORNER FULL
                if (itemId == 52) {
                    if (prods.containsKey("WINGS STORE CORNER FULL")) {
                        int cantW3 = prods.get("WINGS STORE CORNER FULL");
                        cantW3++;
                        prods.put("WINGS STORE CORNER FULL", cantW3);
                    } else {
                        prods.put("WINGS STORE CORNER FULL", 1);
                    }
                }

                //Wings Store FULL
                if (itemId == 53) {
                    if (prods.containsKey("WINGS STORE FULL")) {
                        int cantW3 = prods.get("WINGS STORE FULL");
                        cantW3++;
                        prods.put("WINGS STORE FULL", cantW3);
                    } else {
                        prods.put("WINGS STORE FULL", 1);
                    }
                }

            }

            //armo el reporte final        
            Map<String, String> totalesPorProducto = new HashMap<>();
            int totalW3 = 0;
            int totalW6 = 0;
            int total14 = 0;

            int wsb = 0;
            int wscl = 0;
            int wscf = 0;
            int wsf = 0;

            for (Map.Entry<Long, Map<String, Integer>> entry : products.entrySet()) {
                Long key = entry.getKey();
                Map<String, Integer> value = entry.getValue();
                if (value != null) {
                    if (value.containsKey("W3")) {
                        totalW3 += value.get("W3");
                    }

                    if (value.containsKey("W6")) {
                        totalW6 += value.get("W6");
                    }

                    if (value.containsKey("WingsONEFOUR")) {
                        total14 += value.get("WingsONEFOUR");
                    }

                    if (value.containsKey("WINGS STORE BASE")) {
                        wsb += value.get("WINGS STORE BASE");
                    }

                    if (value.containsKey("WINGS STORE CORNER LIGHT")) {
                        wscl += value.get("WINGS STORE CORNER LIGHT");
                    }

                    if (value.containsKey("WINGS STORE CORNER FULL")) {
                        wscf += value.get("WINGS STORE CORNER FULL");
                    }

                    if (value.containsKey("WINGS STORE FULL")) {
                        wsf += value.get("WINGS STORE FULL");
                    }
                }
            }

            if (totalW3 > 0) {
                totalesPorProducto.put("W3", String.valueOf(totalW3));
            }

            if (totalW6 > 0) {
                totalesPorProducto.put("W6", String.valueOf(totalW6));
            }

            if (total14 > 0) {
                totalesPorProducto.put("WingsONEFOUR", String.valueOf(total14));
            }

            if (wsb > 0) {
                totalesPorProducto.put("WINGS STORE BASE", String.valueOf(wsb));
            }

            if (wscl > 0) {
                totalesPorProducto.put("WINGS STORE CORNER LIGHT", String.valueOf(wscl));
            }

            if (wscf > 0) {
                totalesPorProducto.put("WINGS STORE CORNER FULL", String.valueOf(wscf));
            }

            if (wsf > 0) {
                totalesPorProducto.put("WINGS STORE FULL", String.valueOf(wsf));
            }

            ret.put("Pais", country);
            ret.put("Fecha reporte", DateUtils.formatNormalDateTime(new Date()));
            ret.put("1. Total de productos", totalesPorProducto);
            ret.put("2. Total de Kits", kitsCant);
            ret.put("3. Productos por afiliado", productsPerMember);

            cache.put(cacheKey, ret);

            ////////////////////////////REPORTE
            StringBuilder b = new StringBuilder();
            b.append("id; creation_date; activation_date; payer_email; first_name; last_name; store_title; billing_name; billing_address; billing_city; billing_postal_code; billing_state; billing_country; billing_company; dni; phone; cantidad; producto");
            b.append(System.lineSeparator());

            for (Map.Entry<Long, Map<String, Integer>> entry : productsPerMemberById.entrySet()) {
                Long key = entry.getKey();
                Map<String, Integer> value = entry.getValue();

                String producto = null;
                int cantidad = 0;
                boolean exit = true;

                for (Map.Entry<String, Integer> entry1 : value.entrySet()) {
                    String tempProd = entry1.getKey();
                    int temCant = entry1.getValue();

                    if (tempProd.equals("W6")) {
                        producto = tempProd;
                        cantidad = temCant;
                        exit = false;
                        break;
                    }
                }

                if (exit) {
                    continue;
                }

                if (cantidad == 0) {
                    continue;
                }

                Order order = ordersService.getOrder(key);
                Member m = membersRepository.findOne(order.getMemberId());

                String email = m.getEmail();
                String address = m.getStreet() != null ? m.getStreet().replaceAll(";", " ") : order.getBillingAddress();
                String name = m.getFirstName();
                String lastName = m.getLastName();
                String billingName = m.toString();
                String city = m.getCity();
                String postal = m.getPostal();
                String state = m.getState();
                String scountry = m.getCountry();
                String dni = m.getDni() != null ? m.getDni() : order.getDni();
                String phone = m.getPhone() != null ? m.getPhone() : "";
                phone = "\"" + phone + "\"";

                b.append(order.getId());
                b.append(";");
                b.append(DateUtils.formatNormalDateTime(order.getCreationDate()));
                b.append(";");
                b.append(DateUtils.formatNormalDateTime(order.getActivationDate()));
                b.append(";");
                b.append(email);
                b.append(";");
                b.append(name);
                b.append(";");
                b.append(lastName);
                b.append(";");
                b.append(order.getStoreTitle() != null ? order.getStoreTitle().replaceAll("\n", "") : "");
                b.append(";");
                b.append(billingName);
                b.append(";");
                b.append(address);
                b.append(";");
                b.append(city);
                b.append(";");
                b.append(postal);
                b.append(";");
                b.append(state);
                b.append(";");
                b.append(scountry);
                b.append(";");
                b.append(order.getBillingCompany() != null ? order.getBillingCompany() : "");
                b.append(";");
                b.append(dni);
                b.append(";");
                b.append(phone);
                b.append(";");
                b.append(cantidad);
                b.append(";");
                b.append(producto);
                b.append(";");

                b.append(System.lineSeparator());

            }

            //System.out.println(b.toString());
        } catch (Exception e) {
            log.error("Error creating report", e);
        }

        return ret;
    }

    public ReportDto getOrdersByCountryDto(List<String> countries, List<Integer> months, List<Integer> years, Date f, Date t, String currency) {
        final ReportDto reportDto = new ReportDto();
        countries.forEach(country -> {
            final CountryDto cd = new CountryDto(country, null, null, null);
            if (f == null) {
                getOrdersDtoPaidByCountryMonthAndYear(cd, months, years);
            } else {
                cd.setOrders(ordersService.getOrdersDtoPaidByCountryFromAndTo(cd.getCountry(), f, t));
            }

            reportDto.addCountryDto(cd);
            if (currency != null) {
                cd.setCurrency(currency);
            }
        });

        return reportDto;
    }

    public ReportDto getProductsByCountryDto(List<String> countries, List<Integer> months, List<Integer> years, Date f, Date t, String installmentComplete) {
        final ReportDto reportDto = new ReportDto();
        countries.forEach(country -> {
            final CountryDto cd = new CountryDto(country, null, null, null);
            List<ProductsView> orders;
            if (f != null) {
                orders = productsViewRepository.findByBillingCountryAndPeriod(country, f, t);
            } else {
                orders = findByBillingCountryAndPeriod(country, months, years);
            }
            if (installmentComplete.equals("1") || installmentComplete.equals("2")) {
                cd.setProductsComplete(getProductsPerCountry(orders, true));
            }
            if (installmentComplete.equals("1") || installmentComplete.equals("3")) {
                cd.setProductsPartial(getProductsPerCountry(orders, false));
            }
            reportDto.addCountryDto(cd);
        });

        return reportDto;
    }

    public Map<Integer, Map<String, Map<String, Integer>>> getProductsPerCountry(List<ProductsView> orders, Boolean installment) {
        List<ProductsView> ordersFinal = new ArrayList<>();

        Map<Integer, Map<String, Map<String, Integer>>> ret = new LinkedHashMap<>();
        try {
//            for (ProductsView order : orders) {
//                //si debe alguna cuota, lo salto
//                boolean todoPago = true;
//
//                List<Order> upgrades = null;
//                //get member upgrades, en caso de no tener member id es porque no es afiliado, no se buscan sus upgrades
//                if (order.getMemberId() != null) {
//                    upgrades = ordersService.getMemberUpgrades(order.getMemberId());
//                    for (Order upgrade : upgrades) {
//                        if (!upgrade.getStatus().equals(OrderStatus.paid.name())) {
//                            todoPago = false;
//                            break;
//                        }
//                    }
//                }
//
//                //si tiene algun upgrade, y no esta pago
//                if (!todoPago) {
//                    continue;
//                }
//
//                //si tiene mas de un upgrade pago, lo salto
//                if (upgrades != null && upgrades.size() > 1) {
//                    log.error("Revisar afiliado, mas de un upgrade pago " + order.getMemberId());
//                    continue;
//                }
//                ordersFinal.add(order);
//            }

            //finalmente saco la cantidad de productos a entregar        
            for (ProductsView product : orders) {
                if (installment) {
                    if (product.getInstallmentNumber() != null && product.getInstallmentNumber() > 0 && product.getInstallmentComplete().equals(!installment)) {
                        continue;
                    }
                } else if (installment == false) {
                    if (product.getInstallmentNumber() == null || product.getInstallmentNumber() == 0 || (product.getInstallmentNumber() == 1 && product.getInstallmentComplete().equals(!installment))) {
                        continue;
                    }
                }
                String mes = product.getMonthStr();
                Integer year = product.getYear();

                //Mes, Producto, Cantidad
                Map<String, Map<String, Integer>> months = new HashMap<>();
                if (ret.containsKey(year)) {
                    months = ret.get(year);
                } else {
                    ret.put(year, months);
                }

                Map<String, Integer> prods = new HashMap<>();
                //prods.put("W2", 0);
                prods.put("W3", 0);
                prods.put("W4", 0);
                //prods.put("W5", 0);
                prods.put("W6", 0);
                prods.put("W7", 0);
                prods.put("WingsONEFOUR", 0);
                prods.put("Wings Scooter", 0);
                prods.put("Smartwatch Executive", 0);
                prods.put("Computador All In One", 0);
                prods.put("WINGS STORE BASE", 0);
                prods.put("WINGS STORE CORNER LIGHT", 0);
                prods.put("WINGS STORE CORNER FULL", 0);
                prods.put("WINGS STORE FULL", 0);
                if (months.containsKey(mes)) {
                    prods = months.get(mes);
                } else {
                    months.put(mes, prods);
                }

                long itemId = product.getItemId();

                //starter
                if (itemId == 3 || itemId == 69 || itemId == 89 || itemId == 82 || itemId == 206 || itemId == 190 || itemId == 127) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 1;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }
                }

                //starterx2
                if (itemId == 41 || itemId == 128) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 2;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }
                }

                //starter PRO y STARTER 2 viejo
                if (itemId == 4 || itemId == 70 || itemId == 90 || itemId == 83 || itemId == 207 || itemId == 195) {
                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 1;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }
                }

                //combo
                if (itemId == 71 || itemId == 91 || itemId == 84 || itemId == 208 || itemId == 196) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 1;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //combo pro / combo viejo 5
                if (itemId == 5 || itemId == 72 || itemId == 92 || itemId == 85 || itemId == 209 || itemId == 197 || itemId == 129) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 1;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 1;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }
                }

                //combo pro viejo 
                if (itemId == 6) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 1;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 2;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }
                }

                //combo bussines viejo 
                if (itemId == 7) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 2;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 3;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 3);
                    }
                }

                //combo founder viejo 
                if (itemId == 8) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 4;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 4);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 3;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 3);
                    }
                }

                //mini founder
                if (itemId == 68 || itemId == 73 || itemId == 93 || itemId == 86) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 1;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 1);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 1;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //golden founder
                if (itemId == 74 || itemId == 94 || itemId == 87 || itemId == 210 || itemId == 198) {

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 1;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 1);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //diamond founder
                if (itemId == 75 || itemId == 95 || itemId == 88 || itemId == 211 || itemId == 199) {
                    if (prods.containsKey("W3")) {
                        int cantW3 = prods.get("W3") + 2;
                        prods.put("W3", cantW3);
                    } else {
                        prods.put("W3", 2);
                    }

                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 2;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //Promos / peru - ecuador
                if (itemId == 65 || itemId == 67) {
                    if (prods.containsKey("W6")) {
                        int cantW3 = prods.get("W6") + 2;
                        prods.put("W6", cantW3);
                    } else {
                        prods.put("W6", 2);
                    }

                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //Wings Store Base
                if (itemId == 50) {
                    if (prods.containsKey("WINGS STORE BASE")) {
                        int cantW3 = prods.get("WINGS STORE BASE") + 1;
                        prods.put("WINGS STORE BASE", cantW3);
                    } else {
                        prods.put("WINGS STORE BASE", 1);
                    }
                }

                //Wings Store CORNER LIGTH
                if (itemId == 51) {
                    if (prods.containsKey("WINGS STORE CORNER LIGHT")) {
                        int cantW3 = prods.get("WINGS STORE CORNER LIGHT") + 1;
                        prods.put("WINGS STORE CORNER LIGHT", cantW3);
                    } else {
                        prods.put("WINGS STORE CORNER LIGHT", 1);
                    }
                }

                //Wings Store CORNER FULL
                if (itemId == 52) {
                    if (prods.containsKey("WINGS STORE CORNER FULL")) {
                        int cantW3 = prods.get("WINGS STORE CORNER FULL") + 1;
                        prods.put("WINGS STORE CORNER FULL", cantW3);
                    } else {
                        prods.put("WINGS STORE CORNER FULL", 1);
                    }
                }

                //Wings Store FULL
                if (itemId == 53) {
                    if (prods.containsKey("WINGS STORE FULL")) {
                        int cantW3 = prods.get("WINGS STORE FULL") + 1;
                        prods.put("WINGS STORE FULL", cantW3);
                    } else {
                        prods.put("WINGS STORE FULL", 1);
                    }
                }

                //W4
                if (itemId == 298 || itemId == 304 || itemId == 305 || itemId == 306 || itemId == 307 || itemId == 311
                        || itemId == 312 || itemId == 314 || itemId == 316 || itemId == 319 || itemId == 320 || itemId == 321 || itemId == 322 || itemId == 323 || itemId == 324) {
                    if (prods.containsKey("W4")) {
                        int cantW4 = prods.get("W4") + itemId == 298 ? 1 : 2;
                        prods.put("W4", cantW4);
                    } else {
                        prods.put("W4", itemId == 298 ? 1 : 2);
                    }
                }

                //W7
                if (itemId == 299 || itemId == 300 || itemId == 302 || itemId == 303 || itemId == 304 || itemId == 305
                        || itemId == 309 || itemId == 310 || itemId == 317 || itemId == 318 || itemId == 319 || itemId == 320 || itemId == 321 || itemId == 322 || itemId == 323 || itemId == 324) {
                    if (prods.containsKey("W4")) {
                        int cantW4 = prods.get("W4") + itemId == 319 || itemId == 320 || itemId == 321 || itemId == 322 || itemId == 323 || itemId == 324 ? 2 : 1;
                        prods.put("W4", cantW4);
                    } else {
                        prods.put("W4", itemId == 319 || itemId == 320 || itemId == 321 || itemId == 322 || itemId == 323 || itemId == 324 ? 2 : 1);
                    }
                }

                //Wings OneFour
                if (itemId == 302 || itemId == 303 || itemId == 306 || itemId == 307 || itemId == 319 || itemId == 320) {
                    if (prods.containsKey("WingsONEFOUR")) {
                        int cantW3 = prods.get("WingsONEFOUR") + 1;
                        prods.put("WingsONEFOUR", cantW3);
                    } else {
                        prods.put("WingsONEFOUR", 1);
                    }
                }

                //Scooter and Smartwatch
                if (itemId == 314 || itemId == 316 || itemId == 317 || itemId == 318 || itemId == 323 || itemId == 324) {
                    if (prods.containsKey("Wings Scooter")) {
                        int cantW3 = prods.get("Wings Scooter") + 1;
                        prods.put("Wings Scooter", cantW3);
                    } else {
                        prods.put("Wings Scooter", 1);
                    }

                    if (prods.containsKey("Smartwatch Executive")) {
                        int cantW3 = prods.get("Smartwatch Executive") + 2;
                        prods.put("Smartwatch Executive", cantW3);
                    } else {
                        prods.put("Smartwatch Executive", 2);
                    }
                }

                //Computador All In One
                if (itemId == 309 || itemId == 310 || itemId == 311 || itemId == 312 || itemId == 321 || itemId == 322) {
                    if (prods.containsKey("Computador All In One")) {
                        int cantW3 = prods.get("Computador All In One") + 1;
                        prods.put("Computador All In One", cantW3);
                    } else {
                        prods.put("Computador All In One", 1);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error creating report", e);
        }

        return ret;
    }

    public ReportDto getMembersByCountryDto(List<String> countries, List<Integer> months, List<Integer> years, Date f, Date t) {
        final ReportDto reportDto = new ReportDto();
        countries.forEach(country -> {
            final CountryDto cd = new CountryDto(country, null, null, null);
            List<MembersView> members;
            if (f != null) {
                members = membersViewRepository.findByBillingCountryAndPeriod(country, f, t);
            } else {
                members = findMembersByBillingCountryAndPeriod(country, months, years);
            }
            cd.setMembers(members);
            reportDto.addCountryDto(cd);
        });

        return reportDto;
    }

    /*
        MÉTODOS DE SOPORTE
     */
    private void getOrdersDtoPaidByCountryMonthAndYear(CountryDto cd, List<Integer> months, List<Integer> years) {
        years.stream().forEach(year -> {
            months.stream().forEach(month -> {
                cd.addOrders(ordersService.getOrdersDtoPaidByCountryMonthAndYear(cd.getCountry(), month, year));
            });
        });
    }

    private List<ProductsView> findByBillingCountryAndPeriod(String country, List<Integer> months, List<Integer> years) {
        List<String> smonths = new ArrayList<>();
        months.forEach((month) -> {
            smonths.add(month.toString());
        });

        List<String> syears = new ArrayList<>();
        years.forEach((year) -> {
            syears.add(year.toString());
        });
        List<ProductsView> orders = ordersService.getProductsViewPaidByCountryMonthAndYear(country, smonths, syears);

        return orders;
    }

    private List<MembersView> findMembersByBillingCountryAndPeriod(String country, List<Integer> months, List<Integer> years) {
        List<String> smonths = new ArrayList<>();
        months.forEach((month) -> {
            smonths.add(month.toString());
        });

        List<String> syears = new ArrayList<>();
        years.forEach((year) -> {
            syears.add(year.toString());
        });
        List<MembersView> members = membersViewRepository.findByBillingCountryAndMonth(country, smonths, syears);

        return members;
    }

    public ReportDto getOrdersIncompleteByCountryDto(List<String> countries, List<Integer> years, String currency) {
        final ReportDto reportDto = new ReportDto();
        countries.forEach(country -> {
            final CountryDto cd = new CountryDto(country);
            final List<OrderDto> orders = ordersService.getOrdersDtoPending(country, years);
            cd.setOrders(orders);
            cd.setCurrency(currency);
            reportDto.addCountryDto(cd);
        });

        return reportDto;
    }

    public String getOrderPendingCsv(List<OrderDto> orders) {
        StringBuilder b = new StringBuilder();
        b.append("#;Fecha de Creacion;Email;Nombre y Apellido;Kit;Precio").append(System.lineSeparator());
        orders.forEach((order) -> {
            b.append(order.getId())
                    .append(";")
                    .append(sdf.format(order.getCreationDate()))
                    .append(";")
                    .append(order.getPayerEmail())
                    .append(";")
                    .append(order.getClient())
                    .append(";")
                    .append(order.getItem() != null ? order.getItem() : order.getStoreTitle())
                    .append(";")
                    .append(order.getPaymentAmountStr())
                    .append(System.lineSeparator());

        });

        return b.toString();
    }

    public String getProductsCsv(ReportDto reportDto, List<String> columnas, List<Integer> years, List<Integer> months, String installment) {
        StringBuilder b = new StringBuilder();

        for (CountryDto country : reportDto.getCountryDtos()) {

            for (Integer year : years) {
                if (installment.equals("1") || installment.equals("2")) {

                    b.append(country.getCountry()).append(" ").append(year).append(" Pago Completo").append(System.lineSeparator());
                    b.append("Mes").append(";").append("Total").append(";");
                    for (String columna : columnas) {
                        b.append(columna).append(";");
                    }
                    b.append(System.lineSeparator());
                    for (Integer month : months) {
                        b.append(DateUtils.getMonthName(month)).append(";").append(country.getTotalProductsCompleteByYearAndMonth(year, month)).append(";");
                        for (String columna : columnas) {
                            b.append(country.getTotalProductsCompleteByYearAndMonth(year, month, columna)).append(";");
                        }
                        b.append(System.lineSeparator());
                    }

                    b.append("Total").append(";").append(country.getTotalProductsCompleteByYear(year)).append(";");
                    for (String columna : columnas) {
                        b.append(country.getTotalProductsCompleteByYearAndProduct(year, columna)).append(";");
                    }
                    b.append(System.lineSeparator());
                    b.append(System.lineSeparator());
                }
                if (installment.equals("1") || installment.equals("3")) {
                    b.append(country.getCountry()).append(" ").append(year).append(" Pago Parcial").append(System.lineSeparator());
                    b.append("Mes").append(";").append("Total").append(";");
                    for (String columna : columnas) {
                        b.append(columna).append(";");
                    }
                    b.append(System.lineSeparator());
                    for (Integer month : months) {
                        b.append(DateUtils.getMonthName(month)).append(";").append(country.getTotalProductsPartialByYearAndMonth(year, month)).append(";");
                        for (String columna : columnas) {
                            b.append(country.getTotalProductsPartialByYearAndMonth(year, month, columna)).append(";");
                        }
                        b.append(System.lineSeparator());
                    }

                    b.append("Total").append(";").append(country.getTotalProductsPartialByYear(year)).append(";");
                    for (String columna : columnas) {
                        b.append(country.getTotalProductsPartialByYearAndProduct(year, columna)).append(";");
                    }
                    b.append(System.lineSeparator());
                    b.append(System.lineSeparator());
                }
            }

        }
        return b.toString();
    }

    public String getMembersCsv(ReportDto reportDto, List<Integer> years, List<Integer> months) {
        StringBuilder b = new StringBuilder();
        List<CountryDto> countries = reportDto.getCountryDtos();
        boolean total = countries.size() > 1;
        for (Integer year : years) {
            b.append(year).append(System.lineSeparator());
            b.append("Registraciones").append(System.lineSeparator());
            b.append("Mes").append(";");
            if (total) {
                b.append("Total").append(";");
            }
            for (CountryDto country : countries) {
                b.append(messageSource.getMessage(country.getCountry(), null, new Locale("es", "ES"))).append(";");
            }
            b.append(System.lineSeparator());
            for (Integer month : months) {
                b.append(DateUtils.getMonthName(month)).append(";");
                if (total) {
                    b.append(reportDto.membersByYearAndMonth(year, month, false)).append(";");
                }
                for (CountryDto country : countries) {
                    b.append(country.getMembersByActiveAndYearAndMonth(year, month, false)).append(";");
                }
                b.append(System.lineSeparator());
            }
            b.append("Total").append(";");
            if (total) {
                b.append(reportDto.membersByYear(year, false)).append(";");
            }
            for (CountryDto country : countries) {
                b.append(country.getMembersByActiveAndYear(year, false)).append(";");
            }
            b.append(System.lineSeparator());
            b.append(System.lineSeparator());

            b.append("Afiliaciones").append(System.lineSeparator());
            b.append("Mes").append(";");
            if (total) {
                b.append("Total").append(";");
            }
            for (CountryDto country : countries) {
                b.append(messageSource.getMessage(country.getCountry(), null, new Locale("es", "ES"))).append(";");
            }
            b.append(System.lineSeparator());
            for (Integer month : months) {
                b.append(DateUtils.getMonthName(month)).append(";");
                if (total) {
                    b.append(reportDto.membersByYearAndMonth(year, month, true)).append(";");
                }
                for (CountryDto country : countries) {
                    b.append(country.getMembersByActiveAndYearAndMonth(year, month, true)).append(";");
                }
                b.append(System.lineSeparator());
            }
            b.append("Total").append(";");
            if (total) {
                b.append(reportDto.membersByYear(year, true)).append(";");
            }
            for (CountryDto country : countries) {
                b.append(country.getMembersByActiveAndYear(year, true)).append(";");
            }
            b.append(System.lineSeparator());
            b.append(System.lineSeparator());

        }
        return b.toString();
    }

    public String getOrdersByCountryCsv(ReportDto reportDto, List<Integer> years, List<Integer> months) {
        StringBuilder b = new StringBuilder();

        for (CountryDto country : reportDto.getCountryDtos()) {
            for (Integer year : years) {
                b.append(messageSource.getMessage(country.getCountry(), null, new Locale("es", "ES"))).append(" ").append(year).append(System.lineSeparator());
                b.append("Mes").append(";").append("Total").append(";").append(messageSource.getMessage("bank_transfer", null, new Locale("es", "ES"))).append(";")
                        .append(messageSource.getMessage("others", null, new Locale("es", "ES"))).append(";")
                        .append(messageSource.getMessage("coms", null, new Locale("es", "ES"))).append(";");
                b.append(System.lineSeparator());
                for (Integer month : months) {
                    b.append(DateUtils.getMonthName(month)).append(";");
                    b.append(country.getTotalByMonthStr(month, year)).append(";");
                    b.append(country.getTotalBacsByMonthStr(month, year)).append(";");
                    b.append(country.getTotalOthersByMonthStr(month, year)).append(";");
                    b.append(country.getTotalComsByMonthStr(month, year)).append(";");
                    b.append(System.lineSeparator());
                }

                b.append("Total").append(";").append(country.getTotalByYearStr(year)).append(";")
                        .append(country.getTotalBacsByYearStr(year)).append(";")
                        .append(country.getTotalOthersByYearStr(year)).append(";")
                        .append(country.getTotalComsByYearStr(year));
                b.append(System.lineSeparator());
                b.append(System.lineSeparator());
            }
        }
        return b.toString();
    }

    public String getPromoLatamCsv(List<PromoLatam> items) {
        StringBuilder b = new StringBuilder();
        b.append("País;Email;Afiliado;Rango Inicio; Puntos de Equipo; Puntos de Equipo (%); Califica").append(System.lineSeparator());
        items.forEach((item) -> {
            b.append(messageSource.getMessage(item.getCountry(), null, new Locale("es", "ES"))).append(";")
                    .append(item.getEmail()).append(";")
                    .append(item.getFirstName()).append(" ").append(item.getLastName()).append(" - ").append(item.getUsername()).append(";")
                    .append(item.getRankName()).append(";")
                    .append(item.getPointsReached()).append("/").append(item.getPointsNeeded()).append(";")
                    .append(item.getPercentReached()).append(";")
                    .append(item.getQualify() ? "Si" : "No").append(System.lineSeparator());
        });

        return b.toString();
    }

    public StringBuilder getFounderCsv(List<FoundersView> items, String title, Integer total) {
        StringBuilder b = new StringBuilder();
        b.append(title).append(System.lineSeparator());
        b.append("Cantidad;País").append(System.lineSeparator());
        items.forEach((item) -> {
            b.append(item.getSize()).append(";").append(messageSource.getMessage(item.getBillingCountry(), null, new Locale("es", "ES"))).append(System.lineSeparator());
        });
        b.append(total).append(";").append(messageSource.getMessage("total", null, new Locale("es", "ES"))).append(System.lineSeparator()).append(System.lineSeparator());

        return b;
    }

    public List<FoundersView> getFundersComplete() {
        List<FoundersView> list = ordersViewRepository.findCompleteDiamond();
        list.addAll(ordersViewRepository.findCompleteGolden());
        return list;
    }

    public List<FoundersView> getFundersFirst() {
        List<FoundersView> list = ordersViewRepository.findFirstDiamond();
        list.addAll(ordersViewRepository.findFirstGolden());
        return list;
    }

    public List<FoundersView> getFundersSecond() {
        List<FoundersView> list = ordersViewRepository.findSecondDiamond();
        list.addAll(ordersViewRepository.findSecondGolden());
        return list;
    }
}
