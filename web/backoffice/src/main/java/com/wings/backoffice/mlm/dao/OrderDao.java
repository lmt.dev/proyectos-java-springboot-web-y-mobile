package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas
 */
@Service
public class OrderDao {

    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public List<OrderDto> find(String country, Date from, Date to) {
        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.dao.OrderDto(o.id, o.creationDate, o.activationDate, o.itemId, o.paymentAmount, o.paymentCurrency, o.paymentType, o.billingCountry, o.storeTitle, o.payerEmail, o.orderItem.name)");
        b.append("FROM Order as o ");
        b.append("WHERE 1=1 AND o.itemId != 15 AND o.billingCountry IS NOT NULL and o.deleted = 0"); //15 indicates that is a Free item and excludes orders without country

        if (from != null) {
            b.append(" AND (o.activationDate >= '").append(sdf.format(from)).append("')");
        }

        if (to != null) {
            b.append(" AND (o.activationDate <= '").append(sdf.format(to)).append("')");
        }
        
        if(country != null){
            b.append(" AND (o.billingCountry ='").append(country).append("')");
        }
        b.append(" ORDER BY o.activationDate");
        Query query = em.createQuery(b.toString(), OrderDto.class);
        final List<OrderDto> orders = (List<OrderDto>) query.getResultList();

        return orders;
    }
}
