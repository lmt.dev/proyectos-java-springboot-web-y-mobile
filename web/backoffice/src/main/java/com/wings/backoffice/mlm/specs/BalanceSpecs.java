/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class BalanceSpecs {

    public static Specification<Balance> search(String q, Date from, Date to, String type, String bonusType, Long memberId, String country) {

        return (Root<Balance> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Balance, Member> member = root.join("member");

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(member.get("firstName"), search),
                                builder.like(member.get("lastName"), search),
                                builder.like(member.get("email"), search),
                                builder.like(member.get("username"), search),
                                builder.like(root.get("description"), search),
                                builder.like(builder.concat(builder.concat(member.get("firstName"), " "), member.get("lastName")), search)
                        )
                );
            }

            if (memberId != null) {
                predicates.add(builder.equal(root.get("memberId"), memberId));
            }

            if (from != null) {
                Date withHours = DateUtils.getFirstHourOfDay(from);
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), from));
            }

            if (to != null) {
                Date withHour = DateUtils.getLastHourOfDay(to);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), withHour));
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            if (bonusType != null) {
                predicates.add(builder.equal(root.get("bonusType"), bonusType));
            }

            if (country != null) {
                if(country.equals("other")){
                    //TODO: cambia a listado de paises por config
                    List<String> countries = Arrays.asList("CO","EC","PE","ES");
                    predicates.add(member.get("country").in(countries).not());
                }else{
                    predicates.add(builder.equal(member.get("country"), country));
                }
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
