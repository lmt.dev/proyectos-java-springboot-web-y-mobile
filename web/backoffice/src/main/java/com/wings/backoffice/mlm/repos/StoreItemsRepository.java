/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface StoreItemsRepository extends JpaRepository<StoreItem, Long>, JpaSpecificationExecutor<StoreItem> {

    StoreItem findByName(String name);

    StoreItem findByCode(String code);

    List<StoreItem> findByCategoryAndIdGreaterThanAndSourceLike(Integer category, Long id, String source);

    List<StoreItem> findByAvailableToLikeAndEnabled(String storeItemId, Boolean enabled);

    List<StoreItem> findByCategoryName(String categoryName);

    List<StoreItem> findByCategoryNameAndEnabled(String categoryName, Boolean enabled);

    List<StoreItem> findBySingleAndAvailableToComplex(Boolean single, Boolean availableToComplex);

    List<StoreItem> findBySingleAndAvailableToComplexAndEnabled(Boolean single, Boolean availableToComplex, Boolean enabled);

    @Query(value = "select name from bis_store_items where single = true and category_name = ?1", nativeQuery = true)
    List<String> getItemsNamesByCategory(String category);

}
