/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberMedia;
import com.wings.backoffice.mlm.domain.MemberOld;
import com.wings.backoffice.mlm.enums.MediaStatus;
import com.wings.backoffice.mlm.enums.MediaType;
import com.wings.backoffice.mlm.repos.MembersMediaRepository;
import com.wings.backoffice.mlm.repos.MembersOldRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.utils.ImageUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Service
public class MediaService {

    @Autowired
    private MembersMediaRepository mediaRepository;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private MembersOldRepository membersOldRepository;

    private final Logger log = LoggerFactory.getLogger(MediaService.class);
    private final String salt = "2b654fa00c0d38b6e97a";
    private final String newAvatarsDir = "/var/wings/bo_media/avatars/";

    /**
     * Import avatars from old site
     *
     * @return
     */
    @Deprecated
    public String importAvatars() {

        StringBuilder b = new StringBuilder();
        String oldAvatarsDir = "/var/www/wingsmobile.net/public_html/media/avatars/normal/";

        List<MemberMedia> toSave = new ArrayList<>();

        List<MemberOld> oldies = membersOldRepository.findAll();
        for (MemberOld old : oldies) {

            Long id = old.getMemberId();
            File avatar = new File(oldAvatarsDir + id + ".jpg");
            if (avatar.exists()) {

                //get the new user
                Member m = membersRepository.findByUsername(old.getUsername());
                if (m == null) {
                    log.error("Unable to find member for username " + old.getUsername());
                    b.append("Unable to find member for username ").append(old.getUsername()).append(System.lineSeparator());
                    continue;
                }

                String code = getAvatarCode(m);
                String path = newAvatarsDir + code + ".png";

                MemberMedia already = mediaRepository.findByCode(code);
                if (already != null) {
                    log.error("Avatar already imported, skipping");
                    continue;
                }

                //copy the file
                File newAvatar = new File(path);
                try {
                    FileUtils.copyFile(avatar, newAvatar);
                    ImageUtils.resizeImage(newAvatar, newAvatar);

                    MemberMedia media = new MemberMedia();
                    media.setCode(code);
                    media.setMemberId(m.getId());
                    media.setPath(path);
                    media.setType(MediaType.avatar.name());

                    toSave.add(media);

                } catch (Exception e) {
                    log.error("Error importing avatar for member " + m.getId());
                    b.append("Error importing avatar for member ").append(m.getId()).append(System.lineSeparator());
                }
            }
        }

        mediaRepository.save(toSave);
        log.error("Done, imported " + toSave.size());
        b.append("Done, imported ").append(toSave.size()).append(System.lineSeparator());

        return b.toString();
    }

    /**
     * get user avatar by code
     *
     * @param code
     * @return
     */
    public String getUserAvatar(String code) {
        MemberMedia m = mediaRepository.findByCode(code);
        return m != null ? m.getPath() : "";
    }

    /**
     * Store user avatar
     *
     * @param file
     * @param member
     * @return
     */
    public String storeUserAvatar(MultipartFile file, Member member) {

        String code = getAvatarCode(member);
        String storePath = null;
        try {
            if (file != null && !file.isEmpty()) {

                Path rootLocation = Paths.get(newAvatarsDir);
                Path resolved = rootLocation.resolve(code + ".png");
                String path = resolved.toString();

                Files.copy(file.getInputStream(), resolved, StandardCopyOption.REPLACE_EXISTING);

                File sourceImg = new File(path);
                File destImg = new File(path + "_scaled");
                if (ImageUtils.resizeImage(sourceImg, destImg)) {
                    sourceImg.delete();
                    destImg.renameTo(sourceImg);
                    storePath = sourceImg.getAbsolutePath();
                }

                MemberMedia media = mediaRepository.findByCode(code);
                if (media == null) {
                    media = new MemberMedia();
                    media.setCode(code);
                    media.setMemberId(member.getId());
                    media.setPath(storePath);
                    media.setType(MediaType.avatar.name());
                    mediaRepository.save(media);
                } else {
                    media.setCode(code);
                    media.setPath(storePath);
                    mediaRepository.save(media);
                }
            }
        } catch (IOException e) {
            if (!(e instanceof FileAlreadyExistsException)) {
                log.error("Error saving file", e);
            }
        }

        return code;
    }

    /**
     * get avatar code
     *
     * @param m
     * @return
     */
    public String getAvatarCode(Member m) {
        return DigestUtils.sha256Hex(m.getId() + salt + m.getUsername());
    }

    /**
     * get Dni picture code
     *
     * @param m
     * @return
     */
    public String getDniCode(Member m) {
        return DigestUtils.sha256Hex(m.getId() + salt + m.getDni() + "-dni" + m.getDniType());
    }

    /**
     * get Idt picure Code (identificacion tributaria)
     *
     * @param m
     * @return
     */
    public String getIdtCode(Member m) {
        return DigestUtils.sha256Hex(m.getId() + salt + m.getIdt() + "-idt" + m.getIdtType());
    }

    /**
     * get address picure Code (identificacion tributaria)
     *
     * @param m
     * @return
     */
    public String getAddressCode(Member m) {
        return DigestUtils.sha256Hex(m.getId() + salt + m.getStreet());
    }

    /**
     * get address picure Code (identificacion tributaria)
     *
     * @param bacsId
     * @return
     */
    public String getBacsCode(Long bacsId) {
        return DigestUtils.sha256Hex(bacsId + salt);
    }

    /**
     * Get all medias from member
     *
     * @param m
     * @return
     */
    public List<MemberMedia> getMemberMedias(Member m) {
        return mediaRepository.findByMemberId(m.getId());
    }

    /**
     * create media for dni, address, idt
     *
     * @param member
     * @param filePath
     * @param type
     * @return
     */
    public MemberMedia storeMedia(Member member, String filePath, String type) {

        MemberMedia media = null;
        //if media is dni, idt or address only one
        if (type.equals(MediaType.address.name())) {
            media = mediaRepository.findByMemberIdAndType(member.getId(), MediaType.address.name());
        } else if (type.equals(MediaType.dni.name())) {
            media = mediaRepository.findByMemberIdAndType(member.getId(), MediaType.dni.name());
        } else if (type.equals(MediaType.idt.name())) {
            media = mediaRepository.findByMemberIdAndType(member.getId(), MediaType.idt.name());
        }

        //if not exists, create new one
        if (media == null) {
            media = new MemberMedia();
            media.setMemberId(member.getId());
        }

        media.setStatus(MediaStatus.pending.name());

        if (type.equals(MediaType.address.name())) {
            media.setCode(getAddressCode(member));
            media.setType(MediaType.address.name());
        } else if (type.equals(MediaType.dni.name())) {
            media.setCode(getDniCode(member));
            media.setType(MediaType.dni.name());
        } else if (type.equals(MediaType.idt.name())) {
            media.setCode(getIdtCode(member));
            media.setType(MediaType.idt.name());
        }

        media.setPath(filePath);

        mediaRepository.save(media);

        return media;
    }

    /**
     * create media for bacs
     *
     * @param member
     * @param filePath
     * @param bacsId
     * @return
     */
    public MemberMedia storeMediaForBac(Member member, String filePath, Long bacsId) {
        String code = getBacsCode(bacsId);
        MemberMedia media = mediaRepository.findByCode(code);
        //if not exists, create new one
        if (media == null) {
            media = new MemberMedia();
            media.setMemberId(member.getId());
            media.setCode(code);
            media.setType(MediaType.bacs.name());
        }

        media.setStatus(MediaStatus.pending.name());
        media.setPath(filePath);
        mediaRepository.save(media);
        return media;
    }

    /**
     * get media for dni, address, idt
     *
     * @param member
     * @return
     */
    public List<MemberMedia> getProfileMedias(Member member) {

        List<String> filter = new ArrayList<>();
        filter.add(MediaType.address.name());
        filter.add(MediaType.dni.name());
        filter.add(MediaType.idt.name());

        List<MemberMedia> medias = mediaRepository.findByMemberIdAndTypeIn(member.getId(), filter);

        return medias;
    }

    /**
     *
     * @param mediaStatus
     * @return
     */
    public List<MemberMedia> getMediasByStatus(String mediaStatus) {
        return mediaRepository.findByStatus(mediaStatus);
    }

    /**
     *
     * @param mediaCode
     * @return
     */
    public MemberMedia getMediaByCode(String mediaCode) {
        return mediaRepository.findByCode(mediaCode);
    }

    /**
     *
     * @param mediaCode
     */
    public void deleteMedia(String mediaCode) {
        MemberMedia media = mediaRepository.findByCode(mediaCode);
        if (media != null) {
            mediaRepository.delete(media);
        }
    }

    /**
     *
     * @param memberId
     * @return
     */
    public List<MemberMedia> getMediasUnverifiedForMemberId(Long memberId) {
        List<MemberMedia> medias = mediaRepository.findByMemberIdAndStatus(memberId, MediaStatus.pending.name());
        return medias;
    }

}
