/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class MatchingDAO {

    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    private final SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat sdfTo = new SimpleDateFormat("yyyy-MM-dd");

    public MatchingSums find(String offer, String receive, Date from, Date to) {

        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.dao.MatchingSums(sum(m.quantity) as quantity)");
        b.append("FROM Matching as m, Member as m1, Member as m2 ");
        b.append(" WHERE m1.id = m.offerUserId ");
        b.append(" AND m2.id = m.receiveUserId ");
        //b.append("WHERE 1=1 ");

        if (offer != null) {
            b.append(" AND (");
            b.append(" m1.firstName like '%").append(offer).append("%'");
            b.append(" OR m1.lastName like '%").append(offer).append("%'");
            b.append(" OR m1.email like '%").append(offer).append("%'");
            b.append(" OR m1.username like '%").append(offer).append("%'");
            b.append(" OR concat(m1.firstName,' ',m1.lastName) like '%").append(offer).append("%')");
        }

        if (receive != null) {
            b.append(" AND (");
            b.append(" m2.firstName like '%").append(receive).append("%'");
            b.append(" OR m2.lastName like '%").append(receive).append("%'");
            b.append(" OR m2.email like '%").append(receive).append("%'");
            b.append(" OR m2.username like '%").append(receive).append("%'");
            b.append(" OR concat(m2.firstName,' ',m2.lastName) like '%").append(receive).append("%')");
        }

        if (from != null) {
            b.append(" AND (m.date >= '").append(sdfFrom.format(from)).append("')");
        }

        if (to != null) {
            b.append(" AND (m.date <= '").append(sdfFrom.format(to)).append("')");
        }

        Query query = em.createQuery(b.toString(), MatchingSums.class);
        MatchingSums ret = (MatchingSums) query.getSingleResult();

        return ret;
    }

}
