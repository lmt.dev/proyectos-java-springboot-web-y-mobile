/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.promo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_promo_latam")
public class PromoLatam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "member_id")
    private long memberId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rank_id")
    private long rankId;
    @Size(max = 150)
    @Column(name = "rank_name")
    private String rankName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "points_needed")
    private Double pointsNeeded;
    @Column(name = "points_reached")
    private Double pointsReached;
    @Column(name = "percent_reached")
    private Integer percentReached;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qualify")
    private boolean qualify;
    @Size(max = 200)
    @Column(name = "country")
    private String country;
    @Size(max = 200)
    @Column(name = "username")
    private String username;
    @Size(max = 500)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 500)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 500)
    @Column(name = "email")
    private String email;
    @Column(name = "points_needed2")
    private Double pointsNeeded2;
    @Column(name = "points_reached2")
    private Double pointsReached2;
    @Column(name = "percent_reached2")
    private Integer percentReached2;

    public PromoLatam() {
    }

    public PromoLatam(Long id) {
        this.id = id;
    }

    public PromoLatam(Long id, long memberId, long rankId, boolean qualify) {
        this.id = id;
        this.memberId = memberId;
        this.rankId = rankId;
        this.qualify = qualify;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getRankId() {
        return rankId;
    }

    public void setRankId(long rankId) {
        this.rankId = rankId;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public Double getPointsNeeded() {
        return pointsNeeded;
    }

    public void setPointsNeeded(Double pointsNeeded) {
        this.pointsNeeded = pointsNeeded;
    }

    public Double getPointsReached() {
        return pointsReached;
    }

    public void setPointsReached(Double pointsReached) {
        this.pointsReached = pointsReached;
    }

    public Integer getPercentReached() {
        return percentReached;
    }

    public void setPercentReached(Integer percentReached) {
        this.percentReached = percentReached;
    }

    public boolean getQualify() {
        return qualify;
    }

    public void setQualify(boolean qualify) {
        this.qualify = qualify;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromoLatam)) {
            return false;
        }
        PromoLatam other = (PromoLatam) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.mlm.domain.promo.PromoLatam[ id=" + id + " ]";
    }

    public Double getPointsNeeded2() {
        return pointsNeeded2;
    }

    public void setPointsNeeded2(Double pointsNeeded2) {
        this.pointsNeeded2 = pointsNeeded2;
    }

    public Double getPointsReached2() {
        return pointsReached2;
    }

    public void setPointsReached2(Double pointsReached2) {
        this.pointsReached2 = pointsReached2;
    }

    public Integer getPercentReached2() {
        return percentReached2;
    }

    public void setPercentReached2(Integer percentReached2) {
        this.percentReached2 = percentReached2;
    }

}
