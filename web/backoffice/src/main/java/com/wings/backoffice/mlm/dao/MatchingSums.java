/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.NumberUtils;

/**
 *
 * @author seba
 */
public class MatchingSums {

    private Double quantityPay;
    
    public MatchingSums(Double quantityPay) {
        
        this.quantityPay = quantityPay;
    
        if (this.quantityPay == null) {
            this.quantityPay = 0.0;
        }
    }

    public Double getQuantityPay() {
        return quantityPay;
    }

    public void setQuantityPay(Double quantityPay) {
        this.quantityPay = quantityPay;
    }
    
    public String getQuantityPayText(){
        return NumberUtils.formatMoney(quantityPay);
    }

}
