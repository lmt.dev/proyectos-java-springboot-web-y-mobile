/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.Invitation;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class InvitationsSpecs {

    public static Specification<Invitation> search(String q, String status, String confirmed, String mass,String checked) {

        return (Root<Invitation> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("nombre"), search),
                                builder.like(root.get("apellido"), search),
                                builder.like(root.get("email"), search),
                                builder.like(builder.concat(builder.concat(root.get("nombre"), " "), root.get("apellido")), search)
                        )
                );
            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (confirmed != null) {
                int flag = confirmed.equals("true") ? 1 : 0;
                predicates.add(builder.equal(root.get("confirmed"), flag));
            }
            
            if (mass != null) {
                int flag = mass.equals("true") ? 1 : 0;
                predicates.add(builder.equal(root.get("mass"), flag));
            }
            
            if (checked != null) {
                int flag = checked.equals("true") ? 1 : 0;
                predicates.add(builder.equal(root.get("checked"), flag));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
