/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis.orders;

import com.wings.backoffice.utils.NumberUtils;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_store_items")
public class StoreItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String name;
    private String description;
    private Integer category;
    private Double price;
    private Double rankPoints;
    private Double payPoints;
    private String source;
    private String availableTo;
    private boolean enabled;
    private String image;
    @Column(name = "final_item_id")
    private Long finalItemId;
    private Double priceForComs;
    private String categoryName;
    private String subCategoryName;
    private String shortDescription;
    private String status;
    private String visibility;
    private Long salesCount;
    private Integer rating;
    private boolean single;
    private boolean availableToComplex;
    private String countries;
    private Long stock;
    private boolean installments;
    private Integer installmentsQty;
    private Integer weight;
    private Integer sizeWidth;
    private Integer sizeHeight;
    private Integer sizeLength;
    private Double shippingScore = 1.0;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", nullable = true, updatable = false, insertable = false)
    private final List<StoreItemChild> children = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_item_id", nullable = true, updatable = false, insertable = false)
    private final List<StoreItemI18n> i18n = new LinkedList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "final_item_id", nullable = true, updatable = false, insertable = false)
    private StoreItem finalItem;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_item_id", nullable = true, updatable = false, insertable = false)
    private final List<StoreItemOption> options = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_item_id", nullable = true, updatable = false, insertable = false)
    private final List<StoreItemAttribute> attributes = new LinkedList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_item_id", nullable = true, updatable = false, insertable = false)
    private final List<StoreItemImage> images = new LinkedList<>();

    @Transient
    private final Map<String, StoreItemI18n> i18nMap = new HashMap<>();

    @Transient
    private String upgradePrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getRankPoints() {
        return rankPoints;
    }

    public void setRankPoints(Double rankPoints) {
        this.rankPoints = rankPoints;
    }

    public Double getPayPoints() {
        return payPoints;
    }

    public void setPayPoints(Double payPoints) {
        this.payPoints = payPoints;
    }

    public String getPriceText() {
        if (price == null) {
            return "";
        }
        return NumberUtils.formatMoney(price);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUpgradePrice() {
        return upgradePrice;
    }

    public void setAvailableTo(String availableTo) {
        this.availableTo = availableTo;
    }

    public String getAvailableTo() {
        return availableTo;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getFinalItemId() {
        return finalItemId;
    }

    public void setFinalItemId(Long finalItemId) {
        this.finalItemId = finalItemId;
    }

    public Double getPriceForComs() {
        return priceForComs;
    }

    public void setPriceForComs(Double priceForComs) {
        this.priceForComs = priceForComs;
    }

    public String getLocalPrice(String currency) {
        if (price == null) {
            return "";
        }
        return NumberUtils.convertAndFormatMoney(price, currency);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getSalesCount() {
        return salesCount;
    }

    public void setSalesCount(Long salesCount) {
        this.salesCount = salesCount;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public boolean isAvailableToComplex() {
        return availableToComplex;
    }

    public void setAvailableToComplex(boolean availableToComplex) {
        this.availableToComplex = availableToComplex;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

    public boolean isSingle() {
        return single;
    }

    public List<StoreItemChild> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public List<StoreItemI18n> getI18n() {
        return i18n;
    }

    public StoreItemI18n getI18nForCountry(String country) {
        if (i18nMap.isEmpty() && !i18n.isEmpty()) {
            i18n.forEach((storeItemI18n) -> {
                i18nMap.put(storeItemI18n.getCountry().toLowerCase(), storeItemI18n);
                i18nMap.put(storeItemI18n.getCountry().toUpperCase(), storeItemI18n);
            });
        }
        return i18nMap.get(country);
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public StoreItem getFinalItem() {
        return finalItem;
    }

    public void setFinalItem(StoreItem finalItem) {
        this.finalItem = finalItem;
    }

    public List<StoreItemOption> getOptions() {
        return options;
    }

    public List<StoreItemAttribute> getAttributes() {
        return attributes;
    }

    public boolean isInstallments() {
        return installments;
    }

    public void setInstallments(boolean installments) {
        this.installments = installments;
    }

    public Integer getInstallmentsQty() {
        return installmentsQty;
    }

    public void setInstallmentsQty(Integer installmentsQty) {
        this.installmentsQty = installmentsQty;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getSizeHeight() {
        return sizeHeight;
    }

    public void setSizeHeight(Integer sizeHeight) {
        this.sizeHeight = sizeHeight;
    }

    public Integer getSizeLength() {
        return sizeLength;
    }

    public void setSizeLength(Integer sizeLength) {
        this.sizeLength = sizeLength;
    }

    public Integer getSizeWidth() {
        return sizeWidth;
    }

    public void setSizeWidth(Integer sizeWidth) {
        this.sizeWidth = sizeWidth;
    }

    public List<StoreItemImage> getImages() {
        return images;
    }

    public Double getShippingScore() {
        return shippingScore;
    }

    public void setShippingScore(Double shippingScore) {
        this.shippingScore = shippingScore;
    }

}
