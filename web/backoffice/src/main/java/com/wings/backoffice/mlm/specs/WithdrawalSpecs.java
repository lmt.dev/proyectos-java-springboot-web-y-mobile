/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Withdrawal;
import com.wings.backoffice.mlm.domain.bis.Differential;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class WithdrawalSpecs {

    public static Specification<Withdrawal> search(String q, String status, Date from, Date to, Long memberId, String country) {

        return (Root<Withdrawal> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Withdrawal, Member> member = root.join("member");
            List<Predicate> predicates = new ArrayList<>();

            if (q != null) {

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(member.get("firstName"), search),
                                builder.like(member.get("lastName"), search),
                                builder.like(member.get("email"), search),
                                builder.like(member.get("username"), search),
                                builder.like(builder.concat(builder.concat(member.get("firstName"), " "), member.get("lastName")), search)
                        )
                );

            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (from != null) {
                Date withHours = DateUtils.getFirstHourOfDay(from);
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("requestDate"), withHours));
            }

            if (to != null) {
                Date withHour = DateUtils.getLastHourOfDay(to);
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("requestDate"), withHour));
            }

            if (memberId != null) {
                predicates.add(builder.equal(root.get("memberId"), memberId));
            }

            if (country != null) {
                if (country.equals("other")) {
                    //TODO: cambiar a listado de paises por config
                    List<String> countries = Arrays.asList("CO", "EC", "PE", "ES");
                    predicates.add(member.get("country").in(countries).not());
                } else {
                    predicates.add(builder.equal(member.get("country"), country));
                }
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
