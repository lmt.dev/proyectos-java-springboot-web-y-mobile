/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.CountryOpen;
import com.wings.backoffice.mlm.domain.CountryProperties;
import com.wings.backoffice.mlm.domain.bis.orders.FoundersView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CountryOpenService {

    @Autowired
    private CountriesService countriesService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MembersService membersService;

    /**
     *
     * @param country
     * @return
     */
    public CountryOpen getCountryOpen(String country) {

        CountryOpen open = new CountryOpen();
        CountryProperties props = countriesService.getPropertiesByCode(country);
        Map<String, Long> map = getMap(country);

        open.setCountry(country);
        open.setGolden(map.get("Golden"));
        open.setDiamond(map.get("Diamond"));

        Long cant = membersService.countPaidMembersByCountry(country);
        open.setMembersCount(cant);
        open.setLevel(props != null && props.getPopulationLevel() != null ? props.getPopulationLevel() : 0);
        return open;
    }

    /**
     *
     */
    public void getCountryLevelInfo() {

        List<String> enabled = countriesService.getEnabledCountries();

        for (String country : enabled) {
            CountryProperties props = countriesService.getPropertiesByCode(country);
            CountryOpen open = new CountryOpen();
            Map<String, Long> map = getMap(country);

            open.setCountry(country);
            open.setGolden(map.get("Golden"));
            open.setDiamond(map.get("Diamond"));

            Long cant = membersService.countPaidMembersByCountry(country);
            open.setMembersCount(cant);
            open.setLevel(props.getPopulationLevel() != null ? props.getPopulationLevel() : 0);

            System.out.println(country + " " + open);

        }

        //paso 1
        //primero check 200 golden y 100 founder
        //paso 2
        //luego check cantidad de afiliados de acuerdo al popLevel
        //1k,2k,3k                        
        //paso 3
        //luego check cantidad de afiliados de acuerdo al popLevel
        //3k,5k,7k
    }

    private Map<String, Long> getMap(String country) {
        List<FoundersView> list = reportService.getFundersComplete();
        Map<String, Long> ret = new HashMap<>();
        ret.put("Golden", 0l);
        ret.put("Diamond", 0l);

        list.forEach((foundersView) -> {
            if (foundersView.getBillingCountry().equals(country)) {
                ret.put(foundersView.getFounder(), foundersView.getSize());
            }
        });
        return ret;
    }
}
