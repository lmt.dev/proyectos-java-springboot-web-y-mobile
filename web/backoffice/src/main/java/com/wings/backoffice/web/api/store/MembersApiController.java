/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api.store;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.services.ApiAuthService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.services.MembersService;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
@RequestMapping(value = "api/v1/")
public class MembersApiController {

    @Autowired
    private MembersService membersService;
    @Autowired
    private MediaService mediaService;
    @Autowired
    private ApiAuthService apiAuthService;
    @Value("${wings.site.url}")
    private String url;

    /**
     * @param request
     * @param username
     * @return
     */
    @RequestMapping(value = "members/{username}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> getMemberInfo(HttpServletRequest request, @PathVariable("username") String username) {

        if (!apiAuthService.authRequest(request)) {
            return ResponseEntity.badRequest().build();
        }

        Map<String, String> ret = new HashMap<>();
        Member m = membersService.findByUsername(username);
        if (m == null) {
            ret.put("found", "0");
        } else {

            m.setAvatar(url + "/open/media/" + mediaService.getAvatarCode(m));

            ret.put("found", "1");
            ret.put("avatar", m.getAvatar());
        }
        return ResponseEntity.ok(ret);
    }

}
