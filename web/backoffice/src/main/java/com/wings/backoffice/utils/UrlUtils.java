/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class UrlUtils {

    public static String cleanQueryString(String queryString) {

        String cleaned = "";

        if (queryString != null && !queryString.isEmpty()) {
            List<String> list = new ArrayList<>();
            String[] qarray = queryString.split("&");
            for (String paramValue : qarray) {
                //added by pagination
                if (paramValue.contains("page") || paramValue.contains("size")) {
                    continue;
                }

                if (!list.contains(paramValue)) {
                    list.add(paramValue);
                }
            }
            cleaned = list.stream().map((string) -> "&" + string).reduce(cleaned, String::concat);
            //cleaned = cleaned.substring(0, cleaned.length() - 1);
        }
        return cleaned;
    }
}
