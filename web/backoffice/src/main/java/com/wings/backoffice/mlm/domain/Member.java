/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import com.wings.backoffice.config.auth.WingsGrantedAuthority;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.utils.DateUtils;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_members")
public class Member implements UserDetails, Serializable {

    private static final long serialVersionUID = 562500937725816920L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String companyName;

    private String street;
    private String city;
    private String state;
    private String country;
    private String postal;
    private String phone;
    private Long sponsorId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;
    private String status;
    private Long rankId;
    private Long initialRankId;
    private Long calculatedRankId;
    private String dni;
    private String dniType;
    private String dniValidated;
    private boolean founder;
    @Temporal(TemporalType.TIMESTAMP)
    private Date founderDate;
    private boolean colorSelected;
    private String colors;
    private Long indirectCount;
    private Long orderId;
    private boolean specialOrder;
    private boolean mustUpdate;
    private String source;
    private Boolean loginEnabled;
    private Boolean deleted;
    private Long speedBonusId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date speedBonusEndDate;

    private boolean contractSigned;
    @Temporal(TemporalType.TIMESTAMP)
    private Date contractSignedDate;
    private String contractUrl;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private String companyDni;
    private String companyDniType;
    private boolean isCompany;
    private String marketingUrl;

    private String idt;
    private String idtType;
    private boolean paymentsEnabled;

    private String bwnWallet;
    private Integer bwnClaim;
    private boolean bwnSent;
    private boolean excludePromo;

    @Transient
    private Collection<WingsGrantedAuthority> grantedAuthorities;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "sponsorId", nullable = true, updatable = false, insertable = false)
    @Where(clause = "deleted = 0")
    private List<Member> children = new LinkedList<Member>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sponsorId", updatable = false, insertable = false, nullable = true)
    private Member sponsor;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rankId", updatable = false, insertable = false, nullable = true)
    private RankBis rank;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "initialRankId", updatable = false, insertable = false, nullable = true)
    private RankBis initialRank;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calculatedRankId", updatable = false, insertable = false, nullable = true)
    private RankBis calculatedRank;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", updatable = false, insertable = false, nullable = true)
    private Order order;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "speedBonusId", updatable = false, insertable = false, nullable = true)
    private SpeedBonus speedBonus;

    @Transient
    private Map<Long, Long> differential;

    @Transient
    private String avatar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(Long sponsorId) {
        this.sponsorId = sponsorId;
    }

    public Long getRankId() {
        return rankId;
    }

    public void setRankId(Long rankId) {
        this.rankId = rankId;
    }

    public List<Member> getChildren() {
        return children;
    }

    public void setChildren(List<Member> children) {
        this.children = children;
    }

    public RankBis getRank() {
        return rank;
    }

    public void setRank(RankBis rank) {
        this.rank = rank;
    }

    public Map<Long, Long> getDifferential() {
        return differential;
    }

    public void setDifferential(Map<Long, Long> differential) {
        this.differential = differential;
    }

    public void setFounder(boolean founder) {
        this.founder = founder;
    }

    public boolean isFounder() {
        return founder;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDniType() {
        return dniType;
    }

    public void setDniType(String dniType) {
        this.dniType = dniType;
    }

    public String getDniValidated() {
        return dniValidated;
    }

    public void setDniValidated(String dniValidated) {
        this.dniValidated = dniValidated;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setGrantedAuthorities(Collection<WingsGrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getNameText() {
        return firstName + " " + lastName + " (" + username + ") (#" + id + ")";
    }

    public Member getSponsor() {
        return sponsor;
    }

    public void setSponsor(Member sponsor) {
        this.sponsor = sponsor;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public boolean getColorSelected() {
        return colorSelected;
    }

    public void setColorSelected(boolean colorSelected) {
        this.colorSelected = colorSelected;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public boolean isColorSelected() {
        return colorSelected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getInitialRankId() {
        return initialRankId;
    }

    public void setInitialRankId(Long initialRankId) {
        this.initialRankId = initialRankId;
    }

    public RankBis getInitialRank() {
        return initialRank;
    }

    public void setInitialRank(RankBis initialRank) {
        this.initialRank = initialRank;
    }

    public Long getIndirectCount() {
        return indirectCount;
    }

    public void setIndirectCount(Long indirectCount) {
        this.indirectCount = indirectCount;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public boolean isSpecialOrder() {
        return specialOrder;
    }

    public void setSpecialOrder(boolean specialOrder) {
        this.specialOrder = specialOrder;
    }

    public boolean isMustUpdate() {
        return mustUpdate;
    }

    public void setMustUpdate(boolean mustUpdate) {
        this.mustUpdate = mustUpdate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Boolean getLoginEnabled() {
        return loginEnabled;
    }

    public void setLoginEnabled(Boolean loginEnabled) {
        this.loginEnabled = loginEnabled;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public SpeedBonus getSpeedBonus() {
        return speedBonus;
    }

    public void setSpeedBonus(SpeedBonus speedBonus) {
        this.speedBonus = speedBonus;
    }

    public Long getSpeedBonusId() {
        return speedBonusId;
    }

    public void setSpeedBonusId(Long speedBonusId) {
        this.speedBonusId = speedBonusId;
    }

    public RankBis getCalculatedRank() {
        return calculatedRank;
    }

    public void setCalculatedRank(RankBis calculatedRank) {
        this.calculatedRank = calculatedRank;
    }

    public Long getCalculatedRankId() {
        return calculatedRankId;
    }

    public void setCalculatedRankId(Long calculatedRankId) {
        this.calculatedRankId = calculatedRankId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getFounderDate() {
        return founderDate;
    }

    public void setFounderDate(Date founderDate) {
        this.founderDate = founderDate;
    }

    public void setSpeedBonusEndDate(Date speedBonusEndDate) {
        this.speedBonusEndDate = speedBonusEndDate;
    }

    public Date getSpeedBonusEndDate() {
        return speedBonusEndDate;
    }

    public boolean isInSpeedBonus() {
        if (speedBonusEndDate != null) {
            return DateUtils.getDiffDays(speedBonusEndDate) <= 0;
        }
        return false;
    }

    public boolean getContractSigned() {
        return contractSigned;
    }

    public void setContractSigned(boolean contractSigned) {
        this.contractSigned = contractSigned;
    }

    public Date getContractSignedDate() {
        return contractSignedDate;
    }

    public void setContractSignedDate(Date contractSignedDate) {
        this.contractSignedDate = contractSignedDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCompanyDni() {
        return companyDni;
    }

    public void setCompanyDni(String companyDni) {
        this.companyDni = companyDni;
    }

    public String getCompanyDniType() {
        return companyDniType;
    }

    public void setCompanyDniType(String companyDniType) {
        this.companyDniType = companyDniType;
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public void setContractUrl(String contractUrl) {
        this.contractUrl = contractUrl;
    }

    public boolean getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean isCompany) {
        this.isCompany = isCompany;
    }

    public String getMarketingUrl() {
        return marketingUrl;
    }

    public void setMarketingUrl(String marketingUrl) {
        this.marketingUrl = marketingUrl;
    }

    public String getIdt() {
        return idt;
    }

    public void setIdt(String idt) {
        this.idt = idt;
    }

    public String getIdtType() {
        return idtType;
    }

    public void setIdtType(String idtType) {
        this.idtType = idtType;
    }

    public boolean isPaymentsEnabled() {
        return paymentsEnabled;
    }

    public void setPaymentsEnabled(boolean paymentsEnabled) {
        this.paymentsEnabled = paymentsEnabled;
    }

    public Integer getBwnClaim() {
        return bwnClaim;
    }

    public void setBwnClaim(Integer bwnClaim) {
        this.bwnClaim = bwnClaim;
    }

    public String getBwnWallet() {
        return bwnWallet;
    }

    public void setBwnWallet(String bwnWallet) {
        this.bwnWallet = bwnWallet;
    }

    public boolean isBwnSent() {
        return bwnSent;
    }

    public void setBwnSent(boolean bwnSent) {
        this.bwnSent = bwnSent;
    }

    public boolean isExcludePromo() {
        return excludePromo;
    }

    public void setExcludePromo(boolean excludePromo) {
        this.excludePromo = excludePromo;
    }
}
