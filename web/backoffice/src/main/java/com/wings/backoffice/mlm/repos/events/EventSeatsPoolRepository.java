/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventSeatsPool;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface EventSeatsPoolRepository extends JpaRepository<EventSeatsPool, Long> {

    List<EventSeatsPool> findByEventId(Long eventId);

    List<EventSeatsPool> findByEventIdAndPrivatePool(Long eventId, Boolean privatePool);
    
    
}
