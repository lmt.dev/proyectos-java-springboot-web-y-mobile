/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author seba
 */
public class MapUtils {

    private final static Logger log = LoggerFactory.getLogger(MapUtils.class);

    public static List<FormElement> getFormElementsFiltered(Object o, String... fieldNames) {
        return getFormElementsFiltered(o, Arrays.asList(fieldNames));
    }

    public static List<FormElement> getFormElementsFiltered(Object o, List<String> fieldNames) {

        List<FormElement> list = new LinkedList<>();
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            try {

                if (fieldNames != null && !fieldNames.contains(field.getName())) {
                    continue;
                }

                if (field.get(o) instanceof Collection) {
                    continue;
                }

                //log.error(field.getName());
                list.add(new FormElement(field.getName(), field.get(o), field.getType().toString()));

            } catch (IllegalAccessException | IllegalArgumentException e) {
            }
        }
        return list;
    }

    public static List<FormElement> getFormElements(Object o) {

        List<FormElement> list = new LinkedList<>();
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            try {
                if (field.get(o) instanceof Collection) {
                    continue;
                }

                //log.error(field.getName());
                list.add(new FormElement(field.getName(), field.get(o), field.getType().toString()));

            } catch (IllegalAccessException | IllegalArgumentException e) {
            }
        }
        return list;
    }

    public static class FormElement<T> {

        private String name;
        private T value;
        private String type;

        public FormElement(String name, T value, String type) {
            this.name = name;
            this.value = value;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    /**
     * Given a string provided by map.toString() returns the map converted - ej:
     * {487=78.0, 233=65.0, 234=2.0}
     *
     * @param string
     * @return
     */
    public static Map<Long, Double> stringToMap(String string) {
        Map<Long, Double> ret = new HashMap<>();
        if (string == null) {
            return ret;
        }
        final String cleaned = string.substring(1, string.length() - 1);
        String[] arr = cleaned.split(",");
        for (String item : arr) {
            String[] arr1 = item.split("=");
            Long key = Long.parseLong(arr1[0].trim());
            Double value = Double.parseDouble(arr1[1].trim());
            ret.put(key, value);
        }
        return ret;
    }

    /**
     * Get the String difference bewteen 2 string formated as resutl of
     * ReflectionToStringBuilder(object).toString()
     *
     * @param one
     * @param two
     * @return
     */
    public static String getDiff(String one, String two) {

        one = one.substring(one.indexOf("[id"), one.length() - 1);
        two = two.substring(two.indexOf("[id"), two.length() - 1);

        String[] first = one.split(",");
        String[] second = two.split(",");

        List<String> before = new ArrayList<>();
        List<String> after = new ArrayList<>();

        for (int i = 0; i < second.length; i++) {
            try {
                String f = first[i];
                String s = second[i];
                if (!s.equals(f)) {
                    before.add(f);
                    after.add(s);
                }
            } catch (Exception e) {
            }
        }

        if (before.isEmpty() && after.isEmpty()) {
            return "";
        }

        return "Antes: " + before.toString() + System.lineSeparator() + "Despues:" + after.toString();
    }

}
