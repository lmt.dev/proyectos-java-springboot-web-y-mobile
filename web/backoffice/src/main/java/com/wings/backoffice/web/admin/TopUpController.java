/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Topup;
import com.wings.backoffice.mlm.domain.TopupProvider;
import com.wings.backoffice.services.SumaProviderService;
import com.wings.backoffice.services.TopUpsService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class TopUpController {

    @Autowired
    private TopUpsService topUpsService;
    @Autowired
    private SumaProviderService sumaProviderService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/topups"}, method = {RequestMethod.GET})
    public String getIndex(Model model, HttpServletRequest request) {
        return "admin/store/topups";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/topups/internal"}, method = {RequestMethod.GET})
    public String getItemsLocal(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Page<Topup> items = topUpsService.searchItems(page, size, q, null, f, t);
        PageWrapper<Topup> pageWrapper = new PageWrapper<>(items, "admin/store/topups/internal");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/store/topups_local";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/topups/external"}, method = {RequestMethod.GET})
    public String getItemsProvider(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Page<TopupProvider> items = topUpsService.searchProviderItems(page, size, q, null, f, t);
        PageWrapper<TopupProvider> pageWrapper = new PageWrapper<>(items, "admin/store/topups/external");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/store/topups_provider";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/topups/external/files"}, method = {RequestMethod.GET})
    public String getItemsProviderFiles(Model model, HttpServletRequest request) {

        /*String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Page<TopupProvider> items = topUpsService.searchProviderItems(page, size, q, null, f, t);
        PageWrapper<TopupProvider> pageWrapper = new PageWrapper<>(items, "admin/store/topups/external");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("from", from);
        model.addAttribute("to", to);*/
        model.addAttribute("files", topUpsService.getProviderFiles("suma"));

        return "admin/store/topups_provider_files";
    }

    @RequestMapping(value = {"admin/store/topups/external/files/{file}"}, method = {RequestMethod.GET})
    public ResponseEntity<Resource> getDownloadFile(@PathVariable("file") String fileName) throws IOException {

        if (fileName == null || fileName.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        File file = topUpsService.getFile(fileName + ".csv");
        if (!file.exists()) {
            return ResponseEntity.badRequest().build();
        }

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        HttpHeaders headers = new HttpHeaders();

        //para descargar, sin esto abre en el browser
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName + ".csv");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.TEXT_PLAIN)
                .body(resource);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/addtopup"}, method = {RequestMethod.GET})
    public String getTopup(Model model, HttpServletRequest request) {
        return "admin/store/topupadd";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/addtopup"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postTopup(Model model, HttpServletRequest request) {

        String msisdn = request.getParameter("msisdn");
        String amount = request.getParameter("amount");

        //TODO: validar valores        
        Topup topup = new Topup();
        topup.setAmount(Double.parseDouble(amount));
        topup.setCreationDate(new Date());
        topup.setCurrency("COP");
        topup.setModIp(AuthUtils.getIp(request));
        topup.setModUser(AuthUtils.getAdminUser().getUsername());
        topup.setModStatus("created from admin page");
        topup.setMsisdn(Long.parseLong(msisdn));
        topup.setProvider("suma");
        topup.setStatus("created");

        topUpsService.addTopUp(topup);

        return ResponseEntity.ok(topup.getStatus());
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/store/topupprovider"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postDownloadProvider(Model model, HttpServletRequest request) {
        //TODO:remove this once it's called once a day
        try {
            sumaProviderService.downloadFile();
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok("Ok");
    }

}
