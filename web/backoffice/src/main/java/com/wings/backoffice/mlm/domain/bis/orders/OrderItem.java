/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis.orders;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_order_items")
public class OrderItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long orderId;
    private String name;
    private Long storeItemOptionId;
    private boolean shipped;
    private String storeItemEan;
    private String complexName;
    private String simpleName;
    private Long complexId;
    private Long simpleId;
    private Long itemOptionId;
    private String itemOptionSelected;
    private int quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getStoreItemOptionId() {
        return storeItemOptionId;
    }

    public void setStoreItemOptionId(Long storeItemOptionId) {
        this.storeItemOptionId = storeItemOptionId;
    }

    public boolean isShipped() {
        return shipped;
    }

    public void setShipped(boolean shipped) {
        this.shipped = shipped;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStoreItemEan(String storeItemEan) {
        this.storeItemEan = storeItemEan;
    }

    public String getStoreItemEan() {
        return storeItemEan;
    }

    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public Long getComplexId() {
        return complexId;
    }

    public void setComplexId(Long complexId) {
        this.complexId = complexId;
    }

    public Long getSimpleId() {
        return simpleId;
    }

    public void setSimpleId(Long simpleId) {
        this.simpleId = simpleId;
    }

    public Long getItemOptionId() {
        return itemOptionId;
    }

    public void setItemOptionId(Long itemOptionId) {
        this.itemOptionId = itemOptionId;
    }

    public String getItemOptionSelected() {
        return itemOptionSelected;
    }

    public void setItemOptionSelected(String itemOptionSelected) {
        this.itemOptionSelected = itemOptionSelected;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
