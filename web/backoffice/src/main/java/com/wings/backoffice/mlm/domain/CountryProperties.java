/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_country_properties")
public class CountryProperties implements Serializable {

    private static long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "code")
    private String code;
    @Size(max = 50)
    @Column(name = "currency")
    private String currency;
    @Size(max = 50)
    @Column(name = "default_lang")
    private String defaultLang;
    @Size(max = 50)
    @Column(name = "default_locale")
    private String defaultLocale;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "exchange_rate")
    private Double exchangeRate;
    @Size(max = 250)
    @Column(name = "flag")
    private String flag;
    @Column(name = "sort_order")
    private Integer sortOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "available_store")
    private boolean availableStore;
    @Basic(optional = false)
    @NotNull
    @Column(name = "available_backoffice")
    private boolean availableBackoffice;
    @Column(name = "iva1")
    private Double iva1;
    @Column(name = "iva2")
    private Double iva2;
    @Column(name = "iva3")
    private Double iva3;
    @Column(name = "shipping1")
    private Double shipping1;
    @Column(name = "shipping2")
    private Double shipping2;
    @Column(name = "shipping3")
    private Double shipping3;
    @Column(name = "shipping4")
    private Double shipping4;
    @Column(name = "population_level")
    private Integer populationLevel;
    @Size(max = 500)
    @Column(name = "dnis_list")
    private String dnisList;
    @Size(max = 500)
    @Column(name = "idts_list")
    private String idtsList;
    @Size(max = 500)
    @Column(name = "custom1")
    private String custom1;
    @Size(max = 500)
    @Column(name = "custom2")
    private String custom2;
    @Size(max = 500)
    @Column(name = "custom3")
    private String custom3;
    @Size(max = 500)
    @Column(name = "custom4")
    private String custom4;
    @Size(max = 500)
    @Column(name = "custom5")
    private String custom5;
    @Size(max = 150)
    @Column(name = "country_level")
    private String countryLevel;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public boolean isAvailableStore() {
        return availableStore;
    }

    public void setAvailableStore(boolean availableStore) {
        this.availableStore = availableStore;
    }

    public boolean isAvailableBackoffice() {
        return availableBackoffice;
    }

    public void setAvailableBackoffice(boolean availableBackoffice) {
        this.availableBackoffice = availableBackoffice;
    }

    public Double getIva1() {
        return iva1;
    }

    public void setIva1(Double iva1) {
        this.iva1 = iva1;
    }

    public Double getIva2() {
        return iva2;
    }

    public void setIva2(Double iva2) {
        this.iva2 = iva2;
    }

    public Double getIva3() {
        return iva3;
    }

    public void setIva3(Double iva3) {
        this.iva3 = iva3;
    }

    public Double getShipping1() {
        return shipping1;
    }

    public void setShipping1(Double shipping1) {
        this.shipping1 = shipping1;
    }

    public Double getShipping2() {
        return shipping2;
    }

    public void setShipping2(Double shipping2) {
        this.shipping2 = shipping2;
    }

    public Double getShipping3() {
        return shipping3;
    }

    public void setShipping3(Double shipping3) {
        this.shipping3 = shipping3;
    }

    public Double getShipping4() {
        return shipping4;
    }

    public void setShipping4(Double shipping4) {
        this.shipping4 = shipping4;
    }

    public String getDnisList() {
        return dnisList;
    }

    public void setDnisList(String dnisList) {
        this.dnisList = dnisList;
    }

    public String getIdtsList() {
        return idtsList;
    }

    public void setIdtsList(String idtsList) {
        this.idtsList = idtsList;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom4() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4 = custom4;
    }

    public String getCustom5() {
        return custom5;
    }

    public void setCustom5(String custom5) {
        this.custom5 = custom5;
    }

    public Integer getPopulationLevel() {
        return populationLevel;
    }

    public void setPopulationLevel(Integer populationLevel) {
        this.populationLevel = populationLevel;
    }

    public String getCountryLevel() {
        return countryLevel;
    }

    public void setCountryLevel(String countryLevel) {
        this.countryLevel = countryLevel;
    }

}
