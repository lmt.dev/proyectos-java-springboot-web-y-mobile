/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "wingstore_comments")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long commentID;
    @Column(name = "comment_post_ID")
    private long commentpostID;
    private String commentAuthor;
    private String commentAuthorEmail;
    private String commentAuthorUrl;
    @Column(name = "comment_author_IP")
    private String commentauthorIP;
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDateGmt;
    private String commentContent;
    private int commentKarma;
    private String commentApproved;
    private String commentAgent;
    private String commentType;
    private long commentParent;
    private long userId;

    public Comment() {
    }

    public Comment(Long commentID) {
        this.commentID = commentID;
    }

    public Comment(Long commentID, long commentpostID, String commentAuthor, String commentAuthorEmail, String commentAuthorUrl, String commentauthorIP, Date commentDate, Date commentDateGmt, String commentContent, int commentKarma, String commentApproved, String commentAgent, String commentType, long commentParent, long userId) {
        this.commentID = commentID;
        this.commentpostID = commentpostID;
        this.commentAuthor = commentAuthor;
        this.commentAuthorEmail = commentAuthorEmail;
        this.commentAuthorUrl = commentAuthorUrl;
        this.commentauthorIP = commentauthorIP;
        this.commentDate = commentDate;
        this.commentDateGmt = commentDateGmt;
        this.commentContent = commentContent;
        this.commentKarma = commentKarma;
        this.commentApproved = commentApproved;
        this.commentAgent = commentAgent;
        this.commentType = commentType;
        this.commentParent = commentParent;
        this.userId = userId;
    }

    public Long getCommentID() {
        return commentID;
    }

    public void setCommentID(Long commentID) {
        this.commentID = commentID;
    }

    public long getCommentpostID() {
        return commentpostID;
    }

    public void setCommentpostID(long commentpostID) {
        this.commentpostID = commentpostID;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    public void setCommentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
    }

    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    public void setCommentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
    }

    public String getCommentauthorIP() {
        return commentauthorIP;
    }

    public void setCommentauthorIP(String commentauthorIP) {
        this.commentauthorIP = commentauthorIP;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Date getCommentDateGmt() {
        return commentDateGmt;
    }

    public void setCommentDateGmt(Date commentDateGmt) {
        this.commentDateGmt = commentDateGmt;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public int getCommentKarma() {
        return commentKarma;
    }

    public void setCommentKarma(int commentKarma) {
        this.commentKarma = commentKarma;
    }

    public String getCommentApproved() {
        return commentApproved;
    }

    public void setCommentApproved(String commentApproved) {
        this.commentApproved = commentApproved;
    }

    public String getCommentAgent() {
        return commentAgent;
    }

    public void setCommentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public long getCommentParent() {
        return commentParent;
    }

    public void setCommentParent(long commentParent) {
        this.commentParent = commentParent;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentID != null ? commentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) object;
        if ((this.commentID == null && other.commentID != null) || (this.commentID != null && !this.commentID.equals(other.commentID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.store.domain.WingstoreComments[ commentID=" + commentID + " ]";
    }
    
}
