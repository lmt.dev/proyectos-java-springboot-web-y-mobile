/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Topup;
import com.wings.backoffice.mlm.domain.TopupProvider;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class TopupSpecs {

    public static Specification<Topup> searchTopup(String q, String provider, Date from, Date to) {

        return (Root<Topup> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                Long msisdn = null;
                try {
                    msisdn = Long.valueOf(q);
                    predicates.add(builder.equal(root.get("msisdn"), msisdn));
                } catch (Exception e) {
                }

                if (msisdn == null) {
                    String search = "%" + q + "%";
                    predicates.add(
                            builder.or(
                                    builder.like(root.get("modUser"), search),
                                    builder.like(root.get("modStatus"), search),
                                    builder.like(root.get("iccid"), search)
                            )
                    );
                }
            }

            if (provider != null) {
                predicates.add(builder.equal(root.get("provider"), provider));
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), DateUtils.getFirstHourOfDay(from)));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), DateUtils.getLastHourOfDay(to)));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    public static Specification<TopupProvider> searchTopupProvider(String q, String provider, Date from, Date to) {

        return (Root<TopupProvider> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("reference"), search),
                                builder.like(root.get("iccid"), search),
                                builder.like(root.get("msisdn"), search)
                        )
                );
            }

            if (provider != null) {
                predicates.add(builder.equal(root.get("provider"), provider));
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), DateUtils.getFirstHourOfDay(from)));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), DateUtils.getLastHourOfDay(to)));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
