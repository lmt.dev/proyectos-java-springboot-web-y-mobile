/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.MemberSim;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class MemberSimSpecs {

    /**
     * Search members
     *
     * @param memberId
     * @param q
     * @return
     */
    public static Specification<MemberSim> memberSimsSearch(Long memberId, String q) {

        return (Root<MemberSim> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (memberId != null) {
                predicates.add(
                        builder.or(
                                builder.equal(root.get("memberId"), memberId),
                                builder.equal(root.get("transferMemberId"), memberId)
                        )
                );
            }

            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("iccid"), search),
                                builder.like(root.get("transferMemberName"), search)
                        )
                );
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
