/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_countries")
public class Country {

    @Id
    private String code;
    private boolean isEu;
    private String nameEs;
    private boolean enabled;

    public Country() {
    }

    public Country(String code, boolean isEu, String nameEs, boolean enabled) {
        this.code = code;
        this.isEu = isEu;
        this.nameEs = nameEs;
        this.enabled = enabled;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameEs) {
        this.nameEs = nameEs;
    }

    public boolean getIsEu() {
        return isEu;
    }

    public void setIsEu(boolean isEu) {
        this.isEu = isEu;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
