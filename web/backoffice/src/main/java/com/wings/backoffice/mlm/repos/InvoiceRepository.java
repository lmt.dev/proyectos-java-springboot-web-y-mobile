/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long>, JpaSpecificationExecutor<Invoice> {

    Invoice findByOrderId(Long orderId);

    Invoice findByInvoiceNumberAndClientCountry(String invoiceNumber, String coutnry);

    Invoice findByToken(String token);

    Invoice findByTypeId(Long typeId);

    Invoice findByTypeAndTypeId(String type, Long typeId);

    List<Invoice> findByType(String typeId);

    @Query(value = "select IFNULL(MAX(type_number), 0) from bis_invoices where type = ?1 and year = ?2", nativeQuery = true)
    long getMaxTypeNumber(String type, int year);

    @Query(value = "select IFNULL(MAX(type_number), 0) from bis_invoices where type in (?1) and client_country = ?2", nativeQuery = true)
    long getMaxTypeNumber(List<String> types, String country);

}
