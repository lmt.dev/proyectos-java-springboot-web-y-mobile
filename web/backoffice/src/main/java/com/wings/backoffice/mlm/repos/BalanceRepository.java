/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.Balance;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface BalanceRepository extends JpaRepository<Balance, Long>, JpaSpecificationExecutor<Balance> {

    Balance findByControl(String control);

    Balance findByMemberIdAndType(Long memberId, String type);

    Balance findByOrderIdAndReferenceId(Long orderId, Long referenceId);

    @Transactional
    Long deleteByType(String type);

    @Transactional
    Long deleteByTypeAndMemberId(String type, Long memberId);

    @Transactional
    Long deleteByBonusType(String bonusType);

    List<Balance> findByReferenceId(Long referenceId);

    List<Balance> findByStatusAndType(String status, String type);

    List<Balance> findByTypeAndReferenceId(String type, Long referenceId);

    Page<Balance> findByMemberIdAndBonusType(Long memberId, String bonusType, Pageable page);

}
