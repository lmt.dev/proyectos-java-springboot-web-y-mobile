/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;


import com.wings.backoffice.mlm.domain.Customer;
import com.wings.backoffice.mlm.domain.OrderDetailOld;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class OrderDetailSpecs {

    public static Specification<OrderDetailOld> search(String storeOwner) {

        return (Root<OrderDetailOld> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            
            Join<OrderDetailOld,Customer> customer = root.join("customer");
//            List<Predicate> predicates = new ArrayList<>();
//            List<Predicate> orPredicates = new ArrayList<>();
//            if (storeOwner != null) {
//                String search = "%" + q + "%";
//                predicates.add(
//                        builder.or(
//                                builder.like(customer.get("name"), search),
//                                builder.like(customer.get("lastname"), search),
//                                builder.like(customer.get("email"), search),
//                                builder.like(customer.get("idValue"), search),
//                                builder.like(
//                                        builder.concat(
//                                                builder.concat(customer.get("name"), " "),
//                                                customer.get("lastname")),
//                                         search)
//                        )
//                );
//            }
            /*if (status != null) {
                String[] statuses = status.split(",");
                Expression<String> exp = root.get("status");
                predicates.add(exp.in(Arrays.asList(statuses)));
            }*/
            return builder.equal(customer.get("storeOwner"), storeOwner);
            //return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
