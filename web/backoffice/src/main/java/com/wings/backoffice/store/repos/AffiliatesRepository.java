/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.repos;

import com.wings.backoffice.store.domain.Affiliate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface AffiliatesRepository extends JpaRepository<Affiliate, Long> {

    Affiliate findByPaymentEmail(String paymentEmail);

    @Modifying
    @Transactional
    @Query(value = "UPDATE wingstore_affiliate_wp_affiliates set affiliate_id = :newId WHERE affiliate_id = :oldId", nativeQuery = true)
    int updateAffiliateId(@Param("oldId") Long oldId, @Param("newId") Long newId);

}
