/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface OrdersRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {

    List<Order> findByCreationDateAndPayerEmail(Date creationDate, String payerEmail);

    List<Order> findByActivationDateAndStatusIn(Date activationDate, List<String> status);

    List<Order> findByActiveAndPayerEmail(boolean active, String payerEmail);

    List<Order> findByDeletedAndMemberId(boolean deleted, Long memberId);

    List<Order> findByActive(boolean active);

    List<Order> findByPayerEmail(String payerEmail);

    List<Order> findByPayerEmailAndDeleted(String payerEmail, Boolean deleted);

    List<Order> findByPayerEmailAndDeletedAndTypeIn(String payerEmail, Boolean deleted, List<String> types);

    List<Order> findByPayerEmailAndDeletedAndTypeInOrderByIdDesc(String payerEmail, Boolean deleted, List<String> types);

    Order findByExternalId(Long externalId);

    List<Order> findByPayerEmailAndExternalIdAndDeleted(String payerEmail, Long externalId, Boolean deleted);

    List<Order> findByPayerEmailAndExternalIdAndStatusAndDeleted(String payerEmail, Long externalId, String status, Boolean deleted);

    Order findByTypeAndExternalId(String orderType, Long externalId);

    Order findFirstByExternalIdAndTypeIn(Long externalId, List<String> type);

    Order findFirstByExternalIdAndTypeInAndSource(Long externalId, List<String> type, String source);

    Order findFirstByExternalIdAndTypeInAndSourceNotIn(Long externalId, List<String> type, List<String> sources);

    List<Order> findByExternalIdGreaterThanEqual(Long externalId);

    List<Order> findByType(String type);

    List<Order> findByTypeAndDeletedAndStatus(String type, Boolean deleted, String status);

    List<Order> findByTypeOrderByCreationDateAscMemberIdAscExternalIdAsc(String type);

    List<Order> findByTypeAndMemberId(String type, Long memberId);

    List<Order> findByTypeAndStoreId(String type, String storeId);

    List<Order> findBySource(String source);

    @Query(value = "select member_id from bis_orders where id = ?1", nativeQuery = true)
    Long findMemberIdByOrderId(Long orderId);

    //select * from bis_orders where store_id = '2108633824' and `type` = 'product' and status = 'active' where activation_date <= ;
    Long countByStoreIdAndTypeAndActiveAndActivationDateLessThanEqual(String storeId, String type, Boolean active, Date activationDate);

    List<Order> findByMemberIdAndTypeAndDeletedAndExternalIdAndStatus(Long memberId, String type, boolean deleted, Long extrenalId, String status);

    List<Order> findByActiveAndDeleted(Boolean active, Boolean deleted);

    List<Order> findByMemberIdAndStatusAndIdNotIn(Long externalId, String status, List<Long> ids);

    List<Order> findByMemberIdAndStatusAndTypeInAndIdNotIn(Long externalId, String status, List<String> types, List<Long> ids);

    /*    
    select * from bis_orders where 
    status = 'paid' 
    and item_id in (190,195,196,197,198,199,200,201,202,203,204,206,207,208,209,210,211) 
    and type_status = 'paid'
    and deleted = 0;
     */
    List<Order> findByStatusAndItemIdInAndTypeStatusAndDeleted(String status, List<Long> itemIds, String typeStatus, Boolean deleted);

    List<Order> findByStatusAndActivationDateGreaterThanEqualAndActivationDateLessThanEqual(String status, Date from, Date to);

    List<Order> findByStatusAndBillingCountryAndDeleted(String status, String billingCountry, Boolean deleted);

    long countByMemberIdAndItemIdAndDeletedAndStatus(Long memberId, Long itemId, Boolean deleted, String status);

    List<Order> findByDeletedAndInstallmentNumberAndInstallmentComplete(Boolean deleted, Integer installmentNumber, Boolean installmentComplete);
    
}
