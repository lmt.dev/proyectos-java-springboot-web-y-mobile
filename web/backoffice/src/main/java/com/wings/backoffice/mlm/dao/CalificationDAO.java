/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.utils.DateUtils;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CalificationDAO {

    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    public CalificationSums find(String q, String type, Date from, Date to) {

        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.dao.CalificationSums(sum(c.points) as total,");
        b.append("sum(CASE WHEN (type = 'kit') THEN c.points ELSE 0 END) as totalKit, ");
        b.append("sum(CASE WHEN (type = 'service') THEN c.points ELSE 0 END) as totalService, ");
        b.append("sum(CASE WHEN (type = 'product') THEN c.points ELSE 0 END) as totalProduct) ");
        b.append("FROM CalificationPoint as c, Member as m1 ");
        b.append("WHERE m1.id = c.memberId ");

        if (q != null) {
            b.append(" AND (");
            b.append(" m1.firstName like '%").append(q).append("%'");
            b.append(" OR m1.lastName like '%").append(q).append("%'");
            b.append(" OR m1.email like '%").append(q).append("%'");
            b.append(" OR m1.username like '%").append(q).append("%'");
            b.append(" OR concat(m1.firstName,' ',m1.lastName) like '%").append(q).append("%')");
        }

        if (type != null) {
            b.append(" AND (c.type = '").append(type).append("')");
        }

        if (from != null) {
            b.append(" AND (c.date >= '").append(DateUtils.fromDateToMysqlString(from)).append("')");
        }

        if (to != null) {
            b.append(" AND (c.date <= '").append(DateUtils.fromDateToMysqlString(to)).append("')");
        }

        Query query = em.createQuery(b.toString(), CalificationSums.class);
        CalificationSums ret = (CalificationSums) query.getSingleResult();

        return ret;
    }

}
