/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.marketing.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "users")
public class MktUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ref")
    private int ref;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lider")
    private int lider;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lider_ok")
    private int liderOk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "admin")
    private int admin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "ip_address")
    private String ipAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "track")
    private String track;
    @Size(max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password_first")
    private String passwordFirst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "autopass")
    private String autopass;
    @Size(max = 255)
    @Column(name = "salt")
    private String salt;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 40)
    @Column(name = "activation_code")
    private String activationCode;
    @Size(max = 40)
    @Column(name = "forgotten_password_code")
    private String forgottenPasswordCode;
    @Column(name = "forgotten_password_time")
    private Integer forgottenPasswordTime;
    @Size(max = 40)
    @Column(name = "remember_code")
    private String rememberCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    private int createdOn;
    @Column(name = "last_login")
    private Integer lastLogin;
    @Column(name = "datetrial")
    private Integer datetrial;
    @Column(name = "datefull")
    private Integer datefull;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "confirm")
    private Boolean confirm;
    @Column(name = "new")
    private Boolean new1;
    @Column(name = "encuesta_ok")
    private Boolean encuestaOk;
    @Column(name = "encuesta_act")
    private Boolean encuestaAct;
    @Size(max = 30)
    @Column(name = "encuesta_act_date")
    private String encuestaActDate;
    @Column(name = "pagado")
    private Boolean pagado;
    @Column(name = "send_resp")
    private Boolean sendResp;
    @Column(name = "pago_primero")
    private Boolean pagoPrimero;
    @Column(name = "pagos")
    private Boolean pagos;
    @Column(name = "vip")
    private Boolean vip;
    @Column(name = "price_num")
    private Boolean priceNum;
    @Column(name = "trial")
    private Boolean trial;
    @Column(name = "trial_usado")
    private Boolean trialUsado;
    @Column(name = "pagado_ok")
    private Boolean pagadoOk;
    @Column(name = "autoresponder_ok")
    private Boolean autoresponderOk;
    @Size(max = 50)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 50)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 50)
    @Column(name = "idioma")
    private String idioma;
    @Size(max = 100)
    @Column(name = "pais")
    private String pais;
    @Size(max = 100)
    @Column(name = "country")
    private String country;
    @Size(max = 50)
    @Column(name = "area_code")
    private String areaCode;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "phone")
    private String phone;
    @Size(max = 100)
    @Column(name = "whatsapp")
    private String whatsapp;
    @Size(max = 100)
    @Column(name = "whatsapp_key")
    private String whatsappKey;
    @Size(max = 100)
    @Column(name = "skype")
    private String skype;
    @Size(max = 200)
    @Column(name = "facebook")
    private String facebook;
    @Size(max = 100)
    @Column(name = "responder")
    private String responder;
    @Size(max = 100)
    @Column(name = "campaign_name")
    private String campaignName;
    @Size(max = 100)
    @Column(name = "CampaignCode")
    private String campaignCode;
    @Size(max = 100)
    @Column(name = "AffiliateName")
    private String affiliateName;
    @Size(max = 100)
    @Column(name = "lp")
    private String lp;
    @Size(max = 100)
    @Column(name = "lp_pordefecto")
    private String lpPordefecto;
    @Size(max = 100)
    @Column(name = "tracker")
    private String tracker;
    @Lob
    @Size(max = 65535)
    @Column(name = "pixel_fb")
    private String pixelFb;
    @Lob
    @Size(max = 65535)
    @Column(name = "pixel_visita")
    private String pixelVisita;
    @Lob
    @Size(max = 65535)
    @Column(name = "pixel_conversion")
    private String pixelConversion;
    @Lob
    @Size(max = 65535)
    @Column(name = "analytics")
    private String analytics;
    @Size(max = 150)
    @Column(name = "lr")
    private String lr;
    @Size(max = 150)
    @Column(name = "wingsmobile")
    private String wingsmobile;
    @Size(max = 200)
    @Column(name = "youcanbook")
    private String youcanbook;
    @Size(max = 200)
    @Column(name = "zaxaa")
    private String zaxaa;
    @Size(max = 200)
    @Column(name = "c2s")
    private String c2s;
    @Size(max = 200)
    @Column(name = "clickbank")
    private String clickbank;
    @Size(max = 150)
    @Column(name = "runcpa")
    private String runcpa;
    @Size(max = 150)
    @Column(name = "runcpa_genesismining")
    private String runcpaGenesismining;
    @Size(max = 150)
    @Column(name = "cupon_genesismining")
    private String cuponGenesismining;
    @Size(max = 150)
    @Column(name = "hashflare")
    private String hashflare;
    @Size(max = 150)
    @Column(name = "runcpa_hashflare")
    private String runcpaHashflare;
    @Size(max = 150)
    @Column(name = "iftt_contact")
    private String ifttContact;
    @Size(max = 150)
    @Column(name = "iftt_visto")
    private String ifttVisto;
    @Size(max = 150)
    @Column(name = "iftt_vistowhatsapp")
    private String ifttVistowhatsapp;
    @Size(max = 150)
    @Column(name = "iftt_cita")
    private String ifttCita;
    @Size(max = 150)
    @Column(name = "recyclix")
    private String recyclix;
    @Size(max = 150)
    @Column(name = "getresponse")
    private String getresponse;
    @Size(max = 150)
    @Column(name = "gvo")
    private String gvo;
    @Size(max = 150)
    @Column(name = "circle")
    private String circle;
    @Size(max = 150)
    @Column(name = "coinbase")
    private String coinbase;
    @Column(name = "video_inicio")
    private Integer videoInicio;
    @Size(max = 50)
    @Column(name = "gustaria")
    private String gustaria;
    @Size(max = 50)
    @Column(name = "retos")
    private String retos;
    @Size(max = 50)
    @Column(name = "mktonline")
    private String mktonline;
    @Size(max = 2000)
    @Column(name = "mktonlinedesc")
    private String mktonlinedesc;
    @Size(max = 150)
    @Column(name = "web")
    private String web;
    @Size(max = 50)
    @Column(name = "emprendedora")
    private String emprendedora;
    @Size(max = 1000)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 150)
    @Column(name = "monedero")
    private String monedero;

    public MktUser() {
    }

    public MktUser(Integer id) {
        this.id = id;
    }

    public MktUser(Integer id, int ref, int lider, int liderOk, int admin, String ipAddress, String track, String password, String passwordFirst, String autopass, String email, int createdOn) {
        this.id = id;
        this.ref = ref;
        this.lider = lider;
        this.liderOk = liderOk;
        this.admin = admin;
        this.ipAddress = ipAddress;
        this.track = track;
        this.password = password;
        this.passwordFirst = passwordFirst;
        this.autopass = autopass;
        this.email = email;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getRef() {
        return ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    public int getLider() {
        return lider;
    }

    public void setLider(int lider) {
        this.lider = lider;
    }

    public int getLiderOk() {
        return liderOk;
    }

    public void setLiderOk(int liderOk) {
        this.liderOk = liderOk;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordFirst() {
        return passwordFirst;
    }

    public void setPasswordFirst(String passwordFirst) {
        this.passwordFirst = passwordFirst;
    }

    public String getAutopass() {
        return autopass;
    }

    public void setAutopass(String autopass) {
        this.autopass = autopass;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getForgottenPasswordCode() {
        return forgottenPasswordCode;
    }

    public void setForgottenPasswordCode(String forgottenPasswordCode) {
        this.forgottenPasswordCode = forgottenPasswordCode;
    }

    public Integer getForgottenPasswordTime() {
        return forgottenPasswordTime;
    }

    public void setForgottenPasswordTime(Integer forgottenPasswordTime) {
        this.forgottenPasswordTime = forgottenPasswordTime;
    }

    public String getRememberCode() {
        return rememberCode;
    }

    public void setRememberCode(String rememberCode) {
        this.rememberCode = rememberCode;
    }

    public int getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(int createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Integer lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getDatetrial() {
        return datetrial;
    }

    public void setDatetrial(Integer datetrial) {
        this.datetrial = datetrial;
    }

    public Integer getDatefull() {
        return datefull;
    }

    public void setDatefull(Integer datefull) {
        this.datefull = datefull;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public Boolean getNew1() {
        return new1;
    }

    public void setNew1(Boolean new1) {
        this.new1 = new1;
    }

    public Boolean getEncuestaOk() {
        return encuestaOk;
    }

    public void setEncuestaOk(Boolean encuestaOk) {
        this.encuestaOk = encuestaOk;
    }

    public Boolean getEncuestaAct() {
        return encuestaAct;
    }

    public void setEncuestaAct(Boolean encuestaAct) {
        this.encuestaAct = encuestaAct;
    }

    public String getEncuestaActDate() {
        return encuestaActDate;
    }

    public void setEncuestaActDate(String encuestaActDate) {
        this.encuestaActDate = encuestaActDate;
    }

    public Boolean getPagado() {
        return pagado;
    }

    public void setPagado(Boolean pagado) {
        this.pagado = pagado;
    }

    public Boolean getSendResp() {
        return sendResp;
    }

    public void setSendResp(Boolean sendResp) {
        this.sendResp = sendResp;
    }

    public Boolean getPagoPrimero() {
        return pagoPrimero;
    }

    public void setPagoPrimero(Boolean pagoPrimero) {
        this.pagoPrimero = pagoPrimero;
    }

    public Boolean getPagos() {
        return pagos;
    }

    public void setPagos(Boolean pagos) {
        this.pagos = pagos;
    }

    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    public Boolean getPriceNum() {
        return priceNum;
    }

    public void setPriceNum(Boolean priceNum) {
        this.priceNum = priceNum;
    }

    public Boolean getTrial() {
        return trial;
    }

    public void setTrial(Boolean trial) {
        this.trial = trial;
    }

    public Boolean getTrialUsado() {
        return trialUsado;
    }

    public void setTrialUsado(Boolean trialUsado) {
        this.trialUsado = trialUsado;
    }

    public Boolean getPagadoOk() {
        return pagadoOk;
    }

    public void setPagadoOk(Boolean pagadoOk) {
        this.pagadoOk = pagadoOk;
    }

    public Boolean getAutoresponderOk() {
        return autoresponderOk;
    }

    public void setAutoresponderOk(Boolean autoresponderOk) {
        this.autoresponderOk = autoresponderOk;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getWhatsappKey() {
        return whatsappKey;
    }

    public void setWhatsappKey(String whatsappKey) {
        this.whatsappKey = whatsappKey;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getAffiliateName() {
        return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
        this.affiliateName = affiliateName;
    }

    public String getLp() {
        return lp;
    }

    public void setLp(String lp) {
        this.lp = lp;
    }

    public String getLpPordefecto() {
        return lpPordefecto;
    }

    public void setLpPordefecto(String lpPordefecto) {
        this.lpPordefecto = lpPordefecto;
    }

    public String getTracker() {
        return tracker;
    }

    public void setTracker(String tracker) {
        this.tracker = tracker;
    }

    public String getPixelFb() {
        return pixelFb;
    }

    public void setPixelFb(String pixelFb) {
        this.pixelFb = pixelFb;
    }

    public String getPixelVisita() {
        return pixelVisita;
    }

    public void setPixelVisita(String pixelVisita) {
        this.pixelVisita = pixelVisita;
    }

    public String getPixelConversion() {
        return pixelConversion;
    }

    public void setPixelConversion(String pixelConversion) {
        this.pixelConversion = pixelConversion;
    }

    public String getAnalytics() {
        return analytics;
    }

    public void setAnalytics(String analytics) {
        this.analytics = analytics;
    }

    public String getLr() {
        return lr;
    }

    public void setLr(String lr) {
        this.lr = lr;
    }

    public String getWingsmobile() {
        return wingsmobile;
    }

    public void setWingsmobile(String wingsmobile) {
        this.wingsmobile = wingsmobile;
    }

    public String getYoucanbook() {
        return youcanbook;
    }

    public void setYoucanbook(String youcanbook) {
        this.youcanbook = youcanbook;
    }

    public String getZaxaa() {
        return zaxaa;
    }

    public void setZaxaa(String zaxaa) {
        this.zaxaa = zaxaa;
    }

    public String getC2s() {
        return c2s;
    }

    public void setC2s(String c2s) {
        this.c2s = c2s;
    }

    public String getClickbank() {
        return clickbank;
    }

    public void setClickbank(String clickbank) {
        this.clickbank = clickbank;
    }

    public String getRuncpa() {
        return runcpa;
    }

    public void setRuncpa(String runcpa) {
        this.runcpa = runcpa;
    }

    public String getRuncpaGenesismining() {
        return runcpaGenesismining;
    }

    public void setRuncpaGenesismining(String runcpaGenesismining) {
        this.runcpaGenesismining = runcpaGenesismining;
    }

    public String getCuponGenesismining() {
        return cuponGenesismining;
    }

    public void setCuponGenesismining(String cuponGenesismining) {
        this.cuponGenesismining = cuponGenesismining;
    }

    public String getHashflare() {
        return hashflare;
    }

    public void setHashflare(String hashflare) {
        this.hashflare = hashflare;
    }

    public String getRuncpaHashflare() {
        return runcpaHashflare;
    }

    public void setRuncpaHashflare(String runcpaHashflare) {
        this.runcpaHashflare = runcpaHashflare;
    }

    public String getIfttContact() {
        return ifttContact;
    }

    public void setIfttContact(String ifttContact) {
        this.ifttContact = ifttContact;
    }

    public String getIfttVisto() {
        return ifttVisto;
    }

    public void setIfttVisto(String ifttVisto) {
        this.ifttVisto = ifttVisto;
    }

    public String getIfttVistowhatsapp() {
        return ifttVistowhatsapp;
    }

    public void setIfttVistowhatsapp(String ifttVistowhatsapp) {
        this.ifttVistowhatsapp = ifttVistowhatsapp;
    }

    public String getIfttCita() {
        return ifttCita;
    }

    public void setIfttCita(String ifttCita) {
        this.ifttCita = ifttCita;
    }

    public String getRecyclix() {
        return recyclix;
    }

    public void setRecyclix(String recyclix) {
        this.recyclix = recyclix;
    }

    public String getGetresponse() {
        return getresponse;
    }

    public void setGetresponse(String getresponse) {
        this.getresponse = getresponse;
    }

    public String getGvo() {
        return gvo;
    }

    public void setGvo(String gvo) {
        this.gvo = gvo;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getCoinbase() {
        return coinbase;
    }

    public void setCoinbase(String coinbase) {
        this.coinbase = coinbase;
    }

    public Integer getVideoInicio() {
        return videoInicio;
    }

    public void setVideoInicio(Integer videoInicio) {
        this.videoInicio = videoInicio;
    }

    public String getGustaria() {
        return gustaria;
    }

    public void setGustaria(String gustaria) {
        this.gustaria = gustaria;
    }

    public String getRetos() {
        return retos;
    }

    public void setRetos(String retos) {
        this.retos = retos;
    }

    public String getMktonline() {
        return mktonline;
    }

    public void setMktonline(String mktonline) {
        this.mktonline = mktonline;
    }

    public String getMktonlinedesc() {
        return mktonlinedesc;
    }

    public void setMktonlinedesc(String mktonlinedesc) {
        this.mktonlinedesc = mktonlinedesc;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getEmprendedora() {
        return emprendedora;
    }

    public void setEmprendedora(String emprendedora) {
        this.emprendedora = emprendedora;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMonedero() {
        return monedero;
    }

    public void setMonedero(String monedero) {
        this.monedero = monedero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MktUser)) {
            return false;
        }
        MktUser other = (MktUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.marketing.domain.MtkUser[ id=" + id + " ]";
    }

}
