/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class NativeServicesService {

    private final String url = "https://switch.wingsmobile.com";
    private final String token = "3d90b9c118c65dfbf2215d481cbc79c0f706b76d86a2c4d1179c1a998d81e181";
    private final Logger log = LoggerFactory.getLogger(NativeServicesService.class);

    /**
     * check if numbers is well formed and exits on switch login numbers
     *
     * @param number
     * @return
     */
    public boolean validateNumber(String number) {

        number = number.replaceAll(" ", "").replaceAll("\\+", "");
        //Si necesito loguear uso esto
        //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //Si no necesito loguear uso esto
        RestTemplate template = new RestTemplate();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("login", number);

            RequestEntity<Map<String, String>> entity = RequestEntity
                    .post(new URI(url + "/api/validate"))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .header("auth", token)
                    .body(params);

            ResponseEntity<String> response = template.exchange(entity, String.class);

            return response.getBody() != null && response.getBody().equals("success");

        } catch (URISyntaxException | RestClientException e) {
            log.error("Error on topup", e);
        }

        return false;
    }

    /**
     * add balance to number
     *
     * @param number
     * @param amount
     * @param orderId
     * @return
     */
    public boolean topUpNumber(String number, Double amount, Long orderId) {

        number = number.replaceAll(" ", "").replaceAll("\\+", "");
        //Si necesito loguear uso esto
        //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());        
        //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //Si no necesito loguear uso esto
        RestTemplate template = new RestTemplate();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("login", number);
            params.put("amount", amount.toString());
            params.put("orderId", orderId.toString());

            RequestEntity<Map<String, String>> entity = RequestEntity
                    .post(new URI(url + "/api/topup"))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .header("auth", token)
                    .body(params);

            ResponseEntity<String> response = template.exchange(entity, String.class);
            return response.getBody() != null && response.getBody().equals("success");

        } catch (URISyntaxException | RestClientException e) {
            log.error("Error on topup", e);
        }

        return false;
    }

}
