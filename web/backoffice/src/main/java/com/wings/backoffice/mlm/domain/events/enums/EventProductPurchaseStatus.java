/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.events.enums;

/**
 *
 * @author seba
 */
public enum EventProductPurchaseStatus {
    
    pending,
    paid,
    deleted,
    
}
