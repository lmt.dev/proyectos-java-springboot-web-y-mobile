/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_speed_bonus")
public class SpeedBonus implements Serializable {

    private static long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String country;
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @Temporal(TemporalType.TIMESTAMP)
    private Date durationTo;
    @Temporal(TemporalType.TIMESTAMP)
    private Date durationToFounder;
    private Integer durationDays;
    private Integer durationDaysFounder;
    private boolean excludeClientCount;

    private Double priceRank1;
    private Double priceRank2;
    private Double priceRank3;
    private Double priceRank4;
    private Double priceRank5;
    private Double priceRank6;
    private Double priceRank7;
    private Double priceRank8;
    private Double priceRank9;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    /**
     * @return the durationTo
     */
    public Date getDurationTo() {
        return durationTo;
    }

    /**
     * @param durationTo the durationTo to set
     */
    public void setDurationTo(Date durationTo) {
        this.durationTo = durationTo;
    }

    /**
     * @return the durationToFounder
     */
    public Date getDurationToFounder() {
        return durationToFounder;
    }

    /**
     * @param durationToFounder the durationToFounder to set
     */
    public void setDurationToFounder(Date durationToFounder) {
        this.durationToFounder = durationToFounder;
    }

    /**
     * @return the durationDays
     */
    public Integer getDurationDays() {
        return durationDays;
    }

    /**
     * @param durationDays the durationDays to set
     */
    public void setDurationDays(Integer durationDays) {
        this.durationDays = durationDays;
    }

    /**
     * @return the durationDaysFounder
     */
    public Integer getDurationDaysFounder() {
        return durationDaysFounder;
    }

    /**
     * @param durationDaysFounder the durationDaysFounder to set
     */
    public void setDurationDaysFounder(Integer durationDaysFounder) {
        this.durationDaysFounder = durationDaysFounder;
    }

    @Override
    public String toString() {
        return getName() + "(#" + getId() + ")";
    }

    /**
     * @return the priceRank1
     */
    public Double getPriceRank1() {
        return priceRank1;
    }

    /**
     * @param priceRank1 the priceRank1 to set
     */
    public void setPriceRank1(Double priceRank1) {
        this.priceRank1 = priceRank1;
    }

    /**
     * @return the priceRank2
     */
    public Double getPriceRank2() {
        return priceRank2;
    }

    /**
     * @param priceRank2 the priceRank2 to set
     */
    public void setPriceRank2(Double priceRank2) {
        this.priceRank2 = priceRank2;
    }

    /**
     * @return the priceRank3
     */
    public Double getPriceRank3() {
        return priceRank3;
    }

    /**
     * @param priceRank3 the priceRank3 to set
     */
    public void setPriceRank3(Double priceRank3) {
        this.priceRank3 = priceRank3;
    }

    /**
     * @return the priceRank4
     */
    public Double getPriceRank4() {
        return priceRank4;
    }

    /**
     * @param priceRank4 the priceRank4 to set
     */
    public void setPriceRank4(Double priceRank4) {
        this.priceRank4 = priceRank4;
    }

    /**
     * @return the priceRank5
     */
    public Double getPriceRank5() {
        return priceRank5;
    }

    /**
     * @param priceRank5 the priceRank5 to set
     */
    public void setPriceRank5(Double priceRank5) {
        this.priceRank5 = priceRank5;
    }

    /**
     * @return the priceRank6
     */
    public Double getPriceRank6() {
        return priceRank6;
    }

    /**
     * @param priceRank6 the priceRank6 to set
     */
    public void setPriceRank6(Double priceRank6) {
        this.priceRank6 = priceRank6;
    }

    /**
     * @return the priceRank7
     */
    public Double getPriceRank7() {
        return priceRank7;
    }

    /**
     * @param priceRank7 the priceRank7 to set
     */
    public void setPriceRank7(Double priceRank7) {
        this.priceRank7 = priceRank7;
    }

    /**
     * @return the priceRank8
     */
    public Double getPriceRank8() {
        return priceRank8;
    }

    /**
     * @param priceRank8 the priceRank8 to set
     */
    public void setPriceRank8(Double priceRank8) {
        this.priceRank8 = priceRank8;
    }

    /**
     * @return the priceRank9
     */
    public Double getPriceRank9() {
        return priceRank9;
    }

    /**
     * @param priceRank9 the priceRank9 to set
     */
    public void setPriceRank9(Double priceRank9) {
        this.priceRank9 = priceRank9;
    }

    public boolean getExcludeClientCount() {
        return excludeClientCount;
    }

    public void setExcludeClientCount(boolean excludeClientCount) {
        this.excludeClientCount = excludeClientCount;
    }

}
