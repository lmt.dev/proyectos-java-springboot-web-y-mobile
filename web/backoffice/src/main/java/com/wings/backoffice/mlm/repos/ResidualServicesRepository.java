/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.ResidualService;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ResidualServicesRepository extends JpaRepository<ResidualService, Long>, JpaSpecificationExecutor<ResidualService> {

    ResidualService findByReferenceAndReferenceId(String reference, Long referenceId);

    //List<ResidualService> findByStoreId(String storeId);

    Long countByStoreIdAndStatusAndCreationDateLessThanEqual(String storeId, String status, Date creationDate);

    List<ResidualService> findByTypeAndPersonal(String type, Boolean personal);

}
