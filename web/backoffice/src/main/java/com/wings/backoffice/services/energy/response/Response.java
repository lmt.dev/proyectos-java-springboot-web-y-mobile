
package com.wings.backoffice.services.energy.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "idContrato",
    "honorarios",
    "deposito",
    "codigoComercial",
    "datosSuministro",
    "datosCliente",
    "datosCorrespondencia",
    "tarifa",
    "potencias",
    "telefonoFijo",
    "telefonoMovil",
    "email",
    "nombreContacto",
    "dniContacto",
    "denominacionSocial",
    "nif",
    "cups",
    "cnae",
    "descripcionCnae",
    "tipoContrato",
    "fechaFirma",
    "fechaContrato",
    "consumo",
    "consumoMensual",
    "estado",
    "proceso",
    "incidencia",
    "motivo"
})
public class Response {

    @JsonProperty("idContrato")
    private String idContrato;
    @JsonProperty("honorarios")
    private String honorarios;
    @JsonProperty("deposito")
    private String deposito;
    @JsonProperty("codigoComercial")
    private String codigoComercial;
    @JsonProperty("datosSuministro")
    private DatosSuministro datosSuministro;
    @JsonProperty("datosCliente")
    private DatosCliente datosCliente;
    @JsonProperty("datosCorrespondencia")
    private DatosCorrespondencia datosCorrespondencia;
    @JsonProperty("tarifa")
    private String tarifa;
    @JsonProperty("potencias")
    private Potencias potencias;
    @JsonProperty("telefonoFijo")
    private String telefonoFijo;
    @JsonProperty("telefonoMovil")
    private String telefonoMovil;
    @JsonProperty("email")
    private String email;
    @JsonProperty("nombreContacto")
    private String nombreContacto;
    @JsonProperty("dniContacto")
    private String dniContacto;
    @JsonProperty("denominacionSocial")
    private String denominacionSocial;
    @JsonProperty("nif")
    private String nif;
    @JsonProperty("cups")
    private String cups;
    @JsonProperty("cnae")
    private String cnae;
    @JsonProperty("descripcionCnae")
    private Object descripcionCnae;
    @JsonProperty("tipoContrato")
    private String tipoContrato;
    @JsonProperty("fechaFirma")
    private Object fechaFirma;
    @JsonProperty("fechaContrato")
    private Long fechaContrato;
    @JsonProperty("consumo")
    private String consumo;
    @JsonProperty("consumoMensual")
    private String consumoMensual;
    @JsonProperty("estado")
    private String estado;
    @JsonProperty("proceso")
    private String proceso;
    @JsonProperty("incidencia")
    private Object incidencia;
    @JsonProperty("motivo")
    private Object motivo;

    @JsonProperty("idContrato")
    public String getIdContrato() {
        return idContrato;
    }

    @JsonProperty("idContrato")
    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    @JsonProperty("honorarios")
    public String getHonorarios() {
        return honorarios;
    }

    @JsonProperty("honorarios")
    public void setHonorarios(String honorarios) {
        this.honorarios = honorarios;
    }

    @JsonProperty("deposito")
    public String getDeposito() {
        return deposito;
    }

    @JsonProperty("deposito")
    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }

    @JsonProperty("codigoComercial")
    public String getCodigoComercial() {
        return codigoComercial;
    }

    @JsonProperty("codigoComercial")
    public void setCodigoComercial(String codigoComercial) {
        this.codigoComercial = codigoComercial;
    }

    @JsonProperty("datosSuministro")
    public DatosSuministro getDatosSuministro() {
        return datosSuministro;
    }

    @JsonProperty("datosSuministro")
    public void setDatosSuministro(DatosSuministro datosSuministro) {
        this.datosSuministro = datosSuministro;
    }

    @JsonProperty("datosCliente")
    public DatosCliente getDatosCliente() {
        return datosCliente;
    }

    @JsonProperty("datosCliente")
    public void setDatosCliente(DatosCliente datosCliente) {
        this.datosCliente = datosCliente;
    }

    @JsonProperty("datosCorrespondencia")
    public DatosCorrespondencia getDatosCorrespondencia() {
        return datosCorrespondencia;
    }

    @JsonProperty("datosCorrespondencia")
    public void setDatosCorrespondencia(DatosCorrespondencia datosCorrespondencia) {
        this.datosCorrespondencia = datosCorrespondencia;
    }

    @JsonProperty("tarifa")
    public String getTarifa() {
        return tarifa;
    }

    @JsonProperty("tarifa")
    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    @JsonProperty("potencias")
    public Potencias getPotencias() {
        return potencias;
    }

    @JsonProperty("potencias")
    public void setPotencias(Potencias potencias) {
        this.potencias = potencias;
    }

    @JsonProperty("telefonoFijo")
    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    @JsonProperty("telefonoFijo")
    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    @JsonProperty("telefonoMovil")
    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    @JsonProperty("telefonoMovil")
    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("nombreContacto")
    public String getNombreContacto() {
        return nombreContacto;
    }

    @JsonProperty("nombreContacto")
    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    @JsonProperty("dniContacto")
    public String getDniContacto() {
        return dniContacto;
    }

    @JsonProperty("dniContacto")
    public void setDniContacto(String dniContacto) {
        this.dniContacto = dniContacto;
    }

    @JsonProperty("denominacionSocial")
    public String getDenominacionSocial() {
        return denominacionSocial;
    }

    @JsonProperty("denominacionSocial")
    public void setDenominacionSocial(String denominacionSocial) {
        this.denominacionSocial = denominacionSocial;
    }

    @JsonProperty("nif")
    public String getNif() {
        return nif;
    }

    @JsonProperty("nif")
    public void setNif(String nif) {
        this.nif = nif;
    }

    @JsonProperty("cups")
    public String getCups() {
        return cups;
    }

    @JsonProperty("cups")
    public void setCups(String cups) {
        this.cups = cups;
    }

    @JsonProperty("cnae")
    public String getCnae() {
        return cnae;
    }

    @JsonProperty("cnae")
    public void setCnae(String cnae) {
        this.cnae = cnae;
    }

    @JsonProperty("descripcionCnae")
    public Object getDescripcionCnae() {
        return descripcionCnae;
    }

    @JsonProperty("descripcionCnae")
    public void setDescripcionCnae(Object descripcionCnae) {
        this.descripcionCnae = descripcionCnae;
    }

    @JsonProperty("tipoContrato")
    public String getTipoContrato() {
        return tipoContrato;
    }

    @JsonProperty("tipoContrato")
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    @JsonProperty("fechaFirma")
    public Object getFechaFirma() {
        return fechaFirma;
    }

    @JsonProperty("fechaFirma")
    public void setFechaFirma(Object fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    @JsonProperty("fechaContrato")
    public Long getFechaContrato() {
        return fechaContrato;
    }

    @JsonProperty("fechaContrato")
    public void setFechaContrato(Long fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    @JsonProperty("consumo")
    public String getConsumo() {
        return consumo;
    }

    @JsonProperty("consumo")
    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    @JsonProperty("consumoMensual")
    public String getConsumoMensual() {
        return consumoMensual;
    }

    @JsonProperty("consumoMensual")
    public void setConsumoMensual(String consumoMensual) {
        this.consumoMensual = consumoMensual;
    }

    @JsonProperty("estado")
    public String getEstado() {
        return estado;
    }

    @JsonProperty("estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @JsonProperty("proceso")
    public String getProceso() {
        return proceso;
    }

    @JsonProperty("proceso")
    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    @JsonProperty("incidencia")
    public Object getIncidencia() {
        return incidencia;
    }

    @JsonProperty("incidencia")
    public void setIncidencia(Object incidencia) {
        this.incidencia = incidencia;
    }

    @JsonProperty("motivo")
    public Object getMotivo() {
        return motivo;
    }

    @JsonProperty("motivo")
    public void setMotivo(Object motivo) {
        this.motivo = motivo;
    }

}
