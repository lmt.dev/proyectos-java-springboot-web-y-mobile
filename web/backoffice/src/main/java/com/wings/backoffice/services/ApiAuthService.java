/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ApiAuthService {

    private final String authTokenDev = "dbmSz2GMdi7sRdYp3qgFKhJnmgeD4JvQ99mHoQhAGs2adPeP";
    private final String authTokenProd = "2b42d87da1c98cfd1ac71d837c589393e5a525c58258bca705110d7c65d0a331";

    /**
     * //TODO: improve
     *
     * @param request
     * @return
     */
    public boolean authRequest(HttpServletRequest request) {
        return request.getHeader("auth") != null && (request.getHeader("auth").equals(authTokenDev) || request.getHeader("auth").equals(authTokenProd));
    }
}
