/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.ecuador;

import com.wings.backoffice.mlm.domain.City;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import com.wings.backoffice.mlm.repos.CitiesRepository;
import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class EcuadorShippingService {

    @Autowired
    private CitiesRepository citiesRepository;

    //private final String user = "SER.PRUEBAS";
    //private final String password = "1234";   
    /*
        wingsmobile.s.a.  12345
        WIN.5493518149914 1234
        win.0993128317001 1234
        wingsmobile.s.a   12345
     */
    private final String user = "wingsmobile.s.a.";
    //private final String pass = "12345";
    private final String pass = "252331";

    private final Logger log = LoggerFactory.getLogger(EcuadorShippingService.class);

    //private final String url = "http://200.31.31.74:8021/api/guiawebs/['USER','PASSWORD']";
    //private final String urlPost = "http://200.31.31.74:8021/api/guiawebs";
    //private final String urlCities = "http://200.31.31.74:8021/api/ciudades/['SER.PRUEBAS','1234']";
    //private final String urlTracking = "http://200.31.31.74:8021/api/guiawebs/['TRACKING_NUMBER','USERNAME','PASSWORD']";
    //private final String urlTracking = "https://swservicli.servientrega.com.ec:5052/api/guiawebs/['TRACKING_NUMBER','USERNAME','PASSWORD']";
    private final String urlPost = "https://swservicli.servientrega.com.ec:5052/api/guiawebs";
    private final String urlTracking = "https://swservicli.servientrega.com.ec:5052/api/guiawebs/['TRACKING_NUMBER','USERNAME','PASSWORD']";

    public EcuadorGuiaResponse sendToCurier(Shipment shipment) {

        EcuadorGuiaResponse ret = new EcuadorGuiaResponse();
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //RestTemplate template = new RestTemplate();
        try {

            EcuadorShipmentWrapper wrapper = transform(shipment);
            /*MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
            converter.setObjectMapper(new ObjectMapper());
            template.getMessageConverters().add(converter);*/
            RequestEntity<EcuadorShipmentWrapper> entity = RequestEntity
                    .post(new URI(urlPost))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .body(wrapper);

            ResponseEntity<EcuadorGuiaResponse> response = template.exchange(entity, EcuadorGuiaResponse.class);
            //ResponseEntity<String> response = template.postForEntity(urlPost, wrapper, String.class);

            ret = response.getBody();
            //TODO: despues de esto, deberia enviar un email con los detalles, del nro de guia
            //y los sku de los productos

        } catch (Exception e) {
            log.error("Error on sendToCurier Ecuador", e);
            ret.setId(0l);
            ret.setMsj("Error " + e.getMessage());
        }
        return ret;
    }

    public void checkTracking(String trackingNumber, String username, String password) {

        String urlReal = urlTracking.replaceAll("TRACKING_NUMBER", trackingNumber);
        if (username != null && !username.isEmpty()) {
            urlReal = urlReal.replaceAll("USERNAME", username);
        } else {
            urlReal = urlReal.replaceAll("USERNAME", user);
        }

        if (password != null && !password.isEmpty()) {
            urlReal = urlReal.replaceAll("PASSWORD", password);
        } else {
            urlReal = urlReal.replaceAll("PASSWORD", pass);
        }

        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //RestTemplate template = new RestTemplate();
        try {

            ResponseEntity<String> response = template.getForEntity(urlReal, String.class);
            log.error("response " + response.getBody());

            /*EcuadorTrackingResponseItem[] response = template.getForObject(urlReal, EcuadorTrackingResponseItem[].class);
            if (response.length >= 1) {
                String estado = response[0].getEstado();
                log.error(estado);
            }*/
        } catch (Exception e) {
            log.error("Error on checkTracking Ecuador", e);
        }
    }

    private EcuadorShipmentWrapper transform(Shipment shipment) throws Exception {

        int idCiudadDestino = 1;
        //buscamos la ciudad destino
        List<City> cities = citiesRepository.findByCityAndCountry(shipment.getShippingCity().trim(), "EC");
        if (cities.isEmpty()) {
            log.error("Ciudad simple no encontrada para el envio " + shipment.getId() + " " + shipment.getShippingCity());
            throw new Exception("City not found for " + shipment.getShippingCity());
        }

        if (cities.size() > 1) {
            log.error("Ciudad simple no encontrada para el envio " + shipment.getId() + " " + shipment.getShippingCity());
            throw new Exception("Multiple cities found for " + shipment.getShippingCity());
            //TODO: mejorar la busqueda y poner aproximaciones
            //String q = "%" + shipment.getShippingCity() + "%";
            //List<City> cities = citiesRepository.findByCountryAndCityLike("EC", q);
        } else {
            //id de servientrega para ecuador
            idCiudadDestino = Integer.valueOf(cities.get(0).getCustom1());
        }

        EcuadorShipmentWrapper ret = new EcuadorShipmentWrapper();

        ret.setAlto(1);
        ret.setAncho(1);
        ret.setLargo(1);
        ret.setValorAsegurado(87);
        ret.setValorMercancia(299);
        ret.setIdTipoLogistica(1);
        ret.setNumeroPiezas(1);
        ret.setPesoFisico(1);
        ret.setIdProducto(1);

        ret.setContenido(shipment.getDescription());
        ret.setDetalleEnvio1(shipment.getId().toString());
        //ret.setDetalleEnvio2(shipment.getItemsText());
        ret.setDetalleEnvio2(shipment.getItemsText().replaceAll("\n", " "));
        ret.setDetalleEnvio3(shipment.getShipmentNumber());

        ret.setIdDestinatario(shipment.getOrderId().toString());
        ret.setNombreDestinatario(shipment.getShippingName());
        ret.setApellidoDestinatario("");
        ret.setCodigoPostal(shipment.getShippingPostalCode());
        ret.setDireccionDestinatario(shipment.getAddressText());
        ret.setTelefonoDestinatario(shipment.getPhoneNumber());
        ret.setTelefonoDestinatario2("");
        ret.setRazonSocialDestinatario("");

        ret.setSectorDestinatario("");
        ret.setIdCiudadDestino(idCiudadDestino);

        ret.setIdCiudadOrigen(1);
        ret.setNombreRemitente("Wings");
        ret.setApellidoRemitente("Mobile");
        ret.setRazonSocialRemitente("WINGSMOBILE  S.A.");
        ret.setTelefonoRemitente("042230023");
        ret.setTelefonoRemitente2("");
        ret.setDireccionRemitente("GARZOTA 2 MZ 24 V 45");
        ret.setSectorRemitente("");
        ret.setIdRemitente("1");
        ret.setCodigoPostalRemitente("");

        ret.setLoginCreacion(user);
        ret.setPassword(pass);

        return ret;

    }

}
