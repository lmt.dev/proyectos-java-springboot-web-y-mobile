/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.InvitationLegacy;
import com.wings.backoffice.mlm.domain.MemberOld;
import com.wings.backoffice.mlm.domain.bis.Invitation;
import com.wings.backoffice.mlm.enums.InvitationStatus;
import com.wings.backoffice.mlm.repos.InvitationLegacyRepository;
import com.wings.backoffice.mlm.repos.InvitationRepository;
import com.wings.backoffice.mlm.repos.MembersOldRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class InvitationService {

    @Autowired
    private InvitationRepository invitationRepository;

    @Autowired
    private InvitationLegacyRepository invitationLegacyRepository;

    @Autowired
    private MembersOldRepository membersOldRepository;

    @Autowired
    private EmailService emailService;

    private final Logger log = LoggerFactory.getLogger(InvitationService.class);

    public String importInvitations() {

        List<InvitationLegacy> legacies = invitationLegacyRepository.findAll();
        int created = 0;

        //first parse invitations
        for (InvitationLegacy legacy : legacies) {

            List<Invitation> already = invitationRepository.findByNombreAndApellidoAndMemberId(WordUtils.capitalizeFully(legacy.getNombre().trim()),
                    WordUtils.capitalizeFully(legacy.getApellido().trim()),
                    legacy.getMemberId());

            if (!already.isEmpty()) {
                if (already.size() > 1) {
                    for (Invitation invitation : already) {
                        invitation.setStatus(InvitationStatus.review.name());
                        invitationRepository.save(invitation);
                        log.error("Duplicado " + invitation.getFullName());
                    }
                }
                continue;
            }

            Invitation i = new Invitation();
            i.setApellido(WordUtils.capitalizeFully(legacy.getApellido().trim()));
            i.setDni(legacy.getDni());
            i.setDniValue(legacy.getDniValue());
            i.setEmail(legacy.getEmail());
            i.setMemberId(legacy.getMemberId());
            i.setNombre(WordUtils.capitalizeFully(legacy.getNombre()));
            i.setStatus(InvitationStatus.created.name());
            i.setStatusText("Creada localmente");
            i.setMember(false);
            i.setConfirmed(false);

            invitationRepository.save(i);

            String path = createPdf(i);
            if (path == null) {
                continue;
            }
            i.setFilePath(path);
            try {
                invitationRepository.save(i);
            } catch (Exception e) {
                log.error("error", e);
            }
            created++;
        }

        List<MemberOld> oldies = membersOldRepository.findAll();
        for (MemberOld member : oldies) {
            if (member.isInvitationConfirmed()) {

                List<Invitation> already = invitationRepository.findByNombreAndApellidoAndMemberId(WordUtils.capitalizeFully(member.getFirstName().trim()),
                        WordUtils.capitalizeFully(member.getLastName().trim()),
                        member.getMemberId());

                if (!already.isEmpty()) {
                    if (already.size() > 1) {
                        for (Invitation invitation : already) {
                            invitation.setStatus(InvitationStatus.review.name());
                            invitationRepository.save(invitation);
                            log.error("Duplicado " + invitation.getFullName());
                        }
                    }
                    continue;
                }

                Invitation i = new Invitation();
                i.setApellido(WordUtils.capitalizeFully(member.getLastName().trim()));
                i.setDni(member.getDniType());
                i.setDniValue(member.getDni());
                i.setEmail(member.getEmail());
                i.setMemberId(member.getMemberId());
                i.setNombre(WordUtils.capitalizeFully(member.getFirstName().trim()));
                i.setStatus(InvitationStatus.created.name());
                i.setStatusText("Creada localmente");
                //i.setToken(InvoiceUtils.getNumberRandomString(10));
                i.setMember(true);
                i.setConfirmed(Boolean.TRUE);
                invitationRepository.save(i);

                String path = createPdf(i);
                if (path == null) {
                    continue;
                }

                i.setFilePath(path);
                try {
                    invitationRepository.save(i);
                } catch (Exception e) {
                    log.error("error", e);
                }
                created++;
            }
        }

        return "Invitaciones creadas " + created;
    }

    public String createPdf(Invitation i) {

        String background = "/var/wings/background.png";
        String jasper = "/var/wings/invitacion.jasper";
        String ret = null;

        String destName = "/var/wings/invitation/" + i.getId() + ".pdf";
        Map<String, Object> map = new HashMap<>();

        JREmptyDataSource empty = new JREmptyDataSource();
        map.put(JRParameter.REPORT_LOCALE, new Locale("es", "ES"));
        map.put("nombre", i.getFullName());
        map.put("numero", i.getId() + "");
        map.put("background", background);
        map.put("codigo", DigestUtils.md5Hex(map.get("nombre") + "-" + i.getToken()));
        try {

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(jasper));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, empty);
            JasperExportManager.exportReportToPdfFile(jprint, destName);
            ret = destName;
        } catch (Exception e) {
            log.error("Error creating invoice", e);
        }

        return ret;
    }

    public String sendInvitations() {

        List<Invitation> invitations = invitationRepository.findByStatus(InvitationStatus.created.name());
        int total = invitations.size();
        int sent = 0;
        int error = 0;
        for (Invitation i : invitations) {

            String email;
            if (i.getEmail() == null || i.getEmail().trim().isEmpty()) {
                long id = i.getMemberId();
                MemberOld old = membersOldRepository.findOne(id);
                email = old.getEmail();
            } else {
                email = i.getEmail();
            }

            List<File> attachements = new ArrayList<>();
            attachements.add(new File(i.getFilePath()));
            String body = emailService.getEventEmail("", "Invitación Evento 9 de Diciembre",
                    "<p>Apreciado " + i.getFullName() + ", tenemos el honor de invitarlo/a el dia 9 de diciembre al WINGS MOBILE AWARDS 2017</p><br/>"
                    + "El evento tendrá lugar en el Teatro Goya, en la Calle Sepúlveda, 3-5, 28011 Madrid.<br/><br/>"
                    + "Horario de inicio : 17.00 Horas<br/>"
                    + "Code Dress: Elegant<br/><br/>"
                    + "Recomendamos confirmar presencia enviando un mail a eventos@wingsmobile.es<br/><br/>"
                    + "<br/>");

            boolean success = emailService.sendMail("Eventos<eventos@wingsmobile.es>", email, "Invitación Evento 9 de Diciembre", body, attachements);

            if (success) {
                i.setSendDate(new Date());
                i.setStatus(InvitationStatus.sended.name());
                invitationRepository.save(i);
                sent++;
            } else {
                error++;
            }
        }

        return "Enviados " + sent + " de " + total + " errores " + error;
    }

    public String sendInvitation(Long invId) {

        List<Invitation> invitations = new ArrayList<>();
        invitations.add(invitationRepository.findOne(invId));

        int total = invitations.size();
        int sent = 0;
        int error = 0;
        for (Invitation i : invitations) {

            String email;
            if (i.getEmail() == null || i.getEmail().trim().isEmpty()) {
                long id = i.getMemberId();
                MemberOld old = membersOldRepository.findOne(id);
                email = old.getEmail();
            } else {
                email = i.getEmail();
            }

            List<File> attachements = new ArrayList<>();
            attachements.add(new File(i.getFilePath()));
            String body = emailService.getEventEmail("", "Invitación Evento 9 de Diciembre",
                    "<p>Apreciado " + i.getFullName() + ", tenemos el honor de invitarlo/a el dia 9 de diciembre al WINGS MOBILE AWARDS 2017</p><br/>"
                    + "El evento tendrá lugar en el Teatro Goya, en la Calle Sepúlveda, 3-5, 28011 Madrid.<br/><br/>"
                    + "Horario de inicio : 17.00 Horas<br/>"
                    + "Code Dress: Elegant<br/><br/>"
                    + "Recomendamos confirmar presencia enviando un mail a eventos@wingsmobile.es<br/><br/>"
                    + "<br/>");

            boolean success = emailService.sendMail("Eventos<eventos@wingsmobile.es>", email, "Invitación Evento 9 de Diciembre", body, attachements);

            if (success) {
                i.setSendDate(new Date());
                i.setStatus(InvitationStatus.sended.name());
                invitationRepository.save(i);
                sent++;
            } else {
                error++;
            }
        }

        return "Enviados " + sent + " de " + total + " errores " + error;
    }

    public String checkInvitation(Long invId) {
        Invitation i = invitationRepository.findOne(invId);
        i.setConfirmed(Boolean.TRUE);
        invitationRepository.save(i);
        return "Confirmada";
    }
    
    public String markPresentInvitation(Long invId) {
        Invitation i = invitationRepository.findOne(invId);
        i.setChecked(Boolean.TRUE);
        i.setCheckedDate(new Date());        
        invitationRepository.save(i);
        return "Marcada como Presente";
    }
    
    public String validateInvitation(Long invId) {
        Invitation i = invitationRepository.findOne(invId);
        i.setStatus("created");        
        invitationRepository.save(i);
        return "Validada";
    }

    @Transactional
    public void deleteInvitation(Long id) {
        invitationRepository.delete(id);
    }

}
