/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.Member;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface MembersRepository extends JpaRepository<Member, Long>, JpaSpecificationExecutor<Member> {

    List<Member> findByEmail(String email);

    Member findFirstByEmail(String email);

    Member findByUsername(String username);

    Member findByUsernameAndLoginEnabled(String username, Boolean loginEnabled);

    Member findByEmailAndLoginEnabled(String username, Boolean loginEnabled);

    Page<Member> findBySponsorIdAndDeleted(Long sponsorId, Boolean deleted, Pageable page);

    List<Member> findBySource(String source);

    List<Member> findByDni(String dni);

    List<Member> findByDeleted(Boolean deleted);

    Long countBySponsorIdAndRegistrationDateLessThanEqual(Long sponsorId, Date registrationDate);

    Long countByDeleted(Boolean deleted);

    Long countByOrderStatus(String status);

    List<Member> findByCountry(String country);

    List<Member> findByBwnClaimGreaterThanAndBwnSent(Integer bwnClaim, boolean bwnSent);

    //@Query(value = "select id from Member where sponsorId = ?1 and deleted = false")
    @Query(value = "select id from Member where sponsorId = ?1")
    List<Long> getChildIds(Long sponsorId);

    @Query(value = "select count(1) from Member m where m.deleted = 0 and m.country = ?1 and m.order.status= 'paid' and m.order.deleted = 0 ")
    long countPaidMembersByCountry(String country);

}
