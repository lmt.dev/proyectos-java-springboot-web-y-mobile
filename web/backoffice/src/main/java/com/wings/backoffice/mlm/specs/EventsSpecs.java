/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.events.Event;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class EventsSpecs {

    public static Specification<Event> eventSearch(String q, String status) {

        return (Root<Event> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                //if it's numeric, serach only by id
                try {
                    Long id = Long.parseLong(q);
                    if (id < 100000) {
                        predicates.add(builder.equal(root.get("id"), id));
                        return builder.and(predicates.toArray(new Predicate[]{}));
                    }
                } catch (Exception e) {
                }

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search),
                                builder.like(root.get("shortName"), search)
                        )
                );
            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (status == null) {
                predicates.add(builder.equal(root.get("status"), "finalized").not());
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
