package com.wings.backoffice.web.admin.reports;

import com.wings.backoffice.mlm.dao.ReportDto;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.ReportService;
import com.wings.backoffice.services.promos.PromoLatamService;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class OrderReportsController {

    @Autowired
    private ReportService reportService;
    @Autowired
    private PromoLatamService promoLatamService;
    @Autowired
    private CountriesService countriesService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/admin/reports/orders"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {

        String syear = request.getParameter("year");
        String smonth = request.getParameter("month");
        String scountry = request.getParameter("country");
        String scurrency = request.getParameter("currency");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = countriesService.getCountriesIsos();
        if (scountry != null && !scountry.equals("all")) {
            countries = Arrays.asList(scountry);
        }

        List<Integer> months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        if (smonth != null && !smonth.equals("-1")) {
            months = Arrays.asList(Integer.valueOf(smonth));
        }

        List<Integer> years = Arrays.asList(2017, 2018, 2019, 2020, 2021);
        if (syear != null && !syear.equals("-1")) {
            years = Arrays.asList(Integer.valueOf(syear));
        }

        String currency = null;
        if (scurrency != null && !scurrency.equals("all")) {
            currency = scurrency;
        }
        Date f = null, t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);

                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        } else if (from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getOrdersByCountryDto(countries, months, years, f, t, currency);

        model.addAttribute("countries", reportDto.getCountryDtos());
        model.addAttribute("country", scountry);
        model.addAttribute("month", smonth);
        model.addAttribute("months", months);
        model.addAttribute("year", syear);
        model.addAttribute("years", years);
        model.addAttribute("currency", scurrency);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "admin/reports/orders/ordersByCountry";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/reports/promoLatam"}, method = RequestMethod.GET)
    public String getPromoLatam(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String country = request.getParameter("country");
        String qualify = request.getParameter("qualify");

        if (qualify != null && qualify.equals("all")) {
            qualify = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Boolean bq = qualify == null ? null : qualify.equals("true");
        Page<PromoLatam> items = promoLatamService.getPromoLatam(q, page, size, country, bq);
        /*if (!items.hasContent()) {
            promoLatamService.listMembersWingsLatamPromo();
            items = promoLatamService.getPromoLatam(null, 0, 0, null, Boolean.FALSE);
        }*/

        PageWrapper<PromoLatam> pageWrapper = new PageWrapper<>(items, "admin/reports/promoLatam");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("size", size);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("country", country);
        model.addAttribute("qualify", qualify);

        return "admin/reports/promo/promoLatam";
    }

    @RequestMapping(value = {"admin/reports/promoLatam"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postGenerateReport(Model model, HttpServletRequest request) {
        promoLatamService.listMembersWingsLatamPromo();
        return ResponseEntity.ok("Generado!");
    }

    private String getMapText(Map<String, Double> extras, String currency, String country) {

        StringBuilder ret = new StringBuilder();

        for (Map.Entry<String, Double> entry : extras.entrySet()) {
            String key = entry.getKey();
            Double amount = entry.getValue();

            String amountText;
            if (currency != null) {
                amountText = NumberUtils.formatMoney(amount, currency);
            } else {
                amountText = NumberUtils.formatMoneyForCountryIso(amount, country);
            }
            ret.append("</td><td>").append(amountText);

        }
        return ret.toString();
    }

    private List<Integer> getYearsBetween(Date from, Date to) {
        final List<Integer> years = new ArrayList<>();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        int f = cal.get(Calendar.YEAR);
        years.add(f++);
        cal.setTime(to);
        final int t = cal.get(Calendar.YEAR);
        for (; f <= t; f++) {
            years.add(f);
        }

        return years;
    }

    @RequestMapping(value = {"admin/reports/orders/viewDetails"}, method = RequestMethod.GET)
    public String viewReportDetails(Model model, HttpServletRequest request) {
        String country = request.getParameter("country");
        String year = request.getParameter("year");
        String month = request.getParameter("month");
        String currency = request.getParameter("currency");
        String action = request.getParameter("action");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        List<String> countries = new ArrayList<>();
        countries.add(country);
        List<Integer> years = new ArrayList<>();
        years.add(Integer.valueOf(year));
        List<Integer> months = new ArrayList<>();
        months.add(Integer.valueOf(month));
        if (currency != null && (currency.equals("all") || currency.equals("undefined"))) {
            currency = null;
        }

        Date f = null, t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);

                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        } else if (from != null) {
            t = new Date();
            DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            to = format2.format(t);
            t = DateUtils.getLastHourOfDay(t);
        }

        if (f != null) {
            years = getYearsBetween(f, t);
            months = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        }

        ReportDto reportDto = reportService.getOrdersByCountryDto(countries, months, years, f, t, currency);
        model.addAttribute("orders", reportDto.getCountryDtos().get(0));
        model.addAttribute("action", action);
        model.addAttribute("country", country);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("smonth", DateUtils.getMonthName(Integer.valueOf(month)));
        model.addAttribute("currency", currency);

        return "admin/reports/orders/viewDetails";

    }
}
