package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItemImage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author lucas
 */
public interface StoreItemImagesRepository extends JpaRepository<StoreItemImage, Long> {

    List<StoreItemImage> findByStoreItemId(Long storeItemId);

}
