/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.CorpoContent;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class CorpoContentSpecs {

    public static Specification<CorpoContent> search(String q, String rank, String country, Boolean published, String type) {

        return (Root<CorpoContent> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            List<Predicate> predicatesOr = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("title"), search),
                                builder.like(root.get("description"), search),
                                builder.like(root.get("pathFile"), search)
                        )
                );
            }

            if (rank != null) {

                if (rank.contains(",")) {
                    String[] ranks = rank.split(",");
                    for (String r : ranks) {
                        String search = "%" + r + "%";
                        predicatesOr.add(builder.like(root.get("rankAvailable"), search));
                    }
                } else {
                    String search = "%" + rank + "%";
                    predicates.add(builder.like(root.get("rankAvailable"), search));
                }

            }

            if (country != null) {

                if (country.contains(",")) {
                    String[] countries = country.split(",");
                    for (String c : countries) {
                        String search = "%" + c + "%";
                        predicatesOr.add(builder.like(root.get("countryAvailable"), search));
                    }
                } else {
                    String search = "%" + country + "%";
                    predicates.add(builder.like(root.get("countryAvailable"), search));
                }

            }

            if (published != null) {
                predicates.add(builder.equal(root.get("published"), published));
            }

            if (type != null) {
                String search = "%" + type + "%";
                predicates.add(builder.like(root.get("mimeType"), search));
            }

            //Add Or predicates
            if(!predicatesOr.isEmpty()){
                Predicate predicateOr = builder.or(predicatesOr.toArray(new Predicate[]{}));
                predicates.add(predicateOr);
            }
            
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
