/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis;

import com.wings.backoffice.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_ranks")
public class RankBis implements Serializable{

    private static final long serialVersionUID = -2915953241804076239L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer orderIndex;
    private String name;
    private Long directRequired;
    private Long pvRequired;
    private Long gvRequired;
    private Long quantity;
    private Long sbQuantity;
    private Long percentage;
    private String image;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(Integer orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDirectRequired() {
        return directRequired;
    }

    public void setDirectRequired(Long directRequired) {
        this.directRequired = directRequired;
    }

    public Long getPvRequired() {
        return pvRequired;
    }

    public void setPvRequired(Long pvRequired) {
        this.pvRequired = pvRequired;
    }

    public Long getGvRequired() {
        return gvRequired;
    }

    public void setGvRequired(Long gvRequired) {
        this.gvRequired = gvRequired;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getSbQuantity() {
        return sbQuantity;
    }

    public void setSbQuantity(Long sbQuantity) {
        this.sbQuantity = sbQuantity;
    }

    public Long getPercentage() {
        return percentage;
    }

    public void setPercentage(Long percentage) {
        this.percentage = percentage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public String getRankName(){
        return StringUtils.capitalize(name.toLowerCase().replaceAll("_", " "));
    }
}
