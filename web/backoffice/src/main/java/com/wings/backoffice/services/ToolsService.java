/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.services.promos.PromoLatamService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ToolsService {

    @Autowired
    private CalificationService calificationService;
    @Autowired
    private ResidualServicesService residualServicesService;
    @Autowired
    private RanksService ranksService;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private PromoLatamService promoLatamService;
    @Autowired
    private OrdersService ordersService;

    private boolean running = false;

    private final Logger log = LoggerFactory.getLogger(ToolsService.class);

    @Scheduled(cron = "0 0 6 * * *", zone = "Europe/Madrid")
    public synchronized void recalculate() {

        if (!running) {
            long time = System.currentTimeMillis();
            log.error("Start recalculate all");
            running = true;
            try {
                calificationService.parse();
                //residualServicesService.importGsmLineServices();
                //ranksService.analizeRanks();
                bonusService.calculateDifferential();
                balanceService.processBalance();
                balanceService.activateProductsAndKitsBalances();
                membersService.postImportMembers();

                promoLatamService.listMembersWingsLatamPromo();
                ordersService.markAsInstallmentComplete();

            } catch (Exception e) {
                log.error("Error on recalculate ", e);
            }
            running = false;

            long diff = System.currentTimeMillis() - time;
            Date date = new Date(diff);
            DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateFormatted = formatter.format(date);

            log.error("Done recalculate all - took " + dateFormatted);
        }
    }

    public boolean isRunning() {
        return running;
    }

}
