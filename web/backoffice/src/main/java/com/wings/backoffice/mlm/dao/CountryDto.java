//COUNTRY DTO
package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.mlm.domain.bis.orders.MembersView;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author lucas
 */
public class CountryDto {

    private String country;
    private Integer year;
    private List<OrderDto> orders;
    private Map<Integer, Map<String, Map<String, Integer>>> productsComplete;
    private Map<Integer, Map<String, Map<String, Integer>>> productsPartial;
    private List<MembersView> members;
    private String currency;

    public CountryDto() {
        this(null, null, null, null);
    }

    public CountryDto(String country) {
        this.country = country;
    }

    public CountryDto(String country, List<OrderDto> orderDtos, String currency, Map<Integer, Map<String, Map<String, Integer>>> products) {
        this.country = country;
        this.orders = orders != null ? orders : new ArrayList<>();
        this.productsComplete = products != null ? products : new HashMap<>();
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDto> orders) {
        this.orders = orders;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
        orders.forEach(order -> order.setCurrency(currency));
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<OrderDto> getOrdersByYear(Integer year) {
        return orders.stream().filter(order -> order.getYear().equals(year)).collect(Collectors.toList());
    }

    public List<OrderDto> getOrdersByCreationYear(Integer year) {
        return orders.stream().filter(order -> order.getCreationYear().equals(year)).collect(Collectors.toList());
    }

    public Double getTotal() {
        Double total = 0D;
        total = orders.stream().map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalBacs() {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("bacs")).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalComs() {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("coms")).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalOthers() {
        Double total = 0D;
        total = orders.stream().filter(order -> !order.getPaymentType().equals("bacs") && !order.getPaymentType().equals("coms")).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalByMonth(Integer month, Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getMonth().equals(month) && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalBacsByMonth(Integer month, Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("bacs") && order.getMonth().equals(month) && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalComsByMonth(Integer month, Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("coms") && order.getMonth().equals(month) && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalOthersByMonth(Integer month, Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> !order.getPaymentType().equals("bacs") && !order.getPaymentType().equals("coms") && order.getMonth().equals(month) && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalByYear(Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalBacsByYear(Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("bacs") && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalComsByYear(Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> order.getPaymentType().equals("coms") && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public Double getTotalOthersByYear(Integer year) {
        Double total = 0D;
        total = orders.stream().filter(order -> !order.getPaymentType().equals("bacs") && !order.getPaymentType().equals("coms") && order.getYear().equals(year)).map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    public String getTotalStr() {
        Double total = getTotal();
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalBacsStr() {
        Double total = getTotalBacs();
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalComsStr() {
        Double total = getTotalComs();
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalOthersStr() {
        Double total = getTotalOthers();
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalByMonthStr(Integer month, Integer year) {
        Double total = getTotalByMonth(month, year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalBacsByMonthStr(Integer month, Integer year) {
        Double total = getTotalBacsByMonth(month, year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalComsByMonthStr(Integer month, Integer year) {
        Double total = getTotalComsByMonth(month, year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalOthersByMonthStr(Integer month, Integer year) {
        Double total = getTotalOthersByMonth(month, year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalByYearStr(Integer year) {
        Double total = getTotalByYear(year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalBacsByYearStr(Integer year) {
        Double total = getTotalBacsByYear(year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalComsByYearStr(Integer year) {
        Double total = getTotalComsByYear(year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public String getTotalOthersByYearStr(Integer year) {
        Double total = getTotalOthersByYear(year);
        return currency != null ? NumberUtils.formatMoney(total, currency) : NumberUtils.formatMoneyForCountryIso(total, country);
    }

    public void addOrders(List<OrderDto> orders) {
        this.orders.addAll(orders);
    }

    public List<OrderDto> getOrdersBy(Integer year, Integer month) {
        return orders.stream().filter(order -> order.getYear().equals(year) && order.getMonth().equals(month)).collect(Collectors.toList());
    }

    public List<OrderDto> getOrdersBy(Integer year, Integer month, String type) {
        if (null == type) {
            return orders.stream().filter(order -> order.getYear().equals(year) && order.getMonth().equals(month)).collect(Collectors.toList());
        } else {
            switch (type) {
                case "all":
                    return orders.stream().filter(order -> order.getYear().equals(year) && order.getMonth().equals(month)).collect(Collectors.toList());
                case "bacs":
                case "coms":
                    return orders.stream().filter(order -> order.getYear().equals(year) && order.getMonth().equals(month) && order.getPaymentType().equals(type)).collect(Collectors.toList());
                default:
                    return orders.stream().filter(order -> order.getYear().equals(year) && order.getMonth().equals(month)
                            && !order.getPaymentType().equals("bacs") && !order.getPaymentType().equals("coms")).collect(Collectors.toList());
            }
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos para reporte de productos">
    public Map<Integer, Map<String, Map<String, Integer>>> getProductsComplete() {
        return productsComplete;
    }

    public void setProductsComplete(Map<Integer, Map<String, Map<String, Integer>>> products) {
        this.productsComplete = products;
    }
    
    public Map<Integer, Map<String, Map<String, Integer>>> getProductsPartial() {
        return productsPartial;
    }

    public void setProductsPartial(Map<Integer, Map<String, Map<String, Integer>>> products) {
        this.productsPartial = products;
    }

//    public void addProducts(Map<Integer, Map<String, Map<String, Integer>>> prod) {
//        this.products = prod;
//    }

    public Integer getTotalProductsCompleteByYear(Integer year) {
        Integer total = 0;
        if (productsComplete.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsComplete.get(year);
            Set<String> keySet = months.keySet();
            for (String string : keySet) {
                Map<String, Integer> productos = months.get(string);
                total = productos.keySet().stream().map((producto) -> productos.get(producto)).map((cantidad) -> cantidad).reduce(total, Integer::sum);
            }
        }

        return total;
    }
    
    public Integer getTotalProductsPartialByYear(Integer year) {
        Integer total = 0;
        if (productsPartial.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsPartial.get(year);
            Set<String> keySet = months.keySet();
            for (String string : keySet) {
                Map<String, Integer> productos = months.get(string);
                total = productos.keySet().stream().map((producto) -> productos.get(producto)).map((cantidad) -> cantidad).reduce(total, Integer::sum);
            }
        }

        return total;
    }

    public Integer getTotalProductsCompleteByYearAndProduct(Integer year, String producto) {
        Integer total = 0;
        if (productsComplete.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsComplete.get(year);
            total = months.keySet().stream().map((month) -> months.get(month)).map((productos) -> productos.get(producto)).map((cantidad) -> cantidad).reduce(total, Integer::sum);
        }
        return total;
    }
    
    public Integer getTotalProductsPartialByYearAndProduct(Integer year, String producto) {
        Integer total = 0;
        if (productsPartial.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsPartial.get(year);
            total = months.keySet().stream().map((month) -> months.get(month)).map((productos) -> productos.get(producto)).map((cantidad) -> cantidad).reduce(total, Integer::sum);
        }
        return total;
    }

    public Integer getTotalProductsCompleteByYearAndMonth(Integer year, Integer smonth, String producto) {
        Integer total = 0;
        String month = DateUtils.getMonthName(smonth);
        if (productsComplete.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsComplete.get(year);
            if (months.containsKey(month)) {
                Map<String, Integer> productos = months.get(month);
                if (productos.containsKey(producto)) {
                    Integer cantidad = productos.get(producto);
                    total += cantidad != null ? cantidad : 0;
                }
            }
        }

        return total;
    }
    
    public Integer getTotalProductsPartialByYearAndMonth(Integer year, Integer smonth, String producto) {
        Integer total = 0;
        String month = DateUtils.getMonthName(smonth);
        if (productsPartial.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsPartial.get(year);
            if (months.containsKey(month)) {
                Map<String, Integer> productos = months.get(month);
                if (productos.containsKey(producto)) {
                    Integer cantidad = productos.get(producto);
                    total += cantidad != null ? cantidad : 0;
                }
            }
        }

        return total;
    }

    public Integer getTotalProductsCompleteByYearAndMonth(Integer year, Integer smonth) {
        Integer total = 0;
        String month = DateUtils.getMonthName(smonth);
        if (productsComplete.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsComplete.get(year);
            if (months.containsKey(month)) {
                Map<String, Integer> productos = months.get(month);
                total = productos.keySet().stream().map((string) -> productos.get(string)).map((cantidad) -> cantidad != null ? cantidad : 0).reduce(total, Integer::sum);
            }
        }

        return total;
    }
    
    public Integer getTotalProductsPartialByYearAndMonth(Integer year, Integer smonth) {
        Integer total = 0;
        String month = DateUtils.getMonthName(smonth);
        if (productsPartial.containsKey(year)) {
            Map<String, Map<String, Integer>> months = productsPartial.get(year);
            if (months.containsKey(month)) {
                Map<String, Integer> productos = months.get(month);
                total = productos.keySet().stream().map((string) -> productos.get(string)).map((cantidad) -> cantidad != null ? cantidad : 0).reduce(total, Integer::sum);
            }
        }

        return total;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos para reporte de afiliados">
    public List<MembersView> getMembers() {
        return members;
    }

    public void setMembers(List<MembersView> members) {
        this.members = members;
    }

    public List<MembersView> getMembersBy(Integer year, Integer month, Boolean active) {
        List<MembersView> collect = members.stream().filter(member -> member.getYear(active).equals(year) && member.getMonth(active).equals(month)).collect(Collectors.toList());
        return collect;
    }

    public Integer getMembersByActiveAndYearAndMonth(Integer year, Integer month, Boolean active) {
        Integer total = 0;
        for (MembersView member : members) {
            if (active && member.getActivationDate() != null && member.getYear(active).equals(year) && member.getMonth(active).equals(month)) {
                total++;
            } else if (!active && member.getRegistrationDate() != null && member.getYear(active).equals(year) && member.getMonth(active).equals(month)) {
                total++;
            }
        }
        return total;
    }

    public Integer getMembersByActiveAndYear(Integer year, Boolean active) {
        Integer total = 0;
        for (MembersView member : members) {
            if (active && member.getActivationDate() != null && member.getYear(active).equals(year)) {
                total++;
            } else if (!active && member.getRegistrationDate() != null && member.getYear(active).equals(year)) {
                total++;
            }
        }
        return total;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos para reporte de ordenes pendientes">
    public List<OrderDto> getOrdersPendingBy(Long itemId, Integer year) {
        if(itemId != null){
            return orders.stream().filter(order -> order.getItemId().equals(itemId) && order.getCreationYear().equals(year)).collect(Collectors.toList());
        }else{
            return orders.stream().filter(order -> order.getCreationYear().equals(year)).collect(Collectors.toList());
        }
    }

    public String getTotalPending(Long itemId, Integer year) {
        List<OrderDto> pendings = getOrdersPendingBy(itemId, year);
        return getFormatTotal(pendings);
    }

    public String getTotalPending(Integer year) {
        List<OrderDto> pendings = getOrdersPendingBy(null, year);
        return getFormatTotal(pendings);
    }
    
    public Double getTotalPending(List<OrderDto> orders) {
        Double total = 0.0;
        total = orders.stream().map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }

    private String getFormatTotal(List<OrderDto> pendings) {
        int count = pendings.size();
        Double total = 0.0;
        total = pendings.stream().map((order) -> order.getPaymentAmount()).reduce(total, (accumulator, _item) -> accumulator + _item);
        
        if (currency != null) {
            return NumberUtils.formatMoney(total, currency) + " (" + count + ")";
        } else {
            return NumberUtils.formatMoneyForCountryIso(total, country) + " (" + count + ")";
        }
    }//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Métodos para reporte de founders">
    public Integer getTotalFounderByMonthStr(Integer month, Integer year, String action, String founder){
        int count = 0;
        List<OrderDto> toFind = new ArrayList<>();
        
        if(action.equals("total")){
            return orders.stream().filter((OrderDto order) -> (order.getYear().equals(year) && order.getMonth().equals(month) && order.getFounder().equals(founder))).map((_item) -> 1).reduce(count, Integer::sum);
        }else{
            return orders.stream().filter((OrderDto order) -> (order.getYear().equals(year) && order.getMonth().equals(month)
                    && order.getFounder().equals(founder) && order.getMark().equals(action))).map((_item) -> 1).reduce(count, Integer::sum);
        }
    }
    
    public Integer getTotalFounderByMonthStr(Integer year, String action, String founder){
        int count = 0;
        List<OrderDto> toFind = new ArrayList<>();
        
        if(action.equals("total")){
            return orders.stream().filter((OrderDto order) -> (order.getYear().equals(year) && order.getFounder().equals(founder))).map((_item) -> 1).reduce(count, Integer::sum);
        }else{
            return orders.stream().filter((OrderDto order) -> (order.getYear().equals(year)
                    && order.getFounder().equals(founder) && order.getMark().equals(action))).map((_item) -> 1).reduce(count, Integer::sum);
        }
    }

}
