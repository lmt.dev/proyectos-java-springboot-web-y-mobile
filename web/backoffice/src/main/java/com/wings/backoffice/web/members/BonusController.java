/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.dao.DifferentialDAO;
import com.wings.backoffice.mlm.dao.DifferentialSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.Differential;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.specs.DifferentialSpecs;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.backoffice.mlm.repos.DifferentialRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.specs.BalanceSpecs;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.utils.CountryUtils;
import java.util.Locale;

/**
 *
 * @author seba
 */
@Controller("bonusMemberController")
public class BonusController {

    @Autowired
    private DifferentialRepository differentialBisRepository;

    @Autowired
    private DifferentialDAO differentialDAO;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceDAO balanceDAO;

    @Autowired
    private MembersService membersService;

    @Autowired
    private MembersRepository membersRepository;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/members/bonus/differential"}, method = RequestMethod.GET)
    public String indexMember(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersRepository.findOne(member.getId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String offer = request.getParameter("offer");
        String receive = request.getParameter("receive");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        Sort.Order dateSort = new Sort.Order(Sort.Direction.ASC, "date");
        Sort.Order offerUserIdsort = new Sort.Order(Sort.Direction.DESC, "offerUserId");
        PageRequest pr = new PageRequest(page, size, new Sort(dateSort, offerUserIdsort));

        Page<Differential> items = differentialBisRepository.findAll(DifferentialSpecs.differentialSearchMember(offer, member.getId(), f, t, false), pr);
        DifferentialSums sums = differentialDAO.findMember(offer, member.getId(), f, t, false);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        sums.setLocale(locale);
        for (Differential item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Differential> pageWrapper = new PageWrapper<>(items, "members/bonus/differential");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("offer", offer);
        model.addAttribute("receive", receive);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("sums", sums);
        model.addAttribute("size", ssize);

        return "members/bonus/differential";
    }

    @RequestMapping(value = {"/members/bonus/differential/event/{id}"}, method = RequestMethod.GET)
    public String viewEventMember(Model model, HttpServletRequest request, @PathVariable Long id) {

        Member m = AuthUtils.getMemberUser();
        m = membersRepository.findOne(m.getId());
        Long receiveUserId = m.getId();
        List<Long> ids = membersService.getChildIds(receiveUserId);

        List<Differential> diff = differentialBisRepository.findByEventIdAndReceiveUserIdInAndMatchingOrderByReceiveUserIdDesc(id, ids, false);
        List<Differential> matchs = differentialBisRepository.findByEventIdAndReceiveUserIdInAndMatchingOrderByReceiveUserIdDesc(id, ids, true);
        model.addAttribute("diffs", diff);
        model.addAttribute("matchs", matchs);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        diff.forEach((item) -> {
            item.setLocale(locale);
        });
        matchs.forEach((item) -> {
            item.setLocale(locale);
        });

        return "members/bonus/differential_event";
    }

    @RequestMapping(value = {"/members/bonus/matching"}, method = RequestMethod.GET)
    public String getMatchingMember(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersRepository.findOne(member.getId());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String offer = request.getParameter("offer");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));

        Page<Differential> items = differentialBisRepository.findAll(DifferentialSpecs.differentialSearchMember(offer, member.getId(), f, t, true), pr);
        DifferentialSums sums = differentialDAO.findMember(offer, member.getId(), f, t, true);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        sums.setLocale(locale);
        for (Differential item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Differential> pageWrapper = new PageWrapper<>(items, "members/bonus/matching");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("offer", offer);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("sums", sums);
        model.addAttribute("size", ssize);

        return "members/bonus/matching";
    }

    @RequestMapping(value = {"/members/bonus/direct-indirect"}, method = RequestMethod.GET)
    public String getDirectIndirect(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        m = membersRepository.findOne(m.getId());

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(m.getUsername(), f, t, OrderType.product.name(), type, m.getId(), null), pr);
        BalanceSums sums = balanceDAO.find(m.getUsername(), f, t, OrderType.product.name(), type, m.getId(), null);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        sums.setLocale(locale);
        for (Balance item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/members/bonus/direct-indirect");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);
        model.addAttribute("type", type);

        return "members/bonus/direct-indirect";
    }

    @RequestMapping(value = {"/members/bonus/residual"}, method = RequestMethod.GET)
    public String getResidual(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        m = membersRepository.findOne(m.getId());

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(m.getUsername(), f, t, OrderType.service.name(), null, m.getId(), null), pr);
        BalanceSums sums = balanceDAO.find(m.getUsername(), f, t, OrderType.service.name(), null, m.getId(), null);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        sums.setLocale(locale);
        for (Balance item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/members/bonus/residual");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);

        return "members/bonus/residual";
    }

    @RequestMapping(value = {"/members/bonus/speed"}, method = RequestMethod.GET)
    public String viewSpeedBonus(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        m = membersRepository.findOne(m.getId());

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "creationDate"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.search(null, f, t, "speed", null, m.getId(), null), pr);
        BalanceSums sums = balanceDAO.find(null, f, t, "speed", null, m.getId(), null);

        //set locale for currency conversion
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        sums.setLocale(locale);
        for (Balance item : items) {
            item.setLocale(locale);
        }

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "/members/bonus/speed");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);

        return "members/bonus/speed";
    }

}
