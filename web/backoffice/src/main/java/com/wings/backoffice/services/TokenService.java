/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Token;
import com.wings.backoffice.mlm.enums.TokenType;
import com.wings.backoffice.mlm.repos.TokenRepository;
import com.wings.backoffice.utils.InvoiceUtils;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Transactional
    public Token generateRecoverPasswordToken(Member member) {

        List<Token> tokens = tokenRepository.findByReferenceIdAndTokenType(member.getId(), TokenType.recover_password.name());
        tokenRepository.delete(tokens);

        Date expire = new Date(System.currentTimeMillis() + 86400000l);

        Token t = new Token();
        t.setActive(Boolean.TRUE);
        t.setCreationDate(new Date());
        t.setExpireDate(expire);
        t.setReferenceId(member.getId());
        t.setTokenType(TokenType.recover_password.name());

        //check for duplicates 
        Token already;
        do {
            t.setToken(InvoiceUtils.getRandomString(128));
            already = tokenRepository.findByToken(t.getToken());
        } while (already != null);

        tokenRepository.save(t);

        return t;

    }

    public boolean isTokenExpired(Token token) {
        return new Date().after(token.getExpireDate());
    }

    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public void setTokenInactive(Token t) {
        t.setActive(false);
        tokenRepository.save(t);
    }
}
