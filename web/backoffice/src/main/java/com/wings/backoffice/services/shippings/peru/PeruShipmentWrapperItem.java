/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.peru;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class PeruShipmentWrapperItem {

    @JsonProperty("referencia1")
    private String referencia1;
    @JsonProperty("referencia2")
    private String referencia2;
    @JsonProperty("referencia3")
    private String referencia3;
    @JsonProperty("referencia4")
    private String referencia4;

    public String getReferencia1() {
        return referencia1;
    }

    public void setReferencia1(String referencia1) {
        this.referencia1 = referencia1;
    }

    public String getReferencia2() {
        return referencia2;
    }

    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    public String getReferencia3() {
        return referencia3;
    }

    public void setReferencia3(String referencia3) {
        this.referencia3 = referencia3;
    }

    public String getReferencia4() {
        return referencia4;
    }

    public void setReferencia4(String referencia4) {
        this.referencia4 = referencia4;
    }
}
