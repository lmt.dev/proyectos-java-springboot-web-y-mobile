/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberNode;
import com.wings.backoffice.mlm.enums.OrderStatus;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author seba
 */
public class MemberUtils {

    /**
     * Returns a list of ids descendant from that member
     *
     * @param member
     * @return
     */
//    public static List<Long> getChildIds(Member member) {
//        Set<Long> set = new HashSet<>();
//        //first my id
//        set.add(member.getId());
//        if (member.getChildren() != null && !member.getChildren().isEmpty()) {
//            //then my children id
//            for (Member child : member.getChildren()) {
//                set.addAll(getChildIds(child));
//            }
//        }
//        List<Long> ids = new ArrayList<>(set);
//        return ids;
//    }

    /**
     * Returns a list of ids parents
     *
     * @param member
     * @return
     */
//    public static List<Long> getUplineIds(Member member) {
//        List<Long> ids = new LinkedList<>();
//        //first my id
//        if (member != null && member.getId() != null) {
//            ids.add(member.getId());
//            while ((member = member.getSponsor()) != null) {
//                ids.add(member.getId());
//            }
//        }
//        return ids;
//    }

    /**
     *
     * @param members
     * @return
     */
    public static List<MemberNode> buildNodes(List<Member> members) {

        List<MemberNode> m = new ArrayList<>();
        for (Member member : members) {

            try {
                if (member.getChildren() != null && !member.getChildren().isEmpty()) {
                    m.add(new MemberNode(buildName(member), buildNodes(member.getChildren())));
                } else {
                    m.add(new MemberNode(buildName(member), null));
                }
            } catch (Exception e) {
                System.out.println("Error for " + member.getId());
            }
        }
        return m;
    }

    /**
     *
     * @param member
     * @return
     */
    public static String buildName(Member member) {
        String order = (member.getOrder() != null ? member.getOrder().getOrderItem().getName() : "N/A");
        if (!member.getOrder().getStatus().equals(OrderStatus.paid.name())) {
            order += " (no pagado)";
        }
        return member.getId() + " - " + member.getFirstName() + " " + member.getLastName() + " (" + member.getRank().getName().toLowerCase() + ") - (" + member.getChildren().size() + ") - " + "(" + member.getIndirectCount() + ") - " + order;
    }

    /**
     * Check member profile is complete
     *
     * @param member
     * @return
     */
    public static boolean isProfileComplete(Member member) {
        //Si es administrador, no solicita completar datos del perfil
        if (AuthUtils.isAdmin()) {
            return true;
        } else {
            boolean ret = member.getFirstName() != null && !member.getFirstName().trim().isEmpty()
                    && member.getLastName() != null && !member.getLastName().trim().isEmpty()
                    && member.getDni() != null && !member.getDni().trim().isEmpty()
                    && member.getDniType() != null && !member.getDniType().trim().isEmpty()
                    && member.getIdt() != null && !member.getIdt().trim().isEmpty()
                    && member.getIdtType() != null && !member.getIdtType().trim().isEmpty()
                    && member.getCountry() != null && !member.getCountry().trim().isEmpty()
                    && member.getState() != null && !member.getState().trim().isEmpty()
                    && member.getCity() != null && !member.getCity().trim().isEmpty()
                    && member.getStreet() != null && !member.getStreet().trim().isEmpty()
                    && member.getEmail() != null && !member.getEmail().trim().isEmpty()
                    && member.getPostal() != null && !member.getPostal().trim().isEmpty()
                    && member.getPhone() != null && !member.getPhone().trim().isEmpty()
                    && member.getBirthDate() != null;

            if (member.getIsCompany()) {
                ret = ret
                        && member.getCompanyName() != null && !member.getCompanyName().trim().isEmpty()
                        && member.getCompanyDniType() != null && !member.getCompanyDniType().trim().isEmpty()
                        && member.getCompanyDni() != null && !member.getCompanyDni().trim().isEmpty();
            }
            return ret;
        }
    }
}
