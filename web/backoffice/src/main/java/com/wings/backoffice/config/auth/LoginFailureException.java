/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author seba
 */
public class LoginFailureException extends AuthenticationException {

    private static final long serialVersionUID = -8797070935538537137L;

    private String type;
    private String reference;
    private Long referenceId;
    private String comment;
    private String messageText;
    private String modUser;

    public LoginFailureException(String msg) {
        super(msg);
    }

    public LoginFailureException(String msg, Throwable t) {
        super(msg, t);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

}
