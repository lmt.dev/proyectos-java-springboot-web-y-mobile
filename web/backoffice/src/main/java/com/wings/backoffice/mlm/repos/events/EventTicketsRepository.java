/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.events;

import com.wings.backoffice.mlm.domain.events.EventTicket;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface EventTicketsRepository extends JpaRepository<EventTicket, Long> {

    Page<EventTicket> findAllByEventId(Long eventId, Pageable page);

    Page<EventTicket> findAllByEventIdAndStatus(Long eventId, String status, Pageable page);

    int countByEventIdAndStatus(Long eventId, String eventTicketStatus);

    int countByEventIdAndStatusNotIn(Long eventId, List<String> eventTicketStatus);

    int countByEventId(Long eventId);

    List<EventTicket> findByEventId(Long eventId);

    List<EventTicket> findByEventIdAndReclaimDateGreaterThan(Long eventId, Date reclaimDate);

    Page<EventTicket> findByEventIdAndStatus(Long eventId, String status, Pageable page);

    List<EventTicket> findByEventIdAndStatus(Long eventId, String status);

    Page<EventTicket> findByIdIn(List<Long> ticketIds, Pageable page);

    EventTicket findByTicketCode(String ticketCode);

    EventTicket findByEmailAndEventId(String email, Long eventId);

    EventTicket findByTicketNumber(String ticketNumber);

    EventTicket findByInviteCode(String inviteCode);

    List<EventTicket> findFirst10ByEventIdAndStatusAndEmailSent(Long eventId, String status, boolean emailSent);
    
    List<EventTicket> findFirst10ByStatusAndEmailSent(String status, boolean emailSent);

    @Transactional
    int deleteByEventId(Long eventId);

    EventTicket findFirstByEventIdOrderByIdDesc(Long eventId);

}
