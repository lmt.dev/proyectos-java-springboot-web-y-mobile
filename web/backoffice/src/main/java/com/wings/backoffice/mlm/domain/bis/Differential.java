/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Date;
import java.util.Locale;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_differential")
public class Differential {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double quantityRank;
    private Double quantityPay;
    private Long percentage;
    private Long receiveUserId;
    private Long receiveRankId;
    private Long offerUserId;
    private Long offerRankId;
    private String comment;
    private boolean matching;
    private Long eventId;
    private boolean locked;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @OneToOne
    @JoinColumn(name = "offerUserId", updatable = false, insertable = false)
    private Member offerUser;

    @OneToOne
    @JoinColumn(name = "receiveUserId", updatable = false, insertable = false)
    private Member receiveUser;

    @OneToOne
    @JoinColumn(name = "offerRankId", updatable = false, insertable = false)
    private RankBis offerRank;

    @OneToOne
    @JoinColumn(name = "receiveRankId", updatable = false, insertable = false)
    private RankBis receiveRank;

    @Transient
    private Locale locale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getQuantityRank() {
        return quantityRank;
    }

    public void setQuantityRank(Double quantityRank) {
        this.quantityRank = quantityRank;
    }

    public Double getQuantityPay() {
        return quantityPay;
    }

    public void setQuantityPay(Double quantityPay) {
        this.quantityPay = quantityPay;
    }

    public Long getPercentage() {
        return percentage;
    }

    public void setPercentage(Long percentage) {
        this.percentage = percentage;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public Long getOfferUserId() {
        return offerUserId;
    }

    public void setOfferUserId(Long offerUserId) {
        this.offerUserId = offerUserId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Member getOfferUser() {
        return offerUser;
    }

    public void setOfferUser(Member offerUser) {
        this.offerUser = offerUser;
    }

    public Member getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(Member receiveUser) {
        this.receiveUser = receiveUser;
    }

    public String getQuantityPayText() {
        if (locale != null) {
            return NumberUtils.formatMoney(quantityPay, locale);
        }
        return NumberUtils.formatMoney(quantityPay);
    }

    public boolean isMatching() {
        return matching;
    }

    public boolean getMatching() {
        return matching;
    }

    public void setMatching(boolean matching) {
        this.matching = matching;
    }

    public Long getReceiveRankId() {
        return receiveRankId;
    }

    public void setReceiveRankId(Long receiveRankId) {
        this.receiveRankId = receiveRankId;
    }

    public Long getOfferRankId() {
        return offerRankId;
    }

    public void setOfferRankId(Long offerRankId) {
        this.offerRankId = offerRankId;
    }

    public RankBis getOfferRank() {
        return offerRank;
    }

    public void setOfferRank(RankBis offerRank) {
        this.offerRank = offerRank;
    }

    public RankBis getReceiveRank() {
        return receiveRank;
    }

    public void setReceiveRank(RankBis receiveRank) {
        this.receiveRank = receiveRank;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public boolean getLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
