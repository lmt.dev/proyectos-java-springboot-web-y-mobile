/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberNode;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.utils.MemberSnapshot;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.mlm.specs.MemberSpecs;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.MemberSnapshotService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.RanksService;
import com.wings.backoffice.services.ShipmentsService;
import com.wings.backoffice.store.services.AffiliateServices;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.MemberUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class MemberController {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private MembersService membersService;
    @Autowired
    private RanksService ranksService;
    @Autowired
    private RanksBisRepository ranksBisRepository;
    @Autowired
    private AffiliateServices testStore;
    @Autowired
    private ShipmentsService shipmentsService;
    @Autowired
    private MemberSnapshotService memberSnapshotService;
    @Autowired
    private CalificationPointsRepository calificationPointsRepository;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private LogsService logsService;
    @Autowired
    private CountriesService countriesService;

    private final Logger log = LoggerFactory.getLogger(MemberController.class);

    /**
     * List all members
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/members/list"}, method = RequestMethod.GET)
    public String getMemberList(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String rank = request.getParameter("rank");
        String country = request.getParameter("country");

        Long rankId = null;
        if (rank != null) {
            RankBis rb = ranksBisRepository.findByName(rank);
            if (rb != null) {
                rankId = rb.getId();
            }
        }

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<Member> items = membersRepository.findAll(MemberSpecs.memberSearch(q, rankId, null, status, null, false, country), pr);

        PageWrapper<Member> pageWrapper = new PageWrapper<>(items, "admin/members/list");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<RankBis> ranks = ranksBisRepository.findAll();

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("ranks", ranks);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("rank", rank);
        //model.addAttribute("crank", crank);
        //model.addAttribute("payment", payment);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("user", AuthUtils.getAdminUser());

        return "admin/members/list";
    }

    /**
     * Get all members as tree
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/members/tree"}, method = RequestMethod.GET)
    public String getMemberTree(Model model, HttpServletRequest request) {
        model.addAttribute("id", 1);
        model.addAttribute("simple", request.getParameter("simple") != null);
        return "admin/members/tree";
    }

    /**
     * get one member TREE
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/tree/{id}"}, method = RequestMethod.GET)
    public String getMemberTreeById(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        model.addAttribute("id", id);
        model.addAttribute("simple", request.getParameter("simple") != null);
        return "admin/members/tree";
    }

    /**
     * get one member as tree DATA
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"admin/members/tree/data/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<MemberNode>> gePortRequest(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        List<Member> temp = new ArrayList<>();
        if (id == null) {
            temp = membersRepository.findAll();
        } else {
            Member member = membersRepository.findOne(id);
            temp.add(member);
        }

        return ResponseEntity.ok(MemberUtils.buildNodes(temp));
    }

    /**
     * for old system, no longer used
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @Deprecated
    @RequestMapping(value = {"admin/members/set-color/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> setColor(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        Member m = membersRepository.findOne(id);
        m.setColorSelected(true);
        membersRepository.save(m);
        return ResponseEntity.ok("success");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @Deprecated
    @RequestMapping(value = {"admin/members/set-payed/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> setPayed(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        return ResponseEntity.ok(membersService.setPayed(id));
    }

    /**
     * GET for update member rank
     *
     * @param model
     * @param request
     * @param memberId
     * @return
     */
    @RequestMapping(value = {"admin/members/rank/update/{id}"}, method = RequestMethod.GET)
    public String getUpdateRank(Model model, HttpServletRequest request, @PathVariable("id") Long memberId) {

        List<RankBis> ranks = ranksBisRepository.findAll();
        model.addAttribute("ranks", ranks);
        model.addAttribute("postUrl", "admin/members/rank/update/" + memberId);
        return "admin/members/rank-update";

    }

    /**
     * POST for update member rank
     *
     * @param model
     * @param request
     * @param memberId
     * @return
     */
    @RequestMapping(value = {"admin/members/rank/update/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postUpdateRank(Model model, HttpServletRequest request, @PathVariable("id") Long memberId) {

        String dateString = request.getParameter("creation");
        String rankString = request.getParameter("newRank");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = sdf.parse(dateString);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("La fecha no es valida");
        }
        RankBis newRank = ranksBisRepository.findOne(Long.parseLong(rankString));

        if (newRank == null) {
            return ResponseEntity.badRequest().body("El rango no es valido");
        }

        ranksService.updateRank(memberId, newRank.getId(), date);

        return ResponseEntity.ok("Nuevo rango insertado");
    }

    /**
     * POST for sponsor update
     *
     * @param model
     * @param request
     * @param memberId
     * @param sponsorId
     * @return
     */
    @RequestMapping(value = {"admin/members/sponsor/update"}, method = RequestMethod.POST)
    public ResponseEntity<String> updateSponsor(Model model, HttpServletRequest request, @RequestParam("memberId") Long memberId, @RequestParam("sponsorId") Long sponsorId) {
        return ResponseEntity.ok(membersService.updateSponsor(memberId, sponsorId, AuthUtils.getIp(request)));
    }

    /**
     * POST for KIT update
     *
     * @param model
     * @param request
     * @param memberId
     * @param kitId
     * @return
     */
    @RequestMapping(value = {"admin/members/kit/update"}, method = RequestMethod.POST)
    public ResponseEntity<String> updateKit(Model model, HttpServletRequest request, @RequestParam("memberId") Long memberId, @RequestParam("kitId") Long kitId) {
        String ret;
        try {
            Member m = membersRepository.findOne(memberId);
            ret = membersService.updateKit(memberId, kitId, request);
            ret += shipmentsService.processMembersColors(memberId);
            ret += shipmentsService.generateShipments(m.getOrderId());
        } catch (Exception e) {
            ret = "Error " + e.getMessage();
        }
        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteMeber(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        return ResponseEntity.ok(membersService.deleteMember(id, request));
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = {"admin/members/check-updates"}, method = RequestMethod.POST)
    public ResponseEntity<String> checkStatus() {
        return ResponseEntity.ok(testStore.test());
    }

    /**
     *
     * @param memberId
     * @return
     */
    @RequestMapping(value = {"admin/members/recalculate/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> analizeRank(@PathVariable("id") Long memberId) {
        Member member = membersService.getOne(memberId);
        ranksService.analizeRanks(member);
        return ResponseEntity.ok("ok");
    }

    /**
     *
     * @param memberId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"admin/members/view-snap/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getSnapshot(@PathVariable("id") Long memberId) {
        MemberSnapshot snap = memberSnapshotService.getLastMemberSnapshot(memberId);
        String ret = "Sin datos";
        if (snap != null) {
            ret = snap.toString();
        }
        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param pointId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"admin/members/view-snap/point/{pointId}"}, method = RequestMethod.GET)
    public ResponseEntity<String> getSnapshotForEvent(@PathVariable("pointId") Long pointId) {
        CalificationPoint point = calificationPointsRepository.findOne(pointId);
        MemberSnapshot snap = memberSnapshotService.getMemberSnapshot(point.getMemberId(), point.getEventId());
        return ResponseEntity.ok(snap != null ? snap.toString() : "Sin Datos");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/reset-passwd/{id}"}, method = RequestMethod.GET)
    public String getResetPassword(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        model.addAttribute("referer", request.getParameter("referer"));
        model.addAttribute("url", "admin/members/reset-passwd/" + id);

        return "common/edit-password";
    }

    /**
     *
     * @param redirectAttributes
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/reset-passwd/{id}"}, method = RequestMethod.POST)
    public String postResetPassword(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Long id) {

        String referer = request.getParameter("referer");
        String newPassword1 = request.getParameter("newPassword1");
        String newPassword2 = request.getParameter("newPassword2");

        if (newPassword1.trim().isEmpty() || newPassword2.trim().isEmpty()) {
            String message = messageSource.getMessage("password.invalid", null, localeResolver.resolveLocale(request));
            redirectAttributes.addFlashAttribute("message", message);
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:" + referer;
        }

        if (!newPassword1.trim().equals(newPassword2.trim())) {
            String message = messageSource.getMessage("password.dontmatch", null, localeResolver.resolveLocale(request));
            redirectAttributes.addFlashAttribute("message", message);
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:" + referer;
        }

        Member member = membersRepository.findOne(id);
        member.setPassword(DigestUtils.sha256Hex(newPassword1));
        membersRepository.save(member);

        String message = messageSource.getMessage("reset.success", null, localeResolver.resolveLocale(request));
        redirectAttributes.addFlashAttribute("message", message);
        redirectAttributes.addFlashAttribute("messageType", "success");

        return "redirect:" + referer;
    }

    /**
     *
     * @param redirectAttributes
     * @param request
     * @param mark
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/founder/{mark}/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> founderMark(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("mark") String mark, @PathVariable("id") Long id) {

        Member member = membersRepository.findOne(id);
        if (mark.equals("mark")) {
            member.setFounder(true);
        } else {
            member.setFounder(false);
        }
        membersRepository.save(member);

        return ResponseEntity.ok("Actualizado");
    }

    /**
     *
     * @param redirectAttributes
     * @param request
     * @param promo
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/promo/{promo}/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> excludePromo(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("promo") String promo, @PathVariable("id") Long id) {

        Member member = membersRepository.findOne(id);
        member.setExcludePromo(promo.equals("promo"));

        membersRepository.save(member);

        return ResponseEntity.ok("Actualizado");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/search/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> searchMember(Model model, HttpServletRequest request, @PathVariable("id") String id) {

        Member member = membersRepository.findByUsername(id);
        if (member == null) {
            try {
                member = membersRepository.findOne(Long.parseLong(id));
            } catch (Exception e) {
            }
        }
        if (member == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        return ResponseEntity.ok(member.toString() + "(" + member.getDniType() + " - " + member.getDni() + ")");

    }

    /**
     *
     * @param model
     * @param request
     * @param memberId
     * @return
     */
    @RequestMapping(value = {"admin/members/resend-email/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> resendEmail(Model model, HttpServletRequest request, @PathVariable("id") Long memberId) {
        Member member = membersService.getOne(memberId);
        if (member == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }
        String generatedPassword = InvoiceUtils.getRandomString();
        member.setPassword(DigestUtils.sha256Hex(generatedPassword));
        boolean result = membersService.sendActivationEmail(member, generatedPassword);
        if (result) {
            return ResponseEntity.ok("Correo enviado correctamente");
        } else {
            return ResponseEntity.ok("Ocurrio un error enviando el correo");
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param memberId
     * @param speedId
     * @return
     */
    @RequestMapping(value = {"admin/members/speed/update"}, method = RequestMethod.POST)
    public ResponseEntity<String> updateSpeedBonus(Model model, HttpServletRequest request, @RequestParam("memberId") Long memberId, @RequestParam("speedId") Long speedId) {
        return ResponseEntity.ok(membersService.updateSpeedBonus(memberId, speedId));
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/members/export"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> exportEmailList(Model model, HttpServletRequest request) {

        String ret = membersService.getCsv();
        HttpHeaders headers = new HttpHeaders();
        FileInputStream fis;
        try {
            File f = File.createTempFile("listado_emails", "");
            FileWriter fw = new FileWriter(f);
            fw.append(ret);
            fw.close();

            fis = new FileInputStream(f);

            headers.setContentType(MediaType.parseMediaType("application/csv"));
            headers.add("Access-Control-Allow-Origin", "*");
            headers.add("Content-Disposition", "filename=" + f.getName() + ".csv");
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/email/update/{id}"}, method = RequestMethod.GET)
    public String getChangeEmail(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        model.addAttribute("member", membersRepository.findOne(id));
        model.addAttribute("referer", request.getParameter("referer"));
        model.addAttribute("url", "admin/members/email/update/" + id);

        return "admin/members/change-email";
    }

    /**
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/email/update/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postChangeEmail(HttpServletRequest request, @PathVariable("id") Long id) {

        String ret;
        try {
            String newEmail = request.getParameter("newEmail");
            newEmail = newEmail.trim();

            boolean valid = EmailValidator.getInstance().isValid(newEmail);
            if (!valid) {
                return ResponseEntity.badRequest().body("La dirección de correo ingresada no es válida, no se realizaron cambios");
            }

            List<Member> already = membersRepository.findByEmail(newEmail);
            if (!already.isEmpty()) {
                return ResponseEntity.badRequest().body("La dirección de correo ya esta siendo usada por el afiliado " + already.get(0).toString() + ", no se realizaron cambios");
            }

            ret = membersService.changeEmail(id, newEmail, request);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error modificando el email " + e.getMessage());
        }

        return ResponseEntity.ok().body(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/kit/update/{id}"}, method = RequestMethod.GET)
    public String getChangeAfiliationKit(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Member member = membersRepository.findOne(id);
        model.addAttribute("member", member);
        model.addAttribute("url", "admin/members/kit/update/" + id);

        List<Order> orders = ordersService.findByTypeAndMemberId(OrderType.kit.name(), id);
        model.addAttribute("orders", orders);

        return "admin/members/change-kit";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/kit/update/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postChangeAfiliationKit(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        String newKit = request.getParameter("newKit");
        if (newKit == null || newKit.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El id de kit no puede estar vacio");
        }

        Long orderId;
        try {
            orderId = Long.parseLong(newKit);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().body("El id de kit debe ser numerico");
        }

        Member member = membersRepository.findOne(id);
        Long oldKit = member.getOrderId();

        List<Order> orders = ordersService.findByTypeAndMemberId(OrderType.kit.name(), id);
        boolean valido = false;
        for (Order order : orders) {
            if (order.getId().equals(orderId)) {
                valido = true;
                break;
            }
        }

        if (valido) {
            member.setOrderId(orderId);
            membersRepository.save(member);

            String ip = AuthUtils.getIp(request);
            String modUser = AuthUtils.getAdminUser().getUsername();
            logsService.addKitModificationLog(ip, modUser, member, oldKit);

            return ResponseEntity.ok("Kit de afiliado modificado");
        } else {
            return ResponseEntity.badRequest().body("El id de kit debe ser válido");
        }

    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"admin/members/kit/upgrade/{id}"}, method = RequestMethod.GET)
    public String getKitUpgrade(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Member m = membersRepository.findOne(id);
        List<StoreItem> items = ordersService.getAvailaveUpgrades(m);
        List<Order> orders = ordersService.findByDeletedAndMemberId(false, m.getId());

        model.addAttribute("storeItems", items);
        model.addAttribute("member", m);
        model.addAttribute("orders", orders);

        return "admin/members/upgrade";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/members/kit/upgrade"}, method = RequestMethod.POST)
    public ResponseEntity<String> postKitUpgrade(Model model, HttpServletRequest request) {

        String upgradeSelect = request.getParameter("upgradeSelect");
        String memberId = request.getParameter("memberId");

        if (upgradeSelect == null || upgradeSelect.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El id de kit no puede estar vacio");
        }

        try {

            Member member = membersRepository.findOne(Long.valueOf(memberId));
            String ip = AuthUtils.getIp(request);
            String modUser = AuthUtils.getAdminUser().getUsername();

            ordersService.addUpgrade(member, upgradeSelect, modUser);
            logsService.addKitUpgradeLog(ip, modUser, member, Long.valueOf(upgradeSelect));

        } catch (Exception e) {
            log.error("Error addind upgrade", e);
            return ResponseEntity.badRequest().body("Ocurrió un error al añadir el upgrade");
        }

        return ResponseEntity.ok("Upgrade añadido correctamente");

    }
}
