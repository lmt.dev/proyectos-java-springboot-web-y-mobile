/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Withdrawal;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.WithdrawalStatus;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.WithdrawalRepository;
import com.wings.backoffice.mlm.specs.WithdrawalSpecs;
import com.wings.backoffice.services.BalanceService;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class FinancialControler {

    @Autowired
    private WithdrawalRepository withdrawalRepository;
    @Autowired
    private MembersRepository membersRepository;

    //TODO: eliminar repository
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private BalanceDAO balanceDAO;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private CountriesService countriesService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * List all balances
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/financial/balances"}, method = RequestMethod.GET)
    public String getBalances(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String type = request.getParameter("type");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String country = request.getParameter("country");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
            } catch (ParseException e) {
            }
        }

        Page<Balance> items = balanceService.findAll(page, size, q, f, t, type, null, null, country);
        BalanceSums sums = balanceDAO.find(q, f, t, type, null, null, country);

        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "admin/financial/balances");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("total", sums);
        model.addAttribute("type", type);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());

        return "admin/financial/balances";
    }

    /**
     * GET for create one balance
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/financial/balance/create"}, method = RequestMethod.GET)
    public String createBalance(Model model, HttpServletRequest request) {
        model.addAttribute("postUrl", "/admin/financial/balance/create");
        return "admin/financial/balance-create";
    }

    /**
     * POST for create balance
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/financial/balance/create"}, method = RequestMethod.POST)
    public ResponseEntity<String> postBalance(Model model, HttpServletRequest request) {

        AdminUser admin = AuthUtils.getAdminUser();
        String user = admin.getUsername();
        String amount = request.getParameter("amount");
        String id = request.getParameter("member");
        String description = request.getParameter("description");
        String comment = request.getParameter("comment");
        String type = request.getParameter("type");
        String ret = request.getParameter("effective");
        boolean effective = ret != null;

        if (type == null || type.equals("-1")) {
            return ResponseEntity.badRequest().body("El tipo es inválido");
        }

        //negativo o positivo
        int multiplier = -1;
        if (!type.equals("discount")) {
            multiplier = 1;
        }

        Double monto;
        try {
            monto = Double.parseDouble(amount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("El monto debe ser válido");
        }

        Member member = membersRepository.findByUsername(id);
        if (member == null) {
            try {
                member = membersRepository.findOne(Long.parseLong(id));
            } catch (Exception e) {
            }
        }
        if (member == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        Date creatioDate = new Date();

        Balance b = new Balance();

        b.setAmount(monto * multiplier);
        b.setControl("discount-" + InvoiceUtils.getNumberRandomString(5));
        b.setCreationDate(creatioDate);
        b.setDescription(description);
        b.setMemberId(member.getId());
        b.setReferenceId(null);
        b.setStatus(BalanceStatus.created.name());
        b.setStatusText("Balance created by " + user + " on " + DateUtils.formatNormalDateTime(creatioDate));
        b.setType(type);
        b.setEffective(effective);
        b.setComment(comment);

        balanceService.create(b, request);

        return ResponseEntity.ok("Balance creado");
    }

    /**
     * GET for edit balance
     *
     * @param model
     * @param request
     * @param balanceId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/balance/edit/{id}"}, method = RequestMethod.GET)
    public String getEditBalance(Model model, HttpServletRequest request, @PathVariable("id") Long balanceId) {
        Balance b = balanceService.getOne(balanceId);

        model.addAttribute("balance", b);
        model.addAttribute("postUrl", "/admin/financial/balance/edit/" + balanceId);

        return "admin/financial/balance-edit";
    }

    /**
     * POST for edit balance
     *
     * @param model
     * @param request
     * @param balanceId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/balance/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEditBalance(Model model, HttpServletRequest request, @PathVariable("id") Long balanceId) {

        Balance b = balanceService.getOne(balanceId);
        AdminUser admin = AuthUtils.getAdminUser();
        String user = admin.getUsername();

        String amount = request.getParameter("amount");
        String description = request.getParameter("description");
        String comment = request.getParameter("comment");
        String ret = request.getParameter("effective");
        boolean effective = ret != null;

        Double monto;
        try {
            monto = Double.parseDouble(amount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("El monto debe ser válido");
        }

        b.setAmount(monto);
        b.setDescription(description);
        b.setComment(comment + "\nEdited by " + user + DateUtils.formatNormalDateTime(new Date()));
        b.setEffective(effective);

        balanceService.save(b);

        return ResponseEntity.ok("Balance modificado");
    }

    /**
     * POST for delete balance
     *
     * @param model
     * @param request
     * @param balanceId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/balance/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteBalance(Model model, HttpServletRequest request, @PathVariable("id") Long balanceId) {
        Balance b = balanceService.getOne(balanceId);
        balanceService.removeBalance(b, request);
        return ResponseEntity.ok("Balance eliminado");
    }

    /**
     * List all withdrawals
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal"}, method = RequestMethod.GET)
    public String getWithdrawalRequests(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String country = request.getParameter("country");

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        Sort.Order dateSort = new Sort.Order(Sort.Direction.DESC, "requestDate");
        PageRequest pr = new PageRequest(page, size, new Sort(dateSort));
        Page<Withdrawal> items = withdrawalRepository.findAll(WithdrawalSpecs.search(q, status, f, t, null, country), pr);

        //set locale for currency conversion
        for (Withdrawal item : items) {
            Locale locale = CountryUtils.getLocaleForCountry(item.getMember().getCountry());
            item.setLocale(locale);
        }

        PageWrapper<Withdrawal> pageWrapper = new PageWrapper<>(items, "admin/financial/withdrawal");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("q", q);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", size);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());

        return "admin/financial/withdrawal";
    }

    /**
     * POST for delete withdrawal
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        withdrawalRepository.delete(requestId);
        List<Balance> toDelete = balanceRepository.findByTypeAndReferenceId(BalanceType.withdrawal.name(), requestId);
        balanceRepository.delete(toDelete);

        return ResponseEntity.ok("Solicitud eliminada, balance asociado eliminado");
    }

    /**
     * GET for edit withdrawal
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal/edit/{id}"}, method = RequestMethod.GET)
    public String getEditWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        Withdrawal w = withdrawalRepository.findOne(requestId);
        model.addAttribute("w", w);
        model.addAttribute("postUrl", "/admin/financial/withdrawal/edit/" + requestId);

        return "admin/financial/withdrawal-edit";
    }

    /**
     * POST for edit withdrawal
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postEditWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        String status = request.getParameter("status");
        String comment = request.getParameter("comment");

        if (status == null || status.equals("-1")) {
            return ResponseEntity.badRequest().body("Estado no válido");
        }

        Withdrawal w = withdrawalRepository.findOne(requestId);
        w.setStatus(status);

        AdminUser admin = AuthUtils.getAdminUser();
        String previous = w.getStatusDescription() != null ? w.getStatusDescription() : "";
        w.setStatusDescription(
                previous
                + admin.getUsername() + " "
                + DateUtils.formatNormalDateTime(new Date())
                + " " + status
                + " " + comment
                + System.lineSeparator());

        if (status.equals("rejected")) {
            w.setRequestDescription(comment);
        }

        if (status.equals("rejected")) {

            List<Balance> bal = balanceRepository.findByTypeAndReferenceId(BalanceType.withdrawal.name(), requestId);
            if (bal.isEmpty()) {
                return ResponseEntity.badRequest().body("No se encontro item en balances");
            }

            if (bal.size() > 1) {
                return ResponseEntity.badRequest().body("Se encontro mas de un item en balances");
            }

            balanceService.removeBalance(bal.get(0));

        } else {

            List<Balance> bal = balanceRepository.findByTypeAndReferenceId(BalanceType.withdrawal.name(), requestId);
            if (bal.isEmpty()) {
                Date creationDate = new Date();

                Balance b = new Balance();
                b.setAmount(w.getAmount() * -1);
                b.setControl("withdrawal-" + w.getId());
                b.setCreationDate(creationDate);
                b.setDescription("Solicitud retiro " + DateUtils.formatNormalDateTime(creationDate) + " (#" + w.getId() + ")");
                b.setMemberId(w.getMemberId());
                b.setReferenceId(w.getId());
                b.setEffective(true);
                b.setStatus(BalanceStatus.effective.name());
                b.setType(BalanceType.withdrawal.name());
                b.setComment("Auto balance created from withdrawal request - effective by default");

                balanceService.save(b);

            } else {

                bal.get(0).setEffective(true);
                bal.get(0).setStatus(BalanceStatus.effective.name());

                balanceService.save(bal);
            }

        }

        withdrawalRepository.save(w);

        return ResponseEntity.ok("Solicitud editada, balance asociado editado");
    }

    /**
     * GET for complete withdrawal
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal/complete/{id}"}, method = RequestMethod.GET)
    public String getCompleteWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        Withdrawal w = withdrawalRepository.findOne(requestId);

        Locale locale = CountryUtils.getLocaleForCountry(w.getMember().getCountry());
        w.setLocale(locale);

        model.addAttribute("w", w);
        model.addAttribute("postUrl", "/admin/financial/withdrawal/complete/" + requestId);

        return "admin/financial/withdrawal-complete";
    }

    /**
     * Post for Complete withdrawal
     *
     * @param model
     * @param request
     * @param requestId
     * @return
     */
    @RequestMapping(value = {"/admin/financial/withdrawal/complete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postCompleteWithdrawalRequest(Model model, HttpServletRequest request, @PathVariable("id") Long requestId) {

        String comment = request.getParameter("comment");
        String processor = request.getParameter("processor");
        String dCompleted = request.getParameter("date");
        Date completedDate = DateUtils.fromMysqlStringToDate(dCompleted);

        Withdrawal w = withdrawalRepository.findOne(requestId);

        w.setStatus(WithdrawalStatus.completed.name());
        w.setProcessor(processor);
        w.setCompleted(true);
        w.setCompletedDate(completedDate);
        w.setRequestDescription(comment);

        AdminUser admin = AuthUtils.getAdminUser();
        String previous = w.getStatusDescription() != null ? w.getStatusDescription() : "";
        w.setStatusDescription(
                previous
                + admin.getUsername() + " "
                + DateUtils.formatNormalDateTime(new Date())
                + " completed " + comment
                + System.lineSeparator());

        List<Balance> bal = balanceRepository.findByTypeAndReferenceId(BalanceType.withdrawal.name(), requestId);
        if (bal.isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontro item en balances");
        }

        if (bal.size() > 1) {
            return ResponseEntity.badRequest().body("Se encontro mas de un item en balances");
        }

        bal.get(0).setEffective(true);
        bal.get(0).setStatus(BalanceStatus.effective.name());

        withdrawalRepository.save(w);
        balanceService.save(bal);

        return ResponseEntity.ok("Solicitud completada, balance asociado editado");
    }

}
