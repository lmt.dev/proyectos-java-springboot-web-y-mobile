/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.enums;

/**
 *
 * @author seba
 */
public class PostStatus {

    /**
     * Order received (unpaid)
     */
    public final static String PENDING = "wc-pending";
    
    /**
     * Payment received and stock has been reduced – the order is awaiting
     * fulfillment. All product orders require processing, except those that are
     * Digital and Downloadable.
     */
    public final static String PROCESSING = "wc-processing";

    /**
     * Awaiting payment – stock is reduced, but you need to confirm payment
     */
    public final static String ONHOLD = "wc-on-hold";

    /**
     * Order fulfilled and complete – requires no further action
     */
    public final static String COMPLETED = "wc-completed";

    /**
     * Cancelled by an admin or the customer – no further action required
     * (Cancelling an order does not affect stock quantity by default)
     */
    public final static String CANCELED = "wc-cancelled";

    /**
     * Refunded by an admin – no further action required
     */
    public final static String REFUNDED = "wc-refunded";
    
    /**
     * Payment failed or was declined (unpaid). Note that this status may not
     * show immediately and instead show as Pending until verified (i.e.,
     * PayPal)
     */
    public final static String FAILED = "wc-failed";

}
