/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.open;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.prizes.Pins;
import com.wings.backoffice.mlm.enums.PinsTypes;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.PinsRepository;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class CheckingBonusController {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private PinsRepository pinsRepository;

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @RequestMapping(value = {"/checkingbonus"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {
        model.addAttribute("error", null);
        model.addAttribute("isDate", isDate());
        return "open/checking-bonus";
    }

    @RequestMapping(value = {"/checkingbonus"}, method = RequestMethod.POST)
    public String postCode(Model model, HttpServletRequest request) {

        String username = request.getParameter("username");
        String code = request.getParameter("code");
        String error;

        model.addAttribute("isDate", isDate());

        if (code == null || code.trim().isEmpty()) {
            error = "El código no puede estar vacio";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        if (username == null || username.trim().isEmpty()) {
            error = "El Nombre de usuario o el correo no puede estar vacio";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        Member m = membersRepository.findByUsername(username);
        if (m == null) {
            m = membersRepository.findFirstByEmail(username);
        }
        if (m == null) {
            error = "Nombre de usuario o correo incorrecto - Verifique los datos ingresados y vuelva a intentar, si el problema persiste por favor contacte a atención al cliente atencionalcliente@wingsmobile.es";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        if (!m.getOrder().getActive()) {
            error = "Error validando los datos del afilaido por favor contacte a atención al cliente atencionalcliente@wingsmobile.es indicando sus datos de afiliado";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        Pins pin = pinsRepository.findByCode(code);
        if (pin == null) {
            error = "Códido incorrecto - Verifique el código ingresado y vuelva a intentar, si el problema persiste por favor contacte a atención al cliente atencionalcliente@wingsmobile.es";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        if (pin.getMemberId() != null) {
            error = "Códido ya reclamado";
            model.addAttribute("error", error);
            return "open/checking-bonus";
        }

        boolean valid;
        //validamos que el premio sea el correcto
        switch (pin.getName()) {
            case "crucero":
                valid = m.isFounder();
                break;
            case "luxury":
                valid = m.getRankId() >= 3;
                break;
            case "vuelo":
                valid = true;
                break;
            default:
                valid = false;
        }

        if (valid) {
            String message = "Se ha asignado correctamentamente el bono a su cuenta, en unos días recibirá las instrucciones para utilizarlo en su correo electrónico";
            pin.setMemberId(m.getId());
            pin.setRegistrationDate(new Date());
            pin.setRegistrationParam(username);
            pinsRepository.save(pin);
            model.addAttribute("message", message);
        } else {
            error = "El bono no coincide con el afiliado ingresado, por favor contacte a atención al cliente atencionalcliente@wingsmobile.es";
            model.addAttribute("error", error);
        }
        return "open/checking-bonus";
    }

    private boolean isDate() {
        boolean ret = true;
        try {
            Date now = new Date();
            Date date = sdf.parse("13/12/2017 00:00:00");
            if (now.after(date)) {
                ret = false;
            }
        } catch (Exception e) {
        }
        return ret;
    }
}
