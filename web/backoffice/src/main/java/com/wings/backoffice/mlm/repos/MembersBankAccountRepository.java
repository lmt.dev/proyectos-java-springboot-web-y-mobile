/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.MemberBankAccount;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface MembersBankAccountRepository extends JpaRepository<MemberBankAccount, Long>, JpaSpecificationExecutor<MemberBankAccount> {

    List<MemberBankAccount> findByMemberId(Long memberId);

    List<MemberBankAccount> findByVerified(Boolean verified);

    List<MemberBankAccount> findByMemberIdAndVerified(Long memberId, Boolean verified);

}
