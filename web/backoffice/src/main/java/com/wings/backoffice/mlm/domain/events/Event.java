/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.events;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_events")
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Column(name = "event_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Size(max = 250)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "short_name")
    private String shortName;
    @Size(max = 150)
    @Column(name = "date")
    private String date;
    @Size(max = 150)
    @Column(name = "hour")
    private String hour;
    @Size(max = 250)
    @Column(name = "place")
    private String place;
    @Size(max = 250)
    @Column(name = "place_address")
    private String placeAddress;
    @Size(max = 250)
    @Column(name = "image")
    private String image;
    @Size(max = 250)
    @Column(name = "ticket_background")
    private String ticketBackground;
    @Size(max = 100)
    @Column(name = "ticket_color")
    private String ticketColor;
    @Column(name = "generate_register")
    private Boolean generateRegister;
    @Column(name = "ticket_numbers_from")
    private Integer ticketNumbersFrom;
    @Column(name = "ticket_numbers_to")
    private Integer ticketNumbersTo;
    @Size(max = 200)
    @Column(name = "available_countries")
    private String availableCountries;
    @Column(name = "seats_numbers")
    private Boolean seatsNumbers;
    @Size(max = 250)
    @Column(name = "disclaimer")
    private String disclaimer;
    @Size(max = 250)
    @Column(name = "status")
    private String status;
    @Size(max = 20)
    @Column(name = "ticket_numbers_pattern")
    private String ticketNumbersPattern;
    @Column(name = "show_name")
    private boolean showName;
    @Column(name = "show_date")
    private boolean showDate;
    @Column(name = "show_hour")
    private boolean showHour;
    @Column(name = "show_place")
    private boolean showPlace;
    @Column(name = "show_place_address")
    private boolean showPlaceAddress;
    @Size(max = 100)
    @Column(name = "text_color")
    private String textColor;
    @Column(name = "sold_out")
    private boolean soldOut;
    @Column(name = "enable_sync")
    private boolean enableSync;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", nullable = true, updatable = false, insertable = false)
    private List<EventExtra> extras = new LinkedList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTicketBackground() {
        return ticketBackground;
    }

    public void setTicketBackground(String ticketBackground) {
        this.ticketBackground = ticketBackground;
    }

    public String getTicketColor() {
        return ticketColor;
    }

    public void setTicketColor(String ticketColor) {
        this.ticketColor = ticketColor;
    }

    public Boolean getGenerateRegister() {
        return generateRegister;
    }

    public void setGenerateRegister(Boolean generateRegister) {
        this.generateRegister = generateRegister;
    }

    public Integer getTicketNumbersFrom() {
        return ticketNumbersFrom;
    }

    public void setTicketNumbersFrom(Integer ticketNumbersFrom) {
        this.ticketNumbersFrom = ticketNumbersFrom;
    }

    public Integer getTicketNumbersTo() {
        return ticketNumbersTo;
    }

    public void setTicketNumbersTo(Integer ticketNumbersTo) {
        this.ticketNumbersTo = ticketNumbersTo;
    }

    public String getAvailableCountries() {
        return availableCountries;
    }

    public void setAvailableCountries(String availableCountries) {
        this.availableCountries = availableCountries;
    }

    public Boolean getSeatsNumbers() {
        return seatsNumbers;
    }

    public void setSeatsNumbers(Boolean seatsNumbers) {
        this.seatsNumbers = seatsNumbers;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.web.admin.events.Event[ id=" + id + " ]";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTicketNumbersPattern() {
        return ticketNumbersPattern;
    }

    public void setTicketNumbersPattern(String ticketNumbersPattern) {
        this.ticketNumbersPattern = ticketNumbersPattern;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public void setPlaceAddress(String placeAddress) {
        this.placeAddress = placeAddress;
    }

    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }

    public boolean isShowDate() {
        return showDate;
    }

    public void setShowHour(boolean showHour) {
        this.showHour = showHour;
    }

    public boolean isShowHour() {
        return showHour;
    }

    public void setShowName(boolean showName) {
        this.showName = showName;
    }

    public boolean isShowName() {
        return showName;
    }

    public void setShowPlace(boolean showPlace) {
        this.showPlace = showPlace;
    }

    public boolean isShowPlace() {
        return showPlace;
    }

    public void setShowPlaceAddress(boolean showPlaceAddress) {
        this.showPlaceAddress = showPlaceAddress;
    }

    public boolean isShowPlaceAddress() {
        return showPlaceAddress;
    }

    public List<EventExtra> getExtras() {
        return extras;
    }

    public void setExtras(List<EventExtra> extras) {
        this.extras = extras;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public void setSoldOut(boolean soldOut) {
        this.soldOut = soldOut;
    }

    public void setEnableSync(boolean enableSync) {
        this.enableSync = enableSync;
    }

    public boolean isEnableSync() {
        return enableSync;
    }

}
