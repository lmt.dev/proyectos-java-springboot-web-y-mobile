/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.dao.OrderDto;
import com.wings.backoffice.mlm.domain.CountryProperties;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.paymentsType.PaymentTypeService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.wings.backoffice.mlm.domain.PaymentType;
import com.wings.backoffice.services.BacsService;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Arrays;

/**
 *
 * @author seba
 */
@Controller
public class OrdersController {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private EventsService eventsService;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private PaymentTypeService paymentTypeService;
    @Autowired
    private CountriesService countriesService;
    @Autowired
    private BacsService bacsService;

    private final Logger log = LoggerFactory.getLogger(OrdersController.class);
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private final Map<String, String> mimeTypes = new HashMap<String, String>() {
        {
            put("image/png", ".png");
            put("image/jpeg", ".jpg");
            put("application/pdf", ".pdf");
            put("text/plain", ".txt");
            put("video/mp4", ".mp4");
        }
    };

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/orders/list"}, method = RequestMethod.GET)
    public String getOrdersList(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String type = request.getParameter("type");
        String source = request.getParameter("source");
        String payment = request.getParameter("payment");
        String country = request.getParameter("country");
        String deleted = request.getParameter("deleted");
        String sactivation = request.getParameter("activation");
        String status = request.getParameter("status");
        Boolean showDeleted = deleted != null && deleted.equals("true");
        Boolean activation = sactivation != null && sactivation.equals("true");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String ticketNumber = request.getParameter("ticketNumber");
        String bac = request.getParameter("bac");

        if (bac != null && bac.equals("all")) {
            bac = null;
        }

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (source != null && source.equals("all")) {
            source = null;
        }

        if (payment != null && payment.equals("all")) {
            payment = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        if (ticketNumber != null && !ticketNumber.isEmpty()) {
            try {
                ticketNumber = URLDecoder.decode(ticketNumber.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        Page<Order> items = ordersService.search(page, size, q, type, showDeleted, source, payment, country, f, t, status, activation, ticketNumber, bac);
        PageWrapper<Order> pageWrapper = new PageWrapper<>(items, "admin/orders/list");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("type", type);
        model.addAttribute("status", type);
        model.addAttribute("source", source);
        model.addAttribute("payment", payment);
        model.addAttribute("country", country);
        model.addAttribute("countries", countriesService.getCountriesIsos());
        model.addAttribute("status", status);
        model.addAttribute("deleted", showDeleted);
        model.addAttribute("activation", activation);
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("ticketNumber", ticketNumber);
        model.addAttribute("user", AuthUtils.getAdminUser());
        model.addAttribute("bac", bac);
        model.addAttribute("bacs", bacsService.getAll());

        return "admin/orders/list";
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"admin/orders/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deleteOrder(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        String ret;
        try {
            Order o = ordersService.getOrder(orderId);

            o.setActive(false);
            o.setDeleted(true);
            o.setStatus(OrderStatus.pending.name());
            o.setComment("Order delete by " + AuthUtils.getAdminUser().getUsername() + " " + new Date() + " - " + o.getComment());
            ordersService.save(o);

            //si borro la orden, libero el ticket purchase
            try {
                if (o.getType().equals(OrderType.ticket.name())) {
                    eventsService.deleteEventProductPurchase(o);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                //delete balance if its from coms
                if (o.getStatus().equals(OrderStatus.paid.name()) && o.getPaymentType().equals("coms")) {
                    Balance toDelete = balanceRepository.findByOrderIdAndReferenceId(o.getId(), o.getId());
                    if (toDelete != null) {
                        balanceRepository.delete(toDelete);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ret = "Orden marcada como eliminada";
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"admin/orders/undelete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> undeleteOrder(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        String ret;
        try {
            Order o = ordersService.getOrder(orderId);
            o.setDeleted(false);
            o.setComment("Deleted mark removed by " + AuthUtils.getAdminUser().getUsername() + " " + new Date() + " - " + o.getComment());
            ordersService.save(o);

            ret = "Orden marcada como eliminada";
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/admin/orders/review/{id}"}, method = RequestMethod.GET)
    public String getOrderReview(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        Order order = ordersService.getOrder(orderId);
        List<PaymentType> paymentType = paymentTypeService.findAll();
        model.addAttribute("order", order);
        model.addAttribute("paymentTypes", paymentType);
        model.addAttribute("bacs", bacsService.getBacsForCountry(order.getBillingCountry()));
        return "admin/orders/mark-as-review";
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @param attachment
     * @return
     */
    @RequestMapping(value = {"/admin/orders/review/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postOrderReview(Model model, HttpServletRequest request, MultipartFile[] attachment, @PathVariable("id") Long orderId) {

        AdminUser admin = AuthUtils.getAdminUser();

        String ret;
        String date = request.getParameter("date");
        String hora = request.getParameter("time");
        String paymentType = request.getParameter("payment");
        String ticket = request.getParameter("ticket");
        String scomment = request.getParameter("comment");
        String bank = request.getParameter("bank");

        try {
            Order o = ordersService.getOrder(orderId);
            if (o == null) {
                ret = "No se encontro la orden";
                return ResponseEntity.status(400).body(ret);
            }

            Date paymentDate;
            try {
                paymentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " " + hora);
            } catch (Exception e) {
                ret = "Fecha no válida";
                return ResponseEntity.status(400).body(ret);
            }

            if (paymentDate.before(o.getCreationDate())) {
                ret = "Fecha de pago inferior a fecha de creacion";
                return ResponseEntity.status(400).body(ret);
            }

            if (paymentDate.after(new Date())) {
                ret = "La fecha de pago no puede ser en el futuro";
                return ResponseEntity.status(400).body(ret);
            }

            if (ticket == null || ticket.trim().isEmpty()) {
                ret = "El número de comprobante no es válido";
                return ResponseEntity.status(400).body(ret);
            }

            if (bank == null || bank.trim().isEmpty()) {
                ret = "El banco no es válido";
                return ResponseEntity.status(400).body(ret);
            }

            if (ticket.contains(".")) {
                ticket = ticket.replaceAll("\\.", "");
            }

            if (scomment != null && scomment.length() > 100) {
                return ResponseEntity.badRequest().body("El comentario no puede superar los 100 carácteres.");
            }

            o.setTicketNumber(ticket);
            o.setActivationDate(paymentDate);
            o.setModificationDate(new Date());
            o.setStatus(OrderStatus.review.name());
            o.setPaymentType(paymentType);
            o.setPaymentBacs(bank);

            String comment = o.getComment() != null ? o.getComment() : "";
            if (scomment != null && !scomment.trim().isEmpty()) {
                comment = scomment + "\n" + comment;
            }
            o.setComment("Review mark " + admin.getUsername() + " " + ticket + " " + DateUtils.formatNormalDateTime(new Date()) + "\n" + comment);

            String attachmentFile = "";
            if (attachment != null && attachment.length > 0) {
                try {
                    for (MultipartFile multipartFile : attachment) {
                        if (mimeTypes.containsKey(multipartFile.getContentType())) {
                            String prefix = InvoiceUtils.getRandomString(12);
                            String suffix = mimeTypes.get(multipartFile.getContentType());
                            String fileName = "/var/wings/bo_media/orders/" + prefix + suffix;
                            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                            stream.write(multipartFile.getBytes());
                            attachmentFile += prefix + suffix + ";";
                        } else {
                            ret = "Formato de archivo no soportado";
                            return ResponseEntity.status(400).body(ret);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error uploading voucher image", ex);
                    ret = "Error subiendo el adjunto";
                    return ResponseEntity.status(400).body(ret);
                }
            }

            o.setAttachment(attachmentFile);
            ordersService.save(o);
            ret = "Orden marcada para revisar";
            return ResponseEntity.ok(ret);

        } catch (Exception e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/admin/orders/paid/{id}"}, method = RequestMethod.GET)
    public String getOrderPay(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        Order order = ordersService.getOrder(orderId);
        List<PaymentType> paymentType = paymentTypeService.findAll();
        model.addAttribute("order", order);
        model.addAttribute("images", order.getAttachment() != null ? order.getAttachment().split(";") : null);
        model.addAttribute("paymentTypes", paymentType);
        return "admin/orders/mark-as-paid";
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @param action
     * @return
     */
    @RequestMapping(value = {"/admin/orders/paid/{id}/{action}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postOrderPay(Model model, HttpServletRequest request, @PathVariable("action") String action, @PathVariable("id") Long orderId) {

        AdminUser admin = AuthUtils.getAdminUser();

        String ret;
        String date = request.getParameter("date");
        String hora = request.getParameter("time");
        String scomment = request.getParameter("addComment");
        String payment = request.getParameter("payment");

        try {
            Order o = ordersService.getOrder(orderId);
            if (o == null) {
                ret = "No se encontro la orden";
                return ResponseEntity.status(400).body(ret);
            }

            if (scomment != null && scomment.length() > 100) {
                return ResponseEntity.badRequest().body("El comentario no puede superar los 100 carácteres.");
            }

            String comment = o.getComment() != null ? o.getComment() : "";
            if (scomment != null && !scomment.trim().isEmpty()) {
                comment = scomment + "\n" + comment;
            }

            switch (action) {
                case "save":

                    Date paymentDate;
                    try {
                        paymentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " " + hora);
                    } catch (Exception e) {
                        ret = "Fecha no válida";
                        return ResponseEntity.status(400).body(ret);
                    }

                    if (paymentDate.before(o.getCreationDate())) {
                        ret = "Fecha de pago inferior a fecha de creacion";
                        return ResponseEntity.status(400).body(ret);
                    }

                    /*if (paymentDate.after(new Date())) {
                        ret = "La fecha de pago no puede ser en el futuro";
                        return ResponseEntity.status(400).body(ret);
                    }*/
                    o.setActive(true);
                    o.setModificationDate(new Date());
                    o.setPaymentDate(paymentDate);

                    //when is bacs payment is null, else comes with the processor
                    if (payment != null) {
                        o.setPaymentType(payment);
                    }

                    //payment date
                    if (o.getActivationDate() == null) {
                        o.setActivationDate(paymentDate);
                    }
                    o.setStatus(OrderStatus.paid.name());
                    o.setTypeStatus(OrderStatus.paid.name());
                    o.setComment("Payment mark " + admin.getUsername() + " " + DateUtils.formatNormalDateTime(new Date()) + "\n" + comment);
                    ret = "Orden marcada como pagada";
                    break;
                case "pending":
                    o.setActivationDate(null);
                    o.setActive(false);
                    o.setModificationDate(new Date());
                    o.setStatus(OrderStatus.pending.name());
                    o.setComment("Pending mark " + admin.getUsername() + " " + DateUtils.formatNormalDateTime(new Date()) + "\n" + comment);
                    ret = "Orden marcada como pendiente";
                    break;
                default:
                    return ResponseEntity.status(400).body("ocurrió un error");
            }

            ordersService.save(o);
            return ResponseEntity.ok(ret);

        } catch (Exception e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/admin/order/edit/{id}"}, method = RequestMethod.GET)
    public String getOrderEditPartial(Model model, HttpServletRequest request, @PathVariable("id") Long orderId) {
        Order order = ordersService.getOrder(orderId);
        List<String> countries = countriesService.getEnabledCountries();
        model.addAttribute("order", order);
        model.addAttribute("countries", countries);

        model.addAttribute("isBac", order.getStatus().equals("paid") && order.getPaymentType().equals("bacs"));
        model.addAttribute("bacs", bacsService.getBacsForCountry(order.getBillingCountry()));
        model.addAttribute("images", order.getAttachment() != null ? order.getAttachment().split(";") : null);

        return "admin/orders/order-edit";
    }

    /**
     *
     * @param model
     * @param request
     * @param attachment
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/admin/order/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postOrderEditPrice(Model model, HttpServletRequest request,
            @RequestParam(value = "attachment", required = false) MultipartFile[] attachment,
            @PathVariable("id") Long orderId
    ) {

        AdminUser admin = AuthUtils.getAdminUser();

        Order order = ordersService.getOrder(orderId);

        //solo cambia de precio
        String price = request.getParameter("price");
        Double d;
        try {
            d = Double.parseDouble(price.replaceAll(",", "."));
            if (!order.getPaymentAmount().equals(d)) {
                String oldPrice = order.getPaymentAmountText();
                order.setPaymentAmount(d);
                String newPrice = order.getPaymentAmountText();
                order.setComment("Change price " + admin.getUsername() + " new " + newPrice + " old " + oldPrice + "\n" + order.getComment());
            }
        } catch (Exception e) {
            //return ResponseEntity.badRequest().body("Numero no válido");
        }

        //Cambia de pais
        String newCountry = request.getParameter("newCountry");
        String oldCountry = order.getBillingCountry();
        if (newCountry != null && !newCountry.isEmpty() && !newCountry.equals("0") && !oldCountry.equals(newCountry)) {

            Double euros = NumberUtils.convertMoneyToEuros(order.getPaymentAmount(), CountryUtils.getLocaleForCountry(order.getBillingCountry()));
            Double convert = NumberUtils.convertMoneyFromEuros(euros, CountryUtils.getLocaleForCountry(newCountry));

            String oldPrice = order.getPaymentAmountText();
            order.setPaymentAmount(convert);
            String newPrice = order.getPaymentAmountText();

            order.setComment("Change country " + admin.getUsername() + " new " + newCountry + " old " + oldCountry + "\n" + order.getComment());
            order.setComment("Change price " + admin.getUsername() + " new " + newPrice + " old " + oldPrice + "\n" + order.getComment());

            CountryProperties props = countriesService.getPropertiesByCode(newCountry);
            order.setBillingCountry(newCountry);
            order.setPaymentCurrency(props.getCurrency());
            order.setPaymentAmount(convert);

        }

        //modifica datos bancarios
        String bank = request.getParameter("bank");
        if (bank != null && !bank.trim().isEmpty()) {
            String oldBaks = order.getPaymentBacs();
            order.setPaymentBacs(bank);
            order.setComment("Change bank " + admin.getUsername() + " new " + bank + " old " + oldBaks + "\n" + order.getComment());
        }

        String ticket = request.getParameter("ticket");
        if (ticket != null && !ticket.trim().isEmpty()) {
            String oldTicket = order.getTicketNumber();
            order.setComment("Change voucher " + admin.getUsername() + " new " + ticket + " old " + oldTicket + "\n" + order.getComment());
            order.setTicketNumber(ticket);
        }

        String attachmentFile = order.getAttachment();
        if (attachment != null && attachment.length > 0) {
            try {
                for (MultipartFile multipartFile : attachment) {
                    if (mimeTypes.containsKey(multipartFile.getContentType())) {
                        String prefix = InvoiceUtils.getRandomString(12);
                        String suffix = mimeTypes.get(multipartFile.getContentType());
                        String fileName = "/var/wings/bo_media/orders/" + prefix + suffix;
                        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                        stream.write(multipartFile.getBytes());
                        attachmentFile += prefix + suffix + ";";
                        order.setComment("Add image " + admin.getUsername() + "\n" + order.getComment());
                        order.setAttachment(attachmentFile);
                    }
                }
            } catch (Exception ex) {
                log.error("Error on attachment", ex);
            }
        }

        String date = request.getParameter("date");
        String hora = request.getParameter("time");
        Date paymentDate;
        try {
            paymentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " " + hora);
            String oldDate = order.getPaymentDate() != null ? DateUtils.formatNormalDateTime(order.getPaymentDate()) : "none";
            order.setPaymentDate(paymentDate);
            String newDate = DateUtils.formatNormalDateTime(paymentDate);
            order.setComment("Change payment date " + admin.getUsername() + " new " + newDate + " old " + oldDate + "\n" + order.getComment());
        } catch (Exception e) {
        }

        ordersService.save(order);
        return ResponseEntity.ok("Order modificada");

    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/admin/orders/validate"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderDto>> validateDate(Model model, HttpServletRequest request) {
        String date = request.getParameter("date");
        String time = request.getParameter("time");

        Date paymentDate = null;
        List<OrderDto> orders;
        String error = "";
        try {
            paymentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " " + time);
        } catch (Exception e) {
            error = e.getMessage();
        }
        if (error.isEmpty()) {
            List<String> status = Arrays.asList(OrderStatus.paid.name(), OrderStatus.review.name());
            orders = ordersService.getOrdersStatusAtDate(status, paymentDate);
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/orders/attachment/view"}, method = {RequestMethod.GET})
    public String getOrderAttachments(Model model, HttpServletRequest request) {
        String files = request.getParameter("file");
        if (files != null) {
            model.addAttribute("images", files.split(";"));
        }
        return "admin/orders/view-files";
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     * @param name
     */
    @RequestMapping(value = {"admin/orders/attachment/view/{file:.+}"}, method = {RequestMethod.GET})
    public void getAttachment(Model model, HttpServletRequest request, HttpServletResponse response, @PathVariable("file") String name) {
        try {
            File outputFile = new File("/var/wings/bo_media/orders/" + name);
            FileInputStream fis = new FileInputStream(outputFile);
            response.setContentType(URLConnection.guessContentTypeFromName(outputFile.getName()));
            if (name.contains(".mp4")) {
                response.setContentType("video/mp4");
            }
            response.setContentLengthLong(outputFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=" + name);
            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
