/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_corpo_content")
public class CorpoContent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Size(max = 50)
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "published")
    private Boolean published;
    @Size(max = 250)
    @Column(name = "path_file")
    private String pathFile;
    @Size(max = 250)
    @Column(name = "path_thumb")
    private String pathThumb;
    @Size(max = 250)
    @Column(name = "hash_file")
    private String hashFile;
    @Size(max = 250)
    @Column(name = "hash_thumb")
    private String hashThumb;
    @Size(max = 250)
    @Column(name = "rank_available")
    private String rankAvailable;
    @Size(max = 250)
    @Column(name = "country_available")
    private String countryAvailable;
    @Size(max = 150)
    @Column(name = "mime_type")
    private String mimeType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "downloaded")
    private long downloaded;

    public CorpoContent() {
    }

    public CorpoContent(Long id) {
        this.id = id;
    }

    public CorpoContent(Long id, Date creationDate, long downloaded) {
        this.id = id;
        this.creationDate = creationDate;
        this.downloaded = downloaded;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getPathThumb() {
        return pathThumb;
    }

    public void setPathThumb(String pathThumb) {
        this.pathThumb = pathThumb;
    }

    public String getHashFile() {
        return hashFile;
    }

    public void setHashFile(String hashFile) {
        this.hashFile = hashFile;
    }

    public String getHashThumb() {
        return hashThumb;
    }

    public void setHashThumb(String hashThumb) {
        this.hashThumb = hashThumb;
    }

    public String getRankAvailable() {
        return rankAvailable;
    }

    public void setRankAvailable(String rankAvailable) {
        this.rankAvailable = rankAvailable;
    }

    public String getCountryAvailable() {
        return countryAvailable;
    }

    public void setCountryAvailable(String countryAvailable) {
        this.countryAvailable = countryAvailable;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CorpoContent)) {
            return false;
        }
        CorpoContent other = (CorpoContent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.mlm.domain.CorpoContent[ id=" + id + " ]";
    }

}
