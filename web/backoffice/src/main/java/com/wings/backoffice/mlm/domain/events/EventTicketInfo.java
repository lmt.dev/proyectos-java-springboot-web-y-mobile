/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.events;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_events_tickets_info")
public class EventTicketInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticket_id")
    private long ticketId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_info_id")
    private long eventInfoId;
    @Size(max = 200)
    @Column(name = "info_name")
    private String infoName;
    @Lob
    @Size(max = 65535)
    @Column(name = "info_Value")
    private String infoValue;

    public EventTicketInfo() {
    }

    public EventTicketInfo(Long id) {
        this.id = id;
    }

    public EventTicketInfo(Long id, long ticketId, long eventInfoId) {
        this.id = id;
        this.ticketId = ticketId;
        this.eventInfoId = eventInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public long getEventInfoId() {
        return eventInfoId;
    }

    public void setEventInfoId(long eventInfoId) {
        this.eventInfoId = eventInfoId;
    }

    public String getInfoName() {
        return infoName;
    }

    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    public String getInfoValue() {
        return infoValue;
    }

    public void setInfoValue(String infoValue) {
        this.infoValue = infoValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventTicketInfo)) {
            return false;
        }
        EventTicketInfo other = (EventTicketInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.web.admin.events.EventTicketInfo[ id=" + id + " ]";
    }
    
}
