/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.SpeedBonusRepository;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SpeedBonusService {

    @Autowired
    private SpeedBonusRepository speedBonusRepository;

    @Autowired
    private MembersRepository membersRepository;

    private final List<SpeedBonus> cacheNoCountry = new ArrayList<>();
    private final Map<String, List<SpeedBonus>> cachePerCountry = new HashMap<>();

    private boolean running = false;
    private int good = 0;
    private int nogood = 0;

    public String asignSpeedBonusToAll() {

        if (!running) {
            running = true;
            good = 0;
            nogood = 0;
            List<Member> members = membersRepository.findAll();
            for (Member member : members) {
                if (member.getSpeedBonusId() == null) {
                    asignSpeedBonus(member);
                }

                if (member.getSpeedBonusId() != null) {
                    good++;
                } else {
                    nogood++;
                }
            }
            running = false;
        }
        return "Speed Bonus asignados " + good + ", no asignados " + nogood;
    }

    public void asignSpeedBonus(Member member) {
        //populate cache
        populateCache();

        if (member.getCountry() != null && !member.getCountry().isEmpty()) {

            //seteamos por pais primero
            List<SpeedBonus> speeds = cachePerCountry.get(member.getCountry());
            if (speeds != null && !speeds.isEmpty()) {
                for (SpeedBonus speed : speeds) {
                    if (DateUtils.dateInRange(member.getRegistrationDate(), speed.getValidFrom(), speed.getValidTo())) {
                        member.setSpeedBonusId(speed.getId());
                        membersRepository.save(member);
                    }
                }
            } else {
                //no tiene pais o el pais no tiene speed bonus
                for (SpeedBonus speed : cacheNoCountry) {
                    if (DateUtils.dateInRange(member.getRegistrationDate(), speed.getValidFrom(), speed.getValidTo())) {
                        member.setSpeedBonusId(speed.getId());
                        membersRepository.save(member);
                    }
                }
            }
        } else {
            //no tiene pais o el pais no tiene speed bonus
            for (SpeedBonus speed : cacheNoCountry) {
                if (DateUtils.dateInRange(member.getRegistrationDate(), speed.getValidFrom(), speed.getValidTo())) {
                    member.setSpeedBonusId(speed.getId());
                    membersRepository.save(member);
                }
            }
        }
    }

    private void populateCache() {
        if (cachePerCountry.isEmpty()) {
            List<SpeedBonus> temp = speedBonusRepository.findAll();
            temp.forEach((speedBonus) -> {
                if (speedBonus.getCountry() != null && !speedBonus.getCountry().isEmpty()) {
                    if (cachePerCountry.containsKey(speedBonus.getCountry())) {
                        cachePerCountry.get(speedBonus.getCountry()).add(speedBonus);
                    } else {
                        List<SpeedBonus> list = new ArrayList<>();
                        list.add(speedBonus);
                        cachePerCountry.put(speedBonus.getCountry(), list);
                    }
                } else {
                    cacheNoCountry.add(speedBonus);
                }
            });
        }
    }

    public void clearCache() {
        cacheNoCountry.clear();
        cachePerCountry.clear();
    }

    public SpeedBonus findOne(Long id) {
        return speedBonusRepository.findOne(id);
    }

    public void save(SpeedBonus speed) {
        speedBonusRepository.save(speed);
    }

}
