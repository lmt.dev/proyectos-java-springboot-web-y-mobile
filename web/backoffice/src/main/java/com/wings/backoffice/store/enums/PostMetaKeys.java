/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.enums;

/**
 *
 * @author seba
 */
public class PostMetaKeys {

    public static final String PAYMENT_METHOD = "_payment_method";
    public static final String PAYMENT_METHOD_TITLE = "_payment_method_title";
    /**
     * 2017-12-26 11:03:06
     */
    public static final String PAYMENT_DATE = "_paid_date";

    public static final String BILLING_FIRST_NAME = "_billing_first_name";
    public static final String BILLING_LAST_NAME = "_billing_last_name";
    public static final String BILLING_COMPANY = "_billing_company";
    public static final String BILLING_ADDRESS_1 = "_billing_address_1";
    public static final String BILLING_ADDRESS_2 = "_billing_address_2";
    public static final String BILLING_CITY = "_billing_city";
    public static final String BILLING_STATE = "_billing_state";
    public static final String BILLING_POST_CODE = "_billing_postcode";
    public static final String BILLING_COUNTRY = "_billing_country";
    public static final String BILLING_EMAIL = "_billing_email";
    public static final String BILLING_PHONE = "_billing_phone";
    public static final String BILLING_DNI = "NIF";
    public static final String BILLING_REFERER = "REF";

    public static final String SHIPPING_FIRST_NAME = "_shipping_first_name";
    public static final String SHIPPING_LAST_NAME = "_shipping_last_name";
    public static final String SHIPPING_COMPANY = "_shipping_company";
    public static final String SHIPPING_ADDRESS_1 = "_shipping_address_1";
    public static final String SHIPPING_ADDRESS_2 = "_shipping_address_2";
    public static final String SHIPPING_CITY = "_shipping_city";
    public static final String SHIPPING_STATE = "_shipping_state";
    public static final String SHIPPING_POST_CODE = "_shipping_postcode";
    public static final String SHIPPING_COUNTRY = "_shipping_country";
    
    public static final String ORDER_CURRENCY = "_order_currency";
    /**
     * shipping price
     */
    public static final String ORDER_SHIPPING = "_order_shipping";
    public static final String ORDER_TOTAL = "_order_total";    

}
