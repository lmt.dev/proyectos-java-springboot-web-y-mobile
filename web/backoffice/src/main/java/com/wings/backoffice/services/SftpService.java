/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SftpService {

    private final Logger log = LoggerFactory.getLogger(SftpService.class);

    /**
     * Download all files from SFTP server filter by mtime if downloaded is
     * null, will put all files inside that list
     *
     * @param username
     * @param password
     * @param host
     * @param remoteDirectory
     * @param localDirectory
     * @param mtime
     * @param downloaded
     * @return
     */
    public int downloadFiles(String username, String password, String host, String remoteDirectory, String localDirectory, int mtime, List<File> downloaded) {

        int lastMtime = mtime;
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(username, host);
            session.setPassword(password);

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;

            log.debug("Listing files directory");
            Vector v = sftpChannel.ls(remoteDirectory);
            for (Iterator iterator = v.iterator(); iterator.hasNext();) {
                ChannelSftp.LsEntry next = (ChannelSftp.LsEntry) iterator.next();

                //skip current dir and up dir
                if (next.getFilename().equals(".") || next.getFilename().equals("..")) {
                    continue;
                }

                if (mtime < next.getAttrs().getMTime()) {
                    log.debug("Downloading file " + next.getFilename());

                    File download = new File(localDirectory + "/" + next.getFilename());
                    if (downloaded != null) {
                        downloaded.add(download);
                    }

                    sftpChannel.get(remoteDirectory + "/" + next.getFilename(), new FileOutputStream(download));

                    int currentTime = next.getAttrs().getMTime();
                    if (lastMtime < currentTime) {
                        lastMtime = next.getAttrs().getMTime();
                    }
                }
            }
            sftpChannel.exit();
            session.disconnect();
        } catch (Exception e) {
            log.error("Error get file from sftp", e);
        }
        return lastMtime;
    }

}
