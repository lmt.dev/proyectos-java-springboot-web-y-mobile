/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberNode;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.MemberUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ReferersController {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private MediaService mediaService;

    @RequestMapping(value = {"/members/referers/list"}, method = RequestMethod.GET)
    public String getReferersList(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "12";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 12;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size);

        Page<Member> items = membersRepository.findBySponsorIdAndDeleted(member.getId(), Boolean.FALSE, pr);
        items.getContent().forEach((m) -> {
            m.setAvatar("/open/media/" + mediaService.getAvatarCode(m));
        });
        PageWrapper<Member> pageWrapper = new PageWrapper<>(items, "members/referers/list");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);

        return "members/referers/list";
    }

    @RequestMapping(value = {"/members/referers/tree"}, method = RequestMethod.GET)
    public String getMemberTree(Model model, HttpServletRequest request) {
        Member member = AuthUtils.getMemberUser();
        model.addAttribute("id", member.getId());
        return "members/referers/tree";
    }

    @ResponseBody
    @RequestMapping(value = {"members/referers/tree/data/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<MemberNode>> gePortRequest(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        List<Member> temp = new ArrayList<>();
        Member member = membersRepository.findOne(id);
        temp.add(member);
        return ResponseEntity.ok(MemberUtils.buildNodes(temp));
    }
}
