/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Country;
import com.wings.backoffice.mlm.domain.CountryProperties;
import com.wings.backoffice.mlm.enums.CountryLevel;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.Result;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lucas
 */
@Controller
public class CountryController {

    @Autowired
    private CountriesService countriesService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(path = "/admin/settings/countries", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String status = request.getParameter("status");

        if (status == null) {
            status = "1";
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<Country> list = countriesService.search(q, page, size, status);
        PageWrapper<Country> pageWrapper = new PageWrapper<>(list, "admin/settings/countries");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > list.getTotalElements()) {
            hasta = list.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + list.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<String> countryLevels = new LinkedList<>();
        for (CountryLevel level : CountryLevel.values()) {
            countryLevels.add(level.name());
        }

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", status);
        model.addAttribute("size", size);
        model.addAttribute("q", q);
        model.addAttribute("countryLevels", countryLevels);

        return "admin/settings/countries/list";
    }

    /**
     *
     * @param model
     * @param request
     * @param code
     * @return
     */
    @RequestMapping(path = "/admin/settings/countries/enabled/{code}", method = RequestMethod.POST)
    public ResponseEntity<String> enable(Model model, HttpServletRequest request, @PathVariable("code") String code) {
        try {
            countriesService.enabled(code);
            return ResponseEntity.ok().body("Modificado con éxito");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error modificando al país.");
        }
    }

    /**
     * Get para el edit del form
     *
     * @param model
     * @param request
     * @param countryCode
     * @return
     */
    @RequestMapping(path = "admin/settings/countries/edit/{countryCode}", method = RequestMethod.GET)
    public ResponseEntity<CountryProperties> readEdit(Model model, HttpServletRequest request, @PathVariable("countryCode") String countryCode) {

        Country country = countriesService.getByCode(countryCode);
        if (!country.isEnabled()) {
            return ResponseEntity.badRequest().build();
        }

        CountryProperties props = countriesService.getPropertiesByCode(countryCode);
        if (props == null) {
            props = new CountryProperties();
            props.setCode(countryCode);
            props.setSortOrder(0);
            countriesService.saveProperties(props);
        }

        return ResponseEntity.ok(props);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(path = "/admin/settings/countries/edit", method = RequestMethod.POST)
    public ResponseEntity<String> writeEdit(Model model, HttpServletRequest request) {

        String code = request.getParameter("code");
        String availableStore = request.getParameter("availableStore");
        String availableBackoffice = request.getParameter("availableBackoffice");
        String currency = request.getParameter("currency");
        String sortOrder = request.getParameter("sortOrder");
        String defaultLang = request.getParameter("defaultLang");
        String defaultLocale = request.getParameter("defaultLocale");
        String flag = request.getParameter("flag");
        String exchangeRate = request.getParameter("exchangeRate");
        String populationLevel = request.getParameter("populationLevel");
        String iva1 = request.getParameter("iva1");
        String iva2 = request.getParameter("iva2");
        String iva3 = request.getParameter("iva3");
        String shipping1 = request.getParameter("shipping1");
        String shipping2 = request.getParameter("shipping2");
        String shipping3 = request.getParameter("shipping3");
        String shipping4 = request.getParameter("shipping4");
        String dnisList = request.getParameter("dnisList");
        String idtsList = request.getParameter("idtsList");
        String custom1 = request.getParameter("custom1");
        String custom2 = request.getParameter("custom2");
        String custom3 = request.getParameter("custom3");
        String custom4 = request.getParameter("custom4");
        String custom5 = request.getParameter("custom5");
        String countryLevel = request.getParameter("countryLevel");

        CountryProperties properties = countriesService.getPropertiesByCode(code);
        if (properties != null) {

            properties.setAvailableBackoffice(availableBackoffice != null && availableBackoffice.equals("on"));
            properties.setAvailableStore(availableStore != null && availableStore.equals("on"));
            properties.setCurrency(currency);
            try {
                properties.setSortOrder(Integer.valueOf(sortOrder));
            } catch (Exception e) {
            }
            properties.setCustom1(custom1);
            properties.setCustom2(custom2);
            properties.setCustom3(custom3);
            properties.setCustom4(custom4);
            properties.setCustom5(custom5);
            properties.setDefaultLang(defaultLang);
            properties.setDefaultLocale(defaultLocale);
            properties.setDnisList(dnisList);
            properties.setCountryLevel(countryLevel);
            try {
                properties.setExchangeRate(Double.parseDouble(exchangeRate));
            } catch (Exception e) {
            }
            try {
                properties.setPopulationLevel(Integer.parseInt(populationLevel));
            } catch (Exception e) {
            }
            properties.setFlag(flag);
            properties.setIdtsList(idtsList);
            try {
                properties.setIva1(Double.valueOf(iva1));
            } catch (Exception e) {
            }
            try {
                properties.setIva2(Double.valueOf(iva2));
            } catch (Exception e) {
            }
            try {
                properties.setIva3(Double.valueOf(iva3));
            } catch (Exception e) {
            }
            try {
                properties.setShipping1(Double.valueOf(shipping1));
            } catch (Exception e) {
                properties.setShipping1(null);
            }
            try {
                properties.setShipping2(Double.valueOf(shipping2));
            } catch (Exception e) {
                properties.setShipping2(null);
            }
            try {
                properties.setShipping3(Double.valueOf(shipping3));
            } catch (Exception e) {
                properties.setShipping3(null);
            }
            try {
                properties.setShipping4(Double.valueOf(shipping4));
            } catch (Exception e) {
                properties.setShipping4(null);
            }

            countriesService.saveProperties(properties);
            return ResponseEntity.ok("Almacenado correctamente");
        } else {
            return ResponseEntity.badRequest().body("Error al guardar!!");
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     * @deprecated
     */
    @Deprecated
    @RequestMapping(path = "/admin/settings/countries/create", method = RequestMethod.GET)
    public String create(Model model, HttpServletRequest request) {
        final String action = request.getParameter("action");
        Country country;
        if (action != null && !action.equals("create")) {
            final String code = request.getParameter("code");
            country = countriesService.getByCode(code);
        } else {
            country = new Country();
        }
        model.addAttribute("country", country);
        model.addAttribute("action", action);
        return "admin/settings/countries/country-create";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     * @deprecated
     */
    @Deprecated
    @RequestMapping(path = "/admin/settings/countries/create", method = RequestMethod.POST)
    public ResponseEntity<String> createPost(Model model, HttpServletRequest request) {

        final String action = request.getParameter("action");
        final String code = request.getParameter("code");
        final String oldCode = request.getParameter("oldCode");
        final String name = request.getParameter("name");
        final String status = request.getParameter("status");

        Result<Country> result = countriesService.insertOrUpdate(action, code, oldCode, name, status);
        model.addAttribute("country", result.getResult());
        model.addAttribute("action", action);

        if (result.isOk()) {
            return ResponseEntity.ok().body("Insertado/Modificado con éxito");
        }
        return ResponseEntity.badRequest().body("Hubo un error: " + result.toString());
    }

}
