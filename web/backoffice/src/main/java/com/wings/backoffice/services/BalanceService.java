/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.Differential;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.BalanceStatus;
import com.wings.backoffice.mlm.enums.BalanceType;
import com.wings.backoffice.mlm.enums.BonusType;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.BalanceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wings.backoffice.mlm.repos.DifferentialRepository;
import com.wings.backoffice.mlm.specs.BalanceSpecs;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class BalanceService {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private DifferentialRepository differentialBisRepository;
    @Autowired
    private BonusService bonusService;
    @Autowired
    private BalanceDAO balanceDAO;
    @Autowired
    private MembersService membersService;
    @Autowired
    private LogsService logsService;

    private final Logger log = LoggerFactory.getLogger(BalanceService.class);

    @Transactional
    public String processBalance() {

        log.error("Start processing balances");

        StringBuilder b = new StringBuilder();
        int kits = 0;
        int products = 0;
        int services = 0;

        List<Balance> toSave = new ArrayList<>();
        //process differentials
        List<Differential> diffs = differentialBisRepository.findAll();
        log.error("get differential");
        for (Differential diff : diffs) {

            if (diff.getQuantityPay() <= 0) {
                continue;
            }

            String control = "differential-" + diff.getReceiveUserId() + "-" + diff.getEventId();
            /*Balance already = balanceRepository.findByControl(control);
            if (already != null) {
                continue;
            }*/

            Balance balance = new Balance();
            balance.setAmount(diff.getQuantityPay());
            balance.setComment(diff.getComment());
            balance.setDescription(diff.getComment());
            balance.setMemberId(diff.getReceiveUserId());
            balance.setCreationDate(diff.getDate());
            //balance.setStatus(BalanceStatus.created.name());
            balance.setType(BalanceType.kit.name());
            balance.setOrderId(diff.getEventId());
            balance.setControl(control);
            balance.setReferenceId(diff.getId());
            if (diff.getMatching()) {
                balance.setBonusType(BonusType.matching.name());
            } else {
                balance.setBonusType(BonusType.differential.name());
            }

            balance.setEffective(true);
            balance.setStatus(BalanceStatus.active.name());

            toSave.add(balance);
        }

        log.error("Done parsing balances, deleteing old ones");
        balanceRepository.deleteByBonusType(BonusType.differential.name());
        balanceRepository.deleteByBonusType(BonusType.matching.name());
        balanceRepository.deleteByBonusType(BonusType.direct.name());
        balanceRepository.deleteByBonusType(BonusType.indirect.name());
        balanceRepository.deleteByBonusType(BonusType.residual.name());

        log.error("Saving new ones");
        toSave.forEach((balance) -> {
            balanceRepository.save(balance);
        });
        kits = toSave.size();
        toSave.clear();

        try {
            log.error("Products");
            //process products and services orders
            List<Order> orders = ordersService.findByTypeAndDeletedAndStatus(OrderType.product.name(), Boolean.FALSE, OrderStatus.paid.name());
            bonusService.clearCache();

            for (Order order : orders) {
                try {
                    if (order.getType().equals(OrderType.product.name())) {
                        List<Balance> toAdd = bonusService.getProductBalances(order);
                        toSave.addAll(toAdd);
                        products += toAdd.size();
                    } else if (order.getType().equals(OrderType.service.name())) {
                        List<Balance> toAdd = bonusService.getServiceBalances(order);
                        toSave.addAll(toAdd);
                        services += toAdd.size();
                    }
                } catch (Exception e) {
                    log.error("Error getting balances for order id " + order.getId() + " " + order.getType());
                }
            }

            //topups
            toSave.addAll(bonusService.proccessTopups());

            toSave.forEach((balance) -> {
                balanceRepository.save(balance);
            });
            log.error("Fin Products");

        } catch (Exception e) {
            b.append(e.getMessage()).append(System.lineSeparator());
            log.error("Error generating balances ", e);
        }

        b.append("Generados").append(System.lineSeparator());
        b.append("Kits:").append(kits).append(System.lineSeparator());
        b.append("Productos:").append(products).append(System.lineSeparator());
        b.append("Servicios:").append(services).append(System.lineSeparator());

        log.error("Done processing balances");

        return b.toString();
    }

    /**
     *
     * @return
     */
    public String activateProductsAndKitsBalances() {

        int marked = 0;
        List<Balance> balances = balanceRepository.findByStatusAndType(BalanceStatus.created.name(), BalanceType.kit.name());
        balances.addAll(balanceRepository.findByStatusAndType(BalanceStatus.created.name(), BalanceType.product.name()));
        for (Balance b : balances) {

            //mark all as effective
            b.setEffective(true);
            b.setStatus(BalanceStatus.active.name());
            /*Long orderId = b.getOrderId();
            List<Shipment> ship = shipmentsRepository.findByOrderIdAndStatus(orderId, ShipmentStatus.delivered.name());

            if (ship.isEmpty()) {
                log.debug("no se encontraron envios entregados para la orden " + orderId);
            }

            //just one order delivered is needed for the activation of balance
            for (Shipment shipment : ship) {
                if (DateUtils.getDiffDays(shipment.getDeliveryDate()) > 14) {
                    b.setEffective(true);
                    b.setStatus(BalanceStatus.active.name());
                    marked++;
                    break;
                } else {
                    log.debug("No cumple 14 dias");
                }
            }*/
        }

        balances.forEach((balance) -> {
            balanceRepository.save(balance);
        });

        return "Activados " + marked + " de " + balances.size();
    }

    /**
     *
     * @param memberId
     * @return
     */
    public BalanceSums getMemberBalance(Long memberId) {
        BalanceSums sums = balanceDAO.find(null, null, null, null, null, memberId, null);
        return sums;
    }

    /**
     *
     * @param o
     * @return
     */
    public Balance addBalance(Order o) {

        Member m = membersService.getOne(o.getMemberId());
        //transformo a euros
        Locale locale = CountryUtils.getLocaleForCountry(m.getCountry());
        double realAmount = NumberUtils.convertMoneyToEuros(o.getPaymentAmount(), locale);
        Date creatioDate = new Date();

        Balance b = new Balance();
        b.setAmount(realAmount * -1);
        b.setControl("discount-" + InvoiceUtils.getNumberRandomString(5));
        b.setCreationDate(creatioDate);
        b.setDescription(o.getStoreTitle());
        b.setMemberId(m.getId());
        b.setOrderId(o.getId());
        b.setReferenceId(null);
        b.setStatus(BalanceStatus.created.name());
        b.setStatusText("Balance created by " + m.getUsername() + " on " + DateUtils.formatNormalDateTime(creatioDate));
        b.setType(BalanceType.discount.name());
        b.setEffective(true);
        b.setComment(o.getStoreTitle());

        balanceRepository.save(b);

        return b;
    }

    /**
     *
     * @param balance
     */
    public void removeBalance(Balance balance) {
        balanceRepository.delete(balance);
    }

    /**
     *
     * @param balance
     * @param request
     */
    public void removeBalance(Balance balance, HttpServletRequest request) {

        String ip = AuthUtils.getIp(request);
        String modUser = AuthUtils.getAdminUser().getUsername();
        String message = "Deleted balance " + balance.toString();
        logsService.addGenericLog(ip, modUser, balance.getId(), "balance-deleted", "Balance deleted from admin page", message);

        balanceRepository.delete(balance);

    }

    /**
     *
     * @param id
     * @return
     */
    public Balance getOne(Long id) {
        return balanceRepository.getOne(id);
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param f
     * @param t
     * @param type
     * @param bonusType
     * @param memberId
     * @param country
     * @return
     */
    public Page<Balance> findAll(int page, int size, String q, Date f, Date t, String type, String bonusType, Long memberId, String country) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        return balanceRepository.findAll(BalanceSpecs.search(q, f, t, type, null, null, country), pr);
    }

    /**
     *
     * @param balance
     * @return
     */
    public Balance save(Balance balance) {
        return balanceRepository.save(balance);
    }

    /**
     *
     * @param balance
     * @param request
     * @return
     */
    public Balance create(Balance balance, HttpServletRequest request) {

        String ip = AuthUtils.getIp(request);
        String modUser = AuthUtils.getAdminUser().getUsername();
        String message = "Created balance " + balance.toString();
        logsService.addGenericLog(ip, modUser, balance.getId(), "balance-created", "Balance created from admin page", message);

        return balanceRepository.save(balance);
    }

    /**
     *
     * @param balances
     * @return
     */
    public List<Balance> save(List<Balance> balances) {
        return balanceRepository.save(balances);
    }
}
