/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web;

import com.wings.backoffice.utils.AuthUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author seba
 */
@ControllerAdvice
public class AppErrorHandlingController {

    private final Logger log = LoggerFactory.getLogger(AppErrorHandlingController.class);

    @ExceptionHandler(Exception.class)
    public String handleError(Model model, HttpServletRequest request, Exception ex, HttpServletResponse response) {
        String ip = AuthUtils.getIp(request);
        try {
            log.error("Request: " + ip + " " + request.getRequestURL() + " raised " + ex.getMessage() + " by " + ex.getClass().getName() + " on "
                    + ((ex.getStackTrace() != null && ex.getStackTrace().length > 0) ? ex.getStackTrace()[0].toString() : "")
            );
        } catch (Exception e) {
            log.error("Error handling " + e.toString());
        }
        return null;
    }

    /*@ExceptionHandler(IOException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)   //(1)
    public Object exceptionHandler(IOException e, HttpServletRequest request) {
        if (StringUtils.containsIgnoreCase(ExceptionUtils.getRootCauseMessage(e), "Broken pipe")) {   //(2)
            return null;        //(2)	socket is closed, cannot return any response    
        } else {
            return new HttpEntity<>(e.getMessage());  //(3)
        }
    }*/
}
