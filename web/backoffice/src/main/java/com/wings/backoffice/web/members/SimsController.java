/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberSim;
import com.wings.backoffice.mlm.domain.Sim;
import com.wings.backoffice.services.MemberSimsService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.PageWrapper;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class SimsController {

    @Autowired
    private MemberSimsService membersimsService;
    @Autowired
    private MembersService membersService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/tools/sims"}, method = RequestMethod.GET)
    public String getSimsEnabled(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<MemberSim> items = membersimsService.getMemberSims(member.getId(), q, page, size);
        PageWrapper<MemberSim> pageWrapper = new PageWrapper<>(items, "members/tools/sims");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);
        model.addAttribute("q", q);

        long allowed = membersimsService.getMemberSimsCountAllowed(member.getId());
        if (items.getTotalElements() != 0) {
            allowed = 1;
        }
        model.addAttribute("allowed", allowed);

        return "members/tools/sims";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "members/tools/sims-add", method = RequestMethod.GET)
    public String getAddSim(Model model, HttpServletRequest request) {
        return "members/tools/sims-add";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "members/tools/sims-add", method = RequestMethod.POST)
    public ResponseEntity<String> postAddSim(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        long allowed = membersimsService.getMemberSimsCountAllowed(member.getId());
        long asigned = membersimsService.getMemberSimCount(member.getId());

        //No permitir que un miembro pueda asignarse más tarjetas sim que las que tiene pagadas en orders
        if (allowed <= asigned) {
            return ResponseEntity.badRequest().body("Ha superado el límite de " + allowed + " Sims asignadas");
        }

        String iccid = request.getParameter("iccid");
        if (iccid == null || iccid.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("El iccid es inválido");
        }

        String[] iccids = iccid.trim().split(",");
        for (String i1 : iccids) {
            if (!(i1.length() == 18 || i1.length() == 19)) {
                return ResponseEntity.badRequest().body("El iccid " + i1 + " es inválido");
            }
        }

        List<Sim> sims = new ArrayList<>();
        for (String i1 : iccids) {
            String luhnIccid = membersimsService.addLuhnVerificationNumber(i1.length() == 18 ? i1 : i1.substring(0, 18));
            Sim sim = membersimsService.getSimsByIccid(luhnIccid);
            if (sim == null) {
                return ResponseEntity.badRequest().body("El iccid es inválido");
            }
            MemberSim memberSim = membersimsService.getMemberSimIdByIccid(i1.length() == 18 ? i1 : i1.substring(0, 18));
            if (memberSim != null) {
                return ResponseEntity.badRequest().body("El iccid ya esta asignado, si tiene algun inconveniente , comuniquese con atención al cliente");
            }
            sims.add(sim);
        }

        if (sims.isEmpty()) {
            return ResponseEntity.badRequest().body("El iccid es inválido");
        }

        Member m = AuthUtils.getMemberUser();
        List<MemberSim> mss = new ArrayList<>();

        for (Sim sim : sims) {
            MemberSim ms = new MemberSim();

            ms.setActivationDate(null);
            ms.setCreationDate(new Date());
            ms.setDeleted(false);
            ms.setDeletedDate(null);
            ms.setIccid(sim.getIccid().substring(0, 18));
            ms.setIccidId(sim.getId());
            ms.setMemberId(m.getId());
            ms.setStatus("created");

            mss.add(ms);
        }

        membersimsService.save(mss);
        if (mss.size() == 1) {
            return ResponseEntity.ok().body("Iccid insertado");
        } else {
            return ResponseEntity.ok().body("Iccid insertadas");
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "members/tools/sims-transfer", method = RequestMethod.GET)
    public String getTransferSim(Model model, HttpServletRequest request) {
        String sims = request.getParameter("sims");
        List<Long> ids = new ArrayList<>();
        String[] splited = sims.split(",");
        for (String id : splited) {
            try {
                if (id.trim().isEmpty()) {
                    continue;
                }
                ids.add(Long.valueOf(id));
            } catch (Exception e) {
            }
        }

        List<MemberSim> simList = membersimsService.getMemberSimsByIds(ids);
        model.addAttribute("sims", simList);
        model.addAttribute("simsIds", ids.toString());

        return "members/tools/sims-transfer";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "members/tools/sims-transfer", method = RequestMethod.POST)
    public ResponseEntity<String> postTransferSim(Model model, HttpServletRequest request) {

        String recipient = request.getParameter("recipient");
        if (recipient == null || recipient.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        Member dest = membersService.findByUsername(recipient);
        if (dest == null) {
            dest = membersService.findByEmail(recipient);
        }
        if (dest == null) {
            return ResponseEntity.badRequest().body("No se encontro el afiliado");
        }

        String simsIds = request.getParameter("simsIds");
        simsIds = simsIds.replaceAll("\\[", "").replaceAll("]", "");
        String[] ids = simsIds.split(",");

        List<Long> lids = new ArrayList<>();
        for (String id : ids) {
            try {
                lids.add(Long.valueOf(id.trim()));
            } catch (Exception e) {
            }
        }
        if (lids.isEmpty()) {
            return ResponseEntity.badRequest().body("Iccids invalidos");
        }

        Member member = AuthUtils.getMemberUser();
        List<MemberSim> msims = membersimsService.getMemberSimsByIds(lids);
        for (MemberSim msim : msims) {
            if (!msim.getMemberId().equals(member.getId())) {
                return ResponseEntity.ok().body("Iccid inválidas");
            }

            msim.setTransferMemberId(dest.getId());
            msim.setTransferMemberName(dest.toString());
        }
        membersimsService.save(msims);

        return ResponseEntity.ok().body("Iccid transferidas");
    }

}
