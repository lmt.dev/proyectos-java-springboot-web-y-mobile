/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.utils.AuthUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class SettingsController {

    @Autowired
    private MembersService membersService;

    @RequestMapping(value = {"/members/settings"}, method = {RequestMethod.GET})
    public String getSettings(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        model.addAttribute("member", member);
                
        return "members/settings/settings";

    }

    @RequestMapping(value = {"/members/settings/edit/{section}/{id}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postEditSettings(Model model, HttpServletRequest request, @PathVariable("section") String section, @PathVariable("id") Long memberId) {

        Member member = AuthUtils.getMemberUser();
        if (!member.getId().equals(memberId)) {
            return ResponseEntity.ok("Ocurrió un error modificando la configuración");
        }

        switch (section) {
            case "payments":

                String paymentEnabled = request.getParameter("paymentsEnabled");
                member = membersService.getOne(memberId);
                member.setPaymentsEnabled(paymentEnabled != null && paymentEnabled.equals("on"));

                //save
                membersService.save(member);

                break;
            default:

        }

        return ResponseEntity.ok("Configuración modificada correctamente.");

    }

}
