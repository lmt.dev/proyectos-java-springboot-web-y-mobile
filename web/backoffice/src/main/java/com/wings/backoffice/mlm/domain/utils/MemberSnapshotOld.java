/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.utils;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.bis.SpeedBonus;
import com.wings.backoffice.utils.DateUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class MemberSnapshotOld implements Serializable {

    private static final long serialVersionUID = -6329619034514303916L;

    private Member member;
    private RankBis nextRank;
    private Date snapshotDate;
    private Map<Long, Double> sumsPerBranch;
    private Long mostValuableBranch;
    private Double kitsPv;
    private Double productsPv;
    private Double servicesPv;
    private Double personalServicePv;
    private Map<Long, Double> pointsPerChild = new HashMap<>();

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Date getSnapshotDate() {
        return snapshotDate;
    }

    public void setSnapshotDate(Date snapshotDate) {
        this.snapshotDate = snapshotDate;
    }

    public Map<Long, Double> getSumsPerBranch() {
        return sumsPerBranch;
    }

    public void setSumsPerBranch(Map<Long, Double> sumsPerBranch) {
        this.sumsPerBranch = sumsPerBranch;
    }

    public Long getMostValuableBranch() {
        return mostValuableBranch;
    }

    public void setMostValuableBranch(Long mostValuableBranch) {
        this.mostValuableBranch = mostValuableBranch;
    }

    public RankBis getNextRank() {
        return nextRank;
    }

    public void setNextRank(RankBis nextRank) {
        this.nextRank = nextRank;
    }

    public Double getSum6040(boolean isInSpeedBonus) {

        if (mostValuableBranch == null) {
            return 0.0;
        }

        double sum = 0.0;

        long required = nextRank.getQuantity();
        if (isInSpeedBonus && nextRank.getSbQuantity() > 0) {
            required = nextRank.getSbQuantity();
        }

        Double mostVal = sumsPerBranch.get(mostValuableBranch);
        Double sixty = required * 0.60;
        if (mostVal > sixty) {
            sum += sixty;
        } else {
            sum = mostVal;
        }

        for (Map.Entry<Long, Double> entry : sumsPerBranch.entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            if (!key.equals(mostValuableBranch)) {
                sum += value;
            }
        }

        return sum;
    }

    public void setProductsPv(Double productsPv) {
        this.productsPv = productsPv;
    }

    public Double getProductsPv() {
        return productsPv;
    }

    public Double getServicesPv() {
        return servicesPv;
    }

    public void setServicesPv(Double servicesPv) {
        this.servicesPv = servicesPv;
    }

    public Double getPersonalServicePv() {
        return personalServicePv;
    }

    public void setPersonalServicePv(Double personalServicePv) {
        this.personalServicePv = personalServicePv;
    }

    public Map<Long, Double> getPointsPerChild() {
        return pointsPerChild;
    }

    public void setPointsPerChild(Map<Long, Double> pointsPerChild) {
        this.pointsPerChild = pointsPerChild;
    }

    public void setKitsPv(Double kitsPv) {
        this.kitsPv = kitsPv;
    }

    public Double getKitsPv() {
        return kitsPv;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();

        b.append("<pre>");
        b.append("Afiliado: ").append(member).append(" (#").append(member.getId()).append(")").append(System.lineSeparator());
        b.append("Fecha: ").append(snapshotDate).append(System.lineSeparator());
        b.append("Kits: ").append(kitsPv).append(System.lineSeparator());
        b.append("Productos: ").append(productsPv).append(System.lineSeparator());
        b.append("Servicios: ").append(servicesPv).append(System.lineSeparator());
        b.append("Personal PV: ").append(personalServicePv).append(System.lineSeparator());

        b.append("Ramas:").append(System.lineSeparator());
        for (Map.Entry<Long, Double> entry : sumsPerBranch.entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            b.append("\t ID: ").append(key).append("->").append(value).append(System.lineSeparator());
        }
        b.append("Rama mas valiosa: ").append(mostValuableBranch).append(System.lineSeparator());
        b.append("Rango siguiente: ").append(nextRank.getName()).append(" ").append(getNextRank().getQuantity()).append(System.lineSeparator());
        b.append("Regla 60/40: ").append(getSum6040(true)).append(System.lineSeparator());
        b.append("Speed Bonus: ").append(isInSpeedBonus()).append(System.lineSeparator());

        b.append("</pre>");

        return b.toString();
    }

    public String getNextRankNeeded() {
        return nextRank.getQuantity().toString();
    }

    public String getNextRankText() {
        return nextRank.getName();
    }

    public String getNextRankAchieved() {
        return getSum6040(true).toString();
    }

    public String getNextRankPercentage() {
        int percent = (int) ((getSum6040(true) * 100) / nextRank.getQuantity());
        return percent + "%";
    }

    public String getNextRankPercentageText() {
        return (getSum6040(true).longValue()) + " (60/40) / " + nextRank.getQuantity();
    }

    public boolean isInSpeedBonus() {
        SpeedBonus speed = member.getSpeedBonus();

        long days;
        long daysFounder;

        if (speed.getDurationDays() == 0) {
            //por fecha
            days = DateUtils.getDiffDays(member.getRegistrationDate(), speed.getDurationTo());
            daysFounder = DateUtils.getDiffDays(member.getRegistrationDate(), speed.getDurationToFounder());
        } else {
            //por dias
            days = speed.getDurationDays();
            daysFounder = speed.getDurationDaysFounder();
        }

        long sinceRegistered = DateUtils.getDiffDays(member.getRegistrationDate(), snapshotDate);
        if (member.isFounder()) {
            return sinceRegistered < daysFounder;
        } else {
            return sinceRegistered < days;
        }
    }
}
