/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.repos;

import com.wings.backoffice.store.domain.WpOrderItemmeta;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface WpOrderItemmetasRepository extends JpaRepository<WpOrderItemmeta, Long>{
    
    List<WpOrderItemmeta> findByOrderItemId(Long orderItemId);
    
}
