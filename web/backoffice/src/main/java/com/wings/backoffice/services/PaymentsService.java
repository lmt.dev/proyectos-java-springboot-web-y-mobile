/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.payments.client.WingsPay;
import com.wings.payments.client.dto.RequestDTO;
import com.wings.payments.client.dto.RequestDTOExtra;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PaymentsService {

    @Autowired
    private Environment environment;
    @Autowired
    private OrdersService ordersService;
    @Value("${wings.site.url}")
    private String url;

    public String createPayment(Long orderId) {
        return createPayment(ordersService.getOrder(orderId));
    }

    public String createPayment(Order order) {

        String cancelUrl = url + "/members#views/main";
        String cancelUrlTickets = url + "/members#members/events/tickets";
        String callbackUrl = url + "/api/payments/callback/";

        String env = WingsPay.ENVIRONMENT_TEST;
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("prod")) {
                env = WingsPay.ENVIRONMENT_PROD;
            } else if (profile.contains("desa")) {
                env = WingsPay.ENVIRONMENT_DEV;
            }
        }

        WingsPay.initEnv(env);

        RequestDTO dto = new RequestDTO();
        dto.setAmount(order.getPaymentAmount());
        dto.setBillingAddress(order.getBillingAddress());
        dto.setBillingCity(order.getBillingCity());
        dto.setBillingCountry(order.getBillingCountry());
        dto.setBillingEmail(order.getPayerEmail());
        dto.setBillingFirstName(order.getFirstName());
        dto.setBillingLastName(order.getLastName());
        dto.setBillingState(order.getBillingState());
        dto.setCountry(order.getBillingCountry());
        dto.setCurrency(order.getPaymentCurrency());
        dto.setExcludeBacs(order.getType().equals(OrderType.topup.name()));
        dto.setOrderNumber(order.getId());
        dto.setRequesterName("backoffice");
        dto.setTitle(order.getStoreTitle() != null ? order.getStoreTitle() : order.getOrderItem().getName());
        dto.setDescription(order.getOrderItem().getDescription());
        dto.setImage(order.getOrderItem().getImage());
        dto.setComsDiscountAmount(order.getPriceForComs());

        //extras
        List<RequestDTOExtra> extras = new ArrayList<>();
        if (order.getStoreTitle() != null && (order.getStoreTitle().contains("3C") || order.getStoreTitle().contains("3 cuotas"))) {
            RequestDTOExtra ex = new RequestDTOExtra();
            ex.setExtraName("Cuota:");
            ex.setExtraValue(order.getMessage());
            extras.add(ex);
        }
        dto.setExtras(extras);

        //if it's from tickets go back to tickets
        if (order.getType().equals(OrderType.ticket.name())) {
            cancelUrl = cancelUrlTickets;
        }

        dto.setCallbackUrl(callbackUrl);
        dto.setCancelUrl(cancelUrl);

        //hacer el post a la pasarela
        return WingsPay.createPaymentRequest(dto);
    }

}
