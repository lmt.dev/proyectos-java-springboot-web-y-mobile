/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.invoices.peru;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class Emisor {

    @JsonProperty("ruc")
    private String ruc;
    @JsonProperty("tipo_doc")
    private String tipoDoc;
    @JsonProperty("nom_comercial")
    private String nomComercial;
    @JsonProperty("razon_social")
    private String razonSocial;
    @JsonProperty("codigo_ubigeo")
    private String codigoUbigeo;
    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("direccion_departamento")
    private String direccionDepartamento;
    @JsonProperty("direccion_provincia")
    private String direccionProvincia;
    @JsonProperty("direccion_distrito")
    private String direccionDistrito;
    @JsonProperty("direccion_codigopais")
    private String direccionCodigoPais;
    @JsonProperty("usuariosol")
    private String usuarioSol;
    @JsonProperty("clavesol")
    private String claveSol;

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNomComercial() {
        return nomComercial;
    }

    public void setNomComercial(String nomComercial) {
        this.nomComercial = nomComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCodigoUbigeo() {
        return codigoUbigeo;
    }

    public void setCodigoUbigeo(String codigoUbigeo) {
        this.codigoUbigeo = codigoUbigeo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccionDepartamento() {
        return direccionDepartamento;
    }

    public void setDireccionDepartamento(String direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    public String getDireccionProvincia() {
        return direccionProvincia;
    }

    public void setDireccionProvincia(String direccionProvincia) {
        this.direccionProvincia = direccionProvincia;
    }

    public String getDireccionDistrito() {
        return direccionDistrito;
    }

    public void setDireccionDistrito(String direccionDistrito) {
        this.direccionDistrito = direccionDistrito;
    }

    public String getDireccionCodigoPais() {
        return direccionCodigoPais;
    }

    public void setDireccionCodigoPais(String direccionCodigoPais) {
        this.direccionCodigoPais = direccionCodigoPais;
    }

    public String getUsuarioSol() {
        return usuarioSol;
    }

    public void setUsuarioSol(String usuarioSol) {
        this.usuarioSol = usuarioSol;
    }

    public String getClaveSol() {
        return claveSol;
    }

    public void setClaveSol(String claveSol) {
        this.claveSol = claveSol;
    }

}
