/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface CustomersRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {

    List<Customer> findByStoreOwner(String storeOwner);
    
    List<Customer> findByPayerEmailAndStoreOwner(String payerEmail,String storeOwner);
    
    List<Customer> findByPayerEmail(String payerEmail);
    
    List<Customer> findByDniIgnoreCase(String dni);
    
    
}
