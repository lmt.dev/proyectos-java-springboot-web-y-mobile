package com.wings.backoffice.mlm.dao;

import com.wings.backoffice.mlm.domain.KeyValue;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas
 */
@Service
public class CountryDAO {
    @Autowired
    @Qualifier(value = "entityManagerFactory")
    private EntityManager em;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public List<KeyValue<String,String>> find() {
        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.backoffice.mlm.domain.KeyValue(c.code, c.nameEs) ");
        b.append("FROM Country as c ");
        b.append("WHERE enabled = 1"); 

        b.append(" ORDER BY c.code");
        Query query = em.createQuery(b.toString(), KeyValue.class);
        final List<KeyValue<String,String>> countries = (List<KeyValue<String,String>>) query.getResultList();

        return countries;
    }
}
