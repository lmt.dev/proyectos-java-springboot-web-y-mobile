/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.services;

import com.wings.backoffice.marketing.services.SyncroService;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.OrderItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemName;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemOption;
import com.wings.backoffice.mlm.enums.OrderPaymentStatus;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.enums.ShipmentStatus;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.OrderItemsRepository;
import com.wings.backoffice.mlm.repos.StoreItemNamesRepository;
import com.wings.backoffice.mlm.repos.StoreItemOptionsRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.services.EmailService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.SpeedBonusService;
import com.wings.backoffice.store.domain.WpOrderItem;
import com.wings.backoffice.store.domain.Post;
import com.wings.backoffice.store.domain.Referrals;
import com.wings.backoffice.store.domain.User;
import com.wings.backoffice.store.domain.WpOrderItemmeta;
import com.wings.backoffice.store.enums.PostMetaKeys;
import com.wings.backoffice.store.enums.PostStatus;
import com.wings.backoffice.store.enums.PostType;
import com.wings.backoffice.store.repos.PostsRepository;
import com.wings.backoffice.store.repos.ReferralsRepository;
import com.wings.backoffice.store.repos.UsersRepository;
import com.wings.backoffice.store.repos.WpOrderItemmetasRepository;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class StoreService {

    @Autowired
    private PostsRepository postsRepository;
    @Autowired
    private WpOrderItemmetasRepository wpOrderItemmetasRepository;
    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private StoreItemNamesRepository storeItemNamesRepository;
    @Autowired
    private StoreItemOptionsRepository storeItemOptionsRepository;
    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ReferralsRepository referralsRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private SpeedBonusService speedBonusService;
    @Autowired
    private SyncroService syncroService;

    private final Logger log = LoggerFactory.getLogger(StoreService.class);

    private final Map<String, String> esStates = new HashMap<>();

    public StoreService() {
        esStates.put("C", "A Coruña");
        esStates.put("VI", "Araba/álava");
        esStates.put("AB", "Albacete");
        esStates.put("A", "Alicante");
        esStates.put("AL", "Almería");
        esStates.put("O", "Asturias");
        esStates.put("AV", "Ávila");
        esStates.put("BA", "Badajoz");
        esStates.put("PM", "Baleares");
        esStates.put("B", "Barcelona");
        esStates.put("BU", "Burgos");
        esStates.put("CC", "Cáceres");
        esStates.put("CA", "Cádiz");
        esStates.put("S", "Cantabria");
        esStates.put("CS", "Castellón");
        esStates.put("CE", "Ceuta");
        esStates.put("CR", "Ciudad Real");
        esStates.put("CO", "Córdoba");
        esStates.put("CU", "Cuenca");
        esStates.put("GI", "Girona");
        esStates.put("GR", "Granada");
        esStates.put("GU", "Guadalajara");
        esStates.put("SS", "Gipuzkoa");
        esStates.put("H", "Huelva");
        esStates.put("HU", "Huesca");
        esStates.put("J", "Jaén");
        esStates.put("LO", "La Rioja");
        esStates.put("GC", "Las Palmas");
        esStates.put("LE", "León");
        esStates.put("L", "Lleida");
        esStates.put("LU", "Lugo");
        esStates.put("M", "Madrid");
        esStates.put("MA", "Málaga");
        esStates.put("ML", "Melilla");
        esStates.put("MU", "Murcia");
        esStates.put("NA", "Navarra");
        esStates.put("OR", "Ourense");
        esStates.put("P", "Palencia");
        esStates.put("PO", "Pontevedra");
        esStates.put("SA", "Salamanca");
        esStates.put("TF", "Santa Cruz de Tenerife");
        esStates.put("SG", "Segovia");
        esStates.put("SE", "Sevilla");
        esStates.put("SO", "Soria");
        esStates.put("T", "Tarragona");
        esStates.put("TE", "Teruel");
        esStates.put("TO", "Toledo");
        esStates.put("V", "Valencia");
        esStates.put("VA", "Valladolid");
        esStates.put("BI", "Bizkaia");
        esStates.put("ZA", "Zamora");
        esStates.put("Z", "Zaragoza");
    }

    /**
     *
     * @return
     */
    public String importOrders() {
        return getOrdersProcessing();
    }

    /**
     * Process one order at a time, this is used for callbacks from wordpress
     *
     * @param orderId
     */
    public void processOrder(Long orderId) {

        Post post = postsRepository.findOne(orderId);
        if (post == null) {
            log.error("Order not found in store with id " + orderId);
            return;
        }

        List<String> types = new ArrayList<>();
        types.add(OrderType.kit.name());
        types.add(OrderType.product.name());
        types.add(OrderType.store.name());
        types.add(OrderType.upgrade.name());

        try {
            //already exists
            String source = getSource(post);
            Order already = ordersService.findFirstByExternalIdAndTypeInAndSource(post.getId(), types, source);
            if (already != null) {
                log.error("Order already exists for id " + orderId);

                switch (post.getPostStatus()) {
                    case PostStatus.PROCESSING:
                    case PostStatus.COMPLETED:
                        already.setStatus(OrderStatus.paid.name());
                        already.setPaymentStatus(OrderPaymentStatus.paid.name());
                        already.setTypeStatus(OrderPaymentStatus.paid.name());
                        already.setActive(true);
                        already.setActivationDate(post.getPostDate());
                        break;
                    /*case PostStatus.ONHOLD:
                    case PostStatus.FAILED:
                    case PostStatus.CANCELED:
                        break;
                    case PostStatus.REFUNDED:
                        already.setStatus(OrderStatus.pending.name());
                        already.setPaymentStatus(OrderPaymentStatus.pending.name());
                        already.setTypeStatus(OrderPaymentStatus.pending.name());
                        already.setActive(false);
                        already.setActivationDate(null);
                        //TODO: aviso por email para que autorizen el pago
                        break;
                    default:
                        already.setStatus(OrderStatus.pending.name());
                        already.setPaymentStatus(OrderPaymentStatus.pending.name());
                        already.setTypeStatus(OrderPaymentStatus.pending.name());
                        break;*/
                }

                ordersService.save(already);

                return;
            }

            //separar el solo el kit y crear los pedidos para los otros items
            boolean multiple = false;
            if (post.getItems().size() > 2) {
                multiple = true;
            }

            String prizeText = "";
            String storeTitle = "";
            String orderType = "";
            List<OrderItem> orderItems = new ArrayList<>();
            StoreItem kit = null;
            StoreItem product = null;
            for (WpOrderItem item : post.getItems()) {
                if (item.getOrderItemType().equals("line_item")) {

                    storeTitle += item.getOrderItemName() + System.lineSeparator();
                    String prize = priceText(item.getOrderItemName());
                    if (prize != null) {
                        prizeText += prize + " ";
                    }

                    StoreItem storeItem = getStoreItem(source, item);
                    if (storeItem == null) {
                        log.error("Order sin item encontrado " + post.getId() + " " + item.getOrderItemName());

                    } else {

                        if (storeItem.getCategory() == 1 || storeItem.getCategory() == 5) {
                            kit = storeItem;
                        } else {
                            product = storeItem;
                        }
                        orderItems.addAll(getItemsForOrder(item, item.getOrderItemName(), storeItem.getId()));

                        String orderTypeTemp = getOrderTypeFromCategory(storeItem);
                        if (!orderType.isEmpty() && !orderType.equals(orderTypeTemp)) {
                            orderType = OrderType.mix.name();
                        } else {
                            orderType = orderTypeTemp;
                        }
                    }
                }
            }

            Order o = new Order();
            o.setCreationDate(post.getPostDate());
            o.setExternalId(post.getId());
            o.setFirstName(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_FIRST_NAME)));
            o.setLastName(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_LAST_NAME)));
            o.setPayerEmail(StringUtils.toLowerCase(post.getMetaMap().get(PostMetaKeys.BILLING_EMAIL)));

            o.setPaymentAmount(Double.valueOf(post.getMetaMap().get(PostMetaKeys.ORDER_TOTAL)));
            o.setPaymentCurrency(post.getMetaMap().get(PostMetaKeys.ORDER_CURRENCY));
            o.setPaymentType(post.getMetaMap().get(PostMetaKeys.PAYMENT_METHOD));
            o.setStoreTitle(storeTitle);
            o.setType(orderType);
            o.setItemId(kit != null ? kit.getId() : product.getId());
            o.setOrderItem(kit);
            o.setShippingStatus(ShipmentStatus.created.name());
            o.setMultiple(multiple);
            o.setSource(source);
            o.setPrizeText(prizeText);
            o.setWithPrize(!prizeText.isEmpty());

            o.setBillingCompany(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_COMPANY)));
            o.setBillingAddress(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_ADDRESS_1)) + " " + StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_ADDRESS_2)));
            o.setBillingCity(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_CITY)));
            o.setBillingCountry(post.getMetaMap().get(PostMetaKeys.BILLING_COUNTRY));
            o.setBillingName(o.getFirstName() + " " + o.getLastName());
            o.setBillingPostalCode(post.getMetaMap().get(PostMetaKeys.BILLING_POST_CODE));

            //if has cuotas, this is the first one
            if (kit != null && kit.isInstallments()) {
                o.setInstallmentNumber(1);
            }

            String state = post.getMetaMap().get(PostMetaKeys.BILLING_STATE);
            if (esStates.containsKey(state)) {
                state = esStates.get(state);
            }
            o.setBillingState(StringUtils.capitalize(state));

            switch (post.getPostStatus()) {
                case PostStatus.PROCESSING:
                case PostStatus.COMPLETED:
                    o.setStatus(OrderStatus.paid.name());
                    o.setPaymentStatus(OrderPaymentStatus.paid.name());
                    o.setTypeStatus(OrderPaymentStatus.paid.name());
                    o.setActive(true);
                    o.setActivationDate(post.getPostDate());
                    break;
                case PostStatus.ONHOLD:
                case PostStatus.FAILED:
                case PostStatus.CANCELED:
                case PostStatus.REFUNDED:
                    o.setStatus(OrderStatus.pending.name());
                    o.setPaymentStatus(OrderPaymentStatus.pending.name());
                    o.setTypeStatus(OrderPaymentStatus.pending.name());
                    //TODO: aviso por email para que autorizen el pago
                    break;
                default:
                    o.setStatus(OrderStatus.review.name());
                    o.setPaymentStatus(OrderPaymentStatus.pending.name());
                    o.setTypeStatus(OrderPaymentStatus.pending.name());
                    break;
            }

            //referrer
            String referer = post.getMetaMap().get(PostMetaKeys.BILLING_REFERER);
            if (referer != null) {
                o.setStoreId(referer);
            } else {
                Referrals ref = referralsRepository.findByReference(post.getId().toString());
                if (ref != null) {
                    o.setStoreId(String.valueOf(ref.getAffiliateId()));
                } else {
                    log.error("Atencion: pedido sin referer", post.getId());
                }
            }

            ordersService.save(o);
            orderItems.forEach((orderItem) -> {
                orderItem.setOrderId(o.getId());
            });
            orderItemsRepository.save(orderItems);

            //create member
            createMember(o, post);

        } catch (Exception e) {
            log.error("Error importando desde store ", e);
        }
    }

    /**
     * Import order from wordpress, skips the ones already imported
     *
     * @return
     */
    public String getOrdersProcessing() {

        StringBuilder b = new StringBuilder();
        int count = 0;
        List<Post> posts = postsRepository.findByPostTypeAndPostStatus(PostType.SHOP_ORDER, PostStatus.PROCESSING);
        posts.addAll(postsRepository.findByPostTypeAndPostStatus(PostType.SHOP_ORDER, PostStatus.ONHOLD));

        List<String> types = new ArrayList<>();
        types.add(OrderType.kit.name());
        types.add(OrderType.product.name());
        types.add(OrderType.store.name());
        types.add(OrderType.upgrade.name());

        try {

            for (Post post : posts) {

                String source = getSource(post);
                log.error("Procesando orden " + post.getId());
                //already exists
                Order already = ordersService.findFirstByExternalIdAndTypeInAndSource(post.getId(), types, source);
                if (already != null) {
                    continue;
                }

                //separar el solo el kit y crear los pedidos para los otros items
                boolean multiple = false;
                if (post.getItems().size() > 2) {
                    multiple = true;
                }

                String prizeText = "";
                String storeTitle = "";
                String orderType = "";
                List<OrderItem> orderItems = new ArrayList<>();
                StoreItem kit = null;
                StoreItem product = null;
                for (WpOrderItem item : post.getItems()) {
                    if (item.getOrderItemType().equals("line_item")) {

                        storeTitle += item.getOrderItemName() + System.lineSeparator();
                        String prize = priceText(item.getOrderItemName());
                        if (prize != null) {
                            prizeText += prize + " ";
                        }

                        StoreItem storeItem = getStoreItem(source, item);
                        if (storeItem == null) {
                            b.append("Order sin item encontrado ").append(post.getId()).append(System.lineSeparator());
                        } else {

                            if (storeItem.getCategory() == 1 || storeItem.getCategory() == 5) {
                                kit = storeItem;
                            } else {
                                product = storeItem;
                            }
                            orderItems.addAll(getItemsForOrder(item, item.getOrderItemName(), storeItem.getId()));

                            String orderTypeTemp = getOrderTypeFromCategory(storeItem);
                            if (!orderType.isEmpty() && !orderType.equals(orderTypeTemp)) {
                                orderType = OrderType.mix.name();
                            } else {
                                orderType = orderTypeTemp;
                            }
                        }
                    }
                }

                Order o = new Order();
                o.setCreationDate(post.getPostDate());
                o.setExternalId(post.getId());

                o.setFirstName(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_FIRST_NAME)));
                o.setLastName(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_LAST_NAME)));
                o.setPayerEmail(StringUtils.toLowerCase(post.getMetaMap().get(PostMetaKeys.BILLING_EMAIL)));

                o.setPaymentAmount(Double.valueOf(post.getMetaMap().get(PostMetaKeys.ORDER_TOTAL)));
                o.setPaymentCurrency(post.getMetaMap().get(PostMetaKeys.ORDER_CURRENCY));
                o.setPaymentType(post.getMetaMap().get(PostMetaKeys.PAYMENT_METHOD));
                o.setStoreTitle(storeTitle);
                o.setType(orderType);
                o.setItemId(kit != null ? kit.getId() : product.getId());
                o.setOrderItem(kit);
                o.setShippingStatus(ShipmentStatus.created.name());
                o.setMultiple(multiple);
                o.setSource(source);
                o.setPrizeText(prizeText);
                o.setWithPrize(!prizeText.isEmpty());

                o.setBillingCompany(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_COMPANY)));
                o.setBillingAddress(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_ADDRESS_1)) + " " + StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_ADDRESS_2)));
                o.setBillingCity(StringUtils.capitalize(post.getMetaMap().get(PostMetaKeys.BILLING_CITY)));
                o.setBillingCountry(post.getMetaMap().get(PostMetaKeys.BILLING_COUNTRY));
                o.setBillingName(o.getFirstName() + " " + o.getLastName());
                o.setBillingPostalCode(post.getMetaMap().get(PostMetaKeys.BILLING_POST_CODE));

                //if has cuotas, this is the first one
                if (kit != null && kit.isInstallments()) {
                    o.setInstallmentNumber(1);
                }

                String state = post.getMetaMap().get(PostMetaKeys.BILLING_STATE);
                if (esStates.containsKey(state)) {
                    state = esStates.get(state);
                }
                o.setBillingState(StringUtils.capitalize(state));

                switch (post.getPostStatus()) {
                    case PostStatus.PROCESSING:
                        o.setStatus(OrderStatus.paid.name());
                        o.setPaymentStatus(OrderPaymentStatus.paid.name());
                        o.setTypeStatus(OrderPaymentStatus.paid.name());
                        o.setActive(true);
                        o.setActivationDate(post.getPostDate());
                        break;
                    case PostStatus.ONHOLD:

                        o.setStatus(OrderStatus.pending.name());
                        o.setPaymentStatus(OrderPaymentStatus.pending.name());
                        o.setTypeStatus(OrderPaymentStatus.pending.name());

                        //TODO: aviso por email para que autorizen el pago
                        break;
                    default:
                        o.setStatus(OrderStatus.review.name());
                        o.setPaymentStatus(OrderPaymentStatus.pending.name());
                        o.setTypeStatus(OrderPaymentStatus.pending.name());
                        break;
                }

                String referer = post.getMetaMap().get(PostMetaKeys.BILLING_REFERER);
                if (referer != null) {
                    o.setStoreId(referer);
                } else {
                    //referrer
                    Referrals ref = referralsRepository.findByReference(post.getId().toString());
                    if (ref != null) {
                        o.setStoreId(String.valueOf(ref.getAffiliateId()));
                    } else {
                        b.append("Pedido sin referer ").append(post.getId()).append(System.lineSeparator());
                        log.error("Atencion: pedido sin referer", post.getId());
                    }
                }

                ordersService.save(o);
                orderItems.forEach((orderItem) -> {
                    orderItem.setOrderId(o.getId());
                });
                orderItemsRepository.save(orderItems);
                count++;

                //create member
                createMember(o, post);

            }
        } catch (Exception e) {
            b.append("Error ").append(e.getMessage()).append(System.lineSeparator());
            log.error("Error importando desde store ", e);
        }
        b.append("Procesadas ").append(count);
        return b.toString();
    }

    /**
     *
     * @param source
     * @param orderItem
     * @return
     */
    private StoreItem getStoreItem(String source, WpOrderItem orderItem) {
        List<StoreItemName> names = storeItemNamesRepository.findByItemName(orderItem.getOrderItemName());
        if (names.isEmpty()) {
            return null;
        }
        for (StoreItemName name : names) {
            StoreItem temp = storeItemsRepository.findOne(name.getItemId());
            if (temp.getSource().contains(source) && temp.getEnabled()) {
                return temp;
            }
        }
        return null;
    }

    /**
     *
     * @param item
     * @return
     */
    private String getOrderTypeFromCategory(StoreItem item) {
        switch (item.getCategory()) {
            case 1:
                return OrderType.kit.name();
            case 2:
                return OrderType.product.name();
            case 3:
                return OrderType.service.name();
            case 4:
                return OrderType.upgrade.name();
            case 5:
                return OrderType.store.name();
            default:
                return OrderType.product.name();
        }
    }

    /**
     *
     * @param itemName
     * @return
     */
    private String priceText(String itemName) {
        if (itemName.contains("VUELO")) {
            return "vuelo";
        } else if (itemName.contains("CRUCERO")) {
            return "crucero";
        }
        return null;
    }

    /**
     *
     * @param item
     * @param orderItemName
     * @param storeItemId
     * @return
     */
    private List<OrderItem> getItemsForOrder(WpOrderItem item, String orderItemName, Long storeItemId) {

        Map<String, StoreItemOption> optMap = new HashMap<>();
        List<StoreItemOption> options = storeItemOptionsRepository.findByStoreItemId(storeItemId);
        for (StoreItemOption option : options) {
            optMap.put(option.getName() + option.getValue(), option);
        }

        List<OrderItem> ois = new ArrayList<>();
        List<WpOrderItemmeta> metas = wpOrderItemmetasRepository.findByOrderItemId(item.getOrderItemId());
        int qty = 1;
        for (WpOrderItemmeta meta : metas) {

            if (meta.getMetaKey().contains("_qty")) {
                qty = Integer.parseInt(meta.getMetaValue());
            }

            if (meta.getMetaKey().contains("color")) {
                String key = meta.getMetaKey() + meta.getMetaValue();
                StoreItemOption opt = optMap.get(key);

                if (opt != null) {
                    OrderItem oi = new OrderItem();
                    oi.setShipped(false);
                    oi.setName(opt.getProductName());
                    oi.setStoreItemOptionId(opt.getId());
                    ois.add(oi);
                }
            }
        }

        try {
            if (qty > 1) {
                while (ois.size() < qty) {
                    OrderItem oi = new OrderItem();
                    oi.setShipped(false);
                    oi.setName(ois.get(0).getName());
                    oi.setStoreItemOptionId(ois.get(0).getStoreItemOptionId());
                    ois.add(oi);
                }
            }
        } catch (Exception e) {
        }

        //si no tiene color seleccionado
        if (ois.isEmpty()) {

            if (orderItemName.contains("FOUNDER WINGS")) {
                for (int i = 0; i < 4; i++) {
                    OrderItem oi = new OrderItem();
                    oi.setShipped(false);
                    oi.setName(item.getOrderItemName());
                    oi.setStoreItemOptionId(i % 2 == 0 ? 3l : 4l);
                    ois.add(oi);
                }
                for (int i = 0; i < 3; i++) {
                    OrderItem oi = new OrderItem();
                    oi.setShipped(false);
                    oi.setName(item.getOrderItemName());
                    oi.setStoreItemOptionId(1l);
                    ois.add(oi);
                }
            } else if (orderItemName.contains("2 TELÉFONOS W2")) {

                OrderItem oi = new OrderItem();
                oi.setShipped(false);
                oi.setName(item.getOrderItemName());
                oi.setStoreItemOptionId(3l);
                ois.add(oi);

                oi = new OrderItem();
                oi.setShipped(false);
                oi.setName(item.getOrderItemName());
                oi.setStoreItemOptionId(4l);
                ois.add(oi);

            } else if (item.getOrderItemName().contains("STARTER 2")) {

                OrderItem oi = new OrderItem();
                oi.setShipped(false);
                oi.setName(item.getOrderItemName());
                oi.setStoreItemOptionId(item.getOrderItemName().contains("CHAMPAGNE GOLD") ? 2l : 1l);
                ois.add(oi);

            } else if (item.getOrderItemName().contains("Upgrade")) {

            } else if (item.getOrderItemName().contains("STARTER")) {

                OrderItem oi = new OrderItem();
                oi.setShipped(false);
                oi.setName(item.getOrderItemName());
                oi.setStoreItemOptionId(item.getOrderItemName().contains("CHAMPAGNE GOLD") ? 4l : 3l);
                ois.add(oi);
            } else {

                OrderItem oi = new OrderItem();
                oi.setShipped(false);
                oi.setName(item.getOrderItemName());
                ois.add(oi);

            }

        }

        return ois;
    }

    /**
     *
     * @param orderId
     * @return
     */
    public String createMember(Long orderId) {
        Order o = ordersService.getOrder(orderId);
        Post p = postsRepository.findOne(o.getExternalId());
        createMember(o, p);
        return "ok";
    }

    /**
     *
     * @param order
     * @param post
     */
    @Transactional
    public void createMember(Order order, Post post) {

        StoreItem storeItem = storeItemsRepository.findOne(order.getItemId());

        try {
            if ((storeItem.getCategory() == 1 || storeItem.getCategory() == 5) && storeItem.getId() != 15) {

                List<Member> exist = membersRepository.findByEmail(order.getPayerEmail());
                if (exist.isEmpty()) {

                    Member m = new Member();

                    m.setActivationDate(order.getActivationDate());
                    m.setCity(order.getBillingCity());
                    m.setCountry(order.getBillingCountry());
                    m.setEmail(order.getPayerEmail());
                    m.setFirstName(order.getFirstName());
                    m.setLastName(order.getLastName());
                    m.setCompanyName(order.getBillingCompany());
                    m.setDni(post.getMetaMap().get(PostMetaKeys.BILLING_DNI));
                    m.setCalculatedRankId(1l);
                    m.setRankId(1l);
                    m.setInitialRankId(1l);
                    m.setOrderId(order.getId());
                    m.setSource("store");
                    m.setDeleted(Boolean.FALSE);
                    m.setLoginEnabled(Boolean.TRUE);
                    m.setIndirectCount(0l);

                    String generatedPassword = null;
                    User user = usersRepository.findByUserEmail(m.getEmail());
                    if (user == null) {
                        generatedPassword = InvoiceUtils.getRandomString();
                        m.setPassword(DigestUtils.sha256Hex(generatedPassword));
                    } else {
                        m.setPassword(user.getUserPass());
                    }

                    m.setPhone(post.getMetaMap().get(PostMetaKeys.BILLING_PHONE));
                    m.setPostal(post.getMetaMap().get(PostMetaKeys.BILLING_POST_CODE));
                    m.setRegistrationDate(post.getPostDate());

                    if (order.getStoreId() != null) {
                        Member sponsor = membersRepository.findByUsername(order.getStoreId());
                        if (sponsor == null) {
                            log.error("No se econtro sponsor y el store id existe " + post.getId() + " " + order.getStoreId());
                        } else {
                            m.setSponsorId(sponsor.getId());
                        }
                    }

                    m.setState(order.getBillingState());
                    m.setStatus("created_from_store");
                    m.setStreet(order.getBillingAddress());

                    //Affiliate a = affiliatesRepository.findByPaymentEmail(m.getEmail());
                    String newUsername = InvoiceUtils.getNumberRandomString(11);
//                    if (a != null && a.getAffiliateId() > 50000) {
//                        newUsername = a.getAffiliateId().toString();
//                    } else {
                    while (membersRepository.findByUsername(newUsername) != null || newUsername.startsWith("0")) {
                        newUsername = InvoiceUtils.getNumberRandomString(11);
                    }
                    //}
                    m.setUsername(newUsername);
                    m.setDniValidated("0");
                    m.setPaymentsEnabled(true);

                    membersRepository.save(m);

                    speedBonusService.asignSpeedBonus(m);

                    //save member id on the order
                    order.setMemberId(m.getId());
                    ordersService.save(order);

                    //Create record on marketing
                    try {
                        syncroService.syncMember(m.getId());
                    } catch (Exception e) {
                    }

                    //create legacy site member and orders
                    //MemberOld old = createLegacy(m, order);
                    //if (old != null) {
                    //update wp user and create affiliate
//                        User u = usersRepository.findByUserEmail(m.getEmail());
//                        if (u == null) {
//                            u = new User();
//                            u.setUserPass(m.getPassword());
//                            u.setUserRegistered(m.getRegistrationDate());
//                            u.setUserStatus(0);
//                            u.setUserUrl("");
//                            u.setUserActivationKey("");
//                            u.setUserEmail(m.getEmail());
//                        }
//                        u.setUserLogin(m.getUsername());
//                        u.setUserNicename(m.getUsername());
//                        u.setDisplayName(m.getFirstName() + " " + m.getLastName());
//                        usersRepository.save(u);
//
//                        while (u.getId() == null) {
//                            usersRepository.save(u);
//                        }
                    //saveAffiliate(u, m);
                    //send email to user
                    StringBuilder body = new StringBuilder("Bienvenido a Wings Mobile, para acceder a tu oficina virtual ");
                    body.append("puedes hacer click <a href=\"https://beta.wingsmobile.net/login\" target=\"_blank\">AQUI</a> <br>");
                    body.append("Tu Nombre de usuario es: ").append(m.getUsername()).append("<br/>");
                    if (generatedPassword == null) {
                        body.append("La contraseña es la misma que usaste para registrarte en la tienda");
                    } else {
                        body.append("Tu contraseña es: ").append(generatedPassword);
                    }
                    body.append("<br/><br/>");
                    String text = emailService.getGenericEmail(m.getFirstName(), "Cuenta de Afiliado", body.toString());
                    boolean success = emailService.sendMail("Wings Mobile <contratacion@wingsmobile.com>", m.getEmail(), "Cuenta de Afiliado", text, null);
                    //}
                } else {
                    order.setMemberId(exist.get(0).getId());
                    ordersService.save(order);
                }
            }
        } catch (Exception e) {
            log.error("Error creando afiliado " + order.getId() + " - " + post.getId(), e);
        }
        //create member if has a kit
    }

    public Order saveOrder(Order order) {
        ordersService.save(order);
        return order;
    }

    /**
     *
     * @param order
     */
    @Transactional
    public void createMember(Order order) {

        StoreItem storeItem = storeItemsRepository.findOne(order.getItemId());

        try {
            if ((storeItem.getCategory() == 1 || storeItem.getCategory() == 5) && storeItem.getId() != 15) {

                List<Member> exist = membersRepository.findByEmail(order.getPayerEmail());
                if (exist.isEmpty()) {

                    Member m = new Member();

                    m.setActivationDate(order.getActivationDate());
                    m.setCity(order.getBillingCity());
                    m.setCountry(order.getBillingCountry());
                    m.setEmail(order.getPayerEmail());
                    m.setFirstName(order.getFirstName());
                    m.setLastName(order.getLastName());
                    m.setCompanyName(order.getBillingCompany());
                    m.setDni(order.getDni());
                    m.setCalculatedRankId(1l);
                    m.setRankId(1l);
                    m.setInitialRankId(1l);
                    m.setOrderId(order.getId());
                    m.setSource("store");
                    m.setDeleted(Boolean.FALSE);
                    m.setLoginEnabled(Boolean.TRUE);
                    m.setIndirectCount(0l);

                    String generatedPassword = null;
                    User user = usersRepository.findByUserEmail(m.getEmail());
                    if (user == null) {
                        generatedPassword = InvoiceUtils.getRandomString();
                        m.setPassword(DigestUtils.sha256Hex(generatedPassword));
                    } else {
                        m.setPassword(user.getUserPass());
                    }

                    m.setPhone(null);
                    m.setPostal(order.getBillingPostalCode());
                    m.setRegistrationDate(order.getCreationDate());

                    if (order.getStoreId() != null) {
                        Member sponsor = membersRepository.findByUsername(order.getStoreId());
                        if (sponsor == null) {
                            log.error("No se econtro sponsor y el store id existe " + order.getStoreId());
                        } else {
                            m.setSponsorId(sponsor.getId());
                        }
                    }

                    m.setState(order.getBillingState());
                    m.setStatus("created_from_store");
                    m.setStreet(order.getBillingAddress());

                    //Affiliate a = affiliatesRepository.findByPaymentEmail(m.getEmail());
                    String newUsername = InvoiceUtils.getNumberRandomString(11);
//                    if (a != null && a.getAffiliateId() > 50000) {
//                        newUsername = a.getAffiliateId().toString();
//                    } else {
                    while (membersRepository.findByUsername(newUsername) != null || newUsername.startsWith("0")) {
                        newUsername = InvoiceUtils.getNumberRandomString(11);
                    }
                    //}
                    m.setUsername(newUsername);
                    m.setDniValidated("0");

                    membersRepository.save(m);

                    speedBonusService.asignSpeedBonus(m);

                    //save member id on the order
                    order.setMemberId(m.getId());
                    ordersService.save(order);

                    //Create record on marketing
                    try {
                        syncroService.syncMember(m.getId());
                    } catch (Exception e) {
                    }

                    //create legacy site member and orders
                    //MemberOld old = createLegacy(m, order);
                    //if (old != null) {
                    //update wp user and create affiliate
//                        User u = usersRepository.findByUserEmail(m.getEmail());
//                        if (u == null) {
//                            u = new User();
//                            u.setUserPass(m.getPassword());
//                            u.setUserRegistered(m.getRegistrationDate());
//                            u.setUserStatus(0);
//                            u.setUserUrl("");
//                            u.setUserActivationKey("");
//                            u.setUserEmail(m.getEmail());
//                        }
//                        u.setUserLogin(m.getUsername());
//                        u.setUserNicename(m.getUsername());
//                        u.setDisplayName(m.getFirstName() + " " + m.getLastName());
//                        usersRepository.save(u);
//
//                        while (u.getId() == null) {
//                            usersRepository.save(u);
//                        }
                    //saveAffiliate(u, m);
                    //send email to user
                    StringBuilder body = new StringBuilder("Bienvenido a Wings Mobile, para acceder a tu oficina virtual ");
                    body.append("puedes hacer click <a href=\"https://beta.wingsmobile.net/login\" target=\"_blank\">AQUI</a> <br>");
                    body.append("Tu Nombre de usuario es: ").append(m.getUsername()).append("<br/>");
                    if (generatedPassword == null) {
                        body.append("La contraseña es la misma que usaste para registrarte en la tienda");
                    } else {
                        body.append("Tu contraseña es: ").append(generatedPassword);
                    }
                    body.append("<br/><br/>");
                    String text = emailService.getGenericEmail(m.getFirstName(), "Cuenta de Afiliado", body.toString());
                    boolean success = emailService.sendMail("Wings Mobile <contratacion@wingsmobile.com>", m.getEmail(), "Cuenta de Afiliado", text, null);
                    //}
                } else {
                    order.setMemberId(exist.get(0).getId());
                    ordersService.save(order);
                }
            }
        } catch (Exception e) {
            log.error("Error creando afiliado " + order.getId(), e);
        }
        //create member if has a kit
    }

    /**
     *
     * @return
     */
    public String fixStores() {

        List<Order> orders = ordersService.findByType(OrderType.store.name());
        int proc = 0;
        for (Order order : orders) {
            if (order.getMemberId() == null) {
                Post p = postsRepository.findOne(order.getExternalId());
                createMember(order, p);
                proc++;
            }
        }

        return "Procesadas " + proc + " de " + orders.size();
    }

    /**
     *
     * @return
     */
    public String fixDnis() {

        List<Order> orders = ordersService.findAll();

        for (Order order : orders) {

            if (order.getType().equals(OrderType.kit.name())
                    || order.getType().equals(OrderType.mix.name())
                    || order.getType().equals(OrderType.product.name())
                    || order.getType().equals(OrderType.store.name())
                    || order.getType().equals(OrderType.upgrade.name())) {

                if (order.getSource().equals("store")) {
                    Post post = postsRepository.findOne(order.getExternalId());
                    if (post == null || post.getMetaMap() == null) {
                        //puede ser de las viejas
                        Member m = order.getMember();
                        if (m != null && m.getDni() != null) {
                            order.setDni(m.getDni());
                        }
                    } else {
                        String dni = post.getMetaMap().get(PostMetaKeys.BILLING_DNI);
                        order.setDni(dni);
                    }

                } else {

                    Member m = order.getMember();
                    if (m != null && m.getDni() != null) {
                        order.setDni(m.getDni());
                    }
                }
            }
        }

        ordersService.save(orders);
        return "Orders updated";
    }

    /**
     *
     * @param post
     * @return
     */
    private String getSource(Post post) {
        String ret = "store";
        if (post.getGuid().contains("es.wingsmobile.net")) {
            ret = "store-es";
        } else if (post.getGuid().contains("co.wingsmobile.net")) {
            ret = "store-co";
        } else if (post.getGuid().contains("it.wingsmobile.net")) {
            ret = "store-it";
        } else if (post.getGuid().contains("pe.wingsmobile.net")) {
            ret = "store-pe";
        } else if (post.getGuid().contains("ec.wingsmobile.net")) {
            ret = "store-ec";
        } else if (post.getGuid().contains("mx.wingsmobile.net")) {
            ret = "store-mx";
        }
        return ret;

    }

}
