/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

/**
 *
 * @author seba
 */
public class CalificationSums {

    private Double total;
    private Double totalKit;
    private Double totalService;
    private Double totalProduct;

    public CalificationSums(Double total, Double totalKit, Double totalService, Double totalProduct) {
        this.total = total;
        this.totalKit = totalKit;
        this.totalService = totalService;
        this.totalProduct = totalProduct;

        if (this.total == null) {
            this.total = 0.0;
        }
        if (this.totalService == null) {
            this.totalService = 0.0;
        }

        if (this.totalKit == null) {
            this.totalKit = 0.0;
        }

        if (this.totalProduct == null) {
            this.totalProduct = 0.0;
        }
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return the totalKit
     */
    public Double getTotalKit() {
        return totalKit;
    }

    /**
     * @param totalKit the totalKit to set
     */
    public void setTotalKit(Double totalKit) {
        this.totalKit = totalKit;
    }

    /**
     * @return the totalService
     */
    public Double getTotalService() {
        return totalService;
    }

    /**
     * @param totalService the totalService to set
     */
    public void setTotalService(Double totalService) {
        this.totalService = totalService;
    }

    /**
     * @return the totalProduct
     */
    public Double getTotalProduct() {
        return totalProduct;
    }

    /**
     * @param totalProduct the totalProduct to set
     */
    public void setTotalProduct(Double totalProduct) {
        this.totalProduct = totalProduct;
    }
    
    

}
