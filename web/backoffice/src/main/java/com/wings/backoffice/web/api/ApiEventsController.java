/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.api;

import com.wings.backoffice.mlm.domain.events.Event;
import com.wings.backoffice.mlm.domain.events.EventHost;
import com.wings.backoffice.mlm.domain.events.EventTicket;
import com.wings.backoffice.mlm.domain.events.EventTicketInfo;
import com.wings.backoffice.mlm.domain.events.enums.EventTicketStatus;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.web.api.eventsDto.EventDTO;
import com.wings.backoffice.web.api.eventsDto.EventTicketDTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
@RequestMapping("api/events/")
public class ApiEventsController {

    @Autowired
    private EventsService eventsService;

    private final Logger log = LoggerFactory.getLogger(ApiEventsController.class);

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"login"}, method = {RequestMethod.POST})
    public ResponseEntity<Map<String, String>> login(Model model, HttpServletRequest request) {

        Map<String, String> response = new HashMap<>();

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        EventHost host = eventsService.getHostByEmail(email);
        if (host.getPassword().equals(password)) {
            response.put("message", "success");
            return ResponseEntity.ok(response);
        }

        response.put("message", "Usuario o contraseña incorrecto!");
        return ResponseEntity.badRequest().body(response);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"sync/events"}, method = {RequestMethod.GET})
    public ResponseEntity<List<EventDTO>> syncEvents(Model model, HttpServletRequest request) {

        List<EventDTO> ret = new LinkedList<>();
        List<Event> events = eventsService.getEventsPublished();
        events.forEach((event) -> {
            EventDTO dto = new EventDTO();
            dto.setEventDate(event.getEventDate());
            dto.setEventId(event.getId());
            dto.setEventName(event.getName());
            dto.setEventShortName(event.getShortName());
            dto.setEventStatus(event.getStatus());
            ret.add(dto);
        });

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"sync/tickets"}, method = {RequestMethod.GET})
    public ResponseEntity<List<EventTicketDTO>> syncEventsTickets(Model model, HttpServletRequest request) {

        List<EventTicketDTO> ret = new LinkedList<>();
        String eid = request.getParameter("eventId");
        List<EventTicket> tickets = eventsService.getEventTicketByStatus(Long.parseLong(eid), EventTicketStatus.reclaimed.name());
        tickets.stream().map((ticket) -> {

            EventTicketDTO dto = new EventTicketDTO();
            dto.setAddress(ticket.getAddress());
            dto.setAttendanceDate(null);
            dto.setCity(ticket.getCity());
            dto.setCountry(ticket.getCountry());
            dto.setEmail(ticket.getEmail());
            dto.setFirstName(ticket.getFirstName());
            dto.setLastName(ticket.getLastName());
            dto.setId(ticket.getId());
            dto.setMemberId(ticket.getMemberId());
            dto.setState(ticket.getState());
            dto.setTicketCode(ticket.getTicketCode());
            dto.setTicketNumber(ticket.getTicketNumber());
            dto.setSeatNumber(ticket.getSeatNumber());
            dto.setZip(ticket.getZip());
            List<EventTicketInfo> tinfos = eventsService.getEventTicketInfo(ticket.getId());
            Map<String, String> extras = new HashMap<>();
            tinfos.forEach((tinfo) -> {
                extras.put(tinfo.getInfoName(), tinfo.getInfoValue());
            });
            dto.setExtras(extras);
            return dto;

        }).forEachOrdered((dto) -> {
            ret.add(dto);
        });

        return ResponseEntity.ok(ret);
    }

    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     *
     * @param model
     * @param request
     * @param ticketsDtos
     * @return
     */
    @RequestMapping(value = {"send"}, method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> send(Model model, HttpServletRequest request, @RequestBody List<EventTicketDTO> ticketsDtos) {
        Map<String, String> resp = new HashMap<>();
        resp.put("message", "success");
        try {
            //TODO: audit info
            List<EventTicket> toSave = new LinkedList<>();
            for (EventTicketDTO ticketDto : ticketsDtos) {
                Long id = ticketDto.getId();

                EventTicket ticket = eventsService.getEventTicketById(id);
                ticket.setAttendanceDate(ticketDto.getAttendanceDate());
                ticket.setStatus(EventTicketStatus.completed.name());

                toSave.add(ticket);
            }
            eventsService.saveEventTickets(toSave);
        } catch (Exception e) {
            log.error("Error on send", e);
            resp.put("message", "error");
            return ResponseEntity.badRequest().body(resp);
        }
        return ResponseEntity.ok(resp);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"update/tickets"}, method = {RequestMethod.GET})
    public ResponseEntity<List<Long>> updateTickets(Model model, HttpServletRequest request) {

        String evId = request.getParameter("eventId");

        Long eventId = Long.parseLong(evId);

        List<EventTicket> tickets = eventsService.getEventTicketByStatus(eventId, EventTicketStatus.completed.name());

        List<Long> ret = new ArrayList<>();
        for (EventTicket ticket : tickets) {
            ret.add(ticket.getId());
        }

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"stats"}, method = {RequestMethod.GET})
    public ResponseEntity<Map<String, String>> getStats(Model model, HttpServletRequest request) {
        String evId = request.getParameter("eventId");
        Long eventId = Long.parseLong(evId);
        Map<String, String> stats = eventsService.getStats2(eventId);
        return ResponseEntity.ok(stats);
    }
}
