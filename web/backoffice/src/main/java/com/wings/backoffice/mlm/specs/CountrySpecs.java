/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Country;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class CountrySpecs {

    public static Specification<Country> search(String q, String status) {

        return (Root<Country> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("code"), search),
                                builder.like(root.get("nameEs"), search)
                        )
                );
            }

            if (status != null && status.equals("1")) {
                predicates.add(builder.equal(root.get("enabled"), true));
            }
            if (status != null && status.equals("0")) {
                predicates.add(builder.equal(root.get("enabled"), false));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
