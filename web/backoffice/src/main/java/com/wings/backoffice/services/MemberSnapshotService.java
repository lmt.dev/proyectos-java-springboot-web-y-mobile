/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.utils.MemberSnapshot;
import com.wings.backoffice.mlm.repos.MemberSnapshotRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class MemberSnapshotService {

    @Autowired
    private MemberSnapshotRepository memberSnapshotRepository;

    private final Logger log = LoggerFactory.getLogger(MemberSnapshotService.class);

    public MemberSnapshot getLastMemberSnapshot(Long memberId) {
        return memberSnapshotRepository.findFirstByMemberIdOrderBySnapshotDateDescEventIdDesc(memberId);
    }

    public MemberSnapshot getMemberSnapshot(Long memberId, Long eventId) {
        return memberSnapshotRepository.findByMemberIdAndEventId(memberId, eventId);
    }

}
