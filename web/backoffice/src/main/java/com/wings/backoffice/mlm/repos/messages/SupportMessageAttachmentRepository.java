/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos.messages;

import com.wings.backoffice.mlm.domain.messages.SupportMessageAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface SupportMessageAttachmentRepository extends JpaRepository<SupportMessageAttachment, Long>, JpaSpecificationExecutor<SupportMessageAttachment> {

}
