/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.peru;

/**
 *
 * @author seba
 */
public class PeruResponseWrapper {

    private String status;
    private String message;
    private Integer response;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the response
     */
    public Integer getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(Integer response) {
        this.response = response;
    }
    
    
    
}
