/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class BacsService {

    public final Map<String, String> bacs = new HashMap<>();

    @PostConstruct
    public void init() {

        bacs.put("EC", "Banco Internacional Ecuador;Banco de Pacifico Ecuador;Produbanco Ecuador");
        bacs.put("CO", "Bancolombia Colombia;BBVA Colombia");
        bacs.put("PE", "Scotibanck Perú;BBVA Perú;Interbank Perú;Banco de la Nación Perú;BCP Perú");
        bacs.put("US", "Chase USA");
        bacs.put("CR", "Promerica Costa Rica;BAC Costa Rica");
        bacs.put("GT", "Promerica Guatemala;BAC Guatemala");
        bacs.put("SV", "Promerica El Salvador;BAC El Salvador");
        bacs.put("NI", "Promerica Nicaragua;BAC Nicaragua");
        bacs.put("HN", "Promerica Honduras;BAC Honduras");
        bacs.put("DO", "Promerica Rep. Dom.");
        bacs.put("BO", "Fassil Bolivia");
        bacs.put("AR", "Macro Argentina");
        bacs.put("CL", "Banco de Chile");
        bacs.put("ES", "Caixa España");
        bacs.put("IT", "BPM Italia");
        bacs.put("HK", "Bank Of Honk Kong");
        bacs.put("BR", "Banco Safra Brasil");
        bacs.put("MX", "BBVA Bancomer México");

    }

    /**
     *
     * @param iso2
     * @return
     */
    public List<String> getBacsForCountry(String iso2) {
        List<String> ret = new ArrayList<>();
        String str = bacs.get(iso2);
        if (str != null) {
            ret.addAll(Arrays.asList(str.split(";")));
        }
        return ret;
    }

    public List<String> getAll() {
        List<String> ret = new ArrayList<>();
        bacs.entrySet().stream().map((entry) -> entry.getValue()).forEachOrdered((val) -> {
            ret.addAll(Arrays.asList(val.split(";")));
        });
        return ret;
    }

}
