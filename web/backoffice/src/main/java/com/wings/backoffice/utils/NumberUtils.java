/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.wings.backoffice.services.CountriesService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class NumberUtils {

    private static CountriesService countriesService;

    @Autowired
    private CountriesService countriesServiceNonStatic;

    @PostConstruct
    private void init() {
        countriesService = countriesServiceNonStatic;
    }

    private static final Random rand = new Random();
    private static final DecimalFormat dataFormat = new DecimalFormat("0.00");
    private static final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(new Locale("es", "ES"));
    private static Map<String, Double> CONVERSION_RATES;

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double roundEven(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

    public static String formatMoney(double money) {
        return moneyFormat.format(round(money, 2));
    }

    /**
     * Convert money from euro to locale conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static Double convertMoneyFromEuros(double money, Locale locale) {
        setupConvertionRates();
        double conversionRate = 1.0;
        if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
            conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
        }
        //return round(money * conversionRate, 2);
        return money * conversionRate;
    }

    /**
     * Convert money from euros to currency
     *
     * @param money
     * @param currency
     * @return
     */
    public static Double convertMoneyFromEuros(double money, String currency) {

        Locale locale = new Locale("es", "ES");
        try {
            String iso = countriesService.getCurrenciesMapInverted().get(currency);
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }

        return convertMoneyFromEuros(money, locale);
    }

    /**
     * Convert money to euro from locale conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static Double convertMoneyToEuros(double money, Locale locale) {
        setupConvertionRates();
        double conversionRate = 1.0;
        if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
            conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
        }
        //return round(money / conversionRate, 2);
        return money / conversionRate;
    }

    /**
     * Format money with conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static String formatMoney(double money, Locale locale) {

        if (locale == null || locale.getCountry() == null) {
            return "";
        }

        try {
            setupConvertionRates();
            double conversionRate = 1.0;
            if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
                conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
            }
            NumberFormat f = NumberFormat.getCurrencyInstance(locale);
            //return f.format(round(money * conversionRate, 2));
            return f.format(money * conversionRate);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Format money without conversion rate
     *
     * @param money
     * @param currency
     * @return
     */
    public static String formatMoney(double money, String currency) {

        Locale locale = new Locale("es", "ES");
        try {
            String iso = countriesService.getCurrenciesMapInverted().get(currency);
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }
        if (locale == null) {
            locale = new Locale("es", "ES");
        }

        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(round(money, 2));
    }

    /**
     * Format money without conversion rate
     *
     * @param money
     * @param currency
     * @return
     */
    public static Double unFormatMoney(String money, String currency) {

        Locale locale = new Locale("es", "ES");
        try {
            String iso = countriesService.getCurrenciesMapInverted().get(currency);
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }
        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        try {
            Number number = f.parse(money);
            return number.doubleValue();
        } catch (Exception e) {
        }
        return 0.0;
    }

    /**
     * Format money with conversion rate
     *
     * @param money
     * @param currency
     * @return
     */
    public static String convertAndFormatMoney(double money, String currency) {

        Locale locale = new Locale("es", "ES");
        try {
            String iso = countriesService.getCurrenciesMapInverted().get(currency);
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }

        return formatMoney(money, locale);
    }

    /**
     * Format money without conversion rate, based on iso
     *
     * @param money
     * @param iso
     * @return
     */
    public static String formatMoneyForCountryIso(Double money, String iso) {

        if (money == null) {
            return "";
        }

        Locale locale = new Locale("es", "ES");
        try {
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }

        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(money);
    }

    /**
     * Format money without conversion rate, based on iso
     *
     * @param money
     * @param iso
     * @return
     */
    public static String convertAndformatMoneyForCountryIso(Double money, String iso) {

        if (money == null) {
            return "";
        }

        Locale locale = new Locale("es", "ES");
        try {
            locale = countriesService.getLocalesMap().get(iso);
        } catch (Exception e) {
        }
        return formatMoney(money, locale);
    }

    public static String formatData(long size) {

        String hrSize;
        double k = size / 1024d;
        double m = size / 1048576d;
        double g = size / 1073741824d;
        double t = size / (1073741824d * 1024);

        if (t > 1) {
            hrSize = dataFormat.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dataFormat.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dataFormat.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dataFormat.format(m).concat(" KB");
        } else {
            hrSize = dataFormat.format(size).concat(" B");
        }

        return hrSize;
    }

    public static int randInt1to9999() {
        int min = 1;
        int max = 9999;
        return rand.nextInt((max - min) + 1) + min;
    }

    private static void setupConvertionRates() {
        //if (CONVERSION_RATES == null) {
        CONVERSION_RATES = new HashMap<>();
        CONVERSION_RATES.putAll(countriesService.getExchangeRates());
        //}
    }

}
