/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.utils.DateUtils;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class BitwingsService {

    //TODO: Revisar si esto es Valido
    private final String apiKey = "ANEWKJIVP3Q5FZFY35ZDP9XFHHSPPQ7BV2";
    private static final String apiUrl = "https://api.etherscan.io/api?module=account&action=balance&address=ADDRESS&tag=latest&apikey=ANEWKJIVP3Q5FZFY35ZDP9XFHHSPPQ7BV2";

    @Autowired
    private MembersService membersService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private EmailService emailService;

    public int canReclaim(Long memberId) {

        int ret = -1;
        Member member = membersService.getOne(memberId);
        List<Order> orders = ordersService.getMemberOrders(member);
        //sort by id
        Collections.sort(orders, (Order o1, Order o2) -> o1.getId().compareTo(o2.getId()));

        for (Order order : orders) {

            if (order.getActivationDate() == null) {
                continue;
            }

            StoreItem storeItem;
            if (order.getType().equals(OrderType.upgrade.name()) && order.getOrderItem().getFinalItem() != null) {
                storeItem = order.getOrderItem().getFinalItem();
            } else {
                storeItem = order.getOrderItem();
            }

            //if order is from cuotas, check all paid
            if (storeItem.isInstallments()) {
                if (order.getInstallmentNumber() == 1) {
                    List<Order> cuotasPayed = ordersService.getPayedInstallments(order);
                    if (cuotasPayed.size() != (storeItem.getInstallmentsQty() - 1)) {
                        continue;
                    }
                }
            }

            Date from = DateUtils.fromMysqlStringToDate("2019-05-15 00:00:00");
            Date to = DateUtils.fromMysqlStringToDate("2019-08-10 23:59:59");
            Date toPeru = DateUtils.fromMysqlStringToDate("2019-10-06 23:59:59");
            Date realTo = to;
            if (order.getBillingCountry().equals("PE")) {
                realTo = toPeru;
            }

            boolean inRange = DateUtils.dateInRange(order.getActivationDate(), from, realTo);

            if (inRange) {
                switch (storeItem.getId().intValue()) {
                    case 3:
                    case 69:
                    case 82:
                    case 89:
                        ret = 1495;
                        break;
                    case 4:
                    case 70:
                    case 83:
                    case 90:
                        ret = 2995;
                        break;
                    case 68:
                    case 73:
                    case 86:
                    case 74:
                    case 87:
                    case 93:
                    case 94:
                        ret = 10000;
                        break;
                    case 75:
                    case 88:
                    case 95:
                        ret = 20000;
                        break;
                }
            } else {

                if (order.getActivationDate().before(from)) {
                    switch (storeItem.getId().intValue()) {
                        case 7:
                        case 68:
                        case 73:
                        case 86:
                        case 74:
                        case 87:
                        case 93:
                        case 94:
                            ret = 5000;
                            break;
                        case 75:
                        case 88:
                        case 95:
                            ret = 10000;
                            break;
                    }
                }
            }

            //si ya tiene reclaim no entramos
            if (ret == -1) {
                //PROMO PERU BWN - si no aplica el rango anterior y es de Perú
                if (!inRange && order.getBillingCountry().equals("PE")) {
                    //valido desde el 07 de Octubre hasta el 30 de octubre
                    from = DateUtils.fromMysqlStringToDate("2019-10-07 00:00:00");
                    to = DateUtils.fromMysqlStringToDate("2019-11-30 23:59:59");
                    inRange = DateUtils.dateInRange(order.getActivationDate(), from, to);
                }

                //aplicamos nuevo rango
                if (inRange) {
                    switch (storeItem.getId().intValue()) {
                        case 190:
                        case 206:
                            ret = 300;
                            break;
                        case 195:
                        case 207:
                            ret = 600;
                            break;
                        case 74:
                        case 94:
                        case 198:
                        case 210:
                            ret = 5000;
                            break;
                        case 75:
                        case 88:
                        case 95:
                        case 199:
                        case 211:
                            ret = 10000;
                            break;
                    }
                }
            }
        }
        return ret;
    }

    /* input : Address 
     * Output: true or false 
     * purpose: to validate an Ethereum Address
     * 
     * Procedure :
     * If the string begins with 0x and contain numbers between 0 to 9 and a to f
     * then it is valid ethereum address
     */
    public boolean isValidAddress(String addr) {
        String regex = "^0x[0-9a-f]{40}$";
        //System.out.println("Incoming Address " + addr);
        return addr.matches(regex);
    }

    /* input : Address 
     * Output: true or false 
     * purpose: to validate a checksum an Ethereum Address 
     * Actual Checksum Procedure :
     * If the ith digit of the Address is a letter (ie. it's one of abcdef) print it in uppercase 
     * if the ith bit of the hash of the address (in binary form) is 1
     * otherwise print it in lowercase.
     * 
     * Ref URL: https://github.com/ethereum/EIPs/issues/55
     */
    public boolean isChecksumAddress(String addr) {

        String regex = "^0x[0-9a-fA-F]{40}$";
        if (!addr.matches(regex)) {
            return false;
        }
        String subAddr = addr.substring(2);
        String subAddrLower = subAddr.toLowerCase();

        SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest256();
        digestSHA3.update(subAddrLower.getBytes());
        String digestMessage = Hex.toHexString(digestSHA3.digest());

        for (short i = 0; i < subAddr.length(); i++) {
            if (subAddr.charAt(i) >= 65 && subAddr.charAt(i) <= 91) {

                String ss = Character.toString(digestMessage.charAt(i));
                if (!(Integer.parseInt(ss, 16) > 7)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Return 1 or 2 if it's valid, 0 if not
     *
     * @param address
     * @return
     */
    public int checkValid(String address) {
        if (isValidAddress(address)) {
            return 1;
        } else if (isChecksumAddress(address)) {
            return 2;
        } else {
            return 0; //not valid
        }
    }

    public static void checkBalance(String address) {

        try {

            RestTemplate template = new RestTemplate();
            ResponseEntity<Map> resp = template.getForEntity(apiUrl.replaceAll("ADDRESS", address), Map.class);

            System.out.println(resp.getBody());

            String amount = resp.getBody().get("result").toString();
            String prefix = "0.";
            if (amount.length() == 17) {
                prefix = "0.0";
            }
            System.out.println(prefix + amount);

        } catch (Exception e) {
        }

    }

    public static void main(String[] args) {
        /*checkBalance("0x3aA341Fb23cF0B89a02A3EE18BCF64B051735f0f");
        checkBalance("0x72278ebAaBFFB582FE78F2dB1A8a7c5B06cCD4dC");
        checkBalance("0x0D8bd3fDeb3018E395251aeabA9A6Ba16afE94E5");*/
        //checkBalance("0x393DfbCb6bc63f4EA2a94155ffeaB1C72a4eaA4D");
        //checkBalance("0x61e8f40A0497F8c9Ec03a107D565FbaE151aE6d");
        checkBalance("0x61e8f40A0497F8c9Ec030a107D565FbaE151aE6d");

    }

    /**
     * send email to domenico
     */
    @Scheduled(cron = "0 0 7 * * *", zone = "Europe/Madrid")
    public void sendEmail() {

        List<Member> members = membersService.getBwnToSend();
        if (members.isEmpty()) {
            return;
        }

        StringBuilder bf = new StringBuilder();
        members.forEach((member) -> {
            bf.append(member.getEmail()).append(",")
                    .append(member.getBwnWallet()).append(",")
                    .append(member.getBwnClaim()).append(System.lineSeparator());
            member.setBwnSent(true);
        });

        try {
            Date now = new Date();
            String prefix = "bwn_" + DateUtils.formatComputerDate(now) + "_";
            File f = File.createTempFile(prefix, ".csv");

            FileWriter fw = new FileWriter(f);
            fw.write(bf.toString());
            fw.close();

            List<File> attachements = new ArrayList<>();
            attachements.add(f);

            String title = "BWN Claims " + DateUtils.formatNormalDate(now);
            boolean sent = emailService.sendMail("contratacion@wingsmobile.com", "domenico.cantone1980@gmail.com", title, title, attachements);

            if (sent) {
                membersService.save(members);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //@Scheduled(initialDelay = 20000, fixedDelay = 646444444)
    public void writeEmails() {

        List<Member> members = membersService.getNonDeletedMembers();
        StringBuilder bf = new StringBuilder();
        members.forEach((member) -> {
            long bwn = canReclaim(member.getId());
            if (bwn != -1) {
                bf.append(member.getFirstName()).append(" ").append(member.getLastName()).append(",")
                        .append(member.getEmail()).append(",");
                switch (member.getCountry()) {
                    case "CO":
                        bf.append("Colombia").append(System.lineSeparator());
                        break;
                    case "PE":
                        bf.append("Perú").append(System.lineSeparator());
                        break;
                    case "EC":
                        bf.append("Ecuador").append(System.lineSeparator());
                        break;
                    case "ES":
                        bf.append("España").append(System.lineSeparator());
                        break;
                    default:
                        bf.append("Otro").append(System.lineSeparator());
                }
            }
        });

        try {
            Date now = new Date();
            String prefix = "email_bwn_" + DateUtils.formatComputerDate(now) + "_";
            File f = File.createTempFile(prefix, ".csv");

            FileWriter fw = new FileWriter(f);
            fw.write(bf.toString());
            fw.close();

            List<File> attachements = new ArrayList<>();
            attachements.add(f);

            String title = "BWN Claims " + DateUtils.formatNormalDate(now);
            boolean sent = emailService.sendMail("contratacion@wingsmobile.com", "domenico.cantone1980@gmail.com", title, title, attachements);

            if (sent) {
                membersService.save(members);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

//https://api.etherscan.io/api?module=account&action=balance&address=0x3aA341Fb23cF0B89a02A3EE18BCF64B051735f0f&tag=latest&apikey=ANEWKJIVP3Q5FZFY35ZDP9XFHHSPPQ7BV2
//https://ethplorer.io/address/0x393dfbcb6bc63f4ea2a94155ffeab1c72a4eaa4d?from=search
