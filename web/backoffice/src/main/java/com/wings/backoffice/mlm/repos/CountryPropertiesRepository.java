package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.CountryProperties;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author lucas
 */
public interface CountryPropertiesRepository extends JpaRepository<CountryProperties, String>, JpaSpecificationExecutor<CountryProperties> {

    List<CountryProperties> getByAvailableStore(boolean store, Sort sort);

    CountryProperties getOneByCode(String code);

}
