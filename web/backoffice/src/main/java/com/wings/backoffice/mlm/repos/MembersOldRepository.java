/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.MemberOld;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface MembersOldRepository extends JpaRepository<MemberOld, Long>, JpaSpecificationExecutor<MemberOld> {

    List<MemberOld> findByEmail(String email);

    MemberOld findByUsername(String username);

}
