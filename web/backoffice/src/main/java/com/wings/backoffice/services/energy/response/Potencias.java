
package com.wings.backoffice.services.energy.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "punta",
    "llano",
    "valle"
})
public class Potencias {

    @JsonProperty("punta")
    private String punta;
    @JsonProperty("llano")
    private String llano;
    @JsonProperty("valle")
    private String valle;

    @JsonProperty("punta")
    public String getPunta() {
        return punta;
    }

    @JsonProperty("punta")
    public void setPunta(String punta) {
        this.punta = punta;
    }

    @JsonProperty("llano")
    public String getLlano() {
        return llano;
    }

    @JsonProperty("llano")
    public void setLlano(String llano) {
        this.llano = llano;
    }

    @JsonProperty("valle")
    public String getValle() {
        return valle;
    }

    @JsonProperty("valle")
    public void setValle(String valle) {
        this.valle = valle;
    }

}
