/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import com.wings.backoffice.utils.NumberUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_withdrawals")
public class Withdrawal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date completedDate;
    private Boolean completed;
    private String status;
    private String statusDescription;
    private Long memberId;
    private String accountName;
    private String accountType;
    private String accountNumber;
    private String accountBank;
    private String accountIban;
    private String accountSwift;
    private String accountDescription;
    private String processor;
    private Double amount;
    private Double moneyBase;
    private Double moneyTax;
    private Double moneyRetention;
    private Double moneyPayable;
    private String requestUser;
    private String requestDescription;
    private String completedUser;
    private String completedDescription;
    private boolean iprf;
    private String details;
    private String currency;

    @OneToOne
    @JoinColumn(name = "memberId", insertable = false, updatable = false, unique = false)
    private Member member;

    @Transient
    private Locale locale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Date completedDate) {
        this.completedDate = completedDate;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getMoneyBase() {
        return moneyBase;
    }

    public void setMoneyBase(Double moneyBase) {
        this.moneyBase = moneyBase;
    }

    public String getMoneyBaseText() {
        if (locale != null) {
            return NumberUtils.formatMoney(moneyBase, locale);
        }
        return NumberUtils.formatMoney(moneyBase);
    }

    public Double getMoneyTax() {
        return moneyTax;
    }

    public String getMoneyTaxText() {
        if (locale != null) {
            return NumberUtils.formatMoney(moneyTax, locale);
        }
        return NumberUtils.formatMoney(moneyTax);
    }

    public void setMoneyTax(Double moneyTax) {
        this.moneyTax = moneyTax;
    }

    public Double getMoneyRetention() {
        return moneyRetention;
    }

    public String getMoneyRetentionText() {
        if (locale != null) {
            return NumberUtils.formatMoney(moneyRetention, locale);
        }
        return NumberUtils.formatMoney(moneyRetention);
    }

    public void setMoneyRetention(Double moneyRetention) {
        this.moneyRetention = moneyRetention;
    }

    public Double getMoneyPayable() {
        return moneyPayable;
    }

    public String getMoneyPayableText() {
        if (locale != null) {
            return NumberUtils.formatMoney(moneyPayable, locale);
        }
        return NumberUtils.formatMoney(moneyPayable);
    }

    public void setMoneyPayable(Double moneyPayable) {
        this.moneyPayable = moneyPayable;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getCompletedUser() {
        return completedUser;
    }

    public void setCompletedUser(String completedUser) {
        this.completedUser = completedUser;
    }

    public String getCompletedDescription() {
        return completedDescription;
    }

    public void setCompletedDescription(String completedDescription) {
        this.completedDescription = completedDescription;
    }

    public String getAmountText() {
        if (locale != null) {
            return NumberUtils.formatMoney(amount, locale);
        }
        return NumberUtils.formatMoney(amount);
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setIprf(boolean iprf) {
        this.iprf = iprf;
    }

    public boolean getIprf() {
        return iprf;
    }

    @Override
    public String toString() {
        return "#" + id + " " + member;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getAccountBank() {
        return accountBank;
    }

    public void setAccountBank(String accountBank) {
        this.accountBank = accountBank;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    public void setAccountIban(String accountIban) {
        this.accountIban = accountIban;
    }

    public String getAccountIban() {
        return accountIban;
    }

    public void setAccountSwift(String accountSwift) {
        this.accountSwift = accountSwift;
    }

    public String getAccountSwift() {
        return accountSwift;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

}
