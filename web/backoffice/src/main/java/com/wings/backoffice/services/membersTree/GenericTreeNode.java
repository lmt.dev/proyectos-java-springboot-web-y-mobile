/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.membersTree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author seba
 * @param <T>
 */
public class GenericTreeNode<T> {

    private T data;
    private List<GenericTreeNode<T>> children;
    private GenericTreeNode<T> parent;

    public GenericTreeNode() {
        super();
        children = new ArrayList<>();
    }

    public GenericTreeNode(T data) {
        this();
        this.data = data;
    }

    public GenericTreeNode<T> getParent() {
        return this.parent;
    }

    public List<GenericTreeNode<T>> getChildren() {
        return this.children;
    }

    public List<Long> getChildrenIds() {
        List<Long> ret = getChildIds(this);
        ret.add((Long) getData());
        return ret;
    }

    private List<Long> getChildIds(GenericTreeNode<T> node) {
        Set<Long> temp = new HashSet<>();
        node.children.stream().map((child) -> {
            temp.add((Long) child.getData());
            return child;
        }).forEachOrdered((child) -> {
            temp.addAll(getChildIds(child));
        });
        return new LinkedList<>(temp);
    }

    public List<Long> getParentIds() {
        List<Long> ret = new LinkedList<>();
        if (parent != null) {
            GenericTreeNode<T> tempParent;
            GenericTreeNode<T> currentNode;
            currentNode = parent;
            ret.add((Long) currentNode.getData());
            while ((tempParent = currentNode.getParent()) != null) {
                ret.add((Long) tempParent.getData());
                currentNode = tempParent;
            }
        }
        return ret;
    }

    public int getNumberOfChildren() {
        return getChildren().size();
    }

    public boolean hasChildren() {
        return (getNumberOfChildren() > 0);
    }

    public void setChildren(List<GenericTreeNode<T>> children) {
        children.forEach((child) -> {
            child.parent = this;
        });

        this.children = children;
    }

    public void addChild(GenericTreeNode<T> child) {
        child.parent = this;
        children.add(child);
    }

    public void addChildAt(int index, GenericTreeNode<T> child) throws IndexOutOfBoundsException {
        child.parent = this;
        children.add(index, child);
    }

    public void removeChildren() {
        this.children = new ArrayList<>();
    }

    public void removeChildAt(int index) throws IndexOutOfBoundsException {
        children.remove(index);
    }

    public GenericTreeNode<T> getChildAt(int index) throws IndexOutOfBoundsException {
        return children.get(index);
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return getData().toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GenericTreeNode<?> other = (GenericTreeNode<?>) obj;
        if (data == null) {
            if (other.data != null) {
                return false;
            }
        } else if (!data.equals(other.data)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
    * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        return result;
    }

    public String toStringVerbose() {
        String stringRepresentation = getData().toString() + ":[";

        stringRepresentation = getChildren().stream().map((node) -> node.getData().toString() + ", ").reduce(stringRepresentation, String::concat);

        //Pattern.DOTALL causes ^ and $ to match. Otherwise it won't. It's retarded.
        Pattern pattern = Pattern.compile(", $", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(stringRepresentation);

        stringRepresentation = matcher.replaceFirst("");
        stringRepresentation += "]";

        return stringRepresentation;
    }
}
