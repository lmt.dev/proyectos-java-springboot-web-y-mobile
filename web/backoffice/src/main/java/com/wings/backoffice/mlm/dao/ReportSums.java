/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.dao;

/**
 *
 * @author seba
 */
public class ReportSums {

    private String currency;
    private String country;
    private String paymentType;
    private double amount;
    private long recordCount;

    public ReportSums(String currency, String country, String paymentType, double amount, long recordCount) {
        this.currency = currency;
        this.country = country;
        this.paymentType = paymentType;
        this.amount = amount;
        this.recordCount = recordCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(long recordCount) {
        this.recordCount = recordCount;
    }

}
