/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.invoices.peru;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class Item {

    @JsonProperty("txtITEM")
    private Integer textItem;
    @JsonProperty("txtUNIDAD_MEDIDA_DET")
    private String textUnidadMedidaDet;
    @JsonProperty("txtCANTIDAD_DET")
    private String textCantidadDet;
    @JsonProperty("txtPRECIO_DET")
    private String textPrecioDet;
    @JsonProperty("ICBPER_DET")
    private Integer icbperDet;
    @JsonProperty("txtSUB_TOTAL_DET")
    private String textSubTotalDet;
    @JsonProperty("txtPRECIO_TIPO_CODIGO")
    private String textPrecioTipoCodigo;
    @JsonProperty("txtIGV")
    private String textIGV;
    @JsonProperty("txtISC")
    private String textISC;
    @JsonProperty("txtIMPORTE_DET")
    private String textImporteDet;
    @JsonProperty("txtCOD_TIPO_OPERACION")
    private String textCodigoTipoOperacion;
    @JsonProperty("txtCODIGO_DET")
    private String textCodigoDet;
    @JsonProperty("txtDESCRIPCION_DET")
    private String textDescripcionDet;
    @JsonProperty("txtPRECIO_SIN_IGV_DET")
    private String textPrecioSinIGVDet;
    @JsonProperty("txtCODIGO_PROD_SUNAT")
    private String textCodigoProdSUNAT;

    public Integer getTextItem() {
        return textItem;
    }

    public void setTextItem(Integer textItem) {
        this.textItem = textItem;
    }

    public String getTextUnidadMedidaDet() {
        return textUnidadMedidaDet;
    }

    public void setTextUnidadMedidaDet(String textUnidadMedidaDet) {
        this.textUnidadMedidaDet = textUnidadMedidaDet;
    }

    public String getTextCantidadDet() {
        return textCantidadDet;
    }

    public void setTextCantidadDet(String textCantidadDet) {
        this.textCantidadDet = textCantidadDet;
    }

    public String getTextPrecioDet() {
        return textPrecioDet;
    }

    public void setTextPrecioDet(String textPrecioDet) {
        this.textPrecioDet = textPrecioDet;
    }

    public Integer getIcbperDet() {
        return icbperDet;
    }

    public void setIcbperDet(Integer icbperDet) {
        this.icbperDet = icbperDet;
    }

    public String getTextSubTotalDet() {
        return textSubTotalDet;
    }

    public void setTextSubTotalDet(String textSubTotalDet) {
        this.textSubTotalDet = textSubTotalDet;
    }

    public String getTextPrecioTipoCodigo() {
        return textPrecioTipoCodigo;
    }

    public void setTextPrecioTipoCodigo(String textPrecioTipoCodigo) {
        this.textPrecioTipoCodigo = textPrecioTipoCodigo;
    }

    public String getTextIGV() {
        return textIGV;
    }

    public void setTextIGV(String textIGV) {
        this.textIGV = textIGV;
    }

    public String getTextISC() {
        return textISC;
    }

    public void setTextISC(String textISC) {
        this.textISC = textISC;
    }

    public String getTextImporteDet() {
        return textImporteDet;
    }

    public void setTextImporteDet(String textImporteDet) {
        this.textImporteDet = textImporteDet;
    }

    public String getTextCodigoTipoOperacion() {
        return textCodigoTipoOperacion;
    }

    public void setTextCodigoTipoOperacion(String textCodigoTipoOperacion) {
        this.textCodigoTipoOperacion = textCodigoTipoOperacion;
    }

    public String getTextCodigoDet() {
        return textCodigoDet;
    }

    public void setTextCodigoDet(String textCodigoDet) {
        this.textCodigoDet = textCodigoDet;
    }

    public String getTextDescripcionDet() {
        return textDescripcionDet;
    }

    public void setTextDescripcionDet(String textDescripcionDet) {
        this.textDescripcionDet = textDescripcionDet;
    }

    public String getTextPrecioSinIGVDet() {
        return textPrecioSinIGVDet;
    }

    public void setTextPrecioSinIGVDet(String textPrecioSinIGVDet) {
        this.textPrecioSinIGVDet = textPrecioSinIGVDet;
    }

    public String getTextCodigoProdSUNAT() {
        return textCodigoProdSUNAT;
    }

    public void setTextCodigoProdSUNAT(String textCodigoProdSUNAT) {
        this.textCodigoProdSUNAT = textCodigoProdSUNAT;
    }

}
