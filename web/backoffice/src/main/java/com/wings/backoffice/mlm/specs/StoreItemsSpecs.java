/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class StoreItemsSpecs {

    public static Specification<StoreItem> search(String q, Boolean enabled, String category, String status, String country) {

        return (Root<StoreItem> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                //if it's numeric, search only by id
                try {
                    Long id = Long.parseLong(q);
                    if (id < 100000) {
                        predicates.add(builder.equal(root.get("id"), id));
                        return builder.and(predicates.toArray(new Predicate[]{}));
                    }
                } catch (Exception e) {
                }

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("code"), search),
                                builder.like(root.get("name"), search),
                                builder.like(root.get("description"), search)
                        )
                );
            }

            if (enabled != null) {
                predicates.add(builder.equal(root.get("enabled"), enabled));
            }

            if (category != null) {
                predicates.add(builder.equal(root.get("categoryName"), category));
            }

            /*if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }*/
            if (country != null) {
                if (country.equals("other")) {
                    //TODO: cambia a listado de paises por config
                    List<String> countries = Arrays.asList("CO", "EC", "PE", "ES");
                    predicates.add(root.get("countries").in(countries).not());
                } else {
                    String like = "%" + country + "%";
                    predicates.add(builder.like(root.get("countries"), like));
                }
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    public static Specification<StoreItem> getInternalStore(String q, Boolean enabled, String category, String status, String country) {

        return (Root<StoreItem> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                //if it's numeric, search only by id
                try {
                    Long id = Long.parseLong(q);
                    if (id < 100000) {
                        predicates.add(builder.equal(root.get("id"), id));
                        return builder.and(predicates.toArray(new Predicate[]{}));
                    }
                } catch (Exception e) {
                }

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("code"), search),
                                builder.like(root.get("name"), search),
                                builder.like(root.get("description"), search)
                        )
                );
            }

            if (enabled != null) {
                predicates.add(builder.equal(root.get("enabled"), enabled));
            }

            if (category != null) {
                predicates.add(builder.equal(root.get("categoryName"), category));
            }

            /*if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }*/
            if (country != null) {
                String like = "%" + country + "%";
                predicates.add(builder.like(root.get("countries"), like));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
