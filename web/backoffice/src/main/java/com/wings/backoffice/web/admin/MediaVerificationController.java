/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberBankAccount;
import com.wings.backoffice.mlm.domain.MemberMedia;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.enums.MediaStatus;
import com.wings.backoffice.mlm.enums.MediaType;
import com.wings.backoffice.mlm.repos.MembersBankAccountRepository;
import com.wings.backoffice.mlm.repos.MembersMediaRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class MediaVerificationController {

    @Autowired
    private MediaService mediaService;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private MembersMediaRepository membersMediaRepository;

    @Autowired
    private MembersBankAccountRepository membersBankAccountRepository;

    /**
     * List all verifications
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/financial/verifications"}, method = RequestMethod.GET)
    public String listVerifications(Model model, HttpServletRequest request) {

        String status = request.getParameter("status");
        if (status == null) {
            status = "pending";
        }

        List<MemberMedia> medias;
        List<String> types = new ArrayList<>();
        types.add(MediaType.address.name());
        types.add(MediaType.dni.name());
        types.add(MediaType.idt.name());

        if (status.equals("all")) {
            medias = membersMediaRepository.findByTypeIn(types);
        } else {
            medias = membersMediaRepository.findByTypeInAndStatus(types, status);
        }

        List<MemberBankAccount> accounts;
        switch (status) {
            case "pending":
            case "rejected":
                accounts = membersBankAccountRepository.findByVerified(Boolean.FALSE);
                break;
            case "all":
                accounts = membersBankAccountRepository.findAll();
                break;
            default:
                accounts = membersBankAccountRepository.findByVerified(Boolean.TRUE);
                break;
        }

        //BACS are separated
        for (MemberBankAccount account : accounts) {
            String code = mediaService.getBacsCode(account.getId());
            MemberMedia media = mediaService.getMediaByCode(code);
            if (media != null) {
                if (status.equals("all")) {
                    media.setBacsId(account.getId());
                    medias.add(media);
                } else if (media.getStatus().equals(status)) {
                    media.setBacsId(account.getId());
                    medias.add(media);
                }
            }
        }

        model.addAttribute("medias", medias);
        model.addAttribute("status", status);

        return "admin/financial/verifications";
    }

    /**
     * List medias for profile
     *
     * @param model
     * @param request
     * @param memberId
     * @return
     */
    @RequestMapping(value = {"admin/financial/verification/{id}"}, method = RequestMethod.GET)
    public String getVerification(Model model, HttpServletRequest request, @PathVariable("id") Long memberId) {

        Member member = membersRepository.findOne(memberId);
        List<MemberMedia> medias = mediaService.getProfileMedias(member);
        model.addAttribute("medias", medias);
        model.addAttribute("member", member);

        return "admin/financial/verification";
    }

    /**
     * list media for a particular bank account
     *
     * @param model
     * @param request
     * @param memberId
     * @param bacsId
     * @return
     */
    @RequestMapping(value = {"admin/financial/verification/{id}/{bacsId}"}, method = RequestMethod.GET)
    public String getVerificationBacs(Model model, HttpServletRequest request, @PathVariable("id") Long memberId, @PathVariable("bacsId") Long bacsId) {

        Member member = membersRepository.findOne(memberId);
        MemberBankAccount acc = membersBankAccountRepository.findOne(bacsId);
        String code = mediaService.getBacsCode(bacsId);

        MemberMedia media = mediaService.getMediaByCode(code);
        model.addAttribute("media", media);
        model.addAttribute("member", member);
        model.addAttribute("acc", acc);

        return "admin/financial/verification-bacs";
    }

    /**
     * Post verifications
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/financial/verification"}, method = RequestMethod.POST)
    public ResponseEntity<String> postVerification(Model model, HttpServletRequest request) {

        AdminUser admin = AuthUtils.getAdminUser();
        if (admin == null) {
            return ResponseEntity.badRequest().body("unauthorized");
        }

        String sid = request.getParameter("id");
        String message = request.getParameter("message");
        String status = request.getParameter("status");
        String bacsId = request.getParameter("bacsId");

        try {
            Long mediaId = Long.parseLong(sid);

            if (bacsId != null && !bacsId.isEmpty()) {
                long bacId = Long.parseLong(bacsId);
                MemberBankAccount acc = membersBankAccountRepository.findOne(bacId);
                if (acc != null) {
                    acc.setVerified(status.equals(MediaStatus.verified.name()));
                    membersBankAccountRepository.save(acc);
                }
            }

            MemberMedia media = membersMediaRepository.findOne(mediaId);
            media.setStatus(status);
            media.setStatusComment(message);
            media.setStatusAudit("Status changed to " + status + " by " + admin.getUsername() + " " + DateUtils.formatNormalDateTime(new Date()));
            membersMediaRepository.save(media);

            return ResponseEntity.ok("Cambios efectuados");

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid id");
        }
    }

}
