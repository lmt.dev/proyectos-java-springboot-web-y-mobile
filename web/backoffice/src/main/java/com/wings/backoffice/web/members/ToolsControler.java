/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CorpoContent;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.tools.Viralmedia;
import com.wings.backoffice.mlm.repos.CorpoContentRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.mlm.repos.tools.ViralmediaRepository;
import com.wings.backoffice.mlm.specs.CorpoContentSpecs;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Date;

/**
 *
 * @author seba
 */
@Controller
public class ToolsControler {

    @Autowired
    private RanksBisRepository ranksBisRepository;
    @Autowired
    private CorpoContentRepository corpoContentRepository;
    @Autowired
    private ViralmediaRepository viralmediaRepository;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/tools/list"}, method = RequestMethod.GET)
    public String getList(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        model.addAttribute("marketingUrl", member.getMarketingUrl());

        Viralmedia viralmedia = viralmediaRepository.findByMemberId(member.getId());
        model.addAttribute("viralmediaUrl", "https://wings.viralmediaig.com/");
        model.addAttribute("viralmedia", viralmedia);

        return "members/tools/list";

    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/tools/formation"}, method = RequestMethod.GET)
    public String getFormation(Model model, HttpServletRequest request) {
        return "members/tools/formation";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/tools/content"}, method = RequestMethod.GET)
    public String getContent(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();

        String rank = "all";
        if (m.getRank() != null && m.getRank().getName() != null) {
            rank += "," + m.getRank().getName();
        }

        String country = "all";
        if (m.getCountry() != null) {
            country += "," + m.getCountry().toLowerCase();
        }

        Boolean published = true;
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String type = request.getParameter("type");

        if (type != null && type.equals("all")) {
            type = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<CorpoContent> items = corpoContentRepository.findAll(CorpoContentSpecs.search(q, rank, country, published, type), pr);

        PageWrapper<CorpoContent> pageWrapper = new PageWrapper<>(items, "members/tools/content");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<RankBis> ranks = ranksBisRepository.findAll();

        model.addAttribute("ranks", ranks);
        model.addAttribute("is_admin", true);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("type", type);

        return "members/tools/content";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/tools/viralmedia"}, method = RequestMethod.GET)
    public String getViralemdia(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        Viralmedia viralmedia = viralmediaRepository.findByMemberId(member.getId());
        if (viralmedia == null) {
            viralmedia = viralmediaRepository.findFirstByTaken(Boolean.FALSE);
            if (viralmedia != null) {
                viralmedia.setMemberId(member.getId());
                viralmedia.setTaken(Boolean.TRUE);
                viralmedia.setTakenDate(new Date());
                viralmediaRepository.save(viralmedia);
            }
        }
        model.addAttribute("viralmedia", viralmedia);

        return "members/tools/viralmedia";
    }

}
