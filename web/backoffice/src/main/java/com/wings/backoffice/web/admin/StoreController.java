package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.SettingsService;
import com.wings.backoffice.services.StoreItemsService;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author seba
 */
@Controller(value = "adminStoreController")
public class StoreController {

    @Autowired
    private StoreItemsService storeItemsService;
    @Autowired
    private SettingsService settingsService;
    @Autowired
    private CountriesService countriesService;

    @RequestMapping(value = {"admin/store/items"}, method = {RequestMethod.GET})
    public String getItems(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String enabled = request.getParameter("enabled");
        String status = request.getParameter("status");
        String country = request.getParameter("country");
        String category = request.getParameter("category");

        if (enabled != null && enabled.equals("all")) {
            enabled = null;
        }

        if (status != null && status.equals("all")) {
            status = null;
        }

        if (country != null && country.equals("all")) {
            country = null;
        }

        if (category != null && category.equals("all")) {
            category = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<StoreItem> items = storeItemsService.searchItems(q, page, size, enabled != null ? enabled.equals("true") : null, category, status, country, Sort.Direction.DESC);
        PageWrapper<StoreItem> pageWrapper = new PageWrapper<>(items, "admin/store/items");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("status", status);
        model.addAttribute("countrySelected", country);
        model.addAttribute("countries", countriesService.getEnabledCountries());
        model.addAttribute("enabled", enabled);
        model.addAttribute("category", category);

        return "admin/store/list";
    }

    @RequestMapping(value = {"/admin/store/item/{id}"}, method = {RequestMethod.GET})
    public String getItem(Model model, HttpServletRequest request, @PathVariable("id") Long itemId) {

        List<String> countries = countriesService.getEnabledCountries();
        model.addAttribute("countries", countries);

        StoreItem item = storeItemsService.getOne(itemId);
        model.addAttribute("item", item);

        return "admin/store/item-view";
    }

    @RequestMapping(value = {"/admin/store/item/edit/{id}"}, method = {RequestMethod.GET})
    public String getItemEdit(Model model, HttpServletRequest request, @PathVariable("id") Long itemId) {

        StoreItem item = storeItemsService.getOne(itemId);
        model.addAttribute("item", item);

        List<StoreItem> finalItems = storeItemsService.getUpgradableItems();
        model.addAttribute("finalItems", finalItems);

        List<StoreItem> singleItems = storeItemsService.getSingleItems();
        model.addAttribute("singleItems", singleItems);

        List<String> countries = countriesService.getEnabledCountries();
        model.addAttribute("countries", countries);

        List<String> categories = settingsService.getCategories();
        model.addAttribute("categories", categories);

        List<String> subCategories = settingsService.getSubCategories();
        model.addAttribute("subCategories", subCategories);

        return "admin/store/item-edit";
    }

    @RequestMapping(value = {"/admin/store/item/edit/{id}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postItem(Model model, HttpServletRequest request, @PathVariable("id") Long itemId) {

        StoreItem item = storeItemsService.getOne(itemId);
        if (item == null) {
            return ResponseEntity.badRequest().body("Item no encontrado");
        }

        String action = request.getParameter("action");
        if (action.equals("detail")) {

            String code = request.getParameter("code");
            String name = request.getParameter("name");
            String description = request.getParameter("description");
            String shortDescription = request.getParameter("shortDescription");
            String price = request.getParameter("price");
            String installmentsQtyStr = request.getParameter("installmentsQty");
            String categoryName = request.getParameter("categoryName");
            String subCategoryName = request.getParameter("subCategoryName");
            if (subCategoryName != null && subCategoryName.equals("-1")) {
                subCategoryName = null;
            }
            String rankPoints = request.getParameter("rankPoints");
            String payPoints = request.getParameter("payPoints");
            String single = request.getParameter("single");
            String availableToComplex = request.getParameter("availableToComplex");
            String finalItemId = request.getParameter("finalItemId");
            if (finalItemId != null && finalItemId.equals("-1")) {
                finalItemId = null;
            }
            String shippingScore = request.getParameter("shippingScore");

            List<String> countries = new ArrayList<>();
            List<String> countriesList = settingsService.getCountryCodes();
            countriesList.forEach((country) -> {
                String tmp = request.getParameter(country);
                if (tmp != null && tmp.equals("on")) {
                    countries.add(country.trim());
                }
            });
            String toSet = countries.toString();

            Integer installmentsQty = 0;
            if (installmentsQtyStr != null && !installmentsQtyStr.equals("")) {
                try {
                    installmentsQty = Integer.parseInt(installmentsQtyStr);
                } catch (NumberFormatException e) {
                }
            }
            item.setInstallmentsQty(installmentsQty);
            item.setInstallments(installmentsQty > 0);
            item.setCode(code);
            item.setName(name);
            item.setDescription(description);
            item.setPrice(price != null ? Double.valueOf(price) : null);
            item.setCategoryName(categoryName);
            item.setSubCategoryName(subCategoryName);
            item.setRankPoints(rankPoints != null ? Double.valueOf(rankPoints) : null);
            item.setPayPoints(payPoints != null ? Double.valueOf(payPoints) : null);
            item.setSingle(single != null && single.equals("on"));
            item.setAvailableToComplex(availableToComplex != null && availableToComplex.equals("on"));
            item.setFinalItemId(finalItemId != null ? Long.valueOf(finalItemId) : null);
            item.setCountries(toSet.replaceAll("\\[", "").replaceAll("]", ""));
            item.setShortDescription(shortDescription);

            //por defecto es 1 que es el primer shipping del country
            Double dscore = 1.0d;
            try {
                dscore = Double.parseDouble(shippingScore);
            } catch (Exception e) {
            }
            item.setShippingScore(dscore);

            storeItemsService.save(item);
            return ResponseEntity.ok("Item modificado correctamente");

        } else if (action.equals("children")) {

            String childId = request.getParameter("childId");
            String quantity = request.getParameter("quantity");
            storeItemsService.addChildren(itemId, Long.valueOf(childId), Integer.valueOf(quantity));
            return ResponseEntity.ok("Hijo añadido correctamente");

        } else if (action.equals("children-remove")) {

            String childId = request.getParameter("childId");
            storeItemsService.removeChildren(Long.valueOf(childId));
            return ResponseEntity.ok("Hijo eliminado correctamente");

        }

        return ResponseEntity.badRequest().body("Error modificando item");
    }

    @RequestMapping(value = {"/admin/store/item/enable/{id}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postItemEnabled(Model model, HttpServletRequest request, @PathVariable("id") Long itemId) {
        StoreItem item = storeItemsService.getOne(itemId);
        if (item == null) {
            return ResponseEntity.badRequest().body("Item no encontrado");
        }
        item.setEnabled(!item.getEnabled());
        storeItemsService.save(item);
        return ResponseEntity.ok("Item modificado correctamente");
    }

    @RequestMapping(value = {"/admin/store/item/create"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postItemCreate(Model model, HttpServletRequest request) {

        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            return ResponseEntity.badRequest().body("El código es obligatorio");
        }

        StoreItem item = new StoreItem();
        item.setCode(code);
        storeItemsService.save(item);
        return ResponseEntity.ok("Item creado correctamente");
    }

    @RequestMapping(value = {"/admin/store/item/edit/attributes/{id}"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> editAttributes(Model model, HttpServletRequest request, @RequestParam("attributes") String attributes, @PathVariable("id") Long itemId) {

        boolean save = storeItemsService.saveAttributes(itemId, attributes);

        if (save) {
            return ResponseEntity.ok("Atributos guardados correctamente.");
        }
        return ResponseEntity.badRequest().body("Error al guardar los atributos");
    }

    @RequestMapping(value = {"/admin/store/item/edit/childrens/{id}"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> editChildrens(Model model, HttpServletRequest request, @RequestParam("childrens") String childrens, @PathVariable("id") Long itemId) {

        boolean save = storeItemsService.saveChildrens(itemId, childrens);

        if (save) {
            return ResponseEntity.ok("Hijos guardados correctamente.");
        }
        return ResponseEntity.badRequest().body("Error al guardar los atributos");
    }

    /**
     *
     * @param model
     * @param request
     * @param countries
     * @param itemId
     * @return
     */
    @RequestMapping(value = {"/admin/store/item/edit/countries/{id}"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> editCountries(Model model, HttpServletRequest request, @RequestParam("countries") String countries, @PathVariable("id") Long itemId) {

        boolean save = storeItemsService.saveCountries(itemId, countries);

        if (save) {
            return ResponseEntity.ok("Localizaciones guardados correctamente.");
        }
        return ResponseEntity.badRequest().body("Error al guardar las localizaciones");
    }

    /**
     *
     * @param model
     * @param request
     * @param countries
     * @param itemId
     * @return
     */
    @RequestMapping(value = {"/admin/store/item/edit/images/{id}"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> editImages(Model model, HttpServletRequest request, @RequestParam("images") String images, @PathVariable("id") Long itemId) {

        boolean save = storeItemsService.saveImages(itemId, images);

        if (save) {
            return ResponseEntity.ok("Imágenes guardados correctamente.");
        }
        return ResponseEntity.badRequest().body("Error al guardar las imágenes");
    }

    @RequestMapping(value = {"/admin/store/item/edit/images/main/{id}"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> setImageMain(Model model, HttpServletRequest request, @RequestParam("imageUrl") String imageUrl, @PathVariable("id") Long itemId) {

        boolean save = storeItemsService.saveImageMain(itemId, imageUrl);

        if (save) {
            return ResponseEntity.ok("Imágenes guardados correctamente.");
        }
        return ResponseEntity.badRequest().body("Error al guardar las imágenes");
    }
}
