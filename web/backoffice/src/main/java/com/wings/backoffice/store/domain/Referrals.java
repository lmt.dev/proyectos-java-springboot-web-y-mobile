/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "wingstore_affiliate_wp_referrals")
public class Referrals implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long referralId;
    private long affiliateId;
    private long visitId;
    private String description;
    private String status;
    private String amount;
    private String currency;
    private String custom;
    private String context;
    private String campaign;
    private String reference;
    private String products;
    private long payoutId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Referrals() {
    }

    public Referrals(Long referralId) {
        this.referralId = referralId;
    }

    public Referrals(Long referralId, long affiliateId, long visitId, String description, String status, String amount, String currency, String custom, String context, String campaign, String reference, String products, long payoutId, Date date) {
        this.referralId = referralId;
        this.affiliateId = affiliateId;
        this.visitId = visitId;
        this.description = description;
        this.status = status;
        this.amount = amount;
        this.currency = currency;
        this.custom = custom;
        this.context = context;
        this.campaign = campaign;
        this.reference = reference;
        this.products = products;
        this.payoutId = payoutId;
        this.date = date;
    }

    public Long getReferralId() {
        return referralId;
    }

    public void setReferralId(Long referralId) {
        this.referralId = referralId;
    }

    public long getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(long affiliateId) {
        this.affiliateId = affiliateId;
    }

    public long getVisitId() {
        return visitId;
    }

    public void setVisitId(long visitId) {
        this.visitId = visitId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public long getPayoutId() {
        return payoutId;
    }

    public void setPayoutId(long payoutId) {
        this.payoutId = payoutId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referralId != null ? referralId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Referrals)) {
            return false;
        }
        Referrals other = (Referrals) object;
        if ((this.referralId == null && other.referralId != null) || (this.referralId != null && !this.referralId.equals(other.referralId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.backoffice.store.domain.Referrals[ referralId=" + referralId + " ]";
    }
    
}
