/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankHistory;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.RankHistoryRepository;
import com.wings.backoffice.mlm.specs.CalificationSpecs;
import com.wings.backoffice.mlm.specs.RankHistorySpecs;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller("membersRanksController")
public class RanksController {

    @Autowired
    private RankHistoryRepository rankHistoryRepository;

    @Autowired
    private CalificationPointsRepository calificationPointsRepository;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = {"/members/ranks/calification"}, method = RequestMethod.GET)
    public String getCalificationList(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String from = request.getParameter("from");
        String to = request.getParameter("to");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Date f = null;
        Date t = null;
        if (from != null) {
            try {
                f = sdf.parse(from);
                f = DateUtils.getFirstHourOfDay(f);
            } catch (ParseException e) {
            }
        }
        if (to != null) {
            try {
                t = sdf.parse(to);
                t = DateUtils.getLastHourOfDay(t);
            } catch (ParseException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "date"));
        Page<CalificationPoint> items = calificationPointsRepository.findAll(CalificationSpecs.searchPointsByMember(m.getId(), f, t), pr);

        PageWrapper<CalificationPoint> pageWrapper = new PageWrapper<>(items, "members/ranks/calification");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("size", ssize);
        model.addAttribute("from", from);
        model.addAttribute("to", to);

        return "members/ranks/calification";
    }

    @RequestMapping(value = {"/members/ranks/history"}, method = RequestMethod.GET)
    public String ranksHistory(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String auto = request.getParameter("auto");

        Boolean bauto = null;
        if (auto != null && !auto.equals("all")) {
            bauto = auto.equals("true");
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "date"));
        Page<RankHistory> items = rankHistoryRepository.findAll(RankHistorySpecs.searchByMember(m.getId(), bauto), pr);

        PageWrapper<RankHistory> pageWrapper = new PageWrapper<>(items, "members/ranks/history");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("auto", auto);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "members/ranks/history";
    }

}
