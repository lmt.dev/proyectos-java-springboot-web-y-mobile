/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.orders.Order;

import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.enums.EventMemberTpe;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.CalificationPointsRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.specs.CalificationSpecs;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Component
public class CalificationService {

    @Autowired
    private MembersRepository membersRepository;
    @Autowired
    private MembersService membersService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private CalificationPointsRepository calificationPointsRepository;

    private final Logger log = LoggerFactory.getLogger(CalificationService.class);

    private final Map<String, Member> memberCacheUsername = new HashMap<>();

    public long getCalificationPoints(Long memberId) {
        return calificationPointsRepository.getTotalCalificationPoints(memberId);
    }

    public List<CalificationPoint> getPointsForMember(Member member, Date from, Date to) {
        return calificationPointsRepository.findAll(CalificationSpecs.searchPointsByMember(member.getId(), from, to));
    }

    @Transactional
    public void parse() {

        log.error("Start calification parse");
        memberCacheUsername.clear();
        List<Member> membersTemp = membersRepository.findAll();
        membersTemp.forEach((member) -> {
            memberCacheUsername.put(member.getUsername(), member);
        });

        membersService.clearCache();
        List<CalificationPoint> ret = new ArrayList<>();
        List<Order> orders = ordersService.findByActiveAndDeleted(Boolean.TRUE, Boolean.FALSE);

        orders.forEach((order) -> {
            if (order.getType().equals(OrderType.kit.name()) || order.getType().equals(OrderType.store.name())) {
                ret.addAll(parseKitOrder(order));
            } else if (order.getType().equals(OrderType.product.name())) {
                ret.addAll(parseProductOrder(order));
            } else if (order.getType().equals(OrderType.service.name())) {
                ret.addAll(parseServiceOrder(order));
            } else if (order.getType().equals(OrderType.upgrade.name())) {
                ret.addAll(parseUpgradeOrder(order));
            }
        });

        calificationPointsRepository.deleteAll();

        //Repaso todo y hago x2 promo covid-19
        Date promoFrom = DateUtils.fromMysqlStringToDate("2020-04-01 00:00:00");
        Date promoTo = DateUtils.fromMysqlStringToDate("2020-08-05 23:59:59");
        for (CalificationPoint cp : ret) {
            if (DateUtils.dateInRange(cp.getDate(), promoFrom, promoTo)) {
                double d = NumberUtils.round(cp.getPoints(), 1);
                cp.setDescription(cp.getDescription() + " (" + d + "x2)");
                cp.setPoints(cp.getPoints() * 2);
            }
        }

        int chunk = ret.size() / 10;
        int from = 0;
        int to = chunk;
        for (int i = 0; i < 10; i++) {

            List<CalificationPoint> subret = ret.subList(from, to);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    calificationPointsRepository.save(subret);
                }
            }).start();

            from = to + 1;
            to = to + chunk;
        }

        //si sobraron de la division
        if (from <= ret.size()) {
            calificationPointsRepository.save(ret.subList(from, ret.size()));
        }

        /*ret.forEach((calificationPoint) -> {
            calificationPointsRepository.save(calificationPoint);
        });*/
        log.error("Done calification parse");
    }

    /**
     * For one order after is complete
     *
     * @param order
     */
    public void parse(Order order) {

        //fill cache
        memberCacheUsername.clear();
        List<Member> membersTemp = membersRepository.findAll();
        membersTemp.forEach((member) -> {
            memberCacheUsername.put(member.getUsername(), member);
        });

        List<CalificationPoint> ret = new ArrayList<>();
        if (order.getType().equals(OrderType.kit.name()) || order.getType().equals(OrderType.store.name())) {
            ret.addAll(parseKitOrder(order));
        } else if (order.getType().equals(OrderType.product.name())) {
            ret.addAll(parseProductOrder(order));
        } else if (order.getType().equals(OrderType.service.name())) {
            ret.addAll(parseServiceOrder(order));
        } else if (order.getType().equals(OrderType.upgrade.name())) {
            ret.addAll(parseUpgradeOrder(order));
        }

        calificationPointsRepository.save(ret);

    }

    private List<CalificationPoint> parseKitOrder(Order order) {
        List<CalificationPoint> ret = new ArrayList<>();

        if (order.isDeleted()) {
            return ret;
        }

        if (!order.getActive()) {
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        StoreItem i = order.getOrderItem();
        /*if (i.getRankPoints() == 0) {
            return ret;
        }*/

        if (order.getMemberId() == null) {
            log.debug("No member found for order " + order.getId());
            return ret;
        }

        if (order.getStoreId() == null) {
            log.debug("No Store id found for order " + order.getId());
        }

        Member m = order.getMember();
        List<Long> ids = membersService.getUplineIds(order.getMemberId());
        if (ids == null || ids.isEmpty()) {
            return ret;
        }

        ids.stream().map((id) -> {
            CalificationPoint cp = new CalificationPoint();
            cp.setDate(order.getActivationDate());
            cp.setDescription((order.getType().equals(OrderType.kit.name()) ? "Kit " : "Store ") + i.getName() + " - " + m.getNameText());
            cp.setMemberId(id);
            cp.setType(order.getType());
            cp.setEventId(order.getId());
            cp.setPoints(i.getRankPoints());

            if (order.getMemberId().equals(id)) {
                cp.setMemberType(EventMemberTpe.own.name());
            } else if (order.getStoreId() != null) {

                Member storeOwner = memberCacheUsername.get(order.getStoreId());
                if (storeOwner != null && storeOwner.getId().equals(id)) {
                    cp.setMemberType(EventMemberTpe.direct.name());
                } else {
                    cp.setMemberType(EventMemberTpe.indirect.name());
                }
            } else {
                cp.setMemberType(EventMemberTpe.indirect.name());
            }

            return cp;
        }).forEachOrdered((cp) -> {
            ret.add(cp);
        });

        return ret;
    }

    private List<CalificationPoint> parseUpgradeOrder(Order order) {
        List<CalificationPoint> ret = new ArrayList<>();

        if (order.isDeleted()) {
            return ret;
        }

        if (!order.getActive()) {
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        StoreItem i = order.getOrderItem();
        if (order.getMemberId() == null && order.getStoreId() == null) {
            log.debug("No member or sponsor found for order " + order.getId());
            return ret;
        }

        final Member m = order.getMember() != null ? order.getMember() : memberCacheUsername.get(order.getStoreId());
        if (m == null) {
            log.debug("No member or sponsor found for order " + order.getId());
            return ret;
        }

        //update member id
        if (order.getMemberId() == null) {
            order.setMemberId(m.getId());
            ordersService.save(order);
        }

        List<Long> ids = membersService.getUplineIds(m.getId());
        if (ids == null || ids.isEmpty()) {
            log.debug("No upline for member " + m.getId());
            return ret;
        }

        ids.stream().map((id) -> {

            CalificationPoint cp = new CalificationPoint();
            cp.setDate(order.getActivationDate());
            cp.setDescription(i.getName() + " - " + m.getNameText());
            cp.setMemberId(id);
            cp.setType(OrderType.upgrade.name());
            cp.setEventId(order.getId());
            cp.setPoints(i.getRankPoints());

            if (m.getId().equals(id)) {
                cp.setMemberType(EventMemberTpe.own.name());
            } else if (order.getStoreId() != null) {

                Member storeOwner = memberCacheUsername.get(order.getStoreId());
                if (storeOwner != null && storeOwner.getId().equals(id)) {
                    cp.setMemberType(EventMemberTpe.direct.name());
                } else {
                    cp.setMemberType(EventMemberTpe.indirect.name());
                }
            } else {
                cp.setMemberType(EventMemberTpe.indirect.name());
            }

            return cp;
        }).forEachOrdered((cp) -> {
            ret.add(cp);
        });

        return ret;
    }

    /**
     * este tiene que hacer el control de los 7 niveles
     *
     * @param order
     * @return
     */
    private List<CalificationPoint> parseProductOrder(Order order) {
        List<CalificationPoint> ret = new ArrayList<>();

        if (order.isDeleted()) {
            return ret;
        }

        if (!order.getActive()) {
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        if (order.getStoreId() == null || order.getStoreId().trim().isEmpty()) {
            log.debug("No store found for order " + order.getId());
            return ret;
        }

        Member m = memberCacheUsername.get(order.getStoreId());
        if (m == null) {
            log.debug("No member found for store id " + order.getStoreId() + " for order " + order.getId());
            return ret;
        }

        StoreItem i = order.getOrderItem();
        List<Long> ids = membersService.getUplineIds(m.getId());

        if (ids == null) {
            ids = new ArrayList<>();
        }

        if (i.getRankPoints() == 0) {
            return ret;
        }

        int level = 0;
        for (Long id : ids) {

            CalificationPoint cp = new CalificationPoint();
            cp.setDate(order.getActivationDate());
            cp.setDescription("Product " + i.getName() + " - " + m.getNameText());
            cp.setMemberId(id);
            cp.setEventId(order.getId());
            cp.setPoints(i.getRankPoints());
            cp.setType(OrderType.product.name());

            Member storeOwner = memberCacheUsername.get(order.getStoreId());
            if (storeOwner.getId().equals(id)) {
                cp.setMemberType(EventMemberTpe.direct.name());
            } else {
                cp.setMemberType(EventMemberTpe.indirect.name());
            }

            ret.add(cp);

            level++;

            if (level == 8) {
                break;
            }
        }

        return ret;
    }

    private List<CalificationPoint> parseServiceOrder(Order order) {
        List<CalificationPoint> ret = new ArrayList<>();

        if (order.isDeleted()) {
            return ret;
        }

        if (!order.getActive()) {
            log.debug("Order not active " + order.getId());
            return ret;
        }

        if (order.getOrderItem() == null) {
            log.debug("No item found for order " + order.getId());
            return ret;
        }

        if (order.getStoreId() == null || order.getStoreId().trim().isEmpty()) {
            log.debug("No store found for order " + order.getId());
            return ret;
        }

        Member m = memberCacheUsername.get(order.getStoreId().trim());
        if (m == null) {
            log.debug("No member found for store id " + order.getStoreId());
            return ret;
        }

        StoreItem i = order.getOrderItem();
        List<Long> ids = membersService.getUplineIds(m.getId());

        if (ids == null) {
            log.debug("Afiliado sin upline " + m.getId() + " " + m.toString());
            return ret;
        }

        if (i.getRankPoints() == 0) {
            return ret;
        }

        Calendar cal = Calendar.getInstance();
        Calendar cal1 = Calendar.getInstance();

        int level = 0;
        for (Long id : ids) {

            CalificationPoint cp = new CalificationPoint();

            cal.setTime(order.getActivationDate());
            cal1.setTime(order.getCreationDate());

            cal.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal1.get(Calendar.SECOND));

            cp.setDate(cal.getTime());
            cp.setDescription("Service " + i.getName() + " - " + order.getFullName());
            cp.setMemberId(id);
            cp.setEventId(order.getId());
            cp.setPoints(i.getRankPoints());
            cp.setType(OrderType.service.name());

            if (order.getMember() != null && order.getMemberId().equals(id)) {
                cp.setMemberType(EventMemberTpe.own.name());
            } else {
                Member storeOwner = memberCacheUsername.get(order.getStoreId());
                if (storeOwner != null && storeOwner.getId().equals(id)) {
                    cp.setMemberType(EventMemberTpe.direct.name());
                } else {
                    cp.setMemberType(EventMemberTpe.indirect.name());
                }
            }

            ret.add(cp);

            level++;

            if (level == 7) {
                break;
            }
        }

        return ret;
    }

}
