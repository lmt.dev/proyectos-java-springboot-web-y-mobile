package com.wings.backoffice.mlm.domain.bis.orders;

import com.wings.backoffice.utils.DateUtils;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vw_ordersActives")
public class ProductsView implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    private Long orderId;
    @Temporal(TemporalType.DATE)
    private Date activationDate;
    private Long itemId;
    private String type;
    private String storeTitle;
    private String billingCountry;
    private Long memberId;
    private Boolean installmentComplete;
    private Integer installmentNumber;

    public ProductsView() {
        this(null, null, null, null, null, null);
    }

    public ProductsView(Long orderId, Date activationDate, Long itemId, String type, String storeTitle, String billingCountry) {
        this.orderId = orderId;
        this.activationDate = activationDate;
        this.itemId = itemId;
        this.type = type;
        this.storeTitle = storeTitle;
        this.billingCountry = billingCountry;
    }

    public Long getId() {
        return orderId;
    }

    public void setId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStoreTitle() {
        return storeTitle;
    }

    public void setStoreTitle(String storeTitle) {
        this.storeTitle = storeTitle;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }
    
    public Integer getMonth(){
        Calendar c = new GregorianCalendar();
        c.setTime(activationDate);
        return c.get(Calendar.MONTH) + 1;
    }
    
    public Integer getYear(){
        Calendar c = new GregorianCalendar();
        c.setTime(activationDate);
        return c.get(Calendar.YEAR);
    }
    
    public String getMonthStr(){
        return DateUtils.getMonthName(getMonth());
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Boolean getInstallmentComplete() {
        return installmentComplete;
    }

    public void setInstallmentComplete(Boolean installmentComplete) {
        this.installmentComplete = installmentComplete;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }
}