/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class ShipmentsSpecs {

    public static Specification<Shipment> search(String q, String status, String deleted, String country) {

        return (Root<Shipment> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Order, Shipment> order = root.join("order");

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {

                try {
                    
                                        
                    Long id = Long.valueOf(q);
                    builder.or(builder.equal(root.get("id"), id));
                } catch (Exception e) {
                }

                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("shippingName"), search),
                                builder.like(root.get("email"), search),
                                builder.like(root.get("username"), search),
                                builder.like(root.get("shipmentNumber"), search),
                                builder.like(root.get("trackingNumber"), search),
                                builder.like(order.get("billingName"), search),
                                builder.like(order.get("billingCompany"), search),
                                builder.like(builder.concat(builder.concat(order.get("firstName"), " "), order.get("lastName")), search)
                        )
                );
            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (deleted != null && !deleted.equals("all")) {
                predicates.add(builder.equal(root.get("deleted"), deleted.equals("true")));
            }

            if (country != null) {
                predicates.add(builder.equal(root.get("shippingCountry"), country));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
