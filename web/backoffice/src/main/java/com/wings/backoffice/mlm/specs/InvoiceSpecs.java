/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class InvoiceSpecs {

    public static Specification<Invoice> search(String q, Date from, Date to, String type, String clientDni, String country) {

        return (Root<Invoice> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("invoiceNumber"), search),
                                builder.like(root.get("clientName"), search),
                                builder.like(root.get("companyName"), search)
                        )
                );
            }

            if (from != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("invoiceDate"), from));
            }

            if (to != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("invoiceDate"), to));
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            if (clientDni != null) {
                predicates.add(builder.equal(root.get("clientDni"), clientDni));
            }

            if (country != null) {
                predicates.add(builder.equal(root.get("clientCountry"), country));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
