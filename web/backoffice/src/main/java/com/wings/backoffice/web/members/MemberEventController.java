/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.events.EventTicket;
import com.wings.backoffice.mlm.domain.events.EventTicketAssignation;
import com.wings.backoffice.mlm.domain.events.EventsProductPurchase;
import com.wings.backoffice.mlm.domain.events.enums.EventProductPurchaseStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventStatus;
import com.wings.backoffice.mlm.domain.events.enums.EventTicketStatus;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class MemberEventController {

    @Autowired
    private EventsService eventsService;

    @Autowired
    private MediaService mediaService;

    private final Logger log = LoggerFactory.getLogger(MemberEventController.class);

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/events/tickets"}, method = RequestMethod.GET)
    public String getMemberTickets(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        if (m == null) {
            return "redirect:/members#members/profile";
        }

        List<String> asList = Arrays.asList(EventProductPurchaseStatus.paid.name(), EventProductPurchaseStatus.pending.name());
        List<EventsProductPurchase> purchases = eventsService.getEventPurchasesForMember(m.getId(), asList);

        //remove unpublished events
        List<EventsProductPurchase> toRemove = new ArrayList<>();
        purchases.stream().filter((purchase) -> (!purchase.getEvent().getStatus().equals(EventStatus.published.name()))).forEachOrdered((purchase) -> {
            toRemove.add(purchase);
        });
        toRemove.forEach((eventsProductPurchase) -> {
            purchases.remove(eventsProductPurchase);
        });

        model.addAttribute("purchases", purchases);

        return "members/events/tickets";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/events/invites"}, method = RequestMethod.GET)
    public String getMemberInvites(Model model, HttpServletRequest request) {
        Member m = AuthUtils.getMemberUser();

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        //eventsService.get
        //get  list of purchases ids
        List<String> asList = Arrays.asList(EventProductPurchaseStatus.paid.name());
        List<EventsProductPurchase> myPurchases = eventsService.getEventPurchasesForMember(m.getId(), asList);
        List<Long> purchasesIds = new ArrayList<>();
        myPurchases.forEach((p) -> {
            //only publised events
            if (p.getEvent().getStatus().equals(EventStatus.published.name())) {
                purchasesIds.add(p.getId());
            }
        });

        //get List of tickets ids
        List<Long> ticketIds = new ArrayList<>();
        List<EventTicketAssignation> ass = eventsService.getAssignationTickets(purchasesIds);
        ass.forEach((as) -> {
            ticketIds.add(as.getTicketId());
        });

        //get tickets
        Page<EventTicket> items = eventsService.searchTicketsIn(q, page, size, ticketIds);

        //busco si ya tome un ticket para el evento
        Map<Long, Boolean> assignedToMe = new HashMap<>();
        for (EventTicket item : items) {            
            assignedToMe.put(item.getEventId(), Boolean.FALSE);
        }
        
        for (EventTicket item : items) {
            if (item.getMemberId() != null) {
                assignedToMe.put(item.getEventId(), Boolean.TRUE);
            }
        }
        
        

        PageWrapper<EventTicket> pageWrapper = new PageWrapper<>(items, "members/events/invites");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("mt", assignedToMe);
        model.addAttribute("m", m);

        String acode = mediaService.getAvatarCode(m);
        model.addAttribute("userAvatar", "/open/media/" + acode);

        return "members/events/invites";
    }

    @RequestMapping(value = {"/members/events/choose/{id}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> chooseForMe(Model model, HttpServletRequest request, @PathVariable("id") Long ticketId) {

        try {
            Member m = AuthUtils.getMemberUser();
            EventTicket myTicket = eventsService.getEventTicketById(ticketId);
            myTicket.setStatus(EventTicketStatus.reclaimed.name());

            myTicket.setFirstName(m.getFirstName());
            myTicket.setLastName(m.getLastName());
            myTicket.setEmail(m.getEmail());
            myTicket.setAddress(m.getStreet());
            myTicket.setCity(m.getCity());
            myTicket.setState(m.getState());
            myTicket.setCountry(m.getCountry());
            myTicket.setZip(m.getPostal());
            myTicket.setMemberId(m.getId());

            String ip = AuthUtils.getIp(request);
            myTicket.setStatus(EventTicketStatus.reclaimed.name());
            myTicket.setReclaimDate(new Date());
            myTicket.setStatusAudit(DateUtils.formatNormalDateTime(new Date())
                    + " "
                    + myTicket.getStatus()
                    + " - "
                    + ip
                    + System.lineSeparator()
                    + ((myTicket.getStatusAudit() != null) ? myTicket.getStatusAudit() : ""));

            eventsService.saveEventTicket(myTicket);
            return ResponseEntity.ok("Ticket asignado correctamente");

        } catch (Exception e) {
            log.error("Error asignando ticket al afiliado", e);
            return ResponseEntity.badRequest().body("Ocurrió un error asignado el ticket");
        }
    }

}
