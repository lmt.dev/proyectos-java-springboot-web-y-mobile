/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.shippings.peru;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.backoffice.mlm.domain.City;
import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import com.wings.backoffice.mlm.domain.bis.orders.ShipmentItem;
import com.wings.backoffice.mlm.repos.CitiesRepository;
import com.wings.backoffice.services.EnvironmentService;
import com.wings.backoffice.services.InvoiceService;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class PeruShippingService {

    @Autowired
    private EnvironmentService environmentService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private CitiesRepository citiesRepository;

    private final Logger log = LoggerFactory.getLogger(PeruShippingService.class);

    private final String url = "http://138.59.64.89:8888/api/pedidocliente";
    private final String testToken = "mMpsg04ineaI+yxifuzoaqcUoKvF+Py8rrwBSc70pHFSAs75XkPO+69eEtmJ0FW5Cs+ZuNd+8Z8LLTwBeT5uZg==";
    private final String token = "+jhZkUwYxdjFCA2R1jbEe1yxj8xgAOpXI61ObPAVJUr0Z5dkbyJN/q9/abG0KeiR5aQFVhM6zHh2iSnA8oJ3Rw==";

    private final String invoiceUrl = "https://beta.wingsmobile.net/open/invoices/pdf/";

    public PeruResponseWrapper sendToCurier(Shipment shipment) {

        String apiToken = token;
        if (environmentService.isDevEnvironment()) {
            apiToken = testToken;
        }

        PeruResponseWrapper ret = new PeruResponseWrapper();
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        try {
            PeruShipmentWrapper wrapper = transform(shipment);
            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
            converter.setObjectMapper(new ObjectMapper());
            template.getMessageConverters().add(converter);

            List<PeruShipmentWrapper> list = new LinkedList<>();
            list.add(wrapper);

            RequestEntity<List<PeruShipmentWrapper>> entity = RequestEntity
                    .post(new URI(url))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .header("API-TOKEN", apiToken)
                    .body(list);

            ResponseEntity<PeruResponseWrapper> response = template.exchange(entity, PeruResponseWrapper.class);
            ret = response.getBody();
        } catch (Exception e) {
            ret.setStatus("error");
            ret.setMessage(e.getMessage());
        }
        return ret;
    }

    private PeruShipmentWrapper transform(Shipment shipment) throws Exception {

        List<City> cities = citiesRepository.findByCityAndCountry(shipment.getShippingCity(), "PE");
        City city;
        if (cities.isEmpty()) {
            log.error("Ciudad simple no encontrada para el envio " + shipment.getId() + " " + shipment.getShippingCity());
            throw new Exception("City not found for " + shipment.getShippingCity());
        }

        if (cities.size() > 1) {
            log.error("Ciudad simple no encontrada para el envio " + shipment.getId() + " " + shipment.getShippingCity());
            throw new Exception("Multiple cities found for " + shipment.getShippingCity());
            //TODO: mejorar la busqueda y poner aproximaciones
            //String q = "%" + shipment.getShippingCity() + "%";
            //List<City> cities = citiesRepository.findByCountryAndCityLike("EC", q);
        } else {
            //id de servientrega para ecuador
            city = cities.get(0);
        }

        PeruShipmentWrapper wrapper = new PeruShipmentWrapper();

        wrapper.setDistrito(city.getCity());
        wrapper.setProvincia(city.getCustom1());
        wrapper.setDepartamento(city.getCustom2());
        //TODO: agregar DNI del afiliado o cliente
        wrapper.setIdentificador(null);

        wrapper.setBultos(shipment.getItems().size());
        wrapper.setDireccion(shipment.getShippingAddress());
        wrapper.setNombre(shipment.getShippingName());
        wrapper.setNroPedido(shipment.getShipmentNumber());
        wrapper.setTelefono(shipment.getPhoneNumber());

        Invoice i = invoiceService.getInvoiceForOrder(shipment.getOrder());
        List<ShipmentItem> items = shipment.getItems();
        List<PeruShipmentWrapperItem> shitems = new LinkedList<>();
        items.forEach((item) -> {
            PeruShipmentWrapperItem si = new PeruShipmentWrapperItem();
            si.setReferencia1(item.getItemEan());
            si.setReferencia2(item.getItemName());
            si.setReferencia3(item.getShipmentId().toString());
            si.setReferencia4(invoiceUrl + i.getToken());
            shitems.add(si);
        });
        wrapper.setItems(shitems);

        return wrapper;
    }

    
    public void checkTracking(String orderId){
        
    
    }
}
