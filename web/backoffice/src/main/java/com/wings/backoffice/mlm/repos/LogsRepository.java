/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.Log;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface LogsRepository extends JpaRepository<Log, Long>, JpaSpecificationExecutor<Log> {

    List<Log> findTop5ByTypeAndReferenceIdOrderByCreationDateDesc(String commnet, Long referenceId);

    List<Log> findTop6ByTypeOrderByCreationDateDesc(String commnet);

    List<Log> findByCountryIsNull();

    Page<Log> findByTypeAndComment(String type, String comment, Pageable page);
    
    Page<Log> findByComment(String comment, Pageable page);
    
    Page<Log> findByType(String type, Pageable page);
    

}
