/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.utils.AuthUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author seba
 */
@Controller
public class MemberLoginController {

    @Autowired
    private MembersRepository membersRepository;

    @RequestMapping(value = {"/admin/member/login"}, method = RequestMethod.GET)
    public String impersonate(Model model, HttpServletRequest request, @RequestParam("username") String username) {        
        
        Member m = membersRepository.findByUsername(username);
        AuthUtils.setMember(m);
        
        return "redirect:/members";
    }

}
