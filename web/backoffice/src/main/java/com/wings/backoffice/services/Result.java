/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas
 */
public final class Result<T> {
    private T result;
    private boolean ok;
    private List<String> messages;

    public Result() {
        this.messages = new ArrayList<>();
        ok = true;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean isOk) {
        this.ok = isOk;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
    
    public void addErrorMessage(String error){
        if(error != null && !error.trim().isEmpty()){
            messages.add(error);
            ok = false;
        }        
    }
    
    public void addMessage(String error){
        if(error != null && !error.trim().isEmpty()){
            messages.add(error);
        }        
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        messages.stream().forEach(message -> sb.append(message).append("\n"));
        return sb.toString();
    }
}
