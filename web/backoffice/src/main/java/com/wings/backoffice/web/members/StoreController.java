/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.dao.BalanceDAO;
import com.wings.backoffice.mlm.dao.BalanceSums;
import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.Topup;
import com.wings.backoffice.mlm.domain.bis.Balance;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.events.EventProduct;
import com.wings.backoffice.mlm.domain.events.EventsProductPurchase;
import com.wings.backoffice.mlm.enums.OrderStatus;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.services.BalanceService;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.StoreItemsService;
import com.wings.backoffice.services.TopUpsService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.DateUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class StoreController {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private EventsService eventsService;
    @Autowired
    private StoreItemsService storeItemsService;
    @Autowired
    private BalanceDAO balanceDAO;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private TopUpsService topUpsService;

    private final Logger log = LoggerFactory.getLogger(StoreController.class);

    /**
     * Buy products
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/store/products"}, method = RequestMethod.GET)
    public String getProducts(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        List<StoreItem> sitems = storeItemsService.searchItemsInternal(null, Boolean.TRUE, OrderType.ticket.name(), null, member.getCountry());
        sitems.addAll(storeItemsService.searchItemsInternal(null, Boolean.TRUE, OrderType.product.name(), null, member.getCountry()));

        //TODO: remove products until I fix shipping
        sitems.clear();
        
        model.addAttribute("sitems", sitems);
        model.addAttribute("member", member);
        return "members/store/products";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/store/mine"}, method = RequestMethod.GET)
    public String getMine(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        List<Order> orders = ordersService.getMemberProductOrders(member);
        model.addAttribute("items", orders);

        return "members/store/mine";
    }

    /**
     *
     * @param model
     * @param request
     * @param productId
     * @return
     */
    @RequestMapping(value = {"/members/store/tickets/buy/{productId}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postBuyEvent(Model model, HttpServletRequest request, @PathVariable("productId") Long productId) {
        try {
            Member member = AuthUtils.getMemberUser();
            EventProduct product = eventsService.getProduct(productId);
            EventsProductPurchase purchase = eventsService.createProductPurchase(member, product);
            if (purchase.getId() == null || purchase.getOrderId() == null) {
                return ResponseEntity.badRequest().body("Ocurrio un error efectuando la compra!");
            }
            String ret = "/members/pay/" + purchase.getOrderId();
            return ResponseEntity.ok(ret);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().body("Ocurrio un error efectuando la compra!");
    }

    /**
     *
     * @param model
     * @param request
     * @param itemId
     * @return
     */
    @RequestMapping(value = {"/members/store/buy/{itemId}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postBuyStoreItem(Model model, HttpServletRequest request, @PathVariable("itemId") Long itemId) {
        try {
            Member member = AuthUtils.getMemberUser();
            member = membersService.getOne(member.getId());
            Order newOrder = ordersService.addProduct(member, itemId, member.getUsername());
            String ret = "/members/pay/" + newOrder.getId();
            return ResponseEntity.ok(ret);
        } catch (Exception e) {
            log.error("Error buy on member store", e);
        }
        return ResponseEntity.badRequest().body("Ocurrio un error efectuando la compra!");
    }

    /**
     * GET Topups listings and also add balance credit
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/store/topups"}, method = RequestMethod.GET)
    public String getTopUps(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());

        List<StoreItem> sitems = storeItemsService.searchItemsInternal(null, Boolean.TRUE, OrderType.topup.name(), null, "CO");

        BalanceSums total = balanceDAO.find(null, null, null, null, null, member.getId(), null);
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        total.setLocale(locale);
        model.addAttribute("total", total);

        //TODO: esto es por ahora solo colombia
        StoreItem storeItem = storeItemsService.getOneByCode("TOPUP_CO");
        model.addAttribute("storeItem", storeItem);

        model.addAttribute("sitems", sitems);
        model.addAttribute("member", member);
        return "members/store/topups";
    }

    /**
     * Esto es cundo se recarga credito del movil desde el backoffice de
     * afiliado solo recarga, no compra de paquetes
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/members/store/topups"}, method = RequestMethod.POST)
    public String postTopUps(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        //TODO: mover esto al topuUpService
        Member member = AuthUtils.getMemberUser();
        member = membersService.getOne(member.getId());
        String msisdn = request.getParameter("msisdn");
        String samount = request.getParameter("amount");
        String itemId = request.getParameter("storeItem");

        //Validar Valores        
        //Número 
        if (msisdn.length() != 10) {
            redirectAttributes.addFlashAttribute("message", "El número es inválido");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        Long number;
        try {
            number = Long.valueOf(msisdn);
        } catch (NumberFormatException e) {
            redirectAttributes.addFlashAttribute("message", "El número es inválido");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        //amount
        Double amount;
        try {
            amount = Double.valueOf(samount);
            //remove negative value
            amount = Math.abs(amount);
        } catch (NumberFormatException e) {
            redirectAttributes.addFlashAttribute("message", "El monto es inválido");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        //minimun amount
        if (amount < 1000) {
            redirectAttributes.addFlashAttribute("message", "El monto minimo es 1000 Pesos");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        //maximun amount
        if (amount > 100000) {//TODO: solo para colombia??
            redirectAttributes.addFlashAttribute("message", "El monto máximo es 200.000 Pesos");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        //saldo
        BalanceSums total = balanceDAO.find(null, null, null, null, null, member.getId(), null);
        Locale locale = CountryUtils.getLocaleForCountry(member.getCountry());
        Double realBalance = NumberUtils.convertMoneyFromEuros(total.getEffective(), locale);
        if (realBalance < amount) {
            redirectAttributes.addFlashAttribute("message", "El monto es superior a su balance");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        Long storeItemId = Long.valueOf(itemId);

        //creo la orden
        Order o = ordersService.addTopUp(member, storeItemId, member.getUsername(), amount, number, new Date());
        //creo el balance
        Balance b = balanceService.addBalance(o);
        //crear topup
        Topup topup = new Topup();
        topup.setAmount(amount);
        topup.setCreationDate(new Date());
        topup.setCurrency("COP");
        topup.setModIp(AuthUtils.getIp(request));
        topup.setModUser(member.getUsername());
        topup.setModStatus("created from member page");
        topup.setMsisdn(Long.parseLong(msisdn));
        topup.setProvider("suma");
        topup.setStatus("created");

        boolean ok = topUpsService.addTopUp(topup);
        if (!ok) {
            //ocurrio un error recargando, borro la orden y el balance
            ordersService.deleteOrder(o);
            balanceService.removeBalance(b);

            redirectAttributes.addFlashAttribute("message", "Ocurrió un error agregando saldo");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/members#members/store/topups";
        }

        return "redirect:/members#members/store/mine";
    }

    /**
     *
     * @param model
     * @param request
     * @param productId
     * @return
     */
    @RequestMapping(value = {"/members/store/packs/buy/{productId}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postBuyTopup(Model model, HttpServletRequest request, @PathVariable("productId") Long productId) {
        try {
            Member member = AuthUtils.getMemberUser();
            member = membersService.getOne(member.getId());

            Order newOrder = ordersService.addTopUp(member, productId, member.getUsername(), null, null, null);
            String ret = "/members/pay/" + newOrder.getId();

            return ResponseEntity.ok(ret);

        } catch (Exception e) {
            log.error("Error on packs buy", e);
        }
        return ResponseEntity.badRequest().body("Ocurrio un error efectuando la compra!");
    }

    /**
     *
     * @param model
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/members/store/packs/redeem/{orderId}"}, method = RequestMethod.GET)
    public String getPackRedeem(Model model, HttpServletRequest request, @PathVariable("orderId") Long orderId) {

        try {
            Member member = AuthUtils.getMemberUser();
            member = membersService.getOne(member.getId());

            String message = null;
            Order o = ordersService.getOrder(orderId);
            if (o == null
                    || !o.getMemberId().equals(member.getId())
                    || !o.getStatus().equals("paid")
                    || !o.getType().equals(OrderType.topup.name())) {

                message = "No se encontró la orden";
            }

            model.addAttribute("order", o);
            model.addAttribute("message", message);

        } catch (Exception e) {
            log.error("Error on redeem pack", e);
            model.addAttribute("message", "Ocurrio un error");
        }

        return "members/store/redeem";
    }

    /**
     *
     * @param request
     * @param orderId
     * @return
     */
    @RequestMapping(value = {"/members/store/packs/redeem/{orderId}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postPackRedeem(HttpServletRequest request, @PathVariable("orderId") Long orderId) {
        try {
            Member member = AuthUtils.getMemberUser();
            member = membersService.getOne(member.getId());
            Order o = ordersService.getOrder(orderId);

            if (o == null
                    || !o.getMemberId().equals(member.getId())
                    || !o.getStatus().equals("paid")
                    || !o.getType().equals(OrderType.topup.name())) {

                return ResponseEntity.badRequest().body("No se encontró la orden");
            }

            String number = request.getParameter("msisdn");
            if (number.length() != 10) {
                return ResponseEntity.badRequest().body("El número es inválido");
            }

            if (topUpsService.redeemPackage(number, o)) {

                o.setTypeStatus(OrderStatus.complete.name());
                o.setComment("Pkg applied " + DateUtils.formatNormalDateTime(new Date()) + " to " + number + " " + o.getOrderItem().getName() + " " + o.getComment());
                ordersService.save(o);

                return ResponseEntity.ok("success");
            } else {
                return ResponseEntity.badRequest().body("Error aplicando el paquete!");
            }

        } catch (Exception e) {
            log.error("Error aplicando el paquete", e);
            return ResponseEntity.badRequest().body("Error aplicando el paquete!");
        }

    }

    /**
     *
     * @param orderId
     * @return
     */
    @RequestMapping(value = "/members/order/delete/{orderId}", method = {RequestMethod.POST})
    public String deleteOrder(@PathVariable("orderId") Long orderId) {
        if (orderId != null) {
            Order order = ordersService.getOrder(orderId);
            ordersService.deleteOrder(order);
        }
        return "redirect:/members#members/store/mine";
    }
}
