/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.messages.SupportMessage;
import com.wings.backoffice.services.messages.SupportService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.InvoiceUtils;
import com.wings.backoffice.utils.PageWrapper;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class MembersSupportController {

    @Autowired
    private SupportService supportService;

    private final Map<String, String> mimeTypes = new HashMap<String, String>() {
        {
            put("image/png", ".png");
            put("image/jpeg", ".jpg");
            put("application/pdf", ".pdf");
            put("text/plain", ".txt");
            put("video/mp4", ".mp4");
        }
    };

    /**
     *
     * @param model
     * @param request
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.getIncidencesList(Model, HttpServletRequest))}
     * @see com.wings.backoffice.web.commons.SupportController.getIncidencesList(Model, HttpServletRequest)
     */
    @Deprecated
    @RequestMapping(value = {"members/support/incidences"}, method = {RequestMethod.GET})
    public String getIncidencesList(Model model, HttpServletRequest request) {
        Member m = AuthUtils.getMemberUser();
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");

        PageWrapper<SupportMessage> pageWrapper = supportService.search(m, "", spage, ssize, "", "", "");
        model.addAttribute("page", pageWrapper);

        return "members/support/incidences";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.getNewIncidente()}
     * @see com.wings.backoffice.web.commons.SupportController.getNewIncidente()
     */
    @RequestMapping(value = {"members/support/incidences/create"}, method = {RequestMethod.GET})
    public String getNewIncidences(Model model, HttpServletRequest request) {
        return "members/support/incidences-create";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @param attachment
     * @return
     * @deprecated Replaced by {@link #com.wings.backoffice.web.commons.SupportController.postNewIncidences()}
     * @see com.wings.backoffice.web.commons.SupportController.postNewIncidences()
     */
    @RequestMapping(value = {"members/support/incidences/create"}, method = {RequestMethod.POST})
    public String postNewIncidences(Model model, HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "attachment", required = false) MultipartFile attachment
    ) {

        Member member = AuthUtils.getMemberUser();

        String title = request.getParameter("title");
        String description = request.getParameter("description");

        boolean error = false;
        if (title == null || title.trim().isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "El título no puede estar vacio");
            redirectAttributes.addFlashAttribute("messageType", "error");
            error = true;
        }

        if (description == null || description.trim().isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "La descripcón no puede estar vacia");
            redirectAttributes.addFlashAttribute("messageType", "error");
            error = true;
        }

        String attachmentFile = null;
        if (attachment != null && !attachment.isEmpty()) {
            try {
                if (mimeTypes.containsKey(attachment.getContentType())) {
                    String prefix = InvoiceUtils.getRandomString(12);
                    String suffix = mimeTypes.get(attachment.getContentType());
                    String fileName = "/var/wings/bo_media/attachments/" + prefix + suffix;
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName));
                    stream.write(attachment.getBytes());
                    attachmentFile = prefix + suffix;
                } else {
                    redirectAttributes.addFlashAttribute("message", "Formato de archivo no soportado");
                    redirectAttributes.addFlashAttribute("messageType", "error");
                    error = true;
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("message", "Error subiendo el adjunto!");
                redirectAttributes.addFlashAttribute("messageType", "error");
                error = true;
            }
        }

        if (!error) {
            SupportMessage m = new SupportMessage();
            m.setAssignedTo("support");
            m.setCreationDate(new Date());
            m.setStatus("open");
            m.setTitle(title.trim());
            m.setDescription(description.trim());

            m.setMemberId(member.getId());
            m.setCountry(member.getCountry());
            m.setLastName(member.getLastName());
            m.setFirstName(member.getFirstName());
            m.setEmail(member.getEmail());
            m.setUsername(member.getUsername());
            m.setAttachment(attachmentFile);

            supportService.save(m);

            redirectAttributes.addFlashAttribute("message", "Incidencia creada con éxito");
            redirectAttributes.addFlashAttribute("messageType", "success");

        }

        return "redirect:/members#members/support/incidences";
    }

}
