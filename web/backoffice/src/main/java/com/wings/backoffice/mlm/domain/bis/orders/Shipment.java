/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain.bis.orders;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "bis_shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String shipmentNumber;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendedDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryDate;
    private String description;
    private Long orderId;
    private String status;
    private String statusText;
    private String shippingName;
    private String shippingAddress;
    private String shippingCity;
    private String shippingState;
    private String shippingPostalCode;
    private String shippingCountry;
    private String trackingNumber;
    private String lastTrackingActivity;
    private String phoneNumber;
    private String email;
    private String username;
    private boolean confirmed;
    private boolean deliverable;
    private boolean deleted;
    private boolean emailSent;

    private String xmlPath;
    private String pdfPath;
    private String custom1;
    private String custom2;
    private String custom3;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "shipmentId")
    private List<ShipmentItem> items;

    @OneToOne
    @JoinColumn(name = "orderId", updatable = false, insertable = false)
    private Order order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getSendedDate() {
        return sendedDate;
    }

    public void setSendedDate(Date sendedDate) {
        this.sendedDate = sendedDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingPostalCode() {
        return shippingPostalCode;
    }

    public void setShippingPostalCode(String shippingPostalCode) {
        this.shippingPostalCode = shippingPostalCode;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getLastTrackingActivity() {
        return lastTrackingActivity;
    }

    public void setLastTrackingActivity(String lastTrackingActivity) {
        this.lastTrackingActivity = lastTrackingActivity;
    }

    public List<ShipmentItem> getItems() {
        return items;
    }

    public void setItems(List<ShipmentItem> items) {
        this.items = items;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath;
    }

    public String getPdfPath() {
        return pdfPath;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public boolean isDeliverable() {
        return deliverable;
    }

    public void setDeliverable(boolean deliverable) {
        this.deliverable = deliverable;
    }

    public String getAddressTextShort() {
        String ret = shippingAddress + "," + shippingCity + "," + shippingState + "," + shippingPostalCode + "," + shippingCountry;
        if (ret.length() > 30) {
            ret = ret.substring(0, 30) + "...";
        }
        return ret;
    }

    public String getAddressText() {
        return shippingAddress + "," + shippingCity + "," + shippingState + "," + shippingPostalCode + "," + shippingCountry;
    }

    public String getItemsText() {
        String ret = "";
        ret = items.stream().map((item) -> item.getItemDescription() + System.lineSeparator()).reduce(ret, String::concat);
        return ret;
    }

    public String getItemsDetails() {
        String ret = "";
        Map<String, Integer> maps = new HashMap<>();
        items.stream().map((item) -> item.getItemDescription()).forEachOrdered((key) -> {
            if (maps.containsKey(key)) {
                maps.put(key, maps.get(key) + 1);
            } else {
                maps.put(key, 1);
            }
        });

        for (Map.Entry<String, Integer> entry : maps.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            ret += value + " " + key + " - ";
        }

        return ret;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom3() {
        return custom3;
    }

    public boolean isEmailSent() {
        return emailSent;
    }

    public void setEmailSent(boolean emailSent) {
        this.emailSent = emailSent;
    }

}
