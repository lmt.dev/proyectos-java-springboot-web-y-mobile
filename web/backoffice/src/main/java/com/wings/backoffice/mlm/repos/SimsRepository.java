/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.Sim;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author lucas
 */
public interface SimsRepository extends JpaRepository<Sim, Long> {

    Sim findByIccid(String iccid);

    List<Sim> findByIccidIn(List<String> iccids);

    List<Sim> findByImsi(String imsi);

    List<Sim> findByCountry(String country);

    List<Sim> findByOperator(String operator);

    List<Sim> findByActive(boolean active);

    List<Sim> findByEnabled(boolean enabled);
}
