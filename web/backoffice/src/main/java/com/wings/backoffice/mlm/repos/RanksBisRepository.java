/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.RankBis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface RanksBisRepository extends JpaRepository<RankBis, Long>, JpaSpecificationExecutor<RankBis> {

    
    RankBis findByDirectRequired(Long id);
    
    RankBis findByName(String name);
    
}
