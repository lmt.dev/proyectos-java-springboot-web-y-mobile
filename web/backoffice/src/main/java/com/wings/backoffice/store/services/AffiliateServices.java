/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.store.services;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.store.domain.Affiliate;
import com.wings.backoffice.store.domain.User;
import com.wings.backoffice.store.domain.UserMeta;
import com.wings.backoffice.store.repos.AffiliatesRepository;
import com.wings.backoffice.store.repos.PostsRepository;
import com.wings.backoffice.store.repos.UsersMetaRepository;
import com.wings.backoffice.store.repos.UsersRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class AffiliateServices {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private PostsRepository postsRepository;

    @Autowired
    private AffiliatesRepository affiliatesRepository;

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UsersMetaRepository usersMetaRepository;
    
    private final Logger log = LoggerFactory.getLogger(AffiliateServices.class);

    public String test() {
        List<Member> members = membersRepository.findAll();
        pushAffiliates(members);
        return "ok";
    }

    private void pushAffiliates(List<Member> members) {

        List<User> toSave = new ArrayList<>();

        try {

            for (Member member : members) {

                User already = usersRepository.findByUserEmail(member.getEmail());
                if (already != null) {
                    continue;
                }

                User user = new User();                
                user.setDisplayName(member.toString());
                user.setUserEmail(member.getEmail());
                user.setUserLogin(member.getUsername());
                user.setUserNicename(member.getUsername());
                user.setUserPass(member.getPassword());
                user.setUserRegistered(member.getRegistrationDate());
                user.setUserStatus(0);

                user.setUserActivationKey("");
                user.setUserUrl("");

                usersRepository.save(user);
                toSave.add(user);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Member member : members) {
            User already = usersRepository.findByUserEmail(member.getEmail());            
            already.setMetas(buildMetas(already, member));
            usersMetaRepository.save(already.getMetas());
        }

        for (User user : toSave) {            
            Affiliate a = new Affiliate();
            
            try {
                Long id = Long.parseLong(user.getUserLogin());
                a.setAffiliateId(id);
            } catch (Exception e) {
                log.error("Atencion no se puede crear el afiliado con ese username");
                continue;
            }

            a.setDateRegistered(user.getUserRegistered());
            a.setEarnings("");
            a.setPaymentEmail(user.getUserEmail());
            a.setRate("");
            a.setRateType("");
            a.setReferrals(0);
            a.setUserId(user.getId());
            a.setStatus("active");
            a.setUnpaidEarnings("");
            a.setUserId(user.getId());
            a.setVisits(0);
            
            affiliatesRepository.save(a);
        }

    }

    private List<UserMeta> buildMetas(User user, Member member) {
        List<UserMeta> metas = new ArrayList<>();

        metas.add(new UserMeta(user.getId(),"nickname", member.getUsername()));
        metas.add(new UserMeta(user.getId(),"first_name", member.getFirstName()));
        metas.add(new UserMeta(user.getId(),"last_name", member.getLastName()));
        metas.add(new UserMeta(user.getId(),"wingstore_capabilities", "a:1:{s:10:\"subscriber\";b:1;}"));
        metas.add(new UserMeta(user.getId(),"wingstore_user_level", "0"));
        metas.add(new UserMeta(user.getId(),"billing_first_name", member.getFirstName()));
        metas.add(new UserMeta(user.getId(),"billing_last_name", member.getLastName()));
        metas.add(new UserMeta(user.getId(),"billing_company", ""));
        metas.add(new UserMeta(user.getId(),"billing_address_1", member.getStreet()));
        metas.add(new UserMeta(user.getId(),"billing_address_2", ""));
        metas.add(new UserMeta(user.getId(),"billing_city", member.getCity()));
        metas.add(new UserMeta(user.getId(),"billing_postcode", member.getPostal()));
        metas.add(new UserMeta(user.getId(),"billing_country", member.getCountry()));
        metas.add(new UserMeta(user.getId(),"billing_state", member.getState()));
        metas.add(new UserMeta(user.getId(),"billing_phone", member.getPhone()));
        metas.add(new UserMeta(user.getId(),"billing_email", member.getEmail()));
        metas.add(new UserMeta(user.getId(),"shipping_first_name", member.getFirstName()));
        metas.add(new UserMeta(user.getId(),"shipping_last_name", member.getLastName()));
        metas.add(new UserMeta(user.getId(),"shipping_company", ""));
        metas.add(new UserMeta(user.getId(),"shipping_address_1", member.getStreet()));
        metas.add(new UserMeta(user.getId(),"shipping_address_2", ""));
        metas.add(new UserMeta(user.getId(),"shipping_city", member.getCity()));
        metas.add(new UserMeta(user.getId(),"shipping_postcode", member.getPostal()));
        metas.add(new UserMeta(user.getId(),"shipping_country", member.getCountry()));
        metas.add(new UserMeta(user.getId(),"shipping_state", member.getState()));

        return metas;
    }

}
