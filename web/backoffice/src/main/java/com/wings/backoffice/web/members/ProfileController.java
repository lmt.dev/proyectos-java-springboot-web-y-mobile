/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.MemberBankAccount;
import com.wings.backoffice.mlm.domain.MemberMedia;
import com.wings.backoffice.mlm.repos.CountriesRepository;
import com.wings.backoffice.mlm.repos.MembersBankAccountRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import com.wings.backoffice.mlm.repos.RanksBisRepository;
import com.wings.backoffice.services.ContractService;
import com.wings.backoffice.services.LogsService;
import com.wings.backoffice.services.MediaService;
import com.wings.backoffice.utils.AuthUtils;
import com.wings.backoffice.utils.MapUtils;
import com.wings.backoffice.utils.MemberUtils;
import com.wings.backoffice.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class ProfileController {

    @Autowired
    private MediaService mediaService;

    @Autowired
    private CountriesRepository countriesRepository;

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private LogsService logsService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private RanksBisRepository ranksBisRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private MembersBankAccountRepository membersBankAccountRepository;

    private final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "image/gif");

    private final List<String> dnis = Arrays.asList("DNI", "NIF", "PAS", "TAR", "NIE", "C.C.", "C.I.");
    private final List<String> idts = Arrays.asList("CIF", "CPF", "CUIT", "NIE", "NIF", "NIT", "RUT", "RUC", "RFC", "RIF");

    /**
     * Main Method for member profile display
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/profile"}, method = RequestMethod.GET)
    public String getProfile(Model model, HttpServletRequest request) {
        Member member = membersRepository.findOne(AuthUtils.getMemberUser().getId());
        AuthUtils.setMember(member);

        boolean profileCompleted = MemberUtils.isProfileComplete(member);
        List<MemberMedia> mds = mediaService.getProfileMedias(member);
        Locale locale = LocaleContextHolder.getLocale();
        String acode = mediaService.getAvatarCode(member);
        model.addAttribute("avatar", "/open/media/" + acode);
        model.addAttribute("member", member);
        model.addAttribute("lang", locale.getLanguage());
        model.addAttribute("countries", countriesRepository.findAll());
        model.addAttribute("dnis", dnis);
        model.addAttribute("idts", idts);
        model.addAttribute("profileComplete", profileCompleted);
        model.addAttribute("ranks", ranksBisRepository.findAll());
        model.addAttribute("contractSigned", member.getContractSigned());

        //get user comprobantes
        MemberMedia dni = null;
        MemberMedia idt = null;
        MemberMedia address = null;
        for (MemberMedia media : mds) {
            if (media.getType().equals(com.wings.backoffice.mlm.enums.MediaType.address.name())) {
                address = media;
            } else if (media.getType().equals(com.wings.backoffice.mlm.enums.MediaType.dni.name())) {
                dni = media;
            } else if (media.getType().equals(com.wings.backoffice.mlm.enums.MediaType.idt.name())) {
                idt = media;
            }
        }
        model.addAttribute("dni", dni);
        model.addAttribute("idt", idt);
        model.addAttribute("address", address);

        return "members/profile/profile";
    }

    /**
     * Edit profile method
     *
     * @param redirectAttributes
     * @param request
     * @param id
     * @param member
     * @return
     */
    @RequestMapping(value = {"/members/profile/{id}"}, method = RequestMethod.POST)
    public String postProfile(
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @PathVariable("id") Long id,
            @ModelAttribute("member") Member member
    ) {
        Member existing = membersRepository.findOne(AuthUtils.getMemberUser().getId());
        String antes = new ReflectionToStringBuilder(existing).toString();

        if (member == null || existing == null) {
            redirectAttributes.addFlashAttribute("messageType", "error");
            redirectAttributes.addFlashAttribute("message", messageSource.getMessage("update.member.notfound", null, localeResolver.resolveLocale(request)));
            return "redirect:/members#members/profile";
        }

        existing.setFirstName(StringUtils.capitalize(member.getFirstName()));
        existing.setLastName(StringUtils.capitalize(member.getLastName()));
        existing.setCompanyName(StringUtils.capitalize(member.getCompanyName()));
        existing.setDniType(member.getDniType() != null && !member.getDniType().equals("0") ? member.getDniType() : null);
        existing.setDni(StringUtils.trimAll(member.getDni()));
        existing.setPhone(member.getPhone());

        if (member.getStreet() != null) {
            existing.setStreet(StringUtils.capitalize(member.getStreet()));
        }

        existing.setCity(StringUtils.capitalize(member.getCity()));
        existing.setState(StringUtils.capitalize(member.getState()));
        existing.setCountry(member.getCountry());
        existing.setPostal(StringUtils.capitalize(member.getPostal()));
        existing.setIsCompany(member.getIsCompany());
        existing.setCompanyDniType(member.getCompanyDniType() != null && !member.getCompanyDniType().equals("0") ? member.getCompanyDniType() : null);
        existing.setCompanyDni(StringUtils.trimAll(member.getCompanyDni()));
        //TODO: revisar esto cuando se hacepost de imagenes

        if (member.getBirthDate() != null) {
            existing.setBirthDate(member.getBirthDate());
        }

        existing.setIdt(member.getIdt());
        existing.setIdtType(member.getIdtType());

        String meesageAdd = existing.getContractSigned() ? messageSource.getMessage("update.sign", null, localeResolver.resolveLocale(request)) : "";
        existing.setContractSigned(false);
        existing.setContractSignedDate(null);

        String despues = new ReflectionToStringBuilder(existing).toString();
        String diff = MapUtils.getDiff(antes, despues);

        String message = "update.success";
        boolean error = false;
        if (diff.isEmpty()) {
            message = "update.nochanges";
        }

        //save member 
        membersRepository.save(existing);

        //save log
        String ip = AuthUtils.getIp(request);
        String modUser = AuthUtils.getAdminUser() != null ? AuthUtils.getAdminUser().getUsername() : AuthUtils.getMemberUser().getUsername();
        logsService.addProfileModificationLog(diff, ip, modUser, existing);

        redirectAttributes.addFlashAttribute("message", messageSource.getMessage(message, null, localeResolver.resolveLocale(request)) + System.lineSeparator() + meesageAdd);
        redirectAttributes.addFlashAttribute("messageType", error ? "error" : "success");

        AuthUtils.setMember(existing);

        contractService.generateContract(existing);

        boolean profileCompleted = MemberUtils.isProfileComplete(existing);

        //verificar que el get de cada cual tome los model
        if (!profileCompleted) {
            return "redirect:/required";
        } else {
            return "redirect:/members#members/profile";
        }
    }

    /**
     * upload avater method
     *
     * @param request
     * @param file
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = {"/members/profile/avatar/upload"}, method = RequestMethod.POST)
    public String uploadAvatar(HttpServletRequest request, @RequestParam("avatar") MultipartFile file) throws URISyntaxException {

        Member m = AuthUtils.getMemberUser();
        if (m == null) {
            return "redirect:/members#members/profile";
        }

        String fileContentType = file.getContentType();
        if (!contentTypes.contains(fileContentType)) {
            return "redirect:/members#members/profile";
        }

        String code = mediaService.storeUserAvatar(file, m);
        return "redirect:/members#members/profile";
    }

    /**
     * change password method
     *
     * @param redirectAttributes
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/profile/changepw"}, method = RequestMethod.POST)
    public String changePasword(RedirectAttributes redirectAttributes, HttpServletRequest request) {

        String message = messageSource.getMessage("reset.success", null, localeResolver.resolveLocale(request));

        String current = request.getParameter("currentPassword");
        String new1 = request.getParameter("newPassword1");
        String new2 = request.getParameter("newPassword2");

        if (current == null || current.trim().isEmpty()) {
            message = messageSource.getMessage("password.invalid", null, localeResolver.resolveLocale(request));
            redirectAttributes.addFlashAttribute("messageType", "error");
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/members#members/profile";
        }

        if (!new1.equals(new2)) {
            message = messageSource.getMessage("password.dontmatch", null, localeResolver.resolveLocale(request));
            redirectAttributes.addFlashAttribute("messageType", "error");
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/members#members/profile";
        }

        Member m = AuthUtils.getMemberUser();
        String currentDigested = DigestUtils.sha256Hex(current);
        if (!m.getPassword().equals(currentDigested)) {
            message = messageSource.getMessage("password.invalid", null, localeResolver.resolveLocale(request));
            redirectAttributes.addFlashAttribute("messageType", "error");
            redirectAttributes.addFlashAttribute("message", message);
            return "redirect:/members#members/profile";
        }

        m.setPassword(DigestUtils.sha256Hex(new1));
        membersRepository.save(m);

        //save log
        String ip = AuthUtils.getIp(request);
        String modUser = AuthUtils.getAdminUser() != null ? AuthUtils.getAdminUser().getUsername() : AuthUtils.getMemberUser().getUsername();
        logsService.addPasswordModificationLog(ip, modUser, m);

        redirectAttributes.addFlashAttribute("messageType", "success");
        redirectAttributes.addFlashAttribute("message", message);
        return "redirect:/members#members/profile";
    }

    /**
     * get contract method
     *
     * @param model
     * @param request
     * @param response
     */
    @RequestMapping(value = {"/members/profile/contract"}, method = RequestMethod.GET)
    public void getContractPdf(Model model, HttpServletRequest request, HttpServletResponse response) {

        try {
            Member member = AuthUtils.getMemberUser();
            //reload
            member = membersRepository.findOne(member.getId());
            File pdfFile = contractService.getMemberContract(member);

            FileInputStream fis = new FileInputStream(pdfFile);

            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            response.setContentLengthLong(pdfFile.length());
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Content-Disposition", "filename=afiliation_contract.pdf");

            BufferedInputStream inStream = new BufferedInputStream(fis);
            BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.flush();
            inStream.close();

        } catch (Exception e) {
        }
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/profile/contract/sign"}, method = RequestMethod.GET)
    public String getSignaturePage(Model model, HttpServletRequest request) {
        return "members/profile/sign";
    }

    /**
     *
     * @param redirectAttributes
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/profile/callback"}, method = RequestMethod.GET)
    public String callbackSignature(RedirectAttributes redirectAttributes, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        if (m != null) {
            String status = request.getParameter("status");
            String result = request.getParameter("result");

            if (status != null && status.equals("signed") && result != null && result.equals("success")) {

                m = membersRepository.findOne(m.getId());
                m.setContractSigned(true);
                m.setContractSignedDate(new Date());
                membersRepository.save(m);

                //reload
                AuthUtils.setMember(m);

                redirectAttributes.addFlashAttribute("message", "Contrato firmado correctamente");
                redirectAttributes.addFlashAttribute("messageType", "success");
            } else {
                redirectAttributes.addFlashAttribute("message", "El contrato no se firmo correctamente");
                redirectAttributes.addFlashAttribute("messageType", "error");
            }

            String ip = AuthUtils.getIp(request);
            String modUser = AuthUtils.getAdminUser() != null ? AuthUtils.getAdminUser().getUsername() : AuthUtils.getMemberUser().getUsername();
            logsService.addContractSignedLog("status " + status, ip, modUser, m);
        }

        return "redirect:/members#members/profile";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/members/profile/contract/send"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendContractToPortal(Model model, HttpServletRequest request) {

        Member member = AuthUtils.getMemberUser();
        member = membersRepository.findOne(member.getId());

        //OLD code with porta sigma
        /*File pdfFile = contractService.getMemberContract(member);
        contractService.deleteDocument(member);
        String ret = contractService.postDocument(member, pdfFile);*/
        //new code, we sign the contract
        member.setContractSigned(true);
        member.setContractSignedDate(new Date());
        membersRepository.save(member);

        //reload
        AuthUtils.setMember(member);

        String ret = "/admin#members/profile";

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param type
     * @return
     */
    @RequestMapping(value = {"/members/profile/media/add/{type}"}, method = RequestMethod.GET)
    public String getAddMediaPage(Model model, HttpServletRequest request, @PathVariable("type") String type) {

        Member member = AuthUtils.getMemberUser();
        model.addAttribute("type", type);
        model.addAttribute("member", member);

        return "members/profile/add-media";
    }

    /**
     *
     * @param model
     * @param request
     * @param type
     * @param bacsId
     * @return
     */
    @RequestMapping(value = {"/members/profile/media/add/{type}/{bacsId}"}, method = RequestMethod.GET)
    public String getAddMediaPage(Model model, HttpServletRequest request, @PathVariable("type") String type, @PathVariable("bacsId") Long bacsId) {

        Member member = AuthUtils.getMemberUser();
        model.addAttribute("type", type);
        model.addAttribute("member", member);
        model.addAttribute("bacsId", bacsId);

        return "members/profile/add-media";
    }

    /**
     *
     * @param request
     * @param type
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = {"/members/profile/media/add/{type}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postAddMedia(HttpServletRequest request, @PathVariable("type") String type) throws URISyntaxException {

        Member m = AuthUtils.getMemberUser();
        if (m == null) {
            return ResponseEntity.badRequest().body("Unauthorized");
        }

        //reload
        m = membersRepository.findOne(m.getId());

        String dirPath = "/var/wings/bo_media/member_image/" + AuthUtils.getMemberUser().getId() + "/";
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (request.getParameter("imgBase64") == null) {
            return ResponseEntity.badRequest().body("Image invalid");
        }

        long bacsId = 0l;
        if (request.getParameter("bacsId") != null && !request.getParameter("bacsId").isEmpty()) {
            bacsId = Long.parseLong(request.getParameter("bacsId"));
            MemberBankAccount bacs = membersBankAccountRepository.findOne(bacsId);
            bacs.setVerified(Boolean.FALSE);
            membersBankAccountRepository.save(bacs);
        }

        String imageValue = request.getParameter("imgBase64");
        imageValue = imageValue.replace("data:image/png;base64,", "");
        byte[] imageByte = Base64.decodeBase64(imageValue);

        try {
            String toSave = dirPath + (type.equals("bacs") ? type + "_" + bacsId : type) + ".png";
            new FileOutputStream(toSave).write(imageByte);

            MemberMedia media;
            if (bacsId != 0l) {
                media = mediaService.storeMediaForBac(m, toSave, bacsId);
            } else {
                media = mediaService.storeMedia(m, toSave, type);
            }

            if (media == null) {
                return ResponseEntity.badRequest().body("Error saving image");
            }

            return ResponseEntity.ok("Imagen almacenada correctamente");

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error saving image");
        }
    }

    //TODO:
    /*@RequestMapping(value = {"/members/orders/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDeleteOrder(HttpServletRequest request, @PathVariable("id") Long orderId) {

        Member m = AuthUtils.getMemberUser();
        if (m == null) {
            return ResponseEntity.badRequest().body("No autorizado");
        }

        //reload
        m = membersRepository.findOne(m.getId());
        Order order = ordersRepository.findOne(orderId);

        if (!order.getMemberId().equals(m.getId())) {
            return ResponseEntity.badRequest().body("No autorizado");
        }

        order.setDeleted(true);
        ordersRepository.save(order);

        return ResponseEntity.ok("Kit eliminado correctamente");

    }*/
    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/required"}, method = RequestMethod.GET)
    public String getRequiredPage(Model model, HttpServletRequest request) {

        Member m = membersRepository.getOne(AuthUtils.getMemberUser().getId());
        boolean profileCompleted = MemberUtils.isProfileComplete(m);

        Locale locale = LocaleContextHolder.getLocale();
        String acode = mediaService.getAvatarCode(m);
        model.addAttribute("avatar", "/open/media/" + acode);
        model.addAttribute("member", m);
        model.addAttribute("lang", locale.getLanguage());
        model.addAttribute("countries", countriesRepository.findAll());
        model.addAttribute("dnis", dnis);
        model.addAttribute("idts", idts);
        model.addAttribute("profileComplete", profileCompleted);
        model.addAttribute("ranks", ranksBisRepository.findAll());
        model.addAttribute("contractSigned", m.getContractSigned());

        return "required";
    }
}
