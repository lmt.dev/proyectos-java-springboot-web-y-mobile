/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.Topup;
import com.wings.backoffice.mlm.domain.TopupProvider;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.hibernate.internal.util.compare.ComparableComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class SumaProviderService {

    @Autowired
    private EnvironmentService environmentService;
    @Autowired
    private SftpService sftpService;
    @Autowired
    private ConfigService configService;
    @Autowired
    private TopUpsService topUpsService;

    private final String url = "https://topup.wingsmobile.co";
    private final String token = "nyJs158s6JBdPXwEIjoWMsCTGsww9edCYjCvJAOm";
    private final Logger log = LoggerFactory.getLogger(SumaProviderService.class);
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private final SimpleDateFormat sdfPkg = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     *
     * @param topup
     * @return
     */
    public boolean addTopUp(Topup topup) {

        if (environmentService.isDevEnvironment()) {
            return true;
        }

        //Si necesito loguear uso esto
        //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());        
        //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //Si no necesito loguear uso esto
        RestTemplate template = new RestTemplate();
        try {

            Map<String, String> params = new HashMap<>();
            params.put("msisdn", topup.getMsisdn().toString());
            params.put("amount", topup.getAmount().toString());

            RequestEntity<Map<String, String>> entity = RequestEntity
                    .post(new URI(url + "/topup"))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .header("auth", token)
                    .body(params);

            ResponseEntity<String> response = template.exchange(entity, String.class);

            return response.getBody() != null && response.getBody().equals("success");

        } catch (URISyntaxException | RestClientException e) {
            log.error("Error on topup", e);
        }

        return false;
    }

    /**
     *
     * @param number
     * @param pkg
     * @param orderNumber
     * @return
     */
    public boolean addPackage(String number, String pkg, String orderNumber) {

        //Si necesito loguear uso esto
        //HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build());
        //RestTemplate template = new RestTemplate(clientHttpRequestFactory);
        //Si no necesito loguear uso esto
        RestTemplate template = new RestTemplate();
        try {

            Map<String, String> params = new HashMap<>();
            params.put("msisdn", number);
            params.put("pkg", pkg);
            params.put("orderNumber", orderNumber);

            RequestEntity<Map<String, String>> entity = RequestEntity
                    .post(new URI(url + "/addpkg"))
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .header("auth", token)
                    .body(params);

            ResponseEntity<String> response = template.exchange(entity, String.class);

            return response.getBody() != null && response.getBody().equals("success");

        } catch (URISyntaxException | RestClientException e) {
            log.error("Error on topup", e);
        }

        return false;
    }

    /**
     * Download all files every night
     */
    @Scheduled(cron = "0 0 1 * * *", zone = "Europe/Madrid")
    public void downloadFile() {

        configService.reload();
        String username = configService.getProperty("sumaftp.user");
        String password = configService.getProperty("sumaftp.password");
        String ftpServer = configService.getProperty("sumaftp.server");
        String remoteDir = configService.getProperty("sumaftp.remote");
        String localDir = configService.getProperty("sumaftp.local");
        String slastTime = configService.getProperty("sumaftp.lastTime");

        int lastTime = 0;
        if (slastTime != null) {
            try {
                lastTime = Integer.valueOf(slastTime);
            } catch (Exception e) {
            }
        }

        log.error("Running suma sftp");
        List<File> downloaded = new LinkedList<>();
        lastTime = sftpService.downloadFiles(username, password, ftpServer, remoteDir, localDir, lastTime, downloaded);
        configService.setPropertyAndSave("sumaftp.lastTime", String.valueOf(lastTime));
        log.error("last time " + lastTime);

        parseFiles(downloaded);

    }

    /**
     *
     * @param files
     */
    private void parseFiles(List<File> files) {
        files.stream().filter((file) -> !(file.isDirectory())).forEachOrdered((file) -> {
            parseFile(file);
        });
    }

    /**
     *
     * @param file
     */
    private void parseFile(File file) {
        try {
            BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            while ((line = bf.readLine()) != null) {
                try {

                    if (line.isEmpty()) {
                        continue;
                    }

                    if (line.contains("aggregatorRef") || line.contains("Fecha;Gestor")) {
                        continue;
                    }

                    line = line.replaceAll("\"", "");
                    String[] splited = line.split(";");

                    if (!(splited.length == 5 || splited.length == 7)) {
                        log.error("Error malformed line " + line);
                        continue;
                    }

                    List<TopupProvider> tops;
                    if (splited.length == 5) {
                        tops = parseTopUpProvider(splited);
                    } else {
                        tops = parsePackageProvider(splited);
                    }

                    tops.forEach((top) -> {
                        try {
                            topUpsService.saveTopUpProvider(top);
                        } catch (Exception e) {
                        }
                    });
                } catch (Exception e) {
                    log.error("Error parseando linea " + line, e);
                }
            }
        } catch (IOException e) {
            log.error("Error leyendo archivo " + file.getName(), e);
        }
    }

    /**
     *
     * @param splited
     * @return
     */
    private List<TopupProvider> parseTopUpProvider(String[] splited) {

        List<TopupProvider> ret = new LinkedList<>();
        //Caso perfecto
        //aggregatorRef;amount;date;msisdnID;Subscription lastIccID
        //"EWS5263d77f0efc06COR521647ebeb44a0";3.000,00;"27/03/2020 17:13:49";"3024468181";"8957287016000400328"
        //"EWS5267e2858eaaa8COR5268bf612b974c";2.000,00;"27/03/2020 18:43:42";"3173381444";"8957287016000412786"
        //caso transferencia de saldo
        //;-10.000,00;"31/03/2020 14:51:13";"3204907785";"8957287016000386949"
        //;10.000,00;"31/03/2020 14:51:13";"3336225014";"8957287016000387244"
        //;-10.000,00;"31/03/2020 14:52:05";"3204907785";"8957287016000386949"
        //;10.000,00;"31/03/2020 14:52:05";"3336225014";"8957287016000387244"
        //;-5.000,00;"31/03/2020 14:52:22";"3204907785";"8957287016000386949"
        //;5.000,00;"31/03/2020 14:52:23";"3336225014";"8957287016000387244"
        String reference = splited[0];
        String amount = splited[1];
        String date = splited[2];
        String msisdnID = splited[3];
        String lastIccID = splited[4];

        try {
            TopupProvider top = new TopupProvider();
            amount = amount.replaceAll(",00", "");
            amount = amount.replaceAll("\\.", "");
            amount = amount.replaceAll(",", ".");

            top.setAmount(Double.parseDouble(amount));
            top.setCreationDate(sdf.parse(date));
            top.setIccid(lastIccID);
            top.setMsisdn(msisdnID);
            top.setProvider("suma");
            top.setCurrency("COP");
            top.setReference(reference);

            ret.add(top);

        } catch (Exception e) {
            log.error("Error parse top up provider " + Arrays.toString(splited), e);
        }

        return ret;
    }

    /**
     *
     * @param splited
     * @return
     */
    private List<TopupProvider> parsePackageProvider(String[] splited) {

        List<TopupProvider> ret = new LinkedList<>();

//        2020-05-28 12:42:17;WingsColombiaWS;3336229171;PACK_30_PREMIUM;8957287016000431612;PKGID502658_TS1590687736524;62000.000000
//        2020-05-28 09:52:51;WingsColombiaWS;3228416648;PACK_30_MINI;8957287016000474497;PKGID502586_TS1590677570389;19000.000000
//        2020-05-28 09:52:54;WingsColombiaWS;3228416648;PACK_30_MINI;8957287016000474497;PKGID502590_TS1590677574123;19000.000000
        String date = splited[0];
        String pos = splited[1];
        String msisdnID = splited[2];
        String description = splited[3];
        String lastIccID = splited[4];
        String reference = splited[5];
        String amount = splited[6];

        try {
            TopupProvider top = new TopupProvider();
            amount = amount.replaceAll("\\.000000", "");
            //amount = amount.replaceAll("\\.", "");
            //amount = amount.replaceAll(",", ".");

            top.setAmount(Double.parseDouble(amount));
            if (date.contains("/")) {
                top.setCreationDate(sdf.parse(date));
            } else {
                top.setCreationDate(sdfPkg.parse(date));
            }
            top.setIccid(lastIccID);
            top.setMsisdn(msisdnID);
            top.setProvider("suma");
            top.setCurrency("COP");
            top.setReference(reference);
            top.setPos(pos);
            top.setDescription(description);

            ret.add(top);

        } catch (Exception e) {
            log.error("Error parse top up provider " + Arrays.toString(splited), e);
        }

        return ret;
    }

    public List<String> getFileList() {
        String localDir = configService.getProperty("sumaftp.local");
        File f = new File(localDir);
        List<String> temp = Arrays.asList(f.list());
        temp.sort((o1, o2) -> {
            return o2.compareTo(o1);
        });
        return temp;
    }

    public File getFile(String fileName) {
        String localDir = configService.getProperty("sumaftp.local") + "/" + fileName;
        return new File(localDir);
    }

}
