/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.utils;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author seba
 */
public class AuthUtils {

    public static AdminUser getAdminUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails detail = (UserDetails) auth.getPrincipal();

        if (!(detail instanceof AdminUser)) {
            return null;
        }

        return (AdminUser) detail;
    }

    public static Member getMemberUser() {

        if (isAdmin()) {
            return getMember();
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails detail = (UserDetails) auth.getPrincipal();
        return (Member) detail;

    }

    public static boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof AdminUser;
    }

    public static void setMember(Member member) {
        getSession().setAttribute("member", member);
    }

    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    private static HttpSession getSession() {
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = attributes.getRequest();
        HttpSession httpSession = request.getSession(true);
        return httpSession;
    }

    private static Member getMember() {
        return (Member) getSession().getAttribute("member");
    }
}
