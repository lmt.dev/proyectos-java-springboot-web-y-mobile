/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.filters;

import com.wings.api.models.CountryDto;
import com.wings.backoffice.services.CountriesService;
import com.wings.backoffice.services.GeoIpService;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

/**
 *
 * @author seba
 */
@Component
public class CountryFilter implements Filter {

    @Autowired
    private GeoIpService geoIpService;
    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private CountriesService countriesService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpSession session = ((HttpServletRequest) request).getSession();
            if (session.getAttribute("locale_set") == null) {
                String iso = geoIpService.getIsoForIp(geoIpService.getIp((HttpServletRequest) request));
                CountryDto dto = countriesService.getCountryDto(iso);
                if (dto != null && dto.getDefaultLang() != null) {
                    localeResolver.setLocale((HttpServletRequest) request, (HttpServletResponse) response, new Locale(dto.getDefaultLang()));
                    session.setAttribute("locale_set", true);
                }
            }
        } catch (Exception e) {
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
