/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import java.util.List;

/**
 * class used to build the javascript 
 * node for tree view
 * @author seba
 */
public class MemberNode {

    private String text;
    private List<MemberNode> nodes;

    private String icon = "fa fa-user";
   
    private String color = "#000000";
    private String backColor = "#FFFFFF";
    private boolean selectable = true;
    private NodeState state = new NodeState();

    public MemberNode(String text, List<MemberNode> nodes) {
        this.text = text;
        this.nodes = nodes;
        this.icon = "/images/img.jpg";
    }
    
    

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setNodes(List<MemberNode> nodes) {
        this.nodes = nodes;
    }

    public List<MemberNode> getNodes() {
        return nodes;
    }
    

    public class NodeState {

        private boolean checked = true;
        private boolean disabled = false;
        private boolean expanded = false;
        private boolean selected = false;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public boolean isDisabled() {
            return disabled;
        }

        public void setDisabled(boolean disabled) {
            this.disabled = disabled;
        }

        public boolean isExpanded() {
            return expanded;
        }

        public void setExpanded(boolean expanded) {
            this.expanded = expanded;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBackColor() {
        return backColor;
    }

    public void setBackColor(String backColor) {
        this.backColor = backColor;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public NodeState getState() {
        return state;
    }

    public void setState(NodeState state) {
        this.state = state;
    }
}
