/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.ResidualService;
import com.wings.backoffice.mlm.domain.bis.invoices.Invoice;
import com.wings.backoffice.mlm.domain.bis.orders.Order;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service used to cominucate with GSM lines services, served by
 * wa.uppertel.com:8090
 *
 * @author seba
 */
@Service
public class GsmLinesService {

    private final String url = "http://wa.uppertel.com:8090/api/services/";
    private final String auth = "a0e3fdf8e14cf3b7842a3d715e100faccac0efdb49e9fe510fd33b6ea603bd1a";

    /**
     * Method to import orders from GSM line services, each line will create an
     * order based on Balance of the month and year passed as parameter
     *
     * @param year
     * @param month
     * @return
     */
    public List<Order> getServiceOrders(int year, int month) {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", auth);
        HttpEntity requestEntity = new HttpEntity<>(headers);
        ResponseEntity<Order[]> result = template.postForEntity(url + "orders/" + year + "/" + month, requestEntity, Order[].class);
        return Arrays.asList(result.getBody());
    }

    /**
     * Method to obtain each client and line from GSM service matching with
     * Member Service structure
     *
     * @return
     */
    public List<ResidualService> getLineServices() {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", auth);
        HttpEntity requestEntity = new HttpEntity<>(headers);
        ResponseEntity<ResidualService[]> result = template.postForEntity(url + "lines", requestEntity, ResidualService[].class);
        return Arrays.asList(result.getBody());
    }

    public List<Invoice> getLinesInvoices(List<ResidualService> services) {

        List<Long> ids = new ArrayList<>();
        services.forEach((service) -> {
            ids.add(service.getReferenceId());
        });

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", auth);
        HttpEntity<List<Long>> requestEntity = new HttpEntity<>(ids, headers);

        ResponseEntity<Invoice[]> result = template.postForEntity(url + "invoices", requestEntity, Invoice[].class);

        return Arrays.asList(result.getBody());
    }
}
