/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.paymentsType;

import com.wings.backoffice.mlm.domain.PaymentType;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author maurib
 */
public interface PaymentTypeService {
    
    public List<PaymentType> findAll();
}
