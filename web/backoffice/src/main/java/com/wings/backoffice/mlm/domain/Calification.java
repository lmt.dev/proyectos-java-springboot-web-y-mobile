/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.domain;

import com.wings.backoffice.mlm.domain.bis.RankBis;
import java.util.Map;

/**
 *
 * @author seba
 */
public class Calification {

    private Boolean mustUpgrade;
    private Double points;
    private Double pointsNeeded;

    private long idMosValuable;
    private Integer clientCount;
    private Integer referalCount;

    private RankBis currentRank;
    private RankBis nextRank;
    private Map<Long, Double> offered;
    private Map<Long, Double> taken;
    private Map<Long, String> takenText;

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public Double getPointsNeeded() {
        return pointsNeeded;
    }

    public void setPointsNeeded(Double pointsNeeded) {
        this.pointsNeeded = pointsNeeded;
    }

    public RankBis getCurrentRank() {
        return currentRank;
    }

    public void setCurrentRank(RankBis currentRank) {
        this.currentRank = currentRank;
    }

    public RankBis getNextRank() {
        return nextRank;
    }

    public void setNextRank(RankBis nextRank) {
        this.nextRank = nextRank;
    }

    public Map<Long, Double> getOffered() {
        return offered;
    }

    public void setOffered(Map<Long, Double> offered) {
        this.offered = offered;
    }

    public Map<Long, Double> getTaken() {
        return taken;
    }

    public void setTaken(Map<Long, Double> taken) {
        this.taken = taken;
    }

    public Map<Long, String> getTakenText() {
        return takenText;
    }

    public void setTakenText(Map<Long, String> takenText) {
        this.takenText = takenText;
    }

    public void setMustUpgrade(Boolean mustUpgrade) {
        this.mustUpgrade = mustUpgrade;
    }

    public Boolean getMustUpgrade() {
        return mustUpgrade;
    }

    public void setClientCount(Integer clientCount) {
        this.clientCount = clientCount;
    }

    public Integer getClientCount() {
        return clientCount;
    }

    public void setReferalCount(Integer referalCount) {
        this.referalCount = referalCount;
    }

    public Integer getReferalCount() {
        return referalCount;
    }

    public void setIdMosValuable(long idMosValuable) {
        this.idMosValuable = idMosValuable;
    }

    public long getIdMosValuable() {
        return idMosValuable;
    }

    
}
