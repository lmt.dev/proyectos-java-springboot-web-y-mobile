/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.City;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface CitiesRepository extends JpaRepository<City, Long> {

    List<City> findByCountry(String country);

    List<City> findByStateId(Long stateId);

    List<City> findByCountryAndCityLike(String country, String cityName);

    //City findByCity(String cityName);
    List<City> findByCity(String cityName);

    List<City> findByCityAndCountry(String cityName, String country);

}
