/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.promos;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.bis.CalificationPoint;
import com.wings.backoffice.mlm.domain.bis.RankBis;
import com.wings.backoffice.mlm.domain.promo.PromoLatam;
import com.wings.backoffice.mlm.repos.promo.PromoLatamRepository;
import com.wings.backoffice.mlm.specs.PromoLatamSpecs;
import com.wings.backoffice.services.CalificationService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.services.OrdersService;
import com.wings.backoffice.services.RanksService;
import com.wings.backoffice.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PromoLatamService {

    @Autowired
    private RanksService ranksService;
    @Autowired
    private MembersService membersService;
    @Autowired
    private CalificationService calificationService;
    @Autowired
    private PromoLatamRepository promoLatamRepository;
    @Autowired
    private OrdersService ordersService;

    private final Logger log = LoggerFactory.getLogger(PromoLatamService.class);

    //Cancun
    public void listMembersWingsLatamPromo() {

        log.error("Start promo calc");
        List<PromoLatam> toSave = new LinkedList<>();
        Date desde = DateUtils.fromMysqlStringToDate("2020-04-01 00:00:00");
        Date hasta = DateUtils.fromMysqlStringToDate("2020-07-31 23:59:59");

        //List<String> countries = Arrays.asList("PE", "CO", "EC");
        List<Member> members = membersService.getNonDeletedMembers();
        for (Member member : members) {

            //if is not payed, continue
            if (!member.getOrder().getStatus().equals("paid")) {
                continue;
            }

            if (member.isExcludePromo()) {
                continue;
            }

            RankBis rankAtDate = ranksService.getRankAtDate(member, desde);

            //rank not found continue
            if (rankAtDate == null) {
                continue;
            }

            //get points neeeded
            double pointsNeeded = 0;
            //puntos directos
            double pointsNeeded200 = 200;
            switch (rankAtDate.getName()) {
                case "DEALER":
                    pointsNeeded = 1500;
                    break;
                case "TEAM_DEALER":
                    pointsNeeded = 2500;
                    break;
                case "DIRECTOR":
                    pointsNeeded = 3500;
                    break;
                case "TEAM_DIRECTOR":
                    pointsNeeded = 5000;
                    break;
                case "REGIONAL_PRESIDENT":
                    pointsNeeded = 10000;
                    break;
                case "NATIONAL_PRESIDENT":
                    pointsNeeded = 20000;
                    break;
                case "INTERNATIONAL_PRESIDENT":
                    pointsNeeded = 30000;
                    break;
                case "DIAMOND_PRESIDENT":
                    pointsNeeded = 80000;
                    break;
                default:
                    continue;
            }

            //puntos directos
            double directPoints = 0;
            //get member points
            double sumPoints = 0;
            List<CalificationPoint> points = calificationService.getPointsForMember(member, desde, hasta);

            //get child ids
            Map<Long, Double> pointsPerChild = new HashMap<>();
            Map<Long, Double> sumsPerBranch = new HashMap<>();
            Map<Long, List<Long>> ids = new HashMap<>();
            List<Long> myId = new ArrayList<>();

            myId.add(member.getId());
            ids.put(member.getId(), myId);
            List<Member> children = member.getChildren();
            children.forEach((child) -> {
                ids.put(child.getId(), membersService.getChildIds(child.getId()));
            });

            //sumo puntos por ramas
            for (CalificationPoint point : points) {

                //puntos directos 200 si califica
                if (point.getMemberType().equals("direct")) {
                    directPoints += point.getPoints();
                }

                Long offerMemberId = ordersService.findMemberIdByOrderId(point.getEventId());
                //fill points per child
                if (offerMemberId == null) {
                    //mi rama
                    if (pointsPerChild.containsKey(member.getId())) {
                        pointsPerChild.put(member.getId(), pointsPerChild.get(member.getId()) + point.getPoints());
                    } else {
                        pointsPerChild.put(member.getId(), point.getPoints());
                    }
                } else {
                    //de afiliados    
                    if (pointsPerChild.containsKey(offerMemberId)) {
                        pointsPerChild.put(offerMemberId, pointsPerChild.get(offerMemberId) + point.getPoints());
                    } else {
                        pointsPerChild.put(offerMemberId, point.getPoints());
                    }
                }
            }

            //sum per branch
            for (Map.Entry<Long, List<Long>> entry : ids.entrySet()) {
                Long key = entry.getKey();
                List<Long> idList = entry.getValue();

                if (idList == null) {
                    continue;
                }

                double sum = 0.0;
                for (Map.Entry<Long, Double> entry1 : pointsPerChild.entrySet()) {
                    Long key1 = entry1.getKey();
                    Double value1 = entry1.getValue();
                    if (idList.contains(key1)) {
                        sum += value1;
                    }
                }
                sumsPerBranch.put(key, sum);
            }

            Long idMostValuableBranch = getIdBranchMostValuable(sumsPerBranch);

            double sum;
            Double most = sumsPerBranch.get(idMostValuableBranch);
            Double mostVal = most != null ? most : 0.0;
            Double sixty = pointsNeeded * 0.60;
            if (mostVal > sixty) {
                sum = sixty;
            } else {
                sum = mostVal;
            }

            for (Map.Entry<Long, Double> entry : sumsPerBranch.entrySet()) {
                Long key = entry.getKey();
                Double value = entry.getValue();
                if (!key.equals(idMostValuableBranch)) {
                    sum += value;
                }
            }
            sumPoints = sum;

            PromoLatam promo = new PromoLatam();

            promo.setCountry(member.getCountry());
            promo.setFirstName(member.getFirstName());
            promo.setLastName(member.getLastName());
            promo.setMemberId(member.getId());
            promo.setPointsNeeded(pointsNeeded);
            promo.setPointsReached(sumPoints);
            promo.setRankId(rankAtDate.getId());
            promo.setRankName(rankAtDate.getName());
            promo.setUsername(member.getUsername());
            promo.setEmail(member.getEmail());

            int percentReached;
            //verificar si califica
            if (sumPoints >= pointsNeeded) {
                percentReached = 100;
            } else {
                percentReached = (int) ((sumPoints * 100) / pointsNeeded);
            }
            promo.setPercentReached(percentReached);

            //puntos directos SUM los 200
            //verificar si califica 200
            promo.setPointsNeeded2(pointsNeeded200);
            promo.setPointsReached2(directPoints);
            if (directPoints >= pointsNeeded200) {
                promo.setPercentReached2(100);
            } else {
                promo.setPercentReached2((int) ((directPoints * 100) / pointsNeeded200));
            }

            promo.setQualify(false);
            if (promo.getPercentReached() == 100) {
                promo.setQualify(true);
            }

            toSave.add(promo);
        }

        //save all
        promoLatamRepository.deleteAll();
        promoLatamRepository.save(toSave);

        log.error("Done promo calc");
    }

    /**
     *
     * @param q
     * @param page
     * @param size
     * @param country
     * @param qualify
     * @return
     */
    public Page<PromoLatam> getPromoLatam(String q, int page, int size, String country, Boolean qualify) {
        page = page == 0 ? page : page - 1;
        Sort.Order percentOneSort = new Sort.Order(Sort.Direction.DESC, "percentReached");
        PageRequest pr = new PageRequest(page, size, new Sort(percentOneSort));

        //si se usa el 2do percent
        //Sort.Order percentTwoSort = new Sort.Order(Sort.Direction.DESC, "percentReached2");
        //PageRequest pr = new PageRequest(page, size, new Sort(percentTwoSort, percentOneSort));
        return promoLatamRepository.findAll(PromoLatamSpecs.search(q, country, qualify), pr);
    }

    /**
     *
     * @param country
     * @param qualify
     * @return
     */
    public List<PromoLatam> getPromoLatam(String country, Boolean qualify) {
        return promoLatamRepository.findAll(PromoLatamSpecs.search(null, country, qualify));
    }

    /**
     *
     * @return
     */
    public List<PromoLatam> getTop20PromoLatam() {
        Sort.Order percentOneSort = new Sort.Order(Sort.Direction.DESC, "percentReached");
        PageRequest pr = new PageRequest(0, 20, new Sort(percentOneSort));
        //Sort.Order percentTwoSort = new Sort.Order(Sort.Direction.DESC, "percentReached2");
        //PageRequest pr = new PageRequest(0, 20, new Sort(percentTwoSort, percentOneSort));
        return promoLatamRepository.findAll(PromoLatamSpecs.search(null, null, null), pr).getContent();
    }

    /**
     *
     * @param memberId
     * @return
     */
    public PromoLatam getPromoLatamForMember(Long memberId) {
        return promoLatamRepository.findByMemberId(memberId);
    }

    /**
     *
     * @param pointsPerBranch
     * @return
     */
    private Long getIdBranchMostValuable(Map<Long, Double> pointsPerBranch) {
        Long ret = 0l;
        double maxPoints = 0.0;
        for (Map.Entry<Long, Double> entry : pointsPerBranch.entrySet()) {
            Long key = entry.getKey();
            Double value = entry.getValue();
            if (maxPoints < value) {
                maxPoints = value;
                ret = key;
            }
        }
        return ret;
    }

}
