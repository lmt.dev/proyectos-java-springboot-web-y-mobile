/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.bis.orders.Shipment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ShipmentsRepository extends JpaRepository<Shipment, Long>, JpaSpecificationExecutor<Shipment> {

    List<Shipment> findByOrderId(Long orderId);

    List<Shipment> findByOrderIdAndDeleted(Long orderId, Boolean deleted);

    List<Shipment> findByStatus(String status);

    List<Shipment> findByStatusAndDeleted(String status, Boolean deleted);

    List<Shipment> findByStatusAndDeletedAndEmailSentAndShippingCountry(String status, Boolean deleted, Boolean emailSent, String shippingCountry);

    List<Shipment> findTop25ByStatus(String status);

    Shipment findFirstByStatus(String status);

    Shipment findByShipmentNumber(String shipmentNumber);

    Shipment findByTrackingNumber(String trackingNumber);

    List<Shipment> findByOrderIdAndStatus(Long orderId, String status);

}
