/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.admin;

import com.wings.backoffice.mlm.domain.events.EventHost;
import com.wings.backoffice.services.EventsService;
import com.wings.backoffice.utils.PageWrapper;
import com.wings.backoffice.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class EventUsersController {

    @Autowired
    private EventsService eventsService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/events/users"}, method = {RequestMethod.GET})
    public String getEventUsers(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        Page<EventHost> items = eventsService.searchEventUsers(q, page, size);
        PageWrapper<EventHost> pageWrapper = new PageWrapper<>(items, "admin/events/users");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "admin/events/users";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"admin/events/users/add"}, method = {RequestMethod.GET})
    public String getAddUSer(Model model, HttpServletRequest request) {
        return "admin/events/users-add";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"admin/events/users/add"}, method = {RequestMethod.POST})
    public String postAddUSer(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String name = request.getParameter("daname");
        String email = request.getParameter("daemail");
        String password = request.getParameter("dapassword");
        String admin = request.getParameter("admin");

        EventHost already = eventsService.getHostByEmail(email);
        if (already != null) {
            redirectAttributes.addFlashAttribute("message", "El email ya esta registrado");
            redirectAttributes.addFlashAttribute("messageType", "error");
            return "redirect:/admin#admin/events/users";
        }

        EventHost host = new EventHost();
        host.setAdmin(admin != null && admin.equals("on"));
        host.setEmail(email);
        host.setName(name);
        host.setPassword(password);
        eventsService.saveHost(host);

        return "redirect:/admin#admin/events/users";
    }

    /**
     *
     * @param model
     * @param request
     * @param userId
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"admin/events/users/delete/{id}"}, method = {RequestMethod.POST})
    public String postAddDeleteUser(Model model, HttpServletRequest request, @PathVariable("id") Long userId, RedirectAttributes redirectAttributes) {

        EventHost host = eventsService.getHost(userId);
        eventsService.deleteHost(host);

        redirectAttributes.addFlashAttribute("message", "Usuario eliminado!");
        redirectAttributes.addFlashAttribute("messageType", "succes");

        return "redirect:/admin#admin/events/users";

    }

}
