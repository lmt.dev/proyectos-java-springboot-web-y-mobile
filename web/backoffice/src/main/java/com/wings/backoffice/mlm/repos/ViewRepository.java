package com.wings.backoffice.mlm.repos;
import java.io.Serializable;
import org.springframework.data.repository.*;

@NoRepositoryBean
public interface ViewRepository<T, ID extends Serializable> extends Repository<T, ID> {

    T findOne(ID id);

    boolean exists(ID id);

    Iterable<T> findAll();

    long count();
}
