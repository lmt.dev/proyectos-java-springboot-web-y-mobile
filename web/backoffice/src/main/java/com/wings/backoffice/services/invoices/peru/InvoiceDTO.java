/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services.invoices.peru;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author seba
 */
public class InvoiceDTO {

    //Cabecera del documento
    @JsonProperty("tipo_operacion")
    private String tipoOperacion;
    @JsonProperty("total_gravadas")
    private String totalGravadas;
    @JsonProperty("total_inafecta")
    private String totalInafecta;
    @JsonProperty("total_exoneradas")
    private String totalExoneradas;
    @JsonProperty("total_gratuitas")
    private String totalGratuitas;
    @JsonProperty("total_exportacion")
    private String totalExportacion;
    @JsonProperty("total_descuento")
    private String totalDescuento;
    @JsonProperty("sub_total")
    private String subTotal;
    @JsonProperty("porcentaje_igv")
    private String porcentajeIgv;
    @JsonProperty("total_igv")
    private String totalIgv;
    @JsonProperty("TOTAL_ICBPER")
    private Integer totalIcbPer;
    @JsonProperty("total_isc")
    private String totalIsc;
    @JsonProperty("total_otr_imp")
    private String totalOtrImp;
    @JsonProperty("total")
    private String total;
    @JsonProperty("total_letras")
    private String totalLetras;
    @JsonProperty("nro_guia_remision")
    private String nroGuiaRemision;
    @JsonProperty("cod_guia_remision")
    private String codGuiaRemision;
    @JsonProperty("nro_otr_comprobante")
    private String nroOtrComprobante;
    @JsonProperty("serie_comprobante")
    private String serieComprobante;
    @JsonProperty("numero_comprobante")
    private String numeroComprobante;
    @JsonProperty("fecha_comprobante")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date fechaComprobante;
    @JsonProperty("fecha_vto_comprobante")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date fechaVtoComprobante;
    @JsonProperty("cod_tipo_documento")
    private String codTipoDocumento;
    @JsonProperty("cod_moneda")
    private String codMoneda;

    //Datos del cliente
    @JsonProperty("cliente_numerodocumento")
    private String clienteNumeroDocumento;
    @JsonProperty("cliente_nombre")
    private String clienteNombre;
    @JsonProperty("cliente_tipodocumento")
    private String clienteTipoDocumento;
    @JsonProperty("cliente_direccion")
    private String clienteDireccion;
    @JsonProperty("cliente_pais")
    private String clientePais;
    @JsonProperty("cliente_ciudad")
    private String clienteCiudad;
    @JsonProperty("cliente_codigoubigeo")
    private String clienteCodigoUbigeo;
    @JsonProperty("cliente_departamento")
    private String clienteDepartamento;
    @JsonProperty("clienteProvincia")
    private String clienteProvincia;
    @JsonProperty("cliente_distrito")
    private String clienteDistrito;

    //Emisor
    private Emisor emisor;
    //Detalles
    private List<Item> detalle = new ArrayList<>();

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTotalGravadas() {
        return totalGravadas;
    }

    public void setTotalGravadas(String totalGravadas) {
        this.totalGravadas = totalGravadas;
    }

    public String getTotalInafecta() {
        return totalInafecta;
    }

    public void setTotalInafecta(String totalInafecta) {
        this.totalInafecta = totalInafecta;
    }

    public String getTotalExoneradas() {
        return totalExoneradas;
    }

    public void setTotalExoneradas(String totalExoneradas) {
        this.totalExoneradas = totalExoneradas;
    }

    public String getTotalGratuitas() {
        return totalGratuitas;
    }

    public void setTotalGratuitas(String totalGratuitas) {
        this.totalGratuitas = totalGratuitas;
    }

    public String getTotalExportacion() {
        return totalExportacion;
    }

    public void setTotalExportacion(String totalExportacion) {
        this.totalExportacion = totalExportacion;
    }

    public String getTotalDescuento() {
        return totalDescuento;
    }

    public void setTotalDescuento(String totalDescuento) {
        this.totalDescuento = totalDescuento;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getPorcentajeIgv() {
        return porcentajeIgv;
    }

    public void setPorcentajeIgv(String porcentajeIgv) {
        this.porcentajeIgv = porcentajeIgv;
    }

    public String getTotalIgv() {
        return totalIgv;
    }

    public void setTotalIgv(String totalIgv) {
        this.totalIgv = totalIgv;
    }

    public Integer getTotalIcbPer() {
        return totalIcbPer;
    }

    public void setTotalIcbPer(Integer totalIcbPer) {
        this.totalIcbPer = totalIcbPer;
    }

    public String getTotalIsc() {
        return totalIsc;
    }

    public void setTotalIsc(String totalIsc) {
        this.totalIsc = totalIsc;
    }

    public String getTotalOtrImp() {
        return totalOtrImp;
    }

    public void setTotalOtrImp(String totalOtrImp) {
        this.totalOtrImp = totalOtrImp;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalLetras() {
        return totalLetras;
    }

    public void setTotalLetras(String totalLetras) {
        this.totalLetras = totalLetras;
    }

    public String getNroGuiaRemision() {
        return nroGuiaRemision;
    }

    public void setNroGuiaRemision(String nroGuiaRemision) {
        this.nroGuiaRemision = nroGuiaRemision;
    }

    public String getCodGuiaRemision() {
        return codGuiaRemision;
    }

    public void setCodGuiaRemision(String codGuiaRemision) {
        this.codGuiaRemision = codGuiaRemision;
    }

    public String getNroOtrComprobante() {
        return nroOtrComprobante;
    }

    public void setNroOtrComprobante(String nroOtrComprobante) {
        this.nroOtrComprobante = nroOtrComprobante;
    }

    public String getSerieComprobante() {
        return serieComprobante;
    }

    public void setSerieComprobante(String serieComprobante) {
        this.serieComprobante = serieComprobante;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public Date getFechaComprobante() {
        return fechaComprobante;
    }

    public void setFechaComprobante(Date fechaComprobante) {
        this.fechaComprobante = fechaComprobante;
    }

    public Date getFechaVtoComprobante() {
        return fechaVtoComprobante;
    }

    public void setFechaVtoComprobante(Date fechaVtoComprobante) {
        this.fechaVtoComprobante = fechaVtoComprobante;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public void setCodTipoDocumento(String codTipoDocumento) {
        this.codTipoDocumento = codTipoDocumento;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public String getClienteNumeroDocumento() {
        return clienteNumeroDocumento;
    }

    public void setClienteNumeroDocumento(String clienteNumeroDocumento) {
        this.clienteNumeroDocumento = clienteNumeroDocumento;
    }

    public String getClienteNombre() {
        return clienteNombre;
    }

    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

    public String getClienteTipoDocumento() {
        return clienteTipoDocumento;
    }

    public void setClienteTipoDocumento(String clienteTipoDocumento) {
        this.clienteTipoDocumento = clienteTipoDocumento;
    }

    public String getClienteDireccion() {
        return clienteDireccion;
    }

    public void setClienteDireccion(String clienteDireccion) {
        this.clienteDireccion = clienteDireccion;
    }

    public String getClientePais() {
        return clientePais;
    }

    public void setClientePais(String clientePais) {
        this.clientePais = clientePais;
    }

    public String getClienteCiudad() {
        return clienteCiudad;
    }

    public void setClienteCiudad(String clienteCiudad) {
        this.clienteCiudad = clienteCiudad;
    }

    public String getClienteCodigoUbigeo() {
        return clienteCodigoUbigeo;
    }

    public void setClienteCodigoUbigeo(String clienteCodigoUbigeo) {
        this.clienteCodigoUbigeo = clienteCodigoUbigeo;
    }

    public String getClienteDepartamento() {
        return clienteDepartamento;
    }

    public void setClienteDepartamento(String clienteDepartamento) {
        this.clienteDepartamento = clienteDepartamento;
    }

    public String getClienteProvincia() {
        return clienteProvincia;
    }

    public void setClienteProvincia(String clienteProvincia) {
        this.clienteProvincia = clienteProvincia;
    }

    public String getClienteDistrito() {
        return clienteDistrito;
    }

    public void setClienteDistrito(String clienteDistrito) {
        this.clienteDistrito = clienteDistrito;
    }

    public Emisor getEmisor() {
        return emisor;
    }

    public void setEmisor(Emisor emisor) {
        this.emisor = emisor;
    }

    public List<Item> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Item> detalle) {
        this.detalle = detalle;
    }

    public void addItem(Item item) {
        this.detalle.add(item);
    }
}
