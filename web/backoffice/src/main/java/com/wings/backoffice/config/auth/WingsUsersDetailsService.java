/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.config.auth;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.mlm.domain.admin.AdminUser;
import com.wings.backoffice.mlm.repos.AdminUsersRepository;
import com.wings.backoffice.mlm.repos.MembersRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service("wingsUsersDetailsService")
@Transactional
public class WingsUsersDetailsService implements UserDetailsService {

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private AdminUsersRepository adminUsersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDetails user = adminUsersRepository.findByUsername(username);
        if (user == null) {
            if (username.contains("@")) {
                user = membersRepository.findByEmailAndLoginEnabled(username, true);
            } else {
                user = membersRepository.findByUsernameAndLoginEnabled(username, true);
            }
        }

        if (user instanceof AdminUser) {
            if (((AdminUser) user).getId() == 5) {
                return null;
            }
        }/* else {
            return null;
        }*/

        if (user != null) {
            getAuthorities(user);
        }

        return user;
    }

    private Collection<WingsGrantedAuthority> getAuthorities(UserDetails user) {
        List<WingsGrantedAuthority> authorities = new ArrayList<>();
        if (user instanceof Member) {
            authorities.add(new WingsGrantedAuthority("ROLE_MEMBER"));
            ((Member) user).setGrantedAuthorities(authorities);

        } else {
            authorities.add(new WingsGrantedAuthority("ROLE_ADMIN"));
            ((AdminUser) user).setGrantedAuthorities(authorities);
        }
        return authorities;
    }

}
