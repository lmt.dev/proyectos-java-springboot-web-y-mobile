/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.repos;

import com.wings.backoffice.mlm.domain.OrderDetailOld;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
@Deprecated
public interface OrderDetailsRepository extends JpaRepository<OrderDetailOld, Long>, JpaSpecificationExecutor<OrderDetailOld> {

    List<OrderDetailOld> findByPayerEmail(String payerEmail);
    
    List<OrderDetailOld> findByItemName(String itemName);
    
}
