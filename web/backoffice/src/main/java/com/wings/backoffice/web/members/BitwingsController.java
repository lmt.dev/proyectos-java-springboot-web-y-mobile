/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.web.members;

import com.wings.backoffice.mlm.domain.Member;
import com.wings.backoffice.services.BitwingsService;
import com.wings.backoffice.services.MembersService;
import com.wings.backoffice.utils.AuthUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class BitwingsController {

    @Autowired
    private BitwingsService bitwingsService;
    @Autowired
    private MembersService membersService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"members/crypto/bitwings"}, method = RequestMethod.GET)
    public String getOrdersList(Model model, HttpServletRequest request) {

        Member m = AuthUtils.getMemberUser();
        m = membersService.getOne(m.getId());

        model.addAttribute("reclaimBwn", false);
        model.addAttribute("wallet", m.getBwnWallet());
        //Bitwings reclaim
        if (m.getBwnWallet() == null) {
            int bwn = bitwingsService.canReclaim(m.getId());
            model.addAttribute("bwnAmount", bwn);
            model.addAttribute("reclaimBwn", bwn != -1);
        }

        return "members/bitwings/index";
    }

}
