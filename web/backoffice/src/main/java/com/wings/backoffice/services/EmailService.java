/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.wings.backoffice.mlm.domain.bis.orders.Order;
import java.io.File;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author seba
 */
@Service
public class EmailService {

    @Autowired
    private Environment environment;
    @Autowired
    private MessageSource messageSource;

    private final Logger log = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender mailSender;

    @Autowired
    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public boolean sendMail(String from, String to, String subject, String text, List<File> attachements) {

        boolean ret = false;

        MimeMessage message = mailSender.createMimeMessage();
        //prevent sending emails to clients on dev
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (!profile.contains("prod")) {
                to = "Test<test@wingsmobile.com>";
                subject = "TEST-" + subject;
            }
        }

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(from);

            if (to.contains(",")) {
                String[] tos = to.split(",");
                helper.setTo(tos);
            } else {
                helper.setTo(to);
            }

            helper.setSubject(subject);
            helper.setText(text, true);

            if (attachements != null) {
                for (File f : attachements) {
                    helper.addAttachment(f.getName(), f);
                }
            }

            mailSender.send(message);

            ret = true;

        } catch (Exception e) {
            log.error("Error sending mail " + to, e);
        }
        return ret;
    }

    public String getGenericEmail(String client, String subject, String body) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("subject", subject);
        context.setVariable("client", client);
        context.setVariable("body", body);

        return engine.process("templates/email/email_generic", context);
    }

    public String getOrderEmail(String client, String body, Locale locale, String paymentLink, Order order) {

        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("client", client);
        String subject = messageSource.getMessage("email.thanksForYourOrder", null, locale) + " #" + order.getId();
        context.setVariable("subject", subject);
        context.setVariable("body", body);
        context.setVariable("thanksForYourOrder", messageSource.getMessage("email.thanksForYourOrder", null, locale));
        context.setVariable("hi", messageSource.getMessage("email.hi", null, locale));
        context.setVariable("client", client);
        context.setVariable("receive", messageSource.getMessage("email.receive", null, locale));
        context.setVariable("orderNumber", order.getId());
        context.setVariable("date", DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(order.getCreationDate()));
        context.setVariable("link", messageSource.getMessage("email.link", null, locale));
        context.setVariable("paymentLink", paymentLink);
        context.setVariable("thanks", messageSource.getMessage("email.thanks", null, locale));

        return engine.process("templates/email/email_order", context);
    }

    public String getEventEmail(String client, String subject, String body) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("client", client);
        context.setVariable("subject", subject);
        context.setVariable("body", body);
        return engine.process("templates/email/email_evento", context);
    }
}
