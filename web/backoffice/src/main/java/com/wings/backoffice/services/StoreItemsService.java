/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wings.api.models.AttributeItemDto;
import com.wings.api.models.OptionItemDto;
import com.wings.api.models.StoreItemDto;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItem;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemAttribute;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemChild;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemI18n;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemImage;
import com.wings.backoffice.mlm.domain.bis.orders.StoreItemOption;
import com.wings.backoffice.mlm.enums.OrderType;
import com.wings.backoffice.mlm.repos.StoreItemAttributesRepository;
import com.wings.backoffice.mlm.repos.StoreItemChildRepository;
import com.wings.backoffice.mlm.repos.StoreItemI18nRepository;
import com.wings.backoffice.mlm.repos.StoreItemImagesRepository;
import com.wings.backoffice.mlm.repos.StoreItemNamesRepository;
import com.wings.backoffice.mlm.repos.StoreItemOptionsRepository;
import com.wings.backoffice.mlm.repos.StoreItemsRepository;
import com.wings.backoffice.mlm.specs.StoreItemsSpecs;
import com.wings.backoffice.utils.CountryUtils;
import com.wings.backoffice.utils.NumberUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class StoreItemsService {

    @Autowired
    private StoreItemsRepository storeItemsRepository;
    @Autowired
    private StoreItemOptionsRepository storeItemOptionsRepository;
    @Autowired
    private StoreItemAttributesRepository storeItemAttributesRepository;
    @Autowired
    private StoreItemI18nRepository storeItemI18nRepository;
    @Autowired
    private StoreItemImagesRepository storeItemImagesRepository;
    @Autowired
    private StoreItemChildRepository storeItemChildRepository;
    @Autowired
    private CountriesService countriesService;

    /**
     *
     * @param q
     * @param page
     * @param size
     * @param enabled
     * @param category
     * @param status
     * @param country
     * @param direction
     * @return
     */
    public Page<StoreItem> searchItems(String q, int page, int size, Boolean enabled, String category, String status, String country, Sort.Direction direction) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(direction, "id"));
        return storeItemsRepository.findAll(StoreItemsSpecs.search(q, enabled, category, status, country), pr);
    }

    /**
     *
     * @param page
     * @param size
     * @param enabled
     * @param country
     * @return
     */
    public Page<StoreItem> searchItemsPopular(int page, int size, Boolean enabled, String country) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "salesCount"));
        return storeItemsRepository.findAll(StoreItemsSpecs.search(null, enabled, null, null, country), pr);
    }

    /**
     *
     * @param q
     * @param enabled
     * @param category
     * @param status
     * @param country
     * @return
     */
    public List<StoreItem> searchItemsAsList(String q, Boolean enabled, String category, String status, String country) {
        return storeItemsRepository.findAll(StoreItemsSpecs.search(q, enabled, category, status, country));
    }

    /**
     *
     * @param q
     * @param enabled
     * @param category
     * @param status
     * @param country
     * @return
     */
    public List<StoreItem> searchItemsInternal(String q, Boolean enabled, String category, String status, String country) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        return storeItemsRepository.findAll(StoreItemsSpecs.getInternalStore(q, enabled, category, status, country), sort);
    }

    /**
     *
     * @param itemId
     * @return
     */
    public StoreItem getOne(Long itemId) {
        return storeItemsRepository.findOne(itemId);
    }

    /**
     *
     * @return
     */
    public List<StoreItem> getUpgradableItems() {
        return storeItemsRepository.findByCategoryName(OrderType.kit.name());
    }

    /**
     *
     * @return
     */
    public List<StoreItem> getSingleItems() {
        return storeItemsRepository.findBySingleAndAvailableToComplex(Boolean.TRUE, Boolean.TRUE);
    }

    /**
     *
     * @param item
     */
    public void save(StoreItem item) {
        storeItemsRepository.save(item);
    }

    /**
     *
     * @param parentId
     * @param childId
     * @param quantity
     * @return
     */
    public Long addChildren(Long parentId, Long childId, Integer quantity) {

        StoreItemChild child = new StoreItemChild();
        child.setChildId(childId);
        child.setParentId(parentId);
        child.setQuantity(quantity);

        storeItemChildRepository.save(child);

        return child.getId();
    }

    /**
     *
     * @param id
     */
    public void removeChildren(Long id) {
        storeItemChildRepository.delete(id);
    }

    /**
     *
     * @param storeItemId
     * @return
     */
    public List<StoreItemOption> getStoreItemOptions(Long storeItemId) {
        return storeItemOptionsRepository.findByStoreItemId(storeItemId);
    }

    /**
     *
     * @param id
     * @param attributesJson
     * @return
     */
    public boolean saveAttributes(Long id, String attributesJson) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<StoreItemAttribute> selectedAttributes = mapper.readValue(attributesJson, new TypeReference<List<StoreItemAttribute>>() {
            });
            List<StoreItemAttribute> actualAttributes = storeItemAttributesRepository.findByStoreItemId(id);
            List<StoreItemAttribute> attributesToDelete = new ArrayList<>();

            actualAttributes.forEach((actual) -> {
                boolean contains = false;
                for (StoreItemAttribute selected : selectedAttributes) {
                    if (selected.getId() != null && actual.getId().equals(selected.getId())) {
                        contains = true;
                    }
                }
                if (!contains) {
                    attributesToDelete.add(actual);
                }
            });

            attributesToDelete.forEach(storeItemAttributesRepository::delete);
            selectedAttributes.forEach(storeItemAttributesRepository::save);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean saveChildrens(Long id, String attributesJson) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<StoreItemChild> selectedChildrens = mapper.readValue(attributesJson, new TypeReference<List<StoreItemChild>>() {
            });
            List<StoreItemChild> actualChildrens = storeItemChildRepository.findByParentId(id);
            List<StoreItemChild> childrensToDelete = new ArrayList<>();

            actualChildrens.forEach((actual) -> {
                boolean contains = false;
                for (StoreItemChild selected : selectedChildrens) {
                    if (selected.getId() != null && actual.getId().equals(selected.getId())) {
                        contains = true;
                    }
                }
                if (!contains) {
                    childrensToDelete.add(actual);
                }
            });

            childrensToDelete.forEach(storeItemChildRepository::delete);

            selectedChildrens.forEach(storeItemChildRepository::save);

            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     *
     * @param code
     * @return
     */
    public StoreItem getOneByCode(String code) {
        return storeItemsRepository.findByCode(code);
    }

    public boolean saveCountries(Long itemId, String countries) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<StoreItemI18n> selectedCountries = mapper.readValue(countries, new TypeReference<List<StoreItemI18n>>() {
            });
            List<StoreItemI18n> actualCountries = storeItemI18nRepository.findByStoreItemId(itemId);
            List<StoreItemI18n> countriesToDelete = new ArrayList<>();

            actualCountries.forEach((actual) -> {
                boolean contains = false;
                for (StoreItemI18n selected : selectedCountries) {
                    if (selected.getId() != null && actual.getId().equals(selected.getId())) {
                        contains = true;
                    }
                }
                if (!contains) {
                    countriesToDelete.add(actual);
                }
            });

            countriesToDelete.forEach(storeItemI18nRepository::delete);

            StoreItem item = storeItemsRepository.findOne(itemId);
            StringBuilder newCountries = new StringBuilder("");

            for (int i = 0; i < selectedCountries.size(); i++) {
                String country = selectedCountries.get(i).getCountry();
                if (i == selectedCountries.size() - 1) {
                    newCountries.append(country);
                } else {
                    newCountries.append(country).append(",");
                }
            }
            item.setCountries(newCountries.toString());
            selectedCountries.forEach((selectedCountry) -> {
                Double temp = 0.0;
                if (!(selectedCountry.getPrice() == null || selectedCountry.getPrice() == 0)) {
                    temp = NumberUtils.convertMoneyToEuros(selectedCountry.getPrice(), CountryUtils.getLocaleForCountry(selectedCountry.getCountry()));
                    selectedCountry.setPrice(temp);
                }
            });

            storeItemsRepository.save(item);
            selectedCountries.forEach(storeItemI18nRepository::save);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean saveImages(Long itemId, String images) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<StoreItemImage> selectedImages = mapper.readValue(images, new TypeReference<List<StoreItemImage>>() {
            });
            List<StoreItemImage> actualImages = storeItemImagesRepository.findByStoreItemId(itemId);
            List<StoreItemImage> imagesToDelete = new ArrayList<>();

            actualImages.forEach((actual) -> {
                boolean contains = false;
                for (StoreItemImage selected : selectedImages) {
                    if (selected.getId() != null && actual.getId().equals(selected.getId())) {
                        contains = true;
                    }
                }
                if (!contains) {
                    imagesToDelete.add(actual);
                }
            });

            imagesToDelete.forEach(storeItemImagesRepository::delete);
            selectedImages.forEach(storeItemImagesRepository::save);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean saveImageMain(Long itemId, String imageUrl) {
        StoreItem item = storeItemsRepository.findOne(itemId);
        item.setImage(imageUrl);
        storeItemsRepository.save(item);
        return true;
    }

    public StoreItemDto getFullDto(StoreItem item, String country) {

        final StoreItemDto storeItemDto = getSimpleDto(item, country);
        final List<AttributeItemDto> attributes = new ArrayList<>(item.getAttributes().size());
        item.getAttributes().forEach(attribute -> {
            final AttributeItemDto dto = new AttributeItemDto();
            dto.setName(attribute.getName());
            dto.setValue(attribute.getValue());
            attributes.add(dto);
        });
        final List<OptionItemDto> options = new ArrayList<>(item.getOptions().size());
        item.getOptions().forEach(option -> {
            final OptionItemDto dto = new OptionItemDto();
            dto.setName(option.getName());
            dto.setValue(option.getValue());
            options.add(dto);
        });

        final List<String> images = new ArrayList<>(item.getImages().size());
        item.getImages().forEach(image -> images.add(image.getImage()));
        storeItemDto.setImages(images);
        storeItemDto.setAttributes(attributes);
        storeItemDto.setOptions(options);

        return storeItemDto;
    }

    public StoreItemDto getSimpleDto(StoreItem item, String country) {

        final StoreItemDto storeItemDto = new StoreItemDto();
        storeItemDto.setId(item.getId());
        storeItemDto.setCategory(item.getCategory());
        storeItemDto.setCode(item.getCode());
        storeItemDto.setImage(item.getImage());
        storeItemDto.setRating(item.getRating());
        storeItemDto.setCategoryName(item.getCategoryName());
        storeItemDto.setShippingScore(item.getShippingScore());
        storeItemDto.setInstallment(item.isInstallments());
        storeItemDto.setInstallmentCount(item.getInstallmentsQty() != null ? item.getInstallmentsQty() : 0);

        StoreItemI18n i18nForCountry = item.getI18nForCountry(country);
        if (i18nForCountry != null) {

            double i18nPrice = i18nForCountry.getPrice() != null ? i18nForCountry.getPrice() : item.getPrice();
            storeItemDto.setPrice(countriesService.convertMoneyFromEuros(i18nPrice, country));
            storeItemDto.setTitle(i18nForCountry.getName() != null && !i18nForCountry.getName().trim().isEmpty() ? i18nForCountry.getName() : item.getName());
            storeItemDto.setDescription(i18nForCountry.getDescription() != null && !i18nForCountry.getDescription().trim().isEmpty() ? i18nForCountry.getDescription() : item.getDescription());
            storeItemDto.setPriceString(countriesService.formatMoney(storeItemDto.getPrice(), country));

        } else {
            storeItemDto.setPrice(countriesService.convertMoneyFromEuros(item.getPrice(), country));
            storeItemDto.setTitle(item.getName());
            storeItemDto.setDescription(item.getDescription());
            storeItemDto.setPriceString(countriesService.formatMoney(item.getPrice(), country));
        }

        return storeItemDto;
    }
}
