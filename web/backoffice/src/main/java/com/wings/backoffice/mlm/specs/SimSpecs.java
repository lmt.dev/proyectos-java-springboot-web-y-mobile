/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.backoffice.mlm.specs;

import com.wings.backoffice.mlm.domain.Sim;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author lucas
 */
public class SimSpecs {

    public static Specification<Sim> simSearch(String q) {

        return (Root<Sim> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> 
        {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) 
            {
                //if it's numeric, serach only by id
                try 
                {
                    Long id = Long.parseLong(q);
                    if (id < 100000) 
                    {
                        predicates.add(builder.equal(root.get("id"), id));
                        return builder.and(predicates.toArray(new Predicate[]{}));
                    }
                }
                catch (Exception e) 
                {
                    
                }

                String search = "%" + q + "%";
                predicates.add(builder.like(root.get("iccid"), search));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
