$(document).ready(function ($) {


    $('#saveRank').click(function () {

        var notif = notifyAndWait("Creando solicitud, espere por favor...");
        $.post($('#request-form').attr('action'), $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText, "alert-error");
        });
        
    });
    
});

