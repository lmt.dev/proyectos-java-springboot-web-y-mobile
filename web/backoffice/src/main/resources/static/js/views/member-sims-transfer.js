$(document).ready(function () {

    $('#search-member').click(function (e) {
        e.preventDefault();
        search();
    });

    $('#recipient').on('blur', function (e) {
        search();
    });

    $('#transfer-button').click(function (e) {

        e.preventDefault();

        var recipient = $('#recipient').val();
        var recipientName = $('#recipientName').val();
        var simsCant = $('#simsCant').val();
        var simsIds = $('#simsIds').val();

        var data = {};
        data.recipient = recipient;
        data.simsIds = simsIds;

        confirmDialogDanger('Transferir sims', 'Confirma la transferencia de ' + simsCant + ' sims a ' + recipientName, function (result) {
            if (result) {
                var notif = notifyAndWait("Efectuando transferencia, espere por favor...");
                $.post('/members/tools/sims-transfer', data, function (response) {
                    notif.remove();
                    notifySuccess(response);
                    location.reload();
                }).fail(function (xhr, status, error) {
                    $('.bootbox.modal').modal('hide');
                    notif.remove();
                    notifyError(xhr.responseText);
                });
            } else {
                $('.bootbox.modal').modal('hide');
            }
        });

    });

    function search() {
        var recipient = $('#recipient').val();
        var data = {};
        data.recipient = recipient;

        $('#memberName').hide();

        $.post('/members/financial/transfers/search/', data, function (response) {
            $('#memberName').show();
            $('#memberName').html(response);
            $('#recipientName').val(response);
            $('#transfer-button').removeAttr('disabled');
        }).fail(function (response) {
            $('#memberName').show();
            $('#memberName').html(response.responseText);
            $('#transfer-button').attr('disabled', 'disabled');
        });
    }
});
