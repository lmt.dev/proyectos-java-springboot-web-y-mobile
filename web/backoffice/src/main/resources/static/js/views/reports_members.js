$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'month', $('#month').val());
        url = addParam(url, 'year', $('#year').val());
        url = addParam(url, 'from', $("#from").val());
        url = addParam(url, 'to', $("#to").val());
        setUpUrl(url);
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var country = $(this).data('country');
        var year = $(this).data('year') ;
        var month = $(this).data('month');
        var smonth = $(this).data('smonth');
        var action = $(this).data('action');
        var from = $(this).data('from');
        var to = $(this).data('to');

        var title = '';
        if(from != null && from != undefined){
            var sfrom = from.split('-');
            var sto = to.split('-');
            title = 'Detalle de Afiliados ' + country + ' ' + sfrom[2] + '/' + sfrom[1] + '/' + sfrom[0] + ' a ' + sto[2] + '/' + sto[1] + '/' + sto[0];
        }else{
            title = 'Detalle de Afiliados ' + country + ' ' + year + ' ' + smonth;
        }
        var url = 'admin/reports/members/viewDetails?country=' + country + "&year=" + year + "&month=" + month + "&action=" + action + "&from=" + from + "&to=" + to;
        loadInDialog(url, title, true, true);
    });
    
    $('.export').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        var country = $("#country").val();
        var month = $("#month").val();
        var year = $("#year").val();
        var from = $("#from").val();
        var to = $("#to").val();
        

        var url = "admin/reports/export/members?country=" + country + "&year=" + year + "&month=" + month + "&from=" + from + "&to=" + to;
        var win = window.open(url, '_blank');
    });
});