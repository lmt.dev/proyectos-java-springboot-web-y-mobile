$(document).ready(function () {

    $('.delete-button').click(function (e) {
        var title = 'Confirmar eliminación';
        var id = $(e.target).data('id');

        var message = '¿Desea eliminar la orden?';

        confirmDialogDanger(title, message, function (result) {
            if (result) {
                $.post('members/order/delete/' + id, null, function (response) {
                    notifySuccess(response);
                }).fail(function (response) {
                    notifyError(response.responseText);
                });
            }
        });
    });

});
