$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });


    $('.action-button').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');
        switch (action) {
            case 'add-topup':
                var title = 'Añadir recarga';
                var url = "/admin/store/addtopup";
                loadInDialog(url, title, false, false);
                break;
            case 'download-files':
                var notif = notifyAndWait("Descargando, espere por favor...");
                $.post('/admin/store/topupprovider', null, function (response) {
                    notif.remove();
                    notifySuccess(response);
                    location.reload();
                }).error(function (response) {
                    notifyError(response.responseText);
                });
                break;
        }
    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });


});
