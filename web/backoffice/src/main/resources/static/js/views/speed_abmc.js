$(document).ready(function ($) {

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'save') {
            e.preventDefault();

            var url = 'admin/tools/speed/edit/';
            var notif = notifyAndWait("Creando solicitud, espere por favor...");

            $.post(url, $('#edit-speed-form').serialize(), function (response) {

                notif.remove();
                notifySuccess(response);
                
                $('.bootbox.modal').modal('hide');
                setTimeout(function () {
                    location.reload();
                }, 500);

            }).fail(function (xhr, status, error) {
                notif.remove();
                showAlert(xhr.responseText, "alert-error");
            });
        }
    });
});

