$(document).ready(function ($) {

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'asign-speed') {

            notify("Asignando speed bonus, espere por favor");
            $.post('/admin/tools/speed/asign', null, function (response) {
                notifySuccess(response);
            }).error(function (response) {
                notifyError(response.responseText);
            });

        } else if (action === 'edit-speed') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var url = "/admin/tools/speed/edit/" + id;
            loadInDialog(url, "Editar speed bonus " + name, true, false);

        }
    });
});

