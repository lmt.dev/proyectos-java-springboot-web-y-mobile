$(document).ready(function ($) {


    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'qualify', $('#qualify').val());
        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

// Progressbar
    if ($(".progress .progress-bar")[0]) {
        $('.progress .progress-bar').progressbar();
    }

    $('.action-button').click(function (e) {
        e.preventDefault();

        var action = $(e.target).attr('id');

        if (action === 'create-latam-report') {
            var notif = notifyAndWait("Generando reporte, espere por favor...");
            $.post('/admin/reports/promoLatam/', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
        } else if (action === 'promo-ticket-report') {
            var notif = notifyAndWait("Generando reporte, espere por favor...");
            $.post('/admin/reports/promoLatamTickets', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
        } else if (action === 'promo-cena') {
            var notif = notifyAndWait("Generando reporte, espere por favor...");
            $.post('/admin/reports/promoLatamCena', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
        }
    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });
    
    $('.export').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        var country = $("#country").val();
        var qualify = $("#qualify").val();        

        var url = "admin/reports/export/promoLatam?country=" + country + "&qualify=" + qualify;
        var win = window.open(url, '_blank');
    });

});
