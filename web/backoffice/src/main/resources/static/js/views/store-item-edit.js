$(document).ready(function () {

    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A', size: 'small'
            });
        });
    }

    $("a.tab-menu").click(function (e) {
        $(".active").removeClass("active");
        var action = "#" + $(this).data("id");
        $(action).addClass("in");
        $(action).addClass("active");
    });

    $('#submit-detail').click(function (e) {
        e.preventDefault();
        var id = $('#item-id').val();
        var url = "/admin/store/item/edit/" + id;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post(url, $('#form-item-detail').serialize(), function (response) {
            notif.remove();
            notifySuccess(response);
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    $('.main-image').click(function (e) {
        e.preventDefault();
        var imageId = $(this).data("id");
        var itemId = $('#item-id').val();
        var url = "/admin/store/item/edit/images/main/" + itemId;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post(url, {imageUrl: imageId, storeItem: itemId}, function (response) {
            notif.remove();
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    $('#submit-child').click(function (e) {
        e.preventDefault();
        var id = $('#item-id').val();
        var url = "/admin/store/item/edit/" + id;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post(url, $('#form-item-children').serialize(), function (response) {
            notif.remove();
            //notifySuccess(response);

            var row = '<span>' + $('#childId option:selected').text() + '</span>&nbsp;';
            row += '<strong>(<span>' + $('#quantity').val() + '</span>)</strong>';
            $('#child-container').append(row);

        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });


    $('.remove-child').click(function (e) {
        e.preventDefault();

        var id = $('#item-id').val();
        var childId = $(this).data("id");
        var url = "/admin/store/item/edit/" + id;

        var data = {};
        data.action = 'children-remove';
        data.childId = childId;

        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post(url, data, function (response) {
            notif.remove();
            notifySuccess(response);
            $('#child-id-' + childId).remove();
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    $(document).on('click', '.fa-remove', function (event) {
        event.preventDefault();
        $(this).closest('tr').remove();
    });

    $(document).on('click', '.fa-pencil', function (event) {
        event.preventDefault();
        var action = this.getAttribute("name");

        if (action.includes("attribute")) {

            var id = $(this).parent().parent().find("td").eq(0).html();
            var name = $(this).parent().parent().find("td").eq(1).html();
            var value = $(this).parent().parent().find("td").eq(2).html();
            $("#attributeId").val(id);
            $("#name").val(name);
            $("#value").val(value);

        } else if (action.includes("children")) {

            var id = $(this).parent().parent().find("td").eq(1).html();
            var quantity = $(this).parent().parent().find("td").eq(3).html();
            $("#childId").val(id);
            $("#quantity").val(quantity);

        } else if (action.includes("country")) {

            var id = $(this).parent().parent().find("td").eq(0).html();
            var country = $(this).parent().parent().find("td").eq(1).html();
            var product = $(this).parent().parent().find("td").eq(3).html();
            var description = $(this).parent().parent().find("td").eq(4).html();
            var priceLocal = $(this).parent().parent().find("td").eq(5).html();
            var price = $(this).parent().parent().find("td").eq(6).html();

            $("#countryId").val(id);
            $("#countryCode").val(country);
            $("#childId").val(country);
            $("#productCountryName").val(product);
            $("#productCountryDesc").val(description);
            $("#productCountryPriceLocal").val(priceLocal);
            $("#productCountryPrice").val(price);

        } else if (action.includes("image")) {

            var id = $(this).parent().parent().find("td").eq(0).html();
            var url = $(this).parent().parent().find("td").eq(1).html();
            var name = $(this).parent().parent().find("td").eq(3).html();

            $("#imageId").val(id);
            $("#imageUrl").val(url);
            $("#imageName").val(name);
        }
    });

    $('#add-attribute').click(function (e) {
        e.preventDefault();
        if (!validateEntry()) {
            return false;
        }
        var id = $("#attributeId").val();
        var name = $("#name").val();
        var value = $("#value").val();
        if (id == null || id == "") {
            var rowId = $("#rowAttributeId").val();
            var row = "<tr><td hidden='hidden'>" + rowId + "</td>";
            row += "<td>" + name + "</td>";
            row += "<td>" + value + "</td>";
            row += "<td><i class='fa fa-remove action-button' name='delete-attribute' title='Eliminar'></i> ";
            row += "<i class='fa fa-pencil action-button' name='edit-attribute' title='Editar'></i></td></tr>";
            $("#table-attribute").append(row);
            $("#rowAttributeId").val(rowId - 1);
        } else {
            $("#table-attribute tr").each(function (index) {
                var cellId = $(this).find("td").eq(0).html();
                if (cellId == id) {
                    $(this).find("td").eq(1).html(name);
                    $(this).find("td").eq(2).html(value);
                }
            });
        }
        $("#attributeId").val("");
        $("#name").val("");
        $("#value").val("");
    });

    $('#submit-attributes').click(function (e) {
        e.preventDefault();
        var data = [];
        var itemId = $('#item-id').val();
        var url = "/admin/store/item/edit/attributes/" + itemId;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");

        $("#table-attribute tr").each(function (index) {
            if (index > 0) {
                var id = $(this).find("td").eq(0).html();
                if (id < 0) {
                    id = null;
                }
                var name = $(this).find("td").eq(1).html();
                var value = $(this).find("td").eq(2).html();

                var obj = {id: id, name: name, value: value, storeItemId: itemId};
                data.push(obj);
            }
        });
        var attributes = JSON.stringify(data);
        $.post(url, {attributes: attributes}, function (response) {
            notif.remove();
            $("#attributes-error").text(response);
            $("#attributes-error").show();
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    function validateEntry() {
        var name = $("#name").val();
        var value = $("#value").val();
        if (name != "" && value != "") {
            return true;
        }

        return false;
    }

    function validateChildrenEntry() {
        var id = $("#childId").val();
        var value = $("#quantity").val();
        if (id > 0 && value != null && value != "") {
            return true;
        }

        return false;
    }

    /*
     * Métodos para pantalla de localizaciones
     */
    $('#submit-countries').click(function (e) {
        e.preventDefault();
        var data = [];
        var itemId = $('#item-id').val();
        var url = "/admin/store/item/edit/countries/" + itemId;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");

        $("#table-countries tr").each(function (index) {
            if (index > 0) {
                var id = $(this).find("td").eq(0).html();
                if (id < 0) {
                    id = null;
                }
                var country = $(this).find("td").eq(1).html();
                var productName = $(this).find("td").eq(3).html();
                var productDesc = $(this).find("td").eq(4).html();
                var price = $(this).find("td").eq(6).html();

                var obj = {id: id, storeItemId: itemId, country: country, name: productName, description: productDesc, price: price};
                data.push(obj);
            }
        });
        var countries = JSON.stringify(data);
        $.post(url, {countries: countries}, function (response) {
            notif.remove();
            //notifySuccess(response);

        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    $('#add-country').click(function (e) {
        e.preventDefault();
        var id = $("#countryId").val();
        var code = $("#countryCode").val();
        var name = $("#productCountryName").val();
        var description = $("#productCountryDesc").val();
        var priceLocal = $("#productCountryPriceLocal").val();
        var price = $("#productCountryPrice").val();

        var validate = validateCountryExist(id, code);

        if (!validate) {
            return;
        }

        if (id == null || id == "") {
            var rowId = $("#rowCountryId").val();
            var row = "<tr><td hidden='hidden'>" + rowId + "</td>";
            row += "<td hidden='hidden'>" + code + "</td>";
            row += "<td><i class='flag flag-" + code.toLowerCase() + "'></i><span>" + code + "</span></td>";
            row += "<td>" + name + "</td>";
            row += "<td>" + description + "</td>";
            row += "<td>" + priceLocal + "</td>";
            row += "<td>" + price + "</td>";
            row += "<td><i class='fa fa-remove action-button' name='delete-country' title='Eliminar'></i> ";
            row += "<i class='fa fa-pencil action-button' name='edit-country' title='Editar'></i></td></tr>";
            $("#table-countries").append(row);
            $("#rowCountryId").val(rowId - 1);
        } else {
            $("#table-countries tr").each(function (index) {
                var cellId = $(this).find("td").eq(0).html();
                if (cellId == id) {
                    $(this).find("td").eq(3).html(name);
                    $(this).find("td").eq(4).html(description);
                    $(this).find("td").eq(5).html(priceLocal);
                    $(this).find("td").eq(6).html(price);
                }
            });
        }
        cleanCountryForm();
    });

    $("#reset-country").click(function () {
        cleanCountryForm();
    });

    function cleanCountryForm() {
        $("#countryId").val("");
        $("#countryCode").val(0);
        $("#productCountryName").val("");
        $("#productCountryDesc").val("");
        $("#productCountryPriceLocal").val("");
        $("#productCountryPrice").val("");
    }

    function validateCountryExist(id, code) {
        var exist = true;
        if (id == null || id == "") {
            $("#table-countries tr").each(function (index) {
                var countryCode = $(this).find("td").eq(1).html();
                if (countryCode == code) {
                    exist = false;
                }
            });
        } else {
            $("#table-countries tr").each(function (index) {
                var rowId = $(this).find("td").eq(0).html();
                var countryCode = $(this).find("td").eq(1).html();
                if (countryCode == code && rowId != id) {
                    exist = false;
                }
            });
        }

        return exist;
    }

    /*
     * Métodos para pantalla de imagenes
     */
    $('#submit-images').click(function (e) {
        e.preventDefault();
        var data = [];
        var itemId = $('#item-id').val();
        var url = "/admin/store/item/edit/images/" + itemId;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");

        $("#table-images tr").each(function (index) {
            if (index > 0) {
                var id = $(this).find("td").eq(0).html();
                if (id < 0) {
                    id = null;
                }
                var image = $(this).find("td").eq(1).html();
                var name = $(this).find("td").eq(3).html();

                var obj = {id: id, storeItemId: itemId, image: image, name: name};
                data.push(obj);
            }
        });
        var images = JSON.stringify(data);
        $.post(url, {images: images}, function (response) {
            notif.remove();
            //notifySuccess(response);

        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });

    $('#add-image').click(function (e) {
        e.preventDefault();
        var id = $("#imageId").val();
        var url = $("#imageUrl").val();
        var name = $("#imageName").val();

        var validate = validateImage();

        if (!validate) {
            return;
        }

        if (id == null || id == "") {
            var rowId = $("#rowImageId").val();
            var row = "<tr><td hidden='hidden'>" + rowId + "</td>";
            row += "<td hidden='hidden'>" + url + "</td>";
            row += "<td><img class='avatar-view' src='" + url + "' onerror='/img/no_logo.png' height='100px' width='100px'/></td>";
            row += "<td>" + name + "</td>";
            row += "<td></td>";
            row += "<td><i class='fa fa-remove action-button' name='delete-image' title='Eliminar'></i> ";
            row += "<i class='fa fa-pencil action-button' name='edit-image' title='Editar'></i></td></tr>";
            $("#table-images").append(row);
            $("#rowImageId").val(rowId - 1);
        } else {
            $("#table-images tr").each(function (index) {
                var cellId = $(this).find("td").eq(0).html();
                if (cellId == id) {
                    var img = "<img class='avatar-view' src='" + url + "' onerror='this.src='/img/no_logo.png';' height='100px' width='100px'/>";
                    $(this).find("td").eq(1).html(url);
                    $(this).find("td").eq(2).html(img);
                    $(this).find("td").eq(3).html(name);
                }
            });
        }
        cleanImageForm();
    });

    $("#reset-image").click(function () {
        cleanImageForm();
    });

    function cleanImageForm() {
        $("#imageId").val("");
        $("#imageUrl").val("");
        $("#imageName").val("");
    }

    function validateImage() {
        var exist = true;

        return exist;
    }

    function childrenExist(id) {
        var exist = false;
        $("#table-childrens tr").each(function (index) {
            var cellId = $(this).find("td").eq(1).html();
            if (cellId == id) {
                exist = true;
            }
        });

        return exist;
    }

    $('#add-children').click(function (e) {
        e.preventDefault();
        if (!validateChildrenEntry()) {
            return false;
        }
        var childId = $("#childId").val();
        var id = $("#idChild").val();
        var nombre = $('#childId option:selected').text();
        var quantity = $("#quantity").val();
        if (!childrenExist(childId)) {
            var row = "<tr><td hidden='hidden'>" + id + "</td><td hidden='hidden'>" + childId + "</td>";

            row += "<td>" + nombre + "</td>";
            row += "<td>" + quantity + "</td>";
            row += "<td><i class='fa fa-remove action-button' name='delete-children' title='Eliminar'></i> ";
            row += "<i class='fa fa-pencil action-button' name='edit-children' title='Editar'></i></td></tr>";
            $("#table-childrens").append(row);
        } else {
            $("#table-childrens tr").each(function (index) {
                var cellId = $(this).find("td").eq(1).html();
                if (cellId == childId) {
                    $(this).find("td").eq(2).html(nombre);
                    $(this).find("td").eq(3).html(quantity);
                }
            });
        }
        $("#childId").val(0);
        $("#quantity").val("");
    });

    $('#submit-childrens').click(function (e) {
        e.preventDefault();
        var data = [];
        var itemId = $('#item-id').val();
        var url = "/admin/store/item/edit/childrens/" + itemId;
        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $("#table-childrens tr").each(function (index) {
            if (index > 0) {
                var id = $(this).find("td").eq(0).html();
                if (id < 0) {
                    id = null;
                }

                var childId = $(this).find("td").eq(1).html();
                var quantity = $(this).find("td").eq(3).html();
                var obj = {id: id, childId: childId, quantity: quantity, parentId: itemId};
                data.push(obj);
            }
        });
        var childrens = JSON.stringify(data);
        $.post(url, {childrens: childrens}, function (response) {
            notif.remove();
            $("#childrens-error").text(response);
            $("#childrens-error").show();
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });
    });
});
