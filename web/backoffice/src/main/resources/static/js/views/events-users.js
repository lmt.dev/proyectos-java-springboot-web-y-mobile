$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'add-user') {
        
            loadInDialog("admin/events/users/add", "Añadir usuario", false, false);

        } else if (action === 'delete-user') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Eliminar usuario', '¿Esta seguro eliminar el usuario?', function (result) {
                if (result) {
                    var notif = notifyAndWait("Eliminando, espere por favor...");
                    $.post('/admin/events/users/delete/' + id, null, function (response) {
                        notif.remove();
                    }).error(function (response) {
                        notifyError(response.responseText);
                    });
                }
            });
        }
    });





});

