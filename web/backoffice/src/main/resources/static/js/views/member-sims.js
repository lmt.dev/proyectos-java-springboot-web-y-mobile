$(document).ready(function ($) {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            buscar();
        }
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        buscar();
    });

    function buscar() {
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        //url = addParam(url, 'from', $("#from").val());
        //url = addParam(url, 'to', $("#to").val());
        setUpUrl(url);
    }

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');
        if (action === 'add-sim') {
            loadInDialog("members/tools/sims-add", 'Añadir sims', false, false);
        } else if (action === 'transfer-sim') {
            var ids = getCheckedItems();
            if (ids.length === 0) {
                notifyError('Debe seleccionar al menos una sim');
            } else {
                loadInDialog("members/tools/sims-transfer?sims=" + ids, 'Transferir sims', false, false);
            }
        }
    });

});
