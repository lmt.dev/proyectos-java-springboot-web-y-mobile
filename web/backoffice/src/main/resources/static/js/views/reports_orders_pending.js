$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'currency', $('#currency').val());
        url = addParam(url, 'year', $('#year').val());
        setUpUrl(url);
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var country = $(this).data('country');
        var year = $(this).data('year');
        var currency = $(this).data('currency');
        var itemId = $(this).data('item');

        var title = 'Detalle de Ordenes ' + country + ' ' + year;

        var url = 'admin/reports/orders/viewPendingDetails?country=' + country + "&year=" + year + "&currency=" + currency + "&itemId=" + itemId;

        loadInDialog(url, title, true, true);
    });

    $('.export').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        var country = $("#country").val();
        var year = $("#year").val();
        var item = $("#itemId").val();
        var currency = $("#currency").val();

        if (action === 'export-detail') {
            country = $("#countryDetail").val();
            year = $("#yearDetail").val();
            item = $("#itemIdDetail").val();
            currency = $("#currencyDetail").val();
        }
        
        if (!item) {
            item = null;
        }

        var url = "admin/reports/export/orderPending?country=" + country + "&year=" + year + "&currency=" + currency + "&itemId=" + item;
        var win = window.open(url, '_blank');
    });
});