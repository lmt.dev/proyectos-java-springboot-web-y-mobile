$(document).ready(function () {


    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');
        if (action === 'delete-pool') {
            var id = $(e.target).data('id');
            confirmDialogDanger('Eliminar bloque de asientos', 'Confirma eliminacion Bloque', function (result) {
                if (result) {
                    $.post('/admin/events/seats/delete/' + id, null, function (response) {
                        notifySuccess(response);
                        $('#pool-' + id).remove();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        }
    });



    $('.btne-save').click(function (e) {
        e.preventDefault();

        var id = $(e.target).data('id');
        var section = $(e.target).data('section');
        var data = new FormData($('#form-' + section)[0]);

        $.ajax({
            type: 'POST',
            url: '/admin/events/edit/' + section + '/' + id,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respData) {
                notifySuccess(respData);
                processPostEdit(section, data, id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                notifyError(xhr.responseText);
            }
        });
    });


    function processPostEdit(section, data, id) {
        switch (section) {
            case 'seats':
                var d = $('#form-' + section).serializeArray();
                if (!d[3]) {
                    d[3] = {};
                    d[3].value = 'off';
                }

                var toAppend = '<tr>' +
                        '<td>' + (d[0] !== null ? d[0].value : '&nbsp;') + '</td>' +
                        '<td>' + (d[1] !== null ? d[1].value : '&nbsp;') + '</td>' +
                        '<td>' + (d[2] !== null ? d[2].value : '&nbsp;') + '</td>' +
                        '<td>' + (d[3] !== null ? (d[3].value === 'on') : '&nbsp;') + '</td>' +
                        '<td>&nbsp;</td>' +
                        '</tr>';
                $('#table-pools > tbody:last-child').append(toAppend);
                $('#form-' + section).trigger('reset');
                break;
        }
    }

    $('.collapse-link').on('click', function () {
        var $BOX_PANEL = $(this).closest('.x_panel'),
                $ICON = $(this).find('i'),
                $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function () {
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });


});
