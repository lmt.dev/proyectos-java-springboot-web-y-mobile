$(document).ready(function ($) {

    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A', size: 'small'
            });
        });
    }

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'status', $("#status").val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });


    $('.action-button').click(function (e) {

        e.preventDefault();

        //var button = $(e.target).attr('id');
        var button = $(e.target).parent().attr('id');
        var code = $(e.target).parent().data('code');

        switch (button) {
            case 'enable-country':

                var nombre = $(e.target).text();
                var title = 'Confirmar';
                var message = nombre == 'Habilitar' ? "¿Está seguro de habilitar al país?" : "¿Está seguro de deshabilitar al país?";

                confirmDialogDanger(title, message, function (result) {
                    if (result) {
                        $.post('/admin/settings/countries/enabled/' + code, null, function (response) {
                            notifySuccess(response);
                            $("#search-button").click();
                        }).fail(function (response) {
                            notifyError(response.responseText);
                        });
                    }
                });
                break;
            case 'get-edit-country':

                var url = '/admin/settings/countries/edit/' + code;
                $('#form-properties').trigger("reset");
                $.get(url, null, function (response) {
                    populate($('#form-properties'), response);
                }).fail(function (xhr, status, error) {
                    notifyError("Error o no habilitado!");
                });
                break;

            case 'post-edit-country':

                var url = '/admin/settings/countries/edit';
                var data = $('#form-properties').serialize();

                $.post(url, data, function (response) {
                    notifySuccess(response);
                }).fail(function (response) {
                    notifyError(response.responseText);
                });

                break;

            case 'view-country':
                var action = button.includes("edit") ? "edit" : "view";
                var url = '/admin#admin/settings/countries/create';
                url = addParam(url, 'action', action);
                url = addParam(url, 'code', code);
                setUpUrl(url);
                break;
        }

    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });


    /**
     * carga el form de acuerdo al response del get
     * @param {type} frm
     * @param {type} data
     * @return {undefined}
     */
    function populate(frm, data) {
        $.each(data, function (key, value) {
            var ctrl = $('[name=' + key + ']', frm);
            switch (ctrl.prop("type")) {
                case "radio":
                case "checkbox":
                    ctrl.each(function () {
                        $(this).attr("checked", value);
                    });
                    break;
                default:
                    ctrl.val(value);
            }
        });
    }


});
