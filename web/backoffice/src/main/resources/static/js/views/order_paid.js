$(document).ready(function ($) {

    $(document.getElementById('time')).on('focus', function () {
        new InputMask().Initialize(document.querySelectorAll('#time'), {
            mask: InputMaskDefaultMask.Time,
            placeHolder: 'HH:mm:ss'
        });
    });

    $('.action-button').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        if (id === 'pending' || id === 'save') {

            var orderId = $('#orderId').val();
            var url = '/admin/orders/paid/' + orderId + '/' + id;

            var title = 'Confirma Pagado';
            var message = '¿Marcar la orden como pagada?';
            if (id === 'pending') {
                title = 'Confirma elimnar revisión';
                message = '¿Marcar la orden como pendiente?';
                message += '<br/><br/><h4>Atención!</h4>Esto vuelve el estado a pendiemte y elimina la fecha de activación!';
            }

            confirmDialogDanger(title, message, function (result) {
                if (result) {
                    var notif = notifyAndWait('Guardando, espere por favor...');
                    $.post(url, $('#paid-order-form').serialize(), function (response) {
                        notif.remove();
                        notifySuccess(response);
                        $('#search-button').click();
                        $('.bootbox.modal').modal('hide');
                    }).fail(function (response) {
                        notif.remove();
                        notifyError(response.responseText);
                    });
                }else{
                    $('.bootbox.modal').modal('hide');                    
                }
            });

        }
    });


});
