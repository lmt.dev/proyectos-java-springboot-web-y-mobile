
$(document).ready(function () {

    $('.action-button').click(function (e) {

        var action = $(this).attr('id');
        var data = {};

        if (action === 'reject') {
            data.status = 'rejected';
        } else if (action === 'verify') {
            data.status = 'verified';
        } else {
            return;
        }

        var id = $(this).data('id');
        var type = $(this).data('type');
        var bacsId = $("#bacsId").val();

        data.message = $('input[data-type="' + type + '"]').val();
        data.id = id;
        data.type = type;

        if (bacsId) {
            data.bacsId = bacsId;
        }

        var url = '/admin/financial/verification/';

        var notif = notifyAndWait("Ejecutando, espere por favor...");
        $.post(url, data, function (response) {
            notif.remove();
            notifySuccess(response);
        }).fail(function (xhr, status, error) {
            notif.remove();
            notifyError(xhr.responseText);
        });

    });


});