$(document).ready(function ($) {

    $('#save').click(function (e) {

        e.preventDefault();

        var id = $('#shipmentId').val();
        var url = 'admin/orders/shipments/edit-dest/' + id;

        var notif = notifyAndWait('Guardando, espere por favor...');
        $.post(url, $('#edit-dest-form').serialize(), function (response) {
            notif.remove();
            notifySuccess(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (response) {
            notif.remove();
            notifyError(response.responseText);
        });
    });



});
