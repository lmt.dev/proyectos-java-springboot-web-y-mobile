
$(document).ready(function () {
    $('#status').change(function () {
        $('#search-form').submit();
    });

    $('#generate-invoices').click(function () {
        $.post('/admin/financial/withdrawal-requests-invoices', null, function (status) {
            if (status === 'success') {
                location.reload();
            } else {
                alert('Ocurrió un error! - culpa de alban!!!!!');
            }
        });
    });


    $('#viewModal').on('shown.bs.modal', function (e) {
        var url = $(e.relatedTarget).data('url');
        $(this).find('.modal-body').load(url);
    });

    $('#viewModal').on('hidden.bs.modal', function (e) {
        $('#viewModal').find('.modal-body').empty();
        $(this).find('.modal-body').html('<div class="text-center"><i class="fa fa-circle-o-notch fa-lg mt-4 fa-spin"></i></div>');
    });


    $('.action-reject').click(function (e) {
        var id = $(this).attr('id')
        bootbox.confirm("¿Esta seguro de marcar el retiro como rechazado?", function (result) {
            if (result) {
                markWithdrawal(2, id);
            }
        });
    });

    $('.action-complete').click(function (e) {
        var id = $(this).attr('id')
        bootbox.confirm("¿Marcar el retiro como Completo?", function (result) {
            if (result) {
                markWithdrawal(1, id);
            }
        });
    });

    $('.action-transfered').click(function (e) {
        var id = $(this).attr('id')
        bootbox.confirm("¿Cambiar el estado a transferido?", function (result) {
            if (result) {
                markWithdrawal(3, id);
            }
        });
    });


    function markWithdrawal(status, id) {
        var url = '/admin/financial/withdrawal/mark/' + status + '/' + id;
        $.post(url, null, function (result) {
            if (result === 'success') {
                bootbox.alert("Exito");
            } else {
                bootbox.alert("Ocurrio un error " + result);
            }
        });
    }


});