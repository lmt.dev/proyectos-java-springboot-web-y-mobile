
$(document).ready(function ($) {

    $('#save').click(function (e) {

        e.preventDefault();

        var id = $('#orderId').val();
        var url = '/members/store/packs/redeem/' + id;
        
        showAlertIcon('Aplicando el paquete, espere por favor...', "alert-info");
        
        $.post(url, $('#redeem-pack-form').serialize(), function (response) {
            $('.bootbox.modal').modal('hide');
            location.reload();
        }).fail(function (response) {
            showAlert(response.responseText, "alert-error");
        });
    });

});
