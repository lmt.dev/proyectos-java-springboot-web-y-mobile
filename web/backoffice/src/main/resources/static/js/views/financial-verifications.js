
$(document).ready(function () {


    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'status', $('#status').val());
        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {
        var action = $(this).attr('id');
        if (action === 'edit-verification') {

            var id = $(this).data('id');
            var bacsId = $(this).data('bacsid');

            var url = '/admin/financial/verification/' + id;
            if (bacsId) {
                url = '/admin/financial/verification/' + id + "/" + bacsId;
            }
            var title = 'Verificar documentacion afiliado ' + id;
            var dialog = loadInDialog(url, title, true, true);

            dialog.on('hidden.bs.modal', function (e) {
                location.reload();
            });

        }
    });
});