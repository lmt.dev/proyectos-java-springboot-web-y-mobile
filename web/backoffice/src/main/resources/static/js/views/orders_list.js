$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'payment', $('#payment').val());
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'deleted', $("#deleted").is(':checked'));
        url = addParam(url, 'activation', $("#activation").is(':checked'));
        url = addParam(url, 'from', $("#from").val());
        url = addParam(url, 'to', $("#to").val());
        url = addParam(url, 'status', $("#status").val());
        url = addParam(url, 'ticketNumber', $("#ticketNumber").val());
        url = addParam(url, 'bac', $("#bac").val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {

            case 'invoice-generate':

                $.post('/admin/orders/invoice/generate', null, function (response) {
                    notify(response);
                }).error(function (response) {
                    notify(response.responseText);
                });
                break;

            case 'invoice-send':

                var id = $(e.target).data('id');
                $.post('/admin/orders/invoice/send/' + id, null, function (response) {
                    notify(response);
                }).error(function (response) {
                    notify(response.responseText);
                });
                break;

            case 'sync-members':

                $.post('admin/members/sync/from-store', null, function (response) {
                    notify(response);
                }).error(function (response) {
                    notify(response.responseText);
                });
                break;

            case 'delete-order':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');

                var title = 'Confirmar eliminación';
                var message = '¿Marcar la orden de <b>' + name + '</b> (' + id + ') como eliminada ?';
                message += '<br/><br/><h4>Atención!</h4>Si la orden está pagada, se elimina el pago. No se puede recuperar!.';

                confirmDialogDanger(title, message, function (result) {
                    if (result) {
                        $.post('/admin/orders/delete/' + id, null, function (response) {
                            notifySuccess(response);
                        }).fail(function (response) {
                            notifyError(response.responseText);
                        });
                    }
                });
                break;

            case 'undelete-order':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');

                var title = 'Confirmar remover eliminación';
                var message = '¿Marcar la orden de <b>' + name + '</b> (' + id + ') como NO eliminada ?';
                confirmDialogDanger(title, message, function (result) {
                    if (result) {
                        $.post('/admin/orders/undelete/' + id, null, function (response) {
                            notifySuccess(response);
                        }).fail(function (response) {
                            notifyError(response.responseText);
                        });
                    }
                });
                break;

            case 'edit-order':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');
                var title = 'Editar pedido ' + name;
                var url = "/admin/order/edit/" + id;
                loadInDialog(url, title, false, false);
                break;

            case 'mark-review':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');
                var title = 'Marcar orden para revisar';
                var message = '¿Confirma revisión de <b>' + name + '</b> (' + id + ')?';
                var url = "/admin/orders/review/" + id;
                loadInDialog(url, title, false, false);

                break;

            case 'mark-paid':

                var id = $(e.target).data('id');
                var name = $(e.target).data('name');
                var title = 'Marcar orden pagada';
                var message = '¿Confirma pago de <b>' + name + '</b> (' + id + ')?';
                var url = "/admin/orders/paid/" + id;
                loadInDialog(url, title, false, false);

                break;
        }

    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });


});
