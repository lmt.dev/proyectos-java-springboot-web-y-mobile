$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'auto', $('#auto').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');
        if (action === 'delete-rank') {

            var name = $(e.target).data('name');
            var id = $(e.target).data('id');
            var title = '¿Esta seguro de elimar el rango de ' + name + '?';

            confirmDialogDanger('Eliminar rango', title, function (result) {
                if (result) {
                    var notif = notifyAndWait("Eliminando, espere por favor...");
                    $.post('/admin/ranks/delete/' + id, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                        $('#search-button').click();
                    }).fail(function (response) {
                        notif.remove();
                        notifyError(response.responseText);
                    });
                }
            });
        }
    });

});
