$(document).ready(function () {

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');

        if (action === 'set-action-here') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Eliminar información adicional', '¿Confirma eliminacion información adicional de registro?', function (result) {
                if (result) {
                    $.post('' + id, null, function (response) {
                        notifySuccess(response);
                        $('#info-' + id).remove();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        }
    });


    $('.btne-save').click(function (e) {
        e.preventDefault();

        var id = $(e.target).data('id');
        var section = $(e.target).data('section');
        var data = new FormData($('#form-' + section)[0]);

        $.ajax({
            type: 'POST',
            url: '/members/settings/edit/' + section + '/' + id,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respData) {
                notifySuccess(respData);
                processPostEdit(section, data, id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                notifyError(xhr.responseText);
            }
        });
    });

    function processPostEdit(section, data, id) {
        switch (section) {
            case 'setcase':
                break;
        }
    }

});
