$(document).ready(function () {


    $('#save').click(function () {

        var id = $('#id').val();
        var notif = notifyAndWait("Creando solicitud, espere por favor...");
        $.post('/members/financial/account/edit/' + id, $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText, "alert-error");
        });
    });

});
