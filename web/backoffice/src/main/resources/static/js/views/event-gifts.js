$(document).ready(function () {


    $('.btne-save').click(function (e) {
        e.preventDefault();

        var id = $(e.target).data('id');
        var section = $(e.target).data('section');
        var data = new FormData($('#form-' + section)[0]);

        var notif = notifyAndWait("Procesando, espere por favor...");

        $.ajax({
            type: 'POST',
            url: '/admin/events/gifts/' + id,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respData) {
                notif.remove();
                notifySuccess(respData);
                processPostEdit(section, data, id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                notif.remove();
                notifyError(xhr.responseText);
            }
        });
    });


    function processPostEdit(section, data, id) {
        switch (section) {
            case 'gifts':
                $('#form-' + section).trigger('reset');
                break;
        }
    }



});

