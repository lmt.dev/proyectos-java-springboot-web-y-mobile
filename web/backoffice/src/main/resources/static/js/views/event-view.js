$(document).ready(function () {

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');

        if (action === 'publish-event') {
            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Publicar evento', 'Confirma la publicacion del evento ' + name + '. no se podran hacer cambios posteriores!', function (result) {
                if (result) {
                    $.post('/admin/events/event/publish/' + id, null, function (response) {
                        notifySuccess(response);

                        $('#event-status').removeClass("alert-warning");
                        $('#event-status').addClass("alert-success");
                        $('#event-status').html("Evento Publicado");
                        $('#publish-event').remove();

                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        } else if (action === 'finalize-event') {
            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Finalizar evento', 'Confirma la finalización del evento ' + name + '.', function (result) {
                if (result) {
                    $.post('/admin/events/event/finalize/' + id, null, function (response) {
                        notifySuccess(response);

                        $('#event-status').removeClass("alert-success");
                        $('#event-status').addClass("alert-danger");
                        $('#event-status').html("Evento Finalizado");
                        $('#finalize-event').remove();

                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        }
    });


    $('.collapse-link').on('click', function () {
        var $BOX_PANEL = $(this).closest('.x_panel'),
                $ICON = $(this).find('i'),
                $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function () {
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });


    // Progressbar
    if ($(".progress .progress-bar")[0]) {
        $('.progress .progress-bar').progressbar();
    }


});
