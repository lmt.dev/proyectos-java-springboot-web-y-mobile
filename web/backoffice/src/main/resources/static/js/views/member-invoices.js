$(document).ready(function ($) {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            buscar();
        }
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        buscar();
    });

    function buscar() {
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'type', $('#type').val());
        url = addParam(url, 'from', $("#from").val());
        url = addParam(url, 'to', $("#to").val());

        setUpUrl(url);
    }


    $('.action-button').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');
        if (action === 'view-invoice') {
            var id = $(e.target).data('id');
            var url = 'members/financial/invoices/' + id;
            window.open(url, '_blank');
        }
    });

});
