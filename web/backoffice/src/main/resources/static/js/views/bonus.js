$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'receive', $('#receive').val());
        url = addParam(url, 'offer', $('#offer').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());
        url = addParam(url, 'size', $('#size').val());

        setUpUrl(url);
    });

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('.action-button').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');


        var dataAction = $(e.target).data('action');
        if (dataAction) {
            $.get(dataAction, null, function () {
                console.log(action);
            });
        } else {

            if (action === 'view-event') {
                var id = $(e.target).data('id');
                var url = '/admin/bonus/differential/event/' + id;
                loadInDialog(url, "Detalle del evento", true);
            }
        }
    });

});
