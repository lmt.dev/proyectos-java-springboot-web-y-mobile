$(document).ready(function ($) {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            buscar();
        }
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        buscar();
    });

    function buscar() {
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'rank', $('#rank').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'country', $('#country').val());

        setUpUrl(url);
    }


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');
        if (action === 'delete-member') {

            var name = $(e.target).data('name');
            var id = $(e.target).data('id');
            var title = '¿Esta seguro de elimar el afiliado ' + name + '?';

            confirmDialogDanger('Eliminar afiliado', title, function (result) {
                if (result) {
                    notify("Eliminando, espere por favor...");
                    $.post('/admin/members/delete/' + id, null, function (response) {
                        notifySuccess(response);
                    }).error(function (response) {
                        notifyError(response.responseText);
                    });
                }
            });

        } else if (action === 'login-as-member') {

            var username = $(e.target).data('id');
            var name = $(e.target).data('name');

            var url = "/admin/member/login?username=" + username;
            window.open(url, '_blank');

        } else if (action === 'set-color') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            $.post('/admin/members/set-color/' + id, null, function (response) {
                notify(response);
            }).error(function (response) {
                notify(response.responseText);
            });
        } else if (action === 'check-updates') {

            $.post('/admin/members/check-updates', null, function (response) {
                responseModal = response;
                loadModal();
            }).error(function (response) {
                notify(response.responseText);
            });

        } else if (action === 'set-payed') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            $.post('/admin/members/set-payed/' + id, null, function (response) {
                notify(response);
            }).error(function (response) {
                notify(response.responseText);
            });

        } else if (action === 'recalculate') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var notif = notifyAndWait("Recalculando para " + name + ", espere por favor...");
            $.post('/admin/members/recalculate/' + id, null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (response) {
                notifyError(response.responseText);
            });

        } else if (action === 'view-snap') {

            var id = $(e.target).data('id');
            var title = 'Snapshot para ' + $(e.target).data('name');
            var url = '/admin/members/view-snap/' + id;
            loadInDialog(url, title, false);

        } else if (action === 'view-tree') {

            var id = $(e.target).data('id');
            var title = 'Arbol para ' + $(e.target).data('name');
            var url = '/admin/members/tree/' + id + '?simple=true';

            loadInDialog(url, title, true, true);

        } else if (action === 'edit-member') {

            var id = $(e.target).data('id');
            var referer = encodeURIComponent(window.location.pathname + window.location.hash);

            var title = 'Cambiar contraseña afiliado ' + $(e.target).data('name');
            var url = '/admin/members/reset-passwd/' + id + '?referer=' + referer;

            loadInDialog(url, title, false, false);

        } else if (action === 'rank-update') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir rango para afiliado ' + name;
            var url = 'admin/members/rank/update/' + id;

            loadInDialog(url, title, false, false);

        } else if (action === 'change-email') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Cambiar dirección de correo para afiliado ' + name;
            var url = 'admin/members/email/update/' + id;

            loadInDialog(url, title, false, false);

        } else if (action === 'change-kit') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Cambiar kit afiliación para afiliado ' + name;
            var url = 'admin/members/kit/update/' + id;

            loadInDialog(url, title, true, false);

        } else if (action === 'mark-founder') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var mark = $(e.target).data('mark');

            var title = '¿Marcar el afiliado ' + name + ' como afiliado Founder?';
            var url = '/admin/members/founder/mark/' + id;
            if (mark === 0) {
                url = '/admin/members/founder/unmark/' + id;
                title = '¿Quitar Founder del afiliado ' + name + '?';
            }

            confirmDialogDanger('Afiliado Founder', title, function (result) {
                if (result) {
                    var notif = notifyAndWait("Actualizando, espere por favor...");
                    $.post(url, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });

        } else if (action === 'upgrade-kit') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir kit upgrade para afiliado ' + name;
            var url = 'admin/members/kit/upgrade/' + id;

            loadInDialog(url, title, true, false);

        } else if (action === 'export-emails') {
            var url = "admin/members/export";
            var win = window.open(url, '_blank');

        } else if (action === 'resend-email') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = '¿Reenviar el correo al afiliado ' + name + ', esto cambiará su contraseña?';
            var url = '/admin/members/resend-email/' + id;

            confirmDialogDanger('Reenviar correo', title, function (result) {
                if (result) {
                    var notif = notifyAndWait("Actualizando, espere por favor...");
                    $.post(url, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });
        } else if (action === 'exclude-promo') {
            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var mark = $(e.target).data('promo');

            var title = '¿Excluir al afiliado ' + name + ' para futuras promociones?'; //si es uno no se tiene en cuenta para las promos futuras
            var url = '/admin/members/promo/promo/' + id;
            if (mark === 0) {
                url = '/admin/members/promo/nopromo/' + id;
                title = '¿Incluir al afiliado ' + name + ' a futuras promociones?';
            }

            confirmDialogDanger('Afiliado Promo', title, function (result) {
                if (result) {
                    var notif = notifyAndWait("Actualizando, espere por favor...");
                    $.post(url, null, function (response) {
                        notif.remove();
                        notifySuccess(response);
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });
        }
    });

    //initial rank change
    $('.initial-rank-change').click(function (e) {

        var mId = $(e.target).data('id');
        var name = $(e.target).data('name');

        bootbox.prompt({
            title: "Seleccione el rango inicial para " + name,
            inputType: 'select',
            inputOptions: [
                {
                    text: 'DEALER',
                    value: '1'
                },
                {
                    text: 'TEAM_DEALER',
                    value: '2'
                },
                {
                    text: 'DIRECTOR',
                    value: '3'
                },
                {
                    text: 'TEAM_DIRECTOR',
                    value: '4'
                },
                {
                    text: 'REGIONAL_PRESIDENT',
                    value: '5'
                },
                {
                    text: 'NATIONAL_PRESIDENT',
                    value: '6'
                },
                {
                    text: 'INTERNATIONAL_PRESIDENT',
                    value: '7'
                },
                {
                    text: 'DIAMOND_PRESIDENT',
                    value: '8'
                }
            ],
            callback: function (result) {

                if (result) {
                    var url = 'admin/members/rank/initial/update';
                    var postData = {};
                    postData.memberId = mId;
                    postData.rankId = result;

                    $.post(url, postData, function (response) {
                        new PNotify({
                            title: 'Informacion',
                            text: response,
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    });
                }
            }
        });

    });


    $('.order-change').click(function (e) {

        var mId = $(e.target).data('id');
        var name = $(e.target).data('name');

        bootbox.prompt({
            title: "Seleccione el nuevo Kit  para " + name,
            inputType: 'select',
            inputOptions: [
                {
                    value: 297,
                    text: "STARTER"
                },
                {
                    value: 299,
                    text: "STARTER PRO"
                },
                {
                    value: 302,
                    text: "GOLDEN FOUNDER"
                },
                {
                    value: 304,
                    text: "GOLDEN FOUNDER 2"
                },
                {
                    value: 306,
                    text: "GOLDEN FOUNDER 3"
                },
                {
                    value: 309,
                    text: "GOLDEN FOUNDER 4"
                },
                {
                    value: 311,
                    text: "GOLDEN FOUNDER 5"
                },
                {
                    value: 314,
                    text: "GOLDEN FOUNDER 6"
                },
                {
                    value: 317,
                    text: "GOLDEN FOUNDER 7"
                },
                {
                    value: 319,
                    text: "DIAMOND FOUNDER 1"
                },
                {
                    value: 321,
                    text: "DIAMOND FOUNDER 2"
                },
                {
                    value: 323,
                    text: "DIAMOND FOUNDER 3"
                },
                {
                    text: 'FREE',
                    value: '15'
                },
                {
                    text: 'STORE_BASE',
                    value: '50'
                },
                {
                    text: 'STORE_CORNER_LIGHT',
                    value: '51'
                },
                {
                    text: 'STORE_CORNER_FULL',
                    value: '52'
                },
                {
                    text: 'STORE_FULL',
                    value: '53'
                }
            ],
            callback: function (result) {

                if (result) {
                    var url = 'admin/members/kit/update';
                    var postData = {};
                    postData.memberId = mId;
                    postData.kitId = result;

                    $.post(url, postData, function (response) {
                        new PNotify({
                            title: 'Informacion',
                            text: response,
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    });
                }
            }
        });
    });

    $('.sponsor-change').click(function (e) {

        var mId = $(this).data('id');
        var mName = $(this).data('name');

        bootbox.prompt({
            title: "Ingrese el nuevo id de sponsor para " + mName,
            inputType: 'text',
            callback: function (result) {
                if (result) {
                    var url = 'admin/members/sponsor/update';
                    var postData = {};
                    postData.memberId = mId;
                    postData.sponsorId = result;

                    notify("Actualizando, espere por favor...");

                    $.post(url, postData, function (response) {
                        notifySuccess(response);
                    }).error(function (response) {
                        notifyError(response.responseText);
                    });
                }
            }
        });
    });


    $('.speed-change').click(function (e) {

        var mId = $(this).data('id');
        var mName = $(this).data('name');

        bootbox.prompt({
            title: "Ingrese el nuedo id de speedBonus " + mName,
            inputType: 'text',
            callback: function (result) {
                if (result) {
                    var url = 'admin/members/speed/update';
                    var postData = {};
                    postData.memberId = mId;
                    postData.speedId = result;

                    var notif = notify("Actualizando, espere por favor...");
                    $.post(url, postData, function (response) {
                        notif.remove();
                        notifySuccess(response);
                    }).error(function (response) {
                        notif.remove();
                        notifyError(response.responseText);
                    });
                }
            }
        });
    });


    $('#analize-ranks').click(function (e) {

        var uploadHtml = "<div><span>Lista de elementos separados por coma con formato: <b>nombre,apellido</b></span>" +
                "<label class='upload-area' style='width:100%;text-align:center;' for='fupload'>" +
                "<input id='fupload' name='fupload' type='file' style='display:none;' accept='.csv'>" +
                "<i class='fa fa-upload fa-3x'></i>" +
                "<br />" +
                "Seleccione archivo" +
                "</label>" +
                "<br />" +
                "<span style='margin-left:5px !important;' id='fileList'></span>" +
                "</div><div class='clearfix'></div>";

        bootbox.dialog({
            message: uploadHtml,
            title: "Importar CSV",
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-default",
                    callback: function () {
                        var formData = new FormData();
                        formData.append('fupload', document.getElementById("fupload").files[0]);
                        $.ajax({
                            type: "POST",
                            url: "/admin/members/import-file",
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                responseModal = response;
                                setTimeout(function () {
                                    loadModal();
                                }, 500);
                            },
                            error: function (errResponse) {
                                notifyError(errResponse);
                            }
                        });
                    }
                }
            }
        });

        var fileList = document.getElementById("fupload");
        fileList.addEventListener("change", function (e) {
            var list = "";
            for (var i = 0; i < this.files.length; i++) {
                list += this.files[i].name
            }

            $("#fileList").text(list);
        }, false);
    });

    var responseModal;
    function loadModal() {
        $('#viewModal').modal();
        $('#viewModal').find('.modal-body').empty();
        $('#viewModal').find('.modal-body').html(responseModal);
    }

    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });


    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });

});
