$(document).ready(function () {

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');

        if (action === 'buy-tickets') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Comprar Entrada', 'Confirma la compra de ' + name, function (result) {
                if (result) {
                    var notif = notifyAndWait("Efectuando compra, espere por favor...");
                    $.post('/members/store/tickets/buy/' + id, null, function (response) {
                        notif.remove();
                        window.location = response;
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });
        } else if (action === 'buy-products') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            confirmDialogDanger('Comprar Producto', 'Confirma la compra de ' + name, function (result) {
                if (result) {
                    var notif = notifyAndWait("Efectuando compra, espere por favor...");
                    $.post('/members/store/buy/' + id, null, function (response) {
                        notif.remove();
                        window.location = response;
                    }).fail(function (xhr, status, error) {
                        notif.remove();
                        notifyError(xhr.responseText);
                    });
                }
            });
        } 
    });


});

