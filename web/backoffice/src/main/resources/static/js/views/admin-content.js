$(document).ready(function ($) {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            buscar();
        }
    });

    $('#search-button').click(function (e) {
        e.preventDefault();
        buscar();
    });

    function buscar() {
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'rank', $('#rank').val());
        url = addParam(url, 'country', $('#country').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'type', $('#type').val());

        setUpUrl(url);
    }


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'add-new-content') {

            var title = 'Añadir material';
            var url = '/admin/tools/content-add';
            loadInDialog(url, title, false, false);

        } else if (action === 'edit-content') {

            var id = $(e.target).data('id');
            var title = 'Editar contenido';
            var url = '/admin/tools/content-edit/' + id;
            loadInDialog(url, title, false, false);

        } else if (action === 'delete-content') {

            var id = $(e.target).data('id');
            var title = $(e.target).data('title');
            var url = '/admin/tools/content-delete/' + id;

            confirmDialogDanger('Eliminar materail', 'Confirma eliminacion material ' + title, function (result) {
                
                if (result) {                                        
                    $.post(url, null, function (response) {
                        location.reload();
                    }).fail(function (xhr, status, error) {                        
                        notifyError(xhr.responseText);
                    });
                }
            });

        } else if (action === 'enable-content') {

            var id = $(e.target).data('id');
            var url = '/admin/tools/content-enable/' + id;
            var notif = notifyAndWait("Ejecutando, espere por favor...");

            $.post(url, null, function (response) {

                notif.remove();
                notifySuccess(response);
                location.reload();

            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'disable-content') {

            var id = $(e.target).data('id');
            var url = '/admin/tools/content-disable/' + id;
            var notif = notifyAndWait("Ejecutando, espere por favor...");

            $.post(url, null, function (response) {
                notif.remove();
                notifySuccess(response);
                location.reload();
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
        } else if (action === 'reset-content') {
            var id = $(e.target).data('id');
        }

    });


    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });


    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });

});
