$(document).ready(function () {

    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A', size: 'small'
            });
        });
    }

    $('#save').click(function () {

        var notif = notifyAndWait("Editando balance, espere por favor...");
        $.post($('#request-form').attr('action'), $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText, "alert-error");
        });

    });


  


});
