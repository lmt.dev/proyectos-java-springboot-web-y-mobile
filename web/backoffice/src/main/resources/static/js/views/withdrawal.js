$(document).ready(function () {

    $('#q').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            $('#search-button').click();
        }
    });

    $('#search-button').click(function (e) {
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'from', $('#from').val());
        url = addParam(url, 'to', $('#to').val());
        url = addParam(url, 'size', $('#size').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'country', $('#country').val());

        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {
            case 'withdrawal-request':
                loadInDialog('/admin/financial/withdrawal/create', 'Crear solicitud de retiro', false, false);
                break;
            case 'delete-request':

                var id = $(e.target).data('id');
                var title = $(e.target).data('name');
                confirmDialogDanger('¿Eliminar la solicitud?', title, function (result) {
                    if (result) {
                        var notif = notifyAndWait("Eliminando solicitud, espere por favor...");
                        $.post('/admin/financial/withdrawal/delete/' + id, null, function (response) {
                            notif.remove();
                            notifySuccess(response);
                            $('#search-button').click();
                        }).fail(function (xhr, status, error) {
                            notif.remove();
                            notifyError(xhr.responseText);
                        });
                    }
                });
                break;

            case 'edit-request':
                var id = $(e.target).data('id');
                var title = $(e.target).data('name');

                loadInDialog('/admin/financial/withdrawal/edit/' + id, 'Editar solicitud de retiro ' + title, false, false);
                break;
                
            case 'complete-request':
                
                var id = $(e.target).data('id');
                var title = $(e.target).data('name');

                loadInDialog('/admin/financial/withdrawal/complete/' + id, 'Completar solicitud de retiro ' + title, false, false);
                break;
                
                
        }

    });

    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });

    $('.search-bar').on('show.bs.dropdown', function () {
        $('.search-bar').css("overflow", "inherit");
    });

    $('.search-bar').on('hide.bs.dropdown', function () {
        $('.search-bar').css("overflow", "auto");
    });

});
