$(document).ready(function () {
    
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A', size: 'small'
            });
        });
    }

    $('#save').click(function () {

        var notif = notifyAndWait("Creando solicitud, espere por favor...");
        $.post('/admin/financial/withdrawal/create', $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText,"alert-error");
        });
        
    });


    $('#member').keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode === 13) {
            searchMember($('#member').val());
        }
    });

    $('#member').blur(function (e) {
        searchMember($('#member').val());
    });

    function searchMember(id) {

        $('#memberIcon').show();
        $('#memberName').html('Buscando...').show();

        $.post('/admin/members/search/' + id, null, function (response) {

            $('#memberName').html(response);
            $('#memberIcon').hide();

        }).fail(function (response) {

            $('#memberName').html(response.responseText);
            $('#memberIcon').hide();

        });
    }


});
