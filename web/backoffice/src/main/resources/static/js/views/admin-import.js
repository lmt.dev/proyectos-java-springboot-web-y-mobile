$(document).ready(function () {


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'import-members') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/import/members', null, function (response) {
                notif.remove();
                notify(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notify(xhr.responseText);
            });

        } else if (action === 'post-import') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/import/post-import', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'parse-calification') {

            var notif = notifyAndWait("Generando calificacion, espere por favor...");
            $.post('/admin/tools/calification', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'parse-ranks') {

            var notif = notifyAndWait("Generando rangos, espere por favor...");
            $.post('/admin/tools/ranks', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'import-services') {

            var year = $('#year').val();
            var month = $('#month').val();

            $.post('/admin/tools/import/services/' + year + '/' + month, null, function (response) {
                notify(response);
            }).fail(function (xhr, status, error) {
                notify(xhr.responseText);
            });

        } else if (action === 'calc-diff') {

            var notif = notifyAndWait("Recalculando diferencial, espere por favor...");
            $.post('/admin/tools/differential/calc', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'generate-balances') {

            var notif = notifyAndWait("Regenerando balances, espere por favor...");
            $.post('/admin/tools/balances/generate', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'post-creation') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/members/process', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'trace-log-on') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/import/trace', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'fix-referers') {

            $.post('/admin/tools/fix/referers', null, function (response) {
                notify(response);
            }).fail(function (xhr, status, error) {
                notify(xhr.responseText);
            });

        } else if (action === 'import-line-services') {

            var notif = notifyAndWait("Importando lineas, espere por favor...");
            $.post('/admin/tools/import/services/lines', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'recalculate-all') {

            var notif = notifyAndWait("Recalculando todo, espere por favor...");
            $.post('/admin/tools/calculation', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'recalc-enddate') {

            var notif = notifyAndWait("Recalculando fecha final de speed bonus, espere por favor...");
            $.post('/admin/tools/members/recalculate-enddate', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'fix-stores') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/fix-stores', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'activate-balances') {

            var notif = notifyAndWait("Activando balances, espere por favor...");
            $.post('/admin/tools/balances/activate', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'import-invoices') {

            var notif = notifyAndWait("Importando facturas, espere por favor...");
            $.post('/admin/tools/invoices/import', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'reset-ranks') {

            var notif = notifyAndWait("Activando balances, espere por favor...");
            $.post('/admin/tools/reset-ranks', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'test-energy') {

            var notif = notifyAndWait("consulatndo api, espere por favor...");
            $.post('/admin/tools/energy/test', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });


        } else if (action === 'fix-dnis') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/order/fixdni', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });

        } else if (action === 'sync-marketing') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/sync-marketing', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
        } else if (action === 'order-mark-installment') {

            var notif = notifyAndWait("Ejecutando, espere por favor...");
            $.post('/admin/tools/order-mark-installment', null, function (response) {
                notif.remove();
                notifySuccess(response);
            }).fail(function (xhr, status, error) {
                notif.remove();
                notifyError(xhr.responseText);
            });
            
        }
    });



});
