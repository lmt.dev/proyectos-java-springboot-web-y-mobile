$(document).ready(function () {

    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');

        if (action === 'delete-info') {
            var id = $(e.target).data('id');
            confirmDialogDanger('Eliminar información adicional', '¿Confirma eliminacion información adicional de registro?', function (result) {
                if (result) {
                    $.post('/admin/events/info/delete/' + id, null, function (response) {
                        notifySuccess(response);
                        $('#info-' + id).remove();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        } else if (action === 'delete-extra') {
            var id = $(e.target).data('id');
            confirmDialogDanger('Eliminar información adicional', '¿Confirma eliminacion información adicional del ticket?', function (result) {
                if (result) {
                    $.post('/admin/events/extra/delete/' + id, null, function (response) {
                        notifySuccess(response);
                        $('#extra-' + id).remove();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        }
    });


    /*prevent deleting logo on enter*/
    $('#form-styles').keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });

    $('.btne-save').click(function (e) {
        e.preventDefault();

        var id = $(e.target).data('id');
        var section = $(e.target).data('section');
        var data = new FormData($('#form-' + section)[0]);
        
        var notif = notifyAndWait("Actualizando, espere por favor...");

        $.ajax({
            type: 'POST',
            url: '/admin/events/edit/' + section + '/' + id,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respData) {
                notif.remove();
                notifySuccess(respData);
                processPostEdit(section, data, id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                notif.remove();
                notifyError(xhr.responseText);
            }
        });
    });


    function processPostEdit(section, data, id) {
        switch (section) {
            case 'reginfo':

                var url = '/admin/events/info/last/' + id;
                $.get(url, null, function (response) {
                    $('#table-reginfo > tbody:last-child').append(response);
                }).fail(function (xhr, status, error) {
                    //notifyError(xhr.responseText);
                });
                $('#form-' + section).trigger('reset');
                break;
            case 'extras':
                var d = $('#form-' + section).serializeArray();
                if (d[0] !== null) {
                    $('#extras-list').append('<li>' + d[0].value + '</li>');
                }
                $('#form-' + section).trigger('reset');
                break;
            case 'styles':
                $.post('/admin/events/media/' + id, null, function (response) {
                    $("#mediaLogo").attr("src", "open/event/" + response.logo);
                    $("#mediaBack").attr("src", "open/event/" + response.back);
                }).error(function (response) {
                });
                break;
            case 'delete-logo':
                $("#mediaLogo").attr('src', '');
                break;
            case 'delete-back':
                $("#mediaBack").attr('src', '');
                break;
        }
    }


    /*if ($(".js-switch")[0]) {
     var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
     elems.forEach(function (html) {
     var switchery = new Switchery(html, {
     color: '#26B99A', size: 'small'
     });
     
     var changeCheckbox = document.querySelector('.js-switch');
     changeCheckbox.onchange = function () {
     if (changeCheckbox.checked) {
     $('#companyName').prop('disabled', false);
     $('#companyDniType').prop('disabled', false);
     $('#companyDni').prop('disabled', false);
     } else {
     $('#companyName').prop('disabled', true);
     $('#companyDniType').prop('disabled', true);
     $('#companyDni').prop('disabled', true);
     }
     };
     
     if (changeCheckbox.checked) {
     $('#companyName').prop('disabled', false);
     $('#companyDniType').prop('disabled', false);
     $('#companyDni').prop('disabled', false);
     } else {
     $('#companyName').prop('disabled', true);
     $('#companyDniType').prop('disabled', true);
     $('#companyDni').prop('disabled', true);
     }
     });
     }*/


    /*function upload() {
     $('#avatar-form').submit();
     }
     
     var btnCust = '<button type="button" class="btn btn-default btn-secondary" title="Save" id="btn-save">' +
     '<i class="fa fa-save"></i>' +
     '</button>';
     
     $("#avatar").fileinput({
     overwriteInitial: true,
     maxFileSize: 1500,
     showClose: false,
     showCaption: false,
     showBrowse: true,
     browseOnZoneClick: false,
     language: 'es',
     removeLabel: '',
     removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
     removeTitle: 'Cancel or reset changes',
     browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
     browseLabel: '',
     elErrorContainer: '#kv-avatar-errors-2',
     msgErrorClass: 'alert alert-block alert-danger',
     defaultPreviewContent: '<img src="' + avatar + '" onerror="this.src=\'/images/img.jpg\'" alt="Avatar" width="250px">',
     layoutTemplates: {main2: '{preview} ' + btnCust + '{remove} {browse}'},
     allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
     fileActionSettings: {showZoom: false}
     });
     
     
     $('#btn-save').click(upload);*/

    $('.collapse-link').on('click', function () {
        var $BOX_PANEL = $(this).closest('.x_panel'),
                $ICON = $(this).find('i'),
                $BOX_CONTENT = $BOX_PANEL.find('.x_content');

        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function () {
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200);
            $BOX_PANEL.css('height', 'auto');
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');
        $BOX_PANEL.remove();
    });

    function init_ColorPicker() {

        if (typeof ($.fn.colorpicker) === 'undefined') {
            return;
        }
        console.log('init_ColorPicker');

        $('.demo1').colorpicker();
        $('.demo2').colorpicker();

        $('#demo_forceformat').colorpicker({
            format: 'rgba',
            horizontal: true
        });

        $('#demo_forceformat3').colorpicker({
            format: 'rgba',
        });

        $('.demo-auto').colorpicker();

    }
    ;

    init_ColorPicker();

    //TODO: cambiar esto que lo hace incomodo
    /*$('.collapse-link').not('.collapse-no-auto').click();*/

    /*$('#contract-sign').click(function () {
     
     //load info in dialog
     loadInDialog("/members/profile/contract/sign", "Firma del contrato");
     
     //post contract to portal
     $.post('/members/profile/contract/send', null, function (response) {
     
     //var link = response.replace('&oneTimeRequest', '') + '&urlreturn=' + encodeURI(window.location.origin + '/members/profile/callback');
     //var link = response + '&urlreturn=' + encodeURI(window.location.origin + '/members/profile/callback');
     var link = encodeURI(window.location.origin + '/members/profile/callback?status=signed&result=success');
     //hide loading
     $("#dialog-container-wait-message").html("Redirigiendo al portal de firmas...");
     
     //show button
     $("#dialog-container-sign-link").attr('href', link);
     $("#dialog-container-sign").show();
     
     window.location.href = link;
     
     }).fail(function (response) {
     
     //hide loading
     $("#dialog-container-wait").hide();
     
     //show error
     $("#dialog-container-error-message").html(response.responseText);
     $("#dialog-container-error").show();
     
     });
     
     });*/


    /*$("#chk-acepto").click(function (e) {
     if (this.checked) {
     $("#contract-sign").removeAttr("disabled");
     } else {
     $("#contract-sign").attr("disabled", "true");
     }
     });*/


    /*$('.action-button').click(function (e) {
     
     e.preventDefault();
     var action = $(e.target).attr('id');
     
     if (action === 'upload-address') {
     
     var name = $(e.target).data('name');
     var id = $(e.target).data('id');
     var title = 'Añadir comprobante para dirección';
     
     var url = '/members/profile/media/add/' + name;
     var dialog = loadInDialog(url, title, false, false);
     
     dialog.on('hidden.bs.modal', function (e) {
     location.reload();
     });
     
     } else if (action === 'upload-dni') {
     
     var name = $(e.target).data('name');
     var id = $(e.target).data('id');
     var title = 'Añadir comprobante identificación';
     
     var url = '/members/profile/media/add/' + name;
     var dialog = loadInDialog(url, title, false, false);
     
     dialog.on('hidden.bs.modal', function (e) {
     location.reload();
     });
     
     } else if (action === 'upload-idt') {
     
     var name = $(e.target).data('name');
     var id = $(e.target).data('id');
     var title = 'Añadir comprobante identificación tributaria';
     
     var url = '/members/profile/media/add/' + name;
     var dialog = loadInDialog(url, title, false, false);
     
     dialog.on('hidden.bs.modal', function (e) {
     location.reload();
     });
     
     }
     });*/



});
