$(document).ready(function () {

    $('#save').click(function () {

        var notif = notifyAndWait("Guardando cambios, espere por favor...");
        $.post($('#request-form').attr('action'), $('#request-form').serialize(), function (response) {
            notif.remove();
            notify(response);
            $('#search-button').click();
            $('.bootbox.modal').modal('hide');
        }).fail(function (xhr, status, error) {
            notif.remove();
            showAlert(xhr.responseText, "alert-error");
        });


    });

});
