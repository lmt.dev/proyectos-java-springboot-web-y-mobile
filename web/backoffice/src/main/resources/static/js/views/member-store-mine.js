$(document).ready(function () {
    $('.action-button').click(function (e) {
        var action = $(e.target).attr('id');
        if (action === 'redeem-pack') {
            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            loadInDialog('/members/store/packs/redeem/' + id, 'Reclamar paquete ' + name, false, false);
        }
    });
});

