$(document).ready(function ($) {

    $('#upgradeSelect').change(function () {
        var id = this.value;
        $('#upgradeContent').load('/views/upgrade/' + id);
    });


    $('.action-claim').click(function (e) {
        e.preventDefault();
        var notif = notifyAndWait("Actualizando, espere por favor...");
        $.post('/members/bitwings/claim', $('#form-claim').serialize(), function (response) {
            notif.remove();
            notifySuccess(response);
            $('#reclaim-block').remove();
        }).error(function (response) {
            notif.remove();
            notifyError(response.responseText);
        });

    });


    // Progressbar
    if ($(".progress .progress-bar")[0]) {
        $('.progress .progress-bar').progressbar();
    }

});
