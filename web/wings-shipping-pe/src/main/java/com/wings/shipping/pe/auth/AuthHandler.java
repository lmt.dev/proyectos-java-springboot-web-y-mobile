/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.pe.auth;

import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author seba
 */
public class AuthHandler implements SOAPHandler<SOAPMessageContext> {

    //private String password = "Colombia1";
    //TEST
    //private final String u = "test_wings";
    //private final String p = "BpSUh12jBIiWdACDozgOaQ==";
    //private final String code = "Ser408";
    //private final String cargue = "Pruebas";

    //prod
    //private final String u = "901194517";
    //private final String p = "Tb8Hb+NLWsc=";
    //private final String code = "SER116627";
    //private final String cargue = "Produccion";
    
    //PROD nuevo
    private final String u = "901194517SUC2";
    private final String p = "Tb8Hb+NLWsc=";
    private final String code = "SER117020";
    private final String cargue = "Produccion";

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        try {
            SOAPMessage message = context.getMessage();
            SOAPHeader header = message.getSOAPHeader();
            SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
            if (header == null) {
                header = envelope.addHeader();
            }

            QName qNameAuthHeader = new QName("http://tempuri.org/", "AuthHeader");
            SOAPHeaderElement authHeader = header.addHeaderElement(qNameAuthHeader);

            QName qNamelogin = new QName("http://tempuri.org/", "login");
            SOAPHeaderElement login = header.addHeaderElement(qNamelogin);
            login.addTextNode(this.u);

            QName qNamepwd = new QName("http://tempuri.org/", "pwd");
            SOAPHeaderElement pwd = header.addHeaderElement(qNamepwd);
            pwd.addTextNode(this.p);

            /*QName qNameCodeFacturacion = new QName("http://tempuri.org/", "idCodFacturacion");
            SOAPHeaderElement codeFacturacion = header.addHeaderElement(qNameCodeFacturacion);
            codeFacturacion.addTextNode(this.code);*/
            QName qNameCodeFacturacion = new QName("http://tempuri.org/", "Id_CodFacturacion");
            SOAPHeaderElement codeFacturacion = header.addHeaderElement(qNameCodeFacturacion);
            codeFacturacion.addTextNode(this.code);

            QName qNameCargue = new QName("http://tempuri.org/", "Nombre_Cargue");
            SOAPHeaderElement nombreCargue = header.addHeaderElement(qNameCargue);
            nombreCargue.addTextNode(this.cargue);

            authHeader.addChildElement(login);
            authHeader.addChildElement(pwd);
            authHeader.addChildElement(codeFacturacion);
            authHeader.addChildElement(nombreCargue);

            message.saveChanges();

        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

   
}
