/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model.requests;

/**
 *
 * @author seba
 */
public class RequestMissedCallResponse {
    
    private String callId;
    private String status;
    private String offSet;

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public void setOffSet(String offSet) {
        this.offSet = offSet;
    }

    public String getOffSet() {
        return offSet;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
   
}
