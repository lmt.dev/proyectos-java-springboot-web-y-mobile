/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model.secondNumber;

/**
 *
 * @author seba
 */
public class RoamingFreeResponse {

    private Boolean enabled;
    private String dialString;
    private String redirectNumber;
    private String message;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getDialString() {
        return dialString;
    }

    public void setDialString(String dialString) {
        this.dialString = dialString;
    }

    public String getRedirectNumber() {
        return redirectNumber;
    }

    public void setRedirectNumber(String redirectNumber) {
        this.redirectNumber = redirectNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
