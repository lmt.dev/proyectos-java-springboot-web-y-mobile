/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model;

/**
 *
 * @author seba
 */
public class UpdateCheckResponse {
    
    
    private boolean updateAvailable;
    private String urlLink;
    private String marketLink;

    /**
     * @return the updateAvailable
     */
    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    /**
     * @param updateAvailable the updateAvailable to set
     */
    public void setUpdateAvailable(boolean updateAvailable) {
        this.updateAvailable = updateAvailable;
    }

    /**
     * @return the urlLink
     */
    public String getUrlLink() {
        return urlLink;
    }

    /**
     * @param urlLink the urlLink to set
     */
    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    /**
     * @return the marketLink
     */
    public String getMarketLink() {
        return marketLink;
    }

    /**
     * @param marketLink the marketLink to set
     */
    public void setMarketLink(String marketLink) {
        this.marketLink = marketLink;
    }
    
    
    
}
