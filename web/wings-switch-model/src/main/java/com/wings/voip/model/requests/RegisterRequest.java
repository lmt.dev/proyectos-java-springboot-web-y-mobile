package com.wings.voip.model.requests;

public class RegisterRequest {

    //TODO: mover esto al switch model
    private String login;
    private String token;
    private String pushToken;
    private String instanceId;
    private String deviceName;
    private String deviceModel;
    private String serialNumber;
    private String deviceId;
    private String udid;
    private String imei1;
    private String imei2;
    private String countryPrefix;
    private String countryIso;
    private String locale;
    private String os = "and";
    private String versionCode;
    private String versionName;
    private String mcc;
    private String mnc;

    public RegisterRequest() {
    }

    public RegisterRequest(String login, String token, String pushToken, String instanceId, String deviceName, String deviceModel, String serialNumber, String deviceId, String udid, String imei1, String imei2, String countryPrefix, String countryIso, String versionCode, String versionName) {
        this.login = login;
        this.token = token;
        this.pushToken = pushToken;
        this.instanceId = instanceId;
        this.deviceName = deviceName;
        this.deviceModel = deviceModel;
        this.serialNumber = serialNumber;
        this.deviceId = deviceId;
        this.udid = udid;
        this.imei1 = imei1;
        this.imei2 = imei2;
        this.countryPrefix = countryPrefix;
        this.countryIso = countryIso;
        this.versionCode = versionCode;
        this.versionName = versionName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getImei1() {
        return imei1;
    }

    public void setImei1(String imei1) {
        this.imei1 = imei1;
    }

    public String getImei2() {
        return imei2;
    }

    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }

    public String getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(String countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public String getCountryIso() {
        return countryIso;
    }

    public void setCountryIso(String countryIso) {
        this.countryIso = countryIso;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

}
