/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.model;

import java.util.List;

/**
 *
 * @author seba
 */
public class AccountStatus {

    private Double clientBalance;
    private boolean secure = false;
    private boolean hasDid = false;
    private boolean roamingFreeEnabled = false;
    private List<ServiceDTO> services;

    public Double getClientBalance() {
        return clientBalance;
    }

    public void setClientBalance(Double clientBalance) {
        this.clientBalance = clientBalance;
    }

    public List<ServiceDTO> getServices() {
        return services;
    }

    public void setServices(List<ServiceDTO> services) {
        this.services = services;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setHasDid(boolean hasDid) {
        this.hasDid = hasDid;
    }

    public boolean getHasDid() {
        return hasDid;
    }

    public boolean getRoamingFreeEnabled() {
        return roamingFreeEnabled;
    }

    public void setRoamingFreeEnabled(boolean roamingFreeEnabled) {
        this.roamingFreeEnabled = roamingFreeEnabled;
    }

}
