/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.CdrDAO;
import com.wings.base.dao.specs.CdrSpecs;
import com.wings.base.domain.client.Cdr;
import com.wings.base.repos.CdrRepository;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 *
 * @author seba
 */
@Controller
public class CdrController {

    private final Logger log = LoggerFactory.getLogger(CdrController.class);

    @Autowired
    private CdrRepository cdrRepository;
    
    @Autowired
    private CdrDAO cdrDAO;


    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @RequestMapping(value = {"views/clients/cdrs"}, method = RequestMethod.GET)
    public String getRegistrarions(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");

        String client = request.getParameter("client");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String priced = request.getParameter("priced");
        String type = request.getParameter("type");
        String origin = request.getParameter("origin");

        int page = 0;
        int size = 20;
        Date f = null;
        Date t = null;
        Boolean p = null;
        
        if (ssize == null) {ssize = "20";}
        if (spage == null) {spage = "0";}
        if (priced != null){p = priced.equals("true");}        
        try {page = Integer.parseInt(spage);} catch (NumberFormatException e) {}        
        try {size = Integer.parseInt(ssize);} catch (NumberFormatException e) {}        
        if(from != null){try {f = sdf.parse(from);} catch (ParseException e) {}}
        if(to != null){try {t = sdf.parse(to);} catch (ParseException e) {}}
        page = page == 0 ? page : page - 1;
        
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "date"));
        
        Page<Cdr> items = cdrRepository.findAll(CdrSpecs.cdrSearch(client, f, t, p, type,origin), pr);

        PageWrapper<Cdr> pageWrapper = new PageWrapper<>(items, "views/clients/cdrs");
        

        //summarized
        model.addAttribute("total",cdrDAO.find(client, f, t, p, type, origin));
               
        
        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);
        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("client", client);
        model.addAttribute("priced", priced);
        model.addAttribute("type", type);
        model.addAttribute("origin", origin);

        return "views/clients/cdrs";
    }

}
