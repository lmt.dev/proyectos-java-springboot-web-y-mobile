/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.xtra.PortRequest;
import com.wings.base.domain.xtra.XtraLineStatus;
import com.wings.base.enums.LineStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.PortRequestRepository;
import com.wings.base.repos.TicketsRepository;
import com.wings.base.service.ClientActivityService;
import com.wings.base.service.EmailService;
import com.wings.base.service.ProcessLogService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.service.xtra.XRiskService;
import com.wings.base.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class XtraProccess {

    private final Logger log = LoggerFactory.getLogger(XtraProccess.class);

    @Autowired
    private XPortService xPortService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private XRiskService xRiskService;

    @Autowired
    private PortRequestRepository portRequestRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private ClientActivityService clientActivityService;

    private final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");

    private final String launcher = "system";

    // 10 minutos, 3 horas
    @Scheduled(initialDelay = 600000l, fixedRate = 10800000l)
    public void checkPortStatus() {
        checkPortStatus(launcher);
    }

    public void checkPortStatus(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Verificacion portabilidades");
        List<ClientLine> porting = linesRepository.findByInternalState(LineStatus.port_requested.name());

        pl.appendLine("Portabilidades a procesar " + porting.size());
        pl.setMax(porting.size());

        for (ClientLine line : porting) {

            try {

                if (line.getContractNumber() != null && !line.getContractNumber().isEmpty()) {
                    checkLineByContract(line, pl);
                } else {
                    checkLineByNumber(line, pl);
                }

            } catch (Exception e) {
                pl.appendLine("Error para linea " + line.getNumber() + " - " + e.getMessage());
            }
        }

        linesRepository.save(porting);

        pl.appendLine("Portabilidades procesadas " + porting.size());
        pl.stop();
    }

    //10minutos, 30 minutos
    @Scheduled(initialDelay = 600000l, fixedRate = 1800000l)
    public void checkCreatedLineStatus() {
        checkCreatedLineStatus(launcher);
    }

    public void checkCreatedLineStatus(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Verificacion lineas nuevas");
        List<Client> clients = clientsRepository.findAll();
        int newOnes = 0;
        int emails = 0;
        for (Client client : clients) {

            Collection<ClientLine> clientLines = client.getLines();
            boolean mustCheck = false;
            for (ClientLine clientLine : clientLines) {
                String state = clientLine.getInternalState();
                mustCheck = state.equals(LineStatus.created_operator.name())
                        || state.equals(LineStatus.port_accepted.name())
                        || state.equals(LineStatus.change_brand_requested.name());
                if (mustCheck) {
                    break;
                }
            }

            if (!mustCheck) {
                continue;
            }

            if (client.getRefCustomerId() != null) {

                try {

                    List<XtraLineStatus> stats = xLineService.findLines(client.getRefCustomerId(), "A");
                    for (XtraLineStatus stat : stats) {

                        ClientLine searchLine = linesRepository.findByNumber(Long.valueOf(stat.getMsisdn()));
                        if (searchLine == null) {
                            searchLine = linesRepository.findByIccid(stat.getIccid());
                        }

                        if (searchLine.isActive()) {
                            continue;
                        }

                        if (!stat.getStatus().equals("A")) {
                            continue;
                        }

                        searchLine.setExternalState(stat.getStatus());
                        try {
                            searchLine.setActivationDate(sdf2.parse(stat.getActivateDate()));
                        } catch (Exception e) {
                        }
                        searchLine.setIccid(stat.getIccid());
                        searchLine.setIccidPuk(stat.getPuk());
                        searchLine.setIccidType(stat.getTypeIccid());
                        if (searchLine.getNumber() == null) {
                            searchLine.setNumber(Long.valueOf(stat.getMsisdn()));
                        }
                        searchLine.setInternalState(LineStatus.active.name());
                        newOnes++;

                        linesRepository.save(searchLine);

                        if (searchLine.getNumber() != null) {
                            Plan plan = planRepository.findOne(searchLine.getPlanId());
                            StringBuilder b = new StringBuilder();
                            b.append("<h4>Su linea ha sido activada<h4><br/>");
                            b.append("<b>Informacion de la linea</b><br/>");
                            b.append("<ul>");
                            b.append("<li>Plan: ").append(plan.toString()).append("</li>");
                            b.append("<li>Número: ").append(searchLine.getNumber()).append("</li>");
                            b.append("<li>IccID: ").append(searchLine.getIccid()).append("</li>");
                            b.append("<li>PUK: ").append(searchLine.getIccidPuk()).append("</li>");
                            b.append("</ul>");
                            b.append("<br/>");
                            b.append("<br/>");
                            b.append("<h4>Configurar APN<h4><br/>");
                            b.append("<p>Para el envío de SMS de autoconfiguración de APN (datos) puedes ingresar <a href=\"http://wingsmobile.es/register/config\" target=\"_blank\">aqui</a><p><br/>");

                            b.append("<br/><br/><br/>");

                            String body = emailService.getGenericEmail(searchLine.getClient().getName(), "Linea activada", b.toString());
                            boolean send = emailService.sendMail("contrataciones@wingsmobile.es", client.getEmail(), "Linea activada", body, null);
                            if (send) {
                                emails++;
                            }

                            xRiskService.updateRisk(searchLine.getClient().getRefCustomerId(), searchLine.getNumber().toString(), "L", "30");
                            xRiskService.updateRisk(searchLine.getClient().getRefCustomerId(), searchLine.getNumber().toString(), "H", "30");
                        }
                    }
                } catch (Exception e) {
                    pl.appendLine("Error consultando estado " + e.getMessage());
                }
            }
        }

        pl.appendLine("Lineas activas encontradas " + newOnes);
        pl.appendLine("Correos enviados " + emails);
        pl.stop();
    }

    /**
     * Check porting request for line if we do not have a contract yet
     *
     * @param line
     * @param pl
     * @throws Exception
     */
    private void checkLineByNumber(ClientLine line, ProcessLog pl) throws Exception {

        List<PortRequest> requests = xPortService.getPortingRequests(line.getNumber().toString());

        if (requests == null || requests.isEmpty()) {
            log.error("No port request found for " + line.getNumber());
            line.setExternalState("No porting request found, please check");
            return;
        }

        pl.appendLine("Repuesta obtenida, linea " + line.getNumber() + " estados " + requests.size());
        portRequestRepository.save(requests);

        for (PortRequest request : requests) {

            switch (request.getIdFase()) {
                case "S":
                    line.setExternalState(request.getFase());

                    if (request.getFase().contains("Portabilidad Realizada")) {
                        line.setInternalState(LineStatus.port_accepted.name());
                    } else {
                        line.setInternalState(LineStatus.port_requested.name());
                    }

                    line.setContractNumber(request.getContract() != null ? request.getContract().toString() : "");
                    break;

                case "P":

                    line.setExternalState(request.getFase());
                    line.setInternalState(LineStatus.port_requested.name());
                    line.setContractNumber(request.getContract() != null ? request.getContract().toString() : "");
                    break;

                case "E":

                    line.setExternalState(request.getFase());
                    line.setInternalState(LineStatus.port_rejected.name());
                    line.setContractNumber(request.getContract() != null ? request.getContract().toString() : "");
                    Ticket t = new Ticket(TicketType.line.name(), line.getId(), "Portabilidad rechazada", "system", "CSLEVEL1");
                    ticketsRepository.save(t);
                    break;

                default:

                    if (request.getFase().contains("Lista Para Preactivar")) {
                        line.setInternalState(LineStatus.port_accepted.name());
                    }

                    line.setExternalState(request.getFase());
                    line.setContractNumber(request.getContract() != null ? request.getContract().toString() : "");
                    break;
            }
        }
        pl.appendLine("Resultado para linea " + line.getNumber() + " " + line.getExternalState());
        pl.increment();

    }

    /**
     * Check especific port request
     *
     * @param line
     * @param pl
     * @throws Exception
     */
    private void checkLineByContract(ClientLine line, ProcessLog pl) throws Exception {

        PortRequest request = xPortService.getPortRequestInfo(line);
        if (request == null) {
            log.error("No port request found for " + line.getNumber());
            line.setExternalState("No porting request found, please check");
            return;
        }

        pl.appendLine("Repuesta obtenida, linea " + line.getNumber());
        portRequestRepository.save(request);

        switch (request.getIdFase()) {
            case "S":
                line.setExternalState(request.getFase());

                if (request.getFase().contains("Portabilidad Realizada")) {
                    line.setInternalState(LineStatus.port_accepted.name());
                    clientActivityService.createOperatorActivity(line.getInternalState(), request.getFase(), line);
                } else {
                    line.setInternalState(LineStatus.port_requested.name());
                }
                line.setPortDate(request.getPortingDate());

                break;

            case "P":

                line.setExternalState(request.getFase());
                line.setInternalState(LineStatus.port_requested.name());
                line.setPortDate(request.getPortingDate());

                break;

            case "E":

                line.setExternalState(request.getFase());
                line.setInternalState(LineStatus.port_rejected.name());

                clientActivityService.createOperatorActivity(line.getInternalState(), request.getFase(), line);
                //if port is rejected set to null to set after new port is requested
                line.setContractNumber(null);

                Ticket t = new Ticket(TicketType.line.name(), line.getId(), "Portabilidad rechazada", "system", "CSLEVEL1");
                ticketsRepository.save(t);
                break;

            default:

                if (request.getFase().contains("Lista Para Preactivar")) {
                    line.setInternalState(LineStatus.port_accepted.name());
                    line.setPortDate(request.getPortingDate());
                    clientActivityService.createOperatorActivity(line.getInternalState(), request.getFase(), line);
                }

                line.setExternalState(request.getFase());
                break;
        }

        pl.appendLine("Resultado para linea " + line.getNumber() + " " + line.getExternalState());
        pl.increment();

    }

    //1 dia, 1 dia
    @Scheduled(initialDelay = 86400000l, fixedRate = 86400000l)
    public void checkActiveLines() {

        List<ClientLine> lines = linesRepository.findByInternalState(LineStatus.active.name());
        lines.addAll(linesRepository.findByInternalState(LineStatus.suspended.name()));
        for (ClientLine line : lines) {
            if (line.getNumber() == null) {
                continue;
            }
            checkLineStatus(line);
        }
    }

    public void checkLineStatus(ClientLine line) {

        if (line.getNumber() == null) {
            return;
        }

        List<XtraLineStatus> status = xLineService.getLineStatus(line);
        for (XtraLineStatus stat : status) {

            Date statDate = new Date();
            try {
                statDate = sdf2.parse(stat.getStatusDate());
            } catch (Exception e) {
                Ticket t = new Ticket(TicketType.line.name(), line.getId(), "Error parseando fecha de estado, poniendo fecha actual", "system", "ADMIN");
                ticketsRepository.save(t);
            }

            switch (stat.getStatus()) {
                case "A":
                    if (!line.getInternalState().equals(LineStatus.active.name())
                            && line.getActivationDate() != null) {

                        line.setStatusDate(statDate);
                        line.setInternalState(LineStatus.active.name());
                    }
                    break;
                case "B":
                    line.setCancelDate(statDate);
                    line.setInternalState(LineStatus.canceled.name());
                    break;
                case "D":
                    line.setStatusDate(statDate);
                    line.setInternalState(LineStatus.inactive.name());
                    break;
                case "S":
                    line.setStatusDate(statDate);
                    line.setInternalState(LineStatus.suspended.name());
                    break;
                case "J":
                    line.setStatusDate(statDate);
                    line.setInternalState(LineStatus.callBarring.name());
                    break;
                case "R":
                    line.setStatusDate(statDate);
                    line.setInternalState(LineStatus.fraud.name());
                    break;
                case "L":
                    line.setStatusDate(statDate);
                    line.setInternalState(LineStatus.suspended.name());
                    break;
            }
            line.setExternalState(stat.getStatus());
            linesRepository.save(line);
        }

    }

}
