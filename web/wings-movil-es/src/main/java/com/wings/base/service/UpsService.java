/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.ups.wsdl.xoltws.track.v2.TrackPortType;
import com.ups.wsdl.xoltws.track.v2.TrackService;
import com.ups.xmlschema.xoltws.common.v1.RequestType;
import com.ups.xmlschema.xoltws.track.v2.ActivityType;
import com.ups.xmlschema.xoltws.track.v2.AddressType;
import com.ups.xmlschema.xoltws.track.v2.DeliveryDetailType;
import com.ups.xmlschema.xoltws.track.v2.PackageAddressType;
import com.ups.xmlschema.xoltws.track.v2.PackageType;
import com.ups.xmlschema.xoltws.track.v2.ShipmentType;
import com.ups.xmlschema.xoltws.track.v2.TrackRequest;
import com.ups.xmlschema.xoltws.track.v2.TrackResponse;
import com.ups.xmlschema.xoltws.upss.v1.UPSSecurity;
import com.wings.base.WingsProperties;
import com.wings.base.domain.ConfigOptions;
import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.OrderActivity;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.TicketsRepository;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class UpsService {

    @Autowired
    private ConfigService configService;

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private TicketsRepository ticketsRepository;

    private final Logger log = LoggerFactory.getLogger(UpsService.class);
    private final Map<String, String> codes = new HashMap<>();
    private final TrackService service;
    private final TrackPortType trackPortType;
    private final String username;
    private final String password;
    private final String accessKey;
    private final UPSSecurity upss;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private final FTPClient client = new FTPClient();
    private FileInputStream fis = null;
    private FileOutputStream fos = null;

    private final String ftpServer;
    private final String ftpUser;
    private final String ftpPass;
    private final String ordersDir;

    public UpsService(WingsProperties wingsProperties) {

        codes.put("I", "In Transit");
        codes.put("D", "Delivered");
        codes.put("X", "Exception");
        codes.put("P", "Pickup");
        codes.put("M", "Manifest Pickup");

        WingsProperties.Ups ups = wingsProperties.getUps();

        String url = ups.getTrackingUrl();
        this.username = ups.getUser();
        this.password = ups.getPass();
        this.accessKey = ups.getKey();

        this.upss = new UPSSecurity();
        UPSSecurity.ServiceAccessToken upsSvcToken = new UPSSecurity.ServiceAccessToken();
        upsSvcToken.setAccessLicenseNumber(accessKey);
        upss.setServiceAccessToken(upsSvcToken);
        UPSSecurity.UsernameToken upsSecUsrnameToken = new UPSSecurity.UsernameToken();
        upsSecUsrnameToken.setUsername(username);
        upsSecUsrnameToken.setPassword(password);
        upss.setUsernameToken(upsSecUsrnameToken);

        service = new TrackService();
        trackPortType = service.getTrackPort();
        BindingProvider bp = (BindingProvider) trackPortType;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);

        this.ftpServer = wingsProperties.getUps().getFtpServer();
        this.ftpUser = wingsProperties.getUps().getFtpUser();
        this.ftpPass = wingsProperties.getUps().getFtpPass();
        this.ordersDir = wingsProperties.getUps().getOrdersDir();

    }

    public List<OrderActivity> getTrackingActivity(String trackingCode) {

        List<OrderActivity> activities = new LinkedList<>();

        TrackRequest trackRequest = new TrackRequest();
        trackRequest.setLocale("es_ES");
        RequestType request = new RequestType();
        List<String> requestOption = request.getRequestOption();
        requestOption.add("activity");
        trackRequest.setRequest(request);
        trackRequest.setInquiryNumber(trackingCode);
        trackRequest.setTrackingOption("01");

        try {
            TrackResponse trackResponse = trackPortType.processTrack(trackRequest, upss);

            String statusCode = trackResponse.getResponse().getResponseStatus().getCode();
            String description = trackResponse.getResponse().getResponseStatus().getDescription();

            if (!statusCode.equals("1")) {
                throw new Exception(description);
            }

            List<ShipmentType> shipments = trackResponse.getShipment();
            OrderActivity activity;
            for (ShipmentType shipment : shipments) {
                
                List<PackageType> pkgs = shipment.getPackage();
                for (PackageType pkg : pkgs) {
                    
                    String pkgAddressDescription = null;
                    List<PackageAddressType> pkgAddressTypes = pkg.getPackageAddress();
                    for (PackageAddressType pkgAddressType : pkgAddressTypes) {
                        
                        if(pkgAddressType.getType().getDescription() != null){
                            pkgAddressDescription = pkgAddressType.getType().getDescription();
                        }                        
                        
                        if(pkgAddressDescription != null && pkgAddressDescription.equals("ReturnTo Address")){
                            break;
                        }                        
                    }
                    
                    List<ActivityType> acts = pkg.getActivity();
                    for (ActivityType act : acts) {

                        activity = new OrderActivity();
                        activity.setTrackingCode(trackingCode);
                        activity.setActivityDate(sdf.parse(act.getDate() + act.getTime()));
                        activity.setCode(act.getStatus().getCode());
                        activity.setDescription(act.getStatus().getDescription());
                        activity.setType(codes.get(act.getStatus().getType()));
                        activity.setDetail(false);

                        if (act.getActivityLocation() != null) {
                            
                            activity.setLocationDescription(act.getActivityLocation().getDescription());
                            activity.setLocationCode(act.getActivityLocation().getCode());
                            activity.setSignedForByName(act.getActivityLocation().getSignedForByName());

                            if (act.getActivityLocation().getAddress() != null) {
                                AddressType address = act.getActivityLocation().getAddress();
                                activity.setAddressCity(address.getCity());
                                activity.setAddressCountryCode(address.getCountryCode());
                                activity.setAddressPostalCode(address.getPostalCode());
                            }
                            activity.setPackageAddressDescription(pkgAddressDescription);                            
                        }
                        activities.add(activity);
                    }
                }
            }

        } catch (Exception e) {
            log.error("Error retrieving track information from " + trackingCode, e);
        }
        return activities;
    }

    public boolean upload(List<File> files) {

        boolean ret = false;
        ProcessLog pl = processLogService.createProcess("system", "Ups orders upload");

        try {
            client.connect(ftpServer);
            client.login(ftpUser, ftpPass);
            pl.appendLine("Connect and logged in to server");
            pl.appendLine("Files to upload " + files.size());
            pl.setMax(files.size());

            for (File file : files) {

                if (file.getName().contains("xml")) {
                    client.setFileType(FTPClient.ASCII_FILE_TYPE);
                } else {
                    client.setFileType(FTPClient.BINARY_FILE_TYPE);
                }

                fis = new FileInputStream(file);
                client.storeFile(file.getName(), fis);
                fis.close();

                pl.increment();
            }

            ret = true;

            client.logout();
        } catch (IOException e) {
            log.error("Error uploading file to ftp server", e);
            pl.appendLine("Error uploading file to ftp server " + e.getMessage());

            Ticket t = new Ticket(TicketType.order.name(), null, "Error uploading file to ftpserver", "system", "ADMIN");
            ticketsRepository.save(t);

        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                client.disconnect();
            } catch (IOException e) {
                log.error("Error closing ftp conection", e);
            }
        }

        pl.appendLine("Ups orders upload Done!");
        pl.stop();

        return ret;
    }

    public List<File> downloadOrders() {

        File test = new File(ordersDir + "/complete/");
        if (!test.exists()) {
            test.mkdirs();
        }

        ProcessLog pl = processLogService.createProcess("system", "UPS orders download");

        List<File> downloaded = new ArrayList<>();
        String lastTimestamp = configService.getProperty(ConfigOptions.UPS_LAST_TIMESTAMP);
        if (lastTimestamp == null) {
            lastTimestamp = "0";
            configService.setPropertyAndSave(ConfigOptions.UPS_LAST_TIMESTAMP, lastTimestamp);
        }

        long minTime = Long.parseLong(lastTimestamp);
        final long filterTime = minTime;

        try {
            client.connect(ftpServer);

            pl.appendLine("connected to server");

            client.setControlKeepAliveReplyTimeout(5000);
            client.setDefaultTimeout(300000);
            client.setSoTimeout(300000);
            client.setDataTimeout(0);
            client.setFileType(FTP.ASCII_FILE_TYPE);

            client.login(ftpUser, ftpPass);
            pl.appendLine("logged in");

            FTPFileFilter filter = (FTPFile ftpf) -> ftpf.getTimestamp().getTimeInMillis() > filterTime;

            FTPFile[] files = client.listFiles("New Folder/", filter);
            pl.appendLine("files to download: " + files.length);

            pl.setMax(files.length);
            for (FTPFile remote : files) {

                if (minTime < remote.getTimestamp().getTimeInMillis()) {
                    minTime = remote.getTimestamp().getTimeInMillis();
                }

                File f = new File(ordersDir + "/complete/" + remote.getName());
                fos = new FileOutputStream(f);
                log.info("Downloading : " + remote.getName());
                client.retrieveFile("New Folder/" + remote.getName(), fos);
                fos.flush();
                fos.close();

                pl.increment();
                downloaded.add(f);
            }

            pl.appendLine("setting min time: " + minTime);
            configService.setPropertyAndSave(ConfigOptions.UPS_LAST_TIMESTAMP, String.valueOf(minTime));

            client.logout();
            client.disconnect();

        } catch (IOException e) {
            pl.appendLine("Error downloading file from ftp server: " + e.getMessage());
            log.error("Error downloading file from ftp server", e);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                client.disconnect();
            } catch (IOException e) {
                log.error("Error closing ftp conection", e);
            }
        }

        pl.stop();
        return downloaded;
    }

}
