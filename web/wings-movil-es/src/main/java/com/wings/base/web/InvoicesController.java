/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.InvoiceSpecs;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.cs.Message;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.payment.PaymentRequest;
import com.wings.base.repos.CsMessageRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.PaymentRequestRepository;
import com.wings.base.service.BillingService;
import com.wings.base.service.EmailService;
import com.wings.base.service.InvoiceService;
import com.wings.base.utils.FormatUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author seba
 */
@Controller
public class InvoicesController {

    private final Logger log = LoggerFactory.getLogger(InvoicesController.class);

    @Autowired
    private BillingService billingService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CsMessageRepository csMessageRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private InvoiceService invoiceService;

    @RequestMapping(value = {"views/clients/invoices"}, method = RequestMethod.GET)
    public String getInvoices(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String month = request.getParameter("month");
        String year = request.getParameter("year");

        Integer imonth = null;
        Integer iyear = null;
        try {
            imonth = Integer.parseInt(month);
        } catch (NumberFormatException e) {
        }
        try {
            iyear = Integer.parseInt(year);
        } catch (NumberFormatException e) {
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));
        Page<Invoice> items = invoiceRepository.findAll(InvoiceSpecs.invoiceSearch(q, status, imonth, iyear, null), pr);
        PageWrapper<Invoice> pageWrapper = new PageWrapper<>(items, "views/clients/invoices");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("month", month);
        model.addAttribute("year", year);
        model.addAttribute("page", pageWrapper);

        return "views/clients/invoices";
    }

    @RequestMapping(value = {"views/clients/invoices/{id}/form"}, method = RequestMethod.GET)
    public String getInvoiceForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Invoice invoice = invoiceRepository.findOne(id);
        model.addAttribute("title", "Factura");
        model.addAttribute("subTitle", invoice.getInvoiceNumber());
        model.addAttribute("elements", MapUtils.getFormElements(invoice));

        return "views/common/view-form";
    }

    @RequestMapping(value = {"api/clients/invoices/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getContractPdf(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Invoice invoice = invoiceRepository.findOne(id);
        File pdfFile = new File(invoice.getFilePath().trim());

        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + invoice.getFileName());
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/invoice/send"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendInvoices(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        List<Long> ids = new ArrayList<>();
        StringBuilder ret = new StringBuilder();
        ret.append("<br/>");

        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        new Thread(() -> {
            List<Invoice> invoices = invoiceRepository.findAll(ids);
            for (Invoice invoice : invoices) {

                String subject = "Factura mes " + invoice.getMonth() + " - " + invoice.getYear();
                String boby = "Adjuntamos la factura generada para el mes " + invoice.getMonth() + " de " + invoice.getYear();
                String client = invoice.getClient().getName();

                String htmlBody = emailService.getGenericEmail(client, subject, boby);

                List<File> attachments = new ArrayList<>();
                attachments.add(new File(invoice.getFilePath()));

                emailService.sendMail("Wings Mobile - Facturación <contratacion@wingsmobile.es>", invoice.getClient().getEmail(), subject, htmlBody, attachments);

                invoice.setSended(true);
                invoice.setLastSended(new Date());

            }
            invoiceRepository.save(invoices);
        }).start();

        return ResponseEntity.ok("Solicitud enviada correctamente.");
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/invoice/charge"}, method = RequestMethod.POST)
    public ResponseEntity<String> chargeInvoices(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        if (true) {
            return ResponseEntity.badRequest().body("Funcion deshabilitada :(.");
        }
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        if (ids.isEmpty()) {
            return ResponseEntity.badRequest().body("Debe seleccionar al menos una factura.");
        }

        List<Invoice> invoices = invoiceRepository.findAll(ids);
        if (invoices.isEmpty()) {
            return ResponseEntity.badRequest().body("No se encontraron facturas.");
        }

        new Thread(() -> {
            billingService.processPaymentForInvoices(launcher, invoices, null);
        }).start();

        return ResponseEntity.ok("Solicitud enviada correctamente.");
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/invoice/charge/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> chargeInvoice(HttpServletRequest request, @PathVariable("id") Long invoiceId) {

        if(true){
            return ResponseEntity.badRequest().body("Funcion deshabilitada :( .");
        }
        
        Invoice invoice = invoiceRepository.findOne(invoiceId);
        if (invoice == null) {
            return ResponseEntity.badRequest().body("No se encontro la factura.");
        }

        List<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice);
        billingService.processPaymentForInvoices("", invoices, null);

        return ResponseEntity.ok("Solicitud enviada correctamente, revise las solicitudes de cobro para ver el resultado.");
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/invoice/refund/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> refundInvoice(Model model, HttpServletRequest request, @PathVariable("id") Long invoideId) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        Invoice invoice = invoiceRepository.findOne(invoideId);
        if (invoice == null) {
            return ResponseEntity.badRequest().body("No se encontro la factura.");
        }

        String ret = billingService.refundInvoice(launcher, invoice);

        if (invoice.getStatus().equals("refunded")) {
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(launcher);
            message.setClientId(invoice.getClientId());
            message.setText("Reembolso de factura " + invoice.getInvoiceNumber() + " por un monto total de " + invoice.getTotalText());
            csMessageRepository.save(message);
        }

        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/invoice/payment-details/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getInvoicePaymentDetails(Model model, HttpServletRequest request, @PathVariable("id") Long invoideId) {

        Invoice invoice = invoiceRepository.findOne(invoideId);
        if (invoice == null) {
            return ResponseEntity.badRequest().body("No se encontro la factura.");
        }

        StringBuilder ret = new StringBuilder();
        ret.append("<pre>");

        ret.append("<b>Solicitudes de cobro para factura:")
                .append(invoice.getInvoiceNumber())
                .append(" (")
                .append(invoice.getId())
                .append(") ")
                .append("</b>")
                .append(System.lineSeparator())
                .append(System.lineSeparator());

        //detalle de la factura
        List<PaymentRequest> requests = paymentRequestRepository.findByInvoiceId(invoideId);
        for (PaymentRequest req : requests) {
            ret.append(FormatUtils.formatHumanOnlyDate(req.getRequestDate()))
                    .append(" ").append(req.getTransactionType().equals("4") ? "Devolucion" : "Cobro")
                    .append(" Linea:").append(req.getLineId())
                    .append(" Monto:").append(Double.parseDouble(req.getAmount()) / 100.0d)
                    .append(" Estado:").append(req.getStatus())
                    .append(" ").append(req.getStatusCode())
                    .append(System.lineSeparator());
        }
        ret.append("</pre>");

        return ResponseEntity.ok(ret.toString());
    }

    /**
     * Get one invoice as pdf
     * @param model
     * @param request
     * @param token
     * @return
     */
    @RequestMapping(value = {"/invoices/{token}"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getInvoiceForToken(Model model, HttpServletRequest request, @PathVariable("token") String token) {

        Invoice invoice = invoiceRepository.findByToken(token);
        File pdfFile = new File(invoice.getFilePath().trim());

        if (!pdfFile.exists()) {
            String filePath = invoiceService.generateInvoicePdf(invoice);
            pdfFile = new File(filePath);
        }

        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + invoice.getFileName());
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

}
