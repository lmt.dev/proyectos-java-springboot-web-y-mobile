/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.dao.specs.CdrSpecs;
import com.wings.base.domain.Token;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Cdr;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.client.Order;
import com.wings.base.domain.client.OrderActivity;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.invoice.InvoiceBalanceWrapper;
import com.wings.base.domain.invoice.InvoiceCdrWrapper;
import com.wings.base.domain.xtra.PortRequest;
import com.wings.base.process.BillingProccess;
import com.wings.base.process.CompleteProcess;
import com.wings.base.process.LineProcess;
import com.wings.base.service.OrdersService;
import com.wings.base.process.XtraProccess;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.BillingAddressRepository;
import com.wings.base.repos.CdrRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.ContractsRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrderActivityRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.RegistrationRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.PortRequestRepository;
import com.wings.base.service.ContractSignatureService;
import com.wings.base.service.EmailService;
import com.wings.base.service.InvoiceService;
import com.wings.base.service.TokensService;
import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.utils.MapUtils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author seba
 */
@Controller
public class TestController {

    private final Logger log = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private PortLinesRepository portLinesRepository;

    @Autowired
    private PlanRepository plansRepository;

    @Autowired
    private BillingAddressRepository billingAddressRepository;

    @Autowired
    private LinesRepository lineRepository;

    @Autowired
    private XClientService xClientService;

    @Autowired
    private XPortService xPortService;

    @Autowired
    private PortRequestRepository portRequestRepository;

    @Autowired
    private OrdersService orderProcess;

    @Autowired
    private CompleteProcess completeProcess;

    @Autowired
    private LineProcess lineProcess;

    @Autowired
    private BillingProccess billingProccess;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderActivityRepository orderActivityRepository;

    @Autowired
    private ContractsRepository contractsRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ContractSignatureService contractSignatureService;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CdrRepository cdrRepository;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private TokensService tokensService;
    
    @Autowired
    private PlanRepository planRepository; 

    @ResponseBody
    @RequestMapping(value = {"test/ports"}, method = RequestMethod.GET)
    public String gePortRequest(Model model, HttpServletRequest request) {

        //List<PortRequest> requests = xPortService.getPortingRequests();
        //portRequestRepository.save(requests);
        //xtraProccess.getPortingRequests();
        //xtraProccess.processLineStatus();

        /*List<Client> clients = xClientService.getClientList();
        for (Client client : clients) {
            log.info(client.getName());
            log.info(client.getLastname());
        }*/
        return "views/clients/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"test/port"}, method = RequestMethod.GET)
    public String gePortRequestOne(Model model, HttpServletRequest request) {

        String ret = "";
        try {
            List<ClientLine> lines = lineRepository.findByClientId(8l);
            for (ClientLine line : lines) {
                PortRequest r = xPortService.getPortRequestInfo(line);
                ret = MapUtils.getFormElements(r).toString();
            }
        } catch (Exception e) {
        }
        return ret;
    }

    @ResponseBody
    @RequestMapping(value = {"test/pdf"}, method = RequestMethod.GET)
    public String fillPdf(Model model, HttpServletRequest request) {

        SimpleDateFormat sdf = new SimpleDateFormat();

        //ClientLine l = lineRepository.findByClientId(c.getId()).get(0);
        ClientLine l = lineRepository.findOne(8l);
        Client c = clientsRepository.findOne(l.getClientId());
        Plan p = plansRepository.findOne(l.getPlanId());

        Map<String, String> map = new HashMap<>();
        map.put("Tarifa", p.getProviderProfile());
        map.put("Nombre Partner", "Uppertel Spain SL");
        map.put("Email Dealer", "contratacion@wingsmobile.es");
        map.put("Nº de Tlf usuario", l.getNumber().toString());
        map.put("Numero", c.getIdValue());

        sdf.applyPattern("dd/MM/yyyy");
        map.put("Fecha de nacimiento", sdf.format(c.getBirthDate()));

        map.put("Nombre", c.getName() + " " + c.getLastname());
        map.put("Direccion", c.getAddress());
        map.put("Nacionalidad", c.getNationality());
        map.put("CP", c.getPostalCode());
        map.put("Poblacion", c.getCity());
        map.put("Provincia", c.getState());
        map.put("Email", c.getEmail());

        //a x dias del mes de x del año x
        Date now = new Date();

        sdf.applyPattern("dd");
        map.put("Dia", sdf.format(now));
        sdf.applyPattern("MMM");
        map.put("Mes", sdf.format(now));
        sdf.applyPattern("yy");
        map.put("Año", sdf.format(now));

        sdf.applyPattern("dd");
        map.put("Fecha solicitud 1", sdf.format(now));
        sdf.applyPattern("MM");
        map.put("Fecha solicitud 2", sdf.format(now));
        sdf.applyPattern("yyyy");
        map.put("Fecha solicitud 3", sdf.format(now));

        map.put("Subdealer 1", "");
        map.put("Subdealer 2", "");
        map.put("Firma", c.getName() + " " + c.getLastname());
        map.put("En", "Barcelona");

        map.put("Dealer", "");

        PDDocument pdf;
        try {
            pdf = PDDocument.load(new File("/home/seba/Descargas/test2/cambio_marca.pdf"));

            PDDocumentCatalog docCatalog = pdf.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();
            List<PDField> fields = acroForm.getFields();
            for (PDField field : fields) {
                field.setValue(map.get(field.getPartialName()));
                //log.error(field.getPartialName());
            }
            acroForm.flatten();
            pdf.save(new File("/home/seba/Descargas/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf"));
            pdf.close();

            pdf = PDDocument.load(new File("/home/seba/Descargas/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf"));

            ///agcliento firma
            //PDPage page = pdf.getPage(0);            
            PDImageXObject pdImage = PDImageXObject.createFromFile("/home/seba/Descargas/test2/firma_uppertel.png", pdf);
            PDPageContentStream contentStream = new PDPageContentStream(pdf, pdf.getPage(0), true, false);

            contentStream.drawImage(pdImage, 360, 173);
            contentStream.close();

            pdf.save(new File("/home/seba/Descargas/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf"));

            pdf.close();

        } catch (Exception e) {
            log.error("Error on pdf", e);
        }

        return "views/clients/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"test/import"}, method = RequestMethod.GET)
    public String testImport(Model model, HttpServletRequest request) {
        completeProcess.test();
        return "views/clients/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"test/balance"}, method = RequestMethod.GET)
    public String testBalance(Model model, HttpServletRequest request) {
        billingProccess.processPlanMonthly(8, 2017);
        return "views/clients/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"test/sanitize"}, method = RequestMethod.GET)
    public String testSanitize(Model model, HttpServletRequest request) {

        List<Client> clients = clientsRepository.findAll();
        for (Client client : clients) {
            client.setName(WordUtils.capitalizeFully(client.getName()));
            client.setLastname(WordUtils.capitalizeFully(client.getLastname()));
            client.setAddress(WordUtils.capitalizeFully(client.getAddress()));
            client.setCity(WordUtils.capitalizeFully(client.getCity(), new char[]{'´', '\''}));
            client.setState(WordUtils.capitalizeFully(client.getState()));
            client.setIdValue(StringUtils.upperCase(client.getIdValue()));
        }

        clientsRepository.save(clients);

        return "views/clients/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"test/order-status"}, method = RequestMethod.GET)
    public String testOrderStatus(Model model, HttpServletRequest request) {

        List<Order> orders = ordersRepository.findAll();
        for (Order order : orders) {
            if (order.getTrackingNumber() != null) {
                List<OrderActivity> acts = orderActivityRepository.findByTrackingCode(order.getTrackingNumber(), new Sort(Sort.Direction.DESC, "activityDate"));
                if (!acts.isEmpty()) {
                    order.setOrderStatus(acts.get(0).getDescription());
                }
            }
        }
        ordersRepository.save(orders);

        return "views/clients/clients";
    }

    @RequestMapping(value = {"views/test/invoice"}, method = RequestMethod.GET)
    public String testInvoice(Model model, HttpServletRequest request) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Date from = new Date();
        Date to = new Date();
        try {
            from = sdf.parse("01/08/2017");
            to = sdf.parse("31/08/2017");
        } catch (Exception e) {
        }

        List<Invoice> invoices = invoiceRepository.findAll();
        Invoice i = invoices.get(5);
        Client c = i.getClient();
        Collection<ClientLine> lines = c.getLines();

        Map<Long, List<Balance>> balancePerLine = new HashMap<>();
        Map<Long, List<Cdr>> cdrPerLine = new HashMap<>();

        List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
        List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

        for (ClientLine line : lines) {
            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), "8", "2017"), new Sort(Sort.Direction.ASC, "id"));
            //Collections.sort(balances, (Balance o1, Balance o2) -> o1.getType().compareTo(o2.getType()));

            balancesWrappers.add(new InvoiceBalanceWrapper(line.getNumber().toString(), balances));

            balancePerLine.put(line.getNumber(), balances);
            List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
            cdrPerLine.put(line.getNumber(), cdrs);

            cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
        }

        invoiceService.generateInvoicePDF(i, balancesWrappers, cdrWrappers);

        model.addAttribute("invoice", i);
        model.addAttribute("balancePerLine", balancePerLine);
        model.addAttribute("cdrPerLine", cdrPerLine);

        return "fragments/invoice/invoice";
    }

    @RequestMapping(value = {"/test/payment"}, method = RequestMethod.GET)
    public String testPayment(Model model, HttpServletRequest request) {
        //model.addAttribute("data", billingProccess.proccessPayments(8, 2017));
        return "test";
    }

    @RequestMapping(value = {"/test/contract/email"}, method = RequestMethod.GET)
    public String testContractEmail(Model model, HttpServletRequest request) {

        Contract contract = contractsRepository.findOne(19l);
        //send email
        String body = emailService.getSignatureEmail(contract);
        emailService.sendMail("contratacion@wingsmobile.es",
                "sebastian.lucero@wingsmobile.es",
                "Envio contrato al portal de Firmas.",
                body,
                null);

        return "";
    }

    @RequestMapping(value = {"/test/generic/email"}, method = RequestMethod.GET)
    public String testGenericEmail(Model model, HttpServletRequest request) {

        //Contract contract = contractsRepository.findOne(19l);
        ClientLine searchLine = lineRepository.findOne(33l);
        //send email

        Plan plan = planRepository.findOne(searchLine.getPlanId());
        StringBuilder b = new StringBuilder();
        b.append("<h4>Su linea ha sido activada<h4><br/>");
        b.append("<b>Informacion de la linea</b><br/>");
        b.append("<ul>");
        b.append("<li>Plan: ").append(plan.toString()).append("</li>");
        b.append("<li>Número: ").append(searchLine.getNumber()).append("</li>");
        b.append("<li>IccID: ").append(searchLine.getIccid()).append("</li>");
        b.append("<li>PUK: ").append(searchLine.getIccidPuk()).append("</li>");
        b.append("</ul>");
        b.append("<br/><br/><br/>");

        String body = emailService.getGenericEmail(searchLine.getClient().getName(), "Linea activada", b.toString());
        boolean send = emailService.sendMail("contrataciones@wingsmobile.es", "sebastian.lucero@wingsmobile.es", "Linea activada", body, null);        

        return "";
    }

}
