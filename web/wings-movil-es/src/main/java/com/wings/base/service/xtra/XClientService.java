/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.base.domain.client.Client;
import com.wings.xtra.UrlLocator;
import com.wings.xtra.customer.create.CableCustomers;
import com.wings.xtra.customer.create.CableCustomersPort;
import com.wings.xtra.customer.create.TestCreateObjectFactory;
import com.wings.xtra.customer.create.request.Activate;
import com.wings.xtra.customer.create.request.Address;
import com.wings.xtra.customer.create.request.CableCustomerMaintenanceRequest;
import com.wings.xtra.customer.create.request.Contact;
import com.wings.xtra.customer.create.request.IndividualDetails;
import com.wings.xtra.customer.create.request.Operation;
import com.wings.xtra.customer.create.request.Services;
import com.wings.xtra.customer.create.request.SoapRequest;
import com.wings.xtra.customer.create.response.CableCustomerMaintenanceRequestResponse;
import com.wings.xtra.customer.delete.TestDeleteObjectFactory;
import com.wings.xtra.customer.find.TestFindObjectFactory;
import com.wings.xtra.customer.find.request.Find;
import com.wings.xtra.customer.update.TestUpdateObjectFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author seba
 */
public class XClientService {

    private final Logger log = LoggerFactory.getLogger(XClientService.class);

    private final String env;

    public XClientService(String env) {
        this.env = env;
    }
    
    public Client createClient(Client client) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        try {

            UrlLocator locator = new UrlLocator(env);
            URL url = locator.getCreateClientURL();

            CableCustomers service = new CableCustomers(url);
            CableCustomersPort port = service.getCableCustomersSoapService();
            CableCustomerMaintenanceRequest part1 = new CableCustomerMaintenanceRequest();

            SoapRequest request = new SoapRequest();
            Operation operation = new Operation();

            operation.setInstruction(TestCreateObjectFactory.getInstruction(
                    TestCreateObjectFactory.OP_TYPE_NEW, null));

            Activate activate = new Activate();
            IndividualDetails i = new IndividualDetails();
            
            String name = client.getName() + " " + client.getLastname();
            String companyName = "";
            String idType = client.getIdType();
            String idValue = client.getIdValue().toUpperCase();
            
            if(client.getCompanyName() != null && !client.getCompanyName().trim().isEmpty()){
                name = "";
                companyName = client.getCompanyName();
                idType = client.getCompanyIdType();
                idValue = client.getCompanyIdValue().toUpperCase();                
            }            
            i.setCompanyName(companyName);
            i.setName(name);
            i.setIdentityType(idType);
            i.setIdentityValue(idValue);

            activate.setIndividualDetails(i);

            Address address = new Address();
            address.setCountry(client.getCountry());
            address.setLocality(client.getCity());
            address.setPostalTown(client.getState());
            address.setPostcode(client.getPostalCode());
            address.setStreetName(client.getAddress());

            activate.setAddress(address);

            Contact contact = new Contact();
            contact.setContactName(client.getName() + " " + client.getLastname());
            contact.setContactPhone(client.getPhone());
            contact.setContactMobile(client.getMobile());
            contact.setContactEmail(client.getEmail());

            activate.setContact(contact);

            Services s = new Services();
            s.setDefaultTariffPlan("TAI_CABLE_STANDARD");
            activate.setServices(s);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            CableCustomerMaintenanceRequestResponse result = port.cableCustomerMaintenance(part1);

            //cliente ya existe 
            if (result.getReturn().getActivationCode().equals("NEW-ERR-001")) {
                //TODO: aca tengo que buscar el cliente y poner el re en el cliente local                
                client.setStatus("NEW-ERR-001");
            } else {
                client.setStatus(result.getReturn().getActivateDescription());
                client.setRefCustomerId(result.getReturn().getCustomerId());
            }

        } catch (Exception e) {
            client.setStatus("Error creando cliente " + e.getMessage());
            log.debug(e.getMessage(), e);
        }

        return client;
    }

    public Client updateClient(Client client) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        try {

            UrlLocator locator = new UrlLocator(env);
            URL url = locator.getUpdateClientURL();

            com.wings.xtra.customer.update.CableCustomers service = new com.wings.xtra.customer.update.CableCustomers(url);
            com.wings.xtra.customer.update.CableCustomersPort port = service.getCableCustomersSoapService();
            com.wings.xtra.customer.update.request.CableCustomerMaintenanceRequest part1 = new com.wings.xtra.customer.update.request.CableCustomerMaintenanceRequest();

            com.wings.xtra.customer.update.request.SoapRequest request = new com.wings.xtra.customer.update.request.SoapRequest();
            com.wings.xtra.customer.update.request.Operation operation = new com.wings.xtra.customer.update.request.Operation();
            operation.setInstruction(TestUpdateObjectFactory.getInstruction(
                    TestCreateObjectFactory.OP_TYPE_UPDATE, null));

            operation.getInstruction().setRefCustomerId(client.getRefCustomerId());
            com.wings.xtra.customer.update.request.Activate activate = new com.wings.xtra.customer.update.request.Activate();

            com.wings.xtra.customer.update.request.IndividualDetails i = new com.wings.xtra.customer.update.request.IndividualDetails();
            i.setName(client.getName() + " " + client.getLastname());
            i.setIdentityType(client.getIdType());
            i.setIdentityValue(client.getIdValue());

            activate.setIndividualDetails(i);

            com.wings.xtra.customer.update.request.Address address = new com.wings.xtra.customer.update.request.Address();
            address.setCountry(client.getCountry());
            address.setLocality(client.getCity());
            address.setPostalTown(client.getState());
            address.setPostcode(client.getPostalCode());
            address.setStreetName(client.getAddress());

            activate.setAddress(address);

            com.wings.xtra.customer.update.request.Contact contact = new com.wings.xtra.customer.update.request.Contact();
            contact.setContactName(client.getName() + " " + client.getLastname());
            contact.setContactPhone(client.getPhone());
            contact.setContactMobile(client.getMobile());
            contact.setContactEmail(client.getEmail());

            activate.setContact(contact);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            com.wings.xtra.customer.update.response.CableCustomerMaintenanceRequestResponse result = port.cableCustomerMaintenance(part1);

            client.setStatus(result.getReturn().getActivateDescription());
            //client.setRefCustomerId(result.getReturn().getCustomerId());

        } catch (Exception e) {
            client.setStatus("Error creando cliente");
            log.debug(e.getMessage(), e);
        }

        return client;
    }

    public Client deleteClient(Client client) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getDeleteClientURL();

        com.wings.xtra.customer.delete.CableCustomers service = new com.wings.xtra.customer.delete.CableCustomers(url);
        com.wings.xtra.customer.delete.CableCustomersPort port = service.getCableCustomersSoapService();

        // TODO initialize WS operation arguments here
        com.wings.xtra.customer.delete.request.CableCustomerMaintenanceRequest part1 = new com.wings.xtra.customer.delete.request.CableCustomerMaintenanceRequest();

        com.wings.xtra.customer.delete.request.SoapRequest request = new com.wings.xtra.customer.delete.request.SoapRequest();
        com.wings.xtra.customer.delete.request.Operation operation = new com.wings.xtra.customer.delete.request.Operation();

        operation.setInstruction(TestDeleteObjectFactory.getInstruction(
                TestDeleteObjectFactory.OP_TYPE_DELETE, null));

        operation.getInstruction().setRefCustomerId(client.getRefCustomerId());

        request.setOperation(operation);
        part1.setSoapRequest(request);
        // TODO process result here
        com.wings.xtra.customer.delete.response.CableCustomerMaintenanceRequestResponse result = port.cableCustomerMaintenance(part1);

        System.out.println("Result = " + result.getReturn().getActivateDescription());
        System.out.println("Result = " + result.getReturn().getTransactionId());
        System.out.println("Result = " + result.getReturn().getActivationCode());

        client.setStatus(result.getReturn().getActivateDescription());

        return client;
    }

    public List<Client> getClientList() {
        List<Client> ret = new ArrayList<>();

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getFindClientURL();

        com.wings.xtra.customer.find.CableCustomers service = new com.wings.xtra.customer.find.CableCustomers(url);
        com.wings.xtra.customer.find.CableCustomersPort port = service.getCableCustomersSoapService();

        com.wings.xtra.customer.find.request.CableCustomerMaintenanceRequest part1 = new com.wings.xtra.customer.find.request.CableCustomerMaintenanceRequest();
        com.wings.xtra.customer.find.request.SoapRequest request = new com.wings.xtra.customer.find.request.SoapRequest();
        com.wings.xtra.customer.find.request.Operation operation = new com.wings.xtra.customer.find.request.Operation();

        operation.setInstruction(TestFindObjectFactory.getInstruction(
                TestFindObjectFactory.OP_TYPE_FIND, null));

        Find f = new Find();
        //000 activo
        //300 bloqueado
        //400 baja 
        //f.setStatus("400");
        operation.setFind(f);

        request.setOperation(operation);
        part1.setSoapRequest(request);
        // TODO process result here
        com.wings.xtra.customer.find.response.CableCustomerMaintenanceRequestResponse result = port.cableCustomerMaintenance(part1);

        System.out.println("Result = " + result.getReturn());
        System.out.println("Result = " + result.getReturn().getActivateDescription());
        System.out.println("Result = " + result.getReturn().getTransactionId());
        System.out.println("Result = " + result.getReturn().getActivationCode());
        System.out.println("Result = " + result.getReturn().getClientsList());
        System.out.println("Result = " + result.getReturn().getClientsList().getClient());

        List<com.wings.xtra.customer.find.response.CableCustomerMaintenanceRequestResponse.Return.ClientsList.Client> list = result.getReturn().getClientsList().getClient();
        for (com.wings.xtra.customer.find.response.CableCustomerMaintenanceRequestResponse.Return.ClientsList.Client client : list) {

            Client c = new Client();

            try {                
                //c.setActivationDate(client.getActivateDate());
            } catch (Exception e) {
            }
             
            c.setEmail(client.getContactEmail());
            c.setFax(client.getContactFax());
            c.setMobile(client.getContactMobile());
            c.setPhone(client.getContactPhone());
            c.setIdValue(client.getIdentityValue());
            c.setState(client.getLocality());
            c.setName(client.getName().split(" ")[0]);
            c.setLastname(client.getName().split(" ")[1]);
            //c.setName(client.getPassword());
            c.setCity(client.getPostalTown());
            c.setCountry("España");
            c.setPostalCode(client.getPostcode());
            c.setRefCustomerId(client.getRefCustomerId());
            c.setStatus(client.getStatus());
            c.setAddress(client.getStreetName());

            ret.add(c);

        }

        return ret;
    }
    
}
