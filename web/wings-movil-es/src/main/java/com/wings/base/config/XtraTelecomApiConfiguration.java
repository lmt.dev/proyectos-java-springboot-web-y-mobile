/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.config;

import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XOperatorsService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.service.xtra.XProductsService;
import com.wings.base.service.xtra.XRiskService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 *
 * @author seba
 */
@Configuration
public class XtraTelecomApiConfiguration {
    
    private String env;

    public XtraTelecomApiConfiguration(Environment environment) {
        String[] profiles = environment.getActiveProfiles();
        env = "prod";
        for (String profile : profiles) {
            if (profile.contains("dev")) {
                env = "dev";
                break;
            }
        }
    }

    @Bean
    public XProductsService productService() {
        return new XProductsService(env);
    }

    @Bean
    public XClientService clientService() {
        return new XClientService(env);
    }

    @Bean
    public XRiskService riskService() {
        return new XRiskService(env);
    }

    @Bean
    public XOperatorsService operatorService() {
        return new XOperatorsService(env);
    }

    @Bean
    public XPortService portService() {
        return new XPortService(env);
    }

    @Bean
    public XLineService lineService() {
        return new XLineService(env);
    }
}
