/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.admin;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "process_log")
public class ProcessLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date endDate;
    private String launcher;
    private String info;
    private Long duration;

    @Transient
    private boolean running = false;
    @Transient
    private int max;
    @Transient
    private int step;
    @Transient
    private StringBuilder buffer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLauncher() {
        return launcher;
    }

    public void setLauncher(String launcher) {
        this.launcher = launcher;
    }

    public String getInfo() {
        if (info == null && buffer != null) {
            info = buffer.toString();
        }
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void append(String line) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        buffer.append(line).append(System.lineSeparator());
    }
    
    public void appendLine(String line) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        buffer.append("· ").append(line).append(System.lineSeparator());
    }

    public void replaceText(String line) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }

        buffer.setLength(0);
        appendLine(line);
    }

    public void stop() {
        buffer.append("Proceso finalizado");
        info = buffer.toString();
        this.endDate = new Date();
        this.running = false;
        this.duration = endDate.getTime() - startDate.getTime();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.startDate);
        hash = 83 * hash + Objects.hashCode(this.launcher);
        hash = 83 * hash + Objects.hashCode(this.info);
        hash = 83 * hash + (this.running ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProcessLog other = (ProcessLog) obj;
        if (this.running != other.running) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.launcher, other.launcher)) {
            return false;
        }
        if (!Objects.equals(this.info, other.info)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMax(int max) {
        this.step = 0;
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getStep() {
        return step;
    }

    public void increment() {
        step++;
    }

    public void increment(int inc) {
        step += inc;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

}
