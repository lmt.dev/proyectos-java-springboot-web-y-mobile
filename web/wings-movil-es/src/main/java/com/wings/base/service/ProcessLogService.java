/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.repos.ProcessLogRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ProcessLogService {

    private final Logger log = LoggerFactory.getLogger(ProcessLogService.class);

    @Autowired
    private ProcessLogRepository processLogRepository;
    
    private final Map<Integer, ProcessLog> runninProcess = new HashMap<>();

    public ProcessLog getProccess(Integer processNum) {
        return runninProcess.get(processNum);
    }
    
    public List<ProcessLog> getLastProcess(){    
        Collection<ProcessLog> logs = runninProcess.values();
        
        List<ProcessLog> ret = new ArrayList<>(logs);        
        int size = 12;
        
        if(ret.size() > size){
            return ret.subList(0, size);
        }        
        int fetch = size - ret.size();
        
        if(fetch > 1){
            Page<ProcessLog> page = processLogRepository.findAll(new PageRequest(0, fetch, new Sort(Sort.Direction.DESC,"id")));
            ret.addAll(page.getContent());            
        }
        return ret;
    }

    public ProcessLog createProcess(String user,String name) {
        ProcessLog l = new ProcessLog();
        l.setStartDate(new Date());
        l.setLauncher(user);
        l.setName(name);
        l.setRunning(true);
        l.append("Proceso iniciado");
        int hash = l.hashCode();
        runninProcess.put(hash, l);
        return l;
    }

    @Scheduled(fixedDelay = 30000)
    public void evictProcess() {
        
        List<ProcessLog> toSave = new ArrayList<>();
        for (Iterator<Map.Entry<Integer, ProcessLog>> it = runninProcess.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Integer, ProcessLog> entry = it.next();
            if (!entry.getValue().isRunning()) {
                toSave.add(entry.getValue());
                it.remove();
            }
        }        
        processLogRepository.save(toSave);
        
    }

}
