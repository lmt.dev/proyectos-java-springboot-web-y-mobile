/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.Client;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientsRepository extends JpaRepository<Client, Long>, JpaSpecificationExecutor<Client> {

    Client findOneByIdValue(String idValue);

    List<Client> findByStatus(String status);

    Page<Client> findByBalanceGreaterThanOrderByBalanceDesc(Double balance, Pageable page);

    List<Client> findByBalanceGreaterThan(Double balance);

}
