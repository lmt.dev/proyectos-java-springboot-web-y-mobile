/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author seba
 */
public class FormatUtils {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
    private static final SimpleDateFormat sdfMonth = new SimpleDateFormat("MMMM", new Locale("es", "ES"));
    private static final SimpleDateFormat sdfOnlyDate = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));

    public static String formatMonth(int month) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.MONTH, month - 1);
        return sdfMonth.format(cal.getTime());
    }

    public static String formatHumanDate(Date date) {
        return sdf.format(date);
    }

    public static String formatHumanOnlyDate(Date date) {
        return sdfOnlyDate.format(date);
    }
}
