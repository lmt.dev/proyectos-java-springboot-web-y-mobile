/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.registration;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "registration")
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    private String sessionId;
    private String plan;
    private String planDesc;
    private boolean porta;
    private String nroPorta;
    private String operadorPorta;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaPorta;
    private boolean portaPrepago;
    private String iccid;
    private boolean empresa;
    private String razonSocial;
    private String tipoDocEmpresa;
    private String nroDocEmpresa;
    private String tipoDoc;
    private String nroDoc;
    private String nombre;
    private String apellido;
    private String telefono;
    private String email;
    private String password;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaNac;
    private String direccion;
    private String ciudad;
    private String provincia;
    private String cp;
    private String pais;

    private String scoring;
    private String scoringEmpresa;
    private String pathContrato;
    private String signatureUrl;
    private String matchingData;

    private boolean firmado;
    private String tiendaId;
    private String direccionEnvio;
    private String ciudadEnvio;
    private String provinciaEnvio;
    private String cpEnvio;

    private String referer;
    private String scoringHash;
    private String status;
    private int cantLineas;
    private int cantLineasPorta;
    private String nacionalidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public boolean isPorta() {
        return porta;
    }

    public void setPorta(boolean porta) {
        this.porta = porta;
    }

    public String getNroPorta() {
        return nroPorta;
    }

    public void setNroPorta(String nroPorta) {
        this.nroPorta = nroPorta;
    }

    public String getOperadorPorta() {
        return operadorPorta;
    }

    public void setOperadorPorta(String operadorPorta) {
        this.operadorPorta = operadorPorta;
    }

    public Date getFechaPorta() {
        return fechaPorta;
    }

    public void setFechaPorta(Date fechaPorta) {
        this.fechaPorta = fechaPorta;
    }

    public boolean isPortaPrepago() {
        return portaPrepago;
    }

    public void setPortaPrepago(boolean portaPrepago) {
        this.portaPrepago = portaPrepago;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public boolean isEmpresa() {
        return empresa;
    }

    public void setEmpresa(boolean empresa) {
        this.empresa = empresa;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoDocEmpresa() {
        return tipoDocEmpresa;
    }

    public void setTipoDocEmpresa(String tipoDocEmpresa) {
        this.tipoDocEmpresa = tipoDocEmpresa;
    }

    public String getNroDocEmpresa() {
        return nroDocEmpresa;
    }

    public void setNroDocEmpresa(String nroDocEmpresa) {
        this.nroDocEmpresa = nroDocEmpresa;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getScoring() {
        return scoring;
    }

    public void setScoring(String scoring) {
        this.scoring = scoring;
    }

    public String getScoringEmpresa() {
        return scoringEmpresa;
    }

    public void setScoringEmpresa(String scoringEmpresa) {
        this.scoringEmpresa = scoringEmpresa;
    }

    public String getPathContrato() {
        return pathContrato;
    }

    public void setPathContrato(String pathContrato) {
        this.pathContrato = pathContrato;
    }

    public void setMatchingData(String matchingData) {
        this.matchingData = matchingData;
    }

    public String getMatchingData() {
        return matchingData;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }

    public String getSignatureUrl() {
        return signatureUrl;
    }

    public boolean isFirmado() {
        return firmado;
    }

    public void setFirmado(boolean firmado) {
        this.firmado = firmado;
    }

    public String getTiendaId() {
        return tiendaId;
    }

    public void setTiendaId(String tiendaId) {
        this.tiendaId = tiendaId;
    }

    public String getDireccionEnvio() {
        return direccionEnvio;
    }

    public void setDireccionEnvio(String direccionEnvio) {
        this.direccionEnvio = direccionEnvio;
    }

    public String getCiudadEnvio() {
        return ciudadEnvio;
    }

    public void setCiudadEnvio(String ciudadEnvio) {
        this.ciudadEnvio = ciudadEnvio;
    }

    public String getProvinciaEnvio() {
        return provinciaEnvio;
    }

    public void setProvinciaEnvio(String provinciaEnvio) {
        this.provinciaEnvio = provinciaEnvio;
    }

    public String getCpEnvio() {
        return cpEnvio;
    }

    public void setCpEnvio(String cpEnvio) {
        this.cpEnvio = cpEnvio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getPlanDesc() {
        return planDesc;
    }

    public void setPlanDesc(String planDesc) {
        this.planDesc = planDesc;
    }

    public String getScoringHash() {
        return scoringHash;
    }

    public void setScoringHash(String scoringHash) {
        this.scoringHash = scoringHash;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public int getCantLineas() {
        return cantLineas;
    }

    public void setCantLineas(int cantLineas) {
        this.cantLineas = cantLineas;
    }

    public int getCantLineasPorta() {
        return cantLineasPorta;
    }

    public void setCantLineasPorta(int cantLineasPorta) {
        this.cantLineasPorta = cantLineasPorta;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @Override
    public String toString() {
        return nombre + " " + apellido;
    }

}
