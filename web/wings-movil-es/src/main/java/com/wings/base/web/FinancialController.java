/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.ClientDebtsDAO;
import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.dao.specs.ClientSDebtsReminderSpecs;
import com.wings.base.dao.specs.ClientSDebtsSpecs;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.ClientDebt;
import com.wings.base.domain.client.BalanceSums;
import com.wings.base.domain.client.ClientDebtsReminder;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.enums.BalanceType;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.ClientDebtsReminderRepository;
import com.wings.base.repos.ClientDebtsRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.service.EnvironmentService;
import com.wings.base.service.FinancialServices;
import com.wings.base.utils.NumberUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class FinancialController {

    @Autowired
    private ClientDebtsDAO clientDebtsDAO;

    @Autowired
    private ClientDebtsRepository clientDebtsRepository;

    @Autowired
    private FinancialServices financialServices;

    @Autowired
    private ClientDebtsReminderRepository clientDebtsReminderRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private EnvironmentService environmentService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"views/financial/debts"}, method = {RequestMethod.GET})
    public String debtsIndex(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");
        String sortBy = request.getParameter("sort_by");
        String sortDir = request.getParameter("sort_dir");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        if (sortBy == null) {
            sortBy = "balance";
        }

        if (sortDir == null) {
            sortDir = "desc";
        }

        Sort.Direction dir = Sort.Direction.DESC;
        if (sortDir.equals("asc")) {
            dir = Sort.Direction.ASC;
        }

        PageRequest pr = new PageRequest(page, size, new Sort(dir, sortBy));
        Page<ClientDebt> items = clientDebtsRepository.findAll(ClientSDebtsSpecs.search(q), pr);
        PageWrapper<ClientDebt> pageWrapper = new PageWrapper<>(items, "views/financial/debts");
        BalanceSums sums = clientDebtsDAO.find();

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("total", sums);
        model.addAttribute("sort_by", sortBy);
        model.addAttribute("sort_dir", sortDir);

        return "views/financial/debts";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"api/financial/debts/rebuild"}, method = {RequestMethod.POST})
    public ResponseEntity<String> rebuildBase(Model model, HttpServletRequest request) {
        String ret = financialServices.reGenerateTable();
        return ResponseEntity.ok().body(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = {"api/financial/debts/download"}, method = {RequestMethod.GET})
    public ResponseEntity<InputStreamResource> downloadCsv(Model model, HttpServletRequest request, HttpServletResponse response) {

        String ret = financialServices.getCsv();
        HttpHeaders headers = new HttpHeaders();
        FileInputStream fis;
        try {
            File f = File.createTempFile("clientes_morosos", "");
            FileWriter fw = new FileWriter(f);
            fw.append(ret);
            fw.close();

            fis = new FileInputStream(f);

            headers.setContentType(MediaType.parseMediaType("application/csv"));
            headers.add("Access-Control-Allow-Origin", "*");
            headers.add("Content-Disposition", "filename=" + f.getName() + ".csv");
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);

    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"views/financial/recharge"}, method = {RequestMethod.GET})
    public String rechargeIndex(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "balance"));
        Page<ClientDebtsReminder> items = clientDebtsReminderRepository.findAll(ClientSDebtsReminderSpecs.search(q), pr);
        PageWrapper<ClientDebtsReminder> pageWrapper = new PageWrapper<>(items, "views/financial/recharge");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("status", financialServices.getSenderText());
        model.addAttribute("status", financialServices.getSenderText());
        model.addAttribute("email", financialServices.getEmail());
        model.addAttribute("sms", financialServices.getSms());

        return "views/financial/recharge";
    }

    /**
     *
     * @param model
     * @param request
     * @param remId
     * @return
     */
    @RequestMapping(value = {"views/financial/recharge/sendSMS/{remId}"}, method = {RequestMethod.GET})
    public String rechargeSend(Model model, HttpServletRequest request, @PathVariable("remId") Long remId) {

        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(remId);
        List<ClientLine> lines = linesRepository.findByClientId(rem.getClientId());
        StringBuilder b = new StringBuilder();
        lines.forEach((line) -> {
            b.append("34").append(line.getNumber()).append(",");
        });

        String strLine = b.toString();
        strLine = strLine.substring(0, strLine.length() - 1);

        model.addAttribute("postUrl", "views/financial/recharge/sendSMS/" + remId);
        model.addAttribute("lines", strLine);

        return "views/financial/recharge-send-sms";
    }

    /**
     *
     * @param model
     * @param request
     * @param remId
     * @return
     */
    @RequestMapping(value = {"views/financial/recharge/sendSMS/{remId}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> rechargeSendPost(Model model, HttpServletRequest request, @PathVariable("remId") Long remId) {

        String wich = request.getParameter("seleccion-client");
        String other = request.getParameter("other-number");

        String ret;
        if (wich != null && wich.equals("client")) {
            ret = financialServices.sendSms(remId);
        } else if (other != null && !other.isEmpty()) {
            ret = financialServices.sendSms(remId, other);
        } else {
            ret = "No se envio ningun mensaje";
        }

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param remId
     * @return
     */
    @RequestMapping(value = {"views/financial/recharge/sendEmail/{remId}"}, method = {RequestMethod.GET})
    public String rechargeSendEmail(Model model, HttpServletRequest request, @PathVariable("remId") Long remId) {

        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(remId);
        model.addAttribute("postUrl", "views/financial/recharge/sendEmail/" + remId);
        model.addAttribute("email", rem.getClient().getEmail());

        return "views/financial/recharge-send-email";
    }

    /**
     *
     * @param model
     * @param request
     * @param remId
     * @return
     */
    @RequestMapping(value = {"views/financial/recharge/sendEmail/{remId}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> rechargeSendEmailPost(Model model, HttpServletRequest request, @PathVariable("remId") Long remId) {

        String wich = request.getParameter("seleccion-client");
        String other = request.getParameter("other-email");

        String ret;
        if (wich != null && wich.equals("client")) {
            ret = financialServices.sendEmail(remId);
        } else if (other != null && !other.isEmpty()) {
            ret = financialServices.sendEmail(remId, other);
        } else {
            ret = "No se envio ningun email";
        }

        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"api/financial/reminders/regen"}, method = {RequestMethod.POST})
    public ResponseEntity<String> regenReminders(Model model, HttpServletRequest request) {
        String ret = financialServices.regenReminders();
        return ResponseEntity.ok(ret);
    }

    /**
     *
     * @param model
     * @param request
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping(value = {"/p/{token}"}, method = {RequestMethod.GET})
    public String getPaymentInvoice(Model model, HttpServletRequest request, @PathVariable("token") String token) throws Exception {

        if (token == null) {
            throw new Exception("Token not found");
        }

        ClientDebtsReminder rem = clientDebtsReminderRepository.findFirstByToken(token);
        if (rem == null) {
            throw new Exception("Token not found");
        }

        int visited = rem.getVisited();
        visited++;
        rem.setVisited(visited);
        clientDebtsReminderRepository.save(rem);

        List<Invoice> invoices = invoiceRepository.findByClientId(rem.getClientId());
        double totalRemain = 0;
        double totalPayed = 0;
        double totalInvoice = 0;
        for (Invoice invoice : invoices) {
            double pagado = 0;
            double total = 0;
            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
            for (Balance balance : balances) {
                if (balance.getType().equals(BalanceType.discount.name())
                        && !balance.getDescription().contains("Descuento período")
                        && !balance.getDescription().contains("Pago cuotas anticipadas")) {
                    pagado += balance.getAmount();
                }
                total += balance.getAmount();
            }
            
            invoice.setPayed(pagado * -1);
            invoice.setRemain(total);
            totalRemain += total;
            totalInvoice += invoice.getTotal();
            totalPayed += (pagado * -1);
            
        }

        model.addAttribute("invoices", invoices);
        //model.addAttribute("iframeUrl", request.getScheme() + "://" + request.getServerName() + "/");
        //model.addAttribute("redirectUrl", wingsProperties.getPaymentAuthorizationURL());
        model.addAttribute("redirectUrl", "");
        model.addAttribute("token", token);
        model.addAttribute("totalText", NumberUtils.formatMoney(totalRemain));
        model.addAttribute("total", NumberUtils.formatMoney(totalRemain));

        model.addAttribute("totalRemain", NumberUtils.formatMoney(totalRemain));
        model.addAttribute("totalInvoice", NumberUtils.formatMoney(totalInvoice));
        model.addAttribute("totalPayed", NumberUtils.formatMoney(totalPayed));

        //este redirige a pagos de factura improved
        return "views/support/client-invoices-open";
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = {"api/financial/reminders/send"}, method = {RequestMethod.POST})
    public ResponseEntity<String> markAllToSend() {
        financialServices.send(null);
        return ResponseEntity.ok("Registros marcados para envio");
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = {"api/financial/reminders/stop-send"}, method = {RequestMethod.POST})
    public ResponseEntity<String> munarkAllToSend() {
        financialServices.stopSend();
        return ResponseEntity.ok("Registros desmarcados para envio");
    }

}
