/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.ClientSpecs;
import com.wings.base.dao.specs.HelpPageSpecs;
import com.wings.base.domain.client.BillingAddress;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.cs.HelpPage;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.enums.LineStatus;
import com.wings.base.repos.BillingAddressRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.HelpPagesRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.utils.DniUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.service.xtra.XRiskService;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author seba
 */
@Controller
public class HelpPageController {

    private final Logger log = LoggerFactory.getLogger(HelpPageController.class);

    @Autowired
    private HelpPagesRepository helpPagesRepository;

    @RequestMapping(value = {"views/support/help"}, method = RequestMethod.GET)
    public String getHelps(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }
        
        
        HelpPage one  = null;
        List<HelpPage> items = helpPagesRepository.findAll(HelpPageSpecs.search(q));
        if(items.size() == 1){
            one = items.get(0);
            items = null;
        }

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", one);
        model.addAttribute("pages", items);

        return "views/support/help";
    }
   
    
}
