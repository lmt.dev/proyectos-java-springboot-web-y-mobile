/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.invoice;

import com.wings.base.domain.client.Balance;
import java.util.List;

/**
 *
 * @author seba
 */
public class InvoiceBalanceWrapper {

    private String title;
    private List<Balance> balances;

    public InvoiceBalanceWrapper(String title, List<Balance> balances) {
        this.title = title;
        this.balances = balances;
    }
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }
}
