/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "client_line_plan")
public class PlanChange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long lineId;
    private Long clientId;
    private Long oldPlanId;
    private Long newPlanId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveDate;
    private Boolean applied;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the lineId
     */
    public Long getLineId() {
        return lineId;
    }

    /**
     * @param lineId the lineId to set
     */
    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    /**
     * @return the clientId
     */
    public Long getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the oldPlanId
     */
    public Long getOldPlanId() {
        return oldPlanId;
    }

    /**
     * @param oldPlanId the oldPlanId to set
     */
    public void setOldPlanId(Long oldPlanId) {
        this.oldPlanId = oldPlanId;
    }

    /**
     * @return the newPlanId
     */
    public Long getNewPlanId() {
        return newPlanId;
    }

    /**
     * @param newPlanId the newPlanId to set
     */
    public void setNewPlanId(Long newPlanId) {
        this.newPlanId = newPlanId;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the applied
     */
    public Boolean getApplied() {
        return applied;
    }

    /**
     * @param applied the applied to set
     */
    public void setApplied(Boolean applied) {
        this.applied = applied;
    }

}
