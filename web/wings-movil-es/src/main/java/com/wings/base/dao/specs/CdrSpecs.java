/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Cdr;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class CdrSpecs {

    public static Specification<Cdr> cdrSearch(String client, Date from, Date to, Boolean priced, String type, String origin) {

        return (Root<Cdr> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (client != null) {
                predicates.add(builder.equal(root.get("client"), client));
            }

            if (from != null && to != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("date"), from));
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("date"), to));
            }

            if (priced != null) {
                if (priced) {
                    predicates.add(builder.equal(root.get("price"), 0).not());
                } else {
                    predicates.add(builder.equal(root.get("price"), 0));
                }
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("cdrType"), type));
            }
            
            if (origin != null) {
                predicates.add(builder.equal(root.get("origin"), origin));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
