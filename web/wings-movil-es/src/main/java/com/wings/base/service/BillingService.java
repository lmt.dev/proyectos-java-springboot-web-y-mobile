/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.WingsProperties;
import com.wings.base.dao.BalanceDAO;
import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.BalanceSums;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.payment.PaymentAuthorization;
import com.wings.base.domain.payment.PaymentCallback;
import com.wings.base.domain.payment.PaymentRequest;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.process.SystemStatus;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.PaymentAuthorizationRepository;
import com.wings.base.repos.PaymentCallbackRepository;
import com.wings.base.repos.PaymentRequestRepository;
import com.wings.base.utils.NumberUtils;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class BillingService {

    private final Logger log = LoggerFactory.getLogger(BillingService.class);

    @Autowired
    private PaymentAuthorizationRepository paymentAuthorizationRepository;

    @Autowired
    private TefPayService tefPayService;

    @Autowired
    private BalanceDAO balanceDAO;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentCallbackRepository paymentCallbackRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ProcessLogService processLogService;

    private final WingsProperties properties;

    public BillingService(WingsProperties properties) {
        this.properties = properties;
    }

    public void processPaymentForInvoices(String launcher, List<Invoice> invoices, SystemStatus status) {

        String merchantUrl = properties.getPay().getUrlAutoCallback();
        String merchantKey = properties.getPay().getMerchantKey();

        ProcessLog pl = processLogService.createProcess(launcher, "Proceso de cobros de facturas");
        pl.setMax(invoices.size());

        int percent = invoices.size() / 100;
        int processed = 0;

        for (Invoice invoice : invoices) {

            if (status != null) {
                status.setPercent(processed++ / percent);
            }

            if (invoice.getStatus().equals(InvoiceStatus.payed.name())) {
                pl.appendLine("Factura con estado Pagado, no procesando - id:" + invoice.getId());
                continue;
            }

            double charged = 0;
            Client c = invoice.getClient();
            Collection<ClientLine> lines = c.getLines();
            for (ClientLine line : lines) {

                List<PaymentRequest> alreadyRequested = paymentRequestRepository.findByInvoiceIdAndLineId(invoice.getId(), line.getId());
                //si ya se cobro, hago un continue
                if (!alreadyRequested.isEmpty()) {
                    boolean keepGoing = true;
                    for (PaymentRequest paymentRequest : alreadyRequested) {
                        if (paymentRequest.getStatus() != null && paymentRequest.getStatus().equals("Aceptada") && !paymentRequest.getTransactionType().equals("4")) {
                            keepGoing = false;
                            //charged += NumberUtils.round(Double.parseDouble(paymentRequest.getAmount()) / 100.0d, 2);
                        }
                    }
                    if (!keepGoing) {
                        pl.appendLine("Linea ya cobrada, continuando " + line.getId());
                        continue;
                    }
                }

                BalanceSums sum = balanceDAO.find(c.getId().toString(), line.getId().toString(), invoice.getMonth(), invoice.getYear());
                if (sum.getTotal() <= 0) {
                    pl.appendLine("Linea sin elementos en el balance " + line.getId());
                    continue;
                }

                Long regId = line.getRegistrationId();
                PaymentAuthorization payAuth = paymentAuthorizationRepository.findByLineIdAndClientIdAndActive(line.getId(), line.getClientId(), Boolean.TRUE);
                if (payAuth == null) {
                    log.error("Payment auth null for line " + line.getId());
                    continue;
                }

                PaymentRequest request = new PaymentRequest();
                request.setStatus("created");
                request.setRegistrationId(regId);
                request.setRequestDate(new Date());
                request.setInvoiceId(invoice.getId());
                request.setLineId(line.getId());
                request.setClientId(c.getId());
                request.setName(c.toString());

                //save so we get the id
                paymentRequestRepository.save(request);

                double rounded = NumberUtils.round(sum.getTotal(), 2);

                //transform to cents for Pay request
                String amount = String.valueOf((int) (rounded * 100));
                amount = amount.replaceAll("\\.", "");

                request.setAmount(amount);

                request.setDate(payAuth.getDate());
                request.setPanMask(payAuth.getPanMask());
                request.setCardId(payAuth.getCardId());
                request.setAuthCode(payAuth.getAuthCode());
                request.setMatchingData(payAuth.getMatchingData());

                request.setMerchantCode(properties.getPay().getMerchantCode());
                request.setRecurrency("1"); //probar con cero si no anda
                request.setTransactionType("8"); //cobro recurrente
                request.setUrl(merchantUrl + "/" + request.getId());
                request.setSignature(DigestUtils.sha1Hex(request.getAmount() + request.getMerchantCode() + request.getMatchingData() + request.getUrl() + merchantKey));

                //save again before send
                paymentRequestRepository.save(request);

                tefPayService.postChargeAmmount(request);

                //save again after send
                paymentRequestRepository.save(request);

                //create balance
                if (request.getStatus().equals("Aceptada")) {

                    charged += rounded;

                    Balance b = new Balance();
                    b.setAmount(rounded * -1);
                    b.setClientId(c.getId());
                    b.setControl("auto-payment-" + invoice.getMonth() + "-" + invoice.getYear() + "-" + line.getId());
                    b.setDate(new Date());
                    b.setDescription("Cobro automático");
                    b.setLineId(line.getId());
                    b.setMonth(invoice.getMonth());
                    b.setYear(invoice.getYear());
                    b.setObservation("Cobro automatico a traves de TefPay request " + request.getId());
                    b.setPaymentMethod("auto tefpay");
                    b.setPaymentRef(request.getId().toString());
                    b.setType(BalanceType.discount.name());

                    balanceRepository.save(b);

                } else {
                    pl.appendLine("Pago con error para la factura " + invoice.getId() + " para la linea " + line.getId() + " - estado pago: " + request.getStatus());
                }
            }

            BalanceSums sums = balanceDAO.find(invoice.getClientId().toString(), null, invoice.getMonth(), invoice.getYear());
            double totalBalance = NumberUtils.round(sums.getTotal(), 2);
            double totalInvoice = NumberUtils.round(invoice.getTotal(), 2);

            if (totalBalance == 0) {
                invoice.setStatus(InvoiceStatus.payed.name());
            } else if (totalBalance == totalInvoice) {
                invoice.setStatus(InvoiceStatus.unpayed.name());
            } else if (totalBalance < totalInvoice) {
                invoice.setStatus(InvoiceStatus.partial.name());
            } else {
                invoice.setStatus(InvoiceStatus.review.name());
            }

            /*if (roundedCharged == roundedTotal) {
                invoice.setStatus(InvoiceStatus.payed.name());
                pl.appendLine("Factura con cobro aceptado marcando como pagada - id:" + invoice.getId());
            } else {

                double diff = NumberUtils.round(roundedTotal - roundedCharged, 2);
                if (diff > 0.0d && diff < 1.0d) {
                    //pago total pero diferencia de decimales
                    invoice.setStatus(InvoiceStatus.payed.name());
                } else if (diff == roundedTotal) {
                    //no se cobro nada de la factura    
                    invoice.setStatus(InvoiceStatus.unpayed.name());
                } else if (diff > 1 && diff < roundedTotal) {
                    //se cobro parcialmente    
                    invoice.setStatus(InvoiceStatus.partial.name());
                } else {
                    //caso a revisar
                    invoice.setStatus(InvoiceStatus.review.name());
                }
            }*/
            invoiceRepository.save(invoice);
        }
        pl.stop();
    }

    public String refundInvoice(String launcher, Invoice invoice) {

        List<PaymentRequest> requests = paymentRequestRepository.findByInvoiceIdAndTransactionType(invoice.getId(), "8");

        StringBuilder b = new StringBuilder();

        b.append("<pre>");
        for (PaymentRequest request : requests) {

            if (request.getRefunded()) {
                continue;
            }

            PaymentRequest refundRequest = new PaymentRequest();
            refundRequest.setTransactionType("4");
            refundRequest.setMerchantCode(request.getMerchantCode());
            refundRequest.setMatchingData(request.getMatchingData());
            refundRequest.setPanMask(request.getPanMask());
            refundRequest.setDate(request.getDate());
            refundRequest.setAmount(request.getAmount());
            refundRequest.setName(request.getName());

            refundRequest.setRequestDate(new Date());
            refundRequest.setClientId(request.getClientId());
            refundRequest.setLineId(request.getLineId());
            refundRequest.setInvoiceId(request.getInvoiceId());
            refundRequest.setRegistrationId(request.getRegistrationId());

            paymentRequestRepository.save(refundRequest);

            PaymentCallback callback = paymentCallbackRepository.findFirstByRequestIdAndMessageOrderByIdDesc(request.getId(), "Aceptada");
            if (callback == null) {
                b.append("No se encontro callback ").append(System.lineSeparator());
                continue;
            }

            refundRequest = tefPayService.postRefundAmmount(refundRequest, callback);
            paymentRequestRepository.save(refundRequest);

            //si todo salio bien
            if (refundRequest.getStatus().equals("Aceptada")) {
                request.setRefunded(true);
                request.setRefundRequestId(refundRequest.getId());
            }

            b.append("Line ")
                    .append(refundRequest.getLineId())
                    .append(" ")
                    .append(refundRequest.getStatus())
                    .append(System.lineSeparator());
        }

        boolean refundedComplete = true;
        for (PaymentRequest request : requests) {
            refundedComplete = refundedComplete && request.getRefunded();
        }

        if (refundedComplete) {
            invoice.setStatus(InvoiceStatus.refunded.name());
            invoiceRepository.save(invoice);
            b.append("Invoice ").append(invoice.getId()).append(" refunded");
        }
        b.append("</pre>");

        return b.toString();
    }

}
