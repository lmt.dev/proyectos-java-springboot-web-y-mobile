/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.enums;

/**
 *
 * @author seba
 */
public enum OrderStatus {

    replacement_created,
    replacement_sended,
    replacement_error,
    replacement_received,
    replacement_delivered,
    replacement_deliver_error,
    created,
    sended,
    error,
    received,
    delivered,
    deliver_error
}
