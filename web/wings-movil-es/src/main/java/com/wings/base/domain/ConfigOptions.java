/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain;

/**
 *
 * @author seba
 */
public class ConfigOptions {
    
    
    /**
     * CDR last downloaded file, set to 
     * 0 if you need to redownload all files
     * set to a particular timestamp to get files
     * from that date
     */
    public static final String CDR_LAST_TIMESTAMP = "file.lastTimestamp";
    
    
    /**
     * UPS last downloaded file, value it's a timestamp, set to 0 to redownload
     * all files
     */
    public static final String UPS_LAST_TIMESTAMP = "ups.lastTimestamp";
    
}
