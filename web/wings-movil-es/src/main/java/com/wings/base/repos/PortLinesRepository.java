/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.registration.PortLine;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
public interface PortLinesRepository extends JpaRepository<PortLine, Long> {

    List<PortLine> findByRegistrationId(Long registrationId);

    @Transactional
    int deleteByRegistrationId(Long registrationId);

    @Query(
            value = "select * from port_lines where port_number like %?1% \n#pageable\n",
            countQuery = "select count(1) from port_lines where port_number like %?1% \n#pageable\n",
            nativeQuery = true
    )
    Page<PortLine> findByQuery(String q, Pageable pageable);

}
