/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.dao.specs.CdrSpecs;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Cdr;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.client.PlanChange;
import com.wings.base.domain.cs.Message;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.invoice.InvoiceBalanceWrapper;
import com.wings.base.domain.invoice.InvoiceCdrWrapper;
import com.wings.base.domain.invoice.InvoiceItem;
import com.wings.base.domain.payment.PaymentCallback;
import com.wings.base.domain.payment.PaymentRequest;
import com.wings.base.domain.xtra.XtraLineStatus;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.CdrRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.CsMessageRepository;
import com.wings.base.repos.InvoiceItemsRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PaymentCallbackRepository;
import com.wings.base.repos.PaymentRequestRepository;
import com.wings.base.repos.PlanChangesRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.InvoiceUtils;
import com.wings.base.utils.NumberUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class FixesService {

    @Autowired
    private PlanRepository plansRepository;
    @Autowired
    private LinesRepository lineRepository;
    @Autowired
    private PlanChangesRepository planChangesRepository;
    @Autowired
    private CsMessageRepository csMessageRepository;
    @Autowired
    private XLineService xLineService;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private PaymentRequestRepository paymentRequestRepository;
    @Autowired
    private PaymentCallbackRepository paymentCallbackRepository;
    @Autowired
    private ClientsRepository clientsRepository;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private CdrRepository cdrRepository;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private InvoiceItemsRepository invoiceItemsRepository;

    private final Logger log = LoggerFactory.getLogger(FixesService.class);

    /**
     * Arregla las facturas de planes de 20gb poniendo los balances de voz a 0€
     *
     * @return
     */
    public String fixInvoices20GB() {

        List<ClientLine> lines = lineRepository.findByPlanId(51l);
        for (ClientLine line : lines) {

            //si no esta activa, paso
            if (!line.isActive()) {
                log.error("Linea no activa, paso");
                continue;
            }

            log.error("Procesando linea " + line.getId());

            //si tiene cambio de plan, facturas desde fecha efectivo de cambio de plan
            List<PlanChange> changes = planChangesRepository.findByLineId(line.getId());
            Date effDate = null;
            if (!changes.isEmpty()) {
                for (PlanChange change : changes) {
                    if (change.getNewPlanId() == 51l && change.getOldPlanId() != 51l) {
                        effDate = change.getEffectiveDate();
                        log.error("Efectivo desde el cambio de plan");
                        break;
                    }
                }

                if (effDate == null) {
                    log.error("Efectivo desde el alta de linea, pero tiene changes");
                    effDate = line.getActivationDate();
                }
            } else {
                effDate = line.getActivationDate();
                log.error("Efectivo desde el alta de linea");
            }

            int month = DateUtils.getMonthForDate(effDate);
            int year = DateUtils.getYearForDate(effDate);
            while (month <= 12) {

                log.error("Arreglando mes " + month);
                boolean regenerar = false;

                List<Balance> bals = balanceRepository.findByYearAndMonthAndTypeAndLineId(year, month, BalanceType.consume.name(), line.getId());
                if (bals.isEmpty()) {
                    log.error("Sin balances para el mes " + month);
                } else {
                    for (Balance b : bals) {

                        if (b.getDescription().contains("Voz")) {
                            if (b.getAmount() != 0) {
                                b.setAmount(0.0d);
                                balanceRepository.save(b);
                                log.error("Balance arreglado");
                                regenerar = true;
                            }
                        }

                    }

                    if (regenerar) {
                        List<Invoice> invoices = invoiceRepository.findByMonthAndYearAndClientId(month, year, line.getClientId());
                        for (Invoice invoice : invoices) {
                            regenerateInvoice(invoice);
                            log.error("Factura regenerada");
                        }
                    }
                }
                month++;
            }

        }

        return "ok";
    }

    /**
     * Arregla las facturas con cambio de plan y hace effectivo el cambio en la
     * linea
     *
     * @return
     */
    public String fixInvoicesChangePlan() {

        log.error("Iniciando cambio de plan");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM", new Locale("es", "ES"));
        List<PlanChange> changes = planChangesRepository.findAll();
        for (PlanChange change : changes) {

            //si son de los errores continue
            if (change.getNewPlanId() == 51l && change.getOldPlanId() == 51l) {
                log.error("Fix 20GB continuando");
                continue;
            }

            //si ya se aplico
            if (change.getApplied()) {
                log.error("ya aplicado continuando");
                continue;
            }

            //primero cambianos el plan en la linea
            Date now = new Date();
            Date effDate = change.getEffectiveDate();

            //si ya se hizo effectivo
            if (now.getTime() > effDate.getTime()) {
                Long clientId = change.getClientId();
                Long lineId = change.getLineId();
                Plan plan = plansRepository.findOne(change.getNewPlanId());

                //cambio el plan en la linea
                ClientLine line = lineRepository.findOne(lineId);
                line.setPlanId(plan.getId());
                line.setPlanDescription(plan.getDescription());
                lineRepository.save(line);
                log.error("Guardamos linea");

                //seteo el cambio como effectivo
                change.setApplied(Boolean.TRUE);
                planChangesRepository.save(change);
                log.error("Guardamos cambio aplicado");

                int month = DateUtils.getMonthForDate(effDate);
                int year = DateUtils.getYearForDate(effDate);
                Date from = DateUtils.getStartDateForPeriod(month, year);

                while (month <= 12) {

                    log.error("Arreglando mes " + month);
                    List<Balance> bals = balanceRepository.findByYearAndMonthAndTypeAndLineId(year, month, BalanceType.plan.name(), lineId);
                    for (Balance b : bals) {

                        b.setAmount(plan.getPrice());
                        b.setDescription(StringUtils.capitalize(sdf.format(from)) + " - " + plan.getDescription());
                        b.setControl(plan.getShortName() + "-" + month + "-" + year);
                        balanceRepository.save(b);
                        log.error("Balance arreglado");

                    }

                    List<Invoice> invoices = invoiceRepository.findByMonthAndYearAndClientId(month, year, clientId);
                    for (Invoice invoice : invoices) {
                        regenerateInvoice(invoice);
                        log.error("Factura regenerada");
                    }

                    month++;
                }
            } else {
                log.error("ahora es menor que fecha de effective");
            }
        }

        log.error("Finalizado el cambio ed planes");

        return "ok";
    }

    /**
     * Revisa todos los balances de todas las facturas y arregla, solamente 2
     * facturas tienen mal en la voz
     *
     * @return
     */
    public String fixBalancesForAllPeriod() {
        Integer[] months = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        Integer[] years = new Integer[]{2017, 2018};

        for (Integer year : years) {
            for (Integer month : months) {
                fixBalancesforPeriod(month, year);
            }
        }
        return "ok";
    }

    /**
     * Arregla las facturas que tienen mal los datos de voz en relacion al cdr
     *
     * @param month
     * @param year
     * @return
     */
    public String fixBalancesforPeriod(int month, int year) {

        Date from = DateUtils.getStartDateForPeriod(month, year);
        Date to = DateUtils.getEndDateForPeriod(month, year);

        List<ClientLine> lines = lineRepository.findAll();
        for (ClientLine line : lines) {

            if (line.getNumber() == null) {
                continue;
            }
            //obtengo los consumos
            //Consumos
            List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(line.getClient().getRefCustomerId(), from, to, null, null, line.getNumber().toString()));
            double totalDatos = 0;
            double totalVoz = 0;
            double totalSms = 0;

            for (Cdr cdr : cdrs) {
                switch (cdr.getCdrType()) {
                    case "DAT":
                        totalDatos += cdr.getPrice().doubleValue();
                        break;
                    case "VOZ":
                        totalVoz += cdr.getPrice().doubleValue();
                        break;
                    default:
                        totalSms += cdr.getPrice().doubleValue();
                        break;
                }
            }

            totalDatos = NumberUtils.round(totalDatos, 4);
            totalVoz = NumberUtils.round(totalVoz, 4);
            totalSms = NumberUtils.round(totalSms, 4);

            double currentDatos = 0;
            double currentVoz = 0;
            double currentSms = 0;
            //ahora los balances
            List<Balance> balances = balanceRepository.findByYearAndMonthAndTypeAndLineId(year, month, BalanceType.consume.name(), line.getId());
            if (balances.isEmpty()) {
                continue;
            }

            for (Balance balance : balances) {

                switch (balance.getDescription()) {
                    case "Voz":
                        currentVoz += balance.getAmount();
                        break;
                    case "Datos":
                        currentDatos += balance.getAmount();
                        break;
                    case "SMS":
                        currentSms += balance.getAmount();
                        break;
                }
            }

            boolean fixVoice = false;
            boolean fixDat = false;
            boolean fixSms = false;

            if (currentDatos != totalDatos) {
                double diff = currentDatos - totalDatos;
                diff = Math.abs(diff);
                if (diff > 20) {
                    log.error("Arreglar datos " + month + "/" + year + " " + line.getId() + " " + diff);
                    fixDat = true;
                }
            }

            if (currentVoz != totalVoz) {
                double diff = currentVoz - totalVoz;
                diff = Math.abs(diff);
                if (diff > 50) {
                    log.error("Arreglar voz " + month + "/" + year + " " + line.getId() + " " + diff);
                    fixVoice = true;
                }
            }

            if (currentSms != totalSms) {
                double diff = currentSms - totalSms;
                diff = Math.abs(diff);
                if (diff > 10) {
                    log.error("Arreglar sms " + month + "/" + year + " " + line.getId() + " " + diff);
                    fixSms = true;
                }
            }

            if (fixVoice) {
                for (Balance balance : balances) {
                    if (balance.getDescription().equals("Voz")) {
                        balance.setAmount(totalVoz);
                        balanceRepository.save(balance);

                        List<Invoice> invoices = invoiceRepository.findByMonthAndYearAndClientId(month, year, line.getClientId());
                        for (Invoice invoice : invoices) {
                            regenerateInvoice(invoice);
                            invoiceRepository.save(invoice);
                        }
                    }
                }
            }
        }
        return "Ok";
    }

    public String checkPlans() {

        int fixed = 0;
        List<ClientLine> lines = lineRepository.findAll();
        for (ClientLine line : lines) {

            if (line.getNumber() != null) {

                List<XtraLineStatus> status = xLineService.getLineStatus(line);
                for (XtraLineStatus stat : status) {

                    String current = stat.getProductProfile();
                    String myside = line.getPlan().getProviderProfile();

                    if (!current.equals(myside)) {
                        log.error("CHECK " + line.getNumber() + " " + current + " " + myside);
                        log.error("DATE " + stat.getActivateDate() + " " + stat.getStatusDate());
                    }

                    /*PlanChange change = new PlanChange();
                    change.setClientId(line.getClientId());
                    change.setCreationDate(new Date());
                    change.setEffectiveDate(DateUtils.getStartDateForNextPeriod());
                    change.setLineId(line.getId());
                    change.setNewPlanId(51l);
                    change.setOldPlanId(51l);
                    change.setApplied(Boolean.TRUE);

                    planChangesRepository.save(change);

                    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                    Message message = new Message();
                    message.setDate(new Date());
                    message.setUser(((User) auth.getPrincipal()).getUsername());
                    message.setClientId(line.getClientId());
                    message.setLineId(line.getId());
                    message.setLine(line.getNumber().toString());
                    message.setText("Fix de Plan en operador");

                    csMessageRepository.save(message);

                    xLineService.changeLinePlan(line, line.getPlan());*/
                    fixed++;
                }
            }
        }

        return "Fixed " + fixed + " de " + lines.size();
    }

    /**
     * Revisa las facturas que tienen error de datos entre la suma del balance y
     * el total de la factura
     *
     * @return
     */
    public String checkInvoiceBalance() {

        List<Invoice> invoices = invoiceRepository.findAll();

        for (Invoice invoice : invoices) {

            double invoiceTotal = invoice.getTotal();
            double balanceTotal = 0.0d;

            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())), new Sort(Sort.Direction.ASC, "id"));
            for (Balance balance : balances) {
                if (balance.getType().equals(BalanceType.discount.name()) && balance.getDescription().contains("Cobro")) {
                    continue;
                }
                balanceTotal += balance.getAmount();
            }

            balanceTotal = NumberUtils.round(balanceTotal, 2);
            if (invoiceTotal != balanceTotal) {
                double diff = balanceTotal - invoiceTotal;
                if (diff < 0.5) {
                    continue;
                }
                log.error("Regenerando " + invoice.getId());
                regenerateInvoice(invoice);
            }
        }

        log.error("Facturas regeneradas");
        return "Facturas regeneradas";
    }

    /**
     * Cambia la numeracion de todas las facturas para que sean secuenciales
     * desde el 1 de cada año
     *
     * @return
     */
    public String fixInvoiceNumbers() {

        List<Invoice> invoices = invoiceRepository.findAll();
        log.error("Arreglando numeracion");
        Collections.sort(invoices, Comparator.comparing((Invoice p) -> p.getId()).thenComparing(p -> p.getYear()).thenComparing(p -> p.getMonth()));
        for (Invoice invoice : invoices) {
            int year = invoice.getYear();
            long number = invoiceRepository.getMaxPeriodNumber(year);
            number++;
            invoice.setPeriodNumber(number);
            invoice.setInvoiceNumber(InvoiceUtils.getInvoiceNumber(number, year));
            invoiceRepository.save(invoice);
        }
        log.error("Finalizado arreglando numeracion");

        log.error("Regenerando pdfs");
        //generate invoice PDFs
        for (Invoice invoice : invoices) {
            Date from = DateUtils.getStartDateForPeriod(invoice.getMonth(), invoice.getYear());
            Date to = DateUtils.getEndDateForPeriod(invoice.getMonth(), invoice.getYear());
            Client c = invoice.getClient();
            Collection<ClientLine> lines = c.getLines();
            List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
            List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

            for (ClientLine line : lines) {
                List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())), new Sort(Sort.Direction.ASC, "id"));
                if (balances.isEmpty()) {
                    continue;
                }

                Iterator<Balance> iterator = balances.iterator();
                while (iterator.hasNext()) {
                    Balance next = iterator.next();
                    if (next.getControl().contains("auto-payment-")) {
                        iterator.remove();
                    }
                }

                balancesWrappers.add(new InvoiceBalanceWrapper(line.getNumber().toString(), balances));
                List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
                cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
            }

            String invoicePath = invoiceService.generateInvoicePDF(invoice, balancesWrappers, cdrWrappers);
            if (invoicePath != null && !invoicePath.isEmpty()) {
                invoice.setFilePath(invoicePath);
            }
        }

        invoiceRepository.save(invoices);
        log.error("Numeracion areglada, pdf regenerados");

        return "Numeracion areglada, pdf regenerados";
    }

    /**
     * Revisa las facturas que estan en review y las modifica para asignarles el
     * estado correcto
     *
     * @return
     */
    public String checkInvoices() {

        List<Invoice> invoices = invoiceRepository.findAll();
        for (Invoice invoice : invoices) {

            //revisamos las que estan en review
            if (invoice.getStatus().equals(InvoiceStatus.created.name())) {

                List<PaymentRequest> requests = paymentRequestRepository.findByInvoiceId(invoice.getId());
                if (requests.isEmpty()) {
                    invoice.setStatus(InvoiceStatus.unpayed.name());
                    continue;
                }

                double total = NumberUtils.round(invoice.getTotal(), 2);
                double charged = 0.0d;
                double refunded = 0.0d;
                for (PaymentRequest request : requests) {
                    //si el pago esta aceptado
                    if (request.getStatus().equals("Aceptada") && !request.getTransactionType().equals("4")) {
                        //revisamos el callback
                        PaymentCallback callback = paymentCallbackRepository.findFirstByRequestIdOrderByIdDesc(request.getId());
                        if (callback != null && callback.getMessage() != null && callback.getMessage().equals("Aceptada")) {
                            charged += Double.parseDouble(request.getAmount()) / 100;
                        } else {
                            log.error("Revisar request sin callback de cliente " + invoice.getClient() + " invoice id " + invoice.getId());
                        }
                    }

                    if (request.getStatus().equals("Aceptada") && request.getTransactionType().equals("4")) {
                        refunded += Double.parseDouble(request.getAmount()) / 100;
                    }
                }

                double diff = NumberUtils.round(total - charged, 2);
                diff = Math.abs(diff); // skip -0.01

                if (diff == 0) {
                    //pago total
                    invoice.setStatus(InvoiceStatus.payed.name());
                } else if (diff > 0.0d && diff < 1.0d) {
                    //pago total pero diferencia de decimales
                    invoice.setStatus(InvoiceStatus.payed.name());
                } else if (diff == total) {
                    //no se cobro nada de la factura    
                    invoice.setStatus(InvoiceStatus.unpayed.name());
                } else if (diff > 1 && diff < total) {
                    //se cobro parcialmente    
                    invoice.setStatus(InvoiceStatus.partial.name());
                } else {
                    //caso a revisar
                    invoice.setStatus(InvoiceStatus.review.name());
                }

                if (refunded > 0) {
                    if (refunded == charged) {
                        invoice.setStatus(InvoiceStatus.refunded.name());
                    } else {
                        invoice.setStatus(InvoiceStatus.refunded_partial.name());
                    }
                }
            }
        }

        invoiceRepository.save(invoices);
        log.error("Terminado");
        return "Procesado";
    }

    /**
     * Regenera la factura de acuerdo a los datos del balance funcion usada por
     * un metodo mas arriba
     *
     * @param invoice
     * @return
     */
    private String regenerateInvoice(Invoice invoice) {

        List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
        if (balances.isEmpty()) {
            return "Error";
        }

        Map<String, InvoiceItem> items = new HashMap<>();
        double grandTotal = 0.0d;
        for (Balance balance : balances) {

            if (balance.getDescription().contains("Cobro")) {
                continue;
            }

            grandTotal += balance.getAmount();

            if (items.containsKey(balance.getType())) {
                InvoiceItem item = items.get(balance.getType());
                item.setAmount(item.getAmount() + balance.getAmount());
            } else {
                InvoiceItem item = new InvoiceItem();
                String concept = "";
                String type = balance.getType();
                int order = 0;
                if (type.equals(BalanceType.plan.name())) {
                    concept = "Cuotas";
                    order = 1;
                } else if (type.equals(BalanceType.discount.name())) {
                    concept = "Descuentos y promociones";
                    order = 4;
                } else if (type.equals(BalanceType.extra.name())) {
                    concept = "Otros cargos";
                    order = 3;
                } else if (type.equals(BalanceType.consume.name())) {
                    concept = "Consumos";
                    order = 2;
                }

                item.setOrder(order);
                item.setConcept(concept);
                item.setAmount(balance.getAmount());
                item.setInvoiceId(invoice.getId());
                items.put(balance.getType(), item);
            }
        }

        List<InvoiceItem> toSort = new LinkedList<>(items.values());
        Collections.sort(toSort, (InvoiceItem o1, InvoiceItem o2) -> o1.getOrder().compareTo(o2.getOrder()));
        invoice.setItems(toSort);

        List<InvoiceItem> existingItems = invoiceItemsRepository.findByInvoiceId(invoice.getId());
        invoiceItemsRepository.delete(existingItems);

        invoiceItemsRepository.save(invoice.getItems());
        invoice.setTotal(grandTotal);
        invoiceRepository.save(invoice);

        //generate invoice PDFs
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
        Date from = DateUtils.getStartDateForPeriod(invoice.getMonth(), invoice.getYear());
        Date to = DateUtils.getEndDateForPeriod(invoice.getMonth(), invoice.getYear());

        Client c = invoice.getClient();
        Collection<ClientLine> lines = c.getLines();
        List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
        List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

        for (ClientLine line : lines) {
            balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())), new Sort(Sort.Direction.ASC, "id"));
            if (balances.isEmpty()) {
                continue;
            }
            String title = line.getNumber().toString() + " (fecha activación: " + sdf.format(line.getActivationDate()) + ")";
            balancesWrappers.add(new InvoiceBalanceWrapper(title, balances));
            List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
            cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
        }

        String invoicePath = invoiceService.generateInvoicePDF(invoice, balancesWrappers, cdrWrappers);
        if (invoicePath != null && !invoicePath.isEmpty()) {
            invoice.setFilePath(invoicePath);
        }

        invoiceRepository.save(invoice);
        return "Factura regenerada";
    }

    /**
     * Se uso para arreglar las lineas que tenian el plan solo de 20gb de
     * navegacion
     *
     * @return
     */
    public String fix20() {

        Plan newPlan = plansRepository.findOne(51l);
        int fixed = 0;
        List<ClientLine> lines = lineRepository.findAll();
        //List<ClientLine> lines = new ArrayList<>();
        //lines.add(lineRepository.findOne(70l));

        for (ClientLine line : lines) {

            if (line.getNumber() != null && line.getCancelDate() == null) {

                List<XtraLineStatus> status = xLineService.getLineStatus(line);
                for (XtraLineStatus stat : status) {

                    String current = stat.getProductProfile();
                    String myside = line.getPlan().getProviderProfile();

                    if (current.equals("CM_POSTPAGO_NAVEGA_20_G")) {
                        log.error("planXXX - line " + line.getNumber());
                        log.error("planXXX - current " + current);
                        log.error("planXXX - myside " + myside);

                        PlanChange change = new PlanChange();
                        change.setClientId(line.getClientId());
                        change.setCreationDate(new Date());
                        change.setEffectiveDate(DateUtils.getStartDateForNextPeriod());
                        change.setLineId(line.getId());
                        change.setNewPlanId(51l);
                        change.setOldPlanId(51l);
                        change.setApplied(Boolean.TRUE);

                        planChangesRepository.save(change);

                        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                        Message message = new Message();
                        message.setDate(new Date());
                        message.setUser(((User) auth.getPrincipal()).getUsername());
                        message.setClientId(line.getClientId());
                        message.setLineId(line.getId());
                        message.setLine(line.getNumber().toString());
                        message.setText("Fix de Plan en operador");

                        csMessageRepository.save(message);

                        //xLineService.changeLinePlan(line, newPlan);
                        fixed++;
                    }

                }
            }
        }

        return "Fixed " + fixed + " de " + lines.size();
    }

    /**
     * sin uso, para comparar lo que tengo con lo que hay en extra
     *
     * @return
     */
    public String getClientLinePlans() {

        StringBuilder b = new StringBuilder();
        b.append("<pre>");

        List<Client> clients = clientsRepository.findAll();
        for (Client client : clients) {

            Collection<ClientLine> lines = client.getLines();

            for (ClientLine line : lines) {

                if (line.isActive() && line.getCancelDate() == null) {

                    if (line.getNumber() != null) {

                        List<XtraLineStatus> status = xLineService.getLineStatus(line);
                        if (status.isEmpty()) {
                            continue;
                            //b.append(" No se encontraron estados en operador").append(System.lineSeparator());
                        }

                        for (XtraLineStatus stat : status) {

                            String current = stat.getProductProfile();
                            String myside = line.getPlan().getProviderProfile();
                            String activacion = stat.getActivateDate();
                            String statusDate = stat.getStatusDate();

                            if (!myside.equals(current)) {
                                b.append("<b>Cliente: ").append(client).append("</b>").append(System.lineSeparator());
                                b.append("\tLinea: ");
                                b.append(line.getNumber()).append(System.lineSeparator());
                                
                                b.append("\t\tPlan Wings ->").append(myside).append(System.lineSeparator());
                                b.append("\t\tPlan Extra ->").append(current).append(System.lineSeparator());
                                b.append("\t\tActivacion ->").append(activacion).append(System.lineSeparator());
                                b.append("\t\tEstado ->").append(statusDate).append(System.lineSeparator());
                                
                                b.append("----------------------------------------------------").append(System.lineSeparator());
                            }

                        }
                    } else {
                        //b.append(" Linea sin numero").append(System.lineSeparator());
                    }
                } else {
                    //b.append(" de baja o no activa").append(System.lineSeparator());
                }
            }

            
        }

        b.append("</pre>");

        return b.toString();
    }

}
