/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author seba
 */
@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {

    private static final String[] RESOURCE_LOCATIONS = {
        "classpath:/resources/static",
        "classpath:/resources/static/js",
        "classpath:/resources/static/bower_components",
        "classpath:/resources/static/css",
        "classpath:/resources/static/fonts",
        "classpath:/static/js",
        "classpath:/static/bower_components",
        "classpath:/static/css",
        "classpath:/static/fonts",
        "classpath:/static/",
        "classpath:/public/"};

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //registry.addViewController("/").setViewName("index");
        //registry.addViewController("/views/main.html").setViewName("views/main");
        //registry.addViewController("/views/test.html").setViewName("views/test");
        registry.addViewController("/login").setViewName("login");

        /*registry.addViewController("/store/index").setViewName("store/index");
        registry.addViewController("/views/components/buttons.html").setViewName("views/components/buttons");
        registry.addViewController("/faq").setViewName("faq");
        registry.addViewController("/mobiles").setViewName("mobiles");
        registry.addViewController("/electronics-appliances").setViewName("electronics-appliances");
        registry.addViewController("/test").setViewName("test");        
        registry.addViewController("/single").setViewName("single");        */
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (!registry.hasMappingForPattern("/webjars/**")) {
            registry.addResourceHandler("/webjars/**").addResourceLocations(
                    "classpath:/META-INF/resources/webjars/");
        }
        if (!registry.hasMappingForPattern("/**")) {
            registry.addResourceHandler("/**").addResourceLocations(
                    RESOURCE_LOCATIONS);
        }
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

}
