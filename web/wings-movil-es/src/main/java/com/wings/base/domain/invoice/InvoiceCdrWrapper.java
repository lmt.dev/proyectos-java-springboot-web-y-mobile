/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.invoice;

import com.wings.base.domain.client.Cdr;
import java.util.List;

/**
 *
 * @author seba
 */
public class InvoiceCdrWrapper {

    private String title;
    private List<Cdr> cdrs;

    public InvoiceCdrWrapper(String title, List<Cdr> cdrs) {
        this.title = title;
        this.cdrs = cdrs;
    }

    public List<Cdr> getCdrs() {
        return cdrs;
    }

    public void setCdrs(List<Cdr> cdrs) {
        this.cdrs = cdrs;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
