/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author seba
 */
@ControllerAdvice
public class AppErrorHandlingController {

    private final Logger log = LoggerFactory.getLogger(AppErrorHandlingController.class);

    @ExceptionHandler(Exception.class)
    public String handleError(Model model, HttpServletRequest request, Exception ex) {

        log.error("Request: " + request.getRequestURL() + " raised " + ex);        
        log.error("Error:",ex);
                
        Map<String, String> map = new HashMap<>();
        map.put("status", "500");
        map.put("error", "Error en servidor");
        map.put("message",ex.getClass().getSimpleName() + " " + ex.getMessage());
        model.addAttribute("errorMap", map);
        
        return "errors/error";
    }

}
