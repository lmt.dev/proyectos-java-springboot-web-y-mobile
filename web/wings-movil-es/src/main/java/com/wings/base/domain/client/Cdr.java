/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.utils.NumberUtils;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "cdr")
public class Cdr {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String cdrType;
    private String client;
    private String origin;
    private String originOperator;
    private String destination;
    private String destinationOperator;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    @Temporal(javax.persistence.TemporalType.TIME)
    private Date time;
    private Long duration;
    private Long data1;
    private Long data2;
    private Double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCdrType() {
        return cdrType;
    }

    public void setCdrType(String cdrType) {
        this.cdrType = cdrType;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginOperator() {
        return originOperator;
    }

    public void setOriginOperator(String originOperator) {
        this.originOperator = originOperator;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationOperator() {
        return destinationOperator;
    }

    public void setDestinationOperator(String destinationOperator) {
        this.destinationOperator = destinationOperator;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getData1() {
        return data1;
    }

    public void setData1(Long data1) {
        this.data1 = data1;
    }

    public Long getData2() {
        return data2;
    }

    public void setData2(Long data2) {
        this.data2 = data2;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDataText() {
        return NumberUtils.formatData(data1 + data2);
    }

    public String getPriceText() {
        return NumberUtils.formatMoney(price);
    }

    public String getVoiceText() {
        String ret;
        if (duration > 60) {
            ret = NumberUtils.round(duration / 60d, 4) + " m.";
        } else {
            ret = duration + " s.";
        }
        return ret;
    }

}
