/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
@ConfigurationProperties("wings")
public class WingsProperties {

    private String usrphoto;
    private String logo;
    private String contrato;
    private String invoiceStorage;
    private String logoFull;
    private String invoiceJasper;
    
    private String paymentAuthorizationCallback;
    private String paymentAuthorizationURL;
    private String paymentAuthorizationURLBase;
       
    private Pay pay = new Pay();
    private Contract contract = new Contract();
    private Signature signature = new Signature();
    private Scoring scoring = new Scoring();
    private Ups ups = new Ups();
    private Brand brand = new Brand();

    public static class Pay {

        private String url;
        private String merchantCode;
        private String merchantKey;
        private String merchantUrl;
        private String merchantUrlOK;
        private String merchantUrlKO;
        private String merchantName;
        private String merchantLogo;
        private String merchantDescription;
        private String merchantLang;
        private String merchantCurrency;
        private String merchantTransactionType;
        private String urlAutoCallback;
        private String urlAuto;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrlAuto() {
            return urlAuto;
        }

        public void setUrlAuto(String urlAuto) {
            this.urlAuto = urlAuto;
        }

        public String getMerchantCode() {
            return merchantCode;
        }

        public void setMerchantCode(String merchantCode) {
            this.merchantCode = merchantCode;
        }

        public String getMerchantKey() {
            return merchantKey;
        }

        public void setMerchantKey(String merchantKey) {
            this.merchantKey = merchantKey;
        }

        public String getMerchantUrl() {
            return merchantUrl;
        }

        public void setMerchantUrl(String merchantUrl) {
            this.merchantUrl = merchantUrl;
        }

        public String getMerchantUrlOK() {
            return merchantUrlOK;
        }

        public void setMerchantUrlOK(String merchantUrlOK) {
            this.merchantUrlOK = merchantUrlOK;
        }

        public String getMerchantUrlKO() {
            return merchantUrlKO;
        }

        public void setMerchantUrlKO(String merchantUrlKO) {
            this.merchantUrlKO = merchantUrlKO;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getMerchantLogo() {
            return merchantLogo;
        }

        public void setMerchantLogo(String merchantLogo) {
            this.merchantLogo = merchantLogo;
        }

        public String getMerchantDescription() {
            return merchantDescription;
        }

        public void setMerchantDescription(String merchantDescription) {
            this.merchantDescription = merchantDescription;
        }

        public String getMerchantLang() {
            return merchantLang;
        }

        public void setMerchantLang(String merchantLang) {
            this.merchantLang = merchantLang;
        }

        public String getMerchantCurrency() {
            return merchantCurrency;
        }

        public void setMerchantCurrency(String merchantCurrency) {
            this.merchantCurrency = merchantCurrency;
        }

        public String getMerchantTransactionType() {
            return merchantTransactionType;
        }

        public void setMerchantTransactionType(String merchantTransactionType) {
            this.merchantTransactionType = merchantTransactionType;
        }

        public String getUrlAutoCallback() {
            return urlAutoCallback;
        }

        public void setUrlAutoCallback(String urlAutoCallback) {
            this.urlAutoCallback = urlAutoCallback;
        }

    }

    public static class Contract {

        private String direccionWings;
        private String telefonoWings;
        private String condiciones;
        private String dest;

        public String getDireccionWings() {
            return direccionWings;
        }

        public void setDireccionWings(String direccionWings) {
            this.direccionWings = direccionWings;
        }

        public String getTelefonoWings() {
            return telefonoWings;
        }

        public void setTelefonoWings(String telefonoWings) {
            this.telefonoWings = telefonoWings;
        }

        public String getCondiciones() {
            return condiciones;
        }

        public void setCondiciones(String condiciones) {
            this.condiciones = condiciones;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }

    }

    public static class Signature {

        private String url;
        private String username;
        private String password;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Scoring {

        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Ups {

        private String url;
        private String key;
        private String user;
        private String pass;
        private String shipperNumber;
        private String ordersDir;
        private String reportPath;
        private String from;
        private String to;

        private String ftpServer;
        private String ftpUser;
        private String ftpPass;

        private String trackingUrl;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getShipperNumber() {
            return shipperNumber;
        }

        public void setShipperNumber(String shipperNumber) {
            this.shipperNumber = shipperNumber;
        }

        public String getOrdersDir() {
            return ordersDir;
        }

        public void setOrdersDir(String ordersDir) {
            this.ordersDir = ordersDir;
        }

        public String getReportPath() {
            return reportPath;
        }

        public void setReportPath(String reportPath) {
            this.reportPath = reportPath;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getFtpServer() {
            return ftpServer;
        }

        public void setFtpServer(String ftpServer) {
            this.ftpServer = ftpServer;
        }

        public String getFtpUser() {
            return ftpUser;
        }

        public void setFtpUser(String ftpUser) {
            this.ftpUser = ftpUser;
        }

        public String getFtpPass() {
            return ftpPass;
        }

        public void setFtpPass(String ftpPass) {
            this.ftpPass = ftpPass;
        }

        public String getTrackingUrl() {
            return trackingUrl;
        }

        public void setTrackingUrl(String trackingUrl) {
            this.trackingUrl = trackingUrl;
        }
    }

    public static class Brand {

        private String form;
        private String storage;
        private String signature;
        private String requestEmail;

        public String getForm() {
            return form;
        }

        public void setForm(String form) {
            this.form = form;
        }

        public String getStorage() {
            return storage;
        }

        public void setStorage(String storage) {
            this.storage = storage;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public String getRequestEmail() {
            return requestEmail;
        }

        public void setRequestEmail(String requestEmail) {
            this.requestEmail = requestEmail;
        }

    }

    public Pay getPay() {
        return pay;
    }

    public void setPay(Pay pay) {
        this.pay = pay;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    public Scoring getScoring() {
        return scoring;
    }

    public void setScoring(Scoring scoring) {
        this.scoring = scoring;
    }

    public String getUsrphoto() {
        return usrphoto;
    }

    public void setUsrphoto(String usrphoto) {
        this.usrphoto = usrphoto;
    }

    public Ups getUps() {
        return ups;
    }

    public void setUps(Ups ups) {
        this.ups = ups;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getInvoiceStorage() {
        return invoiceStorage;
    }

    public void setInvoiceStorage(String invoiceStorage) {
        this.invoiceStorage = invoiceStorage;
    }

    public String getLogoFull() {
        return logoFull;
    }

    public void setLogoFull(String logoFull) {
        this.logoFull = logoFull;
    }

    public String getInvoiceJasper() {
        return invoiceJasper;
    }

    public void setInvoiceJasper(String invoiceJasper) {
        this.invoiceJasper = invoiceJasper;
    }

    public String getPaymentAuthorizationCallback() {
        return paymentAuthorizationCallback;
    }

    public void setPaymentAuthorizationCallback(String paymentAuthorizationCallback) {
        this.paymentAuthorizationCallback = paymentAuthorizationCallback;
    }

    public String getPaymentAuthorizationURL() {
        return paymentAuthorizationURL;
    }

    public void setPaymentAuthorizationURL(String paymentAuthorizationURL) {
        this.paymentAuthorizationURL = paymentAuthorizationURL;
    }

    public String getPaymentAuthorizationURLBase() {
        return paymentAuthorizationURLBase;
    }

    public void setPaymentAuthorizationURLBase(String paymentAuthorizationURLBase) {
        this.paymentAuthorizationURLBase = paymentAuthorizationURLBase;
    }

    
}
