/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.xtra.UrlLocator;
import com.wings.xtra.porting.getop.CableMsisdnsPorting;
import com.wings.xtra.porting.getop.CableMsisdnsPortingPort;
import com.wings.xtra.porting.getop.TestCreateObjectFactory;
import com.wings.xtra.porting.getop.request.MsisdnsPortingMaintenanceRequest;
import com.wings.xtra.porting.getop.request.Operation;
import com.wings.xtra.porting.getop.request.SoapRequest;
import com.wings.xtra.porting.getop.response.MsisdnsPortingMaintenanceRequestResponse;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author seba
 */
public class XOperatorsService {

    private final Logger log = LoggerFactory.getLogger(XOperatorsService.class);

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    public static final String resId = "F33563";
    public static final String resPin = "3DlIbN8z3rFQv";

    private final String env;

    public XOperatorsService(String env) {
        this.env = env;
    }

    public String getOperators() {

        StringBuilder b = new StringBuilder();
        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getCreatePortingURL();

        CableMsisdnsPorting service = new CableMsisdnsPorting(url);
        CableMsisdnsPortingPort port = service.getCableMsisdnsPortingSoapService();

        try {

            // TODO initialize WS operation arguments here
            MsisdnsPortingMaintenanceRequest part1 = new MsisdnsPortingMaintenanceRequest();

            SoapRequest request = new SoapRequest();
            Operation operation = new Operation();

            operation.setInstruction(TestCreateObjectFactory.getInstruction());

            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            MsisdnsPortingMaintenanceRequestResponse result = port.msisdnsPortingMaintenance(part1);

            b.append("<pre>");
            List<MsisdnsPortingMaintenanceRequestResponse.Return.Operadores.Operador> list = result.getReturn().getOperadores().getOperador();
            for (MsisdnsPortingMaintenanceRequestResponse.Return.Operadores.Operador o : list) {
                b.append("* ").append(o.getIdOperador()).append(" - ").append(o.getDescripcion());
                b.append(System.lineSeparator());                
            }
            b.append("</pre>");

        } catch (Exception ex) {
            b.append("<b>Error:</b>").append(ex.getMessage());
            b.append("</pre>");
        }

        return b.toString();
    }

}
