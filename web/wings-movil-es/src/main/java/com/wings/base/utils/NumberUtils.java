/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

/**
 *
 * @author seba
 */
public class NumberUtils {

    private static final Random rand = new Random();
    private static final DecimalFormat dataFormat = new DecimalFormat("0.00");
    private static final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(new Locale("es", "ES"));

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String formatMoney(double money) {
        return moneyFormat.format(round(money, 4));
    }

    public static String formatData(long size) {

        String hrSize;
        double k = size / 1024d;
        double m = size / 1048576d;
        double g = size / 1073741824d;
        double t = size / (1073741824d * 1024);

        if (t > 1) {
            hrSize = dataFormat.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dataFormat.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dataFormat.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dataFormat.format(m).concat(" KB");
        } else {
            hrSize = dataFormat.format(size).concat(" B");
        }

        return hrSize;
    }

    public static int randInt1to9999() {
        int min = 1;
        int max = 9999;
        return rand.nextInt((max - min) + 1) + min;
    }

    public static String getRandomString(Integer number) {

        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                .build();
        String string = generator.generate(number);

        return string;
    }
}
