/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.ContractSpecs;
import com.wings.base.domain.ContractStatusResponse;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationFile;
import com.wings.base.enums.ContractStatus;
import com.wings.base.process.LineProcess;
import com.wings.base.repos.ContractsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.RegistrationFilesRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.service.ContractService;
import com.wings.base.service.ContractSignatureService;
import com.wings.base.service.EmailService;
import com.wings.base.utils.MapUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ContractsController {

    private final Logger log = LoggerFactory.getLogger(ContractsController.class);

    @Autowired
    private ContractsRepository contractsRepository;

    @Autowired
    private ContractService contractService;

    @Autowired
    private ContractSignatureService contractSignatureService;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationFilesRepository registrationFilesRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private LineProcess lineProcess;

    @RequestMapping(value = {"views/clients/contracts"}, method = RequestMethod.GET)
    public String getRegistrarions(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        //Page<Contract> items = contractsRepository.findAll(new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate")));
        Page<Contract> items = contractsRepository.findAll(
                ContractSpecs.contractSearch(q, status),
                new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate")));

        PageWrapper<Contract> pageWrapper = new PageWrapper<>(items, "views/clients/contracts");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);
        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);

        return "views/clients/contracts";
    }

    @RequestMapping(value = {"views/clients/contracts/{id}/form"}, method = RequestMethod.GET)
    public String getLineForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Contract c = contractsRepository.findOne(id);
        model.addAttribute("title", "Contrato");
        model.addAttribute("subTitle", c.toString());
        model.addAttribute("elements", MapUtils.getFormElements(c));

        return "views/common/view-form";
    }
    
    @RequestMapping(value = {"views/clients/contracts/{id}/edit"}, method = RequestMethod.GET)
    public String getEditForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Contract c = contractsRepository.findOne(id);
        model.addAttribute("client", c.getClient());
        model.addAttribute("title", "Editar Contrato");
        model.addAttribute("subTitle", c.toString());
        model.addAttribute("elements", MapUtils.getFormElements(c));

        return "views/clients/contract-edit";
    }

    @RequestMapping(value = {"api/clients/contracts/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getContractPdf(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Contract contract = contractsRepository.findOne(id);
        File pdfFile = new File(contract.getPath().trim());

        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + contract.getFileName());
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/contract/gen"}, method = RequestMethod.POST)
    public ResponseEntity<String> genContracts(Model model, HttpServletRequest request) {

        new Thread(() -> {
            lineProcess.createContracts();
        }).start();

        return ResponseEntity.ok("Proceso ejecutado, revise el panel de logs");
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/contract/send"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendContracts(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        if (ids.isEmpty()) {
            return ResponseEntity.ok("Debe seleccionar al menos un contrato");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        new Thread(() -> {
            List<Contract> contracts = contractsRepository.findAll(ids);
            contractSignatureService.resendToSignature(contracts, launcher);
            contractsRepository.save(contracts);
        }).start();

        return ResponseEntity.ok("Proceso ejecutado, revise el panel de logs");
    }

    @ResponseBody
    @RequestMapping(value = {"/api/clients/contract/status"}, method = RequestMethod.POST)
    public ResponseEntity<String> testContractStatus(Model model, HttpServletRequest request) {

        new Thread(() -> {
            lineProcess.checkContractStatus();
        }).start();

        return ResponseEntity.ok("Request sended , check process log");
    }

    @ResponseBody
    @RequestMapping(value = {"/api/clients/contract/sendtest"}, method = RequestMethod.POST)
    public ResponseEntity<String> sendTestContract(Model model, HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        new Thread(() -> {
            lineProcess.sendTestContract(launcher);
        }).start();

        return ResponseEntity.ok("Request sended , check process log");
    }

}
