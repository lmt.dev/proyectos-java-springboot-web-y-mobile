/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.Order;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface OrdersRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {

    List<Order> findByRegistrationId(Long registrationId);

    List<Order> findByRegistrationId(Long registrationId, Sort sort);

    List<Order> findByStatus(String status);

    @Query(
            value = "select * from orders where telefono like %?1 OR status like %?1 \n#pageable\n",
            countQuery = "select count(1) from orders where telefono like %?1 OR status like %?1 \n#pageable\n",
            nativeQuery = true
    )
    Page<Order> findByQuery(String q, Pageable pageable);

    Order findBytrackingNumber(String trackingNumber);

    List<Order> findByParentAndStatus(Long parent, String status);

    List<Order> findByParent(Long parent);

}
