/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.ClientSpecs;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.BillingAddress;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.client.PlanChange;
import com.wings.base.domain.cs.Message;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.enums.LineStatus;
import com.wings.base.repos.BillingAddressRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.CsMessageRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.PlanChangesRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.utils.DniUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.service.xtra.XClientService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XPortService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class ClientsController {

    private final Logger log = LoggerFactory.getLogger(ClientsController.class);

    @Autowired
    private RegistrationCompleteRepository completeRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private PortLinesRepository portLinesRepository;

    @Autowired
    private PlanRepository plansRepository;

    @Autowired
    private BillingAddressRepository billingAddressRepository;

    @Autowired
    private LinesRepository lineRepository;

    @Autowired
    private XClientService xClientService;

    @Autowired
    private XPortService xPortService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private CsMessageRepository csMessageRepository;

    @Autowired
    private PlanChangesRepository planChangesRepository;

    @RequestMapping(value = {"views/clients/clients"}, method = RequestMethod.GET)
    public String getClients(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));

        Page<Client> items = clientsRepository.findAll(ClientSpecs.clientSearch(q, status), pr);

        PageWrapper<Client> pageWrapper = new PageWrapper<>(items, "views/clients/clients");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);

        return "views/clients/clients";
    }

    @RequestMapping(value = {"views/clients/clients/{id}/form/{type}"}, method = RequestMethod.GET)
    public String getClientForm(Model model, HttpServletRequest request, @PathVariable("id") Long id, @PathVariable(name = "type", required = false) String type) {

        Client client = clientsRepository.findOne(id);

        String view = "views/common/view-form";
        String title = "Cliente";
        if (type != null && type.equals("edit")) {
            view = "views/common/edit-form";
            title = "Formulario edicion cliente";
        }

        model.addAttribute("title", title);
        model.addAttribute("subTitle", client.toString());
        model.addAttribute("elements", MapUtils.getFormElements(client));
        model.addAttribute("client", client);
        return view;
    }

    @RequestMapping(value = {"views/clients/lines/{id}/plan-change"}, method = RequestMethod.GET)
    public String getPlanChange(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        ClientLine line = lineRepository.findOne(id);
        List<Plan> plans = plansRepository.findAll();
        model.addAttribute("line", line);
        model.addAttribute("plans", plans);
        model.addAttribute("title", "Cambio de plan para la linea " + line.getNumber());
        String postUrl = "views/clients/lines/" + line.getId() + "/plan-change";
        model.addAttribute("postUrl", postUrl);

        return "views/clients/lines/plan-change";

    }

    @RequestMapping(value = {"views/clients/lines/{id}/plan-change"}, method = RequestMethod.POST)
    public String postPlanChange(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Long id) {

        String newPlanId = request.getParameter("planId");
        Plan newPlan = plansRepository.findOne(Long.parseLong(newPlanId));
        ClientLine line = lineRepository.findOne(id);

        PlanChange change = new PlanChange();
        change.setApplied(Boolean.FALSE);
        change.setClientId(line.getClientId());
        change.setCreationDate(new Date());
        change.setEffectiveDate(DateUtils.getStartDateForNextPeriod());
        change.setLineId(id);
        change.setNewPlanId(newPlan.getId());
        change.setOldPlanId(line.getPlanId());

        planChangesRepository.save(change);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Message message = new Message();
        message.setDate(new Date());
        message.setUser(((User) auth.getPrincipal()).getUsername());
        message.setClientId(line.getClientId());
        message.setLineId(line.getId());
        message.setLine(line.getNumber().toString());
        message.setText("Cambio de plan de " + line.getPlan() + " a " + newPlan + " - Efectivo el dia " + FormatUtils.formatHumanDate(change.getEffectiveDate()));

        csMessageRepository.save(message);

        String response = xLineService.changeLinePlan(line, newPlan, change.getEffectiveDate());

        redirectAttributes.addAttribute("id", line.getClientId());
        redirectAttributes.addAttribute("msg", response + "\n Los cambios de veran reflejados en la linea al inicio del mes calendario");

        return "redirect:/#views/support/clients?msg={msg}&q=id:{id}";
    }

    @RequestMapping(value = {"views/clients/{id}/edit"}, method = RequestMethod.GET)
    public String editClientform(Model model, HttpServletRequest request, @PathVariable("id") Long clientId) {

        Client c = clientsRepository.findOne(clientId);

        model.addAttribute("title", "Editar cliente");
        model.addAttribute("subTitle", c.toString());
        model.addAttribute("elements", MapUtils.getFormElementsFiltered(c, Arrays.asList("email", "idValue", "phone", "mobile", "referer", "name", "lastname", "idType", "companyName", "companyIdType", "companyIdValue","address","city","state","postalCode")));
        model.addAttribute("client", c);
        String postUrl = "views/clients/" + c.getId() + "/edit";
        model.addAttribute("postUrl", postUrl);

        return "views/common/edit-form";
    }

    @RequestMapping(value = {"views/clients/{id}/edit"}, method = RequestMethod.POST)
    public String postClientform(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Long clientId) {

        StringBuilder b = new StringBuilder();
        b.append("Modificacion de usuario:").append(System.lineSeparator());

        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String mobile = request.getParameter("mobile");
        String idValue = request.getParameter("idValue");
        String idType = request.getParameter("idType");
        String name = request.getParameter("name");
        String lastname = request.getParameter("lastname");
        String referer = request.getParameter("referer");
        String companyName = request.getParameter("companyName");
        String companyIdType = request.getParameter("companyIdType");
        String companyIdValue = request.getParameter("companyIdValue");        
        String address = request.getParameter("address");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String postalCode = request.getParameter("postalCode");

        Client c = clientsRepository.findOne(clientId);
        String antes = new ReflectionToStringBuilder(c).toString();

        if (referer != null && !referer.equals(c.getReferer())) {
            Collection<ClientLine> lines = c.getLines();
            lines.forEach((line) -> {
                ClientLine l = lineRepository.findOne(line.getId());
                l.setReferer(referer);
                lineRepository.save(l);
            });
        }

        c.setIdValue(idValue);
        c.setPhone(phone);
        c.setEmail(email);
        c.setReferer(referer);
        c.setIdType(idType);
        c.setName(name);
        c.setLastname(lastname);
        c.setMobile(mobile);
        c.setCompanyName(companyName);
        c.setCompanyIdType(companyIdType);
        c.setCompanyIdValue(companyIdValue);
        c.setAddress(address);
        c.setCity(city);
        c.setState(state);
        c.setPostalCode(postalCode);
               
        String despues = new ReflectionToStringBuilder(c).toString();
        String diff = com.wings.base.utils.StringUtils.getDiff(antes, despues);
        b.append(System.lineSeparator()).append(diff);

        clientsRepository.save(c);

        if (!diff.isEmpty()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(clientId);
            message.setText(b.toString());
            csMessageRepository.save(message);
        }

        redirectAttributes.addAttribute("id", c.getId());
        redirectAttributes.addAttribute("msg", "Cliente modificado");

        return "redirect:/#views/support/clients?msg={msg}&q=id:{id}";
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/create"}, method = RequestMethod.POST)
    public ResponseEntity<String> createClients(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        List<Long> ids = new ArrayList<>();
        StringBuilder ret = new StringBuilder();
        ret.append("<br/>");

        int createdClients = 0;
        int createdLines = 0;

        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        List<RegistrationComplete> registrations = new ArrayList<>();
        for (Long id : ids) {
            RegistrationComplete reg = completeRepository.findOne(id);
            registrations.add(reg);
        }

        for (RegistrationComplete reg : registrations) {

            Client client = clientsRepository.findOneByIdValue(reg.getNroDoc());

            //el cliente no existe
            if (client == null) {
                client = new Client();

                client.setName(WordUtils.capitalizeFully(reg.getNombre()));
                client.setLastname(WordUtils.capitalizeFully(reg.getApellido()));
                client.setMobile(reg.getTelefono());
                client.setPhone(reg.getTelefono());
                client.setIdType(DniUtil.getDniString(reg.getTipoDoc()));
                client.setIdValue(StringUtils.upperCase(reg.getNroDoc()));
                client.setBirthDate(reg.getFechaNac());

                client.setAddress(WordUtils.capitalizeFully(reg.getDireccion()));
                client.setCity(WordUtils.capitalizeFully(reg.getCiudad(), new char[]{'´', '\''}));
                client.setState(WordUtils.capitalizeFully(reg.getProvincia()));
                client.setContract(reg.getPathContrato());
                client.setCountry("España"); //TODO: ver para mas adelante
                client.setNationality(WordUtils.capitalizeFully(reg.getNacionalidad()));
                client.setCreationDate(new Date());
                client.setEmail(reg.getEmail());
                client.setPostalCode(reg.getCp());

                client.setCompanyName(WordUtils.capitalizeFully(reg.getRazonSocial()));
                client.setCompanyIdType(DniUtil.getDniString(reg.getTipoDocEmpresa()));
                client.setCompanyIdValue(StringUtils.upperCase(reg.getNroDocEmpresa()));

                client.setReferer(reg.getReferer());
                client.setRegistrationId(reg.getId());

                clientsRepository.save(client);

                ret.append("Cliente ").append(client).append(" creado").append("<br/>");
                createdClients++;
            } else {
                ret.append("El liente ").append(client).append(" ya existe, continuando proceso...").append("<br/>");
            }

            //busco las direcciones de facturacion
            List<BillingAddress> addresses = billingAddressRepository.findByClientId(client.getId());

            BillingAddress toUse = new BillingAddress();
            toUse.setAddress(reg.getDireccion());
            toUse.setCity(reg.getCiudad());
            toUse.setClientId(client.getId());
            toUse.setCountry(reg.getPais());
            toUse.setCp(reg.getCp());
            toUse.setState(reg.getProvincia());

            //si esta vacia la creo
            if (addresses.isEmpty() || !addresses.contains(toUse)) {
                billingAddressRepository.save(toUse);
            } else {
                for (BillingAddress a : addresses) {
                    if (a.equals(toUse)) {
                        toUse = a;
                        break;
                    }
                }
            }

            Plan plan = plansRepository.findByShortName(reg.getPlan());

            List<ClientLine> clientLines = new ArrayList<>();
            //creo las lineas a portar del cliente
            int porta = reg.getCantLineasPorta();
            if (porta > 0) {
                List<PortLine> portLines = portLinesRepository.findByRegistrationId(reg.getId());
                for (PortLine portLine : portLines) {

                    ClientLine cl = lineRepository.findByNumber(Long.valueOf(portLine.getPortNumber()));
                    if (cl != null) {
                        ret.append("La linea ").append(portLine.getPortNumber()).append(" ya existe, continuando proceso...").append("<br/>");
                        continue;
                    }

                    ClientLine line = new ClientLine();
                    line.setClientId(client.getId());
                    line.setRegistrationId(reg.getId());
                    line.setCreationDate(new Date());
                    line.setInternalState(LineStatus.created_local.name());
                    line.setNumber(Long.valueOf(portLine.getPortNumber()));
                    line.setPlanDescription(plan.getDescription());
                    line.setPlanId(plan.getId());
                    line.setPort(true);
                    line.setPortPlannedDate(portLine.getPortDate());
                    line.setPortIccid(portLine.getIccid());
                    line.setPortOperator(portLine.getPortOperator());
                    line.setBillingAddress(toUse.getId());

                    clientLines.add(line);
                    createdLines++;
                    ret.append("Linea ").append(portLine.getPortNumber()).append(" creada ").append("<br/>");
                    portLine.setStatus("processed");
                }
                //update line status
                portLinesRepository.save(portLines);
            }

            //creo las lineas nuevas
            int nuevas = reg.getCantLineas();
            List<ClientLine> alreadyCreatedLines = lineRepository.findByRegistrationId(reg.getId());
            if (alreadyCreatedLines != null && !alreadyCreatedLines.isEmpty()) {
                if (alreadyCreatedLines.size() >= nuevas) {
                    ret.append("Las lineas ya estan creadas para ese registro").append("<br/>");
                    nuevas = 0;
                } else {
                    nuevas = nuevas - alreadyCreatedLines.size();
                    ret.append("Algunas lineas ya estan creadas para ese registro, lineas a crear ").append(nuevas).append("<br/>");
                }
            }

            for (int i = 0; i < nuevas; i++) {

                ClientLine line = new ClientLine();
                line.setClientId(client.getId());
                line.setCreationDate(new Date());
                line.setInternalState(LineStatus.created_local.name());
                line.setPlanDescription(plan.getDescription());
                line.setPlanId(plan.getId());
                line.setPort(false);
                line.setBillingAddress(toUse.getId());
                line.setRegistrationId(reg.getId());

                createdLines++;
                ret.append("Linea creada ").append("<br/>");
                clientLines.add(line);
            }

            lineRepository.save(clientLines);
            reg.setStatus("processed");
            completeRepository.save(reg);
        }

        ret.append("<br/>Lineas creadas ").append(createdLines);
        ret.append("<br/>Clientes creados ").append(createdClients).append(" de ").append(ids.size());
        return ResponseEntity.ok(ret.toString());
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/create/xtra"}, method = RequestMethod.POST)
    public ResponseEntity<String> createClientsXtra(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {

        List<Long> ids = new ArrayList<>();
        String ret = "";
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (NumberFormatException e) {
            }
        }

        for (Long id : ids) {

            Client client = clientsRepository.findOne(id);
            if (client.getRefCustomerId() == null || client.getRefCustomerId().isEmpty()) {
                xClientService.createClient(client);
                clientsRepository.save(client);

                //pregunto de nuevo para ver si se creo correctamente en xtra
                if (client.getRefCustomerId() == null || client.getRefCustomerId().isEmpty()) {
                    ret += "<br/>El cliente " + client + " no se pudo crear en el operador";
                    ret += "<br/>" + client.getStatus();
                    continue;
                }
            }

            List<ClientLine> lines = lineRepository.findByClientId(client.getId());
            for (ClientLine line : lines) {
                Plan plan = plansRepository.findOne(line.getPlanId());
                if (line.getIccid() == null) {
                    continue;
                }
                if (!line.getInternalState().equals(LineStatus.order_delivered.name())) {
                    continue;
                }

                if (line.isPort()) {
                    xPortService.portClient(line, client, plan);
                } else {
                    xLineService.createLine(client, line, plan);
                }
                lineRepository.save(line);

                ret += "<br/>" + line.getId() + "--" + line.getExternalState();
            }
        }

        //new Lines

        /*List<ClientLine> newLines = lineRepository.findByPort(Boolean.FALSE);
        for (ClientLine newLine : newLines) {
            
            if(!newLine.getInternalState().equals("local_create")){
                continue;
            }         
            
            Client c = clientsRepository.findOne(newLine.getClientId());
            Plan plan = plansRepository.findOne(newLine.getPlanId());
            xLineService.createLine(c, newLine, plan);
            
            lineRepository.save(newLine);
            
        }*/
        //riesgo
        /*List<ClientLine> lines = lineRepository.findAll();
        for (ClientLine line : lines) {
            
            if(line.getNumber() == null){
                continue;
            }
            
            Client c = clientsRepository.findOne(line.getClientId());
            Plan plan = plansRepository.findOne(line.getPlanId());

            int amount = 20;
            if (plan.getPrice() > 20) {
                amount = 25;
            }
            
            xRiskService.updateRisk(c.getRefCustomerId(), line.getNumber().toString(), "M", String.valueOf(amount));            

        }*/
        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<MapUtils.FormElement>> getClient(Model model, HttpServletRequest request, @PathVariable("id") Long clientId) {
        Client client = clientsRepository.findOne(clientId);
        return ResponseEntity.ok(MapUtils.getFormElements(client));
    }

}
