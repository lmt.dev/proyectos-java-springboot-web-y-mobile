/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.WingsProperties;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Contract;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.registration.RegistrationFile;
import com.wings.base.enums.ContractStatus;
import com.wings.base.repos.ContractsRepository;
import com.wings.base.repos.PlanRepository;
import com.wings.base.repos.RegistrationFilesRepository;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class XtraService {

    private final Logger log = LoggerFactory.getLogger(XtraService.class);

    @Autowired
    private PlanRepository plansRepository;

    @Autowired
    private EmailService emailService;
    
    @Autowired
    private ContractsRepository contractsRepository;
    
    @Autowired
    private ContractSignatureService contractSignatureService;
    
    @Autowired
    private RegistrationFilesRepository registrationFilesRepository;
    
    private final String form;
    private final String storage;
    private final String signature;
    private final String requestEmail;

    public XtraService(WingsProperties properties) {
        this.form = properties.getBrand().getForm();
        this.storage = properties.getBrand().getStorage();
        this.signature = properties.getBrand().getSignature();
        this.requestEmail = properties.getBrand().getRequestEmail();
    }

    
    public void requestBrandChange(ClientLine l) {
        
        Client c = l.getClient();
        List<File> attachements = new ArrayList<>();
        File attach = generateBrandeChangePdf(l, c);
        if (attach != null) {
            attachements.add(attach);
        }
        
        Plan plan = plansRepository.findOne(l.getPlanId());
                
        //download signed document
        List<Contract> contract = contractsRepository.findByLineIdAndStatusAndActive(l.getId(), ContractStatus.signed.name(),true);
        File signed = contractSignatureService.downloadDocument(contract.get(0));
        attachements.add(signed);
        
        //get dni FOTOCOPIA :)
        attachements.addAll(generateDniPdf(l));

        String name = c.getName() + " " + c.getLastname();
        String id = c.getIdType() + " " + c.getIdValue();
        if(c.getCompanyName() != null && !c.getCompanyName().trim().isEmpty()){
            name = c.getCompanyName();
            id = c.getCompanyIdType() + " " + c.getCompanyIdValue();
        }
        
        StringBuilder subject = new StringBuilder();
        subject.append("Uppertel - Cambio de Marca - ");
        subject.append(c.getRefCustomerId()).append(" - ");
        subject.append(name).append(" - ");
        subject.append(l.getNumber()).append(" - ");
        subject.append(id).append(" - ");
        subject.append("ICC ").append(l.getIccid());
        subject.append("Tarifa ").append(plan.getProviderProfile()).append("\n");

        StringBuilder text = new StringBuilder();
        text.append("<html><body><b>Uppertel - Solicitud Cambio de Marca </b><br/>");
        text.append("<br/>");
        text.append("Cliente: ").append(c.getRefCustomerId()).append("<br/>");
        text.append("Nombre: ").append(name).append("<br/>");
        text.append("Numero: ").append(l.getNumber()).append("<br/>");
        text.append("DNI: ").append(id).append("<br/>");
        text.append("ICC: ").append(l.getIccid()).append("<br/>");
        text.append("Tarifa: ").append(plan.getProviderProfile()).append("<br/>");
        
        text.append("<br/>");
        text.append("<br/>");
        text.append("<br/>");
        text.append("Muchas Gracias.<br/>");
        text.append("Contrataciones - Wings Mobile <br/></body></html>");

        String from = "contratacion@wingsmobile.es";
        String to = requestEmail;

        emailService.sendMail(from, to, subject.toString(), text.toString(), attachements);

    }

    private File generateBrandeChangePdf(ClientLine l, Client c) {

        File ret = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy",new Locale("es","ES"));

        Plan p = plansRepository.findOne(l.getPlanId());

        String name = c.getName() + " " + c.getLastname();
        String id = c.getIdValue();
        sdf.applyPattern("dd/MM/yyyy");
        String fechaNacimiento = sdf.format(c.getBirthDate());
        String nationality = c.getNationality();
        if(c.getCompanyName() != null && !c.getCompanyName().trim().isEmpty()){
            name = c.getCompanyName();
            id = c.getCompanyIdValue();
            fechaNacimiento = "";
            nationality = "";
        }
        
        Map<String, String> map = new HashMap<>();
        map.put("Tarifa", p.getProviderProfile());
        map.put("Nombre Partner", "Uppertel Spain SL");
        map.put("Email Dealer", "contratacion@wingsmobile.es");
        map.put("Nº de Tlf usuario", l.getNumber().toString());
        
        map.put("Numero", id);
        map.put("Fecha de nacimiento", fechaNacimiento);

        map.put("Nombre", name);
        map.put("Direccion", c.getAddress());
        map.put("Nacionalidad", nationality);
        map.put("CP", c.getPostalCode());
        map.put("Poblacion", c.getCity());
        map.put("Provincia", c.getState());
        map.put("Email", c.getEmail());

        //a x dias del mes de x del año x
        Date now = new Date();

        sdf.applyPattern("dd");
        map.put("Dia", sdf.format(now));
        sdf.applyPattern("MMM");
        map.put("Mes", sdf.format(now));
        sdf.applyPattern("yy");
        map.put("Año", sdf.format(now));

        sdf.applyPattern("dd");
        map.put("Fecha solicitud 1", sdf.format(now));
        sdf.applyPattern("MM");
        map.put("Fecha solicitud 2", sdf.format(now));
        sdf.applyPattern("yyyy");
        map.put("Fecha solicitud 3", sdf.format(now));

        map.put("Subdealer 1", "");
        map.put("Subdealer 2", "");
        map.put("Firma", name);
        map.put("En", "Barcelona");

        map.put("Dealer", "");

        PDDocument pdf;
        try {
            pdf = PDDocument.load(new File(form));

            PDDocumentCatalog docCatalog = pdf.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();
            List<PDField> fields = acroForm.getFields();
            for (PDField field : fields) {
                field.setValue(map.get(field.getPartialName()));
            }
            acroForm.flatten();
            pdf.save(new File(storage + "/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf"));
            pdf.close();

            pdf = PDDocument.load(new File(storage + "/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf"));

            PDImageXObject pdImage = PDImageXObject.createFromFile(signature, pdf);
            PDPageContentStream contentStream = new PDPageContentStream(pdf, pdf.getPage(0), true, false);

            contentStream.drawImage(pdImage, 360, 173);
            contentStream.close();

            ret = new File(storage + "/SOLICITUD_CAMBIO_MARCA_" + l.getNumber() + ".pdf");
            pdf.save(ret);

            pdf.close();

        } catch (Exception e) {
            log.error("Error on pdf", e);
        }

        return ret;

    }
    
    private List<File> generateDniPdf(ClientLine l) {

        List<File> ret = new ArrayList<>();
        long regId = l.getRegistrationId();
        List<RegistrationFile> files = registrationFilesRepository.findByRegistrationId(regId);        
        int copy = 1;
        
        for (RegistrationFile file : files) {
            
            PDDocument pdf;
            try {
                pdf =  new PDDocument();
                String fileName = storage + "/FOTOCOPIA_DNI" + l.getNumber() + "_" + copy + ".pdf";
                copy++;
                pdf.save(new File(fileName));
                pdf.close();

                pdf = PDDocument.load(new File(fileName));
                File image = new File(file.getPath());
                InputStream in = new FileInputStream(image);
                BufferedImage bimg = ImageIO.read(in);
                float width = bimg.getWidth();
                float height = bimg.getHeight();
                PDPage page = new PDPage(new PDRectangle(width, height));
                pdf.addPage(page); 
                                
                PDImageXObject pdImage = PDImageXObject.createFromFileByContent(image, pdf);
                try (PDPageContentStream contentStream = new PDPageContentStream(pdf, page)) {
                    contentStream.drawImage(pdImage, 0, 0);
                    contentStream.close();
                    in.close();
                }

                File tosave =  new File(fileName);
                pdf.save(tosave);
                pdf.close();
                
                ret.add(new File(fileName));


            } catch (Exception e) {
                log.error("Error on pdf", e);
            }
        }
        return ret;
    }
    
}
