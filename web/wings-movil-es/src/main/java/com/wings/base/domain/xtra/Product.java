/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.xtra;

/**
 *
 * @author seba
 */
public class Product {

    private Long id;
    private String productProfile;
    private String description;
    private String bonusDat;
    private String bonusSMS;
    private String bonusVoice;
    private String bonusDefault;
    private String bonusOptional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductProfile() {
        return productProfile;
    }

    public void setProductProfile(String productProfile) {
        this.productProfile = productProfile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBonusDat() {
        return bonusDat;
    }

    public void setBonusDat(String bonusDat) {
        this.bonusDat = bonusDat;
    }

    public String getBonusSMS() {
        return bonusSMS;
    }

    public void setBonusSMS(String bonusSMS) {
        this.bonusSMS = bonusSMS;
    }

    public String getBonusVoice() {
        return bonusVoice;
    }

    public void setBonusVoice(String bonusVoice) {
        this.bonusVoice = bonusVoice;
    }

    public String getBonusDefault() {
        return bonusDefault;
    }

    public void setBonusDefault(String bonusDefault) {
        this.bonusDefault = bonusDefault;
    }

    public String getBonusOptional() {
        return bonusOptional;
    }

    public void setBonusOptional(String bonusOptional) {
        this.bonusOptional = bonusOptional;
    }
    
    

}
