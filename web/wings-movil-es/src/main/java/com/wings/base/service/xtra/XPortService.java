/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.ClientLineHolder;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.xtra.PortRequest;
import com.wings.base.enums.LineStatus;
import com.wings.base.utils.DateUtils;
import com.wings.xtra.UrlLocator;
import com.wings.xtra.porting.create.CableMsisdnsPorting;
import com.wings.xtra.porting.create.CableMsisdnsPortingPort;
import com.wings.xtra.porting.create.request.Activate;
import com.wings.xtra.porting.create.request.Address;
import com.wings.xtra.porting.create.request.ClientDetails;
import com.wings.xtra.porting.create.request.Instruction;
import com.wings.xtra.porting.create.request.MsisdnsPortingMaintenanceRequest;
import com.wings.xtra.porting.create.request.NumberPortDetails;
import com.wings.xtra.porting.create.request.Operation;
import com.wings.xtra.porting.create.request.ProductDetails;
import com.wings.xtra.porting.create.request.SoapRequest;
import com.wings.xtra.porting.create.response.MsisdnsPortingMaintenanceRequestResponse;
import com.wings.xtra.porting.detail.request.PortDetails;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author seba
 */
public class XPortService {

    private final Logger log = LoggerFactory.getLogger(XPortService.class);

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    public static final String resId = "F33563";
    public static final String resPin = "3DlIbN8z3rFQv";

    private final String env;

    public XPortService(String env) {
        this.env = env;
    }

    public void portClient(ClientLine line, Client client, Plan plan) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getCreatePortingURL();

        CableMsisdnsPorting service = new CableMsisdnsPorting(url);
        CableMsisdnsPortingPort port = service.getCableMsisdnsPortingSoapService();

        try {

            // TODO initialize WS operation arguments here
            MsisdnsPortingMaintenanceRequest part1 = new MsisdnsPortingMaintenanceRequest();

            SoapRequest request = new SoapRequest();
            Operation operation = new Operation();

            Instruction i = new Instruction();
            Date date = new Date();
            //identificacion
            i.setResellerId("F33563");
            i.setResellerPin("3DlIbN8z3rFQv");
            //variables
            i.setOperationType("NEWPOR");
            i.setTransactionId(String.valueOf(date.getTime()));
            i.setTimeStamp(sdf.format(date));
            i.setRefCustomerId(client.getRefCustomerId());
            //opcionales
            i.setBranchId("");
            i.setPosId("");
            operation.setInstruction(i);

            Activate activate = new Activate();
            Address address = new Address();
            address.setCountry("España");
            address.setLocality(client.getState());
            address.setPostalTown(client.getCity());
            address.setPostcode(client.getPostalCode());
            address.setStreetName(client.getAddress());

            activate.setAddress(address);

            String name = client.getName();
            String lastName = client.getLastname();
            String secondLastName = "";
            if (lastName.split(" ").length > 1) {
                String[] arr = lastName.split(" ");
                lastName = arr[0];
                secondLastName = arr[1];
                if (arr.length > 2) {
                    for (int j = 2; j < arr.length; j++) {
                        secondLastName += " " + arr[j];
                    }
                }
            }

            String companyName = "";
            String idType = client.getIdType();
            String idValue = client.getIdValue().toUpperCase();

            String clientType = "1";
            if (client.getCompanyName() != null && !client.getCompanyName().trim().isEmpty()) {
                companyName = client.getCompanyName();
                idType = client.getCompanyIdType();
                idValue = client.getCompanyIdValue().toUpperCase();
                clientType = "0";
            }

            ClientDetails cd = new ClientDetails();
            cd.setClientType(clientType); //1 particular - 0 empresa
            cd.setFirstName(name);
            cd.setLastName(lastName);
            cd.setSecondLastName(secondLastName);
            cd.setIdentityType(idType); //DNI-NIF-CIF // TAR tarjeta de residencia - PAS pasaporte
            cd.setIdentityValue(idValue);
            cd.setCompanyName(companyName);
            cd.setContactMail(client.getEmail());
            cd.setContactPhone(client.getMobile());

            activate.setClientDetails(cd);

            //prepago o contrato
            //prepago 1 - contrato 0
            NumberPortDetails d = new NumberPortDetails();
            String contratType = line.getPortIccid() != null && !line.getPortIccid().isEmpty() ? "1" : "0";
            d.setFromContractType(contratType);
            d.setFromPhoneNumber(line.getNumber().toString());
            //solo si es prepago
            if (contratType.equals("1")) {

                //077700016586069
                String iccidDonor = line.getPortIccid().trim();
                if (iccidDonor.length() == 15 && !iccidDonor.startsWith("8934")) {
                    iccidDonor = "8934" + iccidDonor;
                }

                if (iccidDonor.length() == 17 && !iccidDonor.startsWith("89")) {
                    iccidDonor = "89" + iccidDonor;
                }

                d.setFromSIMCardNumber(iccidDonor);

            }
            d.setFromServiceProvider(line.getPortOperator());
            d.setNewIccid(line.getIccid());

            //si la fecha planeada es menor a hoy la actualizo con la fecha de mañana
            line.setPortPlannedDate(DateUtils.validatePortabilityDate(line.getPortPlannedDate()));
            d.setPortingDate(sdf.format(line.getPortPlannedDate()));

            activate.setNumberPortDetails(d);

            ProductDetails pd = new ProductDetails();
            pd.setProductProfile(plan.getProviderProfile());
            pd.setBonosAlta(plan.getProviderDefaultBonus());

            activate.setProductDetails(pd);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            MsisdnsPortingMaintenanceRequestResponse result = port.msisdnsPortingMaintenance(part1);

            line.setExternalState(result.getReturn().getActivateDescription());
            System.out.println("Result = " + result.getReturn().getActivateDescription());
            System.out.println("Result = " + result.getReturn().getTransactionId());
            if (result.getReturn().getActivationCode().equals("OK-001")) {
                line.setInternalState(LineStatus.port_requested.name());
            } else {
                log.error("Resultado de pedido de portabilidad incierto:" + result.getReturn().getActivationCode());
                line.setInternalState(LineStatus.port_rejected.name());
            }

        } catch (Exception ex) {
            log.error("Error solicitando portabilidad", ex);
        }
    }

    public void portClient(ClientLine line, ClientLineHolder holder) throws Exception {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getCreatePortingURL();

        CableMsisdnsPorting service = new CableMsisdnsPorting(url);
        CableMsisdnsPortingPort port = service.getCableMsisdnsPortingSoapService();

        // TODO initialize WS operation arguments here
        MsisdnsPortingMaintenanceRequest part1 = new MsisdnsPortingMaintenanceRequest();

        SoapRequest request = new SoapRequest();
        Operation operation = new Operation();

        Instruction i = new Instruction();
        Date date = new Date();
        //identificacion
        i.setResellerId("F33563");
        i.setResellerPin("3DlIbN8z3rFQv");
        //variables
        i.setOperationType("NEWPOR");
        i.setTransactionId(String.valueOf(date.getTime()));
        i.setTimeStamp(sdf.format(date));
        i.setRefCustomerId(line.getClient().getRefCustomerId());
        //opcionales
        i.setBranchId("");
        i.setPosId("");
        operation.setInstruction(i);

        Activate activate = new Activate();
        Address address = new Address();
        address.setCountry("España");
        address.setLocality(holder.getState());
        address.setPostalTown(holder.getCity());
        address.setPostcode(holder.getPostalCode());
        address.setStreetName(holder.getAddress());

        activate.setAddress(address);

        String name = holder.getName();
        String lastName = holder.getLastname();
        String secondLastName = "";
        if (lastName.split(" ").length > 1) {
            String[] arr = lastName.split(" ");
            lastName = arr[0];
            secondLastName = arr[1];
            if (arr.length > 2) {
                for (int j = 2; j < arr.length; j++) {
                    secondLastName += " " + arr[j];
                }
            }
        }

        String companyName = "";
        String idType = holder.getIdType();
        String idValue = holder.getIdValue().toUpperCase();
        String clientType = "1";
        if (holder.getCompanyName() != null && !holder.getCompanyName().trim().isEmpty()) {
            companyName = holder.getCompanyName();
            idType = holder.getCompanyIdType();
            idValue = holder.getCompanyIdValue().toUpperCase();
            clientType = "0";
        }

        ClientDetails cd = new ClientDetails();
        cd.setClientType(clientType); //1 particular - 0 empresa
        cd.setFirstName(name);
        cd.setLastName(lastName);
        cd.setSecondLastName(secondLastName);
        cd.setIdentityType(idType); //DNI-NIF-CIF // TAR tarjeta de residencia - PAS pasaporte
        cd.setIdentityValue(idValue);
        cd.setCompanyName(companyName);
        cd.setContactMail(holder.getEmail());
        cd.setContactPhone(holder.getMobile());

        activate.setClientDetails(cd);

        //prepago o contrato
        //prepago 1 - contrato 0
        NumberPortDetails d = new NumberPortDetails();
        String contratType = line.getPortIccid() != null && !line.getPortIccid().isEmpty() ? "1" : "0";
        d.setFromContractType(contratType);
        d.setFromPhoneNumber(line.getNumber().toString());
        //solo si es prepago
        if (contratType.equals("1")) {

            //077700016586069
            String iccidDonor = line.getPortIccid().trim();
            if (iccidDonor.length() == 15 && !iccidDonor.startsWith("8934")) {
                iccidDonor = "8934" + iccidDonor;
            }

            if (iccidDonor.length() == 17 && !iccidDonor.startsWith("89")) {
                iccidDonor = "89" + iccidDonor;
            }

            d.setFromSIMCardNumber(iccidDonor);

        }
        d.setFromServiceProvider(line.getPortOperator());
        d.setNewIccid(line.getIccid());

        //si la fecha planeada es menor a hoy la actualizo con la fecha de mañana
        line.setPortPlannedDate(DateUtils.validatePortabilityDate(line.getPortPlannedDate()));
        d.setPortingDate(sdf.format(line.getPortPlannedDate()));

        activate.setNumberPortDetails(d);

        ProductDetails pd = new ProductDetails();
        pd.setProductProfile(line.getPlan().getProviderProfile());
        pd.setBonosAlta(line.getPlan().getProviderDefaultBonus());

        activate.setProductDetails(pd);

        operation.setActivate(activate);
        request.setOperation(operation);
        part1.setSoapRequest(request);
        // TODO process result here
        MsisdnsPortingMaintenanceRequestResponse result = port.msisdnsPortingMaintenance(part1);

        line.setExternalState(result.getReturn().getActivateDescription());
        if (result.getReturn().getActivationCode().equals("OK-001")) {
            line.setInternalState(LineStatus.port_requested.name());
        } else {
            log.error("Resultado de pedido de portabilidad incierto:" + result.getReturn().getActivationCode());
            line.setInternalState(LineStatus.port_rejected.name());
        }
    }

    public List<PortRequest> getPortingRequests(String phoneNumber) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        List<PortRequest> ret = new ArrayList<>();
        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getListPortingURL();

        com.wings.xtra.porting.get.CableMsisdnsPorting service = new com.wings.xtra.porting.get.CableMsisdnsPorting(url);
        com.wings.xtra.porting.get.CableMsisdnsPortingPort port = service.getCableMsisdnsPortingSoapService();

        try {

            // TODO initialize WS operation arguments here
            com.wings.xtra.porting.get.request.MsisdnsPortingMaintenanceRequest part1 = new com.wings.xtra.porting.get.request.MsisdnsPortingMaintenanceRequest();

            com.wings.xtra.porting.get.request.SoapRequest request = new com.wings.xtra.porting.get.request.SoapRequest();
            com.wings.xtra.porting.get.request.Operation operation = new com.wings.xtra.porting.get.request.Operation();

            com.wings.xtra.porting.get.request.Activate a = new com.wings.xtra.porting.get.request.Activate();
            //filtro
            com.wings.xtra.porting.get.request.PortDetails d = new com.wings.xtra.porting.get.request.PortDetails();
            d.setFromPhoneNumber(phoneNumber);
            a.setPortDetails(d);

            operation.setActivate(a);

            com.wings.xtra.porting.get.request.Instruction i = new com.wings.xtra.porting.get.request.Instruction();

            Date date = new Date();
            i.setBranchId("");
            //NEWLINE,ERASED,SERVICE,CHANGE_PRODUCT,FIND
            i.setOperationType("LISTPOR");
            i.setPosId("");
            //i.setRefCustomerId(client.getRefCustomerId());
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            com.wings.xtra.porting.get.response.MsisdnsPortingMaintenanceRequestResponse result = port.msisdnsPortingMaintenance(part1);

            if (result.getReturn().getActivationCode().equalsIgnoreCase("OK-001")) {

                List<com.wings.xtra.porting.get.response.MsisdnsPortingMaintenanceRequestResponse.Return.Portabilidades.SolicitudPortabilidad> list = result.getReturn().getPortabilidades().getSolicitudPortabilidad();
                for (com.wings.xtra.porting.get.response.MsisdnsPortingMaintenanceRequestResponse.Return.Portabilidades.SolicitudPortabilidad s : list) {

                    PortRequest r = new PortRequest();
                    r.setBonosAlta(s.getBonosAlta());
                    r.setCompanyName(s.getCompanyName());
                    r.setContract(Long.valueOf(s.getContract()));
                    r.setDate(sdf.parse(s.getDate()));
                    r.setFase(s.getFase());
                    r.setFirstName(s.getFirstName());
                    r.setFromPhoneNumber(Long.parseLong(s.getFromPhoneNumber()));
                    r.setFromServiceProvider(s.getFromServiceProvider());
                    r.setIdFase(s.getIdFase());
                    r.setLastName(s.getLastName());
                    r.setProductId(s.getProductId());
                    r.setProductProfile(s.getProductProfile());
                    r.setSecondLastName(s.getSecondLastName());
                    ret.add(r);
                }
            }

        } catch (NumberFormatException | ParseException ex) {
            log.error("Error getting portability requests list", ex);
        }
        return ret;
    }

    public PortRequest getPortRequestInfo(ClientLine clientLine) throws Exception {

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getPortDetailURL();

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        com.wings.xtra.porting.detail.CableMsisdnsPorting service = new com.wings.xtra.porting.detail.CableMsisdnsPorting(url);
        com.wings.xtra.porting.detail.CableMsisdnsPortingPort port = service.getCableMsisdnsPortingSoapService();

        // TODO initialize WS operation arguments here
        com.wings.xtra.porting.detail.request.MsisdnsPortingMaintenanceRequest part1 = new com.wings.xtra.porting.detail.request.MsisdnsPortingMaintenanceRequest();

        com.wings.xtra.porting.detail.request.SoapRequest request = new com.wings.xtra.porting.detail.request.SoapRequest();
        com.wings.xtra.porting.detail.request.Operation operation = new com.wings.xtra.porting.detail.request.Operation();

        com.wings.xtra.porting.detail.request.Activate a = new com.wings.xtra.porting.detail.request.Activate();
        //filtro
        PortDetails d = new PortDetails();
        d.setContract(clientLine.getContractNumber());
        a.setPortDetails(d);

        operation.setActivate(a);

        com.wings.xtra.porting.detail.request.Instruction i = new com.wings.xtra.porting.detail.request.Instruction();
        Date date = new Date();
        i.setBranchId("");
        i.setOperationType("DETPOR");
        i.setPosId("");
        i.setRefCustomerId("F33563012239");
        i.setResellerId(resId);
        i.setResellerPin(resPin);
        i.setTimeStamp(sdf.format(date));
        i.setTransactionId(String.valueOf(date.getTime()));

        operation.setInstruction(i);

        request.setOperation(operation);
        part1.setSoapRequest(request);
        com.wings.xtra.porting.detail.response.MsisdnsPortingMaintenanceRequestResponse result = port.msisdnsPortingMaintenance(part1);
        com.wings.xtra.porting.detail.response.MsisdnsPortingMaintenanceRequestResponse.Return.Portabilidad.SolicitudPortabilidad s = result.getReturn().getPortabilidad().getSolicitudPortabilidad();

        PortRequest r = new PortRequest();
        r.setBonosAlta(s.getBonosAlta());
        r.setCompanyName(s.getCompanyName());
        r.setContract(Long.valueOf(s.getContract()));
        r.setDate(sdf.parse(s.getDate()));
        r.setPortingDate(sdf.parse(s.getPortingDate()));
        r.setFase(s.getFase());
        r.setFirstName(s.getFirstName());
        r.setFromPhoneNumber(Long.parseLong(s.getFromPhoneNumber()));
        r.setFromServiceProvider(s.getFromServiceProvider());
        r.setIdFase(s.getIdFase());
        r.setLastName(s.getLastName());
        r.setProductId(s.getProductId());
        r.setProductProfile(s.getProductProfile());
        r.setSecondLastName(s.getSecondLastName());

        return r;
    }

}
