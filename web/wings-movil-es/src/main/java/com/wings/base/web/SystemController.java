/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.process.BillingProccess;
import com.wings.base.process.LineProcess;
import com.wings.base.process.OrderProcess;
import com.wings.base.process.SystemStatus;
import com.wings.base.process.XtraProccess;
import com.wings.base.repos.LinesRepository;
import com.wings.base.service.FixesService;
import com.wings.base.service.ProcessLogService;
import com.wings.base.service.xtra.XRiskService;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class SystemController {

    @Autowired
    private LineProcess lineProcess;

    @Autowired
    private OrderProcess orderProcess;

    @Autowired
    private XtraProccess xtraProccess;

    @Autowired
    private XRiskService xRiskService;

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private FixesService fixesService;

    private final Logger log = LoggerFactory.getLogger(BalanceController.class);

    @Autowired
    private BillingProccess billingProccess;

    private boolean running = false;

    @RequestMapping(value = {"views/system/balances"}, method = RequestMethod.GET)
    public String getBalances(Model model, HttpServletRequest request) {

        //set current month and year
        Calendar cal = Calendar.getInstance();
        model.addAttribute("year", String.valueOf(cal.get(Calendar.YEAR)));
        model.addAttribute("month", String.valueOf(cal.get(Calendar.MONTH)));

        return "views/system/balances";
    }

    @RequestMapping(value = {"views/system/process"}, method = RequestMethod.GET)
    public String getProcess(Model model, HttpServletRequest request) {
        return "views/system/process";
    }

    private Thread t;

    @ResponseBody
    @RequestMapping(value = {"api/system/process/{action}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getProcess(Model model, HttpServletRequest request, @PathVariable("action") String action) {

        if (t != null && t.isAlive()) {
            return ResponseEntity.badRequest().body("Proceso ejecutando por favor espere a que termine...");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();

        switch (action) {
            case "action-check-registration":
                t = t = new Thread(() -> {
                    lineProcess.verifyRegistrations(launcher);
                });
                t.start();
                break;
            case "action-create-clients":
                t = new Thread(() -> {
                    lineProcess.createClients(launcher);
                });
                t.start();
                break;
            case "action-create-contracts":
                t = new Thread(() -> {
                    lineProcess.createContracts(launcher);
                });
                t.start();
                break;
            case "action-send-contracts":
                t = new Thread(() -> {
                    lineProcess.sendContracts(launcher);
                });
                t.start();
                break;
            case "action-check-contracts":
                t = new Thread(() -> {
                    lineProcess.checkContractStatus(launcher);
                });
                t.start();
                break;
            case "action-check-resend-contracts":
                t = new Thread(() -> {
                    lineProcess.checkResendedContractStatus(launcher);
                });
                t.start();
                break;
            case "action-create-orders":
                t = new Thread(() -> {
                    lineProcess.createOrders(launcher);
                });
                t.start();
                break;
            case "action-send-orders":
                t = new Thread(() -> {
                    lineProcess.sendOrders(launcher);
                });
                t.start();
                break;
            case "action-check-orders":
                t = new Thread(() -> {
                    orderProcess.checkUpsStatus(launcher);
                });
                t.start();
                break;
            case "action-check-tracking":
                t = new Thread(() -> {
                    orderProcess.checkUpsDeliveryStatus(launcher, null);
                });
                t.start();
                break;
            case "action-create-operator":
                t = new Thread(() -> {
                    lineProcess.createOperator(launcher);
                });
                t.start();
                break;
            case "action-check-port":
                t = new Thread(() -> {
                    xtraProccess.checkPortStatus(launcher);
                });
                t.start();
                break;
            case "action-check-line":
                t = new Thread(() -> {
                    xtraProccess.checkCreatedLineStatus(launcher);
                });
                t.start();
                break;
            case "action-proces-auth":
                List<ClientLine> lines = linesRepository.findAll();
                t = new Thread(() -> {
                    lineProcess.processPaymentAuthorization(lines, launcher);
                });
                t.start();
                break;
            /*case "action-process-risk":
                lines = linesRepository.findAll();
                lines.forEach((line) -> {
                    if (line.getInternalState().equals(LineStatus.active.name())) {
                        xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "L", "30");
                        xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "H", "30");
                    }
                });
                break;*/
            case "action-check-plans":
                return ResponseEntity.ok(fixesService.checkPlans());
            case "action-check-invoices":
                return ResponseEntity.ok(fixesService.checkInvoices());
            case "action-fix-invoice-balance":
                return ResponseEntity.ok(fixesService.checkInvoiceBalance());
            case "action-fix-invoice-numbers":
                return ResponseEntity.ok(fixesService.fixInvoiceNumbers());
            case "action-fix-invoices-20gb":
                return ResponseEntity.ok(fixesService.fixInvoices20GB());
            case "action-fix-wrong-balance":
                return ResponseEntity.ok(fixesService.fixBalancesForAllPeriod());
            case "action-fix-change-plan":
                return ResponseEntity.ok(fixesService.fixInvoicesChangePlan());
        }

        return ResponseEntity.ok("Proceso ejecutado, revisar logs de proceso");
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/process"}, method = RequestMethod.GET)
    public ResponseEntity<List<ProcessLog>> getProcessLog(Model model, HttpServletRequest request) {
        List<ProcessLog> logs = processLogService.getLastProcess();
        Collections.sort(logs, (ProcessLog o1, ProcessLog o2) -> o1.getStartDate().compareTo(o2.getStartDate()));
        return ResponseEntity.ok(logs);
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/balances"}, method = RequestMethod.POST)
    public ResponseEntity createBalances(Model model, HttpServletRequest request) {

        String month = request.getParameter("month");
        String year = request.getParameter("year");

        int iyear;
        int imonth;

        if (year != null) {
            iyear = Integer.valueOf(year);
        } else {
            return ResponseEntity.badRequest().body("El año no puede estar vacio");
        }

        if (month != null) {
            imonth = Integer.valueOf(month);
        } else {
            return ResponseEntity.badRequest().body("El mes no puede estar vacio");
        }

        if (t != null && t.isAlive()) {
            return ResponseEntity.badRequest().body("Proceso esta corriendo, espere por favor");
        }

        t = new Thread(() -> {
            billingProccess.processPlanMonthly(imonth, iyear);
        });
        t.start();
        String ret = "Ejecutando proceso";

        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/invoices"}, method = RequestMethod.POST)
    public ResponseEntity createInvocies(Model model, HttpServletRequest request) {

        String month = request.getParameter("month");
        String year = request.getParameter("year");

        int iyear;
        int imonth;

        if (year != null) {
            iyear = Integer.valueOf(year);
        } else {
            return ResponseEntity.badRequest().body("El año no puede estar vacio");
        }

        if (month != null) {
            imonth = Integer.valueOf(month);
        } else {
            return ResponseEntity.badRequest().body("El mes no puede estar vacio");
        }

        if (t != null && t.isAlive()) {
            return ResponseEntity.badRequest().body("Proceso ejecutando por favor espere a que termine...");
        }

        t = new Thread(() -> {
            billingProccess.createInvoices(imonth, iyear);
        });
        t.start();

        String ret = "Ejecutando Proceso generacion de facturas";
        return ResponseEntity.ok(ret);
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/payments"}, method = RequestMethod.POST)
    public ResponseEntity processPayments(Model model, HttpServletRequest request) {

        if (t != null && t.isAlive()) {
            return ResponseEntity.badRequest().body("Proceso ejecutando por favor espere a que termine...");
        }

        String month = request.getParameter("month");
        String year = request.getParameter("year");

        int iyear;
        int imonth;

        if (year != null) {
            iyear = Integer.valueOf(year);
        } else {
            return ResponseEntity.badRequest().body("El año no puede estar vacio");
        }

        if (month != null) {
            imonth = Integer.valueOf(month);
        } else {
            return ResponseEntity.badRequest().body("El mes no puede estar vacio");
        }

        t = new Thread(() -> {
            billingProccess.proccessPayments(imonth, iyear);
        });
        t.start();

        return ResponseEntity.ok("Proceso ejecutado con éxito");
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/invoice/regenerate"}, method = RequestMethod.POST)
    public ResponseEntity regenerate(Model model, HttpServletRequest request) {

        t = new Thread(() -> {
            billingProccess.regenerateInvoices();
        });
        t.start();

        return ResponseEntity.ok("Proceso ejecutado con éxito");
    }

    @RequestMapping(value = {"api/planes/client/plans"}, method = RequestMethod.GET)
    public ResponseEntity<String> getPlansClients(Model model, HttpServletRequest request) {
        String salida = fixesService.getClientLinePlans();
        return ResponseEntity.ok(salida);
    }

    @ResponseBody
    @RequestMapping(value = {"api/system/status"}, method = RequestMethod.GET)
    public ResponseEntity<String> getStatus(Model model, HttpServletRequest request) {
        return ResponseEntity.ok(billingProccess.getStatus().toString());
    }

}
