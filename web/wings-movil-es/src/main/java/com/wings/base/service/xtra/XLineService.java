/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Plan;
import com.wings.base.domain.xtra.XtraLineStatus;
import com.wings.base.enums.LineStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.TicketsRepository;
import com.wings.base.service.ProcessLogService;
import com.wings.xtra.UrlLocator;
import com.wings.xtra.lines.create.request.Activate;
import com.wings.xtra.lines.create.request.LineDetails;
import com.wings.xtra.lines.create.request.LineProductDetails;
import com.wings.xtra.lines.find.CableMsisdns;
import com.wings.xtra.lines.find.CableMsisdnsPort;
import com.wings.xtra.lines.find.request.Find;
import com.wings.xtra.lines.status.request.Instruction;
import com.wings.xtra.lines.status.request.MsisdnsMaintenanceRequest;
import com.wings.xtra.lines.status.request.Operation;
import com.wings.xtra.lines.status.request.SoapRequest;
import com.wings.xtra.lines.status.response.MsisdnsMaintenanceRequestResponse;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author seba
 */
public class XLineService {

    private final Logger log = LoggerFactory.getLogger(XLineService.class);

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private TicketsRepository ticketsRepository;

    private final String env;

    public static final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:sss");
    public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
    public static final String resId = "F33563";
    public static final String resPin = "3DlIbN8z3rFQv";

    public XLineService(String env) {
        this.env = env;
    }

    public ClientLine createLine(Client client, ClientLine line, Plan plan) {

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getCreateLineURL();

        com.wings.xtra.lines.create.CableMsisdns service = new com.wings.xtra.lines.create.CableMsisdns(url);
        com.wings.xtra.lines.create.CableMsisdnsPort port = service.getCableMsisdnsSoapService();

        try {

            // TODO initialize WS operation arguments here
            com.wings.xtra.lines.create.request.MsisdnsMaintenanceRequest part1 = new com.wings.xtra.lines.create.request.MsisdnsMaintenanceRequest();

            com.wings.xtra.lines.create.request.SoapRequest request = new com.wings.xtra.lines.create.request.SoapRequest();
            com.wings.xtra.lines.create.request.Operation operation = new com.wings.xtra.lines.create.request.Operation();

            com.wings.xtra.lines.create.request.Instruction i = new com.wings.xtra.lines.create.request.Instruction();
            Date date = new Date();

            i.setOperationType("NEWLINE");
            i.setRefCustomerId(client.getRefCustomerId());
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            Activate activate = new Activate();

            LineDetails l = new LineDetails();
            l.setIccid(line.getIccid());

            activate.setLineDetails(l);

            LineProductDetails pd = new LineProductDetails();
            pd.setBonosAlta(plan.getProviderDefaultBonus());
            pd.setProductProfile(plan.getProviderProfile());
            activate.setLineProductDetails(pd);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            com.wings.xtra.lines.create.response.MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);

            String message = result.getReturn().getActivateDescription();
            line.setExternalState(message);
            if (result.getReturn().getActivationCode().equals("OK-001") && !message.equals("La peticion de alta no se ha creado correctamente")) {
                line.setInternalState(LineStatus.created_operator.name());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return line;
    }

    /**
     * A : Linea activa B : Linea de Baja D : Linea desactivada S : Linea en
     * Suspension J : Bloqueo por Call Barring R : Bloqueo por riesgo. L
     * :Bloqueo temporal.
     *
     * @param refCustomerId
     * @param status
     * @return
     */
    public List<XtraLineStatus> findLines(String refCustomerId, String status) {

        ProcessLog pl = processLogService.createProcess("system", "Check Line operator");

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        pl.append("Entering line status");
        List<XtraLineStatus> stats = new ArrayList<>();

        try {
            UrlLocator locator = new UrlLocator(env);
            URL url = locator.getFindLinesURL();

            CableMsisdns service = new CableMsisdns(url);
            CableMsisdnsPort port = service.getCableMsisdnsSoapService();

            com.wings.xtra.lines.find.request.MsisdnsMaintenanceRequest part1 = new com.wings.xtra.lines.find.request.MsisdnsMaintenanceRequest();
            com.wings.xtra.lines.find.request.SoapRequest request = new com.wings.xtra.lines.find.request.SoapRequest();
            com.wings.xtra.lines.find.request.Operation operation = new com.wings.xtra.lines.find.request.Operation();

            com.wings.xtra.lines.find.request.Instruction i = new com.wings.xtra.lines.find.request.Instruction();
            Date date = new Date();

            i.setBranchId("");
            //NEWLINE,ERASED,SERVICE,CHANGE_PRODUCT,FIND
            i.setOperationType("FIND");
            i.setPosId("");
            i.setRefCustomerId(refCustomerId);
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            Find f = new Find();
            f.setStatus(status);
            operation.setFind(f);

            request.setOperation(operation);

            pl.appendLine("Going to make the request");

            part1.setSoapRequest(request);
            // TODO process result here
            com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);

            pl.appendLine("Request Done, parsing result");

            if (!result.getReturn().getActivationCode().equals("FIND-ERR-003") && result.getReturn().getMsisdnList() != null) {

                List<com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn> list = result.getReturn().getMsisdnList().getMsisdn();

                pl.appendLine("Result parsed " + list);
                if (list != null) {
                    pl.appendLine("Result parsed size " + list.size());
                }

                for (com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn msisdn : list) {

                    XtraLineStatus s = new XtraLineStatus();
                    s.setActivateDate(msisdn.getActivateDate());
                    s.setIccid(msisdn.getIccid());
                    s.setMsisdn(msisdn.getMsisdn());
                    s.setProductId(msisdn.getProductId());
                    s.setProductProfile(msisdn.getProductProfile());
                    s.setPuk(msisdn.getPuk());
                    s.setRefCustomerId(msisdn.getRefCustomerId());
                    s.setStatus(msisdn.getStatus());
                    s.setStatusDate(msisdn.getStatusDate());
                    s.setTariffId(msisdn.getTariffId());
                    s.setTypeIccid(msisdn.getTypeIccid());

                    stats.add(s);
                }
            } else {
                pl.appendLine("No new Lines found.");
            }
        } catch (Exception ex) {
            log.error("Error finding lines ", ex);
            pl.appendLine("Error finding lines " + ex.getMessage());
        }

        pl.appendLine("Done, returning stats " + stats.size());
        pl.stop();
        return stats;
    }

    /**
     * A : Linea activa B : Linea de Baja D : Linea desactivada S : Linea en
     * Suspension J : Bloqueo por Call Barring R : Bloqueo por riesgo. L :
     * Bloqueo temporal.
     *
     * @param line
     * @return
     */
    public List<XtraLineStatus> getLineStatus(ClientLine line) {

        ProcessLog pl = processLogService.createProcess("system", "Check Line operator");
        pl.appendLine("Entering line status");
        List<XtraLineStatus> stats = new ArrayList<>();

        try {
            UrlLocator locator = new UrlLocator(env);
            URL url = locator.getFindLinesURL();

            CableMsisdns service = new CableMsisdns(url);
            CableMsisdnsPort port = service.getCableMsisdnsSoapService();

            com.wings.xtra.lines.find.request.MsisdnsMaintenanceRequest part1 = new com.wings.xtra.lines.find.request.MsisdnsMaintenanceRequest();
            com.wings.xtra.lines.find.request.SoapRequest request = new com.wings.xtra.lines.find.request.SoapRequest();
            com.wings.xtra.lines.find.request.Operation operation = new com.wings.xtra.lines.find.request.Operation();

            com.wings.xtra.lines.find.request.Instruction i = new com.wings.xtra.lines.find.request.Instruction();
            Date date = new Date();

            i.setBranchId("");
            //NEWLINE,ERASED,SERVICE,CHANGE_PRODUCT,FIND
            i.setOperationType("FIND");
            i.setPosId("");
            i.setRefCustomerId(line.getClient().getRefCustomerId());
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            Find f = new Find();
            f.setMsisdn(line.getNumber().toString());
            operation.setFind(f);

            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);

            if (result.getReturn().getActivationCode().contains("OK")) {
                if (result.getReturn() != null && result.getReturn().getMsisdnList() != null) {
                    List<com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn> list = result.getReturn().getMsisdnList().getMsisdn();

                    pl.appendLine("Result parsed " + list);
                    if (list != null) {
                        pl.appendLine("Result parsed size " + list.size());
                    }

                    for (com.wings.xtra.lines.find.response.MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn msisdn : list) {

                        XtraLineStatus s = new XtraLineStatus();
                        s.setActivateDate(msisdn.getActivateDate());
                        s.setIccid(msisdn.getIccid());
                        s.setMsisdn(msisdn.getMsisdn());
                        s.setProductId(msisdn.getProductId());
                        s.setProductProfile(msisdn.getProductProfile());
                        s.setPuk(msisdn.getPuk());
                        s.setRefCustomerId(msisdn.getRefCustomerId());
                        s.setStatus(msisdn.getStatus());
                        s.setStatusDate(msisdn.getStatusDate());
                        s.setTariffId(msisdn.getTariffId());
                        s.setTypeIccid(msisdn.getTypeIccid());

                        stats.add(s);
                    }
                }
            } else {
                String text = result.getReturn().getActivationCode() + " - " + result.getReturn().getActivateDescription();
                Ticket t = new Ticket(TicketType.line.name(), line.getId(), text, "system", "ADMIN");
                ticketsRepository.save(t);
            }

        } catch (Exception ex) {
            log.error("Error finding lines ", ex);
            pl.appendLine("Error finding lines " + ex.getMessage());
        }

        pl.appendLine("Done, returning stats " + stats.size());
        pl.stop();
        return stats;
    }

    /**
     * S : Suspensión. El cliente no puede hacer ni recibir llamadas J : Call
     * Barring. Restricción de llamadas salientes, si puede recibir llamadas. H
     * : Hot Line. Todas las llamadas que realice el cliente van al callcenter
     * (se traducen al 2989) F : Fraude. NO se pueden ni recibir ni realizar
     * llamadas H : Hurto. NO se pueden ni recibir ni realizar llamadas
     * Reactivación: R : Desbloqueo de la línea
     *
     * @param line
     * @param status
     */
    public String setLineStatus(ClientLine line, String status) {

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getLineStatusURL();

        com.wings.xtra.lines.status.CableMsisdns service = new com.wings.xtra.lines.status.CableMsisdns(url);
        com.wings.xtra.lines.status.CableMsisdnsPort port = service.getCableMsisdnsSoapService();

        try {

            // TODO initialize WS operation arguments here
            MsisdnsMaintenanceRequest part1 = new MsisdnsMaintenanceRequest();

            SoapRequest request = new SoapRequest();
            Operation operation = new Operation();

            Instruction i = new Instruction();
            Date date = new Date();

            i.setBranchId("");
            //NEWLINE,ERASED,SERVICE,CHANGE_PRODUCT,FIND
            i.setOperationType("SERVICE");
            i.setPosId("");
            i.setRefCustomerId(line.getClient().getRefCustomerId());
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            com.wings.xtra.lines.status.request.Activate activate = new com.wings.xtra.lines.status.request.Activate();
            com.wings.xtra.lines.status.request.LineDetails l = new com.wings.xtra.lines.status.request.LineDetails();
            l.setMsisdn(line.getNumber().toString());

            activate.setLineDetails(l);

            com.wings.xtra.lines.status.request.LineServices ls = new com.wings.xtra.lines.status.request.LineServices();
            ls.setServiceAction(status);
            activate.setLineServices(ls);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);

            System.out.println("Result = " + result.getReturn().getActivateDescription());
            System.out.println("Result = " + result.getReturn().getTransactionId());
            System.out.println("Result = " + result.getReturn().getActivationCode());

            return result.getReturn().getActivationCode() + "-" + result.getReturn().getActivateDescription();

        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
    }

    public String changeLinePlan(ClientLine line, Plan newPlan, Date fechaCambioProducto) {

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getChangePlanURL();

        com.wings.xtra.lines.update.CableMsisdns service = new com.wings.xtra.lines.update.CableMsisdns(url);
        com.wings.xtra.lines.update.CableMsisdnsPort port = service.getCableMsisdnsSoapService();

        com.wings.xtra.lines.update.request.MsisdnsMaintenanceRequest part1 = new com.wings.xtra.lines.update.request.MsisdnsMaintenanceRequest();

        com.wings.xtra.lines.update.request.SoapRequest request = new com.wings.xtra.lines.update.request.SoapRequest();
        com.wings.xtra.lines.update.request.Operation operation = new com.wings.xtra.lines.update.request.Operation();

        com.wings.xtra.lines.update.request.Instruction i = new com.wings.xtra.lines.update.request.Instruction();

        Date date = new Date();
        i.setBranchId("");
        i.setOperationType("CHANGE_PRODUCT");
        i.setPosId("");
        i.setRefCustomerId(line.getClient().getRefCustomerId());
        i.setResellerId(resId);
        i.setResellerPin(resPin);
        i.setTimeStamp(sdf.format(date));
        i.setTransactionId(String.valueOf(date.getTime()));
        i.setFechaCambioProducto(sdf2.format(fechaCambioProducto));
        
        operation.setInstruction(i);

        com.wings.xtra.lines.update.request.Activate activate = new com.wings.xtra.lines.update.request.Activate();

        com.wings.xtra.lines.update.request.LineDetails l = new com.wings.xtra.lines.update.request.LineDetails();
        l.setMsisdn(line.getNumber().toString());
        activate.setLineDetails(l);

        com.wings.xtra.lines.update.request.LineProductDetails p = new com.wings.xtra.lines.update.request.LineProductDetails();
        p.setBonosAlta(newPlan.getProviderDefaultBonus());
        p.setProductProfile(newPlan.getProviderProfile());
        activate.setLineProductDetails(p);

        operation.setActivate(activate);
        request.setOperation(operation);
        part1.setSoapRequest(request);
        // TODO process result here
        com.wings.xtra.lines.update.response.MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);

        System.out.println("Result = " + result.getReturn().getActivateDescription());
        System.out.println("Result = " + result.getReturn().getTransactionId());
        System.out.println("Result = " + result.getReturn().getActivationCode());

        return result.getReturn().getActivationCode() + "-" + result.getReturn().getActivateDescription();

    }

//    public List<Line> getLines(Client client) {
//
//        List<Line> ret = new ArrayList<>();
//
//        UrlLocator locator = new UrlLocator("dev");
//        URL url = locator.getFindLinesURL();
//
//        CableMsisdns service = new CableMsisdns(url);
//        CableMsisdnsPort port = service.getCableMsisdnsSoapService();
//
//        try {
//
//            // TODO initialize WS operation arguments here
//            MsisdnsMaintenanceRequest part1 = new MsisdnsMaintenanceRequest();
//
//            Instruction i = new Instruction();
//            Date date = new Date();
//            i.setOperationType("FIND");
//            i.setRefCustomerId(client.getRefCustomerId());
//            i.setResellerId(resId);
//            i.setResellerPin(resPin);
//            i.setTimeStamp(sdf.format(date));
//            i.setTransactionId(String.valueOf(date.getTime()));
//
//            SoapRequest request = new SoapRequest();
//            Operation operation = new Operation();
//
//            operation.setInstruction(i);
//
//            Find f = new Find();
//            /*
//            A : Linea activa
//            B : Linea de Baja
//            D : Linea desactivada
//            S : Linea en Suspension
//            J : Bloqueo por Call Barring
//            R : Bloqueo por riesgo.
//            L : Bloqueo temporal.*/
//            //f.setStatus("A");
//
//            operation.setFind(f);
//
//            request.setOperation(operation);
//            part1.setSoapRequest(request);
//            // TODO process result here
//            MsisdnsMaintenanceRequestResponse result = port.msisdnsMaintenance(part1);
//
//            if (result.getReturn() != null
//                    && result.getReturn().getMsisdnList() != null) {
//
//                List<MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn> list = result.getReturn().getMsisdnList().getMsisdn();
//                for (MsisdnsMaintenanceRequestResponse.Return.MsisdnList.Msisdn msisdn : list) {
//
//                    Line l = new Line();
//                    l.setClientId(client.getId());
//                    l.setIccid(msisdn.getIccid());
//                    l.setIccidpuk(msisdn.getPuk());
//                    l.setIccidtype(msisdn.getTypeIccid());
//                    l.setMsisdn(msisdn.getMsisdn());
//                    try {
//                        l.setActivationDate(sdf2.parse(msisdn.getActivateDate()));
//                    } catch (Exception e) {
//                    }
//
//                    Product p = productRepository.findOneByProductProfile(msisdn.getProductProfile());
//                    if (p == null) {
//                        pl.appendLine("Warning, failed to find product for: " + msisdn.getProductProfile());
//                    }
//                    l.setProduct(p);
//                    l.setXtrastatus(LineStatus.valueOf(msisdn.getStatus()));
//                    l.setXtrastatusdate(new Date());
//                    ret.add(l);
//                }
//            }
//        } catch (Exception ex) {
//            pl.appendLine("Error getting lines for client " + client.toString());
//        }
//
//        return ret;
//    }
}
