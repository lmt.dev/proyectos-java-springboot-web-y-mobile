/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.Contract;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ContractsRepository extends JpaRepository<Contract, Long>, JpaSpecificationExecutor<Contract> {

    List<Contract> findByLineId(Long lineId);

    List<Contract> findByLineIdAndStatus(Long lineId, String status);

    List<Contract> findByLineIdAndStatusAndActive(Long lineId, String status, boolean active);
    
    List<Contract> findByLineIdAndActive(Long lineId, boolean active);

    List<Contract> findByStatus(String status);

}
