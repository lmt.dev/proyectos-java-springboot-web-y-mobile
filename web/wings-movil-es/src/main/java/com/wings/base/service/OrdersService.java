/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.WingsProperties;
import com.wings.base.domain.UpsStore;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Order;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.Registration;
import com.wings.base.enums.LineStatus;
import com.wings.base.enums.OrderStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.repos.UpsStoresRepository;
import com.wings.base.service.EmailService;
import com.wings.base.service.UpsService;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author seba
 */
@Component
public class OrdersService {

    private final Logger log = LoggerFactory.getLogger(OrdersService.class);

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private LinesRepository clientLinesRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private UpsStoresRepository upsStoresRepository;

    @Autowired
    private PortLinesRepository linesRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UpsService upsService;

    private final String shipperNumber;
    private final String ordersDir;
    private final String reportPath;
    private final String logo;

    private final String companyName;
    private final String companyAddress;

    private final String from;
    private final String to;

    public OrdersService(WingsProperties properties) {
        this.shipperNumber = properties.getUps().getShipperNumber();
        this.ordersDir = properties.getUps().getOrdersDir();
        this.reportPath = properties.getUps().getReportPath();
        this.logo = properties.getLogo();

        this.companyName = properties.getPay().getMerchantName();
        this.companyAddress = properties.getContract().getDireccionWings();

        this.from = properties.getUps().getFrom();
        this.to = properties.getUps().getTo();

    }

//    public void createOrdersForLines(List<ClientLine> lines) {
//
//        Map<Long, List<ClientLine>> cl = new HashMap<>();
//        //agrupo por clientes
//        //para hacer 1 solo envio
//        for (ClientLine line : lines) {
//            Long id = line.getClientId();
//            if (cl.containsKey(id)) {
//                cl.get(id).add(line);
//            } else {
//                List<ClientLine> list = new ArrayList<>();
//                list.add(line);
//                cl.put(id, list);
//            }
//        }
//
//        for (Map.Entry<Long, List<ClientLine>> entry : cl.entrySet()) {
//
//            List<ClientLine> groupLine = entry.getValue();
//            Client c = null;
//            Long groupId = null;
//            for (ClientLine clientLine : groupLine) {
//
//                List<File> attachments = new ArrayList<>();
//                List<Order> orders = new LinkedList<>();
//
//                if (c == null) {
//                    c = clientLine.getClient();
//                }
//                Long regId = c.getRegistrationId();
//                Registration reg = registrationRepository.findOne(regId);
//
//                if (clientLine.isPort()) {
//                    Order o = new Order();
//                    o.setCreationDate(new Date());
//                    o.setRegistrationId(reg.getId());
//                    o.setStatus("created");
//
//                    o.setType(Order.TYPE_PORTA);
//                    o.setReferencia(Order.REF_PORTA);
//                    o.setDescripcion(Order.DESC_PORTA);
//                    o.setTelefono(clientLine.getNumber().toString());
//
//                    ordersRepository.save(o);
//                    if(groupId == null){
//                        groupId = o.getId();
//                    }
//                    orders.add(o);
//
//                } else {
//
//                    Order o = new Order();
//                    o.setCreationDate(new Date());
//                    o.setRegistrationId(reg.getId());
//                    o.setStatus("created");
//                    o.setTelefono(reg.getTelefono());
//                    o.setType(Order.TYPE_NEW);
//                    o.setReferencia(Order.REF_NEW);
//                    o.setDescripcion(Order.DESC_NEW);
//                    ordersRepository.save(o);
//                    orders.add(o);
//                }
//
//                if (groupLine.size() > 1) {
//                    if(groupId == null){
//                        
//                    }
//                }
//
//                clientLine.setInternalState(LineStatus.order_created.name());
//
//            }
//        }
//    }
    public void sendOrders(List<Order> orders, Long regId) {

        List<File> attachments = new ArrayList<>();
        Registration reg = registrationRepository.findOne(regId);

        attachments.addAll(createXml(orders, reg));
        attachments.add(createPdf(orders, reg));

        try {
            boolean send = emailService.sendMail(from, to, "Pedido " + orders.get(0).getId(), "", attachments);

            Date sended = new Date();
            for (Order order : orders) {
                order.setStatus(send ? "sended" : "mail_error");
                order.setSendDate(send ? sended : null);
            }

            //upload ftp
            try {
                upsService.upload(attachments);
            } catch (Exception e) {
                log.error("Error uploadings orders to UPS ftp", e);
            }

            ordersRepository.save(orders);
        } catch (Exception e) {
            log.error("Error sending orders to UPS ", e);
        }

    }

    /**
     * Send Orders To UPS ftp server, marks order as sended or
     * replacement_sended acording to order state is error, mark order as error
     * or error_replacement
     *
     * @param orders
     */
    public void sendOrders(List<Order> orders) {

        List<File> attachments = new ArrayList<>();

        attachments.addAll(createXml(orders));
        attachments.add(createPdf(orders));

        try {
            boolean emailSend = emailService.sendMail(from, to, "Pedido " + orders.get(0).getId(), "", attachments);

            if (!emailSend) {
                log.error("Error sending orders to UPS, failed to send email for  " + orders.get(0).getId());
            }

            boolean ftpUpload = false;
            //upload ftp
            try {
                ftpUpload = upsService.upload(attachments);
            } catch (Exception e) {
                log.error("Error uploadings orders to UPS ftp", e);
            }

            Date sended = new Date();
            for (Order order : orders) {

                if (order.getStatus().equals(OrderStatus.created.name())) {
                    order.setStatus((ftpUpload) ? OrderStatus.sended.name() : OrderStatus.error.name());
                } else if (order.getStatus().equals(OrderStatus.replacement_created.name())) {
                    order.setStatus((ftpUpload) ? OrderStatus.replacement_sended.name() : OrderStatus.replacement_error.name());
                }

                order.setSendDate((emailSend && ftpUpload) ? sended : null);
            }

        } catch (Exception e) {
            log.error("Error sending orders to UPS ", e);
        }
    }

    public List<File> createXml(List<Order> orders) {

        Order first = orders.get(0);
        Client client = first.getClient();
        //Registration reg = orders.get(0).getRegistration();
        List<File> attachments = new ArrayList<>();

        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("XML");
        resolver.setSuffix(".xml");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);

        Context context = new Context();

        if (client.getCompanyName() != null && !client.getCompanyName().isEmpty()) {
            context.setVariable("empresa", client.getCompanyName());
        } else {
            context.setVariable("empresa", client.getName() + " " + client.getLastname());
        }
        context.setVariable("nombreCompleto", client.getName() + " " + client.getLastname());

        String address = first.getShippingAddress();
        String cp = first.getShippingCp();
        String city = first.getShippingCity();
        String province = first.getShippingState();

        context.setVariable("direccion", address);
        context.setVariable("ciudad", city);
        context.setVariable("cp", cp);

        context.setVariable("shipperNumber", shipperNumber);
        context.setVariable("retira", false);
        context.setVariable("email", client.getEmail());

        if (first.getUpsStoreId() != null && !first.getUpsStoreId().isEmpty()) {

            List<UpsStore> stores = upsStoresRepository.findByTiendaId(first.getUpsStoreId());
            if (!stores.isEmpty()) {

                context.setVariable("retira", true);

                UpsStore store = stores.get(0);
                context.setVariable("upsName", store.getName());
                context.setVariable("upsAddress", store.getAddress());
                context.setVariable("upsCP", store.getCp());
                context.setVariable("upsCiudad", store.getCity());
                context.setVariable("upsId", store.getTiendaId());
            }

        }

        for (Order order : orders) {

            File ret = new File(ordersDir + "/" + order.getId() + ".xml");

            context.setVariable("numPedido", order.getId());
            context.setVariable("telefono", order.getTelefono());
            context.setVariable("tipoTarjeta", order.getDescripcion());

            switch (order.getReferencia()) {
                case Order.REF_NEW:
                    context.setVariable("tipoPedido", "Nueva");
                    break;
                case Order.REF_PORTA:
                    context.setVariable("tipoPedido", "Portabilidad");
                    break;
                case Order.REF_SUS:
                    context.setVariable("tipoPedido", "Sustitución");
                    break;
            }

            String response = engine.process("xmls/pedido_template", context);
            try {
                FileWriter fw = new FileWriter(ret);
                fw.write(response);
                fw.close();

                attachments.add(new File(ordersDir + "/" + order.getId() + ".xml"));

            } catch (IOException e) {
                log.error("Error creating order XML", e);
            }
        }

        return attachments;
    }

    public File createPdf(List<Order> orders) {

        Order first = orders.get(0);
        Registration reg = first.getRegistration();
        String destName = ordersDir + "/" + first.getId() + ".pdf";

        Map<String, Object> properties = new HashMap<>();

        String address = first.getShippingAddress();
        String cp = first.getShippingCp();
        String city = first.getShippingCity();
        String province = first.getShippingState();

        String destinatario = reg.getNombre() + " "
                + reg.getApellido() + ", "
                + address + " "
                + cp + " "
                + city + " "
                + province;

        properties.put("logo", logo);
        properties.put("numPedido", first.getId());
        properties.put("remitente", companyName + ", " + companyAddress);
        properties.put("destinatario", destinatario);

        try {

            List<Map<String, String>> fields = new ArrayList<>();
            for (Order order : orders) {
                Map<String, String> field = new HashMap<>();
                field.put("referencia", order.getReferencia());
                field.put("descripcion", order.getDescripcion());
                field.put("telefono", order.getReferencia().equals(Order.REF_PORTA) ? order.getTelefono() : "");
                field.put("pedido", order.getId().toString());
                fields.add(field);
            }

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(reportPath));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, properties, new JRMapArrayDataSource(fields.toArray()));

            JasperExportManager.exportReportToPdfFile(jprint, destName);

        } catch (JRException e) {
            log.error("Error creating orders pdf ", e);
        }

        return new File(destName);
    }

    /**
     * **************OLD CODE************************
     */
    private List<File> createXml(List<Order> orders, Registration reg) {

        List<File> attachments = new ArrayList<>();

        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("XML");
        resolver.setSuffix(".xml");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);

        Context context = new Context();
        if (reg.getRazonSocial() != null && !reg.getRazonSocial().isEmpty()) {
            context.setVariable("empresa", reg.getRazonSocial());
        } else {
            context.setVariable("empresa", reg.getNombre() + " " + reg.getApellido());
        }
        context.setVariable("nombreCompleto", reg.getNombre() + " " + reg.getApellido());

        String address = reg.getDireccionEnvio() != null ? reg.getDireccionEnvio() : reg.getDireccion();
        String cp = reg.getCpEnvio() != null ? reg.getCpEnvio() : reg.getCp();
        String city = reg.getCiudadEnvio() != null ? reg.getCiudadEnvio() : reg.getCiudad();

        context.setVariable("direccion", address);
        context.setVariable("ciudad", city);
        context.setVariable("cp", cp);

        context.setVariable("shipperNumber", shipperNumber);
        context.setVariable("retira", false);
        context.setVariable("email", reg.getEmail());

        if (reg.getTiendaId() != null && !reg.getTiendaId().isEmpty()) {

            List<UpsStore> stores = upsStoresRepository.findByTiendaId(reg.getTiendaId());
            if (!stores.isEmpty()) {

                context.setVariable("retira", true);

                UpsStore store = stores.get(0);
                context.setVariable("upsName", store.getName());
                context.setVariable("upsAddress", store.getAddress());
                context.setVariable("upsCP", store.getCp());
                context.setVariable("upsCiudad", store.getCity());
                context.setVariable("upsId", store.getTiendaId());
            }

        }

        for (Order order : orders) {

            File ret = new File(ordersDir + "/" + order.getId() + ".xml");

            context.setVariable("numPedido", order.getId());
            context.setVariable("telefono", order.getTelefono());
            context.setVariable("tipoTarjeta", order.getDescripcion());

            switch (order.getReferencia()) {
                case Order.REF_NEW:
                    context.setVariable("tipoPedido", "Nueva");
                    break;
                case Order.REF_PORTA:
                    context.setVariable("tipoPedido", "Portabilidad");
                    break;
                case Order.REF_SUS:
                    context.setVariable("tipoPedido", "Sustitución");
                    break;
            }

            String response = engine.process("xmls/pedido_template", context);
            try {
                FileWriter fw = new FileWriter(ret);
                fw.write(response);
                fw.close();

                attachments.add(new File(ordersDir + "/" + order.getId() + ".xml"));

            } catch (IOException e) {
                log.error("Error creating order XML", e);
            }
        }

        return attachments;
    }

    private File createPdf(List<Order> orders, Registration reg) {

        Order first = orders.get(0);
        String destName = ordersDir + "/" + first.getId() + ".pdf";

        Map<String, Object> properties = new HashMap<>();

        String address = reg.getDireccionEnvio() != null ? reg.getDireccionEnvio() : reg.getDireccion();
        String cp = reg.getCpEnvio() != null ? reg.getCpEnvio() : reg.getCp();
        String city = reg.getCiudadEnvio() != null ? reg.getCiudadEnvio() : reg.getCiudad();
        String province = reg.getProvinciaEnvio() != null ? reg.getProvinciaEnvio() : reg.getProvincia();

        String destinatario = reg.getNombre() + " "
                + reg.getApellido() + ", "
                + address + " "
                + cp + " "
                + city + " "
                + province;

        properties.put("logo", logo);
        properties.put("numPedido", first.getId());
        properties.put("remitente", companyName + ", " + companyAddress);
        properties.put("destinatario", destinatario);

        try {

            List<Map<String, String>> fields = new ArrayList<>();
            for (Order order : orders) {
                Map<String, String> field = new HashMap<>();
                field.put("referencia", order.getReferencia());
                field.put("descripcion", order.getDescripcion());
                field.put("telefono", order.getReferencia().equals(Order.REF_PORTA) ? order.getTelefono() : "");
                field.put("pedido", order.getId().toString());
                fields.add(field);
            }

            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(reportPath));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, properties, new JRMapArrayDataSource(fields.toArray()));

            JasperExportManager.exportReportToPdfFile(jprint, destName);

        } catch (JRException e) {
            log.error("Error creating orders pdf ", e);
        }

        return new File(destName);
    }

}
