/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Balance;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class BalanceSpecs {

    public static Specification<Balance> balanceSearch(String client, String line, String month, String year) {

        return (Root<Balance> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (client != null) {
                predicates.add(builder.equal(root.get("clientId"), client));
            }

            if (line != null) {
                predicates.add(builder.equal(root.get("lineId"), line));
            }

            if (month != null) {
                predicates.add(builder.equal(root.get("month"), month));
            }

            if (year != null) {
                predicates.add(builder.equal(root.get("year"), year));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
