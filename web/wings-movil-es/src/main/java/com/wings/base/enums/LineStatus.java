/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.enums;

/**
 *
 * @author seba
 */
public enum LineStatus {

    /**
     * Line create in local database
     */
    created_local,
    /**
     * Line with contract created
     */
    contract_created,
    /**
     * Line with contract sended to sigma
     */
    contract_sended,
    /**
     * Line with contract sended to sigma
     */
    contract_resended,
    /**
     * Line with contract signed
     */
    contract_signed,
    /**
     * Line with order created
     */
    order_created,
    /**
     * Line with order sended to UPS
     */
    order_sended,
    /**
     * Line with tracking code, means UPS already has the order we should get
     * the iccid here as well
     */
    order_received,
    /**
     * Line with SIM delivered old order_done
     */
    order_delivered,
    /**
     * Line with SIM delivered old order_done
     */
    order_deliver_error,
    /**
     * Line created on the operator side
     */
    created_operator,
    /**
     * Line with portability requested on the operator side
     */
    port_requested,
    /**
     * Portability has been rejected for some reason
     */
    port_rejected,
    /**
     * Portability has been accepted
     */
    port_accepted,
    /**
     * Brand change form already sended to support
     */
    change_brand_requested,
    /**
     * Brand change has been rejected for some reason
     */
    change_brand_rejected,
    /**
     * Brand change has been accepted
     */
    change_brand_accepted,
    /**
     * Line is active
     */
    active,
    /**
     * Line is suspended
     */
    suspended,
    /**
     * Outgoing call blocked
     */
    callBarring,
    /**
     * Blocked hot Line ???
     */
    hotLine,
    /**
     * Line flagged as Fraud
     */
    fraud,
    /**
     * line flagged as theft
     */
    theft,
    /**
     * Line is suspended
     */
    inactive,
    /**
     * Line is suspended
     */
    canceled,
}
