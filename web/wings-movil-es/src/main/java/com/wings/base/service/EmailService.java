/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.client.Contract;
import java.io.File;
import java.util.List;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author seba
 */
@Service
public class EmailService {

    @Autowired
    private Environment environment;

    private final Logger log = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender mailSender;

    @Autowired
    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public boolean sendMail(String from, String to, String subject, String text, List<File> attachements) {

        boolean ret = false;

        MimeMessage message = mailSender.createMimeMessage();
        //prevent sending emails to clients on dev
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("dev")) {
                to = "Test<sebastian.lucero@wingsmobile.es>";
            }
        }

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(from);

            if (to.contains(",")) {
                String[] tos = to.split(",");
                helper.setTo(tos);
            } else {
                helper.setTo(to);
            }

            helper.setSubject(subject);
            helper.setText(text, true);

            if (attachements != null) {
                for (File f : attachements) {
                    if (f.exists()) {
                        helper.addAttachment(f.getName(), f);
                    }
                }
            }

            mailSender.send(message);

            ret = true;

        } catch (Exception e) {
            log.error("Error sending mail", e);
        }
        return ret;
    }

    public String getSignatureEmail(Contract contract) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("nombre", contract.getClient().getName());
        return engine.process("templates/email/email_contract", context);
    }

    public String getGenericEmail(String client, String subject, String body) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("LEGACYHTML5");
        resolver.setSuffix(".html");
        resolver.setCharacterEncoding("UTF-8");
        TemplateEngine engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
        Context context = new Context();
        context.setVariable("client", client);
        context.setVariable("subject", subject);
        context.setVariable("body", body);
        return engine.process("templates/email/email_generic", context);
    }
}
