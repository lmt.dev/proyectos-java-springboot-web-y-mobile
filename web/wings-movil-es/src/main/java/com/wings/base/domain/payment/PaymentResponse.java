/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.payment;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author seba
 */
@XmlRootElement(name = "response")
public class PaymentResponse {

    @XmlElement(name = "Ds_Message", required = true)
    private String Ds_Message;

    @XmlElement(name = "Ds_Merchant_MatchingData", required = true)
    private String Ds_Merchant_MatchingData;
    @XmlElement(name = "Ds_Bank", required = true)
    private String Ds_Bank;

    @XmlElement(name = "Ds_Date", required = true)
    private String Ds_Date;

    @XmlElement(name = "Ds_AuthorisationCode", required = true)
    private String Ds_AuthorisationCode;

    @XmlElement(name = "Ds_PanMask", required = true)
    private String Ds_PanMask;

    @XmlElement(name = "Ds_Expiry", required = true)
    private String Ds_Expiry;

    @XmlElement(name = "Ds_Merchant_TransactionType", required = true)
    private String Ds_Merchant_TransactionType;

    @XmlElement(name = "Ds_Amount", required = true)
    private String Ds_Amount;

    @XmlElement(name = "Ds_Code", required = true)
    private String Ds_Code;

    @XmlElement(name = "Ds_Merchant_Guarantees", required = true)
    private String Ds_Merchant_Guarantees;

    @XmlElement(name = "Ds_Merchant_MerchantCode", required = true)
    private String Ds_Merchant_MerchantCode;

    @XmlElement(name = "Ds_Signature", required = true)
    private String Ds_Signature;

    public String getDs_Message() {
        return Ds_Message;
    }

    public void setDs_Message(String Ds_Message) {
        this.Ds_Message = Ds_Message;
    }

    public String getDs_Merchant_MatchingData() {
        return Ds_Merchant_MatchingData;
    }

    public void setDs_Merchant_MatchingData(String Ds_Merchant_MatchingData) {
        this.Ds_Merchant_MatchingData = Ds_Merchant_MatchingData;
    }

    public String getDs_Bank() {
        return Ds_Bank;
    }

    public void setDs_Bank(String Ds_Bank) {
        this.Ds_Bank = Ds_Bank;
    }

    public String getDs_Date() {
        return Ds_Date;
    }

    public void setDs_Date(String Ds_Date) {
        this.Ds_Date = Ds_Date;
    }

    public String getDs_AuthorisationCode() {
        return Ds_AuthorisationCode;
    }

    public void setDs_AuthorisationCode(String Ds_AuthorisationCode) {
        this.Ds_AuthorisationCode = Ds_AuthorisationCode;
    }

    public String getDs_PanMask() {
        return Ds_PanMask;
    }

    public void setDs_PanMask(String Ds_PanMask) {
        this.Ds_PanMask = Ds_PanMask;
    }

    public String getDs_Expiry() {
        return Ds_Expiry;
    }

    public void setDs_Expiry(String Ds_Expiry) {
        this.Ds_Expiry = Ds_Expiry;
    }

    public String getDs_Merchant_TransactionType() {
        return Ds_Merchant_TransactionType;
    }

    public void setDs_Merchant_TransactionType(String Ds_Merchant_TransactionType) {
        this.Ds_Merchant_TransactionType = Ds_Merchant_TransactionType;
    }

    public String getDs_Amount() {
        return Ds_Amount;
    }

    public void setDs_Amount(String Ds_Amount) {
        this.Ds_Amount = Ds_Amount;
    }

    public String getDs_Code() {
        return Ds_Code;
    }

    public void setDs_Code(String Ds_Code) {
        this.Ds_Code = Ds_Code;
    }

    public String getDs_Merchant_Guarantees() {
        return Ds_Merchant_Guarantees;
    }

    public void setDs_Merchant_Guarantees(String Ds_Merchant_Guarantees) {
        this.Ds_Merchant_Guarantees = Ds_Merchant_Guarantees;
    }

    public String getDs_Merchant_MerchantCode() {
        return Ds_Merchant_MerchantCode;
    }

    public void setDs_Merchant_MerchantCode(String Ds_Merchant_MerchantCode) {
        this.Ds_Merchant_MerchantCode = Ds_Merchant_MerchantCode;
    }

    public String getDs_Signature() {
        return Ds_Signature;
    }

    public void setDs_Signature(String Ds_Signature) {
        this.Ds_Signature = Ds_Signature;
    }

    
}
