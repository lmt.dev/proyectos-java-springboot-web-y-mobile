/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;


import com.wings.base.domain.registration.RegistrationCallback;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface RegistrationCallbackRepository extends JpaRepository<RegistrationCallback, Long>{

    List<RegistrationCallback> findByMatchingData(String matchingData);
    
    RegistrationCallback findByMatchingDataAndMessage(String matchingData, String message);
        
}
