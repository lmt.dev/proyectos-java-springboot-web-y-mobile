/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.client;

import com.wings.base.utils.NumberUtils;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "client_debts")
public class ClientDebt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long clientId;
    private long activeLines;
    private long canceledLines;
    private double balance;

    @OneToOne(optional = false)
    @JoinColumn(name = "clientId", insertable = false, updatable = false)
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getActiveLines() {
        return activeLines;
    }

    public void setActiveLines(long activeLines) {
        this.activeLines = activeLines;
    }

    public long getCanceledLines() {
        return canceledLines;
    }

    public void setCanceledLines(long canceledLines) {
        this.canceledLines = canceledLines;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getBalanceText() {
        return NumberUtils.formatMoney(balance);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
