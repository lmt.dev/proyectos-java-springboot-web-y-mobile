/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.ClientActivity;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientsActivitiesRepository extends JpaRepository<ClientActivity, Long>, JpaSpecificationExecutor<ClientActivity> {

    List<ClientActivity> findByClientId(Long clientId);

    List<ClientActivity> findByClientId(Long clientId, Sort sort);

    List<ClientActivity> findByClientIdAndLineId(Long clientId, Long lineId);

    List<ClientActivity> findByLineId(Long lineId);

}
