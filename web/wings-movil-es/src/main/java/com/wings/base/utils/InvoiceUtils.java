/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.text.RandomStringGenerator;
import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

/**
 *
 * @author seba
 */
public class InvoiceUtils {

    private static final String LABELS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    public static String getEcode(Long clientId, int month, int year) {

        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(LETTERS, DIGITS)
                .build();

        String prefix = generator.generate(5).toUpperCase();
        String suffix = generator.generate(4).toUpperCase();

        StringBuilder b = new StringBuilder();
        String sum = getSumCheck(prefix, suffix);
        b.append(prefix).append("-");
        b.append(suffix).append("-");
        b.append(sum);
        return b.toString();
    }

    public static String getSumCheck(String prefix, String suffix) {

        int sum1 = 0;
        int sum2 = 0;
        char[] prex = prefix.toCharArray();
        for (char c : prex) {
            sum1 += LABELS.indexOf(c);
        }
        char[] suf = suffix.toCharArray();
        for (char c : suf) {
            sum2 += LABELS.indexOf(c);
        }

        sum1 = sum1 / LABELS.length();
        sum2 = sum2 / LABELS.length();

        String ret = String.valueOf(LABELS.charAt(sum1));
        ret += String.valueOf(LABELS.charAt(sum2));

        return ret;
    }

    public static String getInvoiceToken(String ecode) {
        return DigestUtils.sha512Hex(System.currentTimeMillis() + ecode);
    }

    public static String getInvoiceNumber(Long periodNumber, int year) {
        return "S" + String.format("%08d", periodNumber) + "-" + year;
    }

}
