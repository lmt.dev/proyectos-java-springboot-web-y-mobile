/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.Token;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface TokensRepository extends JpaRepository<Token, Long>, JpaSpecificationExecutor<Token> {

    Token findByToken(String token);

    List<Token> findByReferenceIdAndTokenType(Long referenceId, String tokenType);

}
