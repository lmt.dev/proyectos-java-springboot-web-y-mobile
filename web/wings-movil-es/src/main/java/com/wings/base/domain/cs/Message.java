/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.cs;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.wings.base.domain.client.Client;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "cs_messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long lineId;
    private Long clientId;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date;
    private String user;
    private String text;
    private String line;
    private Boolean techNotif;
    private Boolean incidence = Boolean.FALSE;
    private Boolean techRead;
    private String incidenceStatus;
    private String incidenceUser;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date incidenceClosed;

    @ManyToOne(optional = false)
    @JoinColumn(name = "clientId", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonBackReference
    private Client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLineId() {
        return lineId;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public Boolean getTechNotif() {
        return techNotif;
    }

    public void setTechNotif(Boolean techNotif) {
        this.techNotif = techNotif;
    }

    public Boolean getTechRead() {
        return techRead;
    }

    public void setTechRead(Boolean techRead) {
        this.techRead = techRead;
    }

    public void setIncidence(Boolean incidence) {
        this.incidence = incidence;
    }

    public Boolean getIncidence() {
        return incidence;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getIncidenceStatus() {
        return incidenceStatus;
    }

    public void setIncidenceStatus(String incidenceStatus) {
        this.incidenceStatus = incidenceStatus;
    }

    public void setIncidenceUser(String incidenceUser) {
        this.incidenceUser = incidenceUser;
    }

    public String getIncidenceUser() {
        return incidenceUser;
    }

    public Date getIncidenceClosed() {
        return incidenceClosed;
    }

    public void setIncidenceClosed(Date incidenceClosed) {
        this.incidenceClosed = incidenceClosed;
    }

}
