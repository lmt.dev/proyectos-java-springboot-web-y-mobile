/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.specs.OrderSpecs;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Order;
import com.wings.base.domain.client.OrderActivity;
import com.wings.base.domain.registration.Registration;
import com.wings.base.enums.LineStatus;
import com.wings.base.process.LineProcess;
import com.wings.base.process.OrderProcess;
import com.wings.base.service.OrdersService;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrderActivityRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.service.UpsService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Controller
public class OrdersController {

    private final Logger log = LoggerFactory.getLogger(OrdersController.class);

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private OrdersService orderService;
    
    @Autowired
    private OrderProcess orderProcess;

    @Autowired
    private UpsService upsService;

    @Autowired
    private OrderActivityRepository orderActivityRepository;

    @Autowired
    private LineProcess lineProcess;

    @RequestMapping(value = {"views/clients/orders"}, method = RequestMethod.GET)
    public String getOrders(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));
        Page<Order> items = ordersRepository.findAll(OrderSpecs.orderSearch(q, status), pr);
        PageWrapper<Order> pageWrapper = new PageWrapper<>(items, "views/clients/orders");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);

        return "views/clients/orders";
    }

    @RequestMapping(value = {"views/clients/orders/{id}/form"}, method = RequestMethod.GET)
    public String getOrderForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Order order = ordersRepository.findOne(id);
        model.addAttribute("title", "Linea");
        model.addAttribute("subTitle", order.toString());
        model.addAttribute("elements", MapUtils.getFormElements(order));

        return "views/common/view-form";
    }

    @RequestMapping(value = {"views/clients/orders/import"}, method = RequestMethod.GET)
    public String getImportForm(Model model, HttpServletRequest request) {
        return "views/clients/import-csv";
    }

    @RequestMapping(value = {"views/clients/orders/tracking/{trackingNumber}/view"}, method = RequestMethod.GET)
    public String getOrderActivity(Model model, HttpServletRequest request, @PathVariable("trackingNumber") String trackingNumber) {
        List<OrderActivity> activities = orderActivityRepository.findByTrackingCode(trackingNumber, new Sort(Sort.Direction.DESC, "activityDate"));
        model.addAttribute("activities", activities);
        model.addAttribute("trackingNumber", trackingNumber);
        return "views/clients/order-activity";
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/orders/tracking/{trackingNumber}"}, method = RequestMethod.POST)
    public ResponseEntity getOrderActivityUps(Model model, HttpServletRequest request, @PathVariable("trackingNumber") String trackingNumber) {

        
        List<Order> orders = new ArrayList<>();
        Order or = ordersRepository.findBytrackingNumber(trackingNumber);
        orders.add(or);
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        String launcher = user.getUsername();        
        orderProcess.checkUpsDeliveryStatus(launcher, orders);
        

        return ResponseEntity.ok().build();
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/orders/import"}, method = RequestMethod.POST)
    public String postImportFile(Model model, HttpServletRequest request, @RequestParam("file") MultipartFile file) {

        String ret = "";
        if (file != null && !file.isEmpty()) {

            try {
                BufferedReader bf = new BufferedReader(new InputStreamReader(file.getInputStream()));

                String strLine;
                while ((strLine = bf.readLine()) != null) {
                    if (strLine.startsWith("Anulado")) {
                        continue;
                    }

                    //Anulado,Tracking,pedido,ICC,Tipo,Destinatario,Telefono
                    String[] array = strLine.split(",");
                    
                    Long orderId = null;
                    try {
                        orderId = Long.valueOf(array[2].replaceAll("\"", ""));
                    } catch (Exception e) {
                    }

                    if (orderId == null) {
                        continue;
                    }
                    
                    Order o = ordersRepository.findOne(orderId);

                    if (o == null) {
                        ret += "<br/>No se encontro orden para " + array[2];
                        continue;
                    }
                    o.setTrackingNumber(array[1].replaceAll("\"", ""));

                    ClientLine line = null;
                    if (o.getLineId() != null) {
                        line = linesRepository.findOne(o.getLineId());
                    }
                    if (line == null) {
                        line = linesRepository.findByNumber(Long.parseLong(o.getTelefono()));
                    }
                    if (line == null) {
                        ret += "<br/>No se encontro linea para " + array[2] + " - " + array[3];
                        continue;
                    }
                    line.setIccid(array[3].replaceAll("\"", ""));
                    line.setInternalState(LineStatus.order_received.name());

                    ordersRepository.save(o);
                    linesRepository.save(line);
                }

                ret += "<br/>Archivo importado existosamente";
            } catch (IOException | NumberFormatException e) {
                log.error("Error importing csv", e);
                ret += "<br/>Error importing csv: " + e.getMessage();
            }

        } else {
            ret += "<br/>No se puede leer el archivo";
        }

        return ret;
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/orders/create"}, method = RequestMethod.POST)
    public String postCreateOrders(Model model, HttpServletRequest request, @RequestParam("ids[]") String sids) {
        StringBuilder ret = new StringBuilder();

        //lines ids
        List<Long> ids = new ArrayList<>();
        String ar[] = sids.split(",");
        for (String sid : ar) {
            try {
                ids.add(Long.parseLong(sid));
            } catch (Exception e) {
            }
        }

        List<ClientLine> lines = linesRepository.findAll(ids);
        List<Order> orders = new ArrayList<>();
        for (ClientLine clientLine : lines) {

            if (clientLine.getInternalState() != null && clientLine.getInternalState().equals(LineStatus.order_created.name())) {
                ret.append("<br/>Orden para linea").append(clientLine.getNumber()).append(" ya existe, continuando..");
                continue;
            }

            Long regId = clientLine.getRegistrationId();
            Registration reg = registrationRepository.findOne(regId);

            Order o = new Order();
            o.setCreationDate(new Date());
            o.setRegistrationId(reg.getId());
            o.setClientId(clientLine.getClientId());
            o.setLineId(clientLine.getId());
            o.setStatus("created");

            String address = reg.getDireccionEnvio() != null ? reg.getDireccionEnvio() : reg.getDireccion();
            String city = reg.getCiudadEnvio() != null ? reg.getCiudadEnvio() : reg.getCiudad();
            String state = reg.getProvinciaEnvio() != null ? reg.getProvinciaEnvio() : reg.getProvincia();
            String cp = reg.getCpEnvio() != null ? reg.getCpEnvio() : reg.getCp();

            o.setShippingAddress(address);
            o.setShippingCity(city);
            o.setShippingState(state);
            o.setShippingCp(cp);
            o.setUpsStoreId(reg.getTiendaId());

            if (clientLine.isPort()) {
                o.setType(Order.TYPE_PORTA);
                o.setReferencia(Order.REF_PORTA);
                o.setDescripcion(Order.DESC_PORTA);
                o.setTelefono(clientLine.getNumber().toString());
            } else {
                o.setTelefono(reg.getTelefono());
                o.setType(Order.TYPE_NEW);
                o.setReferencia(Order.REF_NEW);
                o.setDescripcion(Order.DESC_NEW);
            }

            orders.add(o);
            clientLine.setInternalState(LineStatus.order_created.name());
        }

        orders = ordersRepository.save(orders);

        //seteo los parents
        for (Order created : orders) {
            List<Order> toset = ordersRepository.findByRegistrationId(created.getRegistrationId(), new Sort(Sort.Direction.ASC, "id"));
            if (toset.size() > 1) {
                boolean first = true;
                long parent = 0l;
                for (Order order : toset) {
                    if (first) {
                        first = false;
                        parent = order.getId();
                        continue;
                    }
                    order.setParent(parent);
                    ordersRepository.save(order);
                }
            }
        }

        ret.append("<br/>Ordenes creadas ").append(orders.size()).append(" de ").append(ids.size());

        return ret.toString();
    }

    @ResponseBody
    @RequestMapping(value = {"api/clients/orders/send"}, method = RequestMethod.POST)
    public String postSendOrders(Model model, HttpServletRequest request) {
        StringBuilder ret = new StringBuilder();

        Map<Long, List<Order>> grouped = new HashMap<>();
        //agrupo por registrationId
        List<Order> toSend = ordersRepository.findByStatus("created");
        for (Order order : toSend) {
            Long regId = order.getRegistrationId();
            if (grouped.containsKey(regId)) {
                grouped.get(regId).add(order);
            } else {
                List<Order> list = new ArrayList<>();
                list.add(order);
                grouped.put(regId, list);
            }
        }

        for (Map.Entry<Long, List<Order>> entry : grouped.entrySet()) {
            Long regId = entry.getKey();
            List<Order> orders = entry.getValue();
            orderService.sendOrders(orders, regId);
        }

        ret.append("<br/> Pedidos enviados");

        return ret.toString();
    }

    @RequestMapping(value = {"views/clients/orders/{id}/xml"}, method = RequestMethod.GET)
    public String getOrderXml(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Order order = ordersRepository.findOne(id);

        List<Order> orders = new ArrayList<>();
        orders.add(order);                
        List<File> files = orderService.createXml(orders);
        
        StringBuilder b = new StringBuilder();
        for (File file : files) {            
            try {
                b.append(new String(Files.readAllBytes(file.toPath())));
            } catch (Exception e) {
            }
        }
        
        String content = b.toString().replaceAll("<", "&lt;");        
        model.addAttribute("content", content);
        return "views/common/view-xml";
    }
    
    @RequestMapping(value = {"views/clients/orders/{id}/pdf"}, method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> getOrderPdf(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Order order = ordersRepository.findOne(id);
        
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        File pdfFile = orderService.createPdf(orders);
        
        FileInputStream fis;
        try {
            fis = new FileInputStream(pdfFile);
        } catch (FileNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Content-Disposition", "filename=" + order.getId().toString() + ".pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return new ResponseEntity<>(new InputStreamResource(fis), headers, HttpStatus.OK);
    }
    
    
}
