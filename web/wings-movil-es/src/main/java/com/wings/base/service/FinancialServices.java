/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.client.ClientDebt;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientDebtsReminder;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.repos.ClientDebtsReminderRepository;
import com.wings.base.repos.ClientDebtsRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.NumberUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author seba
 */
@Service
public class FinancialServices {

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private ClientDebtsRepository clientDebtsRepository;

    @Autowired
    private ClientDebtsReminderRepository clientDebtsReminderRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private LinesRepository linesRepository;

    private String senderText = "Iniciado";
    private long email = 0l;
    private long sms = 0l;

    public synchronized String getSenderText() {
        return senderText;
    }

    public synchronized long getEmail() {
        return email;
    }

    public synchronized long getSms() {
        return sms;
    }

    @Transactional
    public String reGenerateTable() {

        List<ClientDebt> toSave = new ArrayList<>();
        List<Client> clients = clientsRepository.findByBalanceGreaterThan(0.0);

        clients.stream().map((client) -> {
            ClientDebt debt = new ClientDebt();
            debt.setActiveLines(client.getActiveLines());
            debt.setBalance(client.getBalance());
            debt.setCanceledLines(client.getCanceledLines());
            debt.setClientId(client.getId());
            return debt;
        }).forEachOrdered((debt) -> {
            toSave.add(debt);
        });

        clientDebtsRepository.deleteAll();
        clientDebtsRepository.save(toSave);

        return "Registros generados " + toSave.size();
    }

    public String getCsv() {

        StringBuilder b = new StringBuilder();
        b.append("Nombre;Telefono;Movil;Dni;Lineas Activas;Lineas Canceladas;Referencia;Balance").append(System.lineSeparator());

        List<ClientDebt> debts = clientDebtsRepository.findAll();
        debts.forEach((debt) -> {
            b
                    .append(debt.getClient().getName()).append(" ").append(debt.getClient().getLastname()).append(";")
                    .append(debt.getClient().getPhone()).append(";")                    
                    .append(debt.getClient().getMobile()).append(";")                    
                    .append(debt.getClient().getIdType()).append(" ").append(debt.getClient().getIdValue()).append(";")
                    .append(debt.getActiveLines()).append(";")
                    .append(debt.getCanceledLines()).append(";")
                    .append(debt.getClient().getRefCustomerId()).append(";")
                    .append(debt.getBalanceText()).append(System.lineSeparator());
        });

        return b.toString();
    }

    /**
     * Regenerate reminders
     *
     * @return
     */
    @Transactional
    public String regenReminders() {

        String emailText = "Estimado cliente segun nuestros registros su cuenta tiene deuda pendiente, por favor siga el siguiente LINK_DE_PAGOS para abonar las facturas adeudadas, evite la suspencion de la linea";
        String smsText = "Su cuenta presenta deuda, por favor regularize su situacion LINK_DE_PAGOS para evitar la suspencion de la linea";

        int updated = 0;
        List<ClientDebtsReminder> toSave = new ArrayList<>();
        List<ClientDebt> debts = clientDebtsRepository.findAll();
        List<Long> clientIds = new ArrayList<>();

        for (ClientDebt debt : debts) {

            clientIds.add(debt.getClientId());

            ClientDebtsReminder already = clientDebtsReminderRepository.findFirstByClientId(debt.getClientId());
            if (already != null) {
                //only one per client but update balance
                if (already.getBalance() != debt.getBalance()) {
                    already.setBalance(debt.getBalance());
                    already.setLastSendDate(new Date());
                    updated++;
                    clientDebtsReminderRepository.save(already);
                }
                continue;
            }

            ClientDebtsReminder rem = new ClientDebtsReminder();
            rem.setClientId(debt.getClientId());
            rem.setCreationDate(new Date());
            rem.setLastSendDate(null);
            rem.setEmail(0);
            rem.setSms(0);
            rem.setSmsStatus(null);
            rem.setEmailStatus(null);
            rem.setVisited(0);
            rem.setCounter(0);
            rem.setBalance(debt.getBalance());

            //chek for unique token
            String token;
            do {
                token = NumberUtils.getRandomString(10);
                already = clientDebtsReminderRepository.findFirstByToken(token);
            } while (already != null);
            rem.setToken(token);

            rem.setSmsText(smsText.replaceAll("LINK_DE_PAGOS", "https://movil.wingsmobile.net/p/" + token));
            rem.setEmailText(emailText.replaceAll("LINK_DE_PAGOS", "<a href='https://movil.wingsmobile.net/p/" + token + "'>enlace</a>"));

            toSave.add(rem);
        }

        //save reminders
        clientDebtsReminderRepository.save(toSave);
        //delete the ones that not exists anymore
        int deleted = clientDebtsReminderRepository.deleteByClientIdNotIn(clientIds);

        return "Registros generados " + toSave.size() + ", actualizados " + updated + ", eliminados " + deleted;
    }

    /**
     * Send reminders emails and sms
     */
    @Scheduled(initialDelay = 10000, fixedDelay = 60000)
    public void sender() {

        if (!DateUtils.isInWorkHours(new Date())) {
            senderText = "Fuera de horario, no se hacen envios";
            return;
        }

        senderText = "Ejecutando...";
        List<ClientDebtsReminder> toSend = clientDebtsReminderRepository.findFirst20ByCounterGreaterThanOrderByBalanceDesc(0);
        for (ClientDebtsReminder clientDebtsReminder : toSend) {
            //email
            String content = emailService.getGenericEmail(clientDebtsReminder.getClient().toString(), "Wings Mobile - Aviso de Deuda", clientDebtsReminder.getEmailText());
            emailService.sendMail("no-reply@wingsmobile.es", clientDebtsReminder.getClient().getEmail(), "Wings Mobile - Aviso de Deuda", content, null);
            email++;

            //smss
            Client c = clientDebtsReminder.getClient();
            List<ClientLine> lines = c.getLines();

            if (lines == null || lines.isEmpty()) {
                lines = linesRepository.findByClientId(c.getId());
            }

            for (ClientLine line : lines) {
                if (line.getActivationDate() != null && line.getCancelDate() == null) {
                    smsService.sendSms(line.getNumber().toString(), clientDebtsReminder.getSmsText());
                    sms++;
                }
            }

            clientDebtsReminder.setLastSendDate(new Date());
            clientDebtsReminder.setCounter(clientDebtsReminder.getCounter() - 1);
            clientDebtsReminder.setEmail(clientDebtsReminder.getEmail() + 1);
            clientDebtsReminder.setSms(clientDebtsReminder.getSms() + 1);

            clientDebtsReminderRepository.save(clientDebtsReminder);
        }

        senderText = "Ultima ejecucion " + FormatUtils.formatHumanDate(new Date()) + " envios:" + toSend.size();

    }

    public String sendSms(Long reminderId) {
        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(reminderId);

        StringBuilder b = new StringBuilder();
        Client c = rem.getClient();
        List<ClientLine> lines = c.getLines();

        if (lines == null || lines.isEmpty()) {
            lines = linesRepository.findByClientId(c.getId());
        }

        for (ClientLine line : lines) {
            if (line.getActivationDate() != null && line.getCancelDate() == null) {
                b.append(smsService.sendSms(line.getNumber().toString(), rem.getSmsText()));
                b.append("<br/>");
                b.append(System.lineSeparator());
            }
        }

        rem.setLastSendDate(new Date());
        rem.setSms(rem.getSms() + 1);
        clientDebtsReminderRepository.save(rem);

        return b.toString();
    }

    public String sendSms(Long reminderId, String number) {
        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(reminderId);

        StringBuilder b = new StringBuilder();
        b.append(smsService.sendSms(number, rem.getSmsText()));
        b.append(System.lineSeparator());

        return b.toString();
    }

    public String sendEmail(Long reminderId) {
        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(reminderId);
        String content = emailService.getGenericEmail(rem.getClient().toString(), "Wings Mobile - Aviso de Deuda", rem.getEmailText());
        boolean ret = emailService.sendMail("no-reply@wingsmobile.es", rem.getClient().getEmail(), "Wings Mobile - Aviso de Deuda", content, null);

        String message;
        if (ret) {
            rem.setLastSendDate(new Date());
            rem.setEmail(rem.getEmail() + 1);
            clientDebtsReminderRepository.save(rem);
            message = "Email enviado a " + rem.getClient().getEmail();
        } else {
            message = "Error enviando email";
        }

        return message;
    }

    public String sendEmail(Long reminderId, String email) {
        ClientDebtsReminder rem = clientDebtsReminderRepository.findOne(reminderId);
        String content = emailService.getGenericEmail(rem.getClient().toString(), "Wings Mobile - Aviso de Deuda", rem.getEmailText());
        boolean ret = emailService.sendMail("no-reply@wingsmobile.es", email, "Wings Mobile - Aviso de Deuda", content, null);

        String message;
        if (ret) {
            message = "Email enviado a " + email;
        } else {
            message = "Error enviando email";
        }

        return message;
    }

    /**
     * Increment counter by one, making the sender() process to read this
     * reminder and send sms and email
     *
     * @param id
     */
    public void send(Long id) {

        List<ClientDebtsReminder> rems = new ArrayList<>();
        if (id == null) {
            rems = clientDebtsReminderRepository.findAll(new Sort(Sort.Direction.DESC, "balance"));
        } else {
            rems.add(clientDebtsReminderRepository.findOne(id));
        }

        for (ClientDebtsReminder rem : rems) {
            rem.setCounter(rem.getCounter() + 1);
            rem.setLastSendDate(new Date());
        }

        clientDebtsReminderRepository.save(rems);
    }

    /**
     * Put counter to 0, making the sender() stop sending
     */
    public void stopSend() {

        List<ClientDebtsReminder> rems = clientDebtsReminderRepository.findAll();
        rems.forEach((rem) -> {
            rem.setCounter(0);
        });
        clientDebtsReminderRepository.save(rems);

    }
}
