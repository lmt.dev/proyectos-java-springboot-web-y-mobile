/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.OrderActivity;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface OrderActivityRepository extends JpaRepository<OrderActivity, Long>{
    
    List<OrderActivity> findByTrackingCode(String tackingCode,Sort sort);
    
}
