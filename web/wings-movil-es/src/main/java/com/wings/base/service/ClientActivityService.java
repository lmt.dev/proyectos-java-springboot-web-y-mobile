/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.ClientActivity;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.enums.ClientActivityType;
import com.wings.base.repos.ClientsActivitiesRepository;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class ClientActivityService {

    @Autowired
    private ClientsActivitiesRepository clientsActivitiesRepository;

    private List<ClientActivity> toSave = Collections.synchronizedList(new LinkedList<>());

    public void createLineActivity(String title, ClientLine line) {
        createActivity(title, null, line, new Date(), ClientActivityType.line.name());
    }

    public void createLineActivity(String title, String comment, ClientLine line) {
        createActivity(title, comment, line, new Date(), ClientActivityType.line.name());
    }

    public void createContractActivity(String title, ClientLine line) {
        createActivity(title, null, line, new Date(), ClientActivityType.line_contract.name());
    }
        
    public void createOrderActivity(String title, ClientLine line) {
        createActivity(title, null, line, new Date(), ClientActivityType.line_order.name());
    }

    public void createOperatorActivity(String title, String comment, ClientLine line) {
        createActivity(title, comment, line, new Date(), ClientActivityType.line_operator.name());
    }

    public void createActivity(String title, String comment, ClientLine line, Date activityDate, String activityTpe) {
        
        ClientActivity ca = new ClientActivity();

        ca.setCreationDate(new Date());

        ca.setActivityType(activityTpe);
        ca.setActivityDate(activityDate);
        ca.setClientId(line.getClientId());
        ca.setLineId(line.getId());

        ca.setComment(comment);
        ca.setTitle(title);

        toSave.add(ca);
    }

    @Scheduled(fixedDelay = 60000)
    public synchronized void sync() {
        if (!toSave.isEmpty()) {
            clientsActivitiesRepository.save(toSave);
            toSave.clear();
        }
    }

}
