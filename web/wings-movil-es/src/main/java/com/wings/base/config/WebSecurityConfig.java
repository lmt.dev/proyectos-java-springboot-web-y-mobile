/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.config;

import com.wings.base.auth.WingsAutheticatorProvider;
import com.wings.base.auth.WingsUsersDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private WingsUsersDetailsService wingsUsersDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/css/**/*", "/js/**/*", "/bower_components/**/*", "/fonts/**/*", "/img/**/*", "/images/**/*").permitAll()
                .antMatchers(HttpMethod.POST,
                        "/api/payments/callback/**/*", "/api/services/**/*"
                ).permitAll()
                .antMatchers("/pagos/autorizacion/**/*", "/pagos/factura/**/*","/pagos/facturas/**/*", "/invoices/**/*", "/p/**/*"
                ).permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll().and()
                .exceptionHandling().authenticationEntryPoint(new AjaxAwareAuthenticationEntryPoint("/login"));

        http.cors().configure(http);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(new WingsAutheticatorProvider(wingsUsersDetailsService));
//                .inMemoryAuthentication()
//                .withUser("user").password("test").roles("USER")
//                .and()
//                .withUser("admin").password("admin").roles("ADMIN","ACTUATOR");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("css/**/*", "js/**/*", "/images/**", "/img/**", "/bower_components/**", "/fonts/**/*");
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(
                        "*",
                        "http://sebatest.mooo.com",
                        "http://sebatest2.mooo.com",
                        "http://sebatest3.mooo.com",
                        "http://localhost:8090",
                        "http://wa.uppertel.com",
                        "https://wa.uppertel.com",
                        "http://wingsmobile.net",
                        "http://www.wingsmobile.net",
                        "https://wingsmobile.net",
                        "https://www.wingsmobile.net",
                        "http://beta.wingsmobile.net",
                        "https://beta.wingsmobile.net"
                );
            }
        };
    }
}
