/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.repos;

import com.wings.base.domain.client.ClientDebtsReminder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ClientDebtsReminderRepository extends JpaRepository<ClientDebtsReminder, Long>, JpaSpecificationExecutor<ClientDebtsReminder> {

    public ClientDebtsReminder findFirstByToken(String token);

    public ClientDebtsReminder findFirstByClientId(Long clientId);

    public List<ClientDebtsReminder> findByCounterGreaterThan(Integer counter);

    public List<ClientDebtsReminder> findFirst20ByCounterGreaterThan(Integer counter);

    public List<ClientDebtsReminder> findFirst20ByCounterGreaterThanOrderByBalanceDesc(Integer counter);

    public Integer deleteByClientIdNotIn(List<Long> clientIds);

}
