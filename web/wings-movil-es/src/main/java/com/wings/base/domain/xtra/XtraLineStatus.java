/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.domain.xtra;

/**
 *
 * @author seba
 */
public class XtraLineStatus {

    private String msisdn;
    private String refCustomerId;
    private String activateDate;
    private String status;
    private String statusDate;
    private String tariffId;
    private String productId;
    private String iccid;
    private String puk;
    private String typeIccid;
    private String productProfile;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getRefCustomerId() {
        return refCustomerId;
    }

    public void setRefCustomerId(String refCustomerId) {
        this.refCustomerId = refCustomerId;
    }

    public String getActivateDate() {
        return activateDate;
    }

    public void setActivateDate(String activateDate) {
        this.activateDate = activateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getPuk() {
        return puk;
    }

    public void setPuk(String puk) {
        this.puk = puk;
    }

    public String getTypeIccid() {
        return typeIccid;
    }

    public void setTypeIccid(String typeIccid) {
        this.typeIccid = typeIccid;
    }

    public String getProductProfile() {
        return productProfile;
    }

    public void setProductProfile(String productProfile) {
        this.productProfile = productProfile;
    }
    
    
}
