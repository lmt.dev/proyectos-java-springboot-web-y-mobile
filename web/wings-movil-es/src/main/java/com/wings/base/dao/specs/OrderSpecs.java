/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.Order;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class OrderSpecs {

    public static Specification<Order> orderSearch(String q, String status) {

        return (Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            
            Join<Order,Client> client = root.join("client");

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("telefono"), search),
                                builder.like(root.get("trackingNumber"), search),
                                builder.like(root.get("orderStatus"), search),
                                builder.like(client.get("name"), search),
                                builder.like(client.get("lastname"), search),
                                builder.like(client.get("email"), search),
                                builder.like(client.get("idValue"), search),
                                builder.like(
                                        builder.concat(
                                                builder.concat(client.get("name"), " "),
                                                client.get("lastname")),
                                         search)
                        )
                );
                
                try {
                    Long id = Long.parseLong(q);
                    predicates.add(builder.or(builder.equal(root.get("id"), id)));
                } catch (Exception e) {
                }
            }
            
            if (status != null) {
                String[] statuses = status.split(",");
                Expression<String> exp = root.get("status");
                predicates.add(exp.in(Arrays.asList(statuses)));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
