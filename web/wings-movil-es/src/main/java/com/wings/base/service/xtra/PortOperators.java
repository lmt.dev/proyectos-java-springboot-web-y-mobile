/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class PortOperators {

    private final Logger log = LoggerFactory.getLogger(PortOperators.class);

    public String getOperator(String phone) {
        String ret = null;
        if (phone != null && !phone.isEmpty() && phone.length() == 9) {
            try {
                RestTemplate rest = new RestTemplate();
                String url = "https://apiservices.masmovil.es/basket-service/api/basket/shoppingcart/operator/mobile/" + phone;
                ResponseEntity<OperatorResponse> resp = rest.getForEntity(url, OperatorResponse.class);
                ret = resp.getBody().getIdOperador();
            } catch (Exception e) {
                log.info("Error en el operador para " + phone, e);
            }
        }
        return ret;
    }

}

class OperatorResponse {

    private String operador;
    private String idOperador;

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(String idOperador) {
        this.idOperador = idOperador;
    }

}
