/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.dao.BalanceDAO;
import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.payment.PaymentRequest;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.NumberUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author seba
 */
@Controller
public class BalanceController {

    private final Logger log = LoggerFactory.getLogger(BalanceController.class);

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceDAO balanceDAO;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @RequestMapping(value = {"views/clients/balances"}, method = RequestMethod.GET)
    public String getBalances(Model model, HttpServletRequest request) {

        //TODO: con esto obtenemos el usuario logueado
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");

        String client = request.getParameter("client");
        String line = request.getParameter("line");
        String month = request.getParameter("month");
        String year = request.getParameter("year");

        int page = 0;
        int size = 20;
        Integer imonth = null;
        Integer iyear = null;

        if (ssize == null) {
            ssize = "20";
        }
        if (spage == null) {
            spage = "0";
        }
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }
        try {
            imonth = Integer.parseInt(month);
        } catch (NumberFormatException e) {
        }
        try {
            iyear = Integer.parseInt(year);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));
        Page<Balance> items = balanceRepository.findAll(BalanceSpecs.balanceSearch(client, line, month, year), pr);
        PageWrapper<Balance> pageWrapper = new PageWrapper<>(items, "views/clients/balances");

        //summarized
        model.addAttribute("total", balanceDAO.find(client, line, imonth, iyear));

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);
        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("client", client);
        model.addAttribute("line", line);
        model.addAttribute("month", month);
        model.addAttribute("year", year);

        return "views/clients/balances";
    }

    @RequestMapping(value = {"api/clients/invoice/balance-details/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getInvoiceBalanceDetails(HttpServletRequest request, @PathVariable("id") Long invoiceId) {

        Invoice invoice = invoiceRepository.findOne(invoiceId);
        if (invoice == null) {
            return ResponseEntity.badRequest().body("No se encontro la factura.");
        }

        StringBuilder ret = new StringBuilder();
        ret.append("<pre>");

        ret.append("<b>Detalle del balance para la factura:")
                .append(invoice.getInvoiceNumber())
                .append(" (")
                .append(invoice.getId())
                .append(") ")
                .append("</b>")
                .append(System.lineSeparator())
                .append(System.lineSeparator());

        double total = 0.0;
        //detalle de la factura
        List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(invoice.getClientId().toString(), null, String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())));
        for (Balance balance : balances) {
            total += balance.getAmount();
            ret.append("Linea: ").append(balance.getLineId())
                    .append("\t Monto:").append(balance.getAmount())
                    .append("\t Desc:").append(balance.getDescription())
                    .append(System.lineSeparator());
        }
        ret.append(System.lineSeparator());
        ret.append("Total este mes:\t<b>").append(NumberUtils.round(total, 2)).append(" &#8364;</b>");

        ret.append("</pre>");

        return ResponseEntity.ok(ret.toString());

    }

    @RequestMapping(value = {"views/clients/balances-short/{id}"}, method = RequestMethod.GET)
    public String getBalancesShort(Model model, HttpServletRequest request, @PathVariable("id") Long clientId) {

        String client = request.getParameter("client");
        List<Balance> items = balanceRepository.findAll(BalanceSpecs.balanceSearch(client, null, null, null));
        //summarized
        model.addAttribute("total", balanceDAO.find(client, null, null, null));
        model.addAttribute("items", items);

        return "views/clients/balances-short";
    }

}
