/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.enums;

/**
 *
 * @author seba
 */
public enum InvoiceStatus {

    created,
    sended,
    payed,
    unpayed,
    review,
    payment_error,
    partial,
    denied,
    refunded,
    refunded_partial

}
