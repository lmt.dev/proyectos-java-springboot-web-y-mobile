/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class DniUtil {

    private final static Map<Integer, String> dnis = new HashMap<>();

    static {
        dnis.put(1, "NIF");
        dnis.put(2, "NIE");
        dnis.put(3, "PAS");
        dnis.put(4, "CIF");
    }

    public static Map<Integer, String> getDnis() {
        return dnis;
    }

    public static String getDniString(int value) {
        return dnis.get(value);
    }

    public static String getDniString(String value) {

        String ret = "";
        try {
            ret = dnis.get(Integer.valueOf(value));
        } catch (Exception e) {
        }

        return ret;
    }

}
