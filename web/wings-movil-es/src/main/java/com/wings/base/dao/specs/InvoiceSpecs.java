/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao.specs;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.invoice.Invoice;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class InvoiceSpecs {

    public static Specification<Invoice> invoiceSearchForPayments(int month, int year) {

        return (Root<Invoice> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("month"), month));
            predicates.add(builder.equal(root.get("year"), year));
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    public static Specification<Invoice> invoiceSearch(String q, String status, Integer month, Integer year, Boolean sended) {

        return (Root<Invoice> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            Join<Invoice, Client> client = root.join("client");

            List<Predicate> predicates = new ArrayList<>();
            List<Predicate> orPredicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("invoiceNumber"), search),
                                builder.like(root.get("ecode"), search),
                                builder.like(client.get("name"), search),
                                builder.like(client.get("lastname"), search),
                                builder.like(client.get("email"), search),
                                builder.like(client.get("idValue"), search),
                                builder.like(builder.concat(builder.concat(client.get("name"), " "), client.get("lastname")), search)
                        )
                );
            }

            if (status != null) {
                predicates.add(builder.equal(root.get("status"), status));
            }

            if (month != null) {
                predicates.add(builder.equal(root.get("month"), month));
            }

            if (year != null) {
                predicates.add(builder.equal(root.get("year"), year));
            }

            if (sended != null) {
                predicates.add(builder.equal(root.get("sended"), sended));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
