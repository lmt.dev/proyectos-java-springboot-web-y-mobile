/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.utils.MapUtils;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class CompleteProcess {

    private final Logger log = LoggerFactory.getLogger(CompleteProcess.class);

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationCompleteRepository registrationCompleteRepository;

    @Scheduled(initialDelay = 15000l,fixedRate = 900000)
    public void test() {
        List<Registration> regs = registrationRepository.findByStatus("payment_done");
        for (Registration reg : regs) {
            RegistrationComplete temp = registrationCompleteRepository.findOne(reg.getId());
            if(temp != null){
                continue;
            }            
            RegistrationComplete rec = MapUtils.map(reg);
            rec.setId(reg.getId());            
            registrationCompleteRepository.save(rec);            
        }
    }

}
