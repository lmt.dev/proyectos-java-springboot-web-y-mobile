/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class EnvironmentService {

    @Autowired
    private Environment environment;

    public boolean isDevEnvironment() {
        boolean ret = false;
        String[] profiles = environment.getActiveProfiles();
        for (String profile : profiles) {
            if (profile.contains("dev") || profile.contains("default")) {
                ret = true;
                break;
            }
        }
        return ret;
    }

}
