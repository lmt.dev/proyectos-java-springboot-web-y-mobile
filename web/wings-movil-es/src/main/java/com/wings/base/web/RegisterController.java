/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.ClientLineHolder;
import com.wings.base.domain.registration.PortLine;
import com.wings.base.domain.registration.Registration;
import com.wings.base.domain.registration.RegistrationComplete;
import com.wings.base.enums.RegistrationStatus;
import com.wings.base.repos.ClientLineHoldersRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.utils.PageWrapper;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wings.base.repos.PortLinesRepository;
import com.wings.base.repos.RegistrationCompleteRepository;
import com.wings.base.utils.DniUtil;
import com.wings.base.utils.MapUtils;
import com.wings.base.utils.UrlUtils;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class RegisterController {

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationCompleteRepository completeRepository;

    @Autowired
    private PortLinesRepository portLinesRepository;

    @Autowired
    private LinesRepository linesRepository;
    
    @Autowired
    private ClientLineHoldersRepository clientLineHoldersRepository;

    @RequestMapping(value = {"views/registration/register"}, method = RequestMethod.GET)
    public String getRegistrarions(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        Page<Registration> items;
        if (q != null && !q.isEmpty()) {

            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }

            items = registrationRepository.findByQuery(q, new PageRequest(page, size));
            model.addAttribute("q", q);
        } else if (status != null && !status.isEmpty()) {
            q = "";
            items = registrationRepository.findByQueryWithStatus(q, status.split(","), new PageRequest(page, size));
        } else {
            items = registrationRepository.findAll(new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id")));
        }
        PageWrapper<Registration> pageWrapper = new PageWrapper<>(items, "views/registration/register");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("page", pageWrapper);

        return "views/registration/register";
    }

    @RequestMapping(value = {"views/registration/register/{id}/form"}, method = RequestMethod.GET)
    public String getClientForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        Registration reg = registrationRepository.findOne(id);
        model.addAttribute("title", "Cliente");
        model.addAttribute("subTitle", reg.toString());
        model.addAttribute("elements", MapUtils.getFormElements(reg));

        return "views/common/view-form";
    }

    @RequestMapping(value = {"views/registration/complete"}, method = RequestMethod.GET)
    public String getComplete(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        Page<RegistrationComplete> items;
        if (q != null && !q.isEmpty()) {

            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }

            items = completeRepository.findByQuery(q, new PageRequest(page, size));
            model.addAttribute("q", q);
        } else {
            items = completeRepository.findAll(new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id")));
        }
        PageWrapper<RegistrationComplete> pageWrapper = new PageWrapper<>(items, "views/registration/complete");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("page", pageWrapper);

        return "views/registration/complete";
    }

    @RequestMapping(value = {"views/registration/complete/{id}/form"}, method = RequestMethod.GET)
    public String getCompleteForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        RegistrationComplete reg = completeRepository.findOne(id);
        model.addAttribute("title", "Cliente");
        model.addAttribute("subTitle", reg.toString());
        model.addAttribute("elements", MapUtils.getFormElements(reg));

        return "views/common/view-form";
    }

    @RequestMapping(value = {"views/registration/lines"}, method = RequestMethod.GET)
    public String getLines(Model model, HttpServletRequest request) {
        //@RequestParam("chk-dat") String dat, @RequestParam("chk-voz") String voz

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        page = page == 0 ? page : page - 1;

        Page<PortLine> items;
        if (q != null && !q.isEmpty()) {

            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }

            items = portLinesRepository.findByQuery(q, new PageRequest(page, size));
            model.addAttribute("q", q);
            model.addAttribute("qq", "&q=" + q);
        } else {
            items = portLinesRepository.findAll(new PageRequest(page, size));
            model.addAttribute("qq", "");
        }
        PageWrapper<PortLine> pageWrapper = new PageWrapper<>(items, "views/registration/lines");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("page", pageWrapper);

        return "views/registration/lines";
    }

    @RequestMapping(value = {"views/registration/lines/{id}/form"}, method = RequestMethod.GET)
    public String getLineForm(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        PortLine line = portLinesRepository.findOne(id);
        model.addAttribute("title", "Linea");
        model.addAttribute("subTitle", line.toString());
        model.addAttribute("elements", MapUtils.getFormElements(line));

        return "views/common/view-form";
    }

    @ResponseBody
    @RequestMapping(value = {"/api/registration/create-holder"}, method = RequestMethod.POST)
    public ResponseEntity<String> createLineHolder(Model model, HttpServletRequest request, @RequestParam("id") Long id) {

        RegistrationComplete reg = completeRepository.findOne(id);
        if (reg == null) {
            return ResponseEntity.badRequest().body("No se encontró la registración");
        }

        Long number = null;
        try {
            number = Long.parseLong(reg.getNroPorta());
        } catch (Exception e) {
        }

        if (number == null) {
            return ResponseEntity.badRequest().body("No se encontro la linea");
        }

        ClientLine line = linesRepository.findByNumber(number);
        if (line == null) {
            return ResponseEntity.badRequest().body("No se encontro la linea");
        }

        if(line.getClient().getIdValue().equalsIgnoreCase(reg.getNroDoc())){
            return ResponseEntity.badRequest().body("El cliente tiene el mismo dni que el titular a agregar");
        }
        
        
        ClientLineHolder client = new ClientLineHolder();

        client.setName(WordUtils.capitalizeFully(reg.getNombre()));
        client.setLastname(WordUtils.capitalizeFully(reg.getApellido()));
        client.setMobile(reg.getTelefono());
        client.setPhone(reg.getTelefono());
        client.setIdType(DniUtil.getDniString(reg.getTipoDoc()));
        client.setIdValue(StringUtils.upperCase(reg.getNroDoc()));
        client.setBirthDate(reg.getFechaNac());

        client.setAddress(WordUtils.capitalizeFully(reg.getDireccion()));
        client.setCity(WordUtils.capitalizeFully(reg.getCiudad(), new char[]{'´', '\''}));
        client.setState(WordUtils.capitalizeFully(reg.getProvincia()));
        client.setCountry(WordUtils.capitalizeFully(reg.getPais()));
        client.setNationality(WordUtils.capitalizeFully(reg.getNacionalidad()));
        client.setCreationDate(new Date());
        client.setEmail(reg.getEmail());
        client.setPostalCode(reg.getCp());

        client.setCompanyName(WordUtils.capitalizeFully(reg.getRazonSocial()));
        client.setCompanyIdType(DniUtil.getDniString(reg.getTipoDocEmpresa()));
        client.setCompanyIdValue(StringUtils.upperCase(reg.getNroDocEmpresa()));

        client.setLineId(line.getId());
        client.setClientId(line.getClientId());
        

        clientLineHoldersRepository.save(client);

        return ResponseEntity.ok("El titluar se creó correctamente");
    }
    
    @ResponseBody
    @RequestMapping(value = {"/api/registration/mark-verified/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> createMarkVerified(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        RegistrationComplete reg = completeRepository.findOne(id);
        if (reg == null) {
            return ResponseEntity.badRequest().body("No se encontró la registración");
        }

        reg.setStatus(RegistrationStatus.verified.name());
        completeRepository.save(reg);
        return ResponseEntity.ok("La registracion se marco como revisada");
    }

}
