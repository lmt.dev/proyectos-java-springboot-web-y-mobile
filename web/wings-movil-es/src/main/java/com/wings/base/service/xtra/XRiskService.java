/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service.xtra;

import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.xtra.UrlLocator;
import com.wings.xtra.risk.find.CableMsisdnsRisk;
import com.wings.xtra.risk.find.CableMsisdnsRiskPort;
import com.wings.xtra.risk.find.TestCreateObjectFactory;
import static com.wings.xtra.risk.find.TestCreateObjectFactory.sdf;
import com.wings.xtra.risk.find.request.Activate;
import com.wings.xtra.risk.find.request.Instruction;
import com.wings.xtra.risk.find.request.MsisdnsRiskMaintenanceRequest;
import com.wings.xtra.risk.find.request.Operation;
import com.wings.xtra.risk.find.request.SoapRequest;
import com.wings.xtra.risk.find.response.MsisdnsRiskMaintenanceRequestResponse;
import com.wings.xtra.risk.update.request.LineDetails;
import com.wings.xtra.risk.update.request.RiskServices;
import java.net.URL;
import java.util.Date;

/**
 *
 * @author seba
 */
public class XRiskService {

    private final String env;

    public static final String resId = "F33563";
    public static final String resPin = "3DlIbN8z3rFQv";

    public XRiskService(String env) {
        this.env = env;
    }

    public void getRisk(ClientLine line) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getRiskURL();

        CableMsisdnsRisk service = new CableMsisdnsRisk(url);
        CableMsisdnsRiskPort port = service.getCableMsisdnsRiskSoapService();

        // TODO initialize WS operation arguments here
        MsisdnsRiskMaintenanceRequest part1 = new MsisdnsRiskMaintenanceRequest();

        SoapRequest request = new SoapRequest();
        Operation operation = new Operation();

        Instruction i = new Instruction();
        Date date = new Date();

        i.setOperationType("GETRISK");
        i.setRefCustomerId(line.getClient().getRefCustomerId());
        i.setResellerId(resId);
        i.setResellerPin(resPin);
        i.setTimeStamp(sdf.format(date));
        i.setTransactionId(String.valueOf(date.getTime()));

        operation.setInstruction(i);

        Activate activate = new Activate();
        com.wings.xtra.risk.find.request.LineDetails l = new com.wings.xtra.risk.find.request.LineDetails();
        l.setMsisdn(line.getNumber().toString());
        activate.setLineDetails(l);

        //com.wings.xtra.risk.find.request.RiskServices r = new com.wings.xtra.risk.find.request.RiskServices();
        //r.setRiskAction("A");        
        //activate.setRiskServices(TestCreateObjectFactory.getRiskServices());
        operation.setActivate(activate);
        request.setOperation(operation);
        part1.setSoapRequest(request);
        // TODO process result here
        MsisdnsRiskMaintenanceRequestResponse result = port.msisdnsRiskMaintenance(part1);

        if (result.getReturn().getActivationCode().equals("OK-001")) {

            try {
                MsisdnsRiskMaintenanceRequestResponse.Return.RiskDetails.Risk r = result.getReturn().getRiskDetails().getRisk();

                line.setRiskMargin(Float.parseFloat(r.getRiesgoMargen()));
                line.setRiskExtension(Float.parseFloat(r.getAmpliacion()));
                line.setRiskMonthPrice(Float.parseFloat(r.getPrecio()));
                line.setRiskAutoBlock(r.getTipoAutoBloqueo().equals("1"));
                line.setRiskAutoUnblock(r.getTipoAutoDesbloqueo().equals("1"));

            } catch (Exception e) {
                
            }            
        }
    }

    /**
     *
     * Acción: A Solicitud Ampliacion de Riesgo Mes en Curso M Solicitud Cambio
     * Riesgo Mensual(riskAmount) L Solicitud bloqueo Telefono si supera el
     * riesgo mensual U Solicitud de NO bloqueo si el telefono supera riesgo
     * mensual H Solicitud Desbloqueo automatico el dia 1 K Solicitud de no
     * desbloquear automaticamente el telefono el dia 1
     *
     * @param refCustomerId
     * @param line
     * @param action
     * @param amount
     */
    public void updateRisk(String refCustomerId, String line, String action, String amount) {

        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        UrlLocator locator = new UrlLocator(env);
        URL url = locator.getRiskUpdateURL();

        com.wings.xtra.risk.update.CableMsisdnsRisk service = new com.wings.xtra.risk.update.CableMsisdnsRisk(url);
        com.wings.xtra.risk.update.CableMsisdnsRiskPort port = service.getCableMsisdnsRiskSoapService();

        try {

            // TODO initialize WS operation arguments here
            com.wings.xtra.risk.update.request.MsisdnsRiskMaintenanceRequest part1 = new com.wings.xtra.risk.update.request.MsisdnsRiskMaintenanceRequest();

            com.wings.xtra.risk.update.request.SoapRequest request = new com.wings.xtra.risk.update.request.SoapRequest();
            com.wings.xtra.risk.update.request.Operation operation = new com.wings.xtra.risk.update.request.Operation();

            com.wings.xtra.risk.update.request.Instruction i = new com.wings.xtra.risk.update.request.Instruction();
            Date date = new Date();
            i.setOperationType("MODRISK");
            i.setRefCustomerId(refCustomerId);
            //identificacion
            i.setResellerId(resId);
            i.setResellerPin(resPin);
            //variables
            i.setTimeStamp(sdf.format(date));
            i.setTransactionId(String.valueOf(date.getTime()));

            operation.setInstruction(i);

            com.wings.xtra.risk.update.request.Activate activate = new com.wings.xtra.risk.update.request.Activate();

            LineDetails l = new LineDetails();
            l.setMsisdn(line);

            activate.setLineDetails(l);

            RiskServices r = new RiskServices();

            /*
            Acción:
            A Solicitud Ampliacion de Riesgo Mes en Curso
            M Solicitud Cambio Riesgo Mensual(riskAmount)
            L Solicitud bloqueo Telefono si supera el riesgo mensual
            U Solicitud de NO bloqueo si el telefono supera riesgo mensual
            H Solicitud Desbloqueo automatico el dia 1
            K Solicitud de no desbloquear automaticamente el telefono el dia 1
             */
            r.setRiskAction(action);
            r.setRiskAmount(amount);

            activate.setRiskServices(r);

            operation.setActivate(activate);
            request.setOperation(operation);
            part1.setSoapRequest(request);
            // TODO process result here
            com.wings.xtra.risk.update.response.MsisdnsRiskMaintenanceRequestResponse result = port.msisdnsRiskMaintenance(part1);

            System.out.println("Result = " + result.getReturn().getActivateDescription());
            System.out.println("Result = " + result.getReturn().getTransactionId());
            System.out.println("Result = " + result.getReturn().getActivationCode());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
