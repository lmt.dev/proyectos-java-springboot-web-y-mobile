/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.process;

import com.wings.base.domain.admin.ProcessLog;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.client.Order;
import com.wings.base.domain.client.OrderActivity;
import com.wings.base.enums.LineStatus;
import com.wings.base.enums.OrderStatus;
import com.wings.base.enums.TicketType;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrderActivityRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.TicketsRepository;
import com.wings.base.service.ProcessLogService;
import com.wings.base.service.UpsService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class OrderProcess {

    private final Logger log = LoggerFactory.getLogger(OrderProcess.class);

    @Autowired
    private UpsService upsService;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private OrderActivityRepository orderActivityRepository;

    @Autowired
    private ProcessLogService processLogService;

    @Autowired
    private TicketsRepository ticketsRepository;

    private final String launcher = "system";

    /**
     * Proceso cada 3 horas para ver el estado de los pedidos enviados a UPS,
     * descarga el archivo del ftp y actualiza las ordenes
     */
    //@Scheduled(initialDelay = 2160000l, fixedDelay = 10800000l)
    public void checkUpsStatus() {
        checkUpsStatus(launcher);
    }

    public void checkUpsStatus(String launcher) {

        ProcessLog pl = processLogService.createProcess(launcher, "Estado pedidos UPS");
        pl.appendLine("Descargando... ");
        List<File> downloaded = upsService.downloadOrders();
        pl.appendLine("Pedidos encontrados " + downloaded.size());
        int procesados = 0;
        if (downloaded.isEmpty()) {
            pl.stop();
            return;
        }

        List<Long> nulledIds = new ArrayList<>();
        for (File file : downloaded) {
            try {
                BufferedReader bf = new BufferedReader(new FileReader(file));
                String strLine;
                while ((strLine = bf.readLine()) != null) {
                    if (strLine.startsWith("Anulado")) {
                        continue;
                    }

                    if (strLine.trim().isEmpty()) {
                        continue;
                    }

                    //Anulado,Tracking,pedido,ICC,Tipo,Destinatario,Telefono
                    String[] array = strLine.split(",");

                    Long orderId = null;
                    try {
                        orderId = Long.valueOf(array[2].replaceAll("\"", ""));
                    } catch (Exception e) {
                    }

                    if (orderId == null) {
                        continue;
                    }

                    Order o = ordersRepository.findOne(orderId);
                    if (o == null) {
                        log.error("Error Importing orders, order not found for " + array[2]);
                        continue;
                    }

                    if (array[0].replaceAll("\"", "").equals("Y")) {
                        o.setAnulada(true);
                        nulledIds.add(o.getId());
                    } else {
                        o.setAnulada(false);
                    }

                    o.setTrackingNumber(array[1].replaceAll("\"", ""));

                    ClientLine line = null;
                    if (o.getLineId() != null) {
                        line = linesRepository.findOne(o.getLineId());
                    }

                    if (line == null) {
                        line = linesRepository.findByNumber(Long.parseLong(o.getTelefono()));
                    }

                    if (line == null) {
                        log.error("Error Importing orders, line not found for " + array[2] + " - " + array[3]);
                        continue;
                    }

                    if (line.getInternalState().equals(LineStatus.order_sended.name())) {
                        line.setIccid(array[3].replaceAll("\"", ""));
                        line.setInternalState(LineStatus.order_received.name());
                        o.setStatus(OrderStatus.received.name());
                        procesados++;
                    }

                    //if order is a replacement, line is not affected
                    //mark order as replacement received
                    if (o.getStatus().equals(OrderStatus.replacement_sended.name())) {
                        o.setStatus(OrderStatus.replacement_received.name());
                        line.setIccid(array[3].replaceAll("\"", ""));
                        //aca tengo que solicitar el cambio de iccid
                        //una vez este en replacement delivered
                    }

                    ordersRepository.save(o);
                    linesRepository.save(line);
                }

            } catch (IOException | NumberFormatException e) {
                log.error("Error downloading csv", e);
            }
        }

        //ahora repaso las anuladas y pongo el mismo tracking id
        //de la que no esta anulada
        for (Long nulledId : nulledIds) {
            Order nulledOrder = ordersRepository.findOne(nulledId);
            Long parentId = nulledOrder.getParent();

            List<Order> grouped = ordersRepository.findByParent(parentId);
            //get the not null tracking number
            String tracking = null;
            for (Order order : grouped) {
                if (!order.isAnulada()) {
                    tracking = order.getTrackingNumber();
                    break;
                }
            }

            //set same tracking number to all orders
            if (tracking != null) {
                for (Order order : grouped) {
                    order.setTrackingNumber(tracking);
                }
                ordersRepository.save(grouped);
            }
        }

        pl.appendLine("Pedidos procesados " + procesados);
        pl.stop();
    }

    /**
     * Proceso que se ejecuta cada 4 horas y verifica el estado de la orden con
     * el tracking code
     */
    //@Scheduled(initialDelay = 3240000l, fixedDelay = 14400000l)
    public void checkUpsDeliveryStatus() {
        checkUpsDeliveryStatus(launcher, null);
    }

    public void checkUpsDeliveryStatus(String launcher, List<Order> orders) {

        ProcessLog pl = processLogService.createProcess(launcher, "Estado envios UPS");

        if (orders == null) {
            orders = ordersRepository.findByStatus(OrderStatus.received.name());
            orders.addAll(ordersRepository.findByStatus(OrderStatus.replacement_received.name()));
        }

        pl.appendLine("Ordenes a analizar " + orders.size());
        orders.forEach((order) -> {

            List<OrderActivity> activities = upsService.getTrackingActivity(order.getTrackingNumber());
            activities.forEach((activity) -> {
                try {
                    activity.setOrderId(order.getId());
                    orderActivityRepository.save(activity);
                } catch (DataIntegrityViolationException e) {
                }
            });

            //set last activity to order
            boolean error = false;
            if (!activities.isEmpty()) {
                Collections.sort(activities, (OrderActivity o1, OrderActivity o2) -> o1.getActivityDate().compareTo(o2.getActivityDate()));
                OrderActivity last = activities.get(activities.size() - 1);
                order.setOrderStatus(last.getDescription());

                for (OrderActivity activity : activities) {

                    //sometimes the last activity, it's not the delivered one
                    if (activity.getDescription() != null && (activity.getDescription().equals("Entregado") || activity.getDescription().contains("El cliente recogió el paquete"))
                            && (!activity.getDescription().contains("Entregado a UPS") && !activity.getDescription().contains("a que el cliente lo recoja"))) {
                        order.setOrderStatus(activity.getDescription());
                    }

                    if (activity.getPackageAddressDescription() != null
                            && !activity.getPackageAddressDescription().isEmpty()
                            && activity.getPackageAddressDescription().equalsIgnoreCase("ReturnTo Address")) {
                        error = true;
                        break;
                    }
                }
            }

            //set delivered status
            if (order.getOrderStatus() != null
                    && (order.getOrderStatus().contains("Entregado") || order.getOrderStatus().contains("El cliente recogió el paquete"))
                    && (!order.getOrderStatus().contains("Entregado a UPS") && !order.getOrderStatus().contains("a que el cliente lo recoja"))) {

                if (order.getStatus().equals(OrderStatus.received.name())) {

                    ClientLine line = linesRepository.findOne(order.getLineId());
                    if (error) {
                        order.setOrderStatus("Devuelto al remitente");
                        order.setStatus(OrderStatus.deliver_error.name());
                        line.setInternalState(LineStatus.order_deliver_error.name());

                        Ticket ticket = new Ticket(TicketType.order.name(), order.getId(), "Error en la entrega de la SIM reeenviada", "system", "TECHLVL2");
                        ticketsRepository.save(ticket);

                    } else {
                        order.setStatus(OrderStatus.delivered.name());
                        line.setInternalState(LineStatus.order_delivered.name());
                    }
                    linesRepository.save(line);

                } else if (order.getStatus().equals(OrderStatus.replacement_received.name())) {
                    //do not affect line status

                    Ticket ticket;
                    if (error) {
                        order.setOrderStatus("Devuelto al remitente");
                        order.setStatus(OrderStatus.replacement_deliver_error.name());
                        ticket = new Ticket(TicketType.order.name(), order.getId(), "Error en la entrega de la SIM reeenviada", "system", "TECHLVL2");
                    } else {
                        order.setStatus(OrderStatus.replacement_delivered.name());
                        ticket = new Ticket(TicketType.order.name(), order.getId(), "El pedido de reemplazo a sido entregado", "system", "TECHLVL2");
                    }
                    ticketsRepository.save(ticket);
                }
            }
        });

        ordersRepository.save(orders);

        //look for parent
        //this is because we can have 2 or more orders grouped by address
        for (Order order : orders) {
            if (order.getParent() != null) {
                Order parent = ordersRepository.findOne(order.getParent());
                boolean delivered = parent.getStatus().equals(OrderStatus.delivered.name()) || parent.getStatus().equals(OrderStatus.replacement_delivered.name());

                if (delivered) {
                    if (order.getStatus().equals(OrderStatus.replacement_received.name())) {
                        order.setStatus(OrderStatus.replacement_delivered.name());

                        Ticket ticket = new Ticket(TicketType.order.name(), order.getId(), "El pedido de reemplazo a sido entregado", "system", "TECHLVL2");
                        ticketsRepository.save(ticket);

                    } else {
                        order.setStatus(OrderStatus.delivered.name());

                        ClientLine line = linesRepository.findOne(order.getLineId());
                        ClientLine parentLine = linesRepository.findOne(parent.getLineId());
                        line.setInternalState(parentLine.getInternalState());
                        linesRepository.save(line);

                    }
                }

                order.setOrderStatus(parent.getOrderStatus());
                ordersRepository.save(order);
            }
        }
        pl.appendLine("Ordenes a analizadas " + orders.size());
        pl.appendLine("Fin del proceso");
        pl.stop();
    }
}
