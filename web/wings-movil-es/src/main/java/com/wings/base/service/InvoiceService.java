/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.service;

import com.ibm.icu.text.SimpleDateFormat;
import com.wings.base.WingsProperties;
import com.wings.base.dao.specs.BalanceSpecs;
import com.wings.base.dao.specs.CdrSpecs;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Cdr;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.invoice.InvoiceBalanceWrapper;
import com.wings.base.domain.invoice.InvoiceCdrWrapper;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.CdrRepository;
import com.wings.base.utils.DateUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class InvoiceService {

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private CdrRepository cdrRepository;

    private final Logger log = LoggerFactory.getLogger(InvoiceService.class);

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");

    private final String storage;
    private final String jasper;
    private final String logo;

    public InvoiceService(WingsProperties wingsProperties) {
        this.jasper = wingsProperties.getInvoiceJasper();
        this.storage = wingsProperties.getInvoiceStorage();
        this.logo = wingsProperties.getLogoFull();
    }

    public String generateInvoicePDF(Invoice invoice, List<InvoiceBalanceWrapper> balances, List<InvoiceCdrWrapper> cdrs) {

        String ret = "";
        //check if directory exists
        String dirName = storage + invoice.getYear() + "-" + invoice.getMonth();
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String destName = dir.getAbsolutePath() + "/" + invoice.getInvoiceNumber() + ".pdf";
        Map<String, Object> map = new HashMap<>();

        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(invoice.getItems());
        JRBeanCollectionDataSource balDs = new JRBeanCollectionDataSource(balances);
        JRBeanCollectionDataSource cdrDs = new JRBeanCollectionDataSource(cdrs);

        JREmptyDataSource empty = new JREmptyDataSource();
        map.put(JRParameter.REPORT_LOCALE, new Locale("es", "ES"));
        map.put("invoice", invoice);
        map.put("detailds", ds);
        map.put("balDs", balDs);
        map.put("cdrDs", cdrDs);
        map.put("logo", logo);
        try {
            JasperReport jasReport = (JasperReport) JRLoader.loadObject(new File(jasper));
            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasReport, map, empty);
            JasperExportManager.exportReportToPdfFile(jprint, destName);
            ret = destName;
        } catch (Exception e) {
            log.error("Error creating invoice", e);
        }
        return ret;
    }

    /**
     * 
     * @param invoice
     * @return 
     */
    public String generateInvoicePdf(Invoice invoice) {

        Date from = DateUtils.getStartDateForPeriod(invoice.getMonth(), invoice.getYear());
        Date to = DateUtils.getEndDateForPeriod(invoice.getMonth(), invoice.getYear());

        //invoice.setInvoiceNumber(InvoiceUtils.getInvoiceNumber(invoice.getId()));
        Client c = invoice.getClient();
        Collection<ClientLine> lines = c.getLines();
        List<InvoiceBalanceWrapper> balancesWrappers = new ArrayList<>();
        List<InvoiceCdrWrapper> cdrWrappers = new ArrayList<>();

        for (ClientLine line : lines) {
            List<Balance> balances = balanceRepository.findAll(BalanceSpecs.balanceSearch(c.getId().toString(), line.getId().toString(), String.valueOf(invoice.getMonth()), String.valueOf(invoice.getYear())), new Sort(Sort.Direction.ASC, "id"));
            if (balances.isEmpty()) {
                continue;
            }

            Iterator<Balance> iterator = balances.iterator();
            while (iterator.hasNext()) {
                Balance next = iterator.next();
                if (next.getControl().contains("auto-payment-")) {
                    iterator.remove();
                }
            }

            balancesWrappers.add(new InvoiceBalanceWrapper(line.getNumber().toString(), balances));

            List<Cdr> cdrs = cdrRepository.findAll(CdrSpecs.cdrSearch(null, from, to, null, null, line.getNumber().toString()), new Sort(Sort.Direction.ASC, "date", "time"));
            cdrWrappers.add(new InvoiceCdrWrapper(line.getNumber().toString(), cdrs));
        }

        String invoicePath = generateInvoicePDF(invoice, balancesWrappers, cdrWrappers);

        return invoicePath;
    }

}
