/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.web;

import com.wings.base.WingsProperties;
import com.wings.base.dao.BalanceDAO;
import com.wings.base.dao.CdrDAO;
import com.wings.base.dao.specs.ClientSpecs;
import com.wings.base.dao.specs.MessageSpecs;
import com.wings.base.domain.Token;
import com.wings.base.domain.admin.Ticket;
import com.wings.base.domain.admin.User;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.BalanceSums;
import com.wings.base.domain.client.CdrSums;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientActivity;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.cs.Message;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.domain.registration.Registration;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.enums.LineStatus;
import com.wings.base.process.XtraProccess;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.ClientsActivitiesRepository;
import com.wings.base.repos.ClientsRepository;
import com.wings.base.repos.CsMessageRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import com.wings.base.repos.OrderActivityRepository;
import com.wings.base.repos.OrdersRepository;
import com.wings.base.repos.RegistrationRepository;
import com.wings.base.repos.TicketsRepository;

import com.wings.base.service.ClientsService;
import com.wings.base.service.TokensService;
import com.wings.base.service.xtra.XLineService;
import com.wings.base.service.xtra.XRiskService;
import com.wings.base.utils.DateUtils;
import com.wings.base.utils.FormatUtils;
import com.wings.base.utils.NumberUtils;
import com.wings.base.utils.PageWrapper;
import com.wings.base.utils.UrlUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class SupportController {

    private final Logger log = LoggerFactory.getLogger(SupportController.class);

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private CsMessageRepository csMessageRepository;

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private CdrDAO cdrDAO;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private TicketsRepository ticketsRepository;

    @Autowired
    private TokensService tokensService;

    @Autowired
    private XRiskService xRiskService;

    @Autowired
    private XLineService xLineService;

    @Autowired
    private XtraProccess xtraProccess;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceDAO balanceDAO;

    @Autowired
    private ClientsService clientsService;

    @Autowired
    private ClientsActivitiesRepository clientsActivitiesRepository;

    private final WingsProperties wingsProperties;

    public SupportController(WingsProperties wingsProperties) {
        this.wingsProperties = wingsProperties;
    }

    @RequestMapping(value = {"views/support/tickets"}, method = RequestMethod.GET)
    public String getTickets(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        Page<Ticket> items = ticketsRepository.findAll(pr);

        PageWrapper<Ticket> pageWrapper = new PageWrapper<>(items, "views/support/tickets");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);

        return "views/support/tickets";
    }

    @RequestMapping(value = {"views/support/clients"}, method = RequestMethod.GET)
    public String getRegistrarions(Model model, HttpServletRequest request) {

        String q = request.getParameter("q");
        String msg = request.getParameter("msg");

        List<Client> clients = null;
        Client client = null;
        Registration reg = null;
        ClientLine line = null;
        List<ClientLine> lines = null;
        Long number;
        String message = "Ingrese número completo, nombre, apellido, DNI o email para buscar";

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q.trim(), "UTF-8");
                message = "No se encontraron resultados";
                number = Long.parseLong(q);
            } catch (UnsupportedEncodingException | NumberFormatException e) {
                number = null;
            }

            if (number != null) {
                line = linesRepository.findByNumber(number);
                if (line != null) {
                    client = line.getClient();

                    lines = new LinkedList<>();
                    lines.add(line);

                    for (Object line1 : client.getLines()) {
                        if (!lines.contains((ClientLine) line1)) {
                            lines.add((ClientLine) line1);
                        }
                    }

                }
            } else {
                clients = clientsRepository.findAll(ClientSpecs.clientSearch(q, null));
                if (clients.size() == 1) {
                    client = clients.get(0);
                    clients = null;

                    lines = new LinkedList<>();
                    lines.addAll(client.getLines());

                } else {
                    message = "Seleccione un cliente para ver mas detalles";
                    if (clients.isEmpty()) {
                        message = "No se encontraron resultados";
                    }
                }
            }
        }

        if (client != null) {
            List<Message> messages = csMessageRepository.findByClientId(client.getId(), new Sort(Sort.Direction.DESC, "id"));
            model.addAttribute("messages", messages);
            Long regId = client.getRegistrationId();
            reg = registrationRepository.findOne(regId);

            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            int month = cal.get(Calendar.MONTH + 1);
            int year = cal.get(Calendar.YEAR);

            Date start = DateUtils.getStartDateForPeriod(month, year);
            Date end = DateUtils.getEndDateForPeriod(month, year);
            CdrSums sums = cdrDAO.find(client.getRefCustomerId(),
                    start,
                    end,
                    null,
                    null,
                    null);

            model.addAttribute("cdrSums", sums);

            for (ClientLine line1 : lines) {
                CdrSums sumLine = cdrDAO.find(client.getRefCustomerId(),
                        start,
                        end,
                        null,
                        null,
                        line1.getNumber() != null ? line1.getNumber().toString() : "--"
                );
                line1.setCdrSums(sumLine);

                //check if it's on ups, then show tracking status
                if (line1.getInternalState().equals(LineStatus.order_received.name())) {

                    //sacar la orden de la linea y el tracking de la orden
                    //line1.
                    //ordersRepository.findByRegistrationId(line1.getRegistrationId());                    
                    //orderActivityRepository.findByTrackingCode(message, sort)
                }

            }

            List<Invoice> lastInvoices = invoiceRepository.findByClientId(client.getId());
            boolean unpaid = false;
            StringBuilder unpaidText = new StringBuilder();
            for (Invoice lastInvoice : lastInvoices) {

                if (!lastInvoice.getStatus().equals(InvoiceStatus.payed.name())) {
                    unpaidText
                            .append("Factura impaga con vencimiento el ")
                            .append(lastInvoice.getExpireDate())
                            .append(" por un monto de ")
                            .append(lastInvoice.getTotalText())
                            .append("\n");
                }

                unpaid |= !lastInvoice.getStatus().equals(InvoiceStatus.payed.name());
            }

            model.addAttribute("unpaidInvoices", unpaid);
            model.addAttribute("unpaidText", unpaidText.toString());

            List<ClientActivity> activities = clientsActivitiesRepository.findByClientId(client.getId(), new Sort(Sort.Direction.DESC, "id"));
            model.addAttribute("activities", activities);

        }

        model.addAttribute("q", q);
        model.addAttribute("client", client);
        model.addAttribute("reg", reg);
        model.addAttribute("clients", clients);
        model.addAttribute("lines", lines);
        model.addAttribute("message", message);
        model.addAttribute("url", "views/support/clients");
        model.addAttribute("msg", msg);

        return "views/support/clients";
    }

    @ResponseBody
    @RequestMapping(value = {"api/support/messages/{clientId}"}, method = RequestMethod.GET)
    public ResponseEntity<List<Message>> getMessage(Model model, HttpServletRequest request, @PathVariable("clientId") Long clientId) {
        return ResponseEntity.ok(csMessageRepository.findByClientId(clientId, new Sort(Sort.Direction.DESC, "id")));
    }

    @ResponseBody
    @RequestMapping(value = {"api/support/messages"}, method = RequestMethod.POST)
    public ResponseEntity<Message> postMessage(Model model, HttpServletRequest request, @Valid Message message) {
        //TODO: validar el user con la session

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        message.setDate(new Date());
        message.setUser(((User) auth.getPrincipal()).getUsername());
        message.setTechNotif(message.getTechNotif() != null && message.getTechNotif());
        message.setLine(message.getLine().isEmpty() ? null : message.getLine());
        if (message.getIncidence()) {
            message.setIncidenceStatus("pending");
        }

        csMessageRepository.save(message);

        return ResponseEntity.ok(message);
    }

    @RequestMapping(value = {"views/support/client-invoices/{id}"}, method = RequestMethod.GET)
    public String getInvoices(Model model, HttpServletRequest request, @Valid Message message, @PathVariable("id") Long clientId) {

        List<Invoice> invoices = invoiceRepository.findByClientId(clientId);
        Token token = tokensService.getTokenForClient(clientId);

        model.addAttribute("invoices", invoices);
        model.addAttribute("baseUrl", request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort());
        model.addAttribute("token", token.getToken());
        model.addAttribute("redirectUrl", wingsProperties.getPaymentAuthorizationURL());

        return "views/support/client-invoices";
    }

    @RequestMapping(value = {"views/support/payment-auth/{id}"}, method = RequestMethod.GET)
    public String generateLink(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) {

        Token token = tokensService.generateTokenForClient(clientsRepository.findOne(idClient));

        model.addAttribute("token", token.getToken());
        return "views/support/payment-auth";

    }

    @RequestMapping(value = {"views/support/risk/{id}"}, method = RequestMethod.GET)
    public String getRisk(Model model, HttpServletRequest request, @PathVariable("id") Long lineId) {

        ClientLine line = linesRepository.findOne(lineId);
        xRiskService.getRisk(line);
        linesRepository.save(line);

        String url = "views/support/risk/" + lineId;
        model.addAttribute("line", line);
        model.addAttribute("postUrl", url);

        return "views/support/risk";
    }

    @RequestMapping(value = {"views/support/risk/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> modRisk(Model model, HttpServletRequest request, @PathVariable("id") Long lineId) {

        try {

            ClientLine line = linesRepository.findOne(lineId);
            String month = request.getParameter("risk-month");
            String ext = request.getParameter("risk-ext");
            String block = request.getParameter("risk-block");
            String unblock = request.getParameter("risk-unblock");

            float margin = Float.valueOf(month);
            float extension = Float.valueOf(ext);
            boolean autoblock = block != null && block.equals("on");
            boolean autounblock = unblock != null && unblock.equals("on");

            //margen mensual
            if (line.getRiskMargin() != margin) {
                xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "M", String.valueOf(margin));
            }

            //extension
            if (line.getRiskExtension() != extension) {
                xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "A", String.valueOf(extension));
            }

            if (line.getRiskAutoBlock() != autoblock) {
                if (autoblock) {
                    xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "L", String.valueOf(margin));
                } else {
                    xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "U", String.valueOf(margin));
                }
            }

            if (line.getRiskAutoUnblock() != autounblock) {
                if (autounblock) {
                    xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "H", String.valueOf(margin));
                } else {
                    xRiskService.updateRisk(line.getClient().getRefCustomerId(), line.getNumber().toString(), "K", String.valueOf(margin));
                }
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(line.getClientId());
            message.setText("Modificacion de riesgo - Mensual:" + month + ", Ampliacion:" + ext + ", Bloqueo: " + autoblock + ", Desbloqueo:" + autounblock);
            message.setLine(line.getNumber().toString());
            message.setLineId(line.getId());
            csMessageRepository.save(message);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok("success");
    }

    @RequestMapping(value = {"views/support/status/{id}"}, method = RequestMethod.GET)
    public String getStatus(Model model, HttpServletRequest request, @PathVariable("id") Long lineId) {

        ClientLine line = linesRepository.findOne(lineId);
        String url = "views/support/status/" + lineId;
        model.addAttribute("line", line);
        model.addAttribute("postUrl", url);

        return "views/support/status";
    }

    @RequestMapping(value = {"views/support/status/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postStatus(Model model, HttpServletRequest request, @PathVariable("id") Long lineId) {

        Map<String, String> map = new HashMap<>();
        map.put("S", "Suspencion");
        map.put("J", "Call barring");
        map.put("F", "Fraude");
        map.put("H", "Hurto");
        map.put("R", "Reactivacion");

        String status = request.getParameter("status");
        if (!map.containsKey(status)) {
            return ResponseEntity.badRequest().body("Estado incorrecto " + status);
        }

        ClientLine line = linesRepository.findOne(lineId);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Message message = new Message();
        message.setDate(new Date());
        message.setUser(((User) auth.getPrincipal()).getUsername());
        message.setClientId(line.getClientId());
        message.setText("Cambio estado linea:" + map.get(status));
        message.setLine(line.getNumber().toString());
        message.setLineId(line.getId());
        csMessageRepository.save(message);

        String response = xLineService.setLineStatus(line, status);
        if (response.contains("OK")) {
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.badRequest().body(response);
        }

    }

    @RequestMapping(value = {"/views/support/line/xstatus/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> getLineStatus(Model model, HttpServletRequest request, @PathVariable("id") Long lineId) {
        try {
            ClientLine line = linesRepository.findOne(lineId);
            xtraProccess.checkLineStatus(line);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = {"views/support/add-payment/{id}"}, method = RequestMethod.GET)
    public String addPayment(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) throws Exception {

        Client c = clientsRepository.findOne(idClient);

        if (c == null) {
            throw new Exception("Client not found");
        }
        model.addAttribute("postUrl", "views/support/add-payment/" + idClient);
        model.addAttribute("client", c);

        return "views/support/add-payment";
    }

    @RequestMapping(value = {"views/support/add-payment/{id}"}, method = RequestMethod.POST)
    public String postClientform(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("id") Long clientId) {

        try {

            String topay = request.getParameter("topay");
            String paymentDate = request.getParameter("payment-date");
            String paymentType = request.getParameter("payment-type");
            String paymentComment = request.getParameter("payment-comment");

            if (topay.contains(",")) {
                topay = topay.replaceAll(",", ".");
            }

            Double amount = 0.0d;
            try {
                amount = Double.parseDouble(topay);
            } catch (Exception e) {
                throw new Exception("El monto no es correcto");
            }

            if (amount <= 0) {
                throw new Exception("El monto debe ser mayor a cero");
            }

            Date date = new Date();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                sdf.parse(paymentDate);
            } catch (Exception e) {
                throw new Exception("La fecha no es correcta");
            }

            double total = amount;
            List<Invoice> invoices = invoiceRepository.findByClientIdAndStatus(clientId, InvoiceStatus.unpayed.name());
            invoices.addAll(invoiceRepository.findByClientIdAndStatus(clientId, InvoiceStatus.partial.name()));
            invoices.addAll(invoiceRepository.findByClientIdAndStatus(clientId, InvoiceStatus.created.name()));

            Collections.sort(invoices, new Comparator<Invoice>() {
                @Override
                public int compare(Invoice o1, Invoice o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });

            Client client = clientsRepository.findOne(clientId);
            List<ClientLine> lines = new ArrayList<>(client.getLines());

            total = amount;
            for (Invoice invoice : invoices) {

                if (total <= 0) {
                    break;
                }

                int month = invoice.getMonth();
                int year = invoice.getYear();

                for (ClientLine line : lines) {

                    if (total <= 0) {
                        break;
                    }

                    BalanceSums sums = balanceDAO.find(client.getId().toString(), line.getId().toString(), month, year);
                    if (sums.getTotal() > 0) {

                        Balance balance = new Balance();
                        balance.setClientId(clientId);
                        balance.setControl("manual-payment-" + date.getTime() + "-" + line.getId() + month + year);
                        balance.setDate(date);
                        balance.setDescription("Pago manual mediante " + paymentType + " (pago total de " + amount + ")");
                        balance.setLineId(line.getId());
                        balance.setMonth(month);
                        balance.setObservation(paymentComment);
                        balance.setPaymentMethod(paymentType);
                        balance.setType(BalanceType.discount.name());
                        balance.setYear(year);

                        if (sums.getTotal() <= total) {
                            balance.setAmount(sums.getTotal() * -1);
                        } else {
                            balance.setAmount(total * -1);
                        }

                        total -= sums.getTotal();
                        balanceRepository.save(balance);
                    }
                }

                //checkeo el estado de la factura
                BalanceSums sums = balanceDAO.find(client.getId().toString(), null, month, year);
                if (sums.getTotal() == 0) {
                    invoice.setStatus(InvoiceStatus.payed.name());
                } else {
                    invoice.setStatus(InvoiceStatus.partial.name());
                }
                invoiceRepository.save(invoice);
            }

            //aca se deberia agregar saldo negativo, si sobro algo del pago
            if (total > 0) {

                Balance balance = new Balance();
                balance.setClientId(clientId);
                balance.setControl("balance-remainder-" + date.getTime());
                balance.setDate(date);
                balance.setDescription("Saldo a favor del cliente " + paymentType + " (pago total de " + amount + ")");
                balance.setLineId(-1l);
                balance.setMonth(-1);
                balance.setYear(DateUtils.getYearForDate(new Date()));
                balance.setObservation(paymentComment);
                balance.setPaymentMethod(paymentType);
                balance.setType(BalanceType.credit.name());
                balance.setAmount(total * -1);

                balanceRepository.save(balance);
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(clientId);
            message.setText("Pago ingresado por un monto de " + NumberUtils.formatMoney(amount) + " mediante " + paymentType + " " + paymentComment);
            csMessageRepository.save(message);

            redirectAttributes.addAttribute("id", clientId);
            redirectAttributes.addAttribute("msg", "Pago añadido");

        } catch (Exception e) {
            redirectAttributes.addAttribute("id", clientId);
            redirectAttributes.addAttribute("msg", e.getMessage() != null ? e.getMessage() : e.toString());
        }

        return "redirect:/#views/support/clients?msg={msg}&q=id:{id}";
    }

    @RequestMapping(value = {"views/support/block/{id}"}, method = RequestMethod.GET)
    public String blockClient(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) throws Exception {

        Client c = clientsRepository.findOne(idClient);
        if (c == null) {
            throw new Exception("Client not found");
        }

        model.addAttribute("postUrl", "views/support/block/" + idClient);
        model.addAttribute("client", c);

        return "views/support/block";
    }

    @RequestMapping(value = {"views/support/block/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> blockClientPost(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) throws Exception {

        Client c = clientsRepository.findOne(idClient);
        if (c == null) {
            throw new Exception("Client not found");
        }

        String ret;
        try {

            String plan = request.getParameter("plan");
            String riskUnblock = request.getParameter("risk-unblock");
            String status = request.getParameter("status");
            String comment = request.getParameter("comment");

            ret = clientsService.blockAdminClient(c, Long.valueOf(plan), status, riskUnblock == null);

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(idClient);
            message.setText("Bloqueo administrativo: " + comment + System.lineSeparator() + ret);
            csMessageRepository.save(message);

            c.setAdminBlocked(true);
            clientsRepository.save(c);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(ret);

    }

    @RequestMapping(value = {"views/support/unblock/{id}"}, method = RequestMethod.GET)
    public String unblockClient(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) throws Exception {

        Client c = clientsRepository.findOne(idClient);
        if (c == null) {
            throw new Exception("Client not found");
        }

        model.addAttribute("postUrl", "views/support/unblock/" + idClient);
        model.addAttribute("client", c);

        return "views/support/unblock";
    }

    @RequestMapping(value = {"views/support/unblock/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> unblockClientPost(Model model, HttpServletRequest request, @PathVariable("id") Long idClient) throws Exception {

        Client c = clientsRepository.findOne(idClient);
        if (c == null) {
            throw new Exception("Client not found");
        }

        String ret;
        try {

            String comment = request.getParameter("comment");
            ret = clientsService.unblockAdminClient(c);
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Message message = new Message();
            message.setDate(new Date());
            message.setUser(((User) auth.getPrincipal()).getUsername());
            message.setClientId(idClient);
            message.setText("Desbloqueo administrativo: " + comment + System.lineSeparator() + ret);
            csMessageRepository.save(message);

            c.setAdminBlocked(false);
            clientsRepository.save(c);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(ret);

    }

    @RequestMapping(value = {"views/support/incidences"}, method = RequestMethod.GET)
    public String getIncidences(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");
        String status = request.getParameter("status");

        if (status == null) {
            status = "pending";
        } else if (status.equals("all")) {
            status = null;
        }

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "date"));
        Page<Message> items = csMessageRepository.findAll(MessageSpecs.incidenceSearch(Boolean.TRUE, q, status), pr);

        PageWrapper<Message> pageWrapper = new PageWrapper<>(items, "views/support/incidences");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("status", status);
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);

        return "views/support/incidences";
    }

    @RequestMapping(value = {"views/support/incidence/{id}"}, method = RequestMethod.GET)
    public String getIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId) {

        Message message = csMessageRepository.findOne(incidenceId);
        model.addAttribute("postUrl", "views/support/incidence/" + incidenceId);
        model.addAttribute("msg", message);
        model.addAttribute("ref", request.getParameter("ref"));

        return "views/support/incidence-edit";
    }

    @RequestMapping(value = {"views/support/incidence/{id}"}, method = RequestMethod.POST)
    public String postIncidence(Model model, HttpServletRequest request, @PathVariable("id") Long incidenceId) {
        Message message = csMessageRepository.findOne(incidenceId);

        String ref = request.getParameter("ref");
        String comment = request.getParameter("incidence-comment");
        String status = request.getParameter("status");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = ((User) auth.getPrincipal()).getUsername();

        if (comment != null && !comment.trim().isEmpty()) {
            StringBuilder text = new StringBuilder(message.getText());
            Date today = new Date();
            //cerramos
            if (status.equals("closed")) {

                message.setIncidenceStatus("closed");
                message.setUser(user);
                message.setIncidenceClosed(today);
                message.setIncidenceUser(user);

                //actualizo el message
                text.append(System.lineSeparator())
                        .append("· ")
                        .append(FormatUtils.formatHumanDate(today))
                        .append(" ").append(user)
                        .append(" cerró la incidencia")
                        .append(System.lineSeparator())
                        .append(comment.trim())
                        .append(System.lineSeparator());

            } else {

                //solo añadimos mensaje
                text.append(System.lineSeparator())
                        .append("· ")
                        .append(FormatUtils.formatHumanDate(today))
                        .append(" ").append(user)
                        .append(" escribió: ")
                        .append(System.lineSeparator())
                        .append(comment.trim())
                        .append(System.lineSeparator());

            }

            message.setText(text.toString());
            csMessageRepository.save(message);
        }

        String view = "redirect:/#views/support/incidences";
        if (ref != null && ref.equals("client")) {
            view = "redirect:/#views/support/clients?q=id:" + message.getClientId();
        }

        return view;
    }

}
