/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.dao;

import com.wings.base.domain.client.CdrSums;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class CdrDAO {

    @Autowired
    private EntityManager em;
    
    private final SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat sdfTo = new SimpleDateFormat("yyyy-MM-dd");
    
    public CdrSums find(String client, Date from, Date to, Boolean priced, String type,String origin) {
        
        StringBuilder b = new StringBuilder();
        b.append("SELECT new com.wings.base.domain.client.CdrSums(sum(c.duration) as voice,");
        b.append("sum(c.data1+c.data2) as data,");
        b.append("sum(price) as price ,");
        b.append("SUM(CASE WHEN (c.cdrType='VOZ') THEN 1 ELSE 0 END) as calls,");
        b.append("SUM(CASE WHEN (c.cdrType='SMS') THEN 1 ELSE 0 END) as sms,");
        b.append("count(1) as records) ");
        b.append("FROM Cdr c ");        
        b.append("WHERE 1=1 ");
        
        if(client != null){
            b.append(" AND client = '").append(client).append("'");
        }
        
        if(from != null && to != null){
            b.append(" AND (date >= '")
                    .append(sdfFrom.format(from))
                    .append("' AND date <= '")
                    .append(sdfTo.format(to)).append("')");
        }
        
        if(priced != null){
            if(priced){
                b.append(" AND price > 0");
            }else{
                b.append(" AND price = 0");
            }
        }
        
        if(type != null){
            b.append(" AND cdr_type = '").append(type).append("'");
        }
        
        if(origin != null){
            b.append(" AND origin = '").append(origin).append("'");
        }
        
        Query query = em.createQuery(b.toString(),CdrSums.class);        
        CdrSums ret = (CdrSums)query.getSingleResult();
        
        return ret;
    }

}
