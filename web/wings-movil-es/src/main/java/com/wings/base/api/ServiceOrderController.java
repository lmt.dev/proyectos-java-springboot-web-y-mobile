/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.base.api;

import com.wings.base.api.dto.ResidualService;
import com.wings.base.api.dto.ServiceItem;
import com.wings.base.api.dto.ServiceOrder;
import com.wings.base.domain.client.Balance;
import com.wings.base.domain.client.Client;
import com.wings.base.domain.client.ClientLine;
import com.wings.base.domain.invoice.Invoice;
import com.wings.base.enums.BalanceType;
import com.wings.base.enums.InvoiceStatus;
import com.wings.base.repos.BalanceRepository;
import com.wings.base.repos.InvoiceRepository;
import com.wings.base.repos.LinesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class ServiceOrderController {

    @Autowired
    private LinesRepository linesRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @ResponseBody
    @RequestMapping(value = {"/api/services/orders/{year}/{month}"}, method = RequestMethod.POST)
    public ResponseEntity<List<ServiceOrder>> getServiceOrders(HttpServletRequest request, @PathVariable("year") Integer year, @PathVariable("month") Integer month) {

        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("a0e3fdf8e14cf3b7842a3d715e100faccac0efdb49e9fe510fd33b6ea603bd1a")) {
            return ResponseEntity.badRequest().build();
        }

        List<ServiceOrder> orders = new LinkedList<>();

        List<Balance> bals = balanceRepository.findByYearAndMonthAndTypeOrderByLineId(year, month, BalanceType.plan.name());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 5);
        cal.set(Calendar.HOUR_OF_DAY, 1);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date creationDate = cal.getTime();

        for (Balance bal : bals) {

            Long lineId = bal.getLineId();
            ClientLine line = linesRepository.findOne(lineId);
            Invoice invoice = invoiceRepository.findFirstByMonthAndYearAndClientId(month, year, bal.getClientId());

            creationDate = new Date(creationDate.getTime() + 1000);
            Date activationDate = null;
            boolean payed = false;
            if (invoice.getStatus().equals(InvoiceStatus.payed.name())) {
                payed = true;
                activationDate = creationDate;
            }

            ServiceOrder order = new ServiceOrder();
            order.setExternalId(bal.getId());

            order.setActivationDate(activationDate);
            order.setCreationDate(creationDate);

            order.setStatus(payed ? "active" : "created");
            order.setPaymentStatus(payed ? "paid" : "unpaid");
            order.setTypeStatus(line.getInternalState());

            order.setFirstName(line.getClient().getName());
            order.setLastName(line.getClient().getLastname());
            order.setComment(line.getClient().getIdValue());

            ServiceItem orderItem = new ServiceItem();
            orderItem.setCategory(3);

            String code;
            String[] arr = bal.getControl().split("-");
            if (bal.getControl().startsWith("combina")) {
                code = arr[0] + "-" + arr[1] + "-" + arr[2];
            } else {
                code = arr[0];
            }
            orderItem.setCode(code);
            orderItem.setDescription(bal.getDescription());

            orderItem.setName(bal.getDescription());

            orderItem.setPrice(bal.getAmount());
            order.setOrderItem(orderItem);
            order.setPaymentAmount(bal.getAmount());
            order.setPayerEmail(line.getClient().getEmail());
            order.setStoreId(line.getReferer());
            order.setType("service");
            orders.add(order);

        }

        return ResponseEntity.ok(orders);
    }

    @ResponseBody
    @RequestMapping(value = {"/api/services/lines"}, method = RequestMethod.POST)
    public ResponseEntity<List<ResidualService>> getClientLines(HttpServletRequest request) {

        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("a0e3fdf8e14cf3b7842a3d715e100faccac0efdb49e9fe510fd33b6ea603bd1a")) {
            return ResponseEntity.badRequest().build();
        }

        List<ClientLine> lines = linesRepository.findAll();
        List<ResidualService> toSend = new ArrayList<>();

        for (ClientLine l : lines) {
            ResidualService r = new ResidualService();

            Client c = l.getClient();
            r.setCompanyName(l.getClient().getCompanyName());
            r.setCreationDate(l.getCreationDate());
            r.setDescription(l.getPlanDescription());
            r.setFirstName(c.getName());
            r.setLastName(c.getLastname());
            r.setIdType(c.getIdType());
            r.setIdValue(c.getIdValue());
            r.setReferenceId(l.getId());
            r.setEmail(c.getEmail());

            r.setTypeStatus(l.getExternalState());
            r.setStatus(l.getInternalState());
            r.setStoreId(l.getReferer() != null ? l.getReferer() : c.getReferer());

            toSend.add(r);

        }

        return ResponseEntity.ok(toSend);

    }

}
