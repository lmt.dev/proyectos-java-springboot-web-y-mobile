$(document).ready(function ($) {

    /**tokenfield**/
    $('#tokenfield')
            .on('tokenfield:removedtoken', function (e) {
                var tokenList = $('#tokenfield').tokenfield('getTokensList', ',');
                loadWithTokens(cleanTokenList(tokenList));
            })
            .tokenfield();

    $('#drop-status-filter.dropdown-menu a').click(function (e) {
        var newToken = this.text;
        var tokenList = $('#tokenfield').tokenfield('getTokensList', ',');
        if (tokenList.indexOf(newToken) === -1) {
            if (tokenList === '') {
                tokenList = newToken;
            } else {
                tokenList += "," + newToken.trim();
            }
            loadWithTokens(cleanTokenList(tokenList));
        }
    });

    function loadWithTokens(tokenList) {
        var url = window.location.href;
        setUpUrl(addParam(url, 'status', encodeURI(tokenList)));
    }

    function cleanTokenList(tokenlist) {
        return tokenlist.split(' ').join('');
    }


});