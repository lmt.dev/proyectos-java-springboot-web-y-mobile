/****
 * ASIDE ACTIONS
 */
$(document).on('click', '.nav-link.navbar-toggler.aside-menu-toggler', function (e) {
    //e.preventDefault();
    if ($('body').hasClass('aside-menu-hidden')) {
        console.log("cerrado");
    } else {
        console.log("abierto");
        loadLogs();
    }

});


function loadLogs() {

    $('#process-log-container').html('');

    $.get('api/system/process', function (data, status) {
        if (status === 'success') {
            console.log(data);

            $(data).each(function () {
                var clon = $('.to-clone').clone();
                $(clon).removeClass('to-clone');
                $(clon).show();

                var durationText = 'ejecutando';
                if (this.duration) {
                    durationText = this.duration;
                }

                $(clon).find('#title').first().html(this.name);
                $(clon).find('#duration').first().html(this.launcher + ' - ' + durationText + ' ms.');
                $(clon).attr('title', this.info);

                if (!this.running) {
                    $(clon).find('#bar').removeClass('indeterminate');
                    $(clon).find('#bar').css('width', "100%");
                }

                /*duration: 4
                 endDate: 1506071242000
                 id: 4
                 info: "Proceso iniciado↵Solicitando estado de contratos↵Consultas realizadas: 0↵Contratos firmados: 0↵"
                 launcher: "system"
                 max: 0
                 name: "Contract status check"
                 running: false
                 startDate: 1506071242000
                 step: 0*/

                $('#process-log-container').prepend(clon);
            });

        } else {
            $.notify('Ocurrio un error al solicitar la creacion de clientes', {
                type: 'warning',
                allow_dismiss: true
            });
        }
    });


//    for (var i = 0, max = 5; i < max; i++) {
//        var clon = $('.to-clone').clone();
//
//        $(clon).removeClass('to-clone');
//        $(clon).show();
//
//        $('#process-log-container').prepend(clon);
//
//    }
}
