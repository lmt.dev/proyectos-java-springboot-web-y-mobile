$(document).ready(function ($) {

    /*function loadBalance() {
     var clientId = $('#client-id').val();
     $('#balance-content').load('views/clients/balances-short/' + clientId);
     }
     loadBalance();*/


    if ($('#textarea-input')) {
        $('#textarea-input').focus();
    } else {
        $('.search-input').focus();
    }

    $('.dropdown-item').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'add-line-holder') {

            loadInDialog('/views/clients/lines/add-line-holder', 'Añadir titular de linea', true);

        } else if (action === 'plan-change') {

            var lineId = $(e.target).data('id');
            loadInDialog('/views/clients/lines/' + lineId + '/plan-change', 'Cambio de plan', true);

        } else if (action === 'edit-line') {

            var lineId = $(e.target).data('id');
            loadInDialog('/views/clients/lines/' + lineId + '/edit', 'Editar Linea', true);

        } else if (action === 'chek-status') {
            var lineId = $(e.target).data('id');
//            $.post('/api/registration/create-holder', postData, function (data, status) {
//
//            }).error(function (response) {
//                notify(response.responseText);
//            });
        } else if (action === 'request-porta') {

            var postData = {};
            postData.ids = [$(e.target).data('id')];

            $.post('/api/lines/requestportability', postData, function (data, status) {
                notifySuccess(data);
            }).fail(function (response) {
                notifyError(response.responseText);
            });

        } else if (action === 'edit-client') {

            var id = $(e.target).data('id');
            loadInDialog('views/clients/' + id + '/edit', 'Editar Cliente');

        } else if (action === 'payment-invoice') {

            var id = $(e.target).data('id');
            loadInDialog('views/clients/' + id + '/edit', 'Editar Cliente', true);

        } else if (action === 'payment-auth') {

            var id = $(e.target).data('id');
            loadInDialog('views/support/payment-auth/' + id, 'Cambio de Tarjetas');

        } else if (action === 'client-invoices') {

            var id = $(e.target).data('id');
            var client = $(e.target).data('client');
            loadInDialog('views/support/client-invoices/' + id, 'Facturas del cliente ' + client, true);

        } else if (action === 'line-risk') {
            var id = $(e.target).data('id');
            loadInDialog('views/support/risk/' + id, 'Riesgo de la linea', true);

        } else if (action === 'line-status') {
            var id = $(e.target).data('id');
            loadInDialog('views/support/status/' + id, 'Modificar estado de la linea', true);

        } else if (action === 'add-payment') {

            var id = $(e.target).data('id');
            loadInDialog('views/support/add-payment/' + id, 'Añadir pago', true);

        } else if (action === 'block') {

            var id = $(e.target).data('id');
            loadInDialog('views/support/block/' + id, 'Bloquear Cliente', true);

        } else if (action === 'unblock') {

            var id = $(e.target).data('id');
            loadInDialog('views/support/unblock/' + id, 'Desbloquear Cliente', true);

        }
    });

    $('.action-item').click(function (e) {
        e.preventDefault();
        //var target = $(e.currentTarget).first();
        var url = window.location.href;
        setUpUrl(addParam(url, 'q', encodeURI(this.id)));
    });

    $('.action-button').click(function (e) {
        e.preventDefault();

        var action = $(e.target).attr('id');

        switch (action) {
            case 'edit-incidence':
                    
                var id = $(e.target).data('id');
                loadInDialog('views/support/incidence/' + id + "?ref=client", 'Editar / Cerrar incidencia', true);
                
                break;
            case 'status-refresh-icon':
                var id = $(e.target).data('id');

                $('#status-refresh-icon')
                        .removeClass('fa-refresh')
                        .addClass('fa-spinner fa-spin');

                $.post('/views/support/line/xstatus/' + id, null, function (data, status) {

                    $('#status-refresh-icon')
                            .removeClass('fa-spinner fa-spin')
                            .addClass('fa-refresh');

                    location.reload();

                }).fail(function (xhr, status, error) {

                    $('#status-refresh-icon')
                            .addClass('fa-refresh')
                            .removeClass('fa-reload fa-spin');
                });
                break;
        }

    });


    $('#textarea-input').keydown(function (e) {
        if (e.which === 13 && e.ctrlKey) {
            save();
        }
    });

    $('#message-save').click(function () {
        save();
    });


    function save() {

        var text = $('#textarea-input').val();
        if (text === '') {
            return;
        }

        $('#message-save').prop("disabled", true);

        var line = $('#line-selected').val();
        if (line === '') {
            line = null;
        }
        var clientId = $('#client-id').val();
        var techNotif = $('#chk-tech-notif').is(":checked");
        var incidence = $('#chk-incidence').is(":checked");

        var message = {};
        message.line = line;
        message.techNotif = techNotif;
        message.incidence = incidence;
        message.text = text;
        message.clientId = clientId;

        $('#saving-icon').show();

        $.post('api/support/messages', message, function (data, status) {

            $('#saving-icon').hide();
            $('#message-save').prop("disabled", false);

            if (status === 'success') {

                $('#textarea-input').val('');
                $('#chk-tech-notif').checked = false;
                $('#line-selected').val('');

                addMessage(data);

            } else {

                var message = {};
                message.line = line;
                message.techNotif = techNotif;
                message.incidence = incidence;
                message.text = "Ocurrio un error al guardar el mensaje";
                message.clientId = clientId;

                addMessage(data);

            }
        });
    }

    function addMessage(message) {

        var icon = '';
        if (message.techNotif) {
            icon = '  <i class="icon-tag" title="Notificación área técnica"></i>';
        }
        var iconIncidence = '';
        if (message.incidence) {
            iconIncidence = '  <i class="fa fa-exclamation-circle alert-danger" title="Incidencia"></i>';
        }

        var badge = 'badge-success';
        if (message.line === null) {
            badge = 'badge-warning';
            line = 'Cliente';
        } else {
            line = message.line;
        }

        var text = message.text;
        var user = message.user;
        var d = new Date();

        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        date += ' ' + d.getHours() + ":" + d.getMinutes();

        var elem = $('#message-container');
        elem.prepend(
                $.el('div', {'class': 'card'}).append(
                $.el('div', {'class': 'card-block'}).append(
                $.el('div', {'class': 'h6'}).append(
                $.el('span', {'class': 'float-right badge ' + badge}).text(line)
                )
                ).append(
                $.el('p', '').text(text)
                ).append(
                $.el('small', {'class': 'text-muted'}).text(user)
                ).append(
                $.el('small', {'class': 'float-right text-muted'}).append(
                $.el('span', '').text(date)
                ).append(icon).append(iconIncidence)
                )
                )
                );
    }

    $.extend({
        el: function (el, props) {
            var $el = $(document.createElement(el));
            $el.attr(props);
            return $el;
        }
    });
});
