$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'client', $('#client').val());
        url = addParam(url, 'line', $('#line').val());
        url = addParam(url, 'month', $('#month').val());
        url = addParam(url, 'year', $('#year').val());

        setUpUrl(url);
    });

});
