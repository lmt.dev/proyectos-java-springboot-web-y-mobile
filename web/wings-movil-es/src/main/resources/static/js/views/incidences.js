$(document).ready(function ($) {

    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'status', $('#status').val());

        setUpUrl(url);
    });



    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {
            case 'edit-incidence':

                var id = $(e.target).data('id');
                loadInDialog('views/support/incidence/' + id, 'Editar / Cerrar incidencia', true);

                break;
        }


    });

});
