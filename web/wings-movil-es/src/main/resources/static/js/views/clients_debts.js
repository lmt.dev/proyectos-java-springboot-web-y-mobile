$(document).ready(function ($) {


    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'sort_by', $('#sort_by').val());
        url = addParam(url, 'sort_dir', $('#sort_dir').val());

        setUpUrl(url);
    });


    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'gen-tables') {

            var notify = $.notify('Regenerando tablas, espere por favor', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/financial/debts/rebuild', null, function (data, status) {

                notify.close();

                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $.notify('Ocurrio un error al solicitar el pedido', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).fail(function (jxrq) {
                notifyError(jxrq.responseText);
            });
        } else if (action === 'get-csv') {

            var url = 'api/financial/debts/download';
            var win = window.open(url, '_blank');

        }
    });



});
