$(document).ready(function ($) {


    /**acciones de los botones **/
    $('.action-item').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'action-item-check-products') {            
            $('#viewModal').modal();
            $body = $('#viewModal').find('.modal-body');
            var url = 'api/plans/operator';
            $body.load(url);
        }else if (action === 'action-item-check-operators') {
            $('#viewModal').modal();
            $body = $('#viewModal').find('.modal-body');
            var url = 'api/operators/operator';
            $body.load(url);
        }else if(action === 'action-item-client-plans'){
            
            $('#viewModal').modal();
            $body = $('#viewModal').find('.modal-body');
            var url = 'api/planes/client/plans';
            $body.load(url);
                        
        }
    });


});
