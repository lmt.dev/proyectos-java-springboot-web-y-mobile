$(document).ready(function ($) {

    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'gen-recharge') {

            var notify = $.notify('Generando, espere por favor', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/financial/reminders/regen', null, function (data, status) {

                notify.close();

                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $.notify('Ocurrio un error al solicitar el pedido', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).fail(function (jxrq) {
                notifyError(jxrq.responseText);
            });
        } else if (action === 'send-notice') {

            var url = 'api/financial/reminders/send';

            var notify = $.notify('Marcando, espere por favor', {
                type: 'info',
                showProgressbar: true
            });

            $.post(url, null, function (data, status) {

                notify.close();

                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $.notify('Ocurrio un error al marcar los envios', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).fail(function (jxrq) {
                notifyError(jxrq.responseText);
            });
        } else if (action === 'stop-send-notice') {

            var url = 'api/financial/reminders/stop-send';

            var notify = $.notify('Marcando, espere por favor', {
                type: 'info',
                showProgressbar: true
            });

            $.post(url, null, function (data, status) {

                notify.close();

                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $.notify('Ocurrio un error al marcar los envios', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).fail(function (jxrq) {
                notifyError(jxrq.responseText);
            });
        }
    });



    $('.action-button').click(function (e) {
        e.preventDefault();
        var action = $(e.target).attr('id');

        switch (action) {
            case 'send-notice-one-sms':
                var url = 'views/financial/recharge/sendSMS/';
                var remId = $(e.target).data('id');
                loadInDialog(url + remId, 'Enviar SMS de recordatorio', true);
                break;
            case 'send-notice-one-email':
                var url = 'views/financial/recharge/sendEmail/';
                var remId = $(e.target).data('id');
                loadInDialog(url + remId, 'Enviar Email de recordatorio', true);
                break;
        }
    });

});
