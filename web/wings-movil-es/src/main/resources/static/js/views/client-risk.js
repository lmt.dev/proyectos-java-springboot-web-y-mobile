$(document).ready(function ($) {

    $('#post-changes').click(function (e) {

        showalert("Enviando informacion al operador, espere por favor...","alert-info");
        
        $form = $('#form-risk'); //wrap this in jQuery
        
        $.post($form.attr('action'), $form.serialize(), function (data, status) {
            if (status === 'success') {
                showalert("Modificado exitosamente","alert-success");
            } else {
                showalert("Ocurrió un error al modificar el riesgo","alert-danger");
            }
        }).fail(function (response) {
            showalert("Ocurrió un error al modificar el riesgo","alert-danger");
        });

    });

    function showalert(message, alerttype) {        
        $('#alert_placeholder').html('');
        $('#alert_placeholder').append('<div id="alertdiv" class="alert ' + alerttype + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
        /*setTimeout(function () { // this will automatically close the alert and remove this if the users doesnt close it in 5 secs
            $("#alertdiv").remove();
        }, 5000);*/
    }
});
