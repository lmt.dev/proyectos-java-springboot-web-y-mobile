$(document).ready(function ($) {

    /***Acciones del dropdown**/    
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;

        if (action === 'action-create-client-xtra') {

            var notify = $.notify('Solicitando creacion de cliente y lineas asociadas', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/clients/create/xtra', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        allow_dismiss: true
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar la creacion de clientes', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        }
    });


    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }


});
