var bootbox;
$(document).ready(function ($) {

    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;

        if (action === 'action-request-portability') {

            var notify = $.notify('Solicitando portabilidad', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/lines/requestportability', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar la portabilidad', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-request-portability-holder') {

            if (ids.length === 0) {
                bootbox.alert("Debe seleccionar al menos una linea");
                return;
            }

            if (ids.length > 1) {
                bootbox.alert("Solo se puede solicitar una portabilidad a al vez");
                return;
            }

            var notify = $.notify('Solicitando portabilidad con titular', {
                type: 'info',
                showProgressbar: true
            });

            postData.id = ids[0];
            $.post('api/lines/requestportability-holder', postData, function (data, status) {

                notify.close();

                if (status === 'success') {
                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $.notify('Ocurrio un error al solicitar la portabilidad', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            }).fail(function (xhr, status, error) {
                notify.close();
                $.notify({
                    title: 'Ocurrio un error al solicitar la portabilidad',
                    message: xhr.responseText
                }, {
                    type: 'warning',
                    allow_dismiss: true
                });
            });
        } else if (action === 'action-send-sim') {

            var notify = $.notify('Creando ordenes de envio', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/clients/orders/create', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar la portabilidad', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-request-brand-change') {

            var notify = $.notify('Creando solicitando cambio de marca', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/lines/brand-change', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar el cambio de marca', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-process-payment-authorizarion') {
            var url = 'api/lines/process-payment';
            postProcessRequest(url, postData);
        } else if (action === 'action-regenerate-contracts') {

            var notify = $.notify('Regenerando contratos', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/lines/regenerate-contract', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al regenerar contratos', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });

        } else if (action === 'action-send-sim-replacement') {
            postProcessRequest('api/lines/orders/create/replacement', postData);
        }
    });


    /**acciones de los botones **/
    $('.action-item').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'action-item-check-state') {

            var notify = $.notify('Verificando estado de linea', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/lines/check-status', null, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar el pedido', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });

        } else if (action === 'action-item-check-port-state') {

            var notify = $.notify('Verificando estado de portabilidades', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/lines/check-port-status', null, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        delay: 3000
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar el pedido', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });

        }
    });




    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }

});
