$(document).ready(function ($) {


    $('#search-button').click(function (e) {
        e.preventDefault();
        var url = window.location.href;

        url = addParam(url, 'q', $('#q').val());
        url = addParam(url, 'status', $('#status').val());
        url = addParam(url, 'month', $('#month').val());
        url = addParam(url, 'year', $('#year').val());

        setUpUrl(url);
    });

    /***Acciones del dropdown**/
    $('#actions-dropdown').click(function (e) {

        e.preventDefault();
        var ids = getCheckedItems();
        var action = $(e.target).attr('id');
        var postData = {};
        postData.ids = ids;


        if (action === 'action-send-invoices') {

            var notify = $.notify('Solicitando envio de facturas a clientes', {
                type: 'info',
                showProgressbar: true
            });

            $.post('api/clients/invoice/send', postData, function (data, status) {

                notify.close();

                if (status === 'success') {

                    $.notify({
                        title: '<strong>Informacion</strong>',
                        message: data
                    }, {
                        newest_on_top: true,
                        type: 'success',
                        allow_dismiss: true
                    });

                } else {

                    $.notify('Ocurrio un error al solicitar el envio de facturas', {
                        type: 'warning',
                        allow_dismiss: true
                    });
                }
            });
        } else if (action === 'action-charge-invoices') {
            postProcessRequest('api/clients/invoice/charge', postData);
        }
    });



    /**acciones de los botones **/
    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'view-invoice-detail') {

            var id = $(e.target).data('id');
            $.post('/api/clients/invoice/payment-details/' + id, null, function (response) {
                showAlert(response, 'alert-success');
            }).fail(function (xhr, status, error) {
                showAlert('Error en la devolucion ' + xhr.responseText, 'alert-warning');
            });
        }
    });



    function getCheckedItems() {
        var ids = [];
        $('table input:checked').each(function () {
            ids.push($(this).attr('id'));
        });
        return ids;
    }

});
