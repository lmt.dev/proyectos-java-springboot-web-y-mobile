$(document).ready(function ($) {

    function getStatus() {
        var url = 'api/system/status';
        $('#status-pre').load(url, function (response, status, xhr) {
            if (response.includes('Ninguno')) {
                $('#button-pannel').show();
            } else {
                $('#button-pannel').hide();
            }
        });

        setTimeout(function () {
            getStatus();
        }, 1000);
    }

    getStatus();

    $('#action-balance-gen-balance').click(function (e) {
        e.preventDefault();

        var year = $('#year').val();
        var month = $('#month').val();

        var postData = {};
        postData.year = year;
        postData.month = month;

        var notify = $.notify('Creando balance...', {
            type: 'info'
        });

        $.post('api/system/balances', postData, function (data, status) {

            notify.close();

            $.notify({
                title: '<strong>Informacion</strong>',
                message: data
            }, {
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true
            });

        }).fail(function (xhr, status, error) {
            $.notify(xhr.responseText, {
                type: 'warning',
                allow_dismiss: true
            });
        });

    });

    $('#action-invoice-gen-invoice').click(function (e) {
        e.preventDefault();

        var year = $('#year').val();
        var month = $('#month').val();

        var postData = {};
        postData.year = year;
        postData.month = month;

        var notify = $.notify('Creando facturacion...', {
            type: 'info',
            showProgressbar: true
        });

        $.post('api/system/invoices', postData, function (data, status) {

            notify.close();

            $.notify({
                title: '<strong>Informacion</strong>',
                message: data
            }, {
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true
            });

        }).fail(function (xhr, status, error) {
            $.notify(xhr.responseText, {
                type: 'warning',
                allow_dismiss: true
            });
        });
    });

    $('#action-invoice-gen-payment').click(function (e) {
        e.preventDefault();

        var year = $('#year').val();
        var month = $('#month').val();

        var postData = {};
        postData.year = year;
        postData.month = month;

        var notify = $.notify('Procesando Pagos...', {
            type: 'info',
            showProgressbar: true
        });

        $.post('api/system/payments', postData, function (data, status) {

            notify.close();

            $.notify({
                title: '<strong>Informacion</strong>',
                message: data
            }, {
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true
            });


        }).fail(function (xhr, status, error) {
            $.notify(xhr.responseText, {
                type: 'warning',
                allow_dismiss: true
            });
        });
    });


    $('#action-invoice-regenerate').click(function (e) {
        e.preventDefault();

        $.post('api/system/invoice/regenerate', null, function (data, status) {

            $.notify({
                title: '<strong>Informacion</strong>',
                message: data
            }, {
                newest_on_top: true,
                type: 'success',
                allow_dismiss: true
            });

        }).fail(function (xhr, status, error) {
            $.notify(xhr.responseText, {
                type: 'warning',
                allow_dismiss: true
            });
        });
    });

});

