

alter table cs_messages add column incidence tinyint(1) default 0;
alter table cs_messages add column incidence_status varchar(40) default null;
alter table cs_messages add column incidence_user varchar(150) default null;
alter table cs_messages add column incidence_closed DATETIME default null;


INSERT INTO wings_registration.users (first_name, last_name, email, username, password, enabled, creation_date, avatar) 
	VALUES
 ('Daniel', 'operador', 'atencionalcliente@wingsmobile.es', 'opdaniel', sha2('bXVq6Xjth6psfCmy',256), true, CURRENT_TIMESTAMP, 'http://placehold.it/50/55E7C1/fff&text=D'),
 ('Vero', 'operador', 'atencionalcliente@wingsmobile.es', 'opvero', sha2('UeeepRS9k2ZrbTqL',256), true, CURRENT_TIMESTAMP, 'http://placehold.it/50/55E7C1/fff&text=V'),
 ('David', 'Gonzalez', 'atencionalcliente@wingsmobile.es', 'dgonzalez', sha2('jZHZU83DPtmqPyws',256), true, CURRENT_TIMESTAMP, 'http://placehold.it/50/55E7C1/fff&text=D'),
 ('Damian', 'Tirante', 'atencionalcliente@wingsmobile.es', 'dtirante', sha2('QgUZuRPE9FT4p9Z8',256), true, CURRENT_TIMESTAMP, 'http://placehold.it/50/55E7C1/fff&text=D');

INSERT INTO wings_registration.users_roles (user_id, role_id) 
	VALUES 
            (6, 1),
            (7, 1),
            (8, 1),
            (9, 1);
