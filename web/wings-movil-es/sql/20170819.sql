
ALTER TABLE wings_registration.orders ADD shipping_address varchar(100) NULL ;
ALTER TABLE wings_registration.orders ADD shipping_city varchar(100) NULL ;
ALTER TABLE wings_registration.orders ADD shipping_state varchar(100) NULL ;
ALTER TABLE wings_registration.orders ADD shipping_cp varchar(50) NULL ;
ALTER TABLE wings_registration.orders ADD ups_store_id varchar(100) NULL ;
ALTER TABLE wings_registration.orders ADD parent BIGINT NULL ;

