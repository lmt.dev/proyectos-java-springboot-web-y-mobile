CREATE TABLE `client_line_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `line_id` bigint(20) NOT NULL ,
  `client_id` bigint(20) NOT NULL,
  `old_plan_id` bigint(20) NOT NULL,
  `new_plan_id` bigint(20) NOT NULL,  
  `creation_date` DATETIME DEFAULT NULL,
  `effective_date` DATETIME DEFAULT NULL,
  `applied` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
