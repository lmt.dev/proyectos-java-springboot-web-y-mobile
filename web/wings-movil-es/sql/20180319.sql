ALTER TABLE wings_registration.invoices MODIFY COLUMN total double(10,2) NULL ;
ALTER TABLE wings_registration.clients_balance MODIFY COLUMN amount decimal(10,2) NOT NULL ;

alter table wings_registration.client_lines add column risk_margin decimal(10,2) default 0;
alter table wings_registration.client_lines add column risk_extension decimal(10,2) default 0;
alter table wings_registration.client_lines add column risk_month_price decimal(10,2) default 0;
alter table wings_registration.client_lines add column risk_auto_block tinyint(1) default 0;
alter table wings_registration.client_lines add column risk_auto_unblock tinyint(1) default 0;
