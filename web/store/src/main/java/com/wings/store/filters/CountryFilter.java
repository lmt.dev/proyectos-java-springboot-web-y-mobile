/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.filters;

import com.wings.api.models.CountryDto;
import com.wings.api.models.CartDto;
import com.wings.store.services.ApiClientService;
import com.wings.store.services.GeoIpService;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class CountryFilter implements Filter {

    @Autowired
    private ApiClientService apiClientService;
    @Autowired
    private GeoIpService geoIpService;

    private final Logger log = LoggerFactory.getLogger(CountryFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        try {

            HttpSession session = ((HttpServletRequest) request).getSession();

            List<CountryDto> countries = apiClientService.getCountries();
            session.setAttribute("countries", countries);

            //si no tiene seteado pais todavia
            if (session.getAttribute("country") == null) {

                //busco la iso por IP
                String iso = geoIpService.getIsoForIp(geoIpService.getIp((HttpServletRequest) request));
                if (iso != null && !iso.equals("none")) {
                    countries.stream().filter((country) -> (country.getCode().toLowerCase().equals(iso.toLowerCase()))).forEachOrdered((_item) -> {
                        session.setAttribute("country", _item.getCode().toLowerCase());
                        session.setAttribute("countryDto", _item);
                    });

                    if (session.getAttribute("country") == null) {
                        countries.stream().filter((country) -> (country.getCode().equals("OT"))).forEachOrdered((country) -> {
                            session.setAttribute("countryDto", country);
                        });
                        session.setAttribute("country", "ot");
                    }
                } else {
                    countries.stream().filter((country) -> (country.getCode().equals("OT"))).forEachOrdered((country) -> {
                        session.setAttribute("countryDto", country);
                    });
                    session.setAttribute("country", "ot");
                }
            }

            if (session.getAttribute("cart") == null) {
                final CartDto cartDto = new CartDto();
                cartDto.setCountry((CountryDto) session.getAttribute("countryDto"));
                session.setAttribute("cart", cartDto);
            }
        } catch (Exception e) {
            log.error("Error on country filter", e);
        }
        filterChain.doFilter(request, response);
    }

}
