/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.filters;

import com.wings.store.services.ApiClientService;
import java.io.IOException;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author seba
 */
@Component
public class SponsorFilter implements Filter {

    @Autowired
    private ApiClientService apiClientService;

    private final Logger log = LoggerFactory.getLogger(SponsorFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        try {
            HttpSession session = ((HttpServletRequest) request).getSession();
            String query = ((HttpServletRequest) request).getQueryString();

            if (session != null && session.getAttribute("sponsor") == null && query != null && query.contains("store=")) {
                String alias = query.substring(query.indexOf("store=") + 6);

                Map<String, String> map = apiClientService.getMember(alias);
                if (map != null && !map.isEmpty() && map.get("found") != null && map.get("found").equals("1")) {
                    session.setAttribute("sponsor", alias);
                    session.setAttribute("avatar", map.get("avatar"));
                } else {
                    session.setAttribute("sponsor", "member_wrong");
                    session.setAttribute("avatar", "/images/avatars/member_wrong.jpg");
                }
            }
        } catch (Exception e) {
            log.error("Error on sponsor filter", e);
        }

        filterChain.doFilter(request, response);
    }

}
