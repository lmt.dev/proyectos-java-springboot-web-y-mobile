package com.wings.store.web;

import com.wings.api.models.CartDto;
import com.wings.store.services.CartService;
import com.wings.store.services.Result;
import com.wings.store.services.ApiClientService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lucas
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private ApiClientService storeItemsService;

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/cart/{id}"}, method = RequestMethod.POST)
    public String addItemToCart(Model model, HttpServletRequest request, @PathVariable("id") @Positive Long id) {

        CartDto cart = (CartDto) request.getSession().getAttribute("cart");
        if (cart == null || cart.getCountry() == null) {
            return "redirect:/";
        }

        String country = (String) request.getSession().getAttribute("country");
        Result<CartDto> result = cartService.addItemToCart(cart, country, id);
        //model.addAttribute("messages", result.toString());
        //model.addAttribute("quantity", settingsService.getQuantities());
        return "cart";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/cart"}, method = RequestMethod.GET)
    public String getCart(Model model, HttpServletRequest request) {
        //model.addAttribute("quantity", settingsService.getQuantities());

        String country = (String) request.getSession().getAttribute("country");
        model.addAttribute("itemsRelated", storeItemsService.searchRelated(null, country));

        return "cart";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/cart/{id}"}, method = RequestMethod.DELETE)
    public ResponseEntity<CartDto> deleteItemFromCart(Model model, HttpServletRequest request, @PathVariable("id") @Positive Long id) {

        CartDto cart = (CartDto) request.getSession().getAttribute("cart");
        if (cart == null || cart.getItems().isEmpty() || cart.getCountry() == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Result<CartDto> result = cartService.deleteItemToCart(cart, id);
        return new ResponseEntity<>(result.getResult(), HttpStatus.OK);
    }

}
