package com.wings.store.services;

import com.wings.api.models.StoreItemDto;
import com.wings.api.models.CartDto;
import com.wings.api.models.CountryDto;
import com.wings.store.utils.NumberUtils;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucas
 */
@Service
public class CartService {

    @Autowired
    private ApiClientService itemService;

    /**
     *
     * @param cart
     * @param country
     * @param itemId
     * @return
     */
    public Result<CartDto> addItemToCart(CartDto cart, String country, Long itemId) {

        Result<CartDto> result = new Result<>(cart);
        StoreItemDto dto = itemService.getItem(itemId, null, country).getItems().get(0);

        //TODO: por ahora solo se deja un solo item, hasta que se haga 
        //la distribucion de puntos y comisiones para cada uno de los productos
        cart.getItems().clear();
        cart.getItems().add(dto);

        //TODO: esto se deja comentado por que el clear de arriba limpia el carrito
        //cuando se deje de limpiar el carrito esto se comenta para permitir solo un 
        //kit, store, upgrade
        //onlyOneKit(cart);
        sumCart(cart);

        return result;
    }

    /**
     *
     * @param cart
     * @param itemId
     * @return
     */
    public Result<CartDto> deleteItemToCart(CartDto cart, Long itemId) {

        Result<CartDto> result = new Result<>(cart);
        List<StoreItemDto> dtos = cart.getItems();
        StoreItemDto toDelete = null;
        for (StoreItemDto dto : dtos) {
            if (dto.getId().equals(itemId)) {
                toDelete = dto;
                break;
            }
        }
        if (toDelete != null) {
            dtos.remove(toDelete);
        }

        sumCart(cart);

        return result;

    }

    /**
     *
     * @param cart
     */
    private void sumCart(CartDto cart) {
        double total = 0d;
        double shippingScore = 0d;
        for (StoreItemDto item : cart.getItems()) {
            total += item.getPrice();
            shippingScore += item.getShippingScore();
        }

        cart.setSubTotal(total);
        cart.setShipping(shippingCost(shippingScore, cart.getCountry()));
        cart.setTotal(cart.getSubTotal() + cart.getShipping());

        String lang[] = cart.getCountry().getDefaultLocale().split("_");
        Locale locale = new Locale(lang[0], lang[1]);

        cart.setSubTotalString(NumberUtils.formatMoney(cart.getSubTotal(), locale));
        cart.setShippingString(NumberUtils.formatMoney(cart.getShipping(), locale));
        cart.setTotalString(NumberUtils.formatMoney(cart.getTotal(), locale));

    }

    /**
     *
     * @param shippingScore
     * @return
     */
    private Double shippingCost(double shippingScore, CountryDto country) {

        Double shippingCost = 0.0;
        Double floor = Math.floor(shippingScore);
        if (floor > 4) {
            floor = 4d;
        }
        int shippingSelect = floor.intValue();

        switch (shippingSelect) {
            case 0:
                shippingCost = 0.0;
                break;
            case 1:
                shippingCost = country.getShipping1();
                break;
            case 2:
                shippingCost = country.getShipping2() != null ? country.getShipping2() : shippingCost;
                break;
            case 3:
                shippingCost = country.getShipping3() != null ? country.getShipping3() : shippingCost;
                break;
            case 4:
                shippingCost = country.getShipping4() != null ? country.getShipping4() : shippingCost;
                break;
        }

        return shippingCost;
    }

    private void onlyOneKit(CartDto cart) {

        List<StoreItemDto> dtos = cart.getItems();
        int kits = 0;
        StoreItemDto itemToDelete = null;

        for (StoreItemDto storeItemDto : dtos) {
            if (storeItemDto.isAfiliateItem()) {
                kits++;
                if (kits > 1) {
                    break;
                }
                itemToDelete = storeItemDto;
            }
        }

        if (kits > 1 && itemToDelete != null) {
            dtos.remove(itemToDelete);
        }
    }
}
