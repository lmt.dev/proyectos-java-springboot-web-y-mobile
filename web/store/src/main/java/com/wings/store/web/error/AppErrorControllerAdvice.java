/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.web.error;

import com.wings.store.services.GeoIpService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author seba
 */
@ControllerAdvice
public class AppErrorControllerAdvice {

    @Autowired
    private GeoIpService geoIpService;
    private final Logger log = LoggerFactory.getLogger(AppErrorControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public String handleError(Model model, HttpServletRequest request, Exception ex, HttpServletResponse response) {

        String ip = geoIpService.getIp(request);
        try {
            log.error(
                    "Request: " + ip + " "
                    + request.getRequestURL() + " "
                    + "raised " + ex.getMessage() + " "
                    + "by " + ex.getClass().getName()
                    + ((ex.getStackTrace() != null && ex.getStackTrace().length > 0)
                    ? (" on " + ex.getStackTrace()[0].toString())
                    : "")
            );
        } catch (Exception e) {
            log.error("Error handling " + e.toString());
        }
        model.addAttribute("message", ex.getMessage());

        return "errors/error";
    }
}
