/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.web;

import com.wings.api.models.CartDto;
import com.wings.api.models.CartDtoResponse;
import com.wings.api.models.CountryDto;
import com.wings.api.models.PersonalInfoDto;
import com.wings.store.services.ApiClientService;
import com.wings.store.services.RegisterService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class RegisterController {

    @Autowired
    private ApiClientService apiClientService;
    @Autowired
    private RegisterService registerService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public String getRegister(Model model, HttpServletRequest request) {

        CartDto cart = (CartDto) request.getSession().getAttribute("cart");
        if (cart == null || cart.getItems().isEmpty() || cart.getCountry() == null) {
            return "redirect:/";
        }

        List<String> dnis = new ArrayList<>();
        try {
            String dniString = cart.getCountry().getDnisList();
            dnis = Arrays.asList(dniString.split(","));
        } catch (Exception e) {
        }

        cart.getPersonalInfo().setBillingCountry(cart.getCountry().getCode());
        cart.getPersonalInfo().setTermsChecked(false);

        //set sponsor
        if (request.getSession().getAttribute("sponsor") != null && !request.getSession().getAttribute("sponsor").equals("member_wrong")) {
            String sponsor = (String) request.getSession().getAttribute("sponsor");
            cart.getPersonalInfo().setSponsor(sponsor);
        }

        if (cart.getCountry().getCode().equals("OT")) {
            List<CountryDto> others = apiClientService.getOtherCountries();
            model.addAttribute("others", others);
        }

        String categoryName = cart.getItems().get(0).getCategoryName();

        model.addAttribute("info", cart.getPersonalInfo());
        model.addAttribute("cart", cart);
        model.addAttribute("dnis", dnis);
        model.addAttribute("installment", cart.getItems().get(0).isInstallment());
        model.addAttribute("category", categoryName);

        //topups
        if (categoryName.equals("topup")) {
            return "register_topup";
        }
        //only login
        //for upgrade
        if (categoryName.equals("upgrade")) {
            return "register_login";
        }

        return "register";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @param info
     * @return
     */
    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public String postRegister(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
            @ModelAttribute("info") PersonalInfoDto info) {

        CartDto cart = (CartDto) request.getSession().getAttribute("cart");
        if (cart == null || cart.getItems() == null || cart.getItems().isEmpty()) {
            return "redirect:/";
        }

        Locale locale = new Locale(cart.getCountry().getDefaultLang(), cart.getCountry().getCode());
        List<String> errors;
        String categoryName = cart.getItems().get(0).getCategoryName();

        //no es afiliado y no es recarga
        if (!cart.getPersonalInfo().isAlreadyAfiliate() && !categoryName.equals("topup")) {
            //update info
            cart.setPersonalInfo(info);
            //validation
            errors = registerService.validatePersonalInfo(info, locale);
            if (!errors.isEmpty()) {
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:/register";
            }
        }

        //if is topup
        if (categoryName.equals("topup")) {

            errors = registerService.validateTopUpInfo(info, locale);
            if (!errors.isEmpty()) {
                redirectAttributes.addFlashAttribute("errors", errors);
                return "redirect:/register";
            }

            cart.getPersonalInfo().setFirstName(info.getFirstName());
            cart.getPersonalInfo().setLastName(info.getLastName());
            cart.getPersonalInfo().setEmail(info.getEmail());
            cart.getPersonalInfo().setPhoneNumber(info.getPhoneNumber());

        }

        //Send to backoffice
        CartDtoResponse response = apiClientService.postCartToBackoffice(cart);
        if (response.getStatus().equals("error")) {
            errors = Arrays.asList(registerService.getMessage(response.getMessage(), locale));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/register";
        }

        //ya esta todo ok, vacio la session por las dudas
        request.getSession().invalidate();

        //TODO: redirect
        return "redirect:" + response.getPaymentToken();
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/register/login"}, method = RequestMethod.POST)
    public ResponseEntity<String> postMemberLogin(Model model, HttpServletRequest request) {

        try {

            CartDto cart = (CartDto) request.getSession().getAttribute("cart");

            String username = request.getParameter("username");
            String password = request.getParameter("password");
            Map<String, String> res = apiClientService.loginMember(username, password);

            //is login, update cart info
            if (res != null && res.get("login") != null && res.get("login").equals("true")) {

                cart.getPersonalInfo().setAlreadyAfiliate(true);
                cart.getPersonalInfo().setUsername(res.get("username"));

                return ResponseEntity.ok("login");

            } else {

                Locale locale = new Locale(cart.getCountry().getDefaultLang(), cart.getCountry().getCode());
                return ResponseEntity.badRequest().body(registerService.getMessage("wrong_username_or_password", locale));

            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.toString());
        }

    }

}
