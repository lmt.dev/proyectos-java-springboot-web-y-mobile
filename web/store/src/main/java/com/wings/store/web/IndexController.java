/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.web;

import com.wings.api.models.CountryDto;
import com.wings.api.models.StoreItemDto;
import com.wings.api.models.CartDto;
import com.wings.store.services.ApiClientService;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
@Valid
public class IndexController {

    @Autowired
    private ApiClientService apiClientService;

    private final Logger log = LoggerFactory.getLogger(IndexController.class);
    private final String regex = "[^\\p{L}\\p{N}\\p{P}\\p{Z}]";

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String getIndex(Model model, HttpServletRequest request) {

        String country = request.getSession().getAttribute("country").toString();
        List<StoreItemDto> items = apiClientService.mostPopular(country);
        List<StoreItemDto> searchKits = apiClientService.searchKitsNoInstallment(country);
        List<StoreItemDto> kits = searchKits.size() > 8 ? searchKits.subList(0, 8) : searchKits;
        List<String> categories = apiClientService.getCategories();

        model.addAttribute("items", items);
        model.addAttribute("categories", categories);
        model.addAttribute("kits", kits);
        model.addAttribute("available", false);

        return "index";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/afiliation"}, method = RequestMethod.GET)
    public String getAfiliation(Model model, HttpServletRequest request) {
        return "afiliation";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/franchise"}, method = RequestMethod.GET)
    public String getFranchise(Model model, HttpServletRequest request) {
        return "franchise";
    }

    /**
     *
     * @param model
     * @param request
     * @param country
     * @return
     */
    @RequestMapping(value = {"/set/{country}"}, method = RequestMethod.GET)
    public String setCountry(Model model, HttpServletRequest request, @PathVariable("country") String country) {
        final String countryStr = country.toLowerCase();
        final List<CountryDto> countries = (List<CountryDto>) request.getSession().getAttribute("countries");
        final CountryDto countryDto = countries.stream().filter(filter -> filter.getCode().equalsIgnoreCase(countryStr)).collect(Collectors.toList()).get(0);
        final CartDto cartDto = new CartDto();
        cartDto.setCountry(countryDto);
        request.getSession().setAttribute("country", countryStr);
        request.getSession().setAttribute("cart", cartDto);
        request.getSession().setAttribute("countryDto", countryDto);
        String referer = request.getHeader("referer");
        if (referer != null) {
            return "redirect:" + referer;
        }
        return "redirect:/";
    }

    /**
     * Change from list to grid or viceversa on listings
     *
     * @param model
     * @param request
     * @param view
     * @return
     */
    @RequestMapping(value = {"/setview/{view}"}, method = RequestMethod.GET)
    public String setView(Model model, HttpServletRequest request, @PathVariable("view") String view) {

        //TODO: si el cambio se produce en el checkout verificar que el kit
        //este disponible para ese pais, si no esta mostrar error
        request.getSession().setAttribute("view", view);
        String referer = request.getHeader("referer");
        if (referer != null) {
            return "redirect:" + referer;
        }
        return "redirect:/";
    }

    /**
     * From alias set a sponsor
     *
     * @param model
     * @param request
     * @param alias
     * @param detailId
     * @return
     */
    @RequestMapping(value = {"/store", "/store/{alias}", "/store/{alias}/{detailId}"}, method = RequestMethod.GET)
    public String setSponsor(Model model, HttpServletRequest request,
            @PathVariable(name = "alias", required = false) String alias,
            @PathVariable(name = "detailId", required = false) String detailId
    ) {

        if (alias != null) {
            alias = alias.replaceAll(regex, "");
            if (alias.equals("clean")) {
                request.getSession().removeAttribute("sponsor");
            } else {
                Map<String, String> map = apiClientService.getMember(alias);
                if (map != null && !map.isEmpty() && map.get("found") != null && map.get("found").equals("1")) {
                    request.getSession().setAttribute("sponsor", alias);
                    request.getSession().setAttribute("avatar", map.get("avatar"));
                } else {
                    request.getSession().setAttribute("sponsor", "member_wrong");
                    request.getSession().setAttribute("avatar", "/images/avatars/member_wrong.jpg");
                }
            }
        }

        if (detailId != null && !detailId.isEmpty()) {
            return "redirect:/details/" + detailId;
        }

        return "redirect:/";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/terms"}, method = RequestMethod.GET)
    public String getTerms(Model model, HttpServletRequest request) {

        String doc = "conditions_member_";
        String iso = (String) request.getSession().getAttribute("country");
        if (iso != null && !iso.toLowerCase().equals("ot")) {
            doc = "conditions_member_" + iso.toLowerCase();
            CartDto cart = (CartDto) request.getSession().getAttribute("cart");
            if (cart != null && !cart.getItems().isEmpty()) {
                String categoryName = cart.getItems().get(0).getCategoryName();
                List<String> categories = Arrays.asList("product", "ticket", "service", "topup");
                if (categories.contains(categoryName)) {
                    doc = "conditions_customers_" + iso.toLowerCase();
                }
            }
        }

        return "redirect:/docs/" + doc + ".pdf";

    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/installment"}, method = RequestMethod.GET)
    public String getInstallment(Model model, HttpServletRequest request) {
        String iso = (String) request.getSession().getAttribute("country");
        String doc = "installments_" + iso;
        return "redirect:/docs/" + doc + ".pdf";
    }
    
    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/affiliation/contract"}, method = RequestMethod.GET)
    public String getAfiliationContract(Model model, HttpServletRequest request) {
        String iso = (String) request.getSession().getAttribute("country");
        String doc = "afiliation_contract_" + iso;
        return "redirect:/docs/" + doc + ".pdf";
    }

    /**
     *
     * @param model
     * @param request
     * @param type
     * @return
     */
    @RequestMapping(value = {"/terms/{type}"}, method = RequestMethod.GET)
    public String getTermsUrl(Model model, HttpServletRequest request, @PathVariable("type") String type) {

        String view = "terms/terms";
        String iso = (String) request.getSession().getAttribute("country");
        if (iso != null && !iso.toLowerCase().equals("ot")) {
            view = "terms/" + type + "/terms_" + iso.toLowerCase();
        }
        return view;

    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/suscribe"}, method = RequestMethod.POST)
    public String postSuscribe(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        log.error("Suscribe INFO " + request.getParameter("email"));
        redirectAttributes.addFlashAttribute("message", "message.subscribe");
        return "redirect:/";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/contact"}, method = RequestMethod.POST)
    public String postContactMe(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        log.error("Contact INFO " + request.getParameter("email") + " " + request.getParameter("name"));
        redirectAttributes.addFlashAttribute("message", "message.contact");
        return "redirect:/";
    }

}
