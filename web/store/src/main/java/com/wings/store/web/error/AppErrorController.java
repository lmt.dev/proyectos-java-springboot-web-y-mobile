/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.store.web.error;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author seba
 */
@Controller
public class AppErrorController implements ErrorController {

    private final Logger log = LoggerFactory.getLogger(AppErrorController.class);

    @Override
    public String getErrorPath() {
        return "/error";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/error"}, method = RequestMethod.GET)
    public String getError(Model model, HttpServletRequest request) {
        return "errors/error";
    }

}
