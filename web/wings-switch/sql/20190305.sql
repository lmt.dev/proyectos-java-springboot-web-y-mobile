
CREATE TABLE `roaming_free_operators` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `operator_name` varchar(150),
  `mcc` varchar(20),
  `mnc` varchar(20),
  `country` varchar(20),
  `dial_string_enable` varchar(60),
  `dial_string_disable` varchar(60),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `roaming_free_operators_numbers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `operator_id` bigint(20),
  `did_number` varchar(60),
  `asignations` integer not null default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE client_roaming_free;
CREATE TABLE `client_roaming_free` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20),
  `client_number` varchar(60),
  `operator_number` varchar(60),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table clients add column mcc integer not null default 0;
alter table clients add column mnc integer not null default 0;
alter table client_dids add column client_number varchar(60);

