package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.DidCountry;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author seba
 */
public interface DidCountryRepository extends PagingAndSortingRepository<DidCountry, Long>, JpaSpecificationExecutor<DidCountry> {

    List<DidCountry> findByEnabled(Boolean enabled);

    DidCountry findByRemoteId(String remoteId);

    Page<DidCountry> findAll(Pageable pageable);

}
