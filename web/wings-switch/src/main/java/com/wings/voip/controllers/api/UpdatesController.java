/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.model.UpdateCheckResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class UpdatesController {

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/updates/check"}, method = {RequestMethod.GET})
    public ResponseEntity<UpdateCheckResponse> checkVersionUpdate(Model model, HttpServletRequest request) {

        UpdateCheckResponse ret = new UpdateCheckResponse();
        String version = request.getParameter("version");
        String pmodel = request.getParameter("model");

        if (version.equals("4.15")) {
            ret.setUpdateAvailable(true);
            //ret.setMarketLink("market://details?id=com.wings.phone");
            //ret.setUrlLink("https://play.google.com/store/apps/details?id=com.wings.phone");
            ret.setMarketLink("https://wingstel.eu/phone.apk");
            ret.setUrlLink("https://wingstel.eu/phone.apk");
        } else {
            ret.setUpdateAvailable(false);
            ret.setMarketLink("market://details?id=com.wings.phone");
            ret.setUrlLink("https://play.google.com/store/apps/details?id=com.wings.phone");
        }

        //disable updates
        ret.setUpdateAvailable(false);
        return ResponseEntity.ok(ret);
    }

}
