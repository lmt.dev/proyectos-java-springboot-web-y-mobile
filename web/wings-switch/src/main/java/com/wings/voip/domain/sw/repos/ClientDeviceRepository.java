/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.ClientDevice;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author seba
 */
public interface ClientDeviceRepository extends PagingAndSortingRepository<ClientDevice, Long>, JpaSpecificationExecutor<ClientDevice> {

    Page<ClientDevice> findAll(Pageable pageable);

    List<ClientDevice> findByClientId(Long clientId);

    ClientDevice findFirstByClientIdAndActive(Long clientId, Boolean active);

    ClientDevice findFirstByClientIdAndDeviceSerialNumber(Long clientId, String deviceSerialNumber);
}
