/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.model.AccountStatus;
import com.wings.voip.services.ClientServiceService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ApiClientController {

    private final Logger log = LoggerFactory.getLogger(ApiClientController.class);

    @Autowired
    private ClientServiceService clientServiceService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/clients/account/status"}, method = {RequestMethod.GET})
    public ResponseEntity<AccountStatus> getAccountStatusApi(Model model, HttpServletRequest request) {

        //authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            return ResponseEntity.badRequest().build();
        }

        String login = request.getParameter("login");
        String locale = request.getParameter("locale");
        return ResponseEntity.ok(clientServiceService.getAccountStatus(login, locale));
    }

    /**
     * 
     * @param model
     * @param request
     * @param image
     * @return
     * @throws FileNotFoundException 
     */
    @RequestMapping(value = {"/api/images/{image}"}, method = {RequestMethod.GET})
    public ResponseEntity<InputStreamResource> getImage(Model model, HttpServletRequest request, @PathVariable("image") String image) throws FileNotFoundException {

        File f = new File("/var/wings/switch/images/" + image + ".png");
        FileInputStream fis = new FileInputStream(f);

        return ResponseEntity.ok()
                .contentLength(f.length())
                .header("Cache-control: public, max-age=604800")
                .contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(fis));
    }

}
