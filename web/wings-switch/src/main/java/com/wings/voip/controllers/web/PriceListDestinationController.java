package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.PriceList;
import com.wings.voip.domain.sw.entities.PriceListDestination;
import com.wings.voip.services.PriceListService;
import com.wings.voip.utils.PageWrapper;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class PriceListDestinationController {

    private final Logger log = LoggerFactory.getLogger(PriceListDestinationController.class);

    @Autowired
    private PriceListService priceListService;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "views/destinations/", method = RequestMethod.GET)
    public String getDestination(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String priceList = request.getParameter("priceListId");
        String q = request.getParameter("q");

        int page = 0;
        int size = 20;
        Long priceListId = null;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (priceList != null && priceList.equals("all")) {
            priceList = null;
        }

        try {
            priceListId = Long.valueOf(priceList);
        } catch (Exception e) {
        }

        Page<PriceListDestination> items = priceListService.searchDestination(page, size, q, priceListId);
        PageWrapper<PriceListDestination> pageWrapper = new PageWrapper<>(items, "/views/destinations/");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        List<PriceList> priceLists = priceListService.findAllPriceList();

        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);
        model.addAttribute("priceList", priceListId);
        model.addAttribute("priceLists", priceLists);
        model.addAttribute("importStatus", priceListService.getImportRunningStatus());

        return "html/destinations";
    }

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/views/destinations/add", method = RequestMethod.GET)
    public String getDestinationAdd(Model model) {
        List<PriceList> priceLists = priceListService.findAllPriceList();
        model.addAttribute("priceLists", priceLists);
        return "html/destinations-add";
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/views/destinations/add"}, method = RequestMethod.POST)
    public ResponseEntity<String> postDestinationAdd(HttpServletRequest request) {

        String priceListId = request.getParameter("priceListId");
        String name = request.getParameter("name");
        String countryCode = request.getParameter("countryCode");
        String areaCode = request.getParameter("areaCode");
        String price = request.getParameter("price");

        priceListService.postServiceDestinationList(name, countryCode, areaCode, price, priceListId);
        return ResponseEntity.ok("El destino fue creado");

    }

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/views/destinations/import", method = RequestMethod.GET)
    public String getDestinationImport(Model model) {
        List<PriceList> priceLists = priceListService.findAllPriceList();
        model.addAttribute("priceLists", priceLists);
        return "html/destinations-import";
    }

    /**
     *
     * @param request
     * @param file
     * @return
     */
    @RequestMapping(value = "/views/destinations/import", method = RequestMethod.POST)
    public String postDestinationImport(HttpServletRequest request, MultipartFile file) {
        try {
            String sPriceListId = request.getParameter("priceListId");
            File temp = File.createTempFile("import_", ".csv");
            file.transferTo(temp);

            new Thread(() -> {
                priceListService.importFile(Long.valueOf(sPriceListId), temp);
            }).start();

        } catch (Exception e) {
            log.error("Error on import view!", e);
        }
        return "redirect:/#/views/destinations/";
    }

}
