
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.PriceListDestination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public interface PriceListDestinationRepository extends JpaRepository<PriceListDestination, Long>, JpaSpecificationExecutor<PriceListDestination> {

    List<PriceListDestination> findByPriceListId(Long proceListId);

    List<PriceListDestination> findAll();

}
