/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.wings.voip.model.GenericResponse;
import com.wings.voip.model.requests.RequestMissedCallResponse;
import com.wings.voip.services.TwilioClient;
import com.wings.voip.sip.MissedCallResponse;
import com.wings.voip.sip.SipManager;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
@RequestMapping(value = "api/")
public class MissedCallController {

    @Autowired
    private SipManager sipManager;

    @Autowired
    private TwilioClient twilioClient;

    private final Logger log = LoggerFactory.getLogger(MissedCallController.class);
    private final Map<String, String> toVerify = new HashMap<>();

    @ResponseBody
    @RequestMapping(value = {"/verify/request_call"}, method = RequestMethod.GET)
    public ResponseEntity<RequestMissedCallResponse> request(HttpServletRequest request,
            @RequestParam(required = false, value = "app_id") String appId,
            @RequestParam(required = false, value = "access_token") String accessToken,
            @RequestParam(required = false, value = "mobile") String mobile
    ) {

        RequestMissedCallResponse response = new RequestMissedCallResponse();
        boolean serverApiOk = false;
        boolean clientApiOk = false;
        if (appId.equals("ef77fe1a2a914d729fee3ce")
                && accessToken.equals("f53ca2e0990e5a4c0674342e86e8959d6eb35eaf")) {
            serverApiOk = true;
        }

        if (appId.equals("a2dc3b1c57d64fe9840c5cb")
                && accessToken.equals("6f4390a15e9165f37ad6d96e0c63f68fb1cc1f13")) {
            clientApiOk = true;
        }

        if (!(clientApiOk || serverApiOk)) {
            return ResponseEntity.badRequest().header("error", "unauthorized").build();
        }

        try {

            MissedCallResponse res = sipManager.makeMissedCall(mobile);
            //MissedCallResponse res = twilioClient.makeMissedCall(mobile);

            response.setCallId(res.getUuid());
            response.setOffSet(res.getFrom().substring(3, res.getFrom().length() - 4));
            response.setStatus("success");

            log.error("Request: mobile->" + mobile + ", cipher->" + response.getOffSet() + ", full->" + res.getFrom());

            toVerify.put(res.getUuid(), res.getFrom());

        } catch (Exception e) {
            return ResponseEntity.badRequest().header("error", e.getMessage()).build();
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = {"/verify/confirm"}, method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> verify(HttpServletRequest request,
            @RequestParam(required = false, value = "app_id") String appId,
            @RequestParam(required = false, value = "access_token") String accessToken,
            @RequestParam(required = false, value = "mobile") String mobile,
            @RequestParam(required = false, value = "imei") String imei,
            @RequestParam(required = false, value = "mcc") String mcc,
            @RequestParam(required = false, value = "mnc") String mnc,
            @RequestParam(required = false, value = "lat") String lat,
            @RequestParam(required = false, value = "keymatch") String keymatch,
            @RequestParam(required = false, value = "brand_name") String brandName,
            @RequestParam(required = false, value = "os_version") String osVersion,
            @RequestParam(required = false, value = "gmail_id") String gmailId
    ) {

        Map response = new HashMap();

        boolean serverApiOk = false;
        boolean clientApiOk = false;
        if (appId.equals("ef77fe1a2a914d729fee3ce")
                && accessToken.equals("f53ca2e0990e5a4c0674342e86e8959d6eb35eaf")) {
            serverApiOk = true;
        }

        if (appId.equals("a2dc3b1c57d64fe9840c5cb")
                && accessToken.equals("6f4390a15e9165f37ad6d96e0c63f68fb1cc1f13")) {
            clientApiOk = true;
        }

        if (!(clientApiOk || serverApiOk)) {
            response.put("error", "unauthorized");
            return ResponseEntity.badRequest().body(response);
        }

        try {

            if (toVerify.containsKey(mcc)) {
                String callee = toVerify.get(mcc);
                log.error("Verify: mobile-> " + mobile + ", mnc->" + mnc + ", calle-> " + callee);
                if (callee.contains(mnc)) {
                    response.put("status", "success");
                } else {
                    response.put("status", "failed");
                }
                toVerify.remove(mcc);
            } else {
                response.put("status", "failed");
            }

            response.put("mobile", mobile);
            response.put("codes", new int[]{200});
            response.put("app_user_id", appId);

            log.error("Respuesta  " + response.toString());

        } catch (Exception e) {
            response.put("error", e.getMessage());
            log.error("Error: " + e.getMessage());
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = {"/verify/done"}, method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> done(HttpServletRequest request, @RequestParam("callId") String callId) {

        log.error("Error: Done " + callId);

        Map response = new HashMap();
        try {
            sipManager.done(callId);
            response.put("status", "success");
        } catch (Exception e) {
            response.put("error", e.getMessage());
            log.error("Error: " + e.getMessage());
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = {"/verify/number"}, method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> verifyValidNumber(HttpServletRequest request) {

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        GenericResponse gen = new GenericResponse();
        gen.setError("true");
        gen.setCode("400");
        gen.setMessage("invalid number");

        String number = request.getParameter("mobile");
        String code = request.getParameter("countryCode");

        if (code != null) {
            code = code.toUpperCase();
        }

        try {
            PhoneNumber phoneNumber = phoneUtil.parse(number, code);
            boolean isNumberValid = phoneUtil.isValidNumber(phoneNumber);
            //String prettyFormat = isNumberValid ? phoneUtil.formatInOriginalFormat(phoneNumber, code) : "invalid";
            String internationalFormat = isNumberValid ? phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164) : "invalid";

            if (isNumberValid) {
                gen.setError("false");
                gen.setCode("200");
                gen.setMessage(internationalFormat);
            }
        } catch (Exception e) {
            log.error("Error on verify number", e);
        }

        return ResponseEntity.ok(gen);
    }

}
