/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.ClientDevice;
import com.wings.voip.domain.sw.repos.ClientDeviceRepository;

import com.wings.voip.utils.PageWrapper;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class DevicesController {

    @Autowired
    private ClientDeviceRepository clientDeviceRepository;

    @RequestMapping(value = "/views/devices", method = RequestMethod.GET)
    public String getClients(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "id"));

        Page<ClientDevice> clientDevices = clientDeviceRepository.findAll(pr);

        PageWrapper<ClientDevice> pageWrapper = new PageWrapper<>(clientDevices, "/views/devices");

        model.addAttribute("list", clientDevices);
        model.addAttribute("page", pageWrapper);
        return "html/devices";
    }

}
