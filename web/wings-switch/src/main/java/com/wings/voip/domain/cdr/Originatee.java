/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Originatee {

    private Caller_profile originatee_caller_profile;

    public Caller_profile getOriginatee_caller_profile() {
        return originatee_caller_profile;
    }

    public void setOriginatee_caller_profile(Caller_profile originatee_caller_profile) {
        this.originatee_caller_profile = originatee_caller_profile;
    }

}
