/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.specs;

import com.wings.voip.domain.sw.entities.Client;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class ClientsSpecs {

    public static Specification<Client> search(String q) {

        return (Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            //Join<ClientDevice, Client> device = root.join("service");
            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(builder.equal(root.get("login"), search));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
    
    public static Specification<Client> searchByLoginAndDates(String login, Date dateFrom, Date dateTo) {

        return (Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (login != null) {
                String search = "%" + login + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("login"), search)
                        )
                );
            }
            
            if (dateFrom != null) {
                predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("creationDate"), dateFrom));
            }

            if (dateTo != null) {
                predicates.add(builder.lessThanOrEqualTo(root.<Date>get("creationDate"), dateTo));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
