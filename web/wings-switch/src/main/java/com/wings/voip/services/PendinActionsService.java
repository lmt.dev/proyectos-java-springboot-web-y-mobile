/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientContact;
import com.wings.voip.domain.sw.entities.ClientDevice;
import com.wings.voip.domain.sw.entities.ClientPendingAction;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientDeviceRepository;
import com.wings.voip.domain.sw.repos.ClientPendingActionRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PendinActionsService {

    @Autowired
    private ClientPendingActionRepository clientPendingActionRepository;

    @Autowired
    private ClientContactRepository clientContactRepository;

    @Autowired
    private ClientDeviceRepository clientDeviceRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PushService pushService;

    /**
     *
     */
    @Scheduled(initialDelay = 1000, fixedRate = 30000)
    public void sendPendingActions() {

        List<String> pushToSend = new ArrayList<>();
        List<ClientPendingAction> pending = clientPendingActionRepository.findByCompletedAndDisabled(Boolean.FALSE, Boolean.FALSE);

        Date now = new Date();

        for (ClientPendingAction clientPendingAction : pending) {

            //if the client buy secure plan, send to all contacts to update
            //contact list
            if (clientPendingAction.getAction().equals("secure-buy") || clientPendingAction.getAction().equals("register")) {

                //save to prevent doing it again
                clientPendingAction.setCompleted(true);
                clientPendingAction.setCompletionDate(now);
                clientPendingActionRepository.save(clientPendingAction);

                List<ClientPendingAction> toSave = new ArrayList<>();
                List<ClientContact> contacts = clientContactRepository.findByClientId(clientPendingAction.getClientId());

                contacts.stream().filter((contact) -> (contact.isSecure() || contact.isFree())).map((contact) -> {
                    ClientPendingAction newAction = new ClientPendingAction();
                    newAction.setAction("update-contacts");
                    newAction.setClientId(null);
                    newAction.setCompleted(false);
                    newAction.setCompletionDate(null);
                    newAction.setCreationDate(now);
                    newAction.setDisabled(false);
                    newAction.setFailureCount(0);
                    newAction.setLogin(contact.getNumber());
                    return newAction;
                }).map((newAction) -> {
                    newAction.setSendCount(0);
                    return newAction;
                }).forEachOrdered((newAction) -> {
                    toSave.add(newAction);
                });
                clientPendingActionRepository.save(toSave);

            } else if (clientPendingAction.getAction().equals("update-contacts")) {

                //find token
                if (clientPendingAction.getPushToken() == null) {
                    Client client = clientRepository.findByLogin(clientPendingAction.getLogin());
                    ClientDevice device = clientDeviceRepository.findFirstByClientIdAndActive(client.getId(), Boolean.TRUE);
                    clientPendingAction.setPushToken(device.getPushToken());
                }
                clientPendingAction.setSendCount(clientPendingAction.getSendCount() + 1);
                
                if(clientPendingAction.getSendCount() == 1){
                    pushToSend.add(clientPendingAction.getPushToken());
                }else if(clientPendingAction.getSendCount() % 5 == 0){
                    pushToSend.add(clientPendingAction.getPushToken());                
                }else if(clientPendingAction.getSendCount() >= 15){
                    clientPendingAction.setDisabled(true);
                }
                
            }
        }
        
        clientPendingActionRepository.save(pending);

        //send push if theres any
        if (!pushToSend.isEmpty()) {
            pushService.sendUpdateContactsPush(pushToSend);
        }
    }

    /**
     *
     * @param action
     * @param login
     */
    public void markAsCompleted(String action, String login) {
        Date now = new Date();
        List<ClientPendingAction> pendings = clientPendingActionRepository.findByLoginAndCompletedAndAction(login, Boolean.FALSE, action);
        pendings.stream().map((pending) -> {
            pending.setCompleted(true);
            return pending;
        }).forEachOrdered((pending) -> {
            pending.setCompletionDate(now);
        });
        clientPendingActionRepository.save(pendings);

    }

}
