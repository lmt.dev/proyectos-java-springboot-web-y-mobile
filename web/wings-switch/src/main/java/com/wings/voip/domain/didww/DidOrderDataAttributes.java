/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

import java.util.List;

/**
 *
 * @author seba
 */
public class DidOrderDataAttributes {

    private Boolean allow_back_ordering;
    private List<DidOrderDataAttributesItem> items;

    public void setAllow_back_ordering(Boolean allow_back_ordering) {
        this.allow_back_ordering = allow_back_ordering;
    }

    public Boolean getAllow_back_ordering() {
        return allow_back_ordering;
    }

    public void setItems(List<DidOrderDataAttributesItem> items) {
        this.items = items;
    }

    public List<DidOrderDataAttributesItem> getItems() {
        return items;
    }

}
