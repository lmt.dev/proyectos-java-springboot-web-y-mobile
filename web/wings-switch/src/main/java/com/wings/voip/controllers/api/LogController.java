/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Controller
public class LogController {
    
    private final Logger log = LoggerFactory.getLogger(LogController.class);

    /**
     *
     * @param request
     * @param file
     * @return
     */
    @RequestMapping(value = {"/api/logs/upload"}, method = {RequestMethod.POST})
    public ResponseEntity<String> handleFileUpload(HttpServletRequest request, @RequestParam(value = "File", required = false) MultipartFile file) {
        
        if (file == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        
        String fileName = "/var/wings/switch/logs/" + System.currentTimeMillis() + "_" + file.getOriginalFilename();
        try {
            log.error("Upload file with name " + file.getOriginalFilename());
            try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fileName))) {
                stream.write(file.getBytes());
            }
        } catch (Exception e) {
            log.error("Error on handle file upload", e);
        }
        
        return ResponseEntity.ok("ok");
    }
    
}
