/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Variables {

    private String direction;
    private String uuid;
    private String session_id;
    private String sip_from_user;
    private String sip_from_uri;
    private String sip_from_host;
    private String video_media_flow;
    private String audio_media_flow;
    private String channel_name;
    private String ep_codec_string;
    private String sip_local_network_addr;
    private String sip_network_ip;
    private String sip_network_port;
    private String sip_invite_stamp;
    private String sip_received_ip;
    private String sip_received_port;
    private String sip_via_protocol;
    private String sip_authorized;
    private String sip_number_alias;
    private String sip_auth_username;
    private String sip_auth_realm;
    private String number_alias;
    private String requested_user_name;
    private String requested_domain_name;
    private String user_name;
    private String domain_name;
    private String sip_from_user_stripped;
    private String sofia_profile_name;
    private String recovery_profile_name;
    private String sip_allow;
    private String sip_req_user;
    private String sip_req_uri;
    private String sip_req_host;
    private String sip_to_user;
    private String sip_to_uri;
    private String sip_to_host;
    private String sip_contact_params;
    private String sip_contact_user;
    private String sip_contact_port;
    private String sip_contact_uri;
    private String sip_contact_host;
    private String rtp_use_codec_string;
    private String sip_via_host;
    private String sip_via_port;
    private String sip_via_rport;
    private String max_forwards;
    private String presence_id;
    private String switch_r_sdp;
    private String call_uuid;
    private String ringback;
    private String zrtp_secure_media;
    private String hold_music;
    private String proxy_media;
    private String call_timeout;
    private String hangup_after_bridge;
    private String continue_on_fail;
    private String limit_ignore_transfer;
    private String limit_usage;
    private String limit_usage_domain1_5493512328395;
    private String limit_rate;
    private String limit_rate_domain1_5493512328395;
    private String current_application_data;
    private String current_application;
    private String originated_legs;
    private String switch_m_sdp;
    private String rtp_local_sdp_str;
    private String rtp_use_codec_name;
    private String rtp_use_codec_rate;
    private String rtp_use_codec_ptime;
    private String rtp_use_codec_channels;
    private String rtp_last_audio_codec_string;
    private String read_codec;
    private String original_read_codec;
    private String read_rate;
    private String original_read_rate;
    private String write_codec;
    private String write_rate;
    private String local_media_ip;
    private String local_media_port;
    private String advertised_media_ip;
    private String rtp_use_pt;
    private String rtp_use_ssrc;
    private String rtp_2833_send_payload;
    private String rtp_2833_recv_payload;
    private String remote_media_ip;
    private String remote_media_port;
    private String sip_nat_detected;
    private String endpoint_disposition;
    private String originate_disposition;
    private String DIALSTATUS;
    private String originate_causes;
    private String last_bridge_to;
    private String bridge_channel;
    private String bridge_uuid;
    private String signal_bond;
    private String sip_to_tag;
    private String sip_from_tag;
    private String sip_cseq;
    private String sip_call_id;
    private String sip_full_via;
    private String sip_full_from;
    private String sip_full_to;
    private String last_sent_callee_id_name;
    private String last_sent_callee_id_number;
    private String remote_audio_ip_reported;
    private String remote_audio_ip;
    private String remote_audio_port_reported;
    private String remote_audio_port;
    private String rtp_auto_adjust_audio;
    private String sip_term_status;
    private String proto_specific_hangup_cause;
    private String sip_term_cause;
    private String last_bridge_role;
    private String sip_user_agent;
    private String sip_hangup_disposition;
    private String bridge_hangup_cause;
    private String hangup_cause;
    private String hangup_cause_q850;
    private String digits_dialed;
    private String start_stamp;
    private String profile_start_stamp;
    private String answer_stamp;
    private String bridge_stamp;
    private String progress_stamp;
    private String end_stamp;
    private String start_epoch;
    private String start_uepoch;
    private String profile_start_epoch;
    private String profile_start_uepoch;
    private String answer_epoch;
    private String answer_uepoch;
    private String bridge_epoch;
    private String bridge_uepoch;
    private String last_hold_epoch;
    private String last_hold_uepoch;
    private String hold_accum_seconds;
    private String hold_accum_usec;
    private String hold_accum_ms;
    private String resurrect_epoch;
    private String resurrect_uepoch;
    private String progress_epoch;
    private String progress_uepoch;
    private String progress_media_epoch;
    private String progress_media_uepoch;
    private String end_epoch;
    private String end_uepoch;
    private String last_app;
    private String last_arg;
    private String caller_id;
    private String duration;
    private String billsec;
    private String progresssec;
    private String answersec;
    private String waitsec;
    private String progress_mediasec;
    private String flow_billsec;
    private String mduration;
    private String billmsec;
    private String progressmsec;
    private String answermsec;
    private String waitmsec;
    private String progress_mediamsec;
    private String flow_billmsec;
    private String uduration;
    private String billusec;
    private String progressusec;
    private String answerusec;
    private String waitusec;
    private String progress_mediausec;
    private String flow_billusec;
    private String rtp_audio_in_raw_bytes;
    private String rtp_audio_in_media_bytes;
    private String rtp_audio_in_packet_count;
    private String rtp_audio_in_media_packet_count;
    private String rtp_audio_in_skip_packet_count;
    private String rtp_audio_in_jitter_packet_count;
    private String rtp_audio_in_dtmf_packet_count;
    private String rtp_audio_in_cng_packet_count;
    private String rtp_audio_in_flush_packet_count;
    private String rtp_audio_in_largest_jb_size;
    private String rtp_audio_in_jitter_min_variance;
    private String rtp_audio_in_jitter_max_variance;
    private String rtp_audio_in_jitter_loss_rate;
    private String rtp_audio_in_jitter_burst_rate;
    private String rtp_audio_in_mean_interval;
    private String rtp_audio_in_flaw_total;
    private String rtp_audio_in_quality_percentage;
    private String rtp_audio_in_mos;
    private String rtp_audio_out_raw_bytes;
    private String rtp_audio_out_media_bytes;
    private String rtp_audio_out_packet_count;
    private String rtp_audio_out_media_packet_count;
    private String rtp_audio_out_skip_packet_count;
    private String rtp_audio_out_dtmf_packet_count;
    private String rtp_audio_out_cng_packet_count;
    private String rtp_audio_rtcp_packet_count;
    private String rtp_audio_rtcp_octet_count;
    private String zrtp_passthru_active;
    private String wings_dest_id;
    private String wings_service_id;
    private String wings_client_service_id;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getSip_from_user() {
        return sip_from_user;
    }

    public void setSip_from_user(String sip_from_user) {
        this.sip_from_user = sip_from_user;
    }

    public String getSip_from_uri() {
        return sip_from_uri;
    }

    public void setSip_from_uri(String sip_from_uri) {
        this.sip_from_uri = sip_from_uri;
    }

    public String getSip_from_host() {
        return sip_from_host;
    }

    public void setSip_from_host(String sip_from_host) {
        this.sip_from_host = sip_from_host;
    }

    public String getVideo_media_flow() {
        return video_media_flow;
    }

    public void setVideo_media_flow(String video_media_flow) {
        this.video_media_flow = video_media_flow;
    }

    public String getAudio_media_flow() {
        return audio_media_flow;
    }

    public void setAudio_media_flow(String audio_media_flow) {
        this.audio_media_flow = audio_media_flow;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }

    public String getEp_codec_string() {
        return ep_codec_string;
    }

    public void setEp_codec_string(String ep_codec_string) {
        this.ep_codec_string = ep_codec_string;
    }

    public String getSip_local_network_addr() {
        return sip_local_network_addr;
    }

    public void setSip_local_network_addr(String sip_local_network_addr) {
        this.sip_local_network_addr = sip_local_network_addr;
    }

    public String getSip_network_ip() {
        return sip_network_ip;
    }

    public void setSip_network_ip(String sip_network_ip) {
        this.sip_network_ip = sip_network_ip;
    }

    public String getSip_network_port() {
        return sip_network_port;
    }

    public void setSip_network_port(String sip_network_port) {
        this.sip_network_port = sip_network_port;
    }

    public String getSip_invite_stamp() {
        return sip_invite_stamp;
    }

    public void setSip_invite_stamp(String sip_invite_stamp) {
        this.sip_invite_stamp = sip_invite_stamp;
    }

    public String getSip_received_ip() {
        return sip_received_ip;
    }

    public void setSip_received_ip(String sip_received_ip) {
        this.sip_received_ip = sip_received_ip;
    }

    public String getSip_received_port() {
        return sip_received_port;
    }

    public void setSip_received_port(String sip_received_port) {
        this.sip_received_port = sip_received_port;
    }

    public String getSip_via_protocol() {
        return sip_via_protocol;
    }

    public void setSip_via_protocol(String sip_via_protocol) {
        this.sip_via_protocol = sip_via_protocol;
    }

    public String getSip_authorized() {
        return sip_authorized;
    }

    public void setSip_authorized(String sip_authorized) {
        this.sip_authorized = sip_authorized;
    }

    public String getSip_number_alias() {
        return sip_number_alias;
    }

    public void setSip_number_alias(String sip_number_alias) {
        this.sip_number_alias = sip_number_alias;
    }

    public String getSip_auth_username() {
        return sip_auth_username;
    }

    public void setSip_auth_username(String sip_auth_username) {
        this.sip_auth_username = sip_auth_username;
    }

    public String getSip_auth_realm() {
        return sip_auth_realm;
    }

    public void setSip_auth_realm(String sip_auth_realm) {
        this.sip_auth_realm = sip_auth_realm;
    }

    public String getNumber_alias() {
        return number_alias;
    }

    public void setNumber_alias(String number_alias) {
        this.number_alias = number_alias;
    }

    public String getRequested_user_name() {
        return requested_user_name;
    }

    public void setRequested_user_name(String requested_user_name) {
        this.requested_user_name = requested_user_name;
    }

    public String getRequested_domain_name() {
        return requested_domain_name;
    }

    public void setRequested_domain_name(String requested_domain_name) {
        this.requested_domain_name = requested_domain_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDomain_name() {
        return domain_name;
    }

    public void setDomain_name(String domain_name) {
        this.domain_name = domain_name;
    }

    public String getSip_from_user_stripped() {
        return sip_from_user_stripped;
    }

    public void setSip_from_user_stripped(String sip_from_user_stripped) {
        this.sip_from_user_stripped = sip_from_user_stripped;
    }

    public String getSofia_profile_name() {
        return sofia_profile_name;
    }

    public void setSofia_profile_name(String sofia_profile_name) {
        this.sofia_profile_name = sofia_profile_name;
    }

    public String getRecovery_profile_name() {
        return recovery_profile_name;
    }

    public void setRecovery_profile_name(String recovery_profile_name) {
        this.recovery_profile_name = recovery_profile_name;
    }

    public String getSip_allow() {
        return sip_allow;
    }

    public void setSip_allow(String sip_allow) {
        this.sip_allow = sip_allow;
    }

    public String getSip_req_user() {
        return sip_req_user;
    }

    public void setSip_req_user(String sip_req_user) {
        this.sip_req_user = sip_req_user;
    }

    public String getSip_req_uri() {
        return sip_req_uri;
    }

    public void setSip_req_uri(String sip_req_uri) {
        this.sip_req_uri = sip_req_uri;
    }

    public String getSip_req_host() {
        return sip_req_host;
    }

    public void setSip_req_host(String sip_req_host) {
        this.sip_req_host = sip_req_host;
    }

    public String getSip_to_user() {
        return sip_to_user;
    }

    public void setSip_to_user(String sip_to_user) {
        this.sip_to_user = sip_to_user;
    }

    public String getSip_to_uri() {
        return sip_to_uri;
    }

    public void setSip_to_uri(String sip_to_uri) {
        this.sip_to_uri = sip_to_uri;
    }

    public String getSip_to_host() {
        return sip_to_host;
    }

    public void setSip_to_host(String sip_to_host) {
        this.sip_to_host = sip_to_host;
    }

    public String getSip_contact_params() {
        return sip_contact_params;
    }

    public void setSip_contact_params(String sip_contact_params) {
        this.sip_contact_params = sip_contact_params;
    }

    public String getSip_contact_user() {
        return sip_contact_user;
    }

    public void setSip_contact_user(String sip_contact_user) {
        this.sip_contact_user = sip_contact_user;
    }

    public String getSip_contact_port() {
        return sip_contact_port;
    }

    public void setSip_contact_port(String sip_contact_port) {
        this.sip_contact_port = sip_contact_port;
    }

    public String getSip_contact_uri() {
        return sip_contact_uri;
    }

    public void setSip_contact_uri(String sip_contact_uri) {
        this.sip_contact_uri = sip_contact_uri;
    }

    public String getSip_contact_host() {
        return sip_contact_host;
    }

    public void setSip_contact_host(String sip_contact_host) {
        this.sip_contact_host = sip_contact_host;
    }

    public String getRtp_use_codec_string() {
        return rtp_use_codec_string;
    }

    public void setRtp_use_codec_string(String rtp_use_codec_string) {
        this.rtp_use_codec_string = rtp_use_codec_string;
    }

    public String getSip_via_host() {
        return sip_via_host;
    }

    public void setSip_via_host(String sip_via_host) {
        this.sip_via_host = sip_via_host;
    }

    public String getSip_via_port() {
        return sip_via_port;
    }

    public void setSip_via_port(String sip_via_port) {
        this.sip_via_port = sip_via_port;
    }

    public String getSip_via_rport() {
        return sip_via_rport;
    }

    public void setSip_via_rport(String sip_via_rport) {
        this.sip_via_rport = sip_via_rport;
    }

    public String getMax_forwards() {
        return max_forwards;
    }

    public void setMax_forwards(String max_forwards) {
        this.max_forwards = max_forwards;
    }

    public String getPresence_id() {
        return presence_id;
    }

    public void setPresence_id(String presence_id) {
        this.presence_id = presence_id;
    }

    public String getSwitch_r_sdp() {
        return switch_r_sdp;
    }

    public void setSwitch_r_sdp(String switch_r_sdp) {
        this.switch_r_sdp = switch_r_sdp;
    }

    public String getCall_uuid() {
        return call_uuid;
    }

    public void setCall_uuid(String call_uuid) {
        this.call_uuid = call_uuid;
    }

    public String getRingback() {
        return ringback;
    }

    public void setRingback(String ringback) {
        this.ringback = ringback;
    }

    public String getZrtp_secure_media() {
        return zrtp_secure_media;
    }

    public void setZrtp_secure_media(String zrtp_secure_media) {
        this.zrtp_secure_media = zrtp_secure_media;
    }

    public String getHold_music() {
        return hold_music;
    }

    public void setHold_music(String hold_music) {
        this.hold_music = hold_music;
    }

    public String getProxy_media() {
        return proxy_media;
    }

    public void setProxy_media(String proxy_media) {
        this.proxy_media = proxy_media;
    }

    public String getCall_timeout() {
        return call_timeout;
    }

    public void setCall_timeout(String call_timeout) {
        this.call_timeout = call_timeout;
    }

    public String getHangup_after_bridge() {
        return hangup_after_bridge;
    }

    public void setHangup_after_bridge(String hangup_after_bridge) {
        this.hangup_after_bridge = hangup_after_bridge;
    }

    public String getContinue_on_fail() {
        return continue_on_fail;
    }

    public void setContinue_on_fail(String continue_on_fail) {
        this.continue_on_fail = continue_on_fail;
    }

    public String getLimit_ignore_transfer() {
        return limit_ignore_transfer;
    }

    public void setLimit_ignore_transfer(String limit_ignore_transfer) {
        this.limit_ignore_transfer = limit_ignore_transfer;
    }

    public String getLimit_usage() {
        return limit_usage;
    }

    public void setLimit_usage(String limit_usage) {
        this.limit_usage = limit_usage;
    }

    public String getLimit_usage_domain1_5493512328395() {
        return limit_usage_domain1_5493512328395;
    }

    public void setLimit_usage_domain1_5493512328395(String limit_usage_domain1_5493512328395) {
        this.limit_usage_domain1_5493512328395 = limit_usage_domain1_5493512328395;
    }

    public String getLimit_rate() {
        return limit_rate;
    }

    public void setLimit_rate(String limit_rate) {
        this.limit_rate = limit_rate;
    }

    public String getLimit_rate_domain1_5493512328395() {
        return limit_rate_domain1_5493512328395;
    }

    public void setLimit_rate_domain1_5493512328395(String limit_rate_domain1_5493512328395) {
        this.limit_rate_domain1_5493512328395 = limit_rate_domain1_5493512328395;
    }

    public String getCurrent_application_data() {
        return current_application_data;
    }

    public void setCurrent_application_data(String current_application_data) {
        this.current_application_data = current_application_data;
    }

    public String getCurrent_application() {
        return current_application;
    }

    public void setCurrent_application(String current_application) {
        this.current_application = current_application;
    }

    public String getOriginated_legs() {
        return originated_legs;
    }

    public void setOriginated_legs(String originated_legs) {
        this.originated_legs = originated_legs;
    }

    public String getSwitch_m_sdp() {
        return switch_m_sdp;
    }

    public void setSwitch_m_sdp(String switch_m_sdp) {
        this.switch_m_sdp = switch_m_sdp;
    }

    public String getRtp_local_sdp_str() {
        return rtp_local_sdp_str;
    }

    public void setRtp_local_sdp_str(String rtp_local_sdp_str) {
        this.rtp_local_sdp_str = rtp_local_sdp_str;
    }

    public String getRtp_use_codec_name() {
        return rtp_use_codec_name;
    }

    public void setRtp_use_codec_name(String rtp_use_codec_name) {
        this.rtp_use_codec_name = rtp_use_codec_name;
    }

    public String getRtp_use_codec_rate() {
        return rtp_use_codec_rate;
    }

    public void setRtp_use_codec_rate(String rtp_use_codec_rate) {
        this.rtp_use_codec_rate = rtp_use_codec_rate;
    }

    public String getRtp_use_codec_ptime() {
        return rtp_use_codec_ptime;
    }

    public void setRtp_use_codec_ptime(String rtp_use_codec_ptime) {
        this.rtp_use_codec_ptime = rtp_use_codec_ptime;
    }

    public String getRtp_use_codec_channels() {
        return rtp_use_codec_channels;
    }

    public void setRtp_use_codec_channels(String rtp_use_codec_channels) {
        this.rtp_use_codec_channels = rtp_use_codec_channels;
    }

    public String getRtp_last_audio_codec_string() {
        return rtp_last_audio_codec_string;
    }

    public void setRtp_last_audio_codec_string(String rtp_last_audio_codec_string) {
        this.rtp_last_audio_codec_string = rtp_last_audio_codec_string;
    }

    public String getRead_codec() {
        return read_codec;
    }

    public void setRead_codec(String read_codec) {
        this.read_codec = read_codec;
    }

    public String getOriginal_read_codec() {
        return original_read_codec;
    }

    public void setOriginal_read_codec(String original_read_codec) {
        this.original_read_codec = original_read_codec;
    }

    public String getRead_rate() {
        return read_rate;
    }

    public void setRead_rate(String read_rate) {
        this.read_rate = read_rate;
    }

    public String getOriginal_read_rate() {
        return original_read_rate;
    }

    public void setOriginal_read_rate(String original_read_rate) {
        this.original_read_rate = original_read_rate;
    }

    public String getWrite_codec() {
        return write_codec;
    }

    public void setWrite_codec(String write_codec) {
        this.write_codec = write_codec;
    }

    public String getWrite_rate() {
        return write_rate;
    }

    public void setWrite_rate(String write_rate) {
        this.write_rate = write_rate;
    }

    public String getLocal_media_ip() {
        return local_media_ip;
    }

    public void setLocal_media_ip(String local_media_ip) {
        this.local_media_ip = local_media_ip;
    }

    public String getLocal_media_port() {
        return local_media_port;
    }

    public void setLocal_media_port(String local_media_port) {
        this.local_media_port = local_media_port;
    }

    public String getAdvertised_media_ip() {
        return advertised_media_ip;
    }

    public void setAdvertised_media_ip(String advertised_media_ip) {
        this.advertised_media_ip = advertised_media_ip;
    }

    public String getRtp_use_pt() {
        return rtp_use_pt;
    }

    public void setRtp_use_pt(String rtp_use_pt) {
        this.rtp_use_pt = rtp_use_pt;
    }

    public String getRtp_use_ssrc() {
        return rtp_use_ssrc;
    }

    public void setRtp_use_ssrc(String rtp_use_ssrc) {
        this.rtp_use_ssrc = rtp_use_ssrc;
    }

    public String getRtp_2833_send_payload() {
        return rtp_2833_send_payload;
    }

    public void setRtp_2833_send_payload(String rtp_2833_send_payload) {
        this.rtp_2833_send_payload = rtp_2833_send_payload;
    }

    public String getRtp_2833_recv_payload() {
        return rtp_2833_recv_payload;
    }

    public void setRtp_2833_recv_payload(String rtp_2833_recv_payload) {
        this.rtp_2833_recv_payload = rtp_2833_recv_payload;
    }

    public String getRemote_media_ip() {
        return remote_media_ip;
    }

    public void setRemote_media_ip(String remote_media_ip) {
        this.remote_media_ip = remote_media_ip;
    }

    public String getRemote_media_port() {
        return remote_media_port;
    }

    public void setRemote_media_port(String remote_media_port) {
        this.remote_media_port = remote_media_port;
    }

    public String getSip_nat_detected() {
        return sip_nat_detected;
    }

    public void setSip_nat_detected(String sip_nat_detected) {
        this.sip_nat_detected = sip_nat_detected;
    }

    public String getEndpoint_disposition() {
        return endpoint_disposition;
    }

    public void setEndpoint_disposition(String endpoint_disposition) {
        this.endpoint_disposition = endpoint_disposition;
    }

    public String getOriginate_disposition() {
        return originate_disposition;
    }

    public void setOriginate_disposition(String originate_disposition) {
        this.originate_disposition = originate_disposition;
    }

    public String getDIALSTATUS() {
        return DIALSTATUS;
    }

    public void setDIALSTATUS(String DIALSTATUS) {
        this.DIALSTATUS = DIALSTATUS;
    }

    public String getOriginate_causes() {
        return originate_causes;
    }

    public void setOriginate_causes(String originate_causes) {
        this.originate_causes = originate_causes;
    }

    public String getLast_bridge_to() {
        return last_bridge_to;
    }

    public void setLast_bridge_to(String last_bridge_to) {
        this.last_bridge_to = last_bridge_to;
    }

    public String getBridge_channel() {
        return bridge_channel;
    }

    public void setBridge_channel(String bridge_channel) {
        this.bridge_channel = bridge_channel;
    }

    public String getBridge_uuid() {
        return bridge_uuid;
    }

    public void setBridge_uuid(String bridge_uuid) {
        this.bridge_uuid = bridge_uuid;
    }

    public String getSignal_bond() {
        return signal_bond;
    }

    public void setSignal_bond(String signal_bond) {
        this.signal_bond = signal_bond;
    }

    public String getSip_to_tag() {
        return sip_to_tag;
    }

    public void setSip_to_tag(String sip_to_tag) {
        this.sip_to_tag = sip_to_tag;
    }

    public String getSip_from_tag() {
        return sip_from_tag;
    }

    public void setSip_from_tag(String sip_from_tag) {
        this.sip_from_tag = sip_from_tag;
    }

    public String getSip_cseq() {
        return sip_cseq;
    }

    public void setSip_cseq(String sip_cseq) {
        this.sip_cseq = sip_cseq;
    }

    public String getSip_call_id() {
        return sip_call_id;
    }

    public void setSip_call_id(String sip_call_id) {
        this.sip_call_id = sip_call_id;
    }

    public String getSip_full_via() {
        return sip_full_via;
    }

    public void setSip_full_via(String sip_full_via) {
        this.sip_full_via = sip_full_via;
    }

    public String getSip_full_from() {
        return sip_full_from;
    }

    public void setSip_full_from(String sip_full_from) {
        this.sip_full_from = sip_full_from;
    }

    public String getSip_full_to() {
        return sip_full_to;
    }

    public void setSip_full_to(String sip_full_to) {
        this.sip_full_to = sip_full_to;
    }

    public String getLast_sent_callee_id_name() {
        return last_sent_callee_id_name;
    }

    public void setLast_sent_callee_id_name(String last_sent_callee_id_name) {
        this.last_sent_callee_id_name = last_sent_callee_id_name;
    }

    public String getLast_sent_callee_id_number() {
        return last_sent_callee_id_number;
    }

    public void setLast_sent_callee_id_number(String last_sent_callee_id_number) {
        this.last_sent_callee_id_number = last_sent_callee_id_number;
    }

    public String getRemote_audio_ip_reported() {
        return remote_audio_ip_reported;
    }

    public void setRemote_audio_ip_reported(String remote_audio_ip_reported) {
        this.remote_audio_ip_reported = remote_audio_ip_reported;
    }

    public String getRemote_audio_ip() {
        return remote_audio_ip;
    }

    public void setRemote_audio_ip(String remote_audio_ip) {
        this.remote_audio_ip = remote_audio_ip;
    }

    public String getRemote_audio_port_reported() {
        return remote_audio_port_reported;
    }

    public void setRemote_audio_port_reported(String remote_audio_port_reported) {
        this.remote_audio_port_reported = remote_audio_port_reported;
    }

    public String getRemote_audio_port() {
        return remote_audio_port;
    }

    public void setRemote_audio_port(String remote_audio_port) {
        this.remote_audio_port = remote_audio_port;
    }

    public String getRtp_auto_adjust_audio() {
        return rtp_auto_adjust_audio;
    }

    public void setRtp_auto_adjust_audio(String rtp_auto_adjust_audio) {
        this.rtp_auto_adjust_audio = rtp_auto_adjust_audio;
    }

    public String getSip_term_status() {
        return sip_term_status;
    }

    public void setSip_term_status(String sip_term_status) {
        this.sip_term_status = sip_term_status;
    }

    public String getProto_specific_hangup_cause() {
        return proto_specific_hangup_cause;
    }

    public void setProto_specific_hangup_cause(String proto_specific_hangup_cause) {
        this.proto_specific_hangup_cause = proto_specific_hangup_cause;
    }

    public String getSip_term_cause() {
        return sip_term_cause;
    }

    public void setSip_term_cause(String sip_term_cause) {
        this.sip_term_cause = sip_term_cause;
    }

    public String getLast_bridge_role() {
        return last_bridge_role;
    }

    public void setLast_bridge_role(String last_bridge_role) {
        this.last_bridge_role = last_bridge_role;
    }

    public String getSip_user_agent() {
        return sip_user_agent;
    }

    public void setSip_user_agent(String sip_user_agent) {
        this.sip_user_agent = sip_user_agent;
    }

    public String getSip_hangup_disposition() {
        return sip_hangup_disposition;
    }

    public void setSip_hangup_disposition(String sip_hangup_disposition) {
        this.sip_hangup_disposition = sip_hangup_disposition;
    }

    public String getBridge_hangup_cause() {
        return bridge_hangup_cause;
    }

    public void setBridge_hangup_cause(String bridge_hangup_cause) {
        this.bridge_hangup_cause = bridge_hangup_cause;
    }

    public String getHangup_cause() {
        return hangup_cause;
    }

    public void setHangup_cause(String hangup_cause) {
        this.hangup_cause = hangup_cause;
    }

    public String getHangup_cause_q850() {
        return hangup_cause_q850;
    }

    public void setHangup_cause_q850(String hangup_cause_q850) {
        this.hangup_cause_q850 = hangup_cause_q850;
    }

    public String getDigits_dialed() {
        return digits_dialed;
    }

    public void setDigits_dialed(String digits_dialed) {
        this.digits_dialed = digits_dialed;
    }

    public String getStart_stamp() {
        return start_stamp;
    }

    public void setStart_stamp(String start_stamp) {
        this.start_stamp = start_stamp;
    }

    public String getProfile_start_stamp() {
        return profile_start_stamp;
    }

    public void setProfile_start_stamp(String profile_start_stamp) {
        this.profile_start_stamp = profile_start_stamp;
    }

    public String getAnswer_stamp() {
        return answer_stamp;
    }

    public void setAnswer_stamp(String answer_stamp) {
        this.answer_stamp = answer_stamp;
    }

    public String getBridge_stamp() {
        return bridge_stamp;
    }

    public void setBridge_stamp(String bridge_stamp) {
        this.bridge_stamp = bridge_stamp;
    }

    public String getProgress_stamp() {
        return progress_stamp;
    }

    public void setProgress_stamp(String progress_stamp) {
        this.progress_stamp = progress_stamp;
    }

    public String getEnd_stamp() {
        return end_stamp;
    }

    public void setEnd_stamp(String end_stamp) {
        this.end_stamp = end_stamp;
    }

    public String getStart_epoch() {
        return start_epoch;
    }

    public void setStart_epoch(String start_epoch) {
        this.start_epoch = start_epoch;
    }

    public String getStart_uepoch() {
        return start_uepoch;
    }

    public void setStart_uepoch(String start_uepoch) {
        this.start_uepoch = start_uepoch;
    }

    public String getProfile_start_epoch() {
        return profile_start_epoch;
    }

    public void setProfile_start_epoch(String profile_start_epoch) {
        this.profile_start_epoch = profile_start_epoch;
    }

    public String getProfile_start_uepoch() {
        return profile_start_uepoch;
    }

    public void setProfile_start_uepoch(String profile_start_uepoch) {
        this.profile_start_uepoch = profile_start_uepoch;
    }

    public String getAnswer_epoch() {
        return answer_epoch;
    }

    public void setAnswer_epoch(String answer_epoch) {
        this.answer_epoch = answer_epoch;
    }

    public String getAnswer_uepoch() {
        return answer_uepoch;
    }

    public void setAnswer_uepoch(String answer_uepoch) {
        this.answer_uepoch = answer_uepoch;
    }

    public String getBridge_epoch() {
        return bridge_epoch;
    }

    public void setBridge_epoch(String bridge_epoch) {
        this.bridge_epoch = bridge_epoch;
    }

    public String getBridge_uepoch() {
        return bridge_uepoch;
    }

    public void setBridge_uepoch(String bridge_uepoch) {
        this.bridge_uepoch = bridge_uepoch;
    }

    public String getLast_hold_epoch() {
        return last_hold_epoch;
    }

    public void setLast_hold_epoch(String last_hold_epoch) {
        this.last_hold_epoch = last_hold_epoch;
    }

    public String getLast_hold_uepoch() {
        return last_hold_uepoch;
    }

    public void setLast_hold_uepoch(String last_hold_uepoch) {
        this.last_hold_uepoch = last_hold_uepoch;
    }

    public String getHold_accum_seconds() {
        return hold_accum_seconds;
    }

    public void setHold_accum_seconds(String hold_accum_seconds) {
        this.hold_accum_seconds = hold_accum_seconds;
    }

    public String getHold_accum_usec() {
        return hold_accum_usec;
    }

    public void setHold_accum_usec(String hold_accum_usec) {
        this.hold_accum_usec = hold_accum_usec;
    }

    public String getHold_accum_ms() {
        return hold_accum_ms;
    }

    public void setHold_accum_ms(String hold_accum_ms) {
        this.hold_accum_ms = hold_accum_ms;
    }

    public String getResurrect_epoch() {
        return resurrect_epoch;
    }

    public void setResurrect_epoch(String resurrect_epoch) {
        this.resurrect_epoch = resurrect_epoch;
    }

    public String getResurrect_uepoch() {
        return resurrect_uepoch;
    }

    public void setResurrect_uepoch(String resurrect_uepoch) {
        this.resurrect_uepoch = resurrect_uepoch;
    }

    public String getProgress_epoch() {
        return progress_epoch;
    }

    public void setProgress_epoch(String progress_epoch) {
        this.progress_epoch = progress_epoch;
    }

    public String getProgress_uepoch() {
        return progress_uepoch;
    }

    public void setProgress_uepoch(String progress_uepoch) {
        this.progress_uepoch = progress_uepoch;
    }

    public String getProgress_media_epoch() {
        return progress_media_epoch;
    }

    public void setProgress_media_epoch(String progress_media_epoch) {
        this.progress_media_epoch = progress_media_epoch;
    }

    public String getProgress_media_uepoch() {
        return progress_media_uepoch;
    }

    public void setProgress_media_uepoch(String progress_media_uepoch) {
        this.progress_media_uepoch = progress_media_uepoch;
    }

    public String getEnd_epoch() {
        return end_epoch;
    }

    public void setEnd_epoch(String end_epoch) {
        this.end_epoch = end_epoch;
    }

    public String getEnd_uepoch() {
        return end_uepoch;
    }

    public void setEnd_uepoch(String end_uepoch) {
        this.end_uepoch = end_uepoch;
    }

    public String getLast_app() {
        return last_app;
    }

    public void setLast_app(String last_app) {
        this.last_app = last_app;
    }

    public String getLast_arg() {
        return last_arg;
    }

    public void setLast_arg(String last_arg) {
        this.last_arg = last_arg;
    }

    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBillsec() {
        return billsec;
    }

    public void setBillsec(String billsec) {
        this.billsec = billsec;
    }

    public String getProgresssec() {
        return progresssec;
    }

    public void setProgresssec(String progresssec) {
        this.progresssec = progresssec;
    }

    public String getAnswersec() {
        return answersec;
    }

    public void setAnswersec(String answersec) {
        this.answersec = answersec;
    }

    public String getWaitsec() {
        return waitsec;
    }

    public void setWaitsec(String waitsec) {
        this.waitsec = waitsec;
    }

    public String getProgress_mediasec() {
        return progress_mediasec;
    }

    public void setProgress_mediasec(String progress_mediasec) {
        this.progress_mediasec = progress_mediasec;
    }

    public String getFlow_billsec() {
        return flow_billsec;
    }

    public void setFlow_billsec(String flow_billsec) {
        this.flow_billsec = flow_billsec;
    }

    public String getMduration() {
        return mduration;
    }

    public void setMduration(String mduration) {
        this.mduration = mduration;
    }

    public String getBillmsec() {
        return billmsec;
    }

    public void setBillmsec(String billmsec) {
        this.billmsec = billmsec;
    }

    public String getProgressmsec() {
        return progressmsec;
    }

    public void setProgressmsec(String progressmsec) {
        this.progressmsec = progressmsec;
    }

    public String getAnswermsec() {
        return answermsec;
    }

    public void setAnswermsec(String answermsec) {
        this.answermsec = answermsec;
    }

    public String getWaitmsec() {
        return waitmsec;
    }

    public void setWaitmsec(String waitmsec) {
        this.waitmsec = waitmsec;
    }

    public String getProgress_mediamsec() {
        return progress_mediamsec;
    }

    public void setProgress_mediamsec(String progress_mediamsec) {
        this.progress_mediamsec = progress_mediamsec;
    }

    public String getFlow_billmsec() {
        return flow_billmsec;
    }

    public void setFlow_billmsec(String flow_billmsec) {
        this.flow_billmsec = flow_billmsec;
    }

    public String getUduration() {
        return uduration;
    }

    public void setUduration(String uduration) {
        this.uduration = uduration;
    }

    public String getBillusec() {
        return billusec;
    }

    public void setBillusec(String billusec) {
        this.billusec = billusec;
    }

    public String getProgressusec() {
        return progressusec;
    }

    public void setProgressusec(String progressusec) {
        this.progressusec = progressusec;
    }

    public String getAnswerusec() {
        return answerusec;
    }

    public void setAnswerusec(String answerusec) {
        this.answerusec = answerusec;
    }

    public String getWaitusec() {
        return waitusec;
    }

    public void setWaitusec(String waitusec) {
        this.waitusec = waitusec;
    }

    public String getProgress_mediausec() {
        return progress_mediausec;
    }

    public void setProgress_mediausec(String progress_mediausec) {
        this.progress_mediausec = progress_mediausec;
    }

    public String getFlow_billusec() {
        return flow_billusec;
    }

    public void setFlow_billusec(String flow_billusec) {
        this.flow_billusec = flow_billusec;
    }

    public String getRtp_audio_in_raw_bytes() {
        return rtp_audio_in_raw_bytes;
    }

    public void setRtp_audio_in_raw_bytes(String rtp_audio_in_raw_bytes) {
        this.rtp_audio_in_raw_bytes = rtp_audio_in_raw_bytes;
    }

    public String getRtp_audio_in_media_bytes() {
        return rtp_audio_in_media_bytes;
    }

    public void setRtp_audio_in_media_bytes(String rtp_audio_in_media_bytes) {
        this.rtp_audio_in_media_bytes = rtp_audio_in_media_bytes;
    }

    public String getRtp_audio_in_packet_count() {
        return rtp_audio_in_packet_count;
    }

    public void setRtp_audio_in_packet_count(String rtp_audio_in_packet_count) {
        this.rtp_audio_in_packet_count = rtp_audio_in_packet_count;
    }

    public String getRtp_audio_in_media_packet_count() {
        return rtp_audio_in_media_packet_count;
    }

    public void setRtp_audio_in_media_packet_count(String rtp_audio_in_media_packet_count) {
        this.rtp_audio_in_media_packet_count = rtp_audio_in_media_packet_count;
    }

    public String getRtp_audio_in_skip_packet_count() {
        return rtp_audio_in_skip_packet_count;
    }

    public void setRtp_audio_in_skip_packet_count(String rtp_audio_in_skip_packet_count) {
        this.rtp_audio_in_skip_packet_count = rtp_audio_in_skip_packet_count;
    }

    public String getRtp_audio_in_jitter_packet_count() {
        return rtp_audio_in_jitter_packet_count;
    }

    public void setRtp_audio_in_jitter_packet_count(String rtp_audio_in_jitter_packet_count) {
        this.rtp_audio_in_jitter_packet_count = rtp_audio_in_jitter_packet_count;
    }

    public String getRtp_audio_in_dtmf_packet_count() {
        return rtp_audio_in_dtmf_packet_count;
    }

    public void setRtp_audio_in_dtmf_packet_count(String rtp_audio_in_dtmf_packet_count) {
        this.rtp_audio_in_dtmf_packet_count = rtp_audio_in_dtmf_packet_count;
    }

    public String getRtp_audio_in_cng_packet_count() {
        return rtp_audio_in_cng_packet_count;
    }

    public void setRtp_audio_in_cng_packet_count(String rtp_audio_in_cng_packet_count) {
        this.rtp_audio_in_cng_packet_count = rtp_audio_in_cng_packet_count;
    }

    public String getRtp_audio_in_flush_packet_count() {
        return rtp_audio_in_flush_packet_count;
    }

    public void setRtp_audio_in_flush_packet_count(String rtp_audio_in_flush_packet_count) {
        this.rtp_audio_in_flush_packet_count = rtp_audio_in_flush_packet_count;
    }

    public String getRtp_audio_in_largest_jb_size() {
        return rtp_audio_in_largest_jb_size;
    }

    public void setRtp_audio_in_largest_jb_size(String rtp_audio_in_largest_jb_size) {
        this.rtp_audio_in_largest_jb_size = rtp_audio_in_largest_jb_size;
    }

    public String getRtp_audio_in_jitter_min_variance() {
        return rtp_audio_in_jitter_min_variance;
    }

    public void setRtp_audio_in_jitter_min_variance(String rtp_audio_in_jitter_min_variance) {
        this.rtp_audio_in_jitter_min_variance = rtp_audio_in_jitter_min_variance;
    }

    public String getRtp_audio_in_jitter_max_variance() {
        return rtp_audio_in_jitter_max_variance;
    }

    public void setRtp_audio_in_jitter_max_variance(String rtp_audio_in_jitter_max_variance) {
        this.rtp_audio_in_jitter_max_variance = rtp_audio_in_jitter_max_variance;
    }

    public String getRtp_audio_in_jitter_loss_rate() {
        return rtp_audio_in_jitter_loss_rate;
    }

    public void setRtp_audio_in_jitter_loss_rate(String rtp_audio_in_jitter_loss_rate) {
        this.rtp_audio_in_jitter_loss_rate = rtp_audio_in_jitter_loss_rate;
    }

    public String getRtp_audio_in_jitter_burst_rate() {
        return rtp_audio_in_jitter_burst_rate;
    }

    public void setRtp_audio_in_jitter_burst_rate(String rtp_audio_in_jitter_burst_rate) {
        this.rtp_audio_in_jitter_burst_rate = rtp_audio_in_jitter_burst_rate;
    }

    public String getRtp_audio_in_mean_interval() {
        return rtp_audio_in_mean_interval;
    }

    public void setRtp_audio_in_mean_interval(String rtp_audio_in_mean_interval) {
        this.rtp_audio_in_mean_interval = rtp_audio_in_mean_interval;
    }

    public String getRtp_audio_in_flaw_total() {
        return rtp_audio_in_flaw_total;
    }

    public void setRtp_audio_in_flaw_total(String rtp_audio_in_flaw_total) {
        this.rtp_audio_in_flaw_total = rtp_audio_in_flaw_total;
    }

    public String getRtp_audio_in_quality_percentage() {
        return rtp_audio_in_quality_percentage;
    }

    public void setRtp_audio_in_quality_percentage(String rtp_audio_in_quality_percentage) {
        this.rtp_audio_in_quality_percentage = rtp_audio_in_quality_percentage;
    }

    public String getRtp_audio_in_mos() {
        return rtp_audio_in_mos;
    }

    public void setRtp_audio_in_mos(String rtp_audio_in_mos) {
        this.rtp_audio_in_mos = rtp_audio_in_mos;
    }

    public String getRtp_audio_out_raw_bytes() {
        return rtp_audio_out_raw_bytes;
    }

    public void setRtp_audio_out_raw_bytes(String rtp_audio_out_raw_bytes) {
        this.rtp_audio_out_raw_bytes = rtp_audio_out_raw_bytes;
    }

    public String getRtp_audio_out_media_bytes() {
        return rtp_audio_out_media_bytes;
    }

    public void setRtp_audio_out_media_bytes(String rtp_audio_out_media_bytes) {
        this.rtp_audio_out_media_bytes = rtp_audio_out_media_bytes;
    }

    public String getRtp_audio_out_packet_count() {
        return rtp_audio_out_packet_count;
    }

    public void setRtp_audio_out_packet_count(String rtp_audio_out_packet_count) {
        this.rtp_audio_out_packet_count = rtp_audio_out_packet_count;
    }

    public String getRtp_audio_out_media_packet_count() {
        return rtp_audio_out_media_packet_count;
    }

    public void setRtp_audio_out_media_packet_count(String rtp_audio_out_media_packet_count) {
        this.rtp_audio_out_media_packet_count = rtp_audio_out_media_packet_count;
    }

    public String getRtp_audio_out_skip_packet_count() {
        return rtp_audio_out_skip_packet_count;
    }

    public void setRtp_audio_out_skip_packet_count(String rtp_audio_out_skip_packet_count) {
        this.rtp_audio_out_skip_packet_count = rtp_audio_out_skip_packet_count;
    }

    public String getRtp_audio_out_dtmf_packet_count() {
        return rtp_audio_out_dtmf_packet_count;
    }

    public void setRtp_audio_out_dtmf_packet_count(String rtp_audio_out_dtmf_packet_count) {
        this.rtp_audio_out_dtmf_packet_count = rtp_audio_out_dtmf_packet_count;
    }

    public String getRtp_audio_out_cng_packet_count() {
        return rtp_audio_out_cng_packet_count;
    }

    public void setRtp_audio_out_cng_packet_count(String rtp_audio_out_cng_packet_count) {
        this.rtp_audio_out_cng_packet_count = rtp_audio_out_cng_packet_count;
    }

    public String getRtp_audio_rtcp_packet_count() {
        return rtp_audio_rtcp_packet_count;
    }

    public void setRtp_audio_rtcp_packet_count(String rtp_audio_rtcp_packet_count) {
        this.rtp_audio_rtcp_packet_count = rtp_audio_rtcp_packet_count;
    }

    public String getRtp_audio_rtcp_octet_count() {
        return rtp_audio_rtcp_octet_count;
    }

    public void setRtp_audio_rtcp_octet_count(String rtp_audio_rtcp_octet_count) {
        this.rtp_audio_rtcp_octet_count = rtp_audio_rtcp_octet_count;
    }

    public String getZrtp_passthru_active() {
        return zrtp_passthru_active;
    }

    public void setZrtp_passthru_active(String zrtp_passthru_active) {
        this.zrtp_passthru_active = zrtp_passthru_active;
    }

    public String getWings_dest_id() {
        return wings_dest_id;
    }

    public void setWings_dest_id(String wings_dest_id) {
        this.wings_dest_id = wings_dest_id;
    }

    public String getWings_service_id() {
        return wings_service_id;
    }

    public void setWings_service_id(String wings_service_id) {
        this.wings_service_id = wings_service_id;
    }

    public String getWings_client_service_id() {
        return wings_client_service_id;
    }

    public void setWings_client_service_id(String wings_client_service_id) {
        this.wings_client_service_id = wings_client_service_id;
    }

}
