/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.clients;

/**
 *
 * @author seba
 */
public class VerificationRequest {
 
    private String number;
    private boolean validated;
    private long validation_date;
    private float charged_amount;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public long getValidation_date() {
        return validation_date;
    }

    public void setValidation_date(long validation_date) {
        this.validation_date = validation_date;
    }

    public float getCharged_amount() {
        return charged_amount;
    }

    public void setCharged_amount(float charged_amount) {
        this.charged_amount = charged_amount;
    }
    
    
}
