/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Service;
import com.wings.voip.domain.sw.repos.ServiceLocalizationRepository;
import com.wings.voip.domain.sw.repos.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author seba
 */
@org.springframework.stereotype.Service
public class ServicesService {

    @Autowired
    private ServiceRepository serviceRepository;

    public Service getService(Long serviceId) {
        return serviceRepository.getOne(serviceId);
    }
    
    public Page<Service> findAllByOrderByActiveDesc(Pageable pr) {
        return serviceRepository.findAllByOrderByActiveDesc(pr);
    }

}
