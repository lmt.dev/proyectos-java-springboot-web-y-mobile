/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.repos;

import com.wings.voip.domain.sw.entities.Service;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ServiceRepository extends JpaRepository<Service, Long>, JpaSpecificationExecutor<Service> {

    List<Service> findByAsignationAndActive(String asignation, Boolean active);
    
    List<Service> findByAsignationAndActiveOrderBySortOrderAsc(String asignation, Boolean active);
    
    List<Service> findByServiceCodeIn(String[] codeNames);
    
    Page<Service> findAllByOrderByActiveDesc(Pageable pgbl);

}
