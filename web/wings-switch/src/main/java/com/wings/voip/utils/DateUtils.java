/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author seba
 */
public class DateUtils {

    private final static SimpleDateFormat MYSQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Given date returns netx month date
     *
     * @param date
     * @return
     */
    public static Date getNextMonth(Date date) {

        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);

        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        if (month == 11) {
            month = 0;
            year += 1;
        } else {
            month += 1;
        }

        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        return cal.getTime();

    }

    /**
     * Given date returns netx month date
     *
     * @param date
     * @return
     */
    public static Date getNextYear(Date date) {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        year++;
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }

    /**
     * Returns 31/12/2100 00:00:00
     *
     * @return
     */
    public static Date getNeverEndingExpiration() {
        Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
        cal.set(Calendar.YEAR, 2100);
        cal.set(Calendar.MONTH, 12);
        cal.set(Calendar.DAY_OF_MONTH, 31);
        cal.set(Calendar.HOUR_OF_DAY, 00);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        return cal.getTime();
    }

    /**
     * Check if a date is between given dates
     *
     * @param toCheck
     * @param rangeFrom
     * @param rangeTo
     * @return
     */
    public static Boolean dateInRange(Date toCheck, Date rangeFrom, Date rangeTo) {
        return toCheck.after(rangeFrom) && toCheck.before(rangeTo);
    }

    public static String getMysqlDateFormat(Date date) {
        return MYSQL_DATE_FORMAT.format(date);
    }

    /**
     * Returns days bettween from and today
     *
     * @param from
     * @return
     */
    public static long getDiffDays(Date from) {
        long timeFrom = from.getTime();
        long timeTo = new Date().getTime();
        long ret = timeTo - timeFrom;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns days bettween from and to
     *
     * @param from
     * @param to
     * @return
     */
    public static long getDiffDays(Date from, Date to) {
        long timeFrom = from.getTime();
        long timeTo = to.getTime();
        long ret = timeTo - timeFrom;
        return TimeUnit.DAYS.convert(ret, TimeUnit.MILLISECONDS);
    }
}
