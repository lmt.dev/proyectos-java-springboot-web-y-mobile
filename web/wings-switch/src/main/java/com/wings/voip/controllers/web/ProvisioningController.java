/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.Property;
import com.wings.voip.domain.sw.repos.PropertiesRepository;
import com.wings.voip.domain.sw.enums.PropertyCategories;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ProvisioningController {

    @Autowired
    private PropertiesRepository propertiesRepository;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/views/provisioning"}, method = {RequestMethod.GET})
    public String getProvisioning(Model model, HttpServletRequest request) {

        List<Property> props = propertiesRepository.findByPropCategory(PropertyCategories.provisioning.name());
        model.addAttribute("props", props);

        return "html/provisioning";
    }

    /**
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping(value = {"/views/provisioning"}, method = {RequestMethod.POST})
    public String postProvisioning(Model model, HttpServletRequest request) {

        Map<String, Property> propMap = new HashMap<>();
        List<Property> props = propertiesRepository.findByPropCategory(PropertyCategories.provisioning.name());
        props.forEach((prop) -> {
            propMap.put(prop.getPropName(), prop);
        });

        Map<String, String[]> params = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String key = entry.getKey();
            String[] value = entry.getValue();
            Property prop = propMap.get(key);
            prop.setPropValue(value[0]);
        }

        propertiesRepository.save(props);

        return "redirect:/#views/provisioning";
    }

    /**
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping(value = {"/views/provisioning/default"}, method = {RequestMethod.GET})
    public String setProvisioningDefault(Model model, HttpServletRequest request) {

        List<Property> props = propertiesRepository.findByPropCategory(PropertyCategories.provisioning.name());
        props.forEach((prop) -> {
            prop.setPropValue(prop.getPropDefault());
        });
        propertiesRepository.save(props);

        return "redirect:/#views/provisioning";

    }

}
