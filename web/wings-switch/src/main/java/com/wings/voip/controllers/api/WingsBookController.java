/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientRoamingFree;
import com.wings.voip.domain.sw.entities.RoamingFreeOperator;
import com.wings.voip.domain.sw.entities.RoamingFreeOperatorsNumber;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientRoamingFreeRepository;
import com.wings.voip.domain.sw.repos.RoamingFreeOperatorNumberRepository;
import com.wings.voip.domain.sw.repos.RoamingFreeOperatorRepository;
import com.wings.voip.model.WingsBookResponse;
import com.wings.voip.model.secondNumber.RoamingFreeResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class WingsBookController {

    @Autowired
    private RoamingFreeOperatorRepository roamingFreeOperatorRepository;

    @Autowired
    private RoamingFreeOperatorNumberRepository roamingFreeOperatorNumberRepository;

    @Autowired
    private ClientRoamingFreeRepository clientRoamingFreeRepository;

    @Autowired
    private ClientRepository clientRepository;

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/book/action"}, method = {RequestMethod.POST})
    public ResponseEntity<WingsBookResponse> enableSecondNumber(HttpServletRequest request) {
        WingsBookResponse response = new WingsBookResponse();
        response.setEnabled(Boolean.FALSE);
        response.setMessage("roaming_free_operator_not_supported");
        return ResponseEntity.ok(response);
    }

}
