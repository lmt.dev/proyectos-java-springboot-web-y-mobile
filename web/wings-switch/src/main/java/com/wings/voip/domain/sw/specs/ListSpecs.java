package com.wings.voip.domain.sw.specs;

import com.wings.voip.domain.sw.entities.PriceList;
import com.wings.voip.domain.sw.entities.PriceListDestination;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ListSpecs {

    public static Specification<PriceListDestination> searchPriceListDestination(String q) {

        return (Root<PriceListDestination> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search)
                        )
                );
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    public static Specification<PriceList> searchPriceList(String q) {

        return (Root<PriceList> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search)
                        )
                );
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}
