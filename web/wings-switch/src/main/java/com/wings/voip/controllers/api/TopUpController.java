/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.services.ApiAuthService;
import com.wings.voip.services.ClientService;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class TopUpController {

    private final Logger log = LoggerFactory.getLogger(TopUpController.class);

    @Autowired
    private ClientService clientService;
    @Autowired
    private ApiAuthService apiAuthService;

    @RequestMapping(value = "/api/topup", method = RequestMethod.POST)
    public ResponseEntity<String> validateNumber(HttpServletRequest request, @RequestBody Map<String, String> body) {

        String auth = request.getHeader("auth");
        if (!apiAuthService.isTokenValid(auth)) {
            return ResponseEntity.badRequest().body("unauth");
        }

        String number = body.get("login");
        String samount = body.get("amount");
        String orderId = body.get("orderId");

        if (number == null || number.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("error");
        }

        Double amount = 0d;
        try {
            amount = Double.valueOf(samount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("error");
        }

        if (amount <= 0) {
            return ResponseEntity.badRequest().body("error");
        }
        
        boolean res = clientService.addBalance(number, amount, orderId);
        if (res) {            
            //TODO: send a push notification after payment            
            return ResponseEntity.ok("success");
        } else {
            return ResponseEntity.badRequest().body("error");
        }
    }

}
