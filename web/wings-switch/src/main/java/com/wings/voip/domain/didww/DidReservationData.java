/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class DidReservationData {

    private Map<String, String> data = new HashMap<>();

    public DidReservationData(String id) {
        data.put("id", id);
        data.put("type", "available_dids");
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

}
