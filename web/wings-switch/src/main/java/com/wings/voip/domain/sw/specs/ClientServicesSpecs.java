/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.sw.specs;

import com.wings.voip.domain.sw.entities.ClientService;
import com.wings.voip.domain.sw.entities.Service;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author seba
 */
public class ClientServicesSpecs {

    public static Specification<ClientService> searchServiceByType(Long clientId, String serviceType) {

        return (Root<ClientService> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            
            Join<ClientService, Service> srv = root.join("service");
            
            List<Predicate> predicates = new ArrayList<>();
            
            predicates.add(builder.equal(root.get("clientId"), clientId));
            if (serviceType != null) {
                predicates.add(builder.equal(srv.get("serviceType"), serviceType));
            }
            
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
