/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Property;
import com.wings.voip.domain.sw.enums.PropertyCategories;
import com.wings.voip.domain.sw.repos.PropertiesRepository;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ClientProvisioningController {

    private final Logger log = LoggerFactory.getLogger(ClientProvisioningController.class);

    @Autowired
    private PropertiesRepository propertiesRepository;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/provisioning"}, method = {RequestMethod.GET})
    public ResponseEntity<Map<String, String>> getProvisioningApi(Model model, HttpServletRequest request) {

        log.debug("Provisioning request");

        Map<String, String> ret = new TreeMap<>();
        //authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            return ResponseEntity.ok(ret);
        }

        List<Property> props = propertiesRepository.findByPropCategory(PropertyCategories.provisioning.name());
        props.forEach((prop) -> {
            ret.put(prop.getPropName(), prop.getPropValue());
        });

        return ResponseEntity.ok(ret);
    }

}
