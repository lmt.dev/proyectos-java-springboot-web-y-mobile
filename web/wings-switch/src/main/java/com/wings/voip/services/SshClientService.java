/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import java.io.File;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SshClientService {

    private final Logger log = LoggerFactory.getLogger(SshClientService.class);

    /**
     *
     * @param user
     * @param password
     * @param host
     * @param command
     */
    public void sendCommand(String user, String password, String host, String command) {

        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host);
            session.setPassword(password);
            session.connect();
            ChannelExec channel = (ChannelExec) session.openChannel("exec");

            channel.setCommand(command);

            channel.disconnect();
            session.disconnect();

        } catch (Exception e) {
            log.error("Error on sending command", e);
        }
    }

    /**
     *
     * @param user
     * @param password
     * @param host
     * @param commands
     */
    public void sendCommands(String user, String password, String host, List<String> commands) {

        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host);
            session.setPassword(password);
            session.connect();
            ChannelExec channel = (ChannelExec) session.openChannel("exec");

            commands.forEach((command) -> {
                channel.setCommand(command);
            });

            channel.disconnect();
            session.disconnect();

        } catch (Exception e) {
            log.error("Error on sending commands", e);
        }
    }

    /**
     *
     * @param user
     * @param host
     * @param commands
     */
    public void sendCommands(String user, String host, List<String> commands) {

        try {
            JSch jsch = new JSch();
            jsch.setKnownHosts("~/.ssh/known_hosts");
            jsch.addIdentity("~/.ssh/id_rsa");

            Session session = jsch.getSession(user, host);
            session.connect();
            ChannelExec channel = (ChannelExec) session.openChannel("exec");

            StringBuilder b = new StringBuilder();
            commands.forEach((command) -> {
                b.append(command).append(";");
            });
            log.error("Sending commands " + b.toString());
            channel.setCommand(b.toString());
            channel.connect();

            channel.disconnect();
            session.disconnect();

        } catch (Exception e) {
            log.error("Error on sending commands", e);
        }
    }

}
