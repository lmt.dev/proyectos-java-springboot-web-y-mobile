/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.backoffice;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class BackofficeClientService {

    @Value("${wings.apiUrl}")
    private String urlApiBase;
    @Value("${wings.auth.token}")
    private String authToken;

    private final Logger log = LoggerFactory.getLogger(BackofficeClientService.class);

    /**
     *
     * @return
     */
    public List<CountryDto> getCountries() {
        try {
            String url = urlApiBase + "countries";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");
            headers.set("auth", authToken);
            HttpEntity<CountryResponse> requestEntity = new HttpEntity<>(headers);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<CountryResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, CountryResponse.class);
            CountryResponse countries = response.getBody();

            return countries.getCountries();

        } catch (Exception e) {
            log.error("Error gettting countries " + e.getMessage());
        }
        return new ArrayList<>();
    }

}
