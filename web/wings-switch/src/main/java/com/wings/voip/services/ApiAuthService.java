/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ApiAuthService {

    private final String local = "3d90b9c118c65dfbf2215d481cbc79c0f706b76d86a2c4d1179c1a998d81e181";

    public boolean isTokenValid(String token) {
        return token != null && token.equals(local);
    }

}
