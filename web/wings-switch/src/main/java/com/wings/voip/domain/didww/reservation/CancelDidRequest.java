/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww.reservation;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author seba
 */
public class CancelDidRequest {

    private GenericData data = new GenericData();

    public CancelDidRequest(String didId) {
        data.setType("dids");
        data.setId(didId);
        Map<String, Object> attributes = new LinkedHashMap<>();
        data.setAttributes(attributes);
        attributes.put("terminated", true);
    }

    public GenericData getData() {
        return data;
    }

    public void setData(GenericData data) {
        this.data = data;
    }

}
