/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.push.Data;
import com.wings.voip.domain.push.Notification;
import com.wings.voip.domain.push.PushNotification;
import com.wings.voip.domain.push.PushResponse;
import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientDevice;
import com.wings.voip.domain.sw.repos.ClientDeviceRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.utils.EncryptUtils;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class PushService {

    private final String fcmUrl = "https://fcm.googleapis.com/fcm/send";
    private final String fcmKey = "AAAAowbDwkg:APA91bEWoC82cGqFiLy9kWRP5FPVjTga9RTb9quhM66Xp9fI_lvUIU8eIweT0zPEXQ12ErM8qrj4hVdcUlrksJDBJgXKEEjRY1gExQlU_F10HiDiWVxkTTRZDVyS03v46yc42cpHZ0fz";

    private final Logger log = LoggerFactory.getLogger(PushService.class);

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ClientDeviceRepository clientDeviceRepository;
    @Autowired
    private ClientService clientService;
    @Autowired
    private MessageSource messageSource;

    /**
     *
     * @param notif
     * @return
     */
    public PushResponse sendPushNotification(PushNotification notif) {

        PushResponse response = null;
        String res = "";
        try {

            log.error("Sending push: " + notif.getTo());
            //log.error("Sending push: " + notif.getData());
            //log.error("Sending push: " + notif.getNotification());

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Authorization", "key=" + fcmKey);
            httpHeaders.set("Content-Type", "application/json");

//            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//            messageConverters.add(new MappingJackson2HttpMessageConverter());
//            restTemplate.getMessageConverters().addAll(messageConverters);
            //restTemplate.setInterceptors(Collections.singletonList(new RequestLoggingInterceptor()));
            HttpEntity<PushNotification> httpEntity = new HttpEntity<>(notif, httpHeaders);
            res = restTemplate.postForObject(fcmUrl, httpEntity, String.class);

            log.error("Push response : " + res);

        } catch (Exception e) {
            log.error("Error sending push " + res, e);
        }
        return response;

    }

    /**
     *
     * @param from
     * @param dest
     * @param useZrtp
     * @throws Exception
     */
    public void sendWakeUpPush(String from, String dest, boolean useZrtp) throws Exception {

        log.error("Init push to " + dest);
        String preffix = useZrtp ? "sec" : "";

        Client client = clientRepository.findByLogin(dest);

        //Clientsshared shared = clientssharedRepository.findByLogin(dest);
        if (client != null) {

            //log.error("Destination found  " + client.getId());
            ClientDevice clientDevice = clientDeviceRepository.findFirstByClientIdAndActive(client.getId(), Boolean.TRUE);
            if (clientDevice != null) {

                String token = clientDevice.getPushToken();

                PushNotification n = new PushNotification();
                n.setTo(token);
                n.setPriority("high");
                n.setTimeToLive(0);

                Data data = new Data();
                data.setBody(EncryptUtils.encrypt(preffix + from));
                data.setMandatory(true);
                data.setAppId("com.wings.phone");
                data.setTitle("INCOMING_CALL");

                n.setData(data);

                //log.error("Sending push to " + dest);
                sendPushNotification(n);
            }
        } else {
            log.error("Client NOT FOUND for " + dest);
            throw new Exception("USER NOT FOUND");
        }
    }

    /**
     *
     * @param pushTokens
     */
    public void sendUpdateContactsPush(List<String> pushTokens) {

        pushTokens.stream().map((pushToken) -> {
            PushNotification n = new PushNotification();
            n.setTo(pushToken);
            return n;
        }).map((n) -> {
            n.setPriority("high");
            return n;
        }).map((n) -> {
            n.setTimeToLive(0);
            return n;
        }).map((n) -> {
            Data data = new Data();
            data.setMandatory(true);
            data.setBody("please!");
            data.setAppId("com.wings.phone");
            data.setTitle("UPDATE_CONTACTS");
            n.setData(data);
            return n;
        }).forEachOrdered((n) -> {
            sendPushNotification(n);
        });

    }

    /**
     *
     * @param pushTokens
     */
    public void sendUpdateProvisioningPush(List<String> pushTokens) {

        pushTokens.stream().map((pushToken) -> {
            PushNotification n = new PushNotification();
            n.setTo(pushToken);
            return n;
        }).map((n) -> {
            n.setPriority("high");
            return n;
        }).map((n) -> {
            n.setTimeToLive(0);
            return n;
        }).map((n) -> {
            Data data = new Data();
            data.setMandatory(true);
            data.setBody("please!");
            data.setAppId("com.wings.phone");
            data.setTitle("UPDATE_PROVISIONING");
            n.setData(data);
            return n;
        }).forEachOrdered((n) -> {
            sendPushNotification(n);
        });

    }

    public void sendGenericPush(String title, String body, Long clientDeviceId) {

        ClientDevice device = clientService.getClientDevice(clientDeviceId);

        PushNotification push = new PushNotification();
        Notification notif = new Notification();

        notif.setTitle(title);
        notif.setBody(body);
        notif.setVisibility(0);

        push.setNotification(notif);
        push.setPriority("high");
        push.setTimeToLive(0);
        push.setTo(device.getPushToken());
        push.setVisibility("PRIVATE");

        sendPushNotification(push);
    }

    public void sendInsuficientFunds(String login) {

        Client client = clientService.getOne(login);
        ClientDevice device = clientService.getActiveClientDevice(client.getId());

        String title = messageSource.getMessage("push.error.nofunds.title", null, client.getLocaleReal());
        String body = messageSource.getMessage("push.error.nofunds.desc", null, client.getLocaleReal());
      
        sendGenericPush(title, body, device.getId());

    }

}
