/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Cdr {

    Variables variables;
    Callflow callflow;

    public Variables getVariables() {
        return variables;
    }

    public void setVariables(Variables variables) {
        this.variables = variables;
    }

    public Callflow getCallflow() {
        return callflow;
    }

    public void setCallflow(Callflow callflow) {
        this.callflow = callflow;
    }

}
