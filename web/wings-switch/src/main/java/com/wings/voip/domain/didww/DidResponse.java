/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

/**
 *
 * @author seba
 */
public class DidResponse {

    private DidData[] data;

    public DidData[] getData() {
        return data;
    }

    public void setData(DidData[] data) {
        this.data = data;
    }

}
