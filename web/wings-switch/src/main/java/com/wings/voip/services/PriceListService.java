/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.PriceList;
import com.wings.voip.domain.sw.entities.PriceListDestination;
import com.wings.voip.domain.sw.enums.ServiceTypes;
import com.wings.voip.domain.sw.repos.ClientServiceRepository;
import com.wings.voip.domain.sw.repos.PriceListDestinationRepository;
import com.wings.voip.domain.sw.repos.PriceListRepository;
import com.wings.voip.domain.sw.specs.ClientServicesSpecs;
import com.wings.voip.domain.sw.specs.PriceListSpecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author seba
 */
@Service
public class PriceListService {

    @Autowired
    private PriceListRepository priceListRepository;
    @Autowired
    private PriceListDestinationRepository priceListDestinationRepository;
    @Autowired
    private ClientServiceRepository clientServiceRepository;

    //lista de precios, prefijo, Id DEstination
    private final Map<Long, Map<String, Long>> prefixMap = new HashMap<>();
    private final Logger log = LoggerFactory.getLogger(PriceListService.class);

    private boolean importRunning = false;
    private String importRunningStatus = "idle";

    /**
     * Initialize
     */
    @PostConstruct
    public void init() {
        reload();
    }

    /**
     * Given dialedNumber get best price for that user, search in all plans that
     * user have
     *
     * @param client
     * @param dialedNumber
     * @return
     */
    public PriceListDestination getDestinationForNumber(Client client, String dialedNumber) {

        //reload();
        Sort sort = new Sort(Sort.Direction.DESC, "service.priority");
        List<com.wings.voip.domain.sw.entities.ClientService> clientServices
                = clientServiceRepository.findAll(ClientServicesSpecs.searchServiceByType(client.getId(), ServiceTypes.external.name()), sort);

        Long destId = null;
        Long serviceId = null;
        Long clientServiceId = null;
        for (com.wings.voip.domain.sw.entities.ClientService clientService : clientServices) {

            serviceId = clientService.getServiceId();
            clientServiceId = clientService.getId();
            destId = isDestinationInPriceList(dialedNumber, clientService.getService().getPriceListId());

            if (destId != null) {
                break;
            }
        }

        if (destId == null) {
            return null;
        }

        PriceListDestination pld = priceListDestinationRepository.findOne(destId);
        pld.setServiceId(serviceId);
        pld.setClientServiceId(clientServiceId);

        return pld;
    }

    /**
     * check for dialedNumber if destination is in pricelist
     *
     * @param dialedNumber
     * @param priceListId
     * @return
     */
    public Long isDestinationInPriceList(String dialedNumber, Long priceListId) {
        Map<String, Long> dests = prefixMap.get(priceListId);

        //log.error("Using price list " + dests);
        String tempNumber = dialedNumber;
        Long destId = null;
        while (destId == null) {
            if (tempNumber.length() == 0) {
                break;
            }

            //log.error("Searching with " + tempNumber);
            if (dests.containsKey(tempNumber)) {
                destId = dests.get(tempNumber);
                break;
            }
            tempNumber = tempNumber.substring(0, tempNumber.length() - 1);
        }
        return destId;
    }

    /**
     * Given login, calculate how many minutes the user can talk according to
     * price
     *
     * @param client
     * @param price
     * @return
     */
    public int calculateTimeout(Client client, double price) {
        double credit = client.getBalance();
        int duration = (int) (credit / price);
        return duration * 60;
    }

    /**
     *
     * @param minutes
     * @param destinationId
     * @return
     */
    public double calculateCost(int minutes, Long destinationId) {
        PriceListDestination dest = priceListDestinationRepository.findOne(destinationId);
        double price = dest.getPrice();
        return minutes * price;
    }

    /**
     *
     */
    public void reload() {
        prefixMap.clear();
        List<PriceList> lists = priceListRepository.findAll();
        for (PriceList list : lists) {
            Map<String, Long> prefixes = new HashMap<>();
            List<PriceListDestination> dests = priceListDestinationRepository.findByPriceListId(list.getId());
            for (PriceListDestination dest : dests) {
                String prefix = dest.getCountryCode() + (dest.getAreaCode() != null ? dest.getAreaCode() : "");
                prefixes.put(prefix, dest.getId());
            }
            prefixMap.put(list.getId(), prefixes);
        }

        //log.error("Price List Map " + prefixMap.toString());
    }

    /**
     *
     * @param name
     * @param description
     * @param dateFrom
     * @param dateTo
     * @param active
     */
    public void postServiceListCreation(String name, String description, String dateFrom, String dateTo, boolean active) {

        try {
            PriceList priceList = new PriceList();
            priceList.setActive(active);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date validFrom = sdf.parse(dateFrom);
            Date validTo = sdf.parse(dateTo);
            priceList.setValidFrom(validFrom);
            priceList.setValidTo(validTo);
            priceList.setName(name);
            priceList.setDescription(description);

            priceListRepository.save(priceList);

        } catch (Exception e) {
            log.error("Error adding price list", e);
        }
    }

    /**
     *
     * @param priceListId
     */
    public void deletePriceList(Long priceListId) {
        priceListDestinationRepository.deleteByPriceListId(priceListId);
        priceListRepository.delete(priceListId);
    }

    /**
     *
     * @param priceListId
     */
    public void deletePriceListDestinations(Long priceListId) {
        priceListDestinationRepository.deleteByPriceListId(priceListId);
    }

    /**
     *
     * @param size
     * @param page
     * @param q
     * @return
     */
    public Page<PriceList> search(int size, int page, String q) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        return priceListRepository.findAll(PriceListSpecs.searchPriceList(q), pr);
    }

    /**
     *
     * @param id
     * @return
     */
    public PriceList getOnePriceList(Long id) {
        return priceListRepository.findOne(id);
    }

    /**
     *
     * @param priceListId
     * @param name
     * @param description
     * @param dateFrom
     * @param dateTo
     * @param active
     */
    public void postServiceListEdition(Long priceListId, String name, String description, String dateFrom, String dateTo, boolean active) {
        try {
            PriceList priceList = priceListRepository.findOne(priceListId);
            priceList.setActive(active);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date validFrom = sdf.parse(dateFrom);
            Date validTo = sdf.parse(dateTo);
            priceList.setValidFrom(validFrom);
            priceList.setValidTo(validTo);
            priceList.setName(name);
            priceList.setDescription(description);
            priceListRepository.save(priceList);
        } catch (Exception e) {
            log.error("Error editing pricelist ", e);
        }
    }

    /**
     *
     * @param page
     * @param size
     * @param q
     * @param priceListId
     * @return
     */
    public Page<PriceListDestination> searchDestination(int page, int size, String q, Long priceListId) {
        page = page == 0 ? page : page - 1;
        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "name"));
        return priceListDestinationRepository.findAll(PriceListSpecs.searchPriceListDestination(q, priceListId), pr);
    }

    /**
     *
     * @return
     */
    public List<PriceList> findAllPriceList() {
        return priceListRepository.findAll();
    }

    /**
     *
     * @param name
     * @param country_code
     * @param area_code
     * @param price
     * @param price_list_id
     */
    public void postServiceDestinationList(String name, String country_code, String area_code, String price, String price_list_id) {
        PriceListDestination priceListDestination = new PriceListDestination();
        priceListDestination.setName(name);
        priceListDestination.setCountryCode(country_code);
        Integer areaCode = null;
        try {
            areaCode = Integer.valueOf(area_code);
        } catch (Exception e) {
        }
        priceListDestination.setAreaCode(areaCode);
        priceListDestination.setPrice(Double.valueOf(price));
        priceListDestination.setPriceListId(Long.valueOf((price_list_id)));
        priceListDestinationRepository.save(priceListDestination);
    }

    /**
     *
     * @param toSave
     */
    public void saveDestinations(List<PriceListDestination> toSave) {
        toSave.forEach(priceListDestination -> {
            try {
                priceListDestinationRepository.save(priceListDestination);
            } catch (Exception e) {
                log.error("Errror on save " + priceListDestination.getCountryCode() + " " + priceListDestination.getName());
            }
        });
    }

    /**
     *
     * @param priceListId
     * @param file
     * @return
     */
    public String importFile(Long priceListId, File file) {

        if (priceListId == null || file == null) {
            return importRunningStatus;
        }

        if (!importRunning) {

            importRunning = true;
            try {
                List<PriceListDestination> toSave = new ArrayList<>();
                BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line;
                int coutLines = 0;
                while ((line = bf.readLine()) != null) {
                    try {
                        if (line.trim().isEmpty()) {
                            continue;
                        }

                        line = line.replaceAll("\"", "");
                        String[] splited = line.split(";");

                        if (splited.length != 4) {
                            log.error("Error malformed line " + line);
                            continue;
                        }

                        PriceListDestination dest = new PriceListDestination();
                        if (splited[0] == null || splited[0].isEmpty()) {
                            continue;
                        }
                        dest.setName(splited[0]);

                        String countryCode = splited[1];
                        if (countryCode == null || countryCode.isEmpty()) {
                            continue;
                        }
                        dest.setCountryCode(countryCode);

                        Integer areaCode = null;
                        try {
                            areaCode = Integer.valueOf(splited[2]);
                        } catch (Exception e) {
                        }
                        dest.setAreaCode(areaCode);

                        String price = splited[3];
                        if (price == null) {
                            continue;
                        }
                        dest.setPrice(Double.valueOf(price.replaceAll(",", ".")));
                        dest.setPriceListId(priceListId);

                        toSave.add(dest);

                        coutLines++;

                        importRunningStatus = "parsing " + coutLines;

                    } catch (Exception e) {
                        log.error("Error parseando linea " + line, e);
                    }
                }

                importRunningStatus = "saving " + coutLines;
                saveDestinations(toSave);
                importRunning = false;
                importRunningStatus = "done " + coutLines;

            } catch (Exception e) {
                log.error("Error importando lista de precios", e);
                importRunningStatus = "Error: " + e.getMessage();
                importRunning = false;
            }
        }

        return importRunningStatus;
    }

    /**
     *
     * @return
     */
    public String getImportRunningStatus() {
        if (importRunningStatus.contains("done") && !importRunning) {
            importRunningStatus = "idle";
        }
        return importRunningStatus;
    }
}
