/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.clients;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class CheckMobiClient {
    
    private final String validationURL = "https://api.checkmobi.com/v1/validation/request";
    private final String verificationURL = "https://api.checkmobi.com/v1/validation/verify";
    private final String statusURL = "https://api.checkmobi.com/v1/validation/status/";
    private final String authHeader = "B4F205FF-B584-4925-A590-6D8EA6C36307";
    
    public ValidationRequest validationRequest(String number){
    
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", authHeader);
        
        Map<String, String> map = new HashMap<>();
        map.put("number", number);
        map.put("type", "reverse_cli");
        map.put("platform", "android");
        
        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(map, headers);
        ResponseEntity<ValidationRequest> result = template.postForEntity(validationURL, requestEntity, ValidationRequest.class);

        return result.getBody();
    }
    
    public VerificationRequest verificationRequest(String id, String pin){
    
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", authHeader);
        
        Map<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("pin", pin);
                
        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(map, headers);
        ResponseEntity<VerificationRequest> result = template.postForEntity(verificationURL, requestEntity, VerificationRequest.class);

        return result.getBody();
    }
    
    public VerificationRequest statusRequest(String id){
    
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authHeader);
        
        HttpEntity requestEntity = new HttpEntity(headers);        
        VerificationRequest result = new VerificationRequest();
        try {
            URI url = new URI(statusURL+id);
            ResponseEntity<VerificationRequest> r = template.exchange(url, HttpMethod.GET, requestEntity, VerificationRequest.class);
            result = r.getBody();
        } catch (Exception e) {
        }        
        return result;
    }
    
}
