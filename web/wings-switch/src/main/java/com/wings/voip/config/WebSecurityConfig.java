/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//    @Autowired
//    private WingsUsersDetailsService wingsUsersDetailsService;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.authorizeRequests().anyRequest().permitAll();                
        http
                .csrf().disable()
                .cors().disable()
                .authorizeRequests()
                .antMatchers("/css/**/*", "/js/**/*", "/vendors/**/*", "/fonts/**/*", "/img/**/*", "/images/**/*", "/locale").permitAll()
                .antMatchers("/pays/**/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("admin").password("YyAg3hpgD5DjKK22").roles("ADMIN");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/css/**/*", "/js/**/*", "/images/**/*", "/img/**/*", "/vendors/**/*", "/fonts/**/*", "/api/**/*", "/fs/**/*");

        web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        DefaultHttpFirewall firewall = new DefaultHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**").allowedOrigins(
//                        "http://wingsmobile.net",
//                        "http://www.wingsmobile.net",
//                        "https://wingsmobile.net",
//                        "https://www.wingsmobile.net",
//                        "http://beta.wingsmobile.net",
//                        "https://beta.wingsmobile.net",
//                        "http://localhost:8099"
//                ).allowedHeaders("Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Authorization", "X-Requested-With", "Origin","X-Frame-Options");
//            }
//        };
//    }
//
//    @Bean
//    @DependsOn("localeResolver")
//    public AuthenticationSuccessHandler successHandler() {
//        return new WingsAuthenticationSuccessHandler();
//    }
//
//    @Bean
//    public AuthenticationFailureHandler failure() {
//        return new WingsAuthenticationFailureHandler();
//    }
//
//    @Bean("sessionRegistry")
//    public SessionRegistry sessionRegistry() {
//        return new SessionRegistryImpl();
//    }
//
//    @Bean
//    public HttpSessionEventPublisher httpSessionEventPublisher() {
//        return new HttpSessionEventPublisher();
//    }
//    @Bean
//    public SwitchUserFilter switchUserFilter() {
//        SwitchUserFilter filter = new SwitchUserFilter();
//        filter.setUserDetailsService(wingsUsersDetailsService);
//        filter.setUsernameParameter("username");
//        filter.setSwitchUserUrl("/admin/member/login");        
//        filter.setExitUserUrl("/admin/member/logout");
//        filter.setTargetUrl("/");
//        return filter;
//    }
}
