/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Callflow {

    private Times times;
    private Caller_profile caller_profile;

    public Times getTimes() {
        return times;
    }

    public void setTimes(Times times) {
        this.times = times;
    }

    public Caller_profile getCaller_profile() {
        return caller_profile;
    }

    public void setCaller_profile(Caller_profile caller_profile) {
        this.caller_profile = caller_profile;
    }

}
