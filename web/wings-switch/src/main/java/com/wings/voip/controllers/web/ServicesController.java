/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.Service;
import com.wings.voip.domain.sw.repos.ServiceRepository;
import com.wings.voip.services.ServicesService;
import com.wings.voip.utils.PageWrapper;
import com.wings.voip.utils.UrlUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ServicesController {
    
    @Autowired
    private ServicesService servicesService;

    @RequestMapping(value = "/views/services", method = RequestMethod.GET)
    public String getClients(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<Service> items = servicesService.findAllByOrderByActiveDesc(pr);

        PageWrapper<Service> pageWrapper = new PageWrapper<>(items, "views/services");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "html/services/services";
    }

    @RequestMapping(value = "/views/service/{id}", method = RequestMethod.GET)
    public String getService(Model model, HttpServletRequest request, @PathVariable("id") Long serviceId) {
        Service service = servicesService.getService(serviceId);
        model.addAttribute("service", service);
        List<String> countries = Arrays.asList(service.getValidCountries().split(","));
        model.addAttribute("countries", countries);
        return "html/services/service";
    }

}
