/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientService;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientServiceRepository;
import com.wings.voip.domain.sw.repos.ServiceRepository;
import com.wings.voip.model.contact.Contact;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class ContactsService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientServiceRepository clientServiceRepository;

    @Autowired
    private ClientServiceService clientServiceService;

    public List<Contact> parseContactServices(List<Contact> contacts) {

        Map<String, String> clientCodes = buildClientMap();
        for (Contact contact : contacts) {
            List<com.wings.voip.model.contact.Number> numbers = contact.getNumbers();
            for (com.wings.voip.model.contact.Number number : numbers) {
                if (clientCodes.containsKey(number.getNumber())) {
                    String code = clientCodes.get(number.getNumber());
                    number.setFree(code.contains("free"));
                    number.setSecure(code.contains("secure"));
                }
            }
        }

        return contacts;
    }

    public Map<String, String> buildClientMap() {

        Map<Long, String> freeSecureId = clientServiceService.getSecureAndFreeMap();

        //map {login=contatenatedPlanCodes}
        Map<String, String> ret = new HashMap<>();
        List<Client> clients = clientRepository.findAll();

        for (Client client : clients) {
            String clientCodes = "";
            List<ClientService> services = clientServiceRepository.findByClientId(client.getId());
            for (ClientService service : services) {
                String code = freeSecureId.get(service.getServiceId());
                if (code != null) {
                    clientCodes += code + ",";
                }
            }
            ret.put(client.getLogin(), clientCodes);
        }

        return ret;
    }

}
