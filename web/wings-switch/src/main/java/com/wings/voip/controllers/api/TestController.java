/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.services.PushService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author seba
 */
@Controller
public class TestController {

    @Autowired
    private PushService pushService;

    @ResponseBody
    @RequestMapping(value = {"/register/token"}, method = RequestMethod.GET)
    public String registerToken(HttpServletRequest request) {

        String user = request.getParameter("user");
        String token = request.getParameter("token");

        //pushService.addToken(user, token);

        return "ok";

    }
}
