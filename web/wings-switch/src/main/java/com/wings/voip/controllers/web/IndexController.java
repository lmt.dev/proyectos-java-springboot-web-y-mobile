/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.utils.EncryptUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class IndexController {

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) {
        return "html/index";
    }

    @RequestMapping(value = {"/views/main"}, method = RequestMethod.GET)
    public String getMain(Model model, HttpServletRequest request) {
        return "html/blank";
    }

    @RequestMapping(value = {"/pays/{code}"}, method = RequestMethod.GET)
    public String getPaysas(Model model, HttpServletRequest request, @PathVariable("code") String code) {
        //System.out.println(request.getRequestURI());
        //System.out.println(request.getRequestURL());
        //RequestUtils.debugRequestParams(request);
        try {
            String decrypted = EncryptUtils.decrypt(code);
            model.addAttribute("decrypted", decrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("code", code);
        return "html/pays";
    }

}
