/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientContact;
import com.wings.voip.services.ClientService;
import com.wings.voip.services.DidWWService;
import com.wings.voip.utils.PageWrapper;
import com.wings.voip.utils.UrlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author seba
 */
@Controller
public class ClientsController {

    private final Logger log = LoggerFactory.getLogger(ClientsController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private DidWWService didWWService;

    /**
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/views/clients", method = RequestMethod.GET)
    public String getClients(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String sdateFrom = request.getParameter("from");
        String sdateTo = request.getParameter("to");
        String q = request.getParameter("q");
        String slogin = request.getParameter("q");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (ssize == null) {
            ssize = "20";
        }

        if (spage == null) {
            spage = "0";
        }

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }
        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        if (q != null && !q.isEmpty()) {
            try {
                q = URLDecoder.decode(q, "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }

        page = page == 0 ? page : page - 1;

        PageRequest pr = new PageRequest(page, size, new Sort(Sort.Direction.ASC, "id"));
        Page<Client> items;
        Date dateFrom = null, dateTo = null;
        if (sdateFrom != null) {
            try {
                dateFrom = sdf.parse(sdateFrom);
            } catch (ParseException e) {
            }
        }

        if (sdateTo != null) {
            try {
                dateTo = sdf.parse(sdateTo);
            } catch (ParseException e) {
            }
        }

        items = clientService.findAll(pr, slogin, dateFrom, dateTo);

        PageWrapper<Client> pageWrapper = new PageWrapper<>(items, "views/clients");

        long desde = page == 0 ? 1 : (size * page + 1);
        long hasta = desde + size - 1;
        if (hasta > items.getTotalElements()) {
            hasta = items.getTotalElements();
        }
        String summary = "Mostrando " + desde + " a " + hasta + " de " + items.getTotalElements() + " items";
        pageWrapper.setSummary(summary);

        model.addAttribute("qq", UrlUtils.cleanQueryString(request.getQueryString()));
        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        didWWService.sync();

        return "html/clients";
    }

    /**
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/views/clients/contacts/{id}", method = RequestMethod.GET)
    public String getClientContacts(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        Client client = clientService.findOneClient(id);
        List<ClientContact> list = clientService.findByClientId(client.getId());
        model.addAttribute("items", list);
        return "html/contacts_detail";
    }

    /**
     * @param id
     * @return
     */
    @RequestMapping(value = {"views/clients/balance/{id}/credit"}, method = RequestMethod.GET)
    public String getAddCredit(Model model, @PathVariable("id") Long id) {

        model.addAttribute("id", id);

        return "html/add_credit";
    }

    @RequestMapping(value = {"views/clients/balance/{id}/update"}, method = RequestMethod.POST)
    public ResponseEntity<String> postOrderEditPrice(HttpServletRequest request, @PathVariable("id") Long id) {

        String amount = request.getParameter("credit");
        if (amount != null && amount.length() > 0) {
            //clientService.getAddCredit(id, amount, request);
        } else {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok("Balance NoT modified");
    }
}
