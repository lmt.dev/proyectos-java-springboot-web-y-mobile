/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww.reservation;

import java.util.List;

/**
 *
 * @author seba
 */
public class DidListResponse {

    private List<GenericData> data;

    public List<GenericData> getData() {
        return data;
    }

    public void setData(List<GenericData> data) {
        this.data = data;
    }

}
