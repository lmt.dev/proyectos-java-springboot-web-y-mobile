/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author seba
 */
@Service
public class SmsService {

    private final Logger log = LoggerFactory.getLogger(SmsService.class);
    private final String endpoint = "http://sms1.mblox.com:9001/HTTPSMS";
    private static final Map<String, String> PARAMS = new HashMap<>();

    @Autowired
    private Environment environment;

    static {
        PARAMS.put("UN", "amilio1");
        PARAMS.put("P", "dyEbEmM5");
        PARAMS.put("S", "H");
        PARAMS.put("SA", "Wings");
    }

    public String sendSms(String to, String message) {
        String ret;
        try {

            boolean dev = false;
            String[] profiles = environment.getActiveProfiles();
            for (String profile : profiles) {
                if (profile.contains("dev")) {
                    dev = true;
                    break;
                }
            }

            String M;
            try {
                M = URLEncoder.encode(message, "UTF-8");
            } catch (Exception e) {
                M = "";
            }

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "*/*");
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(endpoint)
                    .queryParam("S", PARAMS.get("S"))
                    .queryParam("UN", PARAMS.get("UN"))
                    .queryParam("P", PARAMS.get("P"))
                    .queryParam("DA", to)
                    .queryParam("SA", PARAMS.get("SA"))
                    .queryParam("M", M);

            HttpEntity<?> entity = new HttpEntity<>(headers);
            String url = endpoint + "?S=H&UN=amilio1&P=dyEbEmM5&DA=" + to + "&SA=Wings&M=" + M;
            if (!dev) {

                log.error("Sending sms " + url);
                RestTemplate template = new RestTemplate();
                ResponseEntity<String> response = template.exchange(url, HttpMethod.GET, entity, String.class);
                ret = response.getBody();
                log.error("Sending sms response " + ret);

            } else {
                log.info("SENDING SMS " + url);
                ret = "Fake send SMS";
            }

        } catch (Exception e) {
            ret = e.getMessage();
            log.error("Error sending SMS", e);
        }
        return ret;
    }
}
