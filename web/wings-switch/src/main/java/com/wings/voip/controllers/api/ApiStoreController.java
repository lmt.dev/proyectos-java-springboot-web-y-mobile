/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.api;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.StoreServiceDTO;
import com.wings.voip.services.ClientServiceService;
import com.wings.voip.utils.EncryptUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class ApiStoreController {

    @Autowired
    private ClientServiceService clientServiceService;

    @Autowired
    private ClientRepository clientRepository;

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/store/services"}, method = {RequestMethod.GET})
    public ResponseEntity<List<StoreServiceDTO>> getStoreServices(Model model, HttpServletRequest request) {

        //authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            return ResponseEntity.badRequest().build();
        }

        String login = request.getParameter("login");
        String locale = request.getParameter("locale");

        return ResponseEntity.ok(clientServiceService.getStoreServices(login, locale));
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/api/store/services/buy"}, method = {RequestMethod.POST})
    public ResponseEntity<AccountStatus> postBuyService(Model model, HttpServletRequest request) {

        String login = request.getParameter("login");
        String password = EncryptUtils.decrypt(request.getParameter("token"));
        String appId = request.getParameter("app_id");
        String accessToken = request.getParameter("access_token");
        String serviceId = request.getParameter("serviceId");
        String extend = request.getParameter("extend");

        //Authorize login
        Client client = clientRepository.findByLogin(login);
        if (client == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!client.getToken().equals(password)) {
            return ResponseEntity.badRequest().build();
        }

        //authorize app
        if (appId == null || accessToken == null) {
            return ResponseEntity.badRequest().build();
        }

        //authorize app
        if (!(appId.equals("a2dc3b1c57d64fe9840c5cb") && accessToken.equals("6f4390a15e9165f37ad6d96e0c63f68fb1cc1f13"))) {
            return ResponseEntity.badRequest().build();
        }

        //last and not least, authorize request
        String auth = request.getHeader("Authorization");
        if (auth == null || !auth.equals("Basic YW5kcm9pZF9hcHA6d0c2REVzUzM0clQ1OUE5Vg==")) {
            return ResponseEntity.badRequest().build();
        }

        Long servId;
        try {
            servId = Long.parseLong(serviceId);
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().build();
        }

        try {
            clientServiceService.buyService(servId, client.getId(), extend.equals("true"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().header("error", e.getMessage()).build();            
        }

        return ResponseEntity.ok(clientServiceService.getAccountStatus(client.getId(), null));
    }

}
