/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.LocalizationCountry;
import com.wings.voip.domain.sw.repos.LocalizationCountryRepository;
import com.wings.voip.services.backoffice.CountryDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class LocalizedCountriesService {

    @Autowired
    private LocalizationCountryRepository localizationCountryRepository;
    @Autowired
    private com.wings.voip.services.backoffice.BackofficeClientService backofficeClientService;

    public void syncCountries() {

        List<CountryDto> dtos = backofficeClientService.getCountries();
        for (CountryDto dto : dtos) {

            LocalizationCountry c = localizationCountryRepository.findByCountryCode(dto.getCode().toLowerCase());
            if (c == null) {
                c = new LocalizationCountry();
            }

            c.setCountryCode(dto.getCode().toLowerCase());
            c.setCurrency(dto.getCurrency());
            c.setDefaultLocale(dto.getDefaultLocale());
            c.setExchangeRate(dto.getExchangeRate());
            c.setOtherLocale(null);

            localizationCountryRepository.save(c);

        }
    }

}
