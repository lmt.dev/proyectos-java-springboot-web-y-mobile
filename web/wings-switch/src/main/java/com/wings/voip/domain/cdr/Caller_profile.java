/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Caller_profile {

    private String username;
    private String dialplan;
    private String caller_id_name;
    private String ani;
    private String aniii;
    private String caller_id_number;
    private String network_addr;
    private String rdnis;
    private String destination_number;
    private String uuid;
    private String source;
    private String context;
    private String chan_name;

    private Originatee originatee;
    private Origination origination;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDialplan() {
        return dialplan;
    }

    public void setDialplan(String dialplan) {
        this.dialplan = dialplan;
    }

    public String getCaller_id_name() {
        return caller_id_name;
    }

    public void setCaller_id_name(String caller_id_name) {
        this.caller_id_name = caller_id_name;
    }

    public String getAni() {
        return ani;
    }

    public void setAni(String ani) {
        this.ani = ani;
    }

    public String getAniii() {
        return aniii;
    }

    public void setAniii(String aniii) {
        this.aniii = aniii;
    }

    public String getCaller_id_number() {
        return caller_id_number;
    }

    public void setCaller_id_number(String caller_id_number) {
        this.caller_id_number = caller_id_number;
    }

    public String getNetwork_addr() {
        return network_addr;
    }

    public void setNetwork_addr(String network_addr) {
        this.network_addr = network_addr;
    }

    public String getRdnis() {
        return rdnis;
    }

    public void setRdnis(String rdnis) {
        this.rdnis = rdnis;
    }

    public String getDestination_number() {
        return destination_number;
    }

    public void setDestination_number(String destination_number) {
        this.destination_number = destination_number;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getChan_name() {
        return chan_name;
    }

    public void setChan_name(String chan_name) {
        this.chan_name = chan_name;
    }

    public Originatee getOriginatee() {
        return originatee;
    }

    public void setOriginatee(Originatee originatee) {
        this.originatee = originatee;
    }

    public void setOrigination(Origination origination) {
        this.origination = origination;
    }

    public Origination getOrigination() {
        return origination;
    }

}
