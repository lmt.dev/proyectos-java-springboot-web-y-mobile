/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww.reservation;

/**
 *
 * @author seba
 */
public class DidOrderResponse {

    private GenericData data;

    public GenericData getData() {
        return data;
    }

    public void setData(GenericData data) {
        this.data = data;
    }

}
