package com.wings.voip.controllers.web;

import com.wings.voip.domain.sw.entities.PriceList;
import com.wings.voip.services.PriceListService;
import com.wings.voip.utils.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class PriceListController {

    @Autowired
    private PriceListService priceListService;

    private final Logger log = LoggerFactory.getLogger(PriceListController.class);

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/views/pricelist", method = RequestMethod.GET)
    public String getDestination(Model model, HttpServletRequest request) {

        String ssize = request.getParameter("size");
        String spage = request.getParameter("page");
        String q = request.getParameter("q");

        int page = 0;
        int size = 20;
        try {
            page = Integer.parseInt(spage);
        } catch (NumberFormatException e) {
        }

        try {
            size = Integer.parseInt(ssize);
        } catch (NumberFormatException e) {
        }

        Page<PriceList> items = priceListService.search(size, page, q);
        PageWrapper<PriceList> pageWrapper = new PageWrapper<>(items, "views/pricelist");

        model.addAttribute("q", q);
        model.addAttribute("page", pageWrapper);
        model.addAttribute("size", ssize);

        return "html/pricelist";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/views/pricelist/add/", method = RequestMethod.GET)
    public String getServiceAbm(Model model, HttpServletRequest request) {
        model.addAttribute("pricelist", new PriceList());
        return "html/pricelist-add";
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/views/pricelist/add"}, method = RequestMethod.POST)
    public ResponseEntity<String> postServicePriceList(HttpServletRequest request) {

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String dateFrom = request.getParameter("validFrom");
        String dateTo = request.getParameter("validTo");
        String active = request.getParameter("active");

        priceListService.postServiceListCreation(name, description, dateFrom, dateTo, (active != null && active.equals("on")));

        return ResponseEntity.ok("La Lista de Precios fue Creada");
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/views/pricelist/edit/{id}"}, method = RequestMethod.GET)
    public String editPriceList(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        PriceList priceList = priceListService.getOnePriceList(id);
        model.addAttribute("pricelist", priceList);

        return "html/pricelist-edit";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/views/pricelist/edit/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> postServiceEditPriceList(Model model, HttpServletRequest request, @PathVariable("id") Long id) {

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String dateFrom = request.getParameter("validFrom");
        String dateTo = request.getParameter("validTo");
        String active = request.getParameter("active");

        priceListService.postServiceListEdition(id, name, description, dateFrom, dateTo, (active != null && active.equals("on")));
        return ResponseEntity.ok("La Lista de Precios fue Modificada");
    }

    /**
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/views/pricelist/delete/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> deletePriceListDest(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        try {
            priceListService.deletePriceList(id);
            return ResponseEntity.ok("La Lista de Precios fue eliminada");
        } catch (Exception e) {
            log.error("Error deleting price list", e);
        }
        return ResponseEntity.ok("error");
    }

    /**
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"/views/pricelist/delete/{id}/prices"}, method = RequestMethod.POST)
    public ResponseEntity<String> deletePriceListDestinations(Model model, HttpServletRequest request, @PathVariable("id") Long id) {
        try {
            priceListService.deletePriceListDestinations(id);
            return ResponseEntity.ok("Los precios fueron eliminados");
        } catch (Exception e) {
            log.error("Error deleting price list", e);
        }
        return ResponseEntity.ok("error");
    }

}
