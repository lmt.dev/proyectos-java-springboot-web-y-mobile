/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services.smsto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class AuthResponse {

    @JsonProperty("jwt")
    private String jwt;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires")
    private Long expires;
    @JsonProperty("type")
    private String type;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Long getExpires() {
        return expires;
    }

    public void setExpires(Long expires) {
        this.expires = expires;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
