/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.controllers.fs;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientDid;
import com.wings.voip.domain.sw.entities.ClientRoamingFree;
import com.wings.voip.domain.sw.entities.PriceListDestination;
import com.wings.voip.domain.sw.repos.ClientDidRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientRoamingFreeRepository;
import com.wings.voip.services.BlockIpService;
import com.wings.voip.services.PriceListService;
import com.wings.voip.services.PushService;
import com.wings.voip.utils.RequestUtils;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author seba
 */
@Controller
public class DialplanController {

    private final Logger log = LoggerFactory.getLogger(DialplanController.class);
    //private final String externalServer = "79.143.183.86";
    //private final String externalServer = "79.143.183.89";
    private final String externalServer = "213.166.103.1";

    @Autowired
    private TemplateEngine xmlTemplateEngine;
    @Autowired
    private PushService pushService;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ClientDidRepository clientDidRepository;
    @Autowired
    private ClientRoamingFreeRepository clientRoamingFreeRepository;
    @Autowired
    private PriceListService priceListService;
    @Autowired
    private BlockIpService blockIpService;

    /**
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/fs/dialplan"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String directory(HttpServletRequest request) {

        //RequestUtils.debugRequestParams(request);
        //get FS params
        String destinationNumber = request.getParameter("Caller-Destination-Number");
        String from = request.getParameter("Caller-Username");
        String destinationUri = request.getParameter("variable_sip_to_uri");
        String sdp = request.getParameter("variable_switch_r_sdp");
        String callerIp = request.getParameter("Caller-Network-Addr");

        log.error("Requesting dialplan for " + destinationNumber + " -> " + destinationUri);

        String clientNumber = null;
        //is a call to a did number
        ClientDid did = clientDidRepository.findByDidNumber(destinationNumber);
        if (did != null) {
            //TODO: replace this, adding clientNumber on ClientDid
            Client client = clientRepository.findOne(did.getClientId());
            clientNumber = client.getLogin();
            destinationUri = destinationUri.replaceAll(did.getDidNumber(), client.getLogin());
            destinationUri = destinationUri.replaceAll(":5060", "");
        }

        //if call it's from roaming free
        ClientRoamingFree clientRoamingFree = clientRoamingFreeRepository.findByOperatorNumber(destinationNumber);
        if (clientRoamingFree != null) {
            clientNumber = clientRoamingFree.getClientNumber();
            destinationUri = destinationUri.replaceAll(clientRoamingFree.getOperatorNumber(), clientRoamingFree.getClientNumber());
            destinationUri = destinationUri.replaceAll(":5060", "");
        }

        //init params
        Context context = new Context();
        int sleep = 4000;
        boolean useZrtp = sdp.contains("a=zrtp-hash:");
        boolean sendPush = true;
        boolean internal = true;

        //TESTING
        if (destinationNumber.equals("test1") || destinationNumber.equals("test2")) {
            RequestUtils.debugRequestParams(request);
            context.setVariable("ring", "${es-ring}");
            context.setVariable("zrtp", useZrtp);
            context.setVariable("expression", "^" + destinationNumber);
            context.setVariable("destination", "sofia/internal/" + destinationUri.replaceAll("@", "%"));
            return xmlTemplateEngine.process("xml/dialplan_test", context);
        }

        //set expression it's is free
        String expression = "^" + destinationNumber;
        String destination = destinationUri.replaceAll("@", "%");

        //enable zrtp
        if (useZrtp) {
            sleep = 6000;
            destination = destination.replaceAll("sec", "");
            destinationNumber = destinationNumber.replaceAll("sec", "");
        }

        Integer timeout = null;
        Long destId = null;
        Long serviceId = null;
        Long clientServiceId = null;
        //external, disable push 
        if (destinationNumber.startsWith("int")) {
            sendPush = false;
            internal = false;

            Client client = clientRepository.findByLogin(from);
            if (client == null) {
                log.error("Add ip to block , client does not exists " + callerIp + " " + from);
                blockIpService.addBlockIp(callerIp);
                return xmlTemplateEngine.process("xml/dialplan_fail", context);
            }

            if (client.getLogin().equals("5493518149914")) {

                PriceListDestination pld = priceListService.getDestinationForNumber(client, destinationNumber.replaceAll("int", ""));
                if (pld == null) {

                    log.error("pld is null for " + client.getLogin() + " " + destinationNumber);
                    return xmlTemplateEngine.process("xml/dialplan_fail", context);

                } else {

                    timeout = priceListService.calculateTimeout(client, pld.getPrice());

                    destId = pld.getId();
                    serviceId = pld.getServiceId();
                    clientServiceId = pld.getClientServiceId();

                    log.error("Timeout para " + timeout);

                    //No CREDIT
                    if (timeout <= 0) {

                        try {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(36000);
                                    } catch (Exception e) {
                                    }
                                    pushService.sendInsuficientFunds(client.getLogin());
                                }
                            }).start();
                        } catch (Exception e) {
                        }

                        return xmlTemplateEngine.process("xml/dialplan_fail", context);
                    }
                }

            } else {

                //set timeout according to price
                PriceListDestination pld = priceListService.getDestinationForNumber(client, destinationNumber.replaceAll("int", ""));
                if (pld == null) {
                    log.error("pld is null for " + client.getLogin() + " " + destinationNumber);
                    timeout = 3600;
                    destId = 1l;
                } else {
                    //timeout = priceListService.calculateTimeout(client, pld.getPrice());
                    timeout = 3600;
                    destId = pld.getId();
                }
            }
        }

        //internal or external
        if (internal) {
            destination = "sofia/internal/" + destination;
        } else {
            sleep = 0;
            destination = "{origination_caller_id_number=" + from + ",origination_caller_id_name=" + from + ",caller_id_number=" + from + ",caller_id_name=" + from + ",fail_on_single_reject=true,originate_retries=false,ignore_early_media=true,originate_timeout=30}sofia/external/" + destinationNumber.replaceAll("int", "99900") + "@" + externalServer;
        }

        //Send push
        //TODO: check sofia_contact in order to know is user is registered, avoid sending push        
        if (sendPush) {
            try {
                pushService.sendWakeUpPush(from, clientNumber != null ? clientNumber : destinationNumber, useZrtp);
            } catch (Exception e) {
                return xmlTemplateEngine.process("xml/dialplan_fail", context);
            }
        }

        String ring = getRingback("");

        destinationNumber = destinationNumber.replaceAll("int", "802");

        context.setVariable("destinationNumber", destinationNumber);
        context.setVariable("destination", destination);
        context.setVariable("expression", expression);
        context.setVariable("zrtp", useZrtp);
        context.setVariable("sleep", sleep);
        context.setVariable("ring", ring);
        context.setVariable("timeout", timeout);
        context.setVariable("destId", destId);
        context.setVariable("serviceId", serviceId);
        context.setVariable("clientServiceId", clientServiceId);

        String dialPlan = xmlTemplateEngine.process("xml/dialplan", context);

        //log.error(dialPlan);
        return dialPlan;

    }

    private String getRingback(String number) {

        /*Map<String,String> map = new HashMap<>();
        map.put("au", "%(400,200,383,417);%(400,2000,383,417)");
        map.put("be", "%(1000,3000,425)");
        map.put("ca", "%(2000,4000,440,480)");
        map.put("cn", "%(1000,4000,450)");
        map.put("cy", "%(1500,3000,425)");
        map.put("cz", "%(1000,4000,425)");
        map.put("de", "%(1000,4000,425)");
        map.put("dk", "%(1000,4000,425)");
        map.put("dz", "%(1500,3500,425)");
        map.put("eg", "%(2000,1000,475,375)");
        map.put("es", "%(1500,3000,425)");
        map.put("fi", "%(1000,4000,425)");
        map.put("fr", "%(1500,3500,440)");
        map.put("hk", "%(400,200,440,480);%(400,3000,440,480)");
        map.put("hu", "%(1250,3750,425)");
        map.put("il", "%(1000,3000,400)");
        map.put("in", "%(400,200,425,375);%(400,2000,425,375)");
        map.put("jp", "%(1000,2000,420,380)");
        map.put("ko", "%(1000,2000,440,480)");
        map.put("pk", "%(1000,2000,400)");
        map.put("pl", "%(1000,4000,425)");
        map.put("ro", "%(1850,4150,475,425)");
        map.put("rs", "%(1000,4000,425)");
        map.put("ru", "%(800,3200,425)");
        map.put("sa", "%(1200,4600,425)");
        map.put("tr", "%(2000,4000,450)");
        map.put("uk", "%(400,200,400,450);%(400,2000,400,450)");
        map.put("us", "%(2000,4000,440,480)");
        map.put("beep", "%(1000,0,640)");
        map.put("sit", "%(274,0,913.8);%(274,0,1370.6);%(380,0,1776.7)");

        return map.get("es");*/
        return "${es-ring}";
    }
}
