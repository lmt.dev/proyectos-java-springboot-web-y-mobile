/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.services;

import com.wings.voip.domain.sw.entities.Client;
import com.wings.voip.domain.sw.entities.ClientBalance;
import com.wings.voip.domain.sw.entities.ClientDid;
import com.wings.voip.domain.sw.entities.ClientPendingAction;
import com.wings.voip.domain.sw.entities.ClientRoamingFree;
import com.wings.voip.domain.sw.entities.ClientService;
import com.wings.voip.domain.sw.entities.LocalizationCountry;
import com.wings.voip.domain.sw.entities.Service;
import com.wings.voip.domain.sw.entities.ServiceLocalization;
import com.wings.voip.domain.sw.enums.BalanceType;
import com.wings.voip.domain.sw.enums.ServiceAsignations;
import com.wings.voip.domain.sw.enums.ServiceRenovations;
import com.wings.voip.domain.sw.enums.ServiceTypes;
import com.wings.voip.domain.sw.repos.ClientBalanceRepository;
import com.wings.voip.domain.sw.repos.ClientContactRepository;
import com.wings.voip.domain.sw.repos.ClientDidRepository;
import com.wings.voip.domain.sw.repos.ClientPendingActionRepository;
import com.wings.voip.domain.sw.repos.ClientRepository;
import com.wings.voip.domain.sw.repos.ClientRoamingFreeRepository;
import com.wings.voip.domain.sw.repos.ClientServiceRepository;
import com.wings.voip.domain.sw.repos.LocalizationCountryRepository;
import com.wings.voip.domain.sw.repos.ServiceLocalizationRepository;
import com.wings.voip.domain.sw.repos.ServiceRepository;
import com.wings.voip.model.AccountStatus;
import com.wings.voip.model.DidNumberDTO;
import com.wings.voip.model.ServiceDTO;
import com.wings.voip.model.StoreServiceDTO;
import com.wings.voip.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author seba
 */
@org.springframework.stereotype.Service
public class ClientServiceService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ClientServiceRepository clientServiceRepository;

    @Autowired
    private ClientBalanceRepository clientBalanceRepository;

    @Autowired
    private ServiceLocalizationRepository serviceLocalizationRepository;

    @Autowired
    private LocalizationCountryRepository localizationCountryRepository;

    @Autowired
    private ClientPendingActionRepository clientPendingActionRepository;

    @Autowired
    private ClientContactRepository clientContactRepository;

    @Autowired
    private ClientDidRepository clientDidRepository;

    @Autowired
    private ClientRoamingFreeRepository clientRoamingFreeRepository;

    @Autowired
    private DidWWService didWWService;

    /**
     * This function setup initial settings for a new registration adds free
     * plan, gifts and setup everything on the server side
     *
     * @param clientId
     *
     */
    public void initClient(Long clientId) {

        Client client = clientRepository.findOne(clientId);

        Date creationDate = new Date();
        Date neverEnding = DateUtils.getNeverEndingExpiration();
        Date expiration = DateUtils.getNextMonth(creationDate);

        //Add services on registrations
        List<Service> services = serviceRepository.findByAsignationAndActiveOrderBySortOrderAsc(ServiceAsignations.register.name(), Boolean.TRUE);
        services.forEach((service) -> {

            //if the client already has the plan, skip
            ClientService existing = clientServiceRepository.findByClientIdAndServiceId(client.getId(), service.getId());
            if (existing != null) {
                return;
            }

            //if the client does not have the plan and the registration date is not between service valid dates, skip
            if (service.getValidFrom() != null && service.getValidTo() != null) {
                boolean isOk = DateUtils.dateInRange(client.getCreationDate(), service.getValidFrom(), service.getValidTo());
                if (!isOk) {
                    return;
                }
            }

            //check if the client is within valid countries
            if (!service.getValidCountries().equals("all") && service.getValidCountries().contains(client.getCountryIso().toLowerCase())) {
                return;
            }

            ClientService clientService = new ClientService();
            clientService.setActive(true);
            clientService.setClientId(client.getId());
            clientService.setCreationDate(creationDate);
            if (service.getRenovation().equals(ServiceRenovations.never.name())) {
                clientService.setExpirationDate(neverEnding);
            } else if (service.getRenovation().equals(ServiceRenovations.monthly.name())) {
                clientService.setExpirationDate(expiration);
            }
            clientService.setRemainingMinutes(service.getBonusMinutes());
            clientService.setRemainingMoney(service.getBonusMoney());
            clientService.setRenewalDate(null);
            clientService.setServiceId(service.getId());

            //regalos
            if (service.getServiceType().equals(ServiceTypes.gift.name()) && service.getBonusMoney() > 0) {
                ClientBalance balance = new ClientBalance();
                balance.setAmount(service.getBonusMoney());
                balance.setBalanceType(BalanceType.recharge.name());
                balance.setClientId(client.getId());
                balance.setComment("credit-gift");
                balance.setCreationDate(creationDate);
                balance.setDescription(service.getServiceName());
                balance.setReferenceId(service.getId());

                clientBalanceRepository.save(balance);
                client.setBalance(client.getBalance() + balance.getAmount());
            }

            clientServiceRepository.save(clientService);

            //setup second number
            if (service.getServiceCode().equals("didnumber")) {
                try {
                    setupRegisterDidService(clientId, clientService.getId());
                } catch (Exception e) {
                }
            }

        });
        clientRepository.save(client);
    }

    /**
     * Get acount status for one line
     *
     * @param login - phone number
     * @param locale
     * @return
     */
    public AccountStatus getAccountStatus(String login, String locale) {
        Client client = clientRepository.findByLogin(login);
        return getAccountStatus(client.getId(), locale);
    }

    /**
     * get account status for one client
     *
     * @param clientId
     * @param locale
     * @return
     */
    public AccountStatus getAccountStatus(Long clientId, String locale) {

        AccountStatus status = new AccountStatus();
        Client client = clientRepository.findOne(clientId);
        Map<Long, String> freeSecureId = getSecureAndFreeMap();

        List<ServiceDTO> servicesDTO = new ArrayList<>();
        List<ClientService> clientServices = clientServiceRepository.findByClientIdAndActiveOrderByExpirationDateDescIdAsc(clientId, Boolean.TRUE);
        clientServices.stream().map((clientService) -> {

            ServiceLocalization loc = serviceLocalizationRepository.findByServiceIdAndLocCountry(clientService.getServiceId(), client.getCountryIso());
            if (loc == null) {
                loc = serviceLocalizationRepository.findByServiceIdAndLocCountry(clientService.getServiceId(), "es");
            }

            Service service = serviceRepository.findOne(clientService.getServiceId());

            ServiceDTO dto = new ServiceDTO();
            dto.setCode(service.getServiceCode());
            dto.setType(service.getServiceType());
            dto.setServiceId(clientService.getServiceId());
            dto.setCreationDate(DateUtils.getMysqlDateFormat(clientService.getCreationDate()));
            dto.setExpireDate(DateUtils.getMysqlDateFormat(clientService.getExpirationDate()));
            dto.setDescription(loc.getDescription());
            dto.setIconUrl(loc.getImageUrl());
            dto.setName(loc.getDisplayName());
            dto.setRemainingMinutes(clientService.getRemainingMinutes());
            dto.setRemainingMoney(clientService.getRemainingMoney());
            dto.setStatus(clientService.getActive() ? "active" : "inactive");

            if (freeSecureId.containsKey(clientService.getServiceId())) {
                status.setSecure(freeSecureId.get(clientService.getServiceId()).contains("secure"));
            }

            if (service.getServiceCode().equals("didnumber")) {

                status.setHasDid(true);
                List<ClientDid> dids = clientDidRepository.findByClientId(clientId);
                List<DidNumberDTO> toSend = new ArrayList<>();
                dids.stream().map((did) -> {
                    DidNumberDTO d = new DidNumberDTO();
                    d.setId(did.getId());
                    d.setStatus(did.getLocalStatus());
                    d.setExpiry(DateUtils.getMysqlDateFormat(did.getExpirationDate()));
                    d.setNumber(did.getDidNumber());
                    return d;
                }).forEachOrdered((d) -> {
                    toSend.add(d);
                });
                dto.setDids(toSend);

            }

            return dto;
        }).forEachOrdered((dto) -> {
            servicesDTO.add(dto);
        });

        LocalizationCountry localizationCountry = localizationCountryRepository.findByDefaultLocale(locale != null ? locale : client.getLocale());
        if (localizationCountry == null) {
            localizationCountry = localizationCountryRepository.findByDefaultLocale("es_ES");
        }

        Double balance = client.getBalance() * localizationCountry.getExchangeRate();
        status.setClientBalance(balance);
        status.setServices(servicesDTO);

        //roamingFree
        ClientRoamingFree rf = clientRoamingFreeRepository.findByClientId(clientId);
        status.setRoamingFreeEnabled(rf != null);

        return status;

    }

    /**
     * get line services, already bought
     *
     * @param login
     * @param locale
     * @return
     */
    public List<StoreServiceDTO> getStoreServices(String login, String locale) {
        Client client = clientRepository.findByLogin(login);
        return getStoreServices(client.getId(), locale);
    }

    /**
     * get availe items to buy
     *
     * @param clientId
     * @param locale
     * @return
     */
    public List<StoreServiceDTO> getStoreServices(Long clientId, String locale) {

        Client client = clientRepository.findOne(clientId);
        //LocalizationCountry localizationCountry = localizationCountryRepository.findByDefaultLocale(locale != null ? locale : client.getLocale());        
        LocalizationCountry localizationCountry = localizationCountryRepository.findByCountryCode(client.getCountryIso());

        List<StoreServiceDTO> ret = new ArrayList<>();
        List<Service> services = serviceRepository.findByAsignationAndActive(ServiceAsignations.store.name(), Boolean.TRUE);
        for (Service service : services) {
            //servicio valido para el cliente
            if (service.getValidCountries().equals("all") || service.getValidCountries().contains(client.getCountryIso().toUpperCase())) {

                ServiceLocalization loc = serviceLocalizationRepository.findByServiceIdAndLocCountry(service.getId(), client.getCountryIso());
                if (loc == null) {
                    loc = serviceLocalizationRepository.findByServiceIdAndLocCountry(service.getId(), "es");
                }

                StoreServiceDTO dto = new StoreServiceDTO();
                dto.setServiceId(service.getId());
                dto.setCode(service.getServiceCode());

                dto.setCost(localizationCountry.getExchangeRate() * service.getCost());
                if (loc.getCost() != 0) {
                    dto.setCost(localizationCountry.getExchangeRate() * loc.getCost());
                }

                //apply only localized price if user locale it's the same as user country
                if (loc.getLocalizedCost() != 0 && localizationCountry.getCountryCode().equals(client.getCountryIso())) {
                    dto.setCost(loc.getLocalizedCost());
                }

                dto.setDescription(loc.getDescription());
                dto.setIconUrl(loc.getImageUrl());
                dto.setName(loc.getDisplayName());

                ret.add(dto);
            }
        }

        return ret;
    }

    /**
     * setup did service
     *
     * @param clientId
     * @param clientServiceId
     */
    public void setupRegisterDidService(Long clientId, Long clientServiceId) {

        Date creationDate = new Date();
        Date expirationDate = DateUtils.getNextMonth(creationDate);

        ClientDid did = new ClientDid();
        did.setClientId(clientId);
        did.setClientServiceId(clientServiceId);
        did.setCity(null);
        did.setCountry(null);
        did.setCreationDate(creationDate);
        did.setDidNumber(null);
        did.setExpirationDate(expirationDate);
        did.setLocalStatus("created");
        did.setRemoteStatus(null);
        did.setRemoteSku(null);
        did.setReservationId(null);

        clientDidRepository.save(did);
    }

    /**
     *
     * @param serviceId
     * @param clientId
     * @param extend
     * @throws Exception
     */
    public void buyService(Long serviceId, Long clientId, boolean extend) throws Exception {

        Client client = clientRepository.findOne(clientId);
        Service service = serviceRepository.findOne(serviceId);
        ServiceLocalization loc = serviceLocalizationRepository.findByServiceIdAndLocCountry(service.getId(), client.getCountryIso());

        //if the client does not have the plan and the registration date is not between service valid dates, skip
        if (service.getValidFrom() != null && service.getValidTo() != null) {
            boolean isOk = DateUtils.dateInRange(client.getCreationDate(), service.getValidFrom(), service.getValidTo());
            if (!isOk) {
                throw new Exception("error_service_date_not_valid");
            }
        }

        //check if the client is within valid countries
        if (!service.getValidCountries().equals("all") && service.getValidCountries().contains(client.getCountryIso().toLowerCase())) {
            throw new Exception("error_service_country_not_valid");
        }

        double amount = service.getCost();
        if (loc.getCost() > 0) {
            amount = loc.getCost();
        }

        if (client.getBalance() < amount) {
            throw new Exception("error_not_enough_credit");
        }

        ClientService clientService;
        Date creationDate = new Date();
        Date expirationDate;

        //extend expiration date
        if (extend) {
            clientService = clientServiceRepository.findByClientIdAndServiceId(clientId, serviceId);

            expirationDate = clientService.getExpirationDate();
            switch (service.getRenovation()) {
                case "monthly":
                    expirationDate = DateUtils.getNextMonth(expirationDate);
                    break;
                case "yearly":
                    expirationDate = DateUtils.getNextYear(expirationDate);
                    break;
                default:
                    expirationDate = DateUtils.getNextMonth(expirationDate);
            }
            clientService.setExpirationDate(expirationDate);
            clientServiceRepository.save(clientService);

        } else {
            //buy new one
            switch (service.getRenovation()) {
                case "monthly":
                    expirationDate = DateUtils.getNextMonth(creationDate);
                    break;
                case "yearly":
                    expirationDate = DateUtils.getNextYear(creationDate);
                    break;
                default:
                    expirationDate = DateUtils.getNextMonth(creationDate);
            }

            //create client service
            clientService = new ClientService();
            clientService.setActive(true);
            clientService.setClientId(client.getId());
            clientService.setCreationDate(creationDate);
            clientService.setExpirationDate(expirationDate);
            clientService.setRemainingMinutes(service.getBonusMinutes());
            clientService.setRemainingMoney(service.getBonusMoney());
            clientService.setRenewalDate(null);
            clientService.setServiceId(service.getId());
            clientService.setStatus("active");
            clientService.setDidNumber(null);
            clientServiceRepository.save(clientService);
        }

        //add did number
        if (service.getServiceCode().equals("didnumber")) {
            if (!extend) {
                ClientDid did = new ClientDid();
                did.setClientId(clientId);
                did.setClientServiceId(clientService.getId());
                did.setCity(null);
                did.setCountry(null);
                did.setCreationDate(creationDate);
                did.setDidNumber(null);
                did.setExpirationDate(expirationDate);
                did.setLocalStatus("created");
                did.setRemoteStatus(null);
                did.setRemoteSku(null);
                did.setReservationId(null);
                clientDidRepository.save(did);
            } else {
                ClientDid did = clientDidRepository.findByClientIdAndClientServiceId(clientId, clientService.getId());
                did.setExpirationDate(expirationDate);
                clientDidRepository.save(did);
            }
        }

        //create balance item
        ClientBalance balance = new ClientBalance();
        balance.setAmount(amount * -1);
        balance.setBalanceType(BalanceType.buy_store.name());
        balance.setClientId(client.getId());
        if (extend) {
            balance.setComment("service-extend-" + service.getServiceCode());
        } else {
            balance.setComment("service-buy-" + service.getServiceCode());
        }
        balance.setCreationDate(creationDate);
        balance.setDescription("Purchase service " + service.getServiceName());
        balance.setReferenceId(clientService.getId());
        clientBalanceRepository.save(balance);

        //update client balance
        client.setBalance(client.getBalance() - amount);
        clientRepository.save(client);

        //update remaining money on gifts
        double amountCopy = amount;
        List<ClientService> cservices = clientServiceRepository.findByClientIdAndActiveAndRemainingMoneyGreaterThanOrderByIdAsc(client.getId(), Boolean.TRUE, 0);
        for (ClientService cservice : cservices) {
            double remaining = cservice.getRemainingMoney();
            //el primero cobra todo
            if (remaining - amountCopy > 0) {
                cservice.setRemainingMoney(remaining - amountCopy);
                break;
            }
            amountCopy = amountCopy - remaining;
            cservice.setRemainingMoney(0.0);
        }

        //send push to all my secure contacts to update contacts info
        if (service.getServiceCode().equals("secure")) {

            clientContactRepository.setMeSecureOnContacts(client.getLogin());

            ClientPendingAction clientPendingAction = new ClientPendingAction();
            clientPendingAction.setAction("secure-buy");
            clientPendingAction.setCompleted(false);
            clientPendingAction.setCompletionDate(null);
            clientPendingAction.setCreationDate(creationDate);
            clientPendingAction.setDisabled(false);
            clientPendingAction.setFailureCount(0);
            clientPendingAction.setLogin(client.getLogin());
            clientPendingAction.setPushToken(null);
            clientPendingAction.setSendCount(0);
            clientPendingAction.setClientId(client.getId());
            clientPendingActionRepository.save(clientPendingAction);
        }

        clientServiceRepository.save(cservices);
    }

    /**
     * get line country
     *
     * @param login
     * @return
     */
    public LocalizationCountry getCountry(String login) {
        List<LocalizationCountry> countries = localizationCountryRepository.findAll();
        for (LocalizationCountry country : countries) {
            if (("+" + login).startsWith(country.getCountryPrefix())) {
                return country;
            }
        }
        return null;
    }

    /**
     * Return a map with key id and free or secure as value
     *
     * @return
     */
    public Map<Long, String> getSecureAndFreeMap() {
        //map {id=secure,id=free}
        Map<Long, String> freeSecureId = new HashMap<>();
        List<com.wings.voip.domain.sw.entities.Service> planes = serviceRepository.findByServiceCodeIn(new String[]{"free", "secure"});
        planes.forEach((plane) -> {
            freeSecureId.put(plane.getId(), plane.getServiceCode());
        });
        return freeSecureId;
    }

    @Scheduled(cron = "0 0 * * * *")    
    public void expireServices() {

        List<ClientService> services = clientServiceRepository.findByActive(Boolean.TRUE);
        for (ClientService clientService : services) {

            //TODO: autorenew free from register
            /*Service service = clientService.getService();
            if (service.getAsignation().equals(ServiceAsignations.register.name())) {
                long clientId = clientService.getClientId();
                Client client = clientRepository.getOne(clientId);
            }*/
            Date exp = clientService.getExpirationDate();
            long days = DateUtils.getDiffDays(new Date(), exp);
            if (days <= 0) {
                clientService.setActive(false);
                clientService.setStatus("expired");
            }
        }
        clientServiceRepository.save(services);

        expireDids();
    }

    public void expireDids() {
        //did actualy setup
        List<ClientDid> setups = clientDidRepository.findByLocalStatus("setup");
        for (ClientDid setup : setups) {
            if (setup.getClientService().getStatus() != null && setup.getClientService().getStatus().equals("expired")) {
                //expiro did y marco did como expired
                if (didWWService.cancelSubscription(setup)) {
                    setup.setLocalStatus("expired");
                }
            }
        }
        clientDidRepository.save(setups);
    }

}
