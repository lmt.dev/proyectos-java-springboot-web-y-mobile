/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.didww;

import com.wings.voip.domain.sw.entities.DidCountry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author maurib
 */
public class DidSpecs {
    
    public static Specification<DidCountry> search(String q) {

       return (Root<DidCountry> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                String search = "%" + q + "%";
                predicates.add(
                        builder.or(
                                builder.like(root.get("name"), search)
                        )
                );
            }       
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
