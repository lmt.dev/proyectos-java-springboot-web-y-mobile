/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.voip.domain.cdr;

/**
 *
 * @author seba
 */
public class Times {

    private String created_time;
    private String profile_created_time;
    private String progress_time;
    private String progress_media_time;
    private String answered_time;
    private String hangup_time;
    private String transfer_time;

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getProfile_created_time() {
        return profile_created_time;
    }

    public void setProfile_created_time(String profile_created_time) {
        this.profile_created_time = profile_created_time;
    }

    public String getProgress_time() {
        return progress_time;
    }

    public void setProgress_time(String progress_time) {
        this.progress_time = progress_time;
    }

    public String getProgress_media_time() {
        return progress_media_time;
    }

    public void setProgress_media_time(String progress_media_time) {
        this.progress_media_time = progress_media_time;
    }

    public String getAnswered_time() {
        return answered_time;
    }

    public void setAnswered_time(String answered_time) {
        this.answered_time = answered_time;
    }

    public String getHangup_time() {
        return hangup_time;
    }

    public void setHangup_time(String hangup_time) {
        this.hangup_time = hangup_time;
    }

    public String getTransfer_time() {
        return transfer_time;
    }

    public void setTransfer_time(String transfer_time) {
        this.transfer_time = transfer_time;
    }

    
}
