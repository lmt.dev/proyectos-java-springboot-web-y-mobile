$(document).ready(function ($) {

    $('#search-button-destination').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());

        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'pricelist-add') {

            var url = "/views/pricelist/add/";
            loadInDialog(url, "Crear Lista de Precio", null, null);

        } else if (action === 'pricelist-edit') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/views/pricelist/edit/" + id;

            loadInDialog(url, "Editar " + name, false, false);

        } else if (action === 'delete-list-price') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/views/pricelist/delete/" + id;

            confirmDialogDanger('Eliminar la lista de precio', '¿Confirma eliminacion de la lista de precios? <br\><b>Se eliminaran los destinos asignados a esta lista!</b> ' + name, function (result) {
                if (result) {
                    $.post(url, null, function (response) {
                        location.reload();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });
        
        } else if (action === 'delete-prices') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/views/pricelist/delete/" + id + "/prices";

            confirmDialogDanger('Eliminar los precios de la lista', '¿Confirma eliminacion de precios? <br\><b>Se eliminaran los destinos asignados a esta lista pero no la lista!</b> ' + name, function (result) {
                if (result) {
                    $.post(url, null, function (response) {
                        location.reload();
                    }).fail(function (xhr, status, error) {
                        notifyError(xhr.responseText);
                    });
                }
            });

        }

    });

});
