$(document).ready(function ($) {
    $('#form-submit-list-price').click(function (e) {
        e.preventDefault();
        var url = "/views/pricelist/add";
        $.post(url, $('#add-price-form').serialize(), function (response) {
            location.reload();
        });
    });
});