$(document).ready(function ($) {


    $('#search-button-destination').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        url = addParam(url, 'q', $('#q').val());

        setUpUrl(url);
    });


    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');
        var titleDestinationList = "Crear Lista de Destinos";
        var titlePriceList = "Crear Lista de Precio";

        if (action === 'crear-lista') {

            var urlCreate = "/views/servicesListPriceDestination/abm/";

            loadInDialog(urlCreate, titleDestinationList, null, null);

        } else if (action === 'crear-lista-precio') {

            var urlCreation = "/views/servicesListPrice/abm/";

            loadInDialog(urlCreation, titlePriceList, null, null);

        } else if (action === 'edit-list-destination') {

            var idEdit = $(e.target).data('id');
            var idEditName = $(e.target).data('name');
            var titleEditDestination = 'Editar ' + idEditName;
            var urlEditDestination = "/view/servicesDestination/edit/"+ idEdit;
            loadInDialog(urlEditDestination, titleEditDestination, false, false);

        } else if (action === 'edit-price-list') {

            var idEdit = $(e.target).data('id');
            var idEditName = $(e.target).data('name');
            var titleEditPriceList = 'Editar ' + idEditName;
            var urlEdit = "/view/servicesPriceList/edit/"+ idEdit;
            loadInDialog(urlEdit, titleEditPriceList, false, false);

        } else if (action === 'delete-list-destination') {
            var idDeleteDestination = $(e.target).data('id');
            var nameDestinationList = $(e.target).data('name');
            var titleDeleteDestination = 'Delete Lista ' + nameDestinationList;
            var urlDeleteDestination = "/view/servicesListPriceDestination/delete/" + idDeleteDestination;

            loadInDialog(urlDeleteDestination, titleDeleteDestination, false, false);

            location.reload();

        } else if (action === 'delete-list-price') {
            var idDelete = $(e.target).data('id');
            var namePriceList = $(e.target).data('name');
            var titleDelete = 'Delete Lista ' + namePriceList;
            var urlDelete = "/view/servicesPriceList/delete/" + idDelete;

            loadInDialog(urlDelete, titleDelete, false, false);

            location.reload();
        } else if(action === 'add-import-csv') {

            var title = 'Subir Lista de Destinos ';
            var url = '/view/servicesDestination/importGet';

            loadInDialog(url, title, false, false);
        }

    });

});
