$(document).ready(function ($) {



    $('.action-button').click(function (e) {

        e.preventDefault();
        var action = $(e.target).attr('id');

        if (action === 'send-push') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/view/clientDetails/" + id + "/push/";

            loadInDialog(url, "Enviar push a " + name, false, false);

        } else if (action === 'view-contacts') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');
            var url = "/views/clients/contacts/" + id;

            loadInDialog(url, "Contacts for " + name, false, true);

        } else if (action === 'add-credit') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir credito a ' + name;
            var url = '/views/clients/balance/' + id + "/credit";

            loadInDialog(url, title, false, false);
        } else if (action === 'add-credit-details') {

            var id = $(e.target).data('id');
            var name = $(e.target).data('name');

            var title = 'Añadir credito a ' + name;
            var url = '/views/clientDetails/balance/' + id + "/credit";

            loadInDialog(url, title, false, false);
        } else if (action === 'view-details') {

            var url = $(e.target).attr('href');
            location.href = url;
            location.reload();


        } else if (action === 'view-clients') {

            var url = $(e.target).attr('href');
            location.href = url;
            location.reload();
        }
    });


});
