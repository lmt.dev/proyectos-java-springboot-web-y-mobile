$(document).ready(function ($) {

    $('#form-submit-destination-edit').click(function (e) {
        e.preventDefault();
        var url = "/views/servicesDestinationListEdit/post";
        var name = $("#name").val();
        var countryCode = $("#countryCode").val();
        var areaCode = $("#areaCode").val();
        var priceDestination = $("#priceDestination").val();
        var priceListDestinationSelected = $('#priceListDestination').val();
        var id = $(e.target).data('id');

        $.post(url, {
            name: name,
            countryCode: countryCode,
            areaCode: areaCode,
            priceDestination: priceDestination,
            priceListDestinationSelected: priceListDestinationSelected,
            id:id
        }, function (response) {

            $('#search-button-destination-edit').click();

            location.reload();
        });

    });
});
