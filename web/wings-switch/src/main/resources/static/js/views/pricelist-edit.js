$(document).ready(function ($) {
    $('#form-submit-list-price').click(function (e) {
        e.preventDefault();

        var id = $('#pricelistId').val();
        var url = "/views/pricelist/edit/" + id;

        $.post(url, $('#edit-price-form').serialize(), function (response) {
            location.reload();
        });
    });
});