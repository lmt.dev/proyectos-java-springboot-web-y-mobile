(()=>{
    const addEyeStyle = () => {
        const gwEyes = document.querySelectorAll("[name^=gateway-eye]");
        gwEyes.forEach( x => {
            x.addEventListener('mouseover', () =>{
                x.setAttribute("class", "fas fa-eye fa-2x");
                x.setAttribute("style", "color: #1683BD;");
            });
            x.addEventListener('mouseout', () =>{
                x.setAttribute("class", "far fa-eye fa-2x");
            })
        })
    }

    const addLabels = () => {
        const labels = document.querySelectorAll("[name^=gateway-label-enabled]");
        labels.forEach( x => {
            if(x.innerText == 'SI') {
                x.classList.add('label-enabled');
            }
            if(x.innerText == 'NO') {
                x.classList.add('label-not-enabled');
            }
        });
    }

    const addGatewayCountries = () => {
        const gatewayCountries = document.querySelectorAll('[name="gatewayFlag"]');

        const countries = [
            ['AR', 'argentina'],
            ['BO', 'bolivia'],
            ['PE', 'peru'],
            ['BR', 'brazil'],
            ['CL', 'chile'],
            ['CO', 'colombia'],
            ['CR', 'costa-rica'],
            ['DO', 'dominican-republic'],
            ['EC', 'ehcuador'],
            ['ES', 'spain'],
            ['GT', 'guatemala'],
            ['HN', 'honduras'],
            ['IT', 'italy'],
            ['MX', 'mexico'],
            ['OT', 'cancel-2'],
            ['PA', 'panama'],
            ['SV', 'el-salvador'],
            ['US', 'usa']
        ];

        const flags = 'https://img.icons8.com/color/30/000000/';
        gatewayCountries.forEach( gc => {
            let img = document.createElement('img');
            countries.forEach( country => {
                if(gc.innerText === country[0]) {
                    gc.innerHTML = '';
                    img.src = flags.concat(country[1].concat('.png'));
                    gc.appendChild(img);
                }
            })
        })
    }

    const getUriParams = () => {
        let urlParams = new URLSearchParams();
        if(sessionStorage.getItem('gatewayEnabled') != null) {
            urlParams.append('gatewayEnabled', sessionStorage.getItem('gatewayEnabled'));
        }
        if(sessionStorage.getItem('gatewayCountry') != null) {
            urlParams.append('gatewayCountry', sessionStorage.getItem('gatewayCountry'));
        }
        if(sessionStorage.getItem('gatewayGw') != null) {
            urlParams.append('gatewayGw', sessionStorage.getItem('gatewayGw'));
        }
        return urlParams;
    }

    const filter = () => {
        const btnFilter = document.querySelector('#btn-filter');
        btnFilter.addEventListener('click', async () => {
            const gatewayCountry = document.querySelector('#gatewayCountry');
            const gatewayGw = document.querySelector('#gatewayGw');
            const gatewayEnabled = document.querySelector('#gatewayEnabled');

            const arr = [gatewayCountry, gatewayGw, gatewayEnabled];
            arr.forEach( x => {
                if(x.value == '-1') {
                    sessionStorage.removeItem(x.name);
                } else {
                    sessionStorage.setItem(x.name, x.value.toString());
                }
            })

            const uri = '/admin/gateways?';
            await fetch(uri + getUriParams());
        })
    }

    const paginator = () => {
        let uri = '';
        const paginationItems = document.querySelectorAll('[name="pagination"]');
        paginationItems.forEach( item => {
            if(item.getAttribute('href') != null) {
                item.addEventListener('click', async ()=>{
                    uri = item.getAttribute('href');
                    if(sessionStorage.getItem('gatewayCountry') == null &&
                        sessionStorage.getItem('gatewayGw') == null &&
                        sessionStorage.getItem('gatewayEnabled') == null) {
                        await fetch(uri);
                    } else { await fetch(uri + getUriParams()); }
                })
            }
        })
    }
    paginator();
    filter();
    addEyeStyle();
    addLabels();
    addGatewayCountries();
})();

