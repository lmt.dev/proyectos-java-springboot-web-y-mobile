const init = () => {
    const btnCrearGateway = document.querySelector('#btn-crear');
    btnCrearGateway.addEventListener('click', async() => {

        const ddCountry = document.querySelector('#country');

        if (ddCountry != null && ddCountry.value != 0) {
            const country = $("#country").val();
            const gatewayId = $("#gatewayId").val();
            const gatewayname = $("#gatewayId option:selected").text();
            const enabled = $("#enabled").val() === "1" ? "true" : "false";
            const gi = {
                gatewayId: gatewayId,
                name: gatewayname,
                country: country,
                enabled: enabled
            };
            const dataProperties = [];

            $(".gateways-properties-prod").each(function () {
                const propertyId = $(this).data("id");
                const propertyValue = $("#property-value-prod-" + propertyId).val();
                const propertyName = document.getElementById("property-value-prod-" + propertyId).dataset.propertyname;
                const obj = {
                    propertyId: propertyId,
                    propertyName: propertyName,
                    propertyValue: propertyValue,
                    propertyTest: false
                };
                dataProperties.push(obj);
            });

            $(".gateways-properties-prod").each(function () {
                const propertyId = $(this).data("id");
                const propertyValue = $("#property-value-test-" + propertyId).val();
                const propertyName = document.getElementById("property-value-test-" + propertyId).dataset.propertyname;
                const obj = {
                    propertyId: propertyId,
                    propertyName: propertyName,
                    propertyValue: propertyValue,
                    propertyTest: true
                };
                dataProperties.push(obj);
            });

            const finalObject = {
                gi: gi,
                properties: dataProperties
            };

            let endpoint = "/admin/create-gateway";
            const createSuccessMsg = document.querySelector('#i18n_create_gtw_success');
            const createErrorMsg = document.querySelector('#i18n_create_gtw_error');

            let json = JSON.stringify(finalObject);

            const resp = await fetch(endpoint, {
                method: 'POST',
                body: json,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            return (resp.status === 201) ? swal(createSuccessMsg.innerHTML, {
                icon: "success",
                button: false,
                timer: 2000,
            }).then(() => {
                if (this.getState === undefined) {
                    window.location = '/admin/gateways';
                }
            }) : swal(createErrorMsg.innerHTML, {
                icon: "error",
                button: false,
                timer: 2000,
            }).then(() => {
                if (this.getState === undefined) {
                    window.location.reload();
                }
            });
        } else {
            const selectCountry = document.querySelector('#select-country-error');
            selectCountry.setAttribute('style', 'color:red; margin-top:-1em; margin-bottom:-1em;');
            selectCountry.hidden = false;
        }
    })

    $("#gatewayId").change(function () {
        var id = $(this).val();

        url = "/admin/gateway-properties/" + id

        $.get(url, function (response) {
            $("#divGatewayProperties").html(response);
            $("#btn-crear").removeAttr('hidden');
        });
    });
}

init();