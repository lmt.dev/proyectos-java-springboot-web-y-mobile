(() =>{
    const init = () => {
        const paymentStatus   = document.querySelectorAll('[name="paymentStatus"');
        const statusPending   = document.querySelector('#status-pending');
        const statusCompleted = document.querySelector('#status-completed');
        const statusFailed = document.querySelector('#status-failed');
        const statusWs        = document.querySelector('#status-ws');

        paymentStatus.forEach( x => {
            if(x.innerText.includes('PENDING')) {
                x.classList.add('label-warning');
                x.innerText = statusPending.innerText;
            }
            if(x.innerText.includes('COMPLETED')) {
                x.classList.add('label-success');
                x.innerText = statusCompleted.innerText;
            }
            if(x.innerText.includes('FAILED_WRONG_SIGNATURE')) {
                x.classList.add('label-danger');
                x.innerText = statusWs.innerText;
            }
            if(x.innerText.includes('FAILED')) {
                x.classList.add('label-danger');
                x.innerText = statusFailed.innerText;
            }
        });

        const addGatewayCountries = () => {
            const paymentCountries = document.querySelectorAll('[name="paymentFlag"]');
            const countries = [
                ['AR', 'argentina'],
                ['BO', 'bolivia'],
                ['PE', 'peru'],
                ['BR', 'brazil'],
                ['CL', 'chile'],
                ['CO', 'colombia'],
                ['CR', 'costa-rica'],
                ['DO', 'dominican-republic'],
                ['EC', 'ehcuador'],
                ['ES', 'spain'],
                ['GT', 'guatemala'],
                ['HN', 'honduras'],
                ['IT', 'italy'],
                ['MX', 'mexico'],
                ['OT', 'cancel-2'],
                ['PA', 'panama'],
                ['SV', 'el-salvador'],
                ['US', 'usa']
            ];

            const flags = 'https://img.icons8.com/color/30/000000/';
            paymentCountries.forEach( gc => {
                let img = document.createElement('img');
                countries.forEach( country => {
                    if(gc.innerText === country[0]) {
                        gc.innerHTML = '';
                        img.src = flags.concat(country[1].concat('.png'));
                        img.setAttribute('title',country[1]);
                        gc.appendChild(img);
                    }
                })
            })
        }

        const addEyeStyle = () => {
            const gwEyes = document.querySelectorAll("[name^=payment-eye]");
            gwEyes.forEach( x => {
                x.addEventListener('mouseover', () =>{
                    x.setAttribute("class", "fas fa-eye fa-2x");
                    x.setAttribute("style", "color: #1683BD;");
                });
                x.addEventListener('mouseout', () =>{
                    x.setAttribute("class", "far fa-eye fa-2x");
                })
            })
        }

        addGatewayCountries();
        addEyeStyle();
    }
    init();
})();