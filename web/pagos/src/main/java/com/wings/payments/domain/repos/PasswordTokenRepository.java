package com.wings.payments.domain.repos;

import com.wings.payments.domain.login.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    PasswordResetToken findByToken(String token);

    PasswordResetToken findEmailByToken(String token);
}
