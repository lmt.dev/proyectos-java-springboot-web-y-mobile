/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.paymentez;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.services.CallbackService;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PaymentezService {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private CallbackService callbackService;

    private static final Map<String, String> TRANSLATIONS = new HashMap<>();

    static {
        TRANSLATIONS.put("0", "Esperando pago.");
        TRANSLATIONS.put("1", "Verificaciòn necesaria, por favor revise el área de verificación.");
        TRANSLATIONS.put("3", "Transacción exitosa.");
        TRANSLATIONS.put("6", "Transaccion rechazada por fraude.");
        TRANSLATIONS.put("7", "Refund.");
        TRANSLATIONS.put("8", "Chargeback");
        TRANSLATIONS.put("9", "Transaccion rechazada por el operador.");
        TRANSLATIONS.put("10", "Error de sistema.");
        TRANSLATIONS.put("11", "Rechazada por Paymentez (fraude).");
        TRANSLATIONS.put("12", "Rechazada por Paymentez (blacklist).");
        TRANSLATIONS.put("13", "Tiempo expirado.");
        TRANSLATIONS.put("19", "Código de autorizacion inválido.");
        TRANSLATIONS.put("20", "Código de autorizacion expirado.");
        TRANSLATIONS.put("21", "Paymentez Fraud - Pending refund.");
        TRANSLATIONS.put("22", "Invalid AuthCode - Pending refund.");
        TRANSLATIONS.put("23", "AuthCode expired - Pending refund.");
        TRANSLATIONS.put("24", "Paymentez Fraud - Refund requested.");
        TRANSLATIONS.put("25", "Invalid AuthCode - Refund requested.");
        TRANSLATIONS.put("26", "AuthCode expired - Refund requested.");
        TRANSLATIONS.put("27", "Merchant - Pending refund.");
        TRANSLATIONS.put("28", "Merchant - Refund requested.");
        TRANSLATIONS.put("29", "Annulled.");
        TRANSLATIONS.put("30", "Transaction seated (only Equador).");
        TRANSLATIONS.put("31", "Waiting for OTP.");
        TRANSLATIONS.put("32", "OTP successfully validated.");
        TRANSLATIONS.put("33", "OTP not validated.");
        TRANSLATIONS.put("34", "Partial refund.");
        TRANSLATIONS.put("35", "3DS method requested, waiting to continue.");
        TRANSLATIONS.put("36", "3DS challenge requested, waiting CRES.");
        TRANSLATIONS.put("37", "Rejected by 3DS.");
    }

    /**
     *
     * @param response
     */
    public void processResponse(Map<String, Map<String, String>> response) {

        String status;
        String statusMessage;

        if (response.get("transaction") != null) {
            Map<String, String> transac = response.get("transaction");

            String theyStatus = transac.get("status");
            String compoundId = transac.get("dev_reference");
            String message = transac.get("message");
            String statusDetail = "--";
            try {
                statusDetail = String.valueOf(transac.get("status_detail"));
            } catch (Exception e) {
            }

            //get payment request
            //TODO: Traducir status messages a mi status message
            if (theyStatus.equals("success")) {
                status = PaymentStatus.COMPLETED.name();
            } else {
                status = PaymentStatus.FAILED.name();
            }
            if (TRANSLATIONS.containsKey(statusDetail)) {
                statusMessage = TRANSLATIONS.get(statusDetail);
            } else {
                statusMessage = message;
            }

            String paymentRequestId = compoundId.split("-")[0];
            PaymentRequest payReq = paymentRequestRepository.getOne(Long.parseLong(paymentRequestId));
            PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

            ptx.setLastStatus(status);
            payReq.setStatus(status);
            payReq.setStatusMessage(statusMessage);
            payReq.setStatusShown(false);
            payReq.setStatusDate(new Date());
            payReq.setProcessor("paymentez");

            paymentRequestRepository.save(payReq);
            paymentTransactionRepository.save(ptx);

            //save new callback and values
            //create transacion callback
            PaymentTransactionCallback cbk = new PaymentTransactionCallback();
            cbk.setCallbackDate(new Date());
            cbk.setPaymentTransactionId(ptx.getId());
            cbk.setReportedDate(new Date());
            cbk.setStatus(status);
            paymentTransactionCallbackRepository.save(cbk);

            //create extra values for json
            PaymentTransactionCallbackValue object = new PaymentTransactionCallbackValue();
            object.setCallbackId(cbk.getId());
            object.setValName("response");
            object.setValValue(response.toString());
            paymentTransactionCallbackValueRepository.save(object);

            //send callback
            callbackService.postCallback(payReq);
        }
    }
}
