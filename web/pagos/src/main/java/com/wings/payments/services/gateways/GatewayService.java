package com.wings.payments.services.gateways;

import com.wings.payments.domain.bo.Gateway;
import com.wings.payments.domain.repos.GatewaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GatewayService {

    @Autowired
    private GatewaysRepository gatewaysRepository;

    public List<Gateway> findAllWithFilters() {
        return gatewaysRepository.findAllWithFilters();
    }

    public Gateway findById(Long id) {
        return gatewaysRepository.findById(id).orElse(null);
    }
}
