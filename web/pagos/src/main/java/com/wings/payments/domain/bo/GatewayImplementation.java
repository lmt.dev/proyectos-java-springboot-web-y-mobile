/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.bo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Where;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "gateway_implementation")
public class GatewayImplementation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "gateway_id")
    private Long gatewayId;
    @Size(max = 5)
    @Column(name = "country")
    private String country;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Column(name = "enabled")
    private boolean enabled;
    @Column(name = "test")
    private boolean test;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "gateway_implementation_id", nullable = true, updatable = false, insertable = false)
    @Where(clause = "property_test = 0")
    private List<GatewayImplementationProperty> properties;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "gateway_implementation_id", nullable = true, updatable = false, insertable = false)
    @Where(clause = "property_test = 1")
    private List<GatewayImplementationProperty> testProperties;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gateway_id", updatable = false, insertable = false, nullable = true)
    private Gateway gateway;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Long gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<GatewayImplementationProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<GatewayImplementationProperty> properties) {
        this.properties = properties;
    }

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public String getPropertyByName(String name) {
        String ret = null;
        for (GatewayImplementationProperty property : properties) {
            if (property.getPropertyName().equals(name)) {
                ret = property.getPropertyValue();
                break;
            }
        }
        return ret;
    }

    public List<GatewayImplementationProperty> getTestProperties() {
        return testProperties;
    }

    public void setTestProperties(List<GatewayImplementationProperty> testProperties) {
        this.testProperties = testProperties;
    }

    public boolean getTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }
}
