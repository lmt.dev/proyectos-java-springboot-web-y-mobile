/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways;

import com.wings.payments.domain.bo.GatewayImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class PaymentGatewayFactory {

    private final Logger log = LoggerFactory.getLogger(PaymentGatewayFactory.class);

    /**
     * Create PaymentGateway from gatewayImplementation
     *
     * @param gi
     * @return
     */
    public PaymentGateway getImplementation(GatewayImplementation gi) {
        PaymentGateway ret = null;
        try {
            Class<?> clazz = Class.forName("com.wings.payments.gateways.impl." + gi.getGateway().getServiceClassName());
            Object newIns = clazz.newInstance();
            ret = (PaymentGateway) newIns;
            ret.init(gi);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            log.error("Error creating gateway", e);
        }
        return ret;
    }
   
}
