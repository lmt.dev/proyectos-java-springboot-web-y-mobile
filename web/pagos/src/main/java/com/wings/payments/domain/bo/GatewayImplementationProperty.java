/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.bo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "gateway_implementation_properties")
public class GatewayImplementationProperty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "gateway_implementation_id")
    private Long gatewayImplementationId;
    @Column(name = "property_id")
    private Long propertyId;
    @Size(max = 50)
    @Column(name = "property_name")
    private String propertyName;

    @Column(name = "property_value")
    private String propertyValue;
    @Column(name = "property_test")
    private Boolean propertyTest;

    public GatewayImplementationProperty(Long id, Long gatewayImplementationId, Long propertyId, @Size(max = 50) String propertyName, @Size(max = 255) String propertyValue, Boolean propertyTest) {
        this.id = id;
        this.gatewayImplementationId = gatewayImplementationId;
        this.propertyId = propertyId;
        this.propertyName = propertyName;
        this.propertyValue = propertyValue;
        this.propertyTest = propertyTest;
    }

    public GatewayImplementationProperty() {
    }

    public GatewayImplementationProperty(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGatewayImplementationId() {
        return gatewayImplementationId;
    }

    public void setGatewayImplementationId(Long gatewayImplementationId) {
        this.gatewayImplementationId = gatewayImplementationId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public Boolean getPropertyTest() {
        return propertyTest;
    }

    public void setPropertyTest(Boolean propertyTest) {
        this.propertyTest = propertyTest;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

}
