/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author seba
 */
public class NumberUtils {

    private static final Random rand = new Random();
    private static final DecimalFormat dataFormat = new DecimalFormat("0.00");
    private static final NumberFormat moneyFormat = NumberFormat.getCurrencyInstance(new Locale("es", "ES"));

    private static final Map<String, Double> CONVERSION_RATES = new HashMap<>();

    static {
        CONVERSION_RATES.put("ES", 1.0);
        CONVERSION_RATES.put("IT", 1.0);
        CONVERSION_RATES.put("CO", 3524.73);
        CONVERSION_RATES.put("PE", 3.88);
        CONVERSION_RATES.put("EC", 1.18);
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double roundEven(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

    public static String formatMoney(double money) {
        return moneyFormat.format(round(money, 2));
    }

    /**
     * Convert money from euro to locale conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static Double convertMoneyFromEuros(double money, Locale locale) {
        double conversionRate = 1.0;
        if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
            conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
        }
        //return round(money * conversionRate, 2);
        return money * conversionRate;
    }

    /**
     * Convert money to euro from locale conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static Double convertMoneyToEuros(double money, Locale locale) {
        double conversionRate = 1.0;
        if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
            conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
        }
        //return round(money / conversionRate, 2);
        return money / conversionRate;
    }

    /**
     * Format money with conversion rate
     *
     * @param money
     * @param locale
     * @return
     */
    public static String formatMoney(double money, Locale locale) {
        double conversionRate = 1.0;
        if (CONVERSION_RATES.containsKey(locale.getCountry().toUpperCase())) {
            conversionRate = CONVERSION_RATES.get(locale.getCountry().toUpperCase());
        }
        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        //return f.format(round(money * conversionRate, 2));
        return f.format(money * conversionRate);
    }

    /**
     * Format money without conversion rate
     *
     * @param money
     * @param currency
     * @return
     */
    public static String formatMoney(double money, String currency) {

        Locale locale = new Locale("es", "ES");
        if (currency != null) {
            if (currency.toUpperCase().equals("COP")) {
                locale = new Locale("es", "CO");
            } else if (currency.toUpperCase().equals("PEN")) {
                locale = new Locale("es", "PE");
            } else if (currency.toUpperCase().equals("USD")) {
                locale = new Locale("es", "EC");
            }
        }
        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(round(money, 2));
    }

    public static String formatData(long size) {

        String hrSize;
        double k = size / 1024d;
        double m = size / 1048576d;
        double g = size / 1073741824d;
        double t = size / (1073741824d * 1024);

        if (t > 1) {
            hrSize = dataFormat.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dataFormat.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dataFormat.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dataFormat.format(m).concat(" KB");
        } else {
            hrSize = dataFormat.format(size).concat(" B");
        }

        return hrSize;
    }

    public static int randInt1to9999() {
        int min = 1;
        int max = 9999;
        return rand.nextInt((max - min) + 1) + min;
    }
}
