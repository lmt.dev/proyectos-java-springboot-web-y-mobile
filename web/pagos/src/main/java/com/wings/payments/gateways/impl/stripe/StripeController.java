/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.stripe;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class StripeController {

    @Autowired
    private StripeService stripeService;

    @RequestMapping(value = {"stripe/session"}, method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> createSession(Model model, HttpServletRequest request) {        
        String reqId = request.getParameter("reqId");
        Map<String, String> ret = stripeService.createStripeSession(reqId);
        return ResponseEntity.ok(ret);
    }

}
