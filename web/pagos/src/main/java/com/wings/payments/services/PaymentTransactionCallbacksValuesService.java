package com.wings.payments.services;

import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentTransactionCallbacksValuesService {

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    public Page<PaymentTransactionCallbackValue> findByCallbackId(Long id, Pageable pr) {
        return paymentTransactionCallbackValueRepository.findByCallbackId(id, pr);
    }

    public List<PaymentTransactionCallbackValue> findByCallbackId(Long id) {
        return paymentTransactionCallbackValueRepository.findByCallbackId(id);
    }
}
