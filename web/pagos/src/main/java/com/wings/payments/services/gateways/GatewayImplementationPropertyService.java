package com.wings.payments.services.gateways;

import com.wings.payments.domain.bo.GatewayImplementationProperty;
import com.wings.payments.domain.repos.GatewayImplementationPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GatewayImplementationPropertyService {

    @Autowired
    private GatewayImplementationPropertiesRepository gatewayImplementationPropertiesRepository;

    public List<GatewayImplementationProperty> findAll() {
        return gatewayImplementationPropertiesRepository.findAll();
    }

    public List<GatewayImplementationProperty> findByGatewayImplementationId(Long id) {
        return gatewayImplementationPropertiesRepository.findByGatewayImplementationId(id);
    }

    @Transactional
    public void saveAll(List<GatewayImplementationProperty> gip) {
        gatewayImplementationPropertiesRepository.saveAll(gip);
    }

    @Transactional
    public void deleteByGatewayImplementationId(Long id) {
        gatewayImplementationPropertiesRepository.deleteByGatewayImplementationId(id);
    }

    public GatewayImplementationProperty getReturnUrl(Long giId, Boolean isTest) {
        return gatewayImplementationPropertiesRepository.findByGatewayImplementationIdAndPropertyNameAndPropertyTest(giId, "returnUrl", isTest);
    }
}
