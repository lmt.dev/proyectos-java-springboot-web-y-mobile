/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.stripe;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Event;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.bo.PaymentTransactionCallbackValue;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.gateways.PaymentGateway;
import com.wings.payments.gateways.PaymentGatewayFactory;
import com.wings.payments.services.CallbackService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class StripeService {

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private PaymentGatewayFactory paymentGatewayFactory;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private CallbackService callbackService;

    private final Logger log = LoggerFactory.getLogger(StripeService.class);

    private static final Map<String, String> TRANSLATIONS = new HashMap<>();

    static {
        TRANSLATIONS.put("approve_with_id", "El pago no ha sido autorizado.");
        TRANSLATIONS.put("call_issuer", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("card_declined", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("card_not_supported", "La tarjeta no soporta este tipo de compras.");
        TRANSLATIONS.put("card_velocity_exceeded", "Límite excedido.");
        TRANSLATIONS.put("currency_not_supported", "La tarjeta no soporta el tipo de moneda.");
        TRANSLATIONS.put("do_not_honor", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("do_not_try_again", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("duplicate_transaction", "Una transacción por un monto idéntico ha sido creada para la tarjeta recientemente.");
        TRANSLATIONS.put("expired_card", "La tarjeta ha expirado.");
        TRANSLATIONS.put("fraudulent", "El pago ha sido rechazado por sospecha de fraude.");
        TRANSLATIONS.put("generic_decline", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("incorrect_number", "El número de la tarjeta es incorrecto.");
        TRANSLATIONS.put("incorrect_cvc", "El código de seguridad de la tarjeta no es correcto.");
        TRANSLATIONS.put("requested_block_on_incorrect_cvc", "El código de seguridad de la tarjeta no es correcto.");
        TRANSLATIONS.put("incorrect_pin", "El PIN ingresado es incorrecto.");
        TRANSLATIONS.put("incorrect_zip", "El código postal es incorrecto.");
        TRANSLATIONS.put("insufficient_funds", "Fondos insuficientes.");
        TRANSLATIONS.put("invalid_account", "La tarjeta o la cuenta asociada a la misma es inválida");
        TRANSLATIONS.put("invalid_amount", "El monto es inválido o excede la cantidad permitida.");
        TRANSLATIONS.put("invalid_cvc", "El código de seguridad de la tarjeta no es correcto.");
        TRANSLATIONS.put("invalid_expiry_year", "El año de expiración es incorrecto.");
        TRANSLATIONS.put("invalid_number", "El número de la tarjeta es incorrecto.");
        TRANSLATIONS.put("invalid_pin", "El PIN ingresado es incorrecto.");
        TRANSLATIONS.put("issuer_not_available", "La entidad no response, pago no autorizado.");
        TRANSLATIONS.put("lost_card", "El pago ha sido rechazado por que la tarjeta ha sido reportada como perdida.");
        TRANSLATIONS.put("merchant_blacklist", "Tarjeta bloqueada.");
        TRANSLATIONS.put("new_account_information_available", "La tarjeta o la cuenta asociada a la misma es inválida");
        TRANSLATIONS.put("no_action_taken", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("not_permitted", "El pago no esta permitido.");
        TRANSLATIONS.put("pickup_card", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("pin_try_exceeded", "El número de ingresos de PIN ha excedido el límite.");
        TRANSLATIONS.put("processing_error", "Ha ocurrido un error procesando la tarjeta.");
        TRANSLATIONS.put("reenter_transaction", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("restricted_card", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("revocation_of_all_authorizations", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("revocation_of_authorization", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("security_violation", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("service_not_allowed", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("stolen_card", "El pago ha sido rechazado por que la tarjeta ha sido reportada como robada.");
        TRANSLATIONS.put("stop_payment_order", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("testmode_decline", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("transaction_not_allowed", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("try_again_later", "La tarjeta ha sido rechazada.");
        TRANSLATIONS.put("withdrawal_count_limit_exceeded", "El monto es inválido o excede la cantidad permitida.");
        TRANSLATIONS.put("approved_by_network", "El pago ha sido aprobado.");
    }

    /**
     * CReate a sesssion with stripe, return sessionId and api Key
     *
     * @param paymentRequestId
     * @return
     */
    public Map<String, String> createStripeSession(String paymentRequestId) {

        Map<String, String> ret = new HashMap<>();
        PaymentRequest pr = paymentRequestRepository.getOne(Long.parseLong(paymentRequestId));
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(pr.getId());
        Long giId = ptx.getGatewayImplementantionId();
        PaymentGateway impl = paymentGatewayFactory.getImplementation(gatewayImplementationRepository.getOne(giId));
        Map<String, String> config = impl.getPaymentFormData(pr);

        Stripe.apiKey = config.get("secretKey");

        Map<String, Object> params = new HashMap<>();
        ArrayList<String> paymentMethodTypes = new ArrayList<>();
        paymentMethodTypes.add("card");
        params.put("payment_method_types", paymentMethodTypes);
        params.put("customer_email", pr.getBillingEmail());

        ArrayList<HashMap<String, Object>> lineItems = new ArrayList<>();
        HashMap<String, Object> lineItem = new HashMap<>();
        lineItem.put("name", pr.getTitle());
        lineItem.put("description", pr.getTitle() + " " + pr.getId() + "-" + pr.getOrderNumber());
        lineItem.put("amount", (int) (pr.getAmount() * 100));
        lineItem.put("currency", pr.getCurrency().toLowerCase());
        lineItem.put("quantity", 1);
        lineItems.add(lineItem);
        params.put("line_items", lineItems);

        params.put("success_url", config.get("successUrl"));
        params.put("cancel_url", config.get("cancelUrl"));
        params.put("client_reference_id", pr.getId() + "-" + pr.getOrderNumber());

        try {
            Session session = Session.create(params);
            ret.put("sessionId", session.getId());
            ret.put("key", config.get("publicKey"));

            //create transacion callback
            PaymentTransactionCallback cbk = new PaymentTransactionCallback();
            cbk.setCallbackDate(new Date());
            cbk.setPaymentTransactionId(ptx.getId());
            cbk.setReportedDate(new Date());
            cbk.setStatus("SESSION_CREATED");
            paymentTransactionCallbackRepository.save(cbk);

            //create extra values for session id and paymentIntent
            List<PaymentTransactionCallbackValue> values = new ArrayList<>();
            PaymentTransactionCallbackValue sessId = new PaymentTransactionCallbackValue();
            sessId.setCallbackId(cbk.getId());
            sessId.setValName("session.id");
            sessId.setValValue(session.getId());
            values.add(sessId);

            PaymentTransactionCallbackValue payIntent = new PaymentTransactionCallbackValue();
            payIntent.setCallbackId(cbk.getId());
            payIntent.setValName("payment.intent");
            payIntent.setValValue(session.getPaymentIntent());
            values.add(payIntent);
            paymentTransactionCallbackValueRepository.saveAll(values);

        } catch (Exception e) {
            log.error("Error creating session on Stripe", e);
        }
        return ret;
    }

    /**
     *
     * @param request
     */
    public void processCallback(HttpServletRequest request) {

        try {
            //String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            Event e = new Gson().fromJson(request.getReader(), Event.class);
            String body = e.toString();

            //get stripe objects
            Optional<StripeObject> obj = e.getDataObjectDeserializer().getObject();
            Charge charge = (Charge) obj.get();
            Charge.Outcome outcome = charge.getOutcome();

            //get wings objects
            String paymentIntent = charge.getPaymentIntent();
            PaymentTransactionCallbackValue intentValue = paymentTransactionCallbackValueRepository.findByValNameAndValValue("payment.intent", paymentIntent);
            PaymentTransactionCallback cbkTemp = paymentTransactionCallbackRepository.getOne(intentValue.getCallbackId());
            PaymentTransaction ptx = paymentTransactionRepository.getOne(cbkTemp.getPaymentTransactionId());
            PaymentRequest req = paymentRequestRepository.getOne(ptx.getPaymentRequestId());

            String status;
            String statusMessage;

            switch (e.getType()) {
                case "charge.succeeded":
                    status = PaymentStatus.COMPLETED.name();
                    statusMessage = getStatusMessageByStripeCode("approved_by_network");
                    break;
                case "charge.failed":
                    status = PaymentStatus.FAILED.name();
                    statusMessage = getStatusMessageByStripeCode(outcome.getReason());
                    if (statusMessage == null) {
                        statusMessage = getStatusMessageByStripeCode(charge.getFailureCode());
                    }
                    if (statusMessage == null) {
                        statusMessage = charge.getFailureMessage();
                    }
                    if (statusMessage == null) {
                        statusMessage = "Error desconocido";
                    }
                    break;
                default:
                    status = PaymentStatus.FAILED.name();
                    statusMessage = getStatusMessageByStripeCode(outcome.getReason());
                    if (statusMessage == null) {
                        statusMessage = getStatusMessageByStripeCode(charge.getFailureCode());
                    }
                    if (statusMessage == null) {
                        statusMessage = charge.getFailureMessage();
                    }
                    if (statusMessage == null) {
                        statusMessage = "Error desconocido";
                    }
            }

            ptx.setLastStatus(status);
            req.setStatus(status);
            req.setStatusMessage(statusMessage);
            req.setStatusShown(false);
            req.setStatusDate(new Date());
            req.setProcessor("stripe");

            paymentRequestRepository.save(req);
            paymentTransactionRepository.save(ptx);

            //save new callback and values
            //create transacion callback
            PaymentTransactionCallback cbk = new PaymentTransactionCallback();
            cbk.setCallbackDate(new Date());
            cbk.setPaymentTransactionId(ptx.getId());
            cbk.setReportedDate(new Date());
            cbk.setStatus(status);
            paymentTransactionCallbackRepository.save(cbk);

            //create extra values for json
            PaymentTransactionCallbackValue object = new PaymentTransactionCallbackValue();
            object.setCallbackId(cbk.getId());
            object.setValName("object");
            object.setValValue(body);
            paymentTransactionCallbackValueRepository.save(object);

            //send callback
            callbackService.postCallback(req);

        } catch (Exception e) {
            log.error("Error processing callback for stripe", e);
        }
    }

    /**
     * get human readable messages
     *
     * @param stripeCode
     * @return
     */
    private String getStatusMessageByStripeCode(String stripeCode) {
        return TRANSLATIONS.get(stripeCode);
    }
}
