/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.GatewayImplementationProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.List;

/**
 *
 * @author seba
 */
public interface GatewayImplementationPropertiesRepository extends JpaRepository<GatewayImplementationProperty, Long>, JpaSpecificationExecutor<GatewayImplementationProperty> {

    List<GatewayImplementationProperty> findByGatewayImplementationId(Long id);

    void deleteByGatewayImplementationId(Long id);

    GatewayImplementationProperty findByGatewayImplementationIdAndPropertyNameAndPropertyTest(Long giId, String returnUrl, Boolean isTest);
}
