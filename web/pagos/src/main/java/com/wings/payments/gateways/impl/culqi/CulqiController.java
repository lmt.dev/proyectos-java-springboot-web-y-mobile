/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.culqi;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class CulqiController {

    @Autowired
    private CulqiService culqiService;

    @RequestMapping(value = {"culqi/charge"}, method = RequestMethod.POST)
    public String generateCharge(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String token = request.getParameter("token");
        String reqId = request.getParameter("reqId");
        culqiService.createCulqiCharge(token, reqId);
        redirectAttributes.addFlashAttribute("paymentRequestId", reqId);
        return "redirect:/tr";
    }

}
