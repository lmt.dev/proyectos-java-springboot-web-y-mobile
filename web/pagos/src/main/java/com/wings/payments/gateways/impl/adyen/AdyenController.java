package com.wings.payments.gateways.impl.adyen;

import com.adyen.service.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class AdyenController {

    @Autowired
    private AdyenService adyenService;

    @RequestMapping(value="/adyenLink", method = RequestMethod.POST)
    public String getPaymentLink(HttpServletRequest request) throws IOException, ApiException {

        String adyenUrl = adyenService.generateAdyenLink(request).getUrl();
        return "redirect:".concat(adyenUrl);
    }

}
