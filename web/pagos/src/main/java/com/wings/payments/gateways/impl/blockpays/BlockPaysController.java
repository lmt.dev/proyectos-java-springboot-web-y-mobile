/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.blockpays;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.gateways.impl.blockpays.dto.Currency;
import com.wings.payments.gateways.impl.blockpays.dto.ResponsePayment;
import com.wings.payments.utils.RequestUtils;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.bridge.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class BlockPaysController {

    @Autowired
    private BlockPaysService blockPaysService;
    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private MessageSource messageSource;

    /**
     * get available currencies to pay
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"blockpays"}, method = {RequestMethod.GET})
    public String getBlockPaysIndex(Model model, HttpServletRequest request) {

        //get available currencies
        List<Currency> currencies = blockPaysService.getCryptoCurrencies();
        model.addAttribute("currencies", currencies);

        String code;
        try {
            code = request.getSession().getAttribute("code").toString();
        } catch (Exception e) {
            return "pay-code";
        }

        PaymentRequest req = paymentRequestRepository.findByCode(code);
        if (req == null) {
            return "pay-code";
        }
        model.addAttribute("req", req);

        return "pays/blockpays/selection";
    }

    /**
     * llama a
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"blockpays/options"}, method = {RequestMethod.POST})
    public String postRedirect(Model model, HttpServletRequest request) {
        return "redirect:/blockpays";
    }

    /**
     *
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = {"blockpays/options/{id}"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postReturnUrl(Model model, HttpServletRequest request, @PathVariable("id") Long id, Locale locale) {

        String code;
        try {
            code = request.getSession().getAttribute("code").toString();
            PaymentRequest req = paymentRequestRepository.findByCode(code);
            ResponsePayment resp = blockPaysService.postCharge(req, id);

            if (resp != null && resp.getStatus() != null && resp.getStatus().equals("true")) {
                return ResponseEntity.ok(resp.getData().getUrlPayment());
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(messageSource.getMessage("blockpays.controller.error", null, locale)+" "+ e.getMessage());
        }
        return ResponseEntity.badRequest().body(messageSource.getMessage("blockpays.controller.error", null, locale));
    }

    /**
     * llama a
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"blockpays/callbacks"}, method = {RequestMethod.POST})
    public ResponseEntity postCallback(Model model, HttpServletRequest request) {

        RequestUtils.debugRequestBody(request);
        RequestUtils.debugRequestHeaders(request);
        RequestUtils.debugRequestParams(request);

        return ResponseEntity.ok().build();
    }
}
