/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.bo;

import com.wings.payments.utils.MoneyUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "payment_requests")
public class PaymentRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 255)
    @Column(name = "requester_name")
    private String requesterName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "order_number")
    private long orderNumber;
    @Size(max = 250)
    @Column(name = "title")
    private String title;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "coms_discount_amount")
    private Double comsDiscountAmount;
    @Size(max = 5)
    @Column(name = "currency")
    private String currency;
    @Size(max = 5)
    @Column(name = "country")
    private String country;
    @Size(max = 255)
    @Column(name = "callback_url")
    private String callbackUrl;
    @Size(max = 255)
    @Column(name = "cancel_url")
    private String cancelUrl;
    @Size(max = 255)
    @Column(name = "image")
    private String image;
    @Size(max = 255)
    @Column(name = "preferred_gateway")
    private String preferredGateway;
    @Column(name = "exclude_bacs")
    private boolean excludeBacs;
    @Size(max = 150)
    @Column(name = "status")
    private String status;
    @Column(name = "status_message")
    private String statusMessage;
    @Column(name = "status_shown")
    private boolean statusShown;
    @Size(max = 50)
    @Column(name = "payment_type")
    private String paymentType;
    @Size(max = 250)
    @Column(name = "billing_first_name")
    private String billingFirstName;
    @Column(name = "billing_last_name")
    private String billingLastName;
    @Size(max = 250)
    @Column(name = "billing_email")
    private String billingEmail;
    @Size(max = 250)
    @Column(name = "billing_address")
    private String billingAddress;
    @Size(max = 250)
    @Column(name = "billing_city")
    private String billingCity;
    @Size(max = 250)
    @Column(name = "billing_country")
    private String billingCountry;
    @Size(max = 250)
    @Column(name = "billing_state")
    private String billingState;
    @Size(max = 255)
    @Column(name = "hash_code")
    private String code;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_Date")
    private Date statusDate;
    @Column(name = "processor")
    private String processor;
    @Column(name = "pay_code")
    private String payCode;

    @Column(name="regenerated_by")
    private String regeneratedBy;

    @Column(name="regenerated_at")
    private Date regeneratedAt;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", nullable = true, updatable = false, insertable = false)
    private List<PaymentRequestExtra> extras = new LinkedList<PaymentRequestExtra>();

    public Date getRegeneratedAt() {
        return regeneratedAt;
    }

    public void setRegeneratedAt(Date regeneratedAt) {
        this.regeneratedAt = regeneratedAt;
    }

    public String getRegeneratedBy() {
        return regeneratedBy;
    }

    public void setRegeneratedBy(String regeneratedBy) {
        this.regeneratedBy = regeneratedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public int getIntegerAmount() {
        return (int) (amount * 100);
    }

    public String getStringAmount() {
        return String.valueOf(amount);
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreferredGateway() {
        return preferredGateway;
    }

    public void setPreferredGateway(String preferredGateway) {
        this.preferredGateway = preferredGateway;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PaymentRequestExtra> getExtras() {
        return extras;
    }

    public void setExtras(List<PaymentRequestExtra> extras) {
        this.extras = extras;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentAmount() {
        return MoneyUtils.formatMoney(amount, currency);
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public boolean isExcludeBacs() {
        return excludeBacs;
    }

    public void setExcludeBacs(boolean excludeBacs) {
        this.excludeBacs = excludeBacs;
    }

    public boolean isStatusShown() {
        return statusShown;
    }

    public void setStatusShown(boolean statusShown) {
        this.statusShown = statusShown;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getCompoundId() {
        return id + "-" + orderNumber;
    }

    public Double getComsDiscountAmount() {
        return comsDiscountAmount;
    }

    public void setComsDiscountAmount(Double comsDiscountAmount) {
        this.comsDiscountAmount = comsDiscountAmount;
    }

    public String getComsDiscountAmountText() {
        return MoneyUtils.formatMoney(comsDiscountAmount, currency);
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

}
