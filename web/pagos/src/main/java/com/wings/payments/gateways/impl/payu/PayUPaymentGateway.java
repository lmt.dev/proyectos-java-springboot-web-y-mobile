/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.payu;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.GatewayImplementationProperty;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.gateways.PaymentGateway;
import com.wings.payments.utils.NumberUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author seba
 */
public class PayUPaymentGateway implements PaymentGateway {

    private final Map<String, String> config = new HashMap<>();
    private boolean test = false;

    /**
     * init
     *
     * @param gatewayImplementation
     */
    @Override
    public void init(GatewayImplementation gatewayImplementation) {
        List<GatewayImplementationProperty> properties = gatewayImplementation.getTest() ? gatewayImplementation.getTestProperties() : gatewayImplementation.getProperties();
        properties.forEach((property) -> {
            config.put(property.getPropertyName(), property.getPropertyValue());
        });
        test = gatewayImplementation.getTest();
    }

    /**
     * get al data used by this GW
     *
     * @param request
     * @return
     */
    @Override
    public Map<String, String> getPaymentFormData(PaymentRequest request) {

        String apiKey = config.get("apiKey");
        String merchantId = config.get("merchantId");
        String accountId = config.get("accountId");
        String url = config.get("postUrl");

        Map<String, String> ret = new HashMap<>();

        Double price = request.getAmount();
        price = NumberUtils.round(price, 2);
        String stringPrice = String.valueOf(price);

        String signature = DigestUtils.sha256Hex(apiKey + "~" + merchantId + "~" + "W" + request.getId() + "~" + stringPrice + "~" + request.getCurrency());

        ret.put("action", url);
        ret.put("merchantId", merchantId);
        ret.put("accountId", accountId);

        ret.put("referenceCode", "W" + request.getId().toString());
        ret.put("amount", stringPrice);
        ret.put("currency", request.getCurrency());
        ret.put("signature", signature);

        String description = request.getTitle();
        if (description.length() >= 80) {
            description = description.substring(0, 70) + "...";
        }
        ret.put("description", description);
        ret.put("buyerEmail", request.getBillingEmail());
        ret.put("buyerFullName", getFullName(request.getBillingFirstName(), request.getBillingLastName()));

        ret.put("responseUrl", config.get("responseUrl"));
        ret.put("confirmationUrl", config.get("confirmationUrl"));
        ret.put("algorithmSignature", "sha256");
        ret.put("paymentView", "payu");

        ret.put("test", test ? "1" : "0");

        return ret;
    }

    /**
     * get the template name for this gw
     *
     * @return
     */
    @Override
    public String getTemplateName() {
        return "pays/payu/index";
    }

    /**
     * "ApiKey~merchant_id~reference_sale~new_value~currency~state_pol"
     *
     * @param merchantId
     * @param referenceSale
     * @param value
     * @param currency
     * @param statePol
     * @return
     */
    public String getConfirmationSignature(String merchantId, String referenceSale, String value, String currency, String statePol) {
        if (value.endsWith(".00")) {
            value = value.replaceAll("\\.00", "\\.0");
        }
        String toSign = config.get("apiKey") + "~" + merchantId + "~" + referenceSale + "~" + value + "~" + currency + "~" + statePol;
        return DigestUtils.sha256Hex(toSign);
    }

    /**
     * "ApiKey~merchantId~referenceCode~new_value~currency~transactionState"
     *
     * @param merchantId
     * @param referenceCode
     * @param value
     * @param currency
     * @param transactionState
     * @return
     */
    public String getResponseSignature(String merchantId, String referenceCode, String value, String currency, String transactionState) {
        //TODO: cambiar esto por la cosa enferma de aca , mover al service todo lo de signature
        //http://developers.payulatam.com/es/web_checkout/integration.html
        try {
            Double dvalue = Double.parseDouble(value);
            value = String.valueOf(NumberUtils.roundEven(dvalue, 1));
        } catch (Exception e) {
        }
        String toSign = config.get("apiKey") + "~" + merchantId + "~" + referenceCode + "~" + value + "~" + currency + "~" + transactionState;
        return DigestUtils.md5Hex(toSign);
    }

    private String getFullName(String firstName, String lastName) {
        return (firstName != null ? firstName : "") + " " + (lastName != null ? lastName : "");
    }
}
