/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.GatewayImplementation;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface GatewayImplementationRepository extends JpaRepository<GatewayImplementation, Long>, JpaSpecificationExecutor<GatewayImplementation> {

    List<GatewayImplementation> findByCountryAndEnabled(String country, Boolean enabled);

    @Modifying
    @Query(value="UPDATE gateway_implementation gi SET gi.enabled =?1, gi.test =?2 WHERE gi.id =?3", nativeQuery = true)
    void update(boolean isEnabled, boolean isTest, Long id);

    List<GatewayImplementation> findByGatewayId(Long id);
}