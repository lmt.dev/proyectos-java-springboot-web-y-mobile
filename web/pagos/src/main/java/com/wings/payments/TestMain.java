/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author seba
 */
public class TestMain {

    public static void main(String[] args) {

        String vssalue = "884900.00";

        if (vssalue.endsWith(".00")) {
            vssalue = vssalue.replaceAll("\\.00", "\\.0");
        }

        String sig = "i3qje6rEZQ3lctu0XrfckkrBzT~760816~W1699~" + vssalue + "~COP~4";
        //473c337d9c76bf306e5fa24eabd54d2a01c73d8b8ddf5db46746ef95a69b174f
        //473c337d9c76bf306e5fa24eabd54d2a01c73d8b8ddf5db46746ef95a69b174f
        //473c337d9c76bf306e5fa24eabd54d2a01c73d8b8ddf5db46746ef95a69b174f

        //798371a0105ecd3cc6610f9ffcd12603c76b2b2124a4c93765d689dad6021aab
        //acc2ea2e5cb73e4626216bd877b22cd1964bfcf7585fee42aa36fd7a560a0deb 
        System.out.println(DigestUtils.sha256Hex(sig));

    }

}
