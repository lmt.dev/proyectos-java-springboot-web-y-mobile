/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.coms;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.bo.PaymentTransactionCallback;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.GatewayImplementationRepository;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackRepository;
import com.wings.payments.domain.repos.PaymentTransactionCallbackValueRepository;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import com.wings.payments.gateways.PaymentGateway;
import com.wings.payments.gateways.PaymentGatewayFactory;
import com.wings.payments.services.CallbackService;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class ComsService {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private GatewayImplementationRepository gatewayImplementationRepository;

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentGatewayFactory paymentGatewayService;

    @Autowired
    private CallbackService callbackService;

    private final Logger log = LoggerFactory.getLogger(ComsService.class);

    public String createComsCharge(String username, String password, Long requestId) {

        //get paymentRequest
        PaymentRequest payReq = paymentRequestRepository.getOne(requestId);

        //get tx
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

        //get implementation
        Long giId = ptx.getGatewayImplementantionId();
        GatewayImplementation gi = gatewayImplementationRepository.getOne(giId);
        PaymentGateway comsPaymentGateway = paymentGatewayService.getImplementation(gi);

        Map<String, String> params = comsPaymentGateway.getPaymentFormData(payReq);
        String url = params.get("api_url");
        Long orderId = payReq.getOrderNumber();
        String key = params.get("api_key");
        String authToken = generateAuthToken(username, password, key);

        //get total amount to pay
        double amount;
        if (payReq.getComsDiscountAmount() != null && payReq.getComsDiscountAmount() > 0) {
            amount = payReq.getComsDiscountAmount();
        } else {
            amount = payReq.getAmount();
        }

        String statusMessage = postCharge(url, username, authToken, orderId, amount, payReq.getRequesterName());
        String status;
        if (statusMessage.equals("Transaccion aceptada")) {
            status = PaymentStatus.COMPLETED.name();
        } else {
            status = PaymentStatus.FAILED.name();
        }

        PaymentTransactionCallback ptxCallback = new PaymentTransactionCallback();
        ptxCallback.setCallbackDate(new Date());
        ptxCallback.setReportedDate(new Date());
        ptxCallback.setStatus(status);
        ptxCallback.setPaymentTransactionId(ptx.getId());

        paymentTransactionCallbackRepository.save(ptxCallback);

        //get payment request 
        payReq.setStatus(status);
        payReq.setStatusMessage(statusMessage);
        payReq.setStatusShown(false);
        payReq.setStatusDate(new Date());
        payReq.setProcessor("coms");

        paymentRequestRepository.save(payReq);

        callbackService.postCallback(payReq);

        return statusMessage;
    }

    /**
     * Post charge to backoffice API
     *
     * @param url
     * @param username
     * @param authToken
     * @param orderId
     * @param amount
     * @return
     */
    private String postCharge(String url, String username, String authToken, Long orderId, Double amount, String requester) {

        String ret = "error";
        try {
            RestTemplate template = new RestTemplate();
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("username", username);
            params.add("auth_token", authToken);
            params.add("amount", amount.toString());
            params.add("order_id", orderId.toString());
            params.add("requester", requester);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
            ResponseEntity<String> response = template.exchange(new URI(url), HttpMethod.POST, entity, String.class);

            if (response.getBody() != null) {
                ret = response.getBody();
            }
        } catch (Exception e) {
            log.error("Error creating Coms charge", e);
        }
        return ret;
    }

    /**
     * generate authToken
     *
     * @param username
     * @param password
     * @param salt
     * @return
     */
    private String generateAuthToken(String username, String password, String salt) {
        return DigestUtils.sha256Hex(username + salt + DigestUtils.sha256Hex(password));
    }

    /**
     * Test ping one time on startup
     */
    //@Scheduled(initialDelay = 5000, fixedRate = 10000)
    public void ping() {
        try {
            RestTemplate template = new RestTemplate();
            ResponseEntity<String> response = template.getForEntity("https://beta.wingsmobile.net/login", String.class);

            System.out.println("Ping response with " + response.getStatusCodeValue());
            System.out.println("Ping response " + response.getBody());

        } catch (Exception e) {
            log.error("Error On ping BETA", e);
        }
    }

}
