/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author seba
 */
public class MoneyUtils {

    /**
     * Format money without conversion rate
     *
     * @param money
     * @param currency
     * @return
     */
    public static String formatMoney(double money, String currency) {

        Locale locale = new Locale("es", "ES");
        if (currency != null) {
            switch (currency.toUpperCase()) {
                case "COP":
                    locale = new Locale("es", "CO");
                    break;
                case "PEN":
                    locale = new Locale("es", "PE");
                    break;
                case "ARS":
                    locale = new Locale("es", "AR");
                    break;
                case "BOB":
                    locale = new Locale("es", "BO");
                    break;
                case "BRL":
                    locale = new Locale("pt", "BR");
                    break;
                case "CLP":
                    locale = new Locale("es", "CL");
                    break;
                case "CRC":
                    locale = new Locale("es", "CR");
                    break;
                case "DOP":
                    locale = new Locale("es", "DO");
                    break;
                case "GTQ":
                    locale = new Locale("es", "GT");
                    break;
                case "HNL":
                    locale = new Locale("es", "HN");
                    break;
                case "EUR":
                    locale = new Locale("es", "EUR");
                    break;
                case "MXN":
                    locale = new Locale("es", "MX");
                    break;
                case "NIO":
                    locale = new Locale("es", "NI");
                    break;
                case "PAB":
                    locale = new Locale("es", "PA");
                    break;
                case "USD":
                    locale = new Locale("es", "EC");
                    break;
                default:
                    locale = new Locale("es", "US");
                    break;
            }
        }
        NumberFormat f = NumberFormat.getCurrencyInstance(locale);
        return f.format(money);
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
