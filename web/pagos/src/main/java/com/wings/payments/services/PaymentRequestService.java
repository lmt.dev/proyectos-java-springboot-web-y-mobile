package com.wings.payments.services;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.repos.PaymentRequestRepository;
import com.wings.payments.domain.specs.PaymentRequestSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PaymentRequestService {

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    public PaymentRequest findByOrderNumberAndRequesterName(Long orderNumber, String requesterName) {
        return paymentRequestRepository.findByOrderNumberAndRequesterName(orderNumber, requesterName);
    }

    public PaymentRequest findByCode(String code) {
        return paymentRequestRepository.findByCode(code);
    }

    public PaymentRequest findByPayCode(String payCode) {
        return paymentRequestRepository.findByPayCode(payCode);
    }

    public int countByPayCode(String payCode) {
        return paymentRequestRepository.countByPayCode(payCode);
    }

    public PaymentRequest save(PaymentRequest paymentRequest) {
        return paymentRequestRepository.save(paymentRequest);
    }

    public Page<PaymentRequest> findAll(PageRequest pr) {
        return paymentRequestRepository.findAll(PageRequest.of(pr.getPageNumber(),pr.getPageSize(), Sort.by(Sort.Direction.DESC, "id")));
    }

    public Page<PaymentRequest> findByParams(String params, PageRequest pr) {
        return paymentRequestRepository.findAll(PaymentRequestSpecs.search(params.trim()), pr);
    }

    public PaymentRequest findByOrderNumber(Long orderNumber) {
        return paymentRequestRepository.findByOrderNumber(orderNumber);
    }

    public PaymentRequest findById(Long id) {
        return paymentRequestRepository.findById(id).orElse(null);
    }
}
