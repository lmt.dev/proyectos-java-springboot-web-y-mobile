package com.wings.payments.services;

import com.wings.payments.domain.bo.PaymentTransaction;
import com.wings.payments.domain.repos.PaymentTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentTransactionService {

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    public PaymentTransaction findByPaymentRequestId(Long id) {
        return paymentTransactionRepository.findByPaymentRequestId(id);
    }
}
