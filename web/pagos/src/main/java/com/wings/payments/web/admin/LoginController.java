/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.web.admin;

import java.security.Principal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author maurib
 */
@Controller
public class LoginController {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            Model model, Principal principal, RedirectAttributes flash, Locale locale) {

        if (principal != null) {
            flash.addFlashAttribute("info", messageSource.getMessage("login.previous.session", null, locale));
            return "redirect:/admin";
        }

        if (error != null) {
            model.addAttribute("error", messageSource.getMessage("login.error.bad.credentials", null, locale));
        }

        if (logout != null) {
            model.addAttribute("success", messageSource.getMessage("login.success.logout", null, locale));
        }
        return "login";
    }
}
