/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * @author seba
 */
public interface GatewaysRepository extends JpaRepository<Gateway, Long> , JpaSpecificationExecutor<Gateway>{

    @Query(value = "SELECT * FROM gateways g WHERE g.id IN (SELECT gateway_id FROM gateway_properties)", nativeQuery = true)
    List<Gateway> findAllWithFilters();
}
