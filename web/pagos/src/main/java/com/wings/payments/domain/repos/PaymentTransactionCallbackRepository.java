/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.PaymentTransactionCallback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author seba
 */
public interface PaymentTransactionCallbackRepository extends JpaRepository<PaymentTransactionCallback, Long> {

    Page<PaymentTransactionCallback> findByPaymentTransactionId(Long id, Pageable p);

    List<PaymentTransactionCallback> findByPaymentTransactionId(Long id);
}
