/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.enums;

/**
 *
 * @author seba
 */
public enum PaymentStatus {
    
    PENDING,
    PROCESSING,
    ONHOLD,
    COMPLETED,
    CANCELED,
    REFUNDED,
    FAILED,
    FAILED_WRONG_SIGNATURE,

}
