/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.web;

import com.wings.payments.gateways.impl.adyen.AdyenService;
import com.wings.payments.gateways.impl.payu.PayUService;
import com.wings.payments.gateways.impl.stripe.StripeService;
import com.wings.payments.utils.RequestUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class CallbackController {

    @Autowired
    private PayUService payUService;

    @Autowired
    private StripeService stripeService;

    @Autowired
    private AdyenService adyenService;

    @RequestMapping(value = {"/cbk"}, method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<String> gwCallBacks(HttpServletRequest request) {

        //PayU
        if (request.getParameter("reference_sale") != null && !request.getParameter("reference_sale").isEmpty()) {
            payUService.processPayUCallback(request);
        }

        //Stripe
        if (request.getHeader("user-agent") != null && request.getHeader("user-agent").contains("Stripe")) {
            stripeService.processCallback(request);
        }

        if (request.getHeader("user-agent") != null && request.getHeader("user-agent").contains("Adyen HttpClient")) {
            return adyenService.processCallback(request);
        }

        RequestUtils.debugRequestParams(request);
        RequestUtils.debugRequestHeaders(request);

        return ResponseEntity.accepted().body("");
    }

}
