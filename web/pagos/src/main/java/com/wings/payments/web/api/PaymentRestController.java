package com.wings.payments.web.api;

import com.wings.payments.domain.bo.*;
import com.wings.payments.domain.dto.PaymentRequestDTO;
import com.wings.payments.domain.dto.TransactionDTO;
import com.wings.payments.services.*;
import com.wings.payments.services.gateways.GatewayImplementationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PaymentRestController {

    @Autowired
    private PaymentRequestService paymentRequestService;

    @Autowired
    private GatewayImplementationService gatewayImplementationService;

    @Autowired
    private PaymentTransactionCallbackService paymentTransactionCallbackService;

    @Autowired
    private PaymentTransactionCallbacksValuesService paymentTransactionCallbacksValuesService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @RequestMapping(value="/payment/{orderNumber}")
    public ResponseEntity findPaymentRequest(@PathVariable("orderNumber") Long orderNumber) {

            PaymentRequest pr = paymentRequestService.findByOrderNumber(orderNumber);
            if(pr != null){
                PaymentRequestDTO prdto = new PaymentRequestDTO();
                PaymentTransaction pt = paymentTransactionService.findByPaymentRequestId(pr.getId());
                GatewayImplementation gi = gatewayImplementationService.findById(pt.getGatewayImplementantionId());
                List<PaymentTransactionCallback> ptcList = paymentTransactionCallbackService.findByPaymentTransactionId(pt.getId());

                Map<String, String> values = new HashMap<>();
                List<TransactionDTO> transactions = new ArrayList<>();

                ptcList.forEach( x -> {
                    TransactionDTO transactionDTO = new TransactionDTO();
                    transactionDTO.setPaymentTransactionId(x.getPaymentTransactionId());
                    transactionDTO.setCallbackDate(x.getCallbackDate());
                    transactionDTO.setStatus(x.getStatus());

                    List<PaymentTransactionCallbackValue> valueList = paymentTransactionCallbacksValuesService.findByCallbackId(x.getId());

                    valueList.forEach(value -> {
                       values.put(value.getValName(), value.getValValue());
                    });
                    transactionDTO.setValues(values);
                    transactions.add(transactionDTO);
                });

                prdto.setPaymentRequestId(pr.getId());
                prdto.setOrderNumber(pr.getOrderNumber());
                prdto.setGatewayImplementation(gi.getName());
                prdto.setLastStatus(pt.getLastStatus());
                prdto.setTransactions(transactions);

                return new ResponseEntity(prdto, HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
    }
}
