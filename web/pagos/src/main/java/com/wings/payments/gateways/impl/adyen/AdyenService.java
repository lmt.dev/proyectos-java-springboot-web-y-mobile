package com.wings.payments.gateways.impl.adyen;

import com.adyen.Client;
import com.adyen.enums.Environment;
import com.adyen.model.Amount;
import com.adyen.model.checkout.CreatePaymentLinkRequest;
import com.adyen.model.checkout.CreatePaymentLinkResponse;
import com.adyen.service.Checkout;
import com.adyen.service.exception.ApiException;
import com.wings.payments.domain.bo.*;
import com.wings.payments.domain.enums.PaymentStatus;
import com.wings.payments.domain.repos.*;
import com.wings.payments.services.CallbackService;
import com.wings.payments.services.gateways.GatewayImplementationPropertyService;
import com.wings.payments.utils.DateUtils;
import com.wings.payments.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@Service
public class AdyenService {

    @Autowired
    private PaymentRequestRepository paymentRequestRepository;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionCallbackRepository paymentTransactionCallbackRepository;

    @Autowired
    private PaymentTransactionCallbackValueRepository paymentTransactionCallbackValueRepository;

    @Autowired
    private CallbackService callbackService;

    @Autowired
    private GatewayImplementationPropertyService gips;

    private static final Map<String, String> SUCCESS_MESSAGES = new HashMap<>();
    private static final Map<String, String> EXTRA_MESSAGES = new HashMap<>();
    private final String prefix = "4cb604fb3ae42b9f-WingsMobile";

    static {
        SUCCESS_MESSAGES.put("AUTHORISATION", "Transacción Exitosa");
        SUCCESS_MESSAGES.put("AUTHORISATION_ADJUSTMENT", "Transacción Exitosa");
        SUCCESS_MESSAGES.put("CAPTURE", "Transacción Exitosa");
        SUCCESS_MESSAGES.put("VOID_PENDING_REFUND", "Transacción Exitosa");
        SUCCESS_MESSAGES.put("REFUND_WITH_DATA", "Transacción Exitosa");
        SUCCESS_MESSAGES.put("CANCELLATION", "Transacción Cancelada con Éxito");
        SUCCESS_MESSAGES.put("CANCEL_OR_REFUND", "Transacción Cancelada con Éxito");
        SUCCESS_MESSAGES.put("REFUND", "Transacción Realizada con Éxito");
    }

    static {
        EXTRA_MESSAGES.put("PENDING", "Transacción Pendiente");
        EXTRA_MESSAGES.put("REFUND_FAILED", "La Transacción de Devolución Falló");
        SUCCESS_MESSAGES.put("CANCELLATION", "La Transacción de Cancelación Falló");
        SUCCESS_MESSAGES.put("CANCEL_OR_REFUND", "La Transacción Falló");
    }

    public CreatePaymentLinkResponse generateAdyenLink(HttpServletRequest request) throws IOException, ApiException {

        CreatePaymentLinkRequest linkRequest = new CreatePaymentLinkRequest();
        linkRequest.setReference(request.getParameter("reference"));
        Amount amount = new Amount();
        amount.setValue((new Double(request.getParameter("value"))).longValue() * 100);
        amount.setCurrency(request.getParameter("currency"));
        linkRequest.setAmount(amount);
        linkRequest.setCountryCode(request.getParameter("countryCode"));
        linkRequest.setMerchantAccount(request.getParameter("merchantAccount"));

        String description = request.getParameter("description");
        description = description.length() > 280 ? description.substring(0, 276).concat("...") : description;
        linkRequest.setDescription(description);

        boolean isTest = Boolean.parseBoolean(request.getParameter("isTest"));
        Long giId = Long.valueOf(request.getParameter("giId"));
        GatewayImplementationProperty gip = gips.getReturnUrl(giId, isTest);

        String returnUrl = gip.getPropertyValue().concat(request.getParameter("orderNumber"));
        linkRequest.setReturnUrl(returnUrl);

        String apiKey = request.getParameter("apikey");

        Client client;
        if (isTest) {
            client = new Client(apiKey, Environment.TEST);
        } else {
            client = new Client(apiKey, Environment.LIVE, prefix);
        }

        Checkout checkout = new Checkout(client);
        CreatePaymentLinkResponse response = checkout.paymentLinks(linkRequest);
        return response;
    }

    public ResponseEntity<String> processCallback(HttpServletRequest request) {
        Map<String, String> params = RequestUtils.getRequestParams(request);
        String paymentRequestId = params.get("merchantReference");

        //get paymentRequest
        PaymentRequest payReq = paymentRequestRepository.getOne(Long.parseLong(paymentRequestId));

        //get tx
        PaymentTransaction ptx = paymentTransactionRepository.findByPaymentRequestId(payReq.getId());

        //create new callback
        PaymentTransactionCallback ptxCallback = new PaymentTransactionCallback();
        ptxCallback.setCallbackDate(new Date());
        ptxCallback.setPaymentTransactionId(ptx.getId());

        //get date
        String sreportedDate = params.get("eventDate");
        Date reportedDate = new Date();
        try {
            reportedDate = DateUtils.getDateTimeFromMysqlFormat(sreportedDate);
        } catch (Exception e) {
        }
        //get status
        String sstatus = params.get("eventCode");
        Boolean success = params.get("success").equals("true");
        String status = fromAdyenToWingsStatus(sstatus, success);

        //get status message
        String statusMessage = (success && SUCCESS_MESSAGES.containsKey(sstatus)) ? SUCCESS_MESSAGES.get(sstatus)
                : (EXTRA_MESSAGES.containsKey(sstatus)) ? EXTRA_MESSAGES.get(sstatus)
                : "La Transacción Falló";
        //put values
        ptxCallback.setReportedDate(reportedDate);
        ptxCallback.setStatus(status);

        //save
        paymentTransactionCallbackRepository.save(ptxCallback);

        //refresh status
        ptx.setLastStatus(status);
        paymentTransactionRepository.save(ptx);

        //get other info
        List<PaymentTransactionCallbackValue> values = new ArrayList<>();
        params.entrySet()
                .forEach((entry) -> {
                    String key = entry.getKey();
                    String valu = entry.getValue();
                    PaymentTransactionCallbackValue val = new PaymentTransactionCallbackValue();
                    val.setCallbackId(ptxCallback.getId());
                    val.setValName(key);
                    val.setValValue(valu);
                    values.add(val);
                }
                );
        paymentTransactionCallbackValueRepository.saveAll(values);

        //update payment request
        payReq.setStatus(status);
        payReq.setStatusMessage(statusMessage);
        payReq.setStatusShown(false);
        payReq.setStatusDate(new Date());
        payReq.setProcessor("adyen");

        paymentRequestRepository.save(payReq);

        //send callback
        callbackService.postCallback(payReq);
        RequestUtils.debugRequestParams(request);
        RequestUtils.debugRequestHeaders(request);

        return ResponseEntity.accepted().body("[accepted]");
    }

    public static String fromAdyenToWingsStatus(String eventCode, Boolean success) {

        List<String> codes = new ArrayList<>();
        codes.add("AUTHORISATION");
        codes.add("AUTHORISATION_ADJUSTMENT");
        codes.add("CAPTURE");
        codes.add("VOID_PENDING_REFUND");
        codes.add("REFUND_WITH_DATA");

        if (codes.contains(eventCode)) {
            return (success) ? PaymentStatus.COMPLETED.name() : PaymentStatus.FAILED.name();
        }
        if (eventCode.equals("PENDING")) {
            return PaymentStatus.PENDING.name();
        }
        if (eventCode.equals("REFUND_FAILED")) {
            return PaymentStatus.FAILED.name();
        }
        if (eventCode.equals("REFUND")) {
            return PaymentStatus.REFUNDED.name();
        }
        if (eventCode.equals("CANCELLATION") || eventCode.equals("CANCEL_OR_REFUND")) {
            return (success) ? PaymentStatus.CANCELED.name() : PaymentStatus.FAILED.name();
        }

        return PaymentStatus.FAILED.name();
    }
}
