/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.bacs;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.gateways.PaymentGateway;
import java.util.Map;

/**
 *
 * @author seba
 */
public class BacsPaymentGateway implements PaymentGateway {

    @Override
    public void init(GatewayImplementation gatewayImplementation) {        
    }

    @Override
    public Map<String, String> getPaymentFormData(PaymentRequest request) {
        return null;
    }

    @Override
    public String getTemplateName() {
        return "pays/bacs/index";
    }

}
