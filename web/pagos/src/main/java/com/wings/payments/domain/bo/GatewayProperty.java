/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.bo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author seba
 */
@Entity
@Table(name = "gateway_properties")
public class GatewayProperty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "gateway_id")
    private Long gatewayId;
    @Size(max = 255)
    @Column(name = "property_name")
    private String propertyName;
    @Size(max = 50)
    @Column(name = "property_type")
    private String propertyType;
    @Size(max = 255)
    @Column(name = "property_enum")
    private String propertyEnum;
    @Size(max = 255)
    @Column(name = "property_default")
    private String propertyDefault;
    @Size(max = 255)
    @Column(name = "property_description")
    private String propertyDescription;

    public GatewayProperty() {
    }

    public GatewayProperty(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Long gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyEnum() {
        return propertyEnum;
    }

    public void setPropertyEnum(String propertyEnum) {
        this.propertyEnum = propertyEnum;
    }

    public String getPropertyDefault() {
        return propertyDefault;
    }

    public void setPropertyDefault(String propertyDefault) {
        this.propertyDefault = propertyDefault;
    }

    public String getPropertyDescription() {
        return propertyDescription;
    }

    public void setPropertyDescription(String propertyDescription) {
        this.propertyDescription = propertyDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GatewayProperty)) {
            return false;
        }
        GatewayProperty other = (GatewayProperty) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wings.payments.bo.domain.GatewayProperty[ id=" + id + " ]";
    }

}
