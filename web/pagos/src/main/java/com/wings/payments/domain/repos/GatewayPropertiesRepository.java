/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.domain.repos;

import com.wings.payments.domain.bo.GatewayProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 *
 * @author seba
 */
public interface GatewayPropertiesRepository extends JpaRepository<GatewayProperty, Long>, JpaSpecificationExecutor<GatewayProperty> {

    List<GatewayProperty> findByIdIn(List<Long> id);

    List<GatewayProperty> findByGatewayId(Long id);

}
