package com.wings.payments.domain.specs;

import com.wings.payments.domain.bo.PaymentRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PaymentRequestSpecs {

    public static Specification<PaymentRequest> search(String q) {
        return (Root<PaymentRequest> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicates = new ArrayList<>();
            if (q != null) {
                try {
                    Long number = Long.parseLong(q);
                    predicates.add(builder.or(builder.equal(root.get("orderNumber").as(Long.class), number),
                                   builder.or(builder.equal(root.get("id").as(Long.class), number))));
                    return builder.and(predicates.toArray(new Predicate[]{}));
                } catch (NumberFormatException e) { }

                String search = "%" + q + "%";
                predicates.add(builder.or(builder.like(root.get("billingEmail"), search),
                                          builder.like(root.get("billingFirstName"), search),
                                          builder.like(root.get("billingLastName"), search),
                                          builder.like(root.get("payCode"), search)));
            }
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
