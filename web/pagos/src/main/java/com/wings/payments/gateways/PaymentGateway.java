/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.PaymentRequest;
import java.util.Map;

/**
 *
 * @author seba
 */
public interface PaymentGateway {

    void init(GatewayImplementation gatewayImplementation);

    Map<String, String> getPaymentFormData(PaymentRequest request);

    String getTemplateName();

}
