package com.wings.payments.domain.repos;

import com.wings.payments.domain.login.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query(value="SELECT * FROM authorities WHERE user_id = ?1", nativeQuery=true)
    Role findByUserId(Long id);
}
