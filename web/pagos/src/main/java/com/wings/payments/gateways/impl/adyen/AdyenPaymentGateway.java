package com.wings.payments.gateways.impl.adyen;

import com.wings.payments.domain.bo.GatewayImplementation;
import com.wings.payments.domain.bo.GatewayImplementationProperty;
import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.gateways.PaymentGateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdyenPaymentGateway implements PaymentGateway {

    private final Map<String, String> config = new HashMap<>();

    @Override
    public void init(GatewayImplementation gatewayImplementation) {
        List<GatewayImplementationProperty> properties = gatewayImplementation.getTest() ? gatewayImplementation.getTestProperties() : gatewayImplementation.getProperties();
        properties.forEach((property) -> {
            config.put(property.getPropertyName(), property.getPropertyValue());
        });
        config.put("isTest", String.valueOf(gatewayImplementation.getTest()));
        config.put("giId", gatewayImplementation.getId().toString());
    }

    @Override
    public Map<String, String> getPaymentFormData(PaymentRequest request) {
        config.put("value", String.valueOf(request.getAmount()));
        config.put("currency", request.getCurrency());
        config.put("reference", String.valueOf(request.getId()));
        config.put("countryCode", request.getCountry());
        config.put("description", request.getTitle());
        config.put("orderNumber", String.valueOf(request.getOrderNumber()));
        return config;
    }

    @Override
    public String getTemplateName() {return "pays/adyen/index";
    }
}
