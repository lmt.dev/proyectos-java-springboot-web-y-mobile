/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.blockpays.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seba
 */
public class CurrenciesAvailable {

    private Integer status;
    private List<Currency> data = new ArrayList<>();

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Currency> getData() {
        return data;
    }

    public void setData(List<Currency> data) {
        this.data = data;
    }

}
