/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.gateways.impl.paymentez;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author seba
 */
@Controller
public class PaymentezController {

    @Autowired
    private PaymentezService paymentezService;

    private final Logger log = LoggerFactory.getLogger(PaymentezController.class);

    @RequestMapping(value = {"paymentez/response"}, method = RequestMethod.POST)
    public ResponseEntity<String> processResponse(Model model, HttpServletRequest request) {
        String content = request.getParameter("content");
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Map<String, String>> response = mapper.readValue(content, Map.class);

            if (response.containsKey("error")) {
                String resp = response.get("error").get("description");
                return ResponseEntity.badRequest().body(resp);
            }

            paymentezService.processResponse(response);

        } catch (IOException e) {
            log.error("Error on paymentez response", e);
        }
        return ResponseEntity.ok("success");
    }

}
