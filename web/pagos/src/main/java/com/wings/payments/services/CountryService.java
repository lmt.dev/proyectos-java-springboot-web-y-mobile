
package com.wings.payments.services;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CountryService {

    private static final List<String> COUNTRIES = Arrays.asList("AR","BO","BR","CL","CO","CR","DO","EC","ES",
            "GT","HN","IT","MX","NI","OT","PA","PE","SV","US","OT");
    public List<String> findAll() {
        return COUNTRIES;
    }
}