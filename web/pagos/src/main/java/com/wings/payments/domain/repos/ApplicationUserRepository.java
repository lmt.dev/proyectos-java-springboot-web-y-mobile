package com.wings.payments.domain.repos;

import com.wings.payments.domain.login.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {

    ApplicationUser findByUsername(String username);

    @Modifying
    @Query(value="INSERT INTO authorities (user_id, authority) VALUES (?1,?2)", nativeQuery = true)
    void saveRole(Long user_id, String role);

    boolean existsByUsernameAndIdNot(String username, Long id);

    boolean existsByUsername(String username);

    @Query(value="SELECT * from users WHERE disabled = 0", nativeQuery = true)
    List<ApplicationUser> findAllEnabled();

    ApplicationUser findByEmail(String email);
}
