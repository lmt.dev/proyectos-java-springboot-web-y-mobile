/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.payments.services;

import com.wings.payments.domain.bo.PaymentRequest;
import com.wings.payments.domain.bo.PaymentRequestExtra;
import com.wings.payments.domain.bo.PaymentRequestsCallback;
import com.wings.payments.domain.dto.CallbackDTO;
import com.wings.payments.domain.repos.PaymentRequestCallbackRepository;
import java.net.URI;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author seba
 */
@Service
public class CallbackService {

    @Autowired
    private PaymentRequestCallbackRepository paymentRequestCallbackRepository;

    private final Logger log = LoggerFactory.getLogger(CallbackService.class);
    private static final String SALT = "bbMox7Jc";

    /**
     *
     * @param req
     */
    public void postCallback(PaymentRequest req) {

        CallbackDTO dto = new CallbackDTO();
        dto.setOrderNumber(String.valueOf(req.getOrderNumber()));
        dto.setStatus(req.getStatus());
        dto.setStatusMessage(req.getStatusMessage());
        dto.setStatusDate(req.getStatusDate());
        dto.setProcessor(req.getProcessor());

        List<PaymentRequestExtra> extras = req.getExtras();
        extras.forEach((extra) -> {
            dto.addExtra(extra.getExtraName(), extra.getExtraValue());
        });

        //create signature
        String toSign = req.getRequesterName() + "-" + req.getOrderNumber() + "-" + SALT;
        String signature = DigestUtils.sha256Hex(toSign);

        try {
            RestTemplate template = new RestTemplate();
            RequestEntity<CallbackDTO> requestEntity = RequestEntity
                    .post(new URI(req.getCallbackUrl()))
                    .header("signature", signature)
                    .header("content-type", MediaType.APPLICATION_JSON_VALUE)
                    .body(dto);

            ResponseEntity<String> response = template.exchange(requestEntity, String.class);

            //insert paymentRequest callbacks
            PaymentRequestsCallback prc = new PaymentRequestsCallback();
            prc.setResponseStatus(response.getStatusCodeValue());
            prc.setResponseStatusMessage(response.getBody());
            prc.setPaymentRequestId(req.getId());
            paymentRequestCallbackRepository.save(prc);

        } catch (Exception e) {
            log.error("Error making callback", e);
        }

    }

}
