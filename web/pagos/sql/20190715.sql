---comisiones argentina de prueba
INSERT INTO wings_payments.gateway_implementation (gateway_id, country, `name`, enabled, test) 
	VALUES 
                (12, 'AR', 'Comisiones de Afiliado', false, false);

INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES 
                (14, 26, 'api_url', 'https://beta.wingsmobile.net/api/payments/coms/charge', false),
                (14, 26, 'api_url', 'http://sebatest2.mooo.com/api/payments/coms/charge', true),
                (14, 27, 'api_key', 'Eeop8pxYqtaa9mTH', false),
                (14, 27, 'api_key', 'Eeop8pxYqtaa9mTH', true);