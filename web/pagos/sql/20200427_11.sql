INSERT INTO wings_payments.gateways
(name, code, image, available_countries, `type`, service_class_name)
VALUES('Adyen', 'adyen', 'far fa-credit-card', 'ES,CO,PE,EC', 'card', 'adyen.AdyenPaymentGateway');

INSERT INTO wings_payments.gateway_properties
(gateway_id, property_name, property_type, property_enum, property_default, property_description)
VALUES(13, 'apikey', 'string', NULL, NULL, 'Your Api key');

INSERT INTO wings_payments.gateway_properties
(gateway_id, property_name, property_type, property_enum, property_default, property_description)
VALUES(13, 'merchantAccount', 'string', null, null, 'Your merchant account');