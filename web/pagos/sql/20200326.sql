CREATE TABLE wings_payments.users (
	id INT auto_increment NOT NULL,
	username varchar(40) NOT NULL,
	password varchar(60) NOT NULL,
	enabled TINYINT(1) DEFAULT 1 NOT NULL,
	CONSTRAINT users_PK PRIMARY KEY (id),
	CONSTRAINT users_UN UNIQUE KEY (username)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;


CREATE TABLE wings_payments.authorities (
	id INT auto_increment NOT NULL,
	user_id INT NOT NULL,
	authority varchar(45) NOT NULL,
	CONSTRAINT authorities_PK PRIMARY KEY (id),
	CONSTRAINT fk_authorities_users FOREIGN KEY (user_id) REFERENCES wings_payments.users(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;
CREATE UNIQUE INDEX user_id_authority_unique USING BTREE ON wings_payments.authorities (user_id,authority);


insert into users (username, password, enabled ) values ('user','$2a$10$jswzu5JE4JfctyJEp8.wY.ya.n3KwOlIObGIIFJv11RzsNhulQQSa',1);
insert into users (username, password, enabled ) values ('admin','$2a$10$fmGiCjdWjBLKxjZxD0OseOTX3MzlNuG7IhxRz8nDPbeR8onUUAsLG',1);

INSERT INTO authorities (user_id, authority) VALUES (1, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (2, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES (2, 'ROLE_ADMIN');