ALTER TABLE wings_payments.users ADD email varchar(100) NULL;
ALTER TABLE wings_payments.users ADD name varchar(100) NULL;
ALTER TABLE wings_payments.users ADD lastname varchar(100) NULL;
ALTER TABLE wings_payments.users ADD disabled TINYINT(1) DEFAULT 0 NULL;

UPDATE wings_payments.users SET email='admin@admin.com', name='Admin', lastname='Admin', disabled=0 WHERE username='admin';