INSERT INTO wings_payments.gateway_properties (gateway_id, property_name, property_type, property_enum, property_default, property_description) 
	VALUES 
                (12, 'api_url', 'string', NULL, NULL, 'url del backkofice para generar los pagos'),
                (12, 'api_key', 'string', NULL, NULL, 'salt para el signature');


INSERT INTO wings_payments.gateway_implementation (gateway_id, country, `name`, enabled, test) 
	VALUES 
                (12, 'EC', 'Comisiones de Afiliado', false, false),
                (12, 'ES', 'Comisiones de Afiliado', false, false),
                (12, 'PE', 'Comisiones de Afiliado', false, false),
                (12, 'CO', 'Comisiones de Afiliado', false, false);

INSERT INTO wings_payments.gateway_implementation_properties (gateway_implementation_id, property_id, property_name, property_value, property_test) 
	VALUES 
                (10, 26, 'api_url', 'https://beta.wingsmobile.net/api/payments/coms/charge', false),
                (10, 26, 'api_url', 'http://sebatest2.mooo.com/api/payments/coms/charge', true),
                (10, 27, 'api_key', 'Eeop8pxYqtaa9mTH', false),
                (10, 27, 'api_key', 'Eeop8pxYqtaa9mTH', true),
                (11, 26, 'api_url', 'https://beta.wingsmobile.net/api/payments/coms/charge', false),
                (11, 26, 'api_url', 'http://sebatest2.mooo.com/api/payments/coms/charge', true),
                (11, 27, 'api_key', 'Eeop8pxYqtaa9mTH', false),
                (11, 27, 'api_key', 'Eeop8pxYqtaa9mTH', true),
                (12, 26, 'api_url', 'https://beta.wingsmobile.net/api/payments/coms/charge', false),
                (12, 26, 'api_url', 'http://sebatest2.mooo.com/api/payments/coms/charge', true),
                (12, 27, 'api_key', 'Eeop8pxYqtaa9mTH', false),
                (12, 27, 'api_key', 'Eeop8pxYqtaa9mTH', true),
                (13, 26, 'api_url', 'https://beta.wingsmobile.net/api/payments/coms/charge', false),
                (13, 26, 'api_url', 'http://sebatest2.mooo.com/api/payments/coms/charge', true),
                (13, 27, 'api_key', 'Eeop8pxYqtaa9mTH', false),
                (13, 27, 'api_key', 'Eeop8pxYqtaa9mTH', true);

UPDATE wings_payments.gateways SET `service_class_name` = 'coms.ComsPaymentGateway' WHERE id = 12;

alter table payment_requests add column coms_discount_amount decimal(24,6) default NULL COMMENT 'Si es distinto de null, para pago con comisiones usar este monto';

UPDATE wings_payments.gateways SET `image` = 'fa fa-cc-stripe' WHERE id = 5;
UPDATE wings_payments.gateways SET `image` = 'fa fa-btc' WHERE id = 9;
UPDATE wings_payments.gateways SET `image` = 'fa fa-money' WHERE id = 12;



