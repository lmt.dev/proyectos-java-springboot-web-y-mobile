ALTER TABLE wings_payments.payment_requests ADD regenerated_by varchar(100) NULL;
ALTER TABLE wings_payments.payment_requests ADD regenerated_at DATETIME NULL;