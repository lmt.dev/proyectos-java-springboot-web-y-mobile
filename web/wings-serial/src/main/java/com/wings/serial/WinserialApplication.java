package com.wings.serial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WinserialApplication {

	public static void main(String[] args) {
		SpringApplication.run(WinserialApplication.class, args);
	}

}
