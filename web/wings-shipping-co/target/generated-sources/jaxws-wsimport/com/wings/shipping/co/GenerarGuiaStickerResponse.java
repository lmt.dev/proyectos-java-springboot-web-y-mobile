
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerarGuiaStickerResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bytesReport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generarGuiaStickerResult",
    "bytesReport"
})
@XmlRootElement(name = "GenerarGuiaStickerResponse")
public class GenerarGuiaStickerResponse {

    @XmlElement(name = "GenerarGuiaStickerResult")
    protected boolean generarGuiaStickerResult;
    protected byte[] bytesReport;

    /**
     * Obtiene el valor de la propiedad generarGuiaStickerResult.
     * 
     */
    public boolean isGenerarGuiaStickerResult() {
        return generarGuiaStickerResult;
    }

    /**
     * Define el valor de la propiedad generarGuiaStickerResult.
     * 
     */
    public void setGenerarGuiaStickerResult(boolean value) {
        this.generarGuiaStickerResult = value;
    }

    /**
     * Obtiene el valor de la propiedad bytesReport.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBytesReport() {
        return bytesReport;
    }

    /**
     * Define el valor de la propiedad bytesReport.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBytesReport(byte[] value) {
        this.bytesReport = value;
    }

}
