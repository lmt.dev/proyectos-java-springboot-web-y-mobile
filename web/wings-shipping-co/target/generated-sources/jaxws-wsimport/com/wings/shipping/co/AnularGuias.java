
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="num_Guia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="num_GuiaFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="interno" type="{http://tempuri.org/}ArrayOfResultadoAnulacionGuias" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numGuia",
    "numGuiaFinal",
    "interno"
})
@XmlRootElement(name = "AnularGuias")
public class AnularGuias {

    @XmlElement(name = "num_Guia", required = true)
    protected BigDecimal numGuia;
    @XmlElement(name = "num_GuiaFinal", required = true)
    protected BigDecimal numGuiaFinal;
    protected ArrayOfResultadoAnulacionGuias interno;

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuia(BigDecimal value) {
        this.numGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad numGuiaFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumGuiaFinal() {
        return numGuiaFinal;
    }

    /**
     * Define el valor de la propiedad numGuiaFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumGuiaFinal(BigDecimal value) {
        this.numGuiaFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad interno.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultadoAnulacionGuias }
     *     
     */
    public ArrayOfResultadoAnulacionGuias getInterno() {
        return interno;
    }

    /**
     * Define el valor de la propiedad interno.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultadoAnulacionGuias }
     *     
     */
    public void setInterno(ArrayOfResultadoAnulacionGuias value) {
        this.interno = value;
    }

}
