
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultarTipoGuiasXCodSer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultarTipoGuiasXCodSer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ide_Producto" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Nom_Producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Forma_Pago" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Nom_forma_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Tipo_Guia" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarTipoGuiasXCodSer", propOrder = {
    "ideProducto",
    "nomProducto",
    "ideFormaPago",
    "nomFormaPago",
    "ideTipoGuia"
})
public class ConsultarTipoGuiasXCodSer {

    @XmlElement(name = "Ide_Producto")
    protected short ideProducto;
    @XmlElement(name = "Nom_Producto")
    protected String nomProducto;
    @XmlElement(name = "Ide_Forma_Pago")
    protected short ideFormaPago;
    @XmlElement(name = "Nom_forma_pago")
    protected String nomFormaPago;
    @XmlElement(name = "Ide_Tipo_Guia")
    protected short ideTipoGuia;

    /**
     * Obtiene el valor de la propiedad ideProducto.
     * 
     */
    public short getIdeProducto() {
        return ideProducto;
    }

    /**
     * Define el valor de la propiedad ideProducto.
     * 
     */
    public void setIdeProducto(short value) {
        this.ideProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad nomProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomProducto() {
        return nomProducto;
    }

    /**
     * Define el valor de la propiedad nomProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomProducto(String value) {
        this.nomProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad ideFormaPago.
     * 
     */
    public short getIdeFormaPago() {
        return ideFormaPago;
    }

    /**
     * Define el valor de la propiedad ideFormaPago.
     * 
     */
    public void setIdeFormaPago(short value) {
        this.ideFormaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad nomFormaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomFormaPago() {
        return nomFormaPago;
    }

    /**
     * Define el valor de la propiedad nomFormaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomFormaPago(String value) {
        this.nomFormaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad ideTipoGuia.
     * 
     */
    public short getIdeTipoGuia() {
        return ideTipoGuia;
    }

    /**
     * Define el valor de la propiedad ideTipoGuia.
     * 
     */
    public void setIdeTipoGuia(short value) {
        this.ideTipoGuia = value;
    }

}
