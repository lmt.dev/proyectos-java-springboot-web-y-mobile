
package com.wings.shipping.co;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfEnviosExterno complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEnviosExterno">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EnviosExterno" type="{http://tempuri.org/}EnviosExterno" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEnviosExterno", propOrder = {
    "enviosExterno"
})
public class ArrayOfEnviosExterno {

    @XmlElement(name = "EnviosExterno", nillable = true)
    protected List<EnviosExterno> enviosExterno;

    /**
     * Gets the value of the enviosExterno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enviosExterno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnviosExterno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnviosExterno }
     * 
     * 
     */
    public List<EnviosExterno> getEnviosExterno() {
        if (enviosExterno == null) {
            enviosExterno = new ArrayList<EnviosExterno>();
        }
        return this.enviosExterno;
    }

}
