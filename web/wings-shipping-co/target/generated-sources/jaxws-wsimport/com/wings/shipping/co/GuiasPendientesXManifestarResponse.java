
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GuiasPendientesXManifestarResult" type="{http://tempuri.org/}ArrayOfConsultaGuiasXManifestar" minOccurs="0"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "guiasPendientesXManifestarResult",
    "mensaje"
})
@XmlRootElement(name = "GuiasPendientesXManifestarResponse")
public class GuiasPendientesXManifestarResponse {

    @XmlElement(name = "GuiasPendientesXManifestarResult")
    protected ArrayOfConsultaGuiasXManifestar guiasPendientesXManifestarResult;
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad guiasPendientesXManifestarResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfConsultaGuiasXManifestar }
     *     
     */
    public ArrayOfConsultaGuiasXManifestar getGuiasPendientesXManifestarResult() {
        return guiasPendientesXManifestarResult;
    }

    /**
     * Define el valor de la propiedad guiasPendientesXManifestarResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfConsultaGuiasXManifestar }
     *     
     */
    public void setGuiasPendientesXManifestarResult(ArrayOfConsultaGuiasXManifestar value) {
        this.guiasPendientesXManifestarResult = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

}
