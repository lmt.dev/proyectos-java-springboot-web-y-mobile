
package com.wings.shipping.co;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.wings.shipping.co package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthHeader_QNAME = new QName("http://tempuri.org/", "AuthHeader");
    private final static QName _AuthHeader2_QNAME = new QName("http://tempuri.org/", "AuthHeader2");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.wings.shipping.co
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerDataReimpresionResponse }
     * 
     */
    public GenerarGuiaStikerDataReimpresionResponse createGenerarGuiaStikerDataReimpresionResponse() {
        return new GenerarGuiaStikerDataReimpresionResponse();
    }

    /**
     * Create an instance of {@link InsertarSertEnvio }
     * 
     */
    public InsertarSertEnvio createInsertarSertEnvio() {
        return new InsertarSertEnvio();
    }

    /**
     * Create an instance of {@link ValidarActualizarEnvio }
     * 
     */
    public ValidarActualizarEnvio createValidarActualizarEnvio() {
        return new ValidarActualizarEnvio();
    }

    /**
     * Create an instance of {@link ActualizarEnvioAgregarUnidades }
     * 
     */
    public ActualizarEnvioAgregarUnidades createActualizarEnvioAgregarUnidades() {
        return new ActualizarEnvioAgregarUnidades();
    }

    /**
     * Create an instance of {@link GenerarManifiesto }
     * 
     */
    public GenerarManifiesto createGenerarManifiesto() {
        return new GenerarManifiesto();
    }

    /**
     * Create an instance of {@link ConsultarRestriccionesFisicasEnviosResponse }
     * 
     */
    public ConsultarRestriccionesFisicasEnviosResponse createConsultarRestriccionesFisicasEnviosResponse() {
        return new ConsultarRestriccionesFisicasEnviosResponse();
    }

    /**
     * Create an instance of {@link ConsultarGuiasByNumDocumentoResponse }
     * 
     */
    public ConsultarGuiasByNumDocumentoResponse createConsultarGuiasByNumDocumentoResponse() {
        return new ConsultarGuiasByNumDocumentoResponse();
    }

    /**
     * Create an instance of {@link ValidarActualizarEnvioResponse }
     * 
     */
    public ValidarActualizarEnvioResponse createValidarActualizarEnvioResponse() {
        return new ValidarActualizarEnvioResponse();
    }

    /**
     * Create an instance of {@link ConsultarUnidadEmpaqueIdLoginResponse }
     * 
     */
    public ConsultarUnidadEmpaqueIdLoginResponse createConsultarUnidadEmpaqueIdLoginResponse() {
        return new ConsultarUnidadEmpaqueIdLoginResponse();
    }

    /**
     * Create an instance of {@link CrearUnidadesEmpaqueBlancoResponse }
     * 
     */
    public CrearUnidadesEmpaqueBlancoResponse createCrearUnidadesEmpaqueBlancoResponse() {
        return new CrearUnidadesEmpaqueBlancoResponse();
    }

    /**
     * Create an instance of {@link ActualizarSertEnvio }
     * 
     */
    public ActualizarSertEnvio createActualizarSertEnvio() {
        return new ActualizarSertEnvio();
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerDataResponse }
     * 
     */
    public GenerarGuiaStikerDataResponse createGenerarGuiaStikerDataResponse() {
        return new GenerarGuiaStikerDataResponse();
    }

    /**
     * Create an instance of {@link CrearUnidadesEmpaqueBlanco }
     * 
     */
    public CrearUnidadesEmpaqueBlanco createCrearUnidadesEmpaqueBlanco() {
        return new CrearUnidadesEmpaqueBlanco();
    }

    /**
     * Create an instance of {@link ActualizarSertEnvioResponse }
     * 
     */
    public ActualizarSertEnvioResponse createActualizarSertEnvioResponse() {
        return new ActualizarSertEnvioResponse();
    }

    /**
     * Create an instance of {@link GenerarGuiaSticker }
     * 
     */
    public GenerarGuiaSticker createGenerarGuiaSticker() {
        return new GenerarGuiaSticker();
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerDataReimpresionResponse.GenerarGuiaStikerDataReimpresionResult }
     * 
     */
    public GenerarGuiaStikerDataReimpresionResponse.GenerarGuiaStikerDataReimpresionResult createGenerarGuiaStikerDataReimpresionResponseGenerarGuiaStikerDataReimpresionResult() {
        return new GenerarGuiaStikerDataReimpresionResponse.GenerarGuiaStikerDataReimpresionResult();
    }

    /**
     * Create an instance of {@link ConsultaRangosGuias }
     * 
     */
    public ConsultaRangosGuias createConsultaRangosGuias() {
        return new ConsultaRangosGuias();
    }

    /**
     * Create an instance of {@link RangoGuiasResponse }
     * 
     */
    public RangoGuiasResponse createRangoGuiasResponse() {
        return new RangoGuiasResponse();
    }

    /**
     * Create an instance of {@link ArrayOfConsultarGuiasTotalesResultWS }
     * 
     */
    public ArrayOfConsultarGuiasTotalesResultWS createArrayOfConsultarGuiasTotalesResultWS() {
        return new ArrayOfConsultarGuiasTotalesResultWS();
    }

    /**
     * Create an instance of {@link GenerarManifiestoResponse }
     * 
     */
    public GenerarManifiestoResponse createGenerarManifiestoResponse() {
        return new GenerarManifiestoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfErrorGeneradoPorGuia }
     * 
     */
    public ArrayOfErrorGeneradoPorGuia createArrayOfErrorGeneradoPorGuia() {
        return new ArrayOfErrorGeneradoPorGuia();
    }

    /**
     * Create an instance of {@link GuiasPendientesXManifestarResponse }
     * 
     */
    public GuiasPendientesXManifestarResponse createGuiasPendientesXManifestarResponse() {
        return new GuiasPendientesXManifestarResponse();
    }

    /**
     * Create an instance of {@link ArrayOfConsultaGuiasXManifestar }
     * 
     */
    public ArrayOfConsultaGuiasXManifestar createArrayOfConsultaGuiasXManifestar() {
        return new ArrayOfConsultaGuiasXManifestar();
    }

    /**
     * Create an instance of {@link ConsultarGuiaReimpresionResponse }
     * 
     */
    public ConsultarGuiaReimpresionResponse createConsultarGuiaReimpresionResponse() {
        return new ConsultarGuiaReimpresionResponse();
    }

    /**
     * Create an instance of {@link AnularGuias }
     * 
     */
    public AnularGuias createAnularGuias() {
        return new AnularGuias();
    }

    /**
     * Create an instance of {@link ArrayOfResultadoAnulacionGuias }
     * 
     */
    public ArrayOfResultadoAnulacionGuias createArrayOfResultadoAnulacionGuias() {
        return new ArrayOfResultadoAnulacionGuias();
    }

    /**
     * Create an instance of {@link GenerarGuiaStickerTiendasVirtualesResponse }
     * 
     */
    public GenerarGuiaStickerTiendasVirtualesResponse createGenerarGuiaStickerTiendasVirtualesResponse() {
        return new GenerarGuiaStickerTiendasVirtualesResponse();
    }

    /**
     * Create an instance of {@link InsertarSertEnvio.Documento }
     * 
     */
    public InsertarSertEnvio.Documento createInsertarSertEnvioDocumento() {
        return new InsertarSertEnvio.Documento();
    }

    /**
     * Create an instance of {@link CargueMasivoExternoDetalle }
     * 
     */
    public CargueMasivoExternoDetalle createCargueMasivoExternoDetalle() {
        return new CargueMasivoExternoDetalle();
    }

    /**
     * Create an instance of {@link ArrayOfCargueMasivoExternoDetalleDTO }
     * 
     */
    public ArrayOfCargueMasivoExternoDetalleDTO createArrayOfCargueMasivoExternoDetalleDTO() {
        return new ArrayOfCargueMasivoExternoDetalleDTO();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link ValidarActualizarEnvio.Documento }
     * 
     */
    public ValidarActualizarEnvio.Documento createValidarActualizarEnvioDocumento() {
        return new ValidarActualizarEnvio.Documento();
    }

    /**
     * Create an instance of {@link DesencriptarContrasena }
     * 
     */
    public DesencriptarContrasena createDesencriptarContrasena() {
        return new DesencriptarContrasena();
    }

    /**
     * Create an instance of {@link GenerarGuiaStickerMobile }
     * 
     */
    public GenerarGuiaStickerMobile createGenerarGuiaStickerMobile() {
        return new GenerarGuiaStickerMobile();
    }

    /**
     * Create an instance of {@link ActualizarEnvioAgregarUnidades.Documento }
     * 
     */
    public ActualizarEnvioAgregarUnidades.Documento createActualizarEnvioAgregarUnidadesDocumento() {
        return new ActualizarEnvioAgregarUnidades.Documento();
    }

    /**
     * Create an instance of {@link RangoGuias }
     * 
     */
    public RangoGuias createRangoGuias() {
        return new RangoGuias();
    }

    /**
     * Create an instance of {@link GuiasPendientesXManifestar }
     * 
     */
    public GuiasPendientesXManifestar createGuiasPendientesXManifestar() {
        return new GuiasPendientesXManifestar();
    }

    /**
     * Create an instance of {@link GenerarGuiaStickerTiendasVirtuales }
     * 
     */
    public GenerarGuiaStickerTiendasVirtuales createGenerarGuiaStickerTiendasVirtuales() {
        return new GenerarGuiaStickerTiendasVirtuales();
    }

    /**
     * Create an instance of {@link DesencriptarContrasenaResponse }
     * 
     */
    public DesencriptarContrasenaResponse createDesencriptarContrasenaResponse() {
        return new DesencriptarContrasenaResponse();
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerData }
     * 
     */
    public GenerarGuiaStikerData createGenerarGuiaStikerData() {
        return new GenerarGuiaStikerData();
    }

    /**
     * Create an instance of {@link ArrayOfLong }
     * 
     */
    public ArrayOfLong createArrayOfLong() {
        return new ArrayOfLong();
    }

    /**
     * Create an instance of {@link GenerarGuiaStickerResponse }
     * 
     */
    public GenerarGuiaStickerResponse createGenerarGuiaStickerResponse() {
        return new GenerarGuiaStickerResponse();
    }

    /**
     * Create an instance of {@link ConsultarUnidadEmpaqueIdLogin }
     * 
     */
    public ConsultarUnidadEmpaqueIdLogin createConsultarUnidadEmpaqueIdLogin() {
        return new ConsultarUnidadEmpaqueIdLogin();
    }

    /**
     * Create an instance of {@link ConsultarGuiasByNumDocumento }
     * 
     */
    public ConsultarGuiasByNumDocumento createConsultarGuiasByNumDocumento() {
        return new ConsultarGuiasByNumDocumento();
    }

    /**
     * Create an instance of {@link GenerarManifiesto.ListaGuiasXml }
     * 
     */
    public GenerarManifiesto.ListaGuiasXml createGenerarManifiestoListaGuiasXml() {
        return new GenerarManifiesto.ListaGuiasXml();
    }

    /**
     * Create an instance of {@link ConsultarRestriccionesFisicasEnviosResponse.ConsultarRestriccionesFisicasEnviosResult }
     * 
     */
    public ConsultarRestriccionesFisicasEnviosResponse.ConsultarRestriccionesFisicasEnviosResult createConsultarRestriccionesFisicasEnviosResponseConsultarRestriccionesFisicasEnviosResult() {
        return new ConsultarRestriccionesFisicasEnviosResponse.ConsultarRestriccionesFisicasEnviosResult();
    }

    /**
     * Create an instance of {@link AnularGuiasResponse }
     * 
     */
    public AnularGuiasResponse createAnularGuiasResponse() {
        return new AnularGuiasResponse();
    }

    /**
     * Create an instance of {@link ConsultarGuiasByNumDocumentoResponse.ConsultarGuiasByNumDocumentoResult }
     * 
     */
    public ConsultarGuiasByNumDocumentoResponse.ConsultarGuiasByNumDocumentoResult createConsultarGuiasByNumDocumentoResponseConsultarGuiasByNumDocumentoResult() {
        return new ConsultarGuiasByNumDocumentoResponse.ConsultarGuiasByNumDocumentoResult();
    }

    /**
     * Create an instance of {@link CargueMasivoExterno }
     * 
     */
    public CargueMasivoExterno createCargueMasivoExterno() {
        return new CargueMasivoExterno();
    }

    /**
     * Create an instance of {@link ArrayOfCargueMasivoExternoDTO }
     * 
     */
    public ArrayOfCargueMasivoExternoDTO createArrayOfCargueMasivoExternoDTO() {
        return new ArrayOfCargueMasivoExternoDTO();
    }

    /**
     * Create an instance of {@link ConsultarTipoGuiasResponse }
     * 
     */
    public ConsultarTipoGuiasResponse createConsultarTipoGuiasResponse() {
        return new ConsultarTipoGuiasResponse();
    }

    /**
     * Create an instance of {@link ArrayOfConsultarTipoGuiasXCodSer }
     * 
     */
    public ArrayOfConsultarTipoGuiasXCodSer createArrayOfConsultarTipoGuiasXCodSer() {
        return new ArrayOfConsultarTipoGuiasXCodSer();
    }

    /**
     * Create an instance of {@link CargueMasivoExternoDetalleResponse }
     * 
     */
    public CargueMasivoExternoDetalleResponse createCargueMasivoExternoDetalleResponse() {
        return new CargueMasivoExternoDetalleResponse();
    }

    /**
     * Create an instance of {@link ActualizarEnvioAgregarUnidadesResponse }
     * 
     */
    public ActualizarEnvioAgregarUnidadesResponse createActualizarEnvioAgregarUnidadesResponse() {
        return new ActualizarEnvioAgregarUnidadesResponse();
    }

    /**
     * Create an instance of {@link CargueMasivoExternoResponse }
     * 
     */
    public CargueMasivoExternoResponse createCargueMasivoExternoResponse() {
        return new CargueMasivoExternoResponse();
    }

    /**
     * Create an instance of {@link ValidarActualizarEnvioResponse.ValidarActualizarEnvioResult }
     * 
     */
    public ValidarActualizarEnvioResponse.ValidarActualizarEnvioResult createValidarActualizarEnvioResponseValidarActualizarEnvioResult() {
        return new ValidarActualizarEnvioResponse.ValidarActualizarEnvioResult();
    }

    /**
     * Create an instance of {@link ConsultarUnidadEmpaqueIdLoginResponse.ConsultarUnidadEmpaqueIdLoginResult }
     * 
     */
    public ConsultarUnidadEmpaqueIdLoginResponse.ConsultarUnidadEmpaqueIdLoginResult createConsultarUnidadEmpaqueIdLoginResponseConsultarUnidadEmpaqueIdLoginResult() {
        return new ConsultarUnidadEmpaqueIdLoginResponse.ConsultarUnidadEmpaqueIdLoginResult();
    }

    /**
     * Create an instance of {@link AuthHeader }
     * 
     */
    public AuthHeader createAuthHeader() {
        return new AuthHeader();
    }

    /**
     * Create an instance of {@link InsertarSertEnvioResponse }
     * 
     */
    public InsertarSertEnvioResponse createInsertarSertEnvioResponse() {
        return new InsertarSertEnvioResponse();
    }

    /**
     * Create an instance of {@link AuthHeader2 }
     * 
     */
    public AuthHeader2 createAuthHeader2() {
        return new AuthHeader2();
    }

    /**
     * Create an instance of {@link CrearUnidadesEmpaqueBlancoResponse.CrearUnidadesEmpaqueBlancoResult }
     * 
     */
    public CrearUnidadesEmpaqueBlancoResponse.CrearUnidadesEmpaqueBlancoResult createCrearUnidadesEmpaqueBlancoResponseCrearUnidadesEmpaqueBlancoResult() {
        return new CrearUnidadesEmpaqueBlancoResponse.CrearUnidadesEmpaqueBlancoResult();
    }

    /**
     * Create an instance of {@link ActualizarSertEnvio.Documento }
     * 
     */
    public ActualizarSertEnvio.Documento createActualizarSertEnvioDocumento() {
        return new ActualizarSertEnvio.Documento();
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerDataReimpresion }
     * 
     */
    public GenerarGuiaStikerDataReimpresion createGenerarGuiaStikerDataReimpresion() {
        return new GenerarGuiaStikerDataReimpresion();
    }

    /**
     * Create an instance of {@link ConsultarGuiaReimpresion }
     * 
     */
    public ConsultarGuiaReimpresion createConsultarGuiaReimpresion() {
        return new ConsultarGuiaReimpresion();
    }

    /**
     * Create an instance of {@link EncriptarContrasenaResponse }
     * 
     */
    public EncriptarContrasenaResponse createEncriptarContrasenaResponse() {
        return new EncriptarContrasenaResponse();
    }

    /**
     * Create an instance of {@link ConsultaRangosGuiasResponse }
     * 
     */
    public ConsultaRangosGuiasResponse createConsultaRangosGuiasResponse() {
        return new ConsultaRangosGuiasResponse();
    }

    /**
     * Create an instance of {@link EncriptarContrasena }
     * 
     */
    public EncriptarContrasena createEncriptarContrasena() {
        return new EncriptarContrasena();
    }

    /**
     * Create an instance of {@link GenerarGuiaStickerMobileResponse }
     * 
     */
    public GenerarGuiaStickerMobileResponse createGenerarGuiaStickerMobileResponse() {
        return new GenerarGuiaStickerMobileResponse();
    }

    /**
     * Create an instance of {@link ConsultarRestriccionesFisicasEnvios }
     * 
     */
    public ConsultarRestriccionesFisicasEnvios createConsultarRestriccionesFisicasEnvios() {
        return new ConsultarRestriccionesFisicasEnvios();
    }

    /**
     * Create an instance of {@link GenerarGuiaStikerDataResponse.GenerarGuiaStikerDataResult }
     * 
     */
    public GenerarGuiaStikerDataResponse.GenerarGuiaStikerDataResult createGenerarGuiaStikerDataResponseGenerarGuiaStikerDataResult() {
        return new GenerarGuiaStikerDataResponse.GenerarGuiaStikerDataResult();
    }

    /**
     * Create an instance of {@link ConsultarTipoGuias }
     * 
     */
    public ConsultarTipoGuias createConsultarTipoGuias() {
        return new ConsultarTipoGuias();
    }

    /**
     * Create an instance of {@link ArrayOfEnviosUnidadEmpaqueCargue }
     * 
     */
    public ArrayOfEnviosUnidadEmpaqueCargue createArrayOfEnviosUnidadEmpaqueCargue() {
        return new ArrayOfEnviosUnidadEmpaqueCargue();
    }

    /**
     * Create an instance of {@link CargueMasivoExternoDTO }
     * 
     */
    public CargueMasivoExternoDTO createCargueMasivoExternoDTO() {
        return new CargueMasivoExternoDTO();
    }

    /**
     * Create an instance of {@link EnviosUnidadEmpaqueCargueDetalle }
     * 
     */
    public EnviosUnidadEmpaqueCargueDetalle createEnviosUnidadEmpaqueCargueDetalle() {
        return new EnviosUnidadEmpaqueCargueDetalle();
    }

    /**
     * Create an instance of {@link ConsultarTipoGuiasXCodSer }
     * 
     */
    public ConsultarTipoGuiasXCodSer createConsultarTipoGuiasXCodSer() {
        return new ConsultarTipoGuiasXCodSer();
    }

    /**
     * Create an instance of {@link CargueMasivoExternoDetalleDTO }
     * 
     */
    public CargueMasivoExternoDetalleDTO createCargueMasivoExternoDetalleDTO() {
        return new CargueMasivoExternoDetalleDTO();
    }

    /**
     * Create an instance of {@link ConsultarGuiasTotalesResultWS }
     * 
     */
    public ConsultarGuiasTotalesResultWS createConsultarGuiasTotalesResultWS() {
        return new ConsultarGuiasTotalesResultWS();
    }

    /**
     * Create an instance of {@link EnviosExternoDetalle }
     * 
     */
    public EnviosExternoDetalle createEnviosExternoDetalle() {
        return new EnviosExternoDetalle();
    }

    /**
     * Create an instance of {@link ArrayOfEnviosExterno }
     * 
     */
    public ArrayOfEnviosExterno createArrayOfEnviosExterno() {
        return new ArrayOfEnviosExterno();
    }

    /**
     * Create an instance of {@link EnviosExterno }
     * 
     */
    public EnviosExterno createEnviosExterno() {
        return new EnviosExterno();
    }

    /**
     * Create an instance of {@link ArrayOfEnviosExternoDetalle }
     * 
     */
    public ArrayOfEnviosExternoDetalle createArrayOfEnviosExternoDetalle() {
        return new ArrayOfEnviosExternoDetalle();
    }

    /**
     * Create an instance of {@link EnviosUnidadEmpaqueCargue }
     * 
     */
    public EnviosUnidadEmpaqueCargue createEnviosUnidadEmpaqueCargue() {
        return new EnviosUnidadEmpaqueCargue();
    }

    /**
     * Create an instance of {@link ConsultaGuiasXManifestar }
     * 
     */
    public ConsultaGuiasXManifestar createConsultaGuiasXManifestar() {
        return new ConsultaGuiasXManifestar();
    }

    /**
     * Create an instance of {@link ArrayOfEnviosUnidadEmpaqueCargueDetalle }
     * 
     */
    public ArrayOfEnviosUnidadEmpaqueCargueDetalle createArrayOfEnviosUnidadEmpaqueCargueDetalle() {
        return new ArrayOfEnviosUnidadEmpaqueCargueDetalle();
    }

    /**
     * Create an instance of {@link ResultadoAnulacionGuias }
     * 
     */
    public ResultadoAnulacionGuias createResultadoAnulacionGuias() {
        return new ResultadoAnulacionGuias();
    }

    /**
     * Create an instance of {@link ErrorGeneradoPorGuia }
     * 
     */
    public ErrorGeneradoPorGuia createErrorGeneradoPorGuia() {
        return new ErrorGeneradoPorGuia();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AuthHeader")
    public JAXBElement<AuthHeader> createAuthHeader(AuthHeader value) {
        return new JAXBElement<AuthHeader>(_AuthHeader_QNAME, AuthHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthHeader2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AuthHeader2")
    public JAXBElement<AuthHeader2> createAuthHeader2(AuthHeader2 value) {
        return new JAXBElement<AuthHeader2>(_AuthHeader2_QNAME, AuthHeader2 .class, null, value);
    }

}
