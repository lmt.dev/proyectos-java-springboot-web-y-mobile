
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para EnviosUnidadEmpaqueCargue complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnviosUnidadEmpaqueCargue">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Num_Alto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Distribuidor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Num_Ancho" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_DiceContener" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_IdArchivoOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Largo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Nom_UnidadEmpaque" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_Peso" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Des_UnidadLongitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Des_UnidadPeso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_UnidadEmpaque" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="Ide_Envio" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="Num_Volumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fec_Actualizacion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Num_Consecutivo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Cod_Facturacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_ValorDeclarado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Indicador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroDeCaja" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Id_archivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviosUnidadEmpaqueCargue", propOrder = {
    "numAlto",
    "numDistribuidor",
    "numAncho",
    "numCantidad",
    "desDiceContener",
    "desIdArchivoOrigen",
    "numLargo",
    "nomUnidadEmpaque",
    "numPeso",
    "desUnidadLongitud",
    "desUnidadPeso",
    "ideUnidadEmpaque",
    "ideEnvio",
    "numVolumen",
    "fecActualizacion",
    "numConsecutivo",
    "codFacturacion",
    "numValorDeclarado",
    "indicador",
    "numeroDeCaja",
    "idArchivo"
})
public class EnviosUnidadEmpaqueCargue {

    @XmlElement(name = "Num_Alto", required = true)
    protected BigDecimal numAlto;
    @XmlElement(name = "Num_Distribuidor")
    protected int numDistribuidor;
    @XmlElement(name = "Num_Ancho", required = true)
    protected BigDecimal numAncho;
    @XmlElement(name = "Num_Cantidad")
    protected int numCantidad;
    @XmlElement(name = "Des_DiceContener")
    protected String desDiceContener;
    @XmlElement(name = "Des_IdArchivoOrigen")
    protected String desIdArchivoOrigen;
    @XmlElement(name = "Num_Largo", required = true)
    protected BigDecimal numLargo;
    @XmlElement(name = "Nom_UnidadEmpaque")
    protected String nomUnidadEmpaque;
    @XmlElement(name = "Num_Peso", required = true)
    protected BigDecimal numPeso;
    @XmlElement(name = "Des_UnidadLongitud")
    protected String desUnidadLongitud;
    @XmlElement(name = "Des_UnidadPeso")
    protected String desUnidadPeso;
    @XmlElement(name = "Ide_UnidadEmpaque", required = true)
    protected String ideUnidadEmpaque;
    @XmlElement(name = "Ide_Envio", required = true)
    protected String ideEnvio;
    @XmlElement(name = "Num_Volumen")
    protected String numVolumen;
    @XmlElement(name = "Fec_Actualizacion", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fecActualizacion;
    @XmlElement(name = "Num_Consecutivo")
    protected int numConsecutivo;
    @XmlElement(name = "Cod_Facturacion")
    protected String codFacturacion;
    @XmlElement(name = "Num_ValorDeclarado")
    protected String numValorDeclarado;
    @XmlElement(name = "Indicador")
    protected String indicador;
    @XmlElement(name = "NumeroDeCaja")
    protected String numeroDeCaja;
    @XmlElement(name = "Id_archivo")
    protected String idArchivo;

    /**
     * Obtiene el valor de la propiedad numAlto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumAlto() {
        return numAlto;
    }

    /**
     * Define el valor de la propiedad numAlto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumAlto(BigDecimal value) {
        this.numAlto = value;
    }

    /**
     * Obtiene el valor de la propiedad numDistribuidor.
     * 
     */
    public int getNumDistribuidor() {
        return numDistribuidor;
    }

    /**
     * Define el valor de la propiedad numDistribuidor.
     * 
     */
    public void setNumDistribuidor(int value) {
        this.numDistribuidor = value;
    }

    /**
     * Obtiene el valor de la propiedad numAncho.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumAncho() {
        return numAncho;
    }

    /**
     * Define el valor de la propiedad numAncho.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumAncho(BigDecimal value) {
        this.numAncho = value;
    }

    /**
     * Obtiene el valor de la propiedad numCantidad.
     * 
     */
    public int getNumCantidad() {
        return numCantidad;
    }

    /**
     * Define el valor de la propiedad numCantidad.
     * 
     */
    public void setNumCantidad(int value) {
        this.numCantidad = value;
    }

    /**
     * Obtiene el valor de la propiedad desDiceContener.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDiceContener() {
        return desDiceContener;
    }

    /**
     * Define el valor de la propiedad desDiceContener.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDiceContener(String value) {
        this.desDiceContener = value;
    }

    /**
     * Obtiene el valor de la propiedad desIdArchivoOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesIdArchivoOrigen() {
        return desIdArchivoOrigen;
    }

    /**
     * Define el valor de la propiedad desIdArchivoOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesIdArchivoOrigen(String value) {
        this.desIdArchivoOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad numLargo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumLargo() {
        return numLargo;
    }

    /**
     * Define el valor de la propiedad numLargo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumLargo(BigDecimal value) {
        this.numLargo = value;
    }

    /**
     * Obtiene el valor de la propiedad nomUnidadEmpaque.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomUnidadEmpaque() {
        return nomUnidadEmpaque;
    }

    /**
     * Define el valor de la propiedad nomUnidadEmpaque.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomUnidadEmpaque(String value) {
        this.nomUnidadEmpaque = value;
    }

    /**
     * Obtiene el valor de la propiedad numPeso.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumPeso() {
        return numPeso;
    }

    /**
     * Define el valor de la propiedad numPeso.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumPeso(BigDecimal value) {
        this.numPeso = value;
    }

    /**
     * Obtiene el valor de la propiedad desUnidadLongitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesUnidadLongitud() {
        return desUnidadLongitud;
    }

    /**
     * Define el valor de la propiedad desUnidadLongitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesUnidadLongitud(String value) {
        this.desUnidadLongitud = value;
    }

    /**
     * Obtiene el valor de la propiedad desUnidadPeso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesUnidadPeso() {
        return desUnidadPeso;
    }

    /**
     * Define el valor de la propiedad desUnidadPeso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesUnidadPeso(String value) {
        this.desUnidadPeso = value;
    }

    /**
     * Obtiene el valor de la propiedad ideUnidadEmpaque.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeUnidadEmpaque() {
        return ideUnidadEmpaque;
    }

    /**
     * Define el valor de la propiedad ideUnidadEmpaque.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeUnidadEmpaque(String value) {
        this.ideUnidadEmpaque = value;
    }

    /**
     * Obtiene el valor de la propiedad ideEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeEnvio() {
        return ideEnvio;
    }

    /**
     * Define el valor de la propiedad ideEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeEnvio(String value) {
        this.ideEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad numVolumen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumVolumen() {
        return numVolumen;
    }

    /**
     * Define el valor de la propiedad numVolumen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumVolumen(String value) {
        this.numVolumen = value;
    }

    /**
     * Obtiene el valor de la propiedad fecActualizacion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecActualizacion() {
        return fecActualizacion;
    }

    /**
     * Define el valor de la propiedad fecActualizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecActualizacion(XMLGregorianCalendar value) {
        this.fecActualizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numConsecutivo.
     * 
     */
    public int getNumConsecutivo() {
        return numConsecutivo;
    }

    /**
     * Define el valor de la propiedad numConsecutivo.
     * 
     */
    public void setNumConsecutivo(int value) {
        this.numConsecutivo = value;
    }

    /**
     * Obtiene el valor de la propiedad codFacturacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFacturacion() {
        return codFacturacion;
    }

    /**
     * Define el valor de la propiedad codFacturacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFacturacion(String value) {
        this.codFacturacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numValorDeclarado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumValorDeclarado() {
        return numValorDeclarado;
    }

    /**
     * Define el valor de la propiedad numValorDeclarado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumValorDeclarado(String value) {
        this.numValorDeclarado = value;
    }

    /**
     * Obtiene el valor de la propiedad indicador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicador() {
        return indicador;
    }

    /**
     * Define el valor de la propiedad indicador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicador(String value) {
        this.indicador = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDeCaja.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDeCaja() {
        return numeroDeCaja;
    }

    /**
     * Define el valor de la propiedad numeroDeCaja.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDeCaja(String value) {
        this.numeroDeCaja = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivo() {
        return idArchivo;
    }

    /**
     * Define el valor de la propiedad idArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivo(String value) {
        this.idArchivo = value;
    }

}
