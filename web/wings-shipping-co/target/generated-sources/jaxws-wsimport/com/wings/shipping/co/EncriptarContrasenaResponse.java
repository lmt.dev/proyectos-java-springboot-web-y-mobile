
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EncriptarContrasenaResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "encriptarContrasenaResult"
})
@XmlRootElement(name = "EncriptarContrasenaResponse")
public class EncriptarContrasenaResponse {

    @XmlElement(name = "EncriptarContrasenaResult")
    protected String encriptarContrasenaResult;

    /**
     * Obtiene el valor de la propiedad encriptarContrasenaResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncriptarContrasenaResult() {
        return encriptarContrasenaResult;
    }

    /**
     * Define el valor de la propiedad encriptarContrasenaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncriptarContrasenaResult(String value) {
        this.encriptarContrasenaResult = value;
    }

}
