
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConsultarGuiasTotalesResultWS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultarGuiasTotalesResultWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id_Tipoguia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="GuiasUtilizadas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NombreTipoGuia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GuiaRangoInicial" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="GuiaRangoFinal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="GuiaActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarGuiasTotalesResultWS", propOrder = {
    "idTipoguia",
    "guiasUtilizadas",
    "nombreTipoGuia",
    "guiaRangoInicial",
    "guiaRangoFinal",
    "guiaActual"
})
public class ConsultarGuiasTotalesResultWS {

    @XmlElement(name = "Id_Tipoguia", required = true, nillable = true)
    protected BigDecimal idTipoguia;
    @XmlElement(name = "GuiasUtilizadas")
    protected String guiasUtilizadas;
    @XmlElement(name = "NombreTipoGuia")
    protected String nombreTipoGuia;
    @XmlElement(name = "GuiaRangoInicial", required = true, nillable = true)
    protected BigDecimal guiaRangoInicial;
    @XmlElement(name = "GuiaRangoFinal", required = true, nillable = true)
    protected BigDecimal guiaRangoFinal;
    @XmlElement(name = "GuiaActual")
    protected String guiaActual;

    /**
     * Obtiene el valor de la propiedad idTipoguia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIdTipoguia() {
        return idTipoguia;
    }

    /**
     * Define el valor de la propiedad idTipoguia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIdTipoguia(BigDecimal value) {
        this.idTipoguia = value;
    }

    /**
     * Obtiene el valor de la propiedad guiasUtilizadas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuiasUtilizadas() {
        return guiasUtilizadas;
    }

    /**
     * Define el valor de la propiedad guiasUtilizadas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuiasUtilizadas(String value) {
        this.guiasUtilizadas = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreTipoGuia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTipoGuia() {
        return nombreTipoGuia;
    }

    /**
     * Define el valor de la propiedad nombreTipoGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTipoGuia(String value) {
        this.nombreTipoGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad guiaRangoInicial.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGuiaRangoInicial() {
        return guiaRangoInicial;
    }

    /**
     * Define el valor de la propiedad guiaRangoInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGuiaRangoInicial(BigDecimal value) {
        this.guiaRangoInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad guiaRangoFinal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGuiaRangoFinal() {
        return guiaRangoFinal;
    }

    /**
     * Define el valor de la propiedad guiaRangoFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGuiaRangoFinal(BigDecimal value) {
        this.guiaRangoFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad guiaActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuiaActual() {
        return guiaActual;
    }

    /**
     * Define el valor de la propiedad guiaActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuiaActual(String value) {
        this.guiaActual = value;
    }

}
