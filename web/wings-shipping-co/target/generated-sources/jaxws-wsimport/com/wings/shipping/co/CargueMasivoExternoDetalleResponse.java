
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CargueMasivoExternoDetalleResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="arrayGuias" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cargueMasivoExternoDetalleResult",
    "arrayGuias"
})
@XmlRootElement(name = "CargueMasivoExternoDetalleResponse")
public class CargueMasivoExternoDetalleResponse {

    @XmlElement(name = "CargueMasivoExternoDetalleResult")
    protected boolean cargueMasivoExternoDetalleResult;
    protected ArrayOfString arrayGuias;

    /**
     * Obtiene el valor de la propiedad cargueMasivoExternoDetalleResult.
     * 
     */
    public boolean isCargueMasivoExternoDetalleResult() {
        return cargueMasivoExternoDetalleResult;
    }

    /**
     * Define el valor de la propiedad cargueMasivoExternoDetalleResult.
     * 
     */
    public void setCargueMasivoExternoDetalleResult(boolean value) {
        this.cargueMasivoExternoDetalleResult = value;
    }

    /**
     * Obtiene el valor de la propiedad arrayGuias.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getArrayGuias() {
        return arrayGuias;
    }

    /**
     * Define el valor de la propiedad arrayGuias.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setArrayGuias(ArrayOfString value) {
        this.arrayGuias = value;
    }

}
