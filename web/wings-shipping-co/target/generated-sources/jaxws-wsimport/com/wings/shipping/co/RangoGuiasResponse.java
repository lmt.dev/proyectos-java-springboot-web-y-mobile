
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RangoGuiasResult" type="{http://tempuri.org/}ArrayOfConsultarGuiasTotalesResultWS" minOccurs="0"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rangoGuiasResult",
    "mensaje"
})
@XmlRootElement(name = "RangoGuiasResponse")
public class RangoGuiasResponse {

    @XmlElement(name = "RangoGuiasResult")
    protected ArrayOfConsultarGuiasTotalesResultWS rangoGuiasResult;
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad rangoGuiasResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfConsultarGuiasTotalesResultWS }
     *     
     */
    public ArrayOfConsultarGuiasTotalesResultWS getRangoGuiasResult() {
        return rangoGuiasResult;
    }

    /**
     * Define el valor de la propiedad rangoGuiasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfConsultarGuiasTotalesResultWS }
     *     
     */
    public void setRangoGuiasResult(ArrayOfConsultarGuiasTotalesResultWS value) {
        this.rangoGuiasResult = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

}
