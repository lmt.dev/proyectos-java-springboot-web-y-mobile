
package com.wings.shipping.co;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarTipoGuiasResult" type="{http://tempuri.org/}ArrayOfConsultarTipoGuiasXCodSer" minOccurs="0"/>
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarTipoGuiasResult",
    "mensaje"
})
@XmlRootElement(name = "ConsultarTipoGuiasResponse")
public class ConsultarTipoGuiasResponse {

    @XmlElement(name = "ConsultarTipoGuiasResult")
    protected ArrayOfConsultarTipoGuiasXCodSer consultarTipoGuiasResult;
    @XmlElement(name = "Mensaje")
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad consultarTipoGuiasResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfConsultarTipoGuiasXCodSer }
     *     
     */
    public ArrayOfConsultarTipoGuiasXCodSer getConsultarTipoGuiasResult() {
        return consultarTipoGuiasResult;
    }

    /**
     * Define el valor de la propiedad consultarTipoGuiasResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfConsultarTipoGuiasXCodSer }
     *     
     */
    public void setConsultarTipoGuiasResult(ArrayOfConsultarTipoGuiasXCodSer value) {
        this.consultarTipoGuiasResult = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

}
