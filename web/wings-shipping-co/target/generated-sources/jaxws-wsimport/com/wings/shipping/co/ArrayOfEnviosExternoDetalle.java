
package com.wings.shipping.co;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfEnviosExternoDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEnviosExternoDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EnviosExternoDetalle" type="{http://tempuri.org/}EnviosExternoDetalle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEnviosExternoDetalle", propOrder = {
    "enviosExternoDetalle"
})
public class ArrayOfEnviosExternoDetalle {

    @XmlElement(name = "EnviosExternoDetalle", nillable = true)
    protected List<EnviosExternoDetalle> enviosExternoDetalle;

    /**
     * Gets the value of the enviosExternoDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enviosExternoDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnviosExternoDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnviosExternoDetalle }
     * 
     * 
     */
    public List<EnviosExternoDetalle> getEnviosExternoDetalle() {
        if (enviosExternoDetalle == null) {
            enviosExternoDetalle = new ArrayList<EnviosExternoDetalle>();
        }
        return this.enviosExternoDetalle;
    }

}
