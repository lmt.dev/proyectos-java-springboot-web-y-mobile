
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "GeneracionGuiasSoap", targetNamespace = "http://tempuri.org/")
@HandlerChain(file = "GeneracionGuiasSoap_handler.xml")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface GeneracionGuiasSoap {


    /**
     * Desencriptar contraseña SISCLINET
     * 
     * @param strcontrasenaEncriptada
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "DesencriptarContrasena", action = "http://tempuri.org/DesencriptarContrasena")
    @WebResult(name = "DesencriptarContrasenaResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "DesencriptarContrasena", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.DesencriptarContrasena")
    @ResponseWrapper(localName = "DesencriptarContrasenaResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.DesencriptarContrasenaResponse")
    public String desencriptarContrasena(
        @WebParam(name = "strcontrasenaEncriptada", targetNamespace = "http://tempuri.org/")
        String strcontrasenaEncriptada);

    /**
     * Encriptar contraseña SISCLINET
     * 
     * @param strcontrasena
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "EncriptarContrasena", action = "http://tempuri.org/EncriptarContrasena")
    @WebResult(name = "EncriptarContrasenaResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "EncriptarContrasena", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.EncriptarContrasena")
    @ResponseWrapper(localName = "EncriptarContrasenaResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.EncriptarContrasenaResponse")
    public String encriptarContrasena(
        @WebParam(name = "strcontrasena", targetNamespace = "http://tempuri.org/")
        String strcontrasena);

    /**
     * Generación de Guías por Envio usado para generación de Envíos en SISCLINET. Ver 20190614. CHG 8926 Impresion
     * 
     * @param cargueMasivoExternoResult
     * @param arrayGuias
     * @param envios
     */
    @WebMethod(operationName = "CargueMasivoExterno", action = "http://tempuri.org/CargueMasivoExterno")
    @RequestWrapper(localName = "CargueMasivoExterno", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CargueMasivoExterno")
    @ResponseWrapper(localName = "CargueMasivoExternoResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CargueMasivoExternoResponse")
    public void cargueMasivoExterno(
        @WebParam(name = "envios", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<ArrayOfCargueMasivoExternoDTO> envios,
        @WebParam(name = "arrayGuias", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<ArrayOfString> arrayGuias,
        @WebParam(name = "CargueMasivoExternoResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> cargueMasivoExternoResult);

    /**
     * Generación de Guías por Envio Detalle usado por el cliente Falabella
     * 
     * @param cargueMasivoExternoDetalleResult
     * @param arrayGuias
     * @param envios
     */
    @WebMethod(operationName = "CargueMasivoExternoDetalle", action = "http://tempuri.org/CargueMasivoExternoDetalle")
    @RequestWrapper(localName = "CargueMasivoExternoDetalle", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CargueMasivoExternoDetalle")
    @ResponseWrapper(localName = "CargueMasivoExternoDetalleResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CargueMasivoExternoDetalleResponse")
    public void cargueMasivoExternoDetalle(
        @WebParam(name = "envios", targetNamespace = "http://tempuri.org/")
        ArrayOfCargueMasivoExternoDetalleDTO envios,
        @WebParam(name = "arrayGuias", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<ArrayOfString> arrayGuias,
        @WebParam(name = "CargueMasivoExternoDetalleResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> cargueMasivoExternoDetalleResult);

    /**
     * Obtener bytes Reporte Guía para Bond o Sticker usada por SISCLINET.
     * 
     * @param ideCodFacturacion
     * @param bytesReport
     * @param interno
     * @param generarGuiaStickerResult
     * @param numGuia
     * @param sFormatoImpresionGuia
     * @param idArchivoCargar
     * @param numGuiaFinal
     */
    @WebMethod(operationName = "GenerarGuiaSticker", action = "http://tempuri.org/GenerarGuiaSticker")
    @RequestWrapper(localName = "GenerarGuiaSticker", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaSticker")
    @ResponseWrapper(localName = "GenerarGuiaStickerResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStickerResponse")
    public void generarGuiaSticker(
        @WebParam(name = "num_Guia", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuia,
        @WebParam(name = "num_GuiaFinal", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuiaFinal,
        @WebParam(name = "ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion,
        @WebParam(name = "sFormatoImpresionGuia", targetNamespace = "http://tempuri.org/")
        int sFormatoImpresionGuia,
        @WebParam(name = "Id_ArchivoCargar", targetNamespace = "http://tempuri.org/")
        String idArchivoCargar,
        @WebParam(name = "interno", targetNamespace = "http://tempuri.org/")
        boolean interno,
        @WebParam(name = "bytesReport", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<byte[]> bytesReport,
        @WebParam(name = "GenerarGuiaStickerResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> generarGuiaStickerResult);

    /**
     * para imprimir por sisclinet mobile
     * 
     * @param generarGuiaStickerMobileResult
     * @param ideCodFacturacion
     * @param bytesReport
     * @param interno
     * @param numGuia
     * @param sFormatoImpresionGuia
     * @param idArchivoCargar
     */
    @WebMethod(operationName = "GenerarGuiaStickerMobile", action = "http://tempuri.org/GenerarGuiaStickerMobile")
    @RequestWrapper(localName = "GenerarGuiaStickerMobile", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStickerMobile")
    @ResponseWrapper(localName = "GenerarGuiaStickerMobileResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStickerMobileResponse")
    public void generarGuiaStickerMobile(
        @WebParam(name = "num_Guia", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuia,
        @WebParam(name = "ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion,
        @WebParam(name = "sFormatoImpresionGuia", targetNamespace = "http://tempuri.org/")
        int sFormatoImpresionGuia,
        @WebParam(name = "Id_ArchivoCargar", targetNamespace = "http://tempuri.org/")
        String idArchivoCargar,
        @WebParam(name = "interno", targetNamespace = "http://tempuri.org/")
        boolean interno,
        @WebParam(name = "bytesReport", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<byte[]> bytesReport,
        @WebParam(name = "GenerarGuiaStickerMobileResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> generarGuiaStickerMobileResult);

    /**
     * Genera Guía Bond o Sticker y se envía por Correo, usada por Tiendas Virtuales
     * 
     * @param consumoClienteExterno
     * @param solicitudRecoleccion
     * @param ideCodFacturacion
     * @param bytesReport
     * @param numRemision
     * @param numGuia
     * @param sFormatoImpresionGuia
     * @param idArchivoCargar
     * @param correoElectronico
     * @param numGuiaFinal
     * @param generarGuiaStickerTiendasVirtualesResult
     */
    @WebMethod(operationName = "GenerarGuiaStickerTiendasVirtuales", action = "http://tempuri.org/GenerarGuiaStickerTiendasVirtuales")
    @RequestWrapper(localName = "GenerarGuiaStickerTiendasVirtuales", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStickerTiendasVirtuales")
    @ResponseWrapper(localName = "GenerarGuiaStickerTiendasVirtualesResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStickerTiendasVirtualesResponse")
    public void generarGuiaStickerTiendasVirtuales(
        @WebParam(name = "num_Guia", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuia,
        @WebParam(name = "num_GuiaFinal", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuiaFinal,
        @WebParam(name = "ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion,
        @WebParam(name = "sFormatoImpresionGuia", targetNamespace = "http://tempuri.org/")
        int sFormatoImpresionGuia,
        @WebParam(name = "Id_ArchivoCargar", targetNamespace = "http://tempuri.org/")
        String idArchivoCargar,
        @WebParam(name = "consumoClienteExterno", targetNamespace = "http://tempuri.org/")
        boolean consumoClienteExterno,
        @WebParam(name = "bytesReport", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<byte[]> bytesReport,
        @WebParam(name = "correoElectronico", targetNamespace = "http://tempuri.org/")
        String correoElectronico,
        @WebParam(name = "solicitudRecoleccion", targetNamespace = "http://tempuri.org/")
        String solicitudRecoleccion,
        @WebParam(name = "Num_Remision", targetNamespace = "http://tempuri.org/")
        String numRemision,
        @WebParam(name = "GenerarGuiaStickerTiendasVirtualesResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> generarGuiaStickerTiendasVirtualesResult);

    /**
     * Para unidades de empaque por código ser
     * 
     * @return
     *     returns com.wings.shipping.co.ConsultarUnidadEmpaqueIdLoginResponse.ConsultarUnidadEmpaqueIdLoginResult
     */
    @WebMethod(operationName = "ConsultarUnidadEmpaqueIdLogin", action = "http://tempuri.org/ConsultarUnidadEmpaqueIdLogin")
    @WebResult(name = "ConsultarUnidadEmpaqueIdLoginResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ConsultarUnidadEmpaqueIdLogin", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarUnidadEmpaqueIdLogin")
    @ResponseWrapper(localName = "ConsultarUnidadEmpaqueIdLoginResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarUnidadEmpaqueIdLoginResponse")
    public com.wings.shipping.co.ConsultarUnidadEmpaqueIdLoginResponse.ConsultarUnidadEmpaqueIdLoginResult consultarUnidadEmpaqueIdLogin();

    /**
     * Para consultar restricciones fisicas de los envíos de mercancia industrial por codigo Ser
     * 
     * @return
     *     returns com.wings.shipping.co.ConsultarRestriccionesFisicasEnviosResponse.ConsultarRestriccionesFisicasEnviosResult
     */
    @WebMethod(operationName = "ConsultarRestriccionesFisicasEnvios", action = "http://tempuri.org/ConsultarRestriccionesFisicasEnvios")
    @WebResult(name = "ConsultarRestriccionesFisicasEnviosResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ConsultarRestriccionesFisicasEnvios", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarRestriccionesFisicasEnvios")
    @ResponseWrapper(localName = "ConsultarRestriccionesFisicasEnviosResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarRestriccionesFisicasEnviosResponse")
    public com.wings.shipping.co.ConsultarRestriccionesFisicasEnviosResponse.ConsultarRestriccionesFisicasEnviosResult consultarRestriccionesFisicasEnvios();

    /**
     * Consultar por numero número documento
     * 
     * @param numeroGuia
     * @param ideCodFacturacion
     * @return
     *     returns com.wings.shipping.co.ConsultarGuiasByNumDocumentoResponse.ConsultarGuiasByNumDocumentoResult
     */
    @WebMethod(operationName = "ConsultarGuiasByNumDocumento", action = "http://tempuri.org/ConsultarGuiasByNumDocumento")
    @WebResult(name = "ConsultarGuiasByNumDocumentoResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ConsultarGuiasByNumDocumento", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarGuiasByNumDocumento")
    @ResponseWrapper(localName = "ConsultarGuiasByNumDocumentoResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarGuiasByNumDocumentoResponse")
    public com.wings.shipping.co.ConsultarGuiasByNumDocumentoResponse.ConsultarGuiasByNumDocumentoResult consultarGuiasByNumDocumento(
        @WebParam(name = "numeroGuia", targetNamespace = "http://tempuri.org/")
        String numeroGuia,
        @WebParam(name = "Ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion);

    /**
     * procesa (inserta, actualiza o borra las piezas de un envío de indutrial)
     * 
     * @param documento
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "InsertarSertEnvio", action = "http://tempuri.org/InsertarSertEnvio")
    @WebResult(name = "InsertarSertEnvioResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "InsertarSertEnvio", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.InsertarSertEnvio")
    @ResponseWrapper(localName = "InsertarSertEnvioResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.InsertarSertEnvioResponse")
    public boolean insertarSertEnvio(
        @WebParam(name = "documento", targetNamespace = "http://tempuri.org/")
        com.wings.shipping.co.InsertarSertEnvio.Documento documento);

    /**
     * procesa (inserta, actualiza o borra las piezas de un envío de indutrial)
     * 
     * @param documento
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "ActualizarSertEnvio", action = "http://tempuri.org/ActualizarSertEnvio")
    @WebResult(name = "ActualizarSertEnvioResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ActualizarSertEnvio", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ActualizarSertEnvio")
    @ResponseWrapper(localName = "ActualizarSertEnvioResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ActualizarSertEnvioResponse")
    public boolean actualizarSertEnvio(
        @WebParam(name = "documento", targetNamespace = "http://tempuri.org/")
        com.wings.shipping.co.ActualizarSertEnvio.Documento documento);

    /**
     * procesa (inserta, actualiza o borra las piezas de un envío de indutrial)
     * 
     * @param documento
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "ActualizarEnvioAgregarUnidades", action = "http://tempuri.org/ActualizarEnvioAgregarUnidades")
    @WebResult(name = "ActualizarEnvioAgregarUnidadesResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ActualizarEnvioAgregarUnidades", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ActualizarEnvioAgregarUnidades")
    @ResponseWrapper(localName = "ActualizarEnvioAgregarUnidadesResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ActualizarEnvioAgregarUnidadesResponse")
    public boolean actualizarEnvioAgregarUnidades(
        @WebParam(name = "documento", targetNamespace = "http://tempuri.org/")
        com.wings.shipping.co.ActualizarEnvioAgregarUnidades.Documento documento);

    /**
     * procesa (inserta, actualiza o borra las piezas de un envío de indutrial)
     * 
     * @param documento
     * @return
     *     returns com.wings.shipping.co.ValidarActualizarEnvioResponse.ValidarActualizarEnvioResult
     */
    @WebMethod(operationName = "ValidarActualizarEnvio", action = "http://tempuri.org/ValidarActualizarEnvio")
    @WebResult(name = "ValidarActualizarEnvioResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ValidarActualizarEnvio", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ValidarActualizarEnvio")
    @ResponseWrapper(localName = "ValidarActualizarEnvioResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ValidarActualizarEnvioResponse")
    public com.wings.shipping.co.ValidarActualizarEnvioResponse.ValidarActualizarEnvioResult validarActualizarEnvio(
        @WebParam(name = "documento", targetNamespace = "http://tempuri.org/")
        com.wings.shipping.co.ValidarActualizarEnvio.Documento documento);

    /**
     * procesa (inserta, actualiza o borra las piezas de un envío de indutrial)
     * 
     * @param ideCodFacturacion
     * @param ideEnvio
     * @return
     *     returns com.wings.shipping.co.CrearUnidadesEmpaqueBlancoResponse.CrearUnidadesEmpaqueBlancoResult
     */
    @WebMethod(operationName = "CrearUnidadesEmpaqueBlanco", action = "http://tempuri.org/CrearUnidadesEmpaqueBlanco")
    @WebResult(name = "CrearUnidadesEmpaqueBlancoResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "CrearUnidadesEmpaqueBlanco", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CrearUnidadesEmpaqueBlanco")
    @ResponseWrapper(localName = "CrearUnidadesEmpaqueBlancoResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.CrearUnidadesEmpaqueBlancoResponse")
    public com.wings.shipping.co.CrearUnidadesEmpaqueBlancoResponse.CrearUnidadesEmpaqueBlancoResult crearUnidadesEmpaqueBlanco(
        @WebParam(name = "ideEnvio", targetNamespace = "http://tempuri.org/")
        String ideEnvio,
        @WebParam(name = "Ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion);

    /**
     * Obtener bytes Reporte Guía
     * 
     * @param ideCodFacturacion
     * @param desTipoGuia
     * @param interno
     * @param idArchivoCargar
     * @param numGuiaArreglo
     * @return
     *     returns com.wings.shipping.co.GenerarGuiaStikerDataResponse.GenerarGuiaStikerDataResult
     */
    @WebMethod(operationName = "GenerarGuiaStikerData", action = "http://tempuri.org/GenerarGuiaStikerData")
    @WebResult(name = "GenerarGuiaStikerDataResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GenerarGuiaStikerData", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStikerData")
    @ResponseWrapper(localName = "GenerarGuiaStikerDataResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStikerDataResponse")
    public com.wings.shipping.co.GenerarGuiaStikerDataResponse.GenerarGuiaStikerDataResult generarGuiaStikerData(
        @WebParam(name = "num_GuiaArreglo", targetNamespace = "http://tempuri.org/")
        ArrayOfLong numGuiaArreglo,
        @WebParam(name = "ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion,
        @WebParam(name = "des_TipoGuia", targetNamespace = "http://tempuri.org/")
        int desTipoGuia,
        @WebParam(name = "Id_ArchivoCargar", targetNamespace = "http://tempuri.org/")
        String idArchivoCargar,
        @WebParam(name = "interno", targetNamespace = "http://tempuri.org/")
        boolean interno);

    /**
     * Obtener bytes Reporte Guía
     * 
     * @param ideCodFacturacion
     * @param desTipoGuia
     * @param interno
     * @param idArchivoCargar
     * @param numGuiaArreglo
     * @return
     *     returns com.wings.shipping.co.GenerarGuiaStikerDataReimpresionResponse.GenerarGuiaStikerDataReimpresionResult
     */
    @WebMethod(operationName = "GenerarGuiaStikerDataReimpresion", action = "http://tempuri.org/GenerarGuiaStikerDataReimpresion")
    @WebResult(name = "GenerarGuiaStikerDataReimpresionResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GenerarGuiaStikerDataReimpresion", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStikerDataReimpresion")
    @ResponseWrapper(localName = "GenerarGuiaStikerDataReimpresionResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarGuiaStikerDataReimpresionResponse")
    public com.wings.shipping.co.GenerarGuiaStikerDataReimpresionResponse.GenerarGuiaStikerDataReimpresionResult generarGuiaStikerDataReimpresion(
        @WebParam(name = "num_GuiaArreglo", targetNamespace = "http://tempuri.org/")
        ArrayOfLong numGuiaArreglo,
        @WebParam(name = "ide_CodFacturacion", targetNamespace = "http://tempuri.org/")
        String ideCodFacturacion,
        @WebParam(name = "des_TipoGuia", targetNamespace = "http://tempuri.org/")
        int desTipoGuia,
        @WebParam(name = "Id_ArchivoCargar", targetNamespace = "http://tempuri.org/")
        String idArchivoCargar,
        @WebParam(name = "interno", targetNamespace = "http://tempuri.org/")
        boolean interno);

    /**
     * Se consulta si la guia se puede reimprimir desde sisclinet mobile
     * 
     * @param numeroGuia
     * @return
     *     returns boolean
     */
    @WebMethod(operationName = "ConsultarGuiaReimpresion", action = "http://tempuri.org/ConsultarGuiaReimpresion")
    @WebResult(name = "ConsultarGuiaReimpresionResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "ConsultarGuiaReimpresion", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarGuiaReimpresion")
    @ResponseWrapper(localName = "ConsultarGuiaReimpresionResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarGuiaReimpresionResponse")
    public boolean consultarGuiaReimpresion(
        @WebParam(name = "numeroGuia", targetNamespace = "http://tempuri.org/")
        BigDecimal numeroGuia);

    /**
     * Anular las guias de un cliente en SISCLINET.
     * 
     * @param anularGuiasResult
     * @param interno
     * @param numGuia
     * @param numGuiaFinal
     */
    @WebMethod(operationName = "AnularGuias", action = "http://tempuri.org/AnularGuias")
    @RequestWrapper(localName = "AnularGuias", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.AnularGuias")
    @ResponseWrapper(localName = "AnularGuiasResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.AnularGuiasResponse")
    public void anularGuias(
        @WebParam(name = "num_Guia", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuia,
        @WebParam(name = "num_GuiaFinal", targetNamespace = "http://tempuri.org/")
        BigDecimal numGuiaFinal,
        @WebParam(name = "interno", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<ArrayOfResultadoAnulacionGuias> interno,
        @WebParam(name = "AnularGuiasResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<String> anularGuiasResult);

    /**
     * Consultar el rango de guias asignadas a un cliente en SISCLINET
     * 
     * @param rangoGuiasResult
     * @param tipoGuia
     * @param mensaje
     */
    @WebMethod(operationName = "RangoGuias", action = "http://tempuri.org/RangoGuias")
    @RequestWrapper(localName = "RangoGuias", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.RangoGuias")
    @ResponseWrapper(localName = "RangoGuiasResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.RangoGuiasResponse")
    public void rangoGuias(
        @WebParam(name = "Tipo_Guia", targetNamespace = "http://tempuri.org/")
        int tipoGuia,
        @WebParam(name = "mensaje", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> mensaje,
        @WebParam(name = "RangoGuiasResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<ArrayOfConsultarGuiasTotalesResultWS> rangoGuiasResult);

    /**
     * Operación que permite relacionar los números de guías de los envíos que se despacharan de las instalaciones del cliente así como también los datos del vehículo en que se transportaran los envíos y datos del Courier que recibe los paquetes por parte del cliente generando un manifiesto tanto físico como virtual para control de la operación. Lo anterior permitirá a los Centros Logísticos de SERVIENTREGA la información de todos los envíos que se despacharon de las instalaciones del cliente para preparar la operación.
     * 
     * @param placaVehiculo
     * @param omitirEstadoImpresoGuia
     * @param listaGuiasXml
     * @param numManifiesto
     * @param generarManifiestoResult
     * @param nombreAuxiliar
     * @param mensaje
     * @param cadenaBytes
     * @param desError
     * @param nombreCurrier
     * @param ideCurrier
     * @param ideAuxiliar
     */
    @WebMethod(operationName = "GenerarManifiesto", action = "http://tempuri.org/GenerarManifiesto")
    @RequestWrapper(localName = "GenerarManifiesto", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarManifiesto")
    @ResponseWrapper(localName = "GenerarManifiestoResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GenerarManifiestoResponse")
    public void generarManifiesto(
        @WebParam(name = "Omitir_EstadoImpreso_Guia", targetNamespace = "http://tempuri.org/")
        short omitirEstadoImpresoGuia,
        @WebParam(name = "Ide_Currier", targetNamespace = "http://tempuri.org/")
        String ideCurrier,
        @WebParam(name = "Nombre_Currier", targetNamespace = "http://tempuri.org/")
        String nombreCurrier,
        @WebParam(name = "Ide_Auxiliar", targetNamespace = "http://tempuri.org/")
        String ideAuxiliar,
        @WebParam(name = "Nombre_Auxiliar", targetNamespace = "http://tempuri.org/")
        String nombreAuxiliar,
        @WebParam(name = "Placa_Vehiculo", targetNamespace = "http://tempuri.org/")
        String placaVehiculo,
        @WebParam(name = "Lista_Guias_Xml", targetNamespace = "http://tempuri.org/")
        com.wings.shipping.co.GenerarManifiesto.ListaGuiasXml listaGuiasXml,
        @WebParam(name = "cadenaBytes", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<byte[]> cadenaBytes,
        @WebParam(name = "Des_Error", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<ArrayOfErrorGeneradoPorGuia> desError,
        @WebParam(name = "Num_manifiesto", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> numManifiesto,
        @WebParam(name = "Mensaje", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> mensaje,
        @WebParam(name = "GenerarManifiestoResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<Boolean> generarManifiestoResult);

    /**
     * Operación que permite consultar los tipos de guías que tiene provisionados el cliente informándole cuales aplican a los productos que tiene parametrizados manejar con SERVIENTREGA S.A
     * 
     * @param consultarTipoGuiasResult
     * @param mensaje
     */
    @WebMethod(operationName = "ConsultarTipoGuias", action = "http://tempuri.org/ConsultarTipoGuias")
    @RequestWrapper(localName = "ConsultarTipoGuias", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarTipoGuias")
    @ResponseWrapper(localName = "ConsultarTipoGuiasResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultarTipoGuiasResponse")
    public void consultarTipoGuias(
        @WebParam(name = "Mensaje", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> mensaje,
        @WebParam(name = "ConsultarTipoGuiasResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<ArrayOfConsultarTipoGuiasXCodSer> consultarTipoGuiasResult);

    /**
     * Operación que permite consultar por cliente y código de facturación asignados por SERVIENTREGA S.A los rangos de guías provisionados por cada uno de los tipos de guías asignados y producto, así como la cantidad de guías disponibles de ese rango, el número de guía actual usada de ese rango, rango inicial y final.
     * 
     * @param idTrazaAuditoria
     * @param usuarioTrazaAuditoria
     * @param mensaje
     * @param consultaRangosGuiasResult
     * @param urlTrazaAuditoria
     */
    @WebMethod(operationName = "ConsultaRangosGuias", action = "http://tempuri.org/ConsultaRangosGuias")
    @RequestWrapper(localName = "ConsultaRangosGuias", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultaRangosGuias")
    @ResponseWrapper(localName = "ConsultaRangosGuiasResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.ConsultaRangosGuiasResponse")
    public void consultaRangosGuias(
        @WebParam(name = "Mensaje", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> mensaje,
        @WebParam(name = "idTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String idTrazaAuditoria,
        @WebParam(name = "usuarioTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String usuarioTrazaAuditoria,
        @WebParam(name = "urlTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String urlTrazaAuditoria,
        @WebParam(name = "ConsultaRangosGuiasResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<ArrayOfConsultarGuiasTotalesResultWS> consultaRangosGuiasResult);

    /**
     * Operación que permite consultar por un rango de fechas las guías que no se encuentran manifestadas esto quiere decir no se han despachado de las instalaciones del cliente.
     * 
     * @param fechaInicial
     * @param idTrazaAuditoria
     * @param usuarioTrazaAuditoria
     * @param fechaFinal
     * @param guiasPendientesXManifestarResult
     * @param mensaje
     * @param urlTrazaAuditoria
     */
    @WebMethod(operationName = "GuiasPendientesXManifestar", action = "http://tempuri.org/GuiasPendientesXManifestar")
    @RequestWrapper(localName = "GuiasPendientesXManifestar", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GuiasPendientesXManifestar")
    @ResponseWrapper(localName = "GuiasPendientesXManifestarResponse", targetNamespace = "http://tempuri.org/", className = "com.wings.shipping.co.GuiasPendientesXManifestarResponse")
    public void guiasPendientesXManifestar(
        @WebParam(name = "FechaInicial", targetNamespace = "http://tempuri.org/")
        String fechaInicial,
        @WebParam(name = "FechaFinal", targetNamespace = "http://tempuri.org/")
        String fechaFinal,
        @WebParam(name = "mensaje", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.INOUT)
        Holder<String> mensaje,
        @WebParam(name = "idTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String idTrazaAuditoria,
        @WebParam(name = "usuarioTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String usuarioTrazaAuditoria,
        @WebParam(name = "urlTrazaAuditoria", targetNamespace = "http://tempuri.org/")
        String urlTrazaAuditoria,
        @WebParam(name = "GuiasPendientesXManifestarResult", targetNamespace = "http://tempuri.org/", mode = WebParam.Mode.OUT)
        Holder<ArrayOfConsultaGuiasXManifestar> guiasPendientesXManifestarResult);

}
