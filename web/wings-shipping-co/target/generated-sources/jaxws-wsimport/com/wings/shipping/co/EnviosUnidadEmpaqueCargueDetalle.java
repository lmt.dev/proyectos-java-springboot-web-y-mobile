
package com.wings.shipping.co;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EnviosUnidadEmpaqueCargueDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnviosUnidadEmpaqueCargueDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Num_Alto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Ancho" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Largo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Peso" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Num_Cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Des_DiceContener" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nom_UnidadEmpaque" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Num_ValorDeclarado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviosUnidadEmpaqueCargueDetalle", propOrder = {
    "numAlto",
    "numAncho",
    "numLargo",
    "numPeso",
    "numCantidad",
    "desDiceContener",
    "nomUnidadEmpaque",
    "numValorDeclarado"
})
public class EnviosUnidadEmpaqueCargueDetalle {

    @XmlElement(name = "Num_Alto", required = true)
    protected BigDecimal numAlto;
    @XmlElement(name = "Num_Ancho", required = true)
    protected BigDecimal numAncho;
    @XmlElement(name = "Num_Largo", required = true)
    protected BigDecimal numLargo;
    @XmlElement(name = "Num_Peso", required = true)
    protected BigDecimal numPeso;
    @XmlElement(name = "Num_Cantidad")
    protected int numCantidad;
    @XmlElement(name = "Des_DiceContener")
    protected String desDiceContener;
    @XmlElement(name = "Nom_UnidadEmpaque")
    protected String nomUnidadEmpaque;
    @XmlElement(name = "Num_ValorDeclarado")
    protected String numValorDeclarado;

    /**
     * Obtiene el valor de la propiedad numAlto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumAlto() {
        return numAlto;
    }

    /**
     * Define el valor de la propiedad numAlto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumAlto(BigDecimal value) {
        this.numAlto = value;
    }

    /**
     * Obtiene el valor de la propiedad numAncho.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumAncho() {
        return numAncho;
    }

    /**
     * Define el valor de la propiedad numAncho.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumAncho(BigDecimal value) {
        this.numAncho = value;
    }

    /**
     * Obtiene el valor de la propiedad numLargo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumLargo() {
        return numLargo;
    }

    /**
     * Define el valor de la propiedad numLargo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumLargo(BigDecimal value) {
        this.numLargo = value;
    }

    /**
     * Obtiene el valor de la propiedad numPeso.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumPeso() {
        return numPeso;
    }

    /**
     * Define el valor de la propiedad numPeso.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumPeso(BigDecimal value) {
        this.numPeso = value;
    }

    /**
     * Obtiene el valor de la propiedad numCantidad.
     * 
     */
    public int getNumCantidad() {
        return numCantidad;
    }

    /**
     * Define el valor de la propiedad numCantidad.
     * 
     */
    public void setNumCantidad(int value) {
        this.numCantidad = value;
    }

    /**
     * Obtiene el valor de la propiedad desDiceContener.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesDiceContener() {
        return desDiceContener;
    }

    /**
     * Define el valor de la propiedad desDiceContener.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesDiceContener(String value) {
        this.desDiceContener = value;
    }

    /**
     * Obtiene el valor de la propiedad nomUnidadEmpaque.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomUnidadEmpaque() {
        return nomUnidadEmpaque;
    }

    /**
     * Define el valor de la propiedad nomUnidadEmpaque.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomUnidadEmpaque(String value) {
        this.nomUnidadEmpaque = value;
    }

    /**
     * Obtiene el valor de la propiedad numValorDeclarado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumValorDeclarado() {
        return numValorDeclarado;
    }

    /**
     * Define el valor de la propiedad numValorDeclarado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumValorDeclarado(String value) {
        this.numValorDeclarado = value;
    }

}
