
package com.wings.shipping.co;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfEnviosUnidadEmpaqueCargueDetalle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEnviosUnidadEmpaqueCargueDetalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EnviosUnidadEmpaqueCargueDetalle" type="{http://tempuri.org/}EnviosUnidadEmpaqueCargueDetalle" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEnviosUnidadEmpaqueCargueDetalle", propOrder = {
    "enviosUnidadEmpaqueCargueDetalle"
})
public class ArrayOfEnviosUnidadEmpaqueCargueDetalle {

    @XmlElement(name = "EnviosUnidadEmpaqueCargueDetalle", nillable = true)
    protected List<EnviosUnidadEmpaqueCargueDetalle> enviosUnidadEmpaqueCargueDetalle;

    /**
     * Gets the value of the enviosUnidadEmpaqueCargueDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enviosUnidadEmpaqueCargueDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnviosUnidadEmpaqueCargueDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnviosUnidadEmpaqueCargueDetalle }
     * 
     * 
     */
    public List<EnviosUnidadEmpaqueCargueDetalle> getEnviosUnidadEmpaqueCargueDetalle() {
        if (enviosUnidadEmpaqueCargueDetalle == null) {
            enviosUnidadEmpaqueCargueDetalle = new ArrayList<EnviosUnidadEmpaqueCargueDetalle>();
        }
        return this.enviosUnidadEmpaqueCargueDetalle;
    }

}
