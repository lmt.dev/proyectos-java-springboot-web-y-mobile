
package com.wings.shipping.co;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Omitir_EstadoImpreso_Guia" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="Ide_Currier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nombre_Currier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ide_Auxiliar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nombre_Auxiliar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Placa_Vehiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Lista_Guias_Xml" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cadenaBytes" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Des_Error" type="{http://tempuri.org/}ArrayOfErrorGeneradoPorGuia" minOccurs="0"/>
 *         &lt;element name="Num_manifiesto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "omitirEstadoImpresoGuia",
    "ideCurrier",
    "nombreCurrier",
    "ideAuxiliar",
    "nombreAuxiliar",
    "placaVehiculo",
    "listaGuiasXml",
    "cadenaBytes",
    "desError",
    "numManifiesto",
    "mensaje"
})
@XmlRootElement(name = "GenerarManifiesto")
public class GenerarManifiesto {

    @XmlElement(name = "Omitir_EstadoImpreso_Guia")
    @XmlSchemaType(name = "unsignedByte")
    protected short omitirEstadoImpresoGuia;
    @XmlElement(name = "Ide_Currier")
    protected String ideCurrier;
    @XmlElement(name = "Nombre_Currier")
    protected String nombreCurrier;
    @XmlElement(name = "Ide_Auxiliar")
    protected String ideAuxiliar;
    @XmlElement(name = "Nombre_Auxiliar")
    protected String nombreAuxiliar;
    @XmlElement(name = "Placa_Vehiculo")
    protected String placaVehiculo;
    @XmlElement(name = "Lista_Guias_Xml")
    protected GenerarManifiesto.ListaGuiasXml listaGuiasXml;
    protected byte[] cadenaBytes;
    @XmlElement(name = "Des_Error")
    protected ArrayOfErrorGeneradoPorGuia desError;
    @XmlElement(name = "Num_manifiesto")
    protected String numManifiesto;
    @XmlElement(name = "Mensaje")
    protected String mensaje;

    /**
     * Obtiene el valor de la propiedad omitirEstadoImpresoGuia.
     * 
     */
    public short getOmitirEstadoImpresoGuia() {
        return omitirEstadoImpresoGuia;
    }

    /**
     * Define el valor de la propiedad omitirEstadoImpresoGuia.
     * 
     */
    public void setOmitirEstadoImpresoGuia(short value) {
        this.omitirEstadoImpresoGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad ideCurrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeCurrier() {
        return ideCurrier;
    }

    /**
     * Define el valor de la propiedad ideCurrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeCurrier(String value) {
        this.ideCurrier = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCurrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCurrier() {
        return nombreCurrier;
    }

    /**
     * Define el valor de la propiedad nombreCurrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCurrier(String value) {
        this.nombreCurrier = value;
    }

    /**
     * Obtiene el valor de la propiedad ideAuxiliar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdeAuxiliar() {
        return ideAuxiliar;
    }

    /**
     * Define el valor de la propiedad ideAuxiliar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdeAuxiliar(String value) {
        this.ideAuxiliar = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreAuxiliar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAuxiliar() {
        return nombreAuxiliar;
    }

    /**
     * Define el valor de la propiedad nombreAuxiliar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAuxiliar(String value) {
        this.nombreAuxiliar = value;
    }

    /**
     * Obtiene el valor de la propiedad placaVehiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    /**
     * Define el valor de la propiedad placaVehiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlacaVehiculo(String value) {
        this.placaVehiculo = value;
    }

    /**
     * Obtiene el valor de la propiedad listaGuiasXml.
     * 
     * @return
     *     possible object is
     *     {@link GenerarManifiesto.ListaGuiasXml }
     *     
     */
    public GenerarManifiesto.ListaGuiasXml getListaGuiasXml() {
        return listaGuiasXml;
    }

    /**
     * Define el valor de la propiedad listaGuiasXml.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerarManifiesto.ListaGuiasXml }
     *     
     */
    public void setListaGuiasXml(GenerarManifiesto.ListaGuiasXml value) {
        this.listaGuiasXml = value;
    }

    /**
     * Obtiene el valor de la propiedad cadenaBytes.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCadenaBytes() {
        return cadenaBytes;
    }

    /**
     * Define el valor de la propiedad cadenaBytes.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCadenaBytes(byte[] value) {
        this.cadenaBytes = value;
    }

    /**
     * Obtiene el valor de la propiedad desError.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorGeneradoPorGuia }
     *     
     */
    public ArrayOfErrorGeneradoPorGuia getDesError() {
        return desError;
    }

    /**
     * Define el valor de la propiedad desError.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorGeneradoPorGuia }
     *     
     */
    public void setDesError(ArrayOfErrorGeneradoPorGuia value) {
        this.desError = value;
    }

    /**
     * Obtiene el valor de la propiedad numManifiesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumManifiesto() {
        return numManifiesto;
    }

    /**
     * Define el valor de la propiedad numManifiesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumManifiesto(String value) {
        this.numManifiesto = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "content"
    })
    public static class ListaGuiasXml {

        @XmlMixed
        @XmlAnyElement(lax = true)
        protected List<Object> content;

        /**
         * Gets the value of the content property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * {@link Object }
         * 
         * 
         */
        public List<Object> getContent() {
            if (content == null) {
                content = new ArrayList<Object>();
            }
            return this.content;
        }

    }

}
