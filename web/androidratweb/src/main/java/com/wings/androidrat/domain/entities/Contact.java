/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.androidrat.domain.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "contact")
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "times_contacted")
    private int timesContacted;
    
    @Column(name = "last_time_contacted")
    private int lastTimeContacted;
    
    @Column(name = "display_name")
    private String displayName;
    
    @Column(name = "starred")
    private int starred;
    
    @Column(name = "phones")
    private ArrayList<String> phones;
    
    @Column(name = "emails")
    private ArrayList<String> emails;
    
    @Column(name = "notes")
    private ArrayList<String> notes;
    
    @Column(name = "street")
    private String street;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "region")
    private String region;
    
    @Column(name = "region")
    private String postalcode;

    @Column(name = "country")
    private String country;
    
    @Column(name = "type_addr")
    private String typeAddr;
    
    
    
	String country;
	int type_addr;
	ArrayList<String> messaging;
	String OrganisationName;
	String OrganisationStatus; //manager ..
	byte[] photo;
}
