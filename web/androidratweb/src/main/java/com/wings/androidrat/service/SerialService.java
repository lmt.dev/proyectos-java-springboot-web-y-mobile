/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.androidrat.service;

import com.wings.androidrat.domain.entities.KeyCode;
import com.wings.androidrat.domain.entities.KeyLicense;
import com.wings.androidrat.domain.repo.KeyCodeRepository;
import com.wings.androidrat.domain.repo.KeyLicenseRepository;
import com.wings.androidrat.domain.repo.WingsSerialRepository;
import java.math.BigInteger;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author seba
 */
@Service
public class SerialService {

    @Autowired
    private KeyCodeRepository keyCodeRepository;
    @Autowired
    private KeyLicenseRepository keyLicenseRepository;
    @Autowired
    private WingsSerialRepository wingsSerialRepository;

    private static final int TOP_LICENSE_PER_KEY = 20;

    /**
     * Return null if serial does not exists, else serial
     *
     * @param pinCode
     * @return
     */
    public String getSerialByCode(String pinCode) {

        String key = null;
        KeyCode code = keyCodeRepository.findOneByPinCode(pinCode);
        if (code == null) {
            return null;
        }

        if (code.getActive()) {
            Long licenseId = code.getLicenseId();
            KeyLicense license = getLicences(licenseId);
            key = license.getLicense();
        } else {

            //create new one
            KeyLicense license = getLicences(null);
            if (license == null) {
                return null;
            }

            code.setActivationDate(new Date());
            code.setActive(true);
            code.setLicenseId(license.getId());

            keyCodeRepository.save(code);
            license.setUsage(license.getUsage() + 1);
            keyLicenseRepository.save(license);

            key = license.getLicense();
        }

        return key;
    }

    /**
     * Return license with ID or return new one available if is is null
     *
     * @param licenseId
     * @return
     */
    private KeyLicense getLicences(Long licenseId) {
        KeyLicense license;
            if (licenseId == null) {
            license = keyLicenseRepository.findFirstByUsageLessThan(TOP_LICENSE_PER_KEY);
        } else {
            license = keyLicenseRepository.getOne(licenseId);
        }
        return license;
    }

}
