
package com.wings.api.models;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class CallLogPacketDto {

	private ArrayList<CallPacketDto> list;

	public CallLogPacketDto() {
            
	}
	
	public CallLogPacketDto(ArrayList<CallPacketDto> ar) {
		list = ar;
	}
	
	public byte[] build() {
            try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(bos);
                    out.writeObject(list);
                    return bos.toByteArray();
            } catch (IOException e) {
                    return null;
            }
	}

	public void parse(byte[] packet) {
            ByteArrayInputStream bis = new ByteArrayInputStream(packet);
            ObjectInputStream in;
            try {
                    in = new ObjectInputStream(bis);
                    list = (ArrayList<CallPacketDto>) in.readObject();
            } catch (Exception e) {
            }
	}
	
	public ArrayList<CallPacketDto> getList() {
		return list;
	}
}