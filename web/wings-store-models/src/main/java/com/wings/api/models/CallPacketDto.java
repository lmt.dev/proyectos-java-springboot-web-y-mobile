
package com.wings.api.models;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */

public class CallPacketDto {

private static final long serialVersionUID = 3972539952673409279L;
	
    private int id;
    private int type;
    private long date;
    private long duration;
    private int contact_id;
    private int phoneNumberSize;
    private String phoneNumber;
    private int nameSize;
    private String name;
	
//    public CallPacket() {
//    }
//
//    public CallPacket(int id, int type, long date, long duration, int contact_id, String number, String name) {
//            this.id = id;
//            this.type = type;
//            this.date = date;
//            this.duration = duration;
//            this.contact_id = contact_id;
//            this.phoneNumber = number;
//            if(phoneNumber != null)
//                    this.phoneNumberSize = number.length();
//            else
//                    this.phoneNumberSize = 0;
//            this.name = name;
//            if(name != null)
//                    this.nameSize = name.length();
//            else
//                    this.nameSize = 0;
//    }

    public byte[] build() {
            ByteBuffer b = ByteBuffer.allocate(4*5+8*2+getPhoneNumberSize()+getNameSize());
            b.putInt(getId());
            b.putInt(getType());
            b.putLong(getDate());
            b.putLong(getDuration());
            b.putInt(getContact_id());
            b.putInt(getPhoneNumberSize());
            b.put(getPhoneNumber().getBytes());
            b.putInt(getNameSize());
            b.put(getName().getBytes());
            return b.array();
    }

    public void parse(byte[] packet) {
            ByteBuffer b = ByteBuffer.wrap(packet);
            this.setId(b.getInt());
            this.setType(b.getInt());
            this.setDate(b.getLong());
            this.setDuration(b.getLong());
            this.setContact_id(b.getInt());
            this.setPhoneNumberSize(b.getInt());
            byte[] tmp = new byte[getPhoneNumberSize()];
            b.get(tmp);
            this.setPhoneNumber(new String(tmp));
            this.setNameSize(b.getInt());
            tmp = new byte[getNameSize()];
            b.get(tmp);
            this.setName(new String(tmp));
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the date
     */
    public long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(long date) {
        this.date = date;
    }

    /**
     * @return the duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * @return the contact_id
     */
    public int getContact_id() {
        return contact_id;
    }

    /**
     * @param contact_id the contact_id to set
     */
    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    /**
     * @return the phoneNumberSize
     */
    public int getPhoneNumberSize() {
        return phoneNumberSize;
    }

    /**
     * @param phoneNumberSize the phoneNumberSize to set
     */
    public void setPhoneNumberSize(int phoneNumberSize) {
        this.phoneNumberSize = phoneNumberSize;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the nameSize
     */
    public int getNameSize() {
        return nameSize;
    }

    /**
     * @param nameSize the nameSize to set
     */
    public void setNameSize(int nameSize) {
        this.nameSize = nameSize;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
