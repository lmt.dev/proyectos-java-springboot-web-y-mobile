
package com.wings.api.models;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class GPSPacketDto {

    private double longitude;
    private double latitude;
    private double altitude;
    private float speed;
    private float accuracy;

//	public GPSPacket()
//	{
//		
//	}
//	
//	public GPSPacket(double lat, double lon, double alt, float speed, float acc)
//	{
//		this.latitude = lat ;
//		this.longitude = lon ;
//		this.altitude = alt;
//		this.speed = speed;
//		this.accuracy = acc;
//	}
	
    public byte[] build()
    {
            ByteBuffer b = ByteBuffer.allocate(32);
            System.out.println("Longitude : "+getLongitude());
            b.putDouble(this.getLongitude());
            b.putDouble(this.getLatitude());
            b.putDouble(this.getAltitude());
            b.putFloat(this.getSpeed());
            b.putFloat(this.getAccuracy());
            return b.array();
    }

    public void parse(byte[] packet)
    {
            ByteBuffer b = ByteBuffer.wrap(packet);
            this.setLongitude(b.getDouble());
            this.setLatitude(b.getDouble());
            this.setAltitude(b.getDouble());
            this.setSpeed(b.getFloat());
            this.setAccuracy(b.getFloat());
    }
    
    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the speed
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * @return the accuracy
     */
    public float getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy the accuracy to set
     */
    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }
}
