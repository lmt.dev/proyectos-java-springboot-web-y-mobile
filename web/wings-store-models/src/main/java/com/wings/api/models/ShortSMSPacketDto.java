
package com.wings.api.models;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.ArrayList;

import java.nio.ByteBuffer;

public class ShortSMSPacketDto {

    private int address_size;
    private String address;
    private long date;
    private int body_size;
    private String body;

    public ShortSMSPacketDto() {

    }

    public ShortSMSPacketDto(String ad, long dat, String body) {
            this.address = ad;
            this.address_size = ad.length();
            this.date = dat;
            this.body = body;
            this.body_size = this.body.length();
    }


    public byte[] build() {
            ByteBuffer b = ByteBuffer.allocate(4+4+getAddress().length()+4+4+8+4+getBody().length()+4);
            b.putInt(getAddress_size());
            b.put(getAddress().getBytes());
            b.putLong(getDate());
            b.putInt(getBody_size());
            b.put(getBody().getBytes());
            return b.array();
    }

    public void parse(byte[] packet) {
            ByteBuffer b = ByteBuffer.wrap(packet);
            this.setAddress_size(b.getInt());
            byte[] tmp = new byte[getAddress_size()];
            b.get(tmp);
            this.setAddress(new String(tmp));
            this.setDate(b.getLong());
            this.setBody_size(b.getInt());
            tmp = new byte[getBody_size()];
            b.get(tmp);
            this.setBody(new String(tmp));
    }
    
    /**
     * @return the address_size
     */
    public int getAddress_size() {
        return address_size;
    }

    /**
     * @param address_size the address_size to set
     */
    public void setAddress_size(int address_size) {
        this.address_size = address_size;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the date
     */
    public long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(long date) {
        this.date = date;
    }

    /**
     * @return the body_size
     */
    public int getBody_size() {
        return body_size;
    }

    /**
     * @param body_size the body_size to set
     */
    public void setBody_size(int body_size) {
        this.body_size = body_size;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }
}