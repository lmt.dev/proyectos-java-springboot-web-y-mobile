--wings-files
CREATE DATABASE wings_files;
use wings_files;

CREATE TABLE `uploaded_files` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `store_path` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,  
  `creation_date` datetime NULL,
  PRIMARY KEY (`id`));
