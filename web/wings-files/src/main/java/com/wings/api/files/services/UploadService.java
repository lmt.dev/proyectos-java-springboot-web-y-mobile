/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.api.files.services;

import com.wings.api.files.domain.entities.UploadedFile;
import com.wings.api.files.domain.repos.UploadedFilesRepository;
import com.wings.api.files.utils.FileUtils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Service
public class UploadService {

    @Autowired
    private UploadedFilesRepository uploadedFilesRepository;

    @Autowired
    private AuthService authService;

    /**
     *
     * @param file
     * @param request
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String storeFile(MultipartFile file, HttpServletRequest request) throws FileNotFoundException, IOException {

        String code = FileUtils.getFileName();
        String mime = file.getContentType();

        String filePath = getStorageDirectory() + code;
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(filePath));
        stream.write(file.getBytes());

        UploadedFile newFile = new UploadedFile();
        newFile.setCode(code);
        newFile.setCreationDate(new Date());
        newFile.setStorePath(filePath);
        newFile.setMimeType(mime);
        newFile.setUploaderIp(authService.getIp(request));

        uploadedFilesRepository.save(newFile);

        return code;
    }

    /**
     *
     * @param code
     * @return
     */
    public UploadedFile getByCode(String code) {
        return uploadedFilesRepository.findByCode(code);
    }

    /**
     *
     * @param code
     * @return
     */
    public File getFile(String code) {
        UploadedFile up = uploadedFilesRepository.findByCode(code);
        if (up != null) {
            return new File(up.getStorePath());
        }
        return null;
    }

    /**
     *
     * @return
     */
    private String getStorageDirectory() {
        return "/var/wings/wings_files/";
    }

}
