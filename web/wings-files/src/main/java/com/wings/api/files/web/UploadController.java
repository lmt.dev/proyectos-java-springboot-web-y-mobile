/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.api.files.web;

import com.wings.api.files.domain.entities.UploadedFile;
import com.wings.api.files.services.UploadService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author seba
 */
@Controller
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @RequestMapping(value = {"/"}, method = {RequestMethod.GET})
    public String getIndex(Model model, HttpServletRequest request) {
        return "blank";
    }

    @RequestMapping(value = {"/upload"}, method = {RequestMethod.GET})
    public String getUpload(Model model, HttpServletRequest request) {
        //return "index";
        return "download";
    }

    /**
     *
     * @param code
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/{code}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFile(@PathVariable("code") String code) throws IOException {

        if (code == null || code.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        UploadedFile uf = uploadService.getByCode(code);
        if (uf == null) {
            return ResponseEntity.badRequest().build();
        }

        File file = new File(uf.getStorePath());
        if (!file.exists()) {
            return ResponseEntity.badRequest().build();
        }

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        HttpHeaders headers = new HttpHeaders();

        //para descargar, sin esto abre en el browser
        //headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + code + ".png");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType(uf.getMimeType()))
                .body(resource);
    }

    /**
     *
     * @param request
     * @param file
     * @return
     */
    @RequestMapping(value = {"/p"}, method = {RequestMethod.POST})
    public ResponseEntity<String> postFile(HttpServletRequest request,
            @RequestParam(value = "file", required = false) MultipartFile file) {

        try {
            String code = uploadService.storeFile(file, request);
            String ret = "https://files.wingsmobile.com/" + code;
            return ResponseEntity.ok(ret);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.badRequest().body("error");
    }

}
