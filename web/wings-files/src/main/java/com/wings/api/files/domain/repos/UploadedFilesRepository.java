/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.api.files.domain.repos;

import com.wings.api.files.domain.entities.UploadedFile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author seba
 */
public interface UploadedFilesRepository extends JpaRepository<UploadedFile, Long> {

    UploadedFile findByCode(String code);
    
}
