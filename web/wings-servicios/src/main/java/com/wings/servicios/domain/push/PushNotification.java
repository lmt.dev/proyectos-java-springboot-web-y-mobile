/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.push;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class PushNotification {

    @JsonIgnore
    private Long id;
    @JsonInclude(Include.NON_NULL)
    private String to;
    private String priority;
    @JsonInclude(Include.NON_NULL)
    private Notification notification;
    @JsonInclude(Include.NON_NULL)
    private Data data;
    @JsonProperty("time_to_live")
    private int timeToLive;
    @JsonInclude(Include.NON_NULL)
    @JsonProperty("registrations_ids")
    private String[] registrationIds;

    private String visibility;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    /**
     * normal y high
     *
     * @param priority
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    /**
     * El valor de este parámetro debe ser una duración de entre 0 y 2,419,200
     * segundos (28 días) y corresponde al período máximo de tiempo en el que
     * FCM almacena y trata de enviar el mensaje. Las solicitudes que no
     * contienen este campo reciben de forma predeterminada el período máximo de
     * cuatro semanas.
     *
     * Al Especificar la duración de un mensaje FCM nunca regula mensajes con un
     * valor de time_to_live (TTL) de 0 segundos. En otras palabras, FCM
     * garantiza el mejor esfuerzo para los mensajes que se deben enviar "ahora
     * o nunca". Ten en cuenta que un valor de time_to_live de 0 significa que
     * los mensajes que no se pueden enviar de inmediato se descartan.
     *
     * Sin embargo, dado que estos mensajes nunca se almacenan, esto proporciona
     * la mejor latencia para enviar mensajes de notificación.
     *
     * @param timeToLive
     */
    public void setTimeToLive(int timeToLive) {
        this.timeToLive = timeToLive;
    }

    public void setRegistrationIds(String[] registrationIds) {
        this.registrationIds = registrationIds;
    }

    public String[] getRegistrationIds() {
        return registrationIds;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

}
