package com.wings.servicios.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;


public class EncryptUtils {

    private static final long s = 0x20625438 & 0x42316845 & 0x4232624;

    public static String encrypt(String src){
        String seed = String.valueOf(s);
        StandardPBEStringEncryptor enc = new StandardPBEStringEncryptor();
        enc.setPassword(seed);
        return enc.encrypt(src);
    }

    public static String decrypt(String src){
        String seed = String.valueOf(s);
        StandardPBEStringEncryptor enc = new StandardPBEStringEncryptor();
        enc.setPassword(seed);
        return enc.decrypt(src);
    }

}
