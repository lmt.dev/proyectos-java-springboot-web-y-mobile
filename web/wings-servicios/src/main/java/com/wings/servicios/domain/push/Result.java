/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author seba
 */
public class Result {
    
    @JsonProperty("message_id") 
    private String messageId;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
