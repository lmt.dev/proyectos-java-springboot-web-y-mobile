/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.PriceListDestination;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author seba
 */
public interface PriceListDestinationRepository extends JpaRepository<PriceListDestination, Long> , JpaSpecificationExecutor<PriceListDestination>{

    List<PriceListDestination> findByPriceListId(Long proceListId);
    
    @Query(value = "select distinct name, price from price_list_destination where price_list_id = ?", nativeQuery = true)
    List<Map<String,Double>> returnNamePrice(Long proceListId);
    
}
