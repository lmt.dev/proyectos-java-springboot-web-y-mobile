/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.Service;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface ServiceRepository extends JpaRepository<Service, Long>, JpaSpecificationExecutor<Service> {

    List<Service> findByAsignationAndActive(String asignation, Boolean active);
    
    List<Service> findByAsignationAndActiveOrderBySortOrderAsc(String asignation, Boolean active);
    
    List<Service> findByServiceCodeIn(String[] codeNames);

}
