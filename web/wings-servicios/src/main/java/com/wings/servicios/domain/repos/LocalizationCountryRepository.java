/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.domain.repos;

import com.wings.servicios.domain.entities.LocalizationCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 * @author seba
 */
public interface LocalizationCountryRepository extends JpaRepository<LocalizationCountry, Long>, JpaSpecificationExecutor<LocalizationCountry> {

    LocalizationCountry findByDefaultLocale(String defaultLocale);

    LocalizationCountry findByCountryCode(String countryCode);

}
