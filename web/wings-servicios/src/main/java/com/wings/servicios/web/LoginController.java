/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.web;

import com.wings.servicios.domain.entities.Client;
import com.wings.servicios.services.ClientsService;
import com.wings.servicios.util.EncryptUtils;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author seba
 */
@Controller
public class LoginController {

    @Autowired
    private ClientsService clientService;

    @RequestMapping(value = {"/login"}, method = {RequestMethod.GET})
    public String getLogin(Model model, HttpServletRequest request) {
        return "login";
    }

    @RequestMapping(value = {"/s"}, method = {RequestMethod.POST})
    public String postSetPassword(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String username = request.getParameter("username");
        String pass = request.getParameter("password");
        String repass = request.getParameter("repassword");
        String s = request.getParameter("s");

        boolean error = false;
        if (username == null || username.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Usuario o contraseña incorrectos");
            error = true;
        }

        if (pass == null || pass.isEmpty() || repass == null || repass.isEmpty() || !repass.equals(pass)) {
            redirectAttributes.addFlashAttribute("error", "Usuario o contraseña incorrectos");
            error = true;
        }

        if (!error) {
            //get client and save password
            Client client = clientService.getOne(username);
            if (DigestUtils.sha1Hex(client.getLogin() + client.getId()).equals(s)) {
                client.setWebPwd(DigestUtils.sha256Hex(pass));
                clientService.save(client);
                redirectAttributes.addFlashAttribute("p", pass);
                redirectAttributes.addFlashAttribute("login", client.getLogin());
                redirectAttributes.addFlashAttribute("auto", "true");
            }
        }

        return "redirect:/login";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @param code
     * @param code2
     * @return
     */
    @RequestMapping(value = {"/app/payments/{code}", "/app/payments/{code}/{code2}"}, method = RequestMethod.GET)
    public String getPaysas(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
            @PathVariable("code") String code,
            @PathVariable(name = "code2", required = false) String code2) {

        if (code2 != null) {
            code += "/" + code2;
        }

        String login;
        try {
            login = EncryptUtils.decrypt(code);
        } catch (Exception e) {
            try {
                login = EncryptUtils.decrypt(hexToString(code));
            } catch (Exception ex) {
                return "redirect:/login";
            }
        }

        Client client = clientService.getOne(login);
        //clientService.sendAuthenticationCode(login);
        redirectAttributes.addFlashAttribute("login", login);
        if (client.getWebPwd() == null) {
            redirectAttributes.addFlashAttribute("s", DigestUtils.sha1Hex(login + client.getId()));
        }

        return "redirect:/login";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = {"/recover"}, method = RequestMethod.GET)
    public String getRecover(Model model, HttpServletRequest request) {
        return "recover";
    }

    /**
     *
     * @param model
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = {"/recover"}, method = RequestMethod.POST)
    public String getPassword(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String login = request.getParameter("login");
        if (login == null) {
            return "redirect:/recover";
        }

        String code = request.getParameter("code");
        Client client = clientService.getOne(login);
        if (client == null) {
            return "redirect:/recover";
        }

        if (code == null) {
            clientService.sendAuthenticationCode(login);
            redirectAttributes.addFlashAttribute("login", login);
            return "redirect:/recover";
        }

        if (!clientService.validateAuthenticationCode(login, code)) {
            return "redirect:/recover";
        }

        client.setWebPwd(null);
        clientService.save(client);

        redirectAttributes.addFlashAttribute("login", login);
        redirectAttributes.addFlashAttribute("s", DigestUtils.sha1Hex(login + client.getId()));

        return "redirect:/login";
    }

    private String hexToString(String str) {
        String result = new String();
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i = i + 2) {
            String st = "" + charArray[i] + "" + charArray[i + 1];
            char ch = (char) Integer.parseInt(st, 16);
            result = result + ch;
        }
        return result;
    }

}
