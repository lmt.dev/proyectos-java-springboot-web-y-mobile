/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.servicios.config.auth;

import com.wings.servicios.services.ClientsService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author seba
 */
public class ServiciosAuthenticatorProvider implements AuthenticationProvider {

    @Autowired
    private ClientsService clientsService;

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        
        String login = a.getName();
        String password = (String) a.getCredentials();

        if (clientsService.authenticate(login, password)) {
            UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(login, password, new ArrayList<>());
            return t;
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }

}
