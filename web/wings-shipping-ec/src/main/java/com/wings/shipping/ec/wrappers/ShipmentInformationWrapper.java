/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.ec.wrappers;

import java.util.List;

/**
 *
 * @author seba
 */
public class ShipmentInformationWrapper {

    private String fechaProbable;
    private String numGui;
    private String fecEnv;
    private int numPie;
    private String ciuRem;
    private String nomRem;
    private String dirRem;
    private String ciuDes;
    private String nomDes;
    private String dirDes;
    private int idEstAct;
    private String estAct;
    private String fecEst;
    private String nomRec;
    private String numCun;
    private String regime;
    private String imagen;
    private byte[] bytesImagen;
    private String idDocumentoCliente;
    private String idDocumentoCliente2;
    private String placa;
    private int idGPS;
    private String formPago;
    private String nomProducto;
    private String mensajeBuscarImagen;
    private String motivoDevolucion;
    private String mensajesValidacion;

    private List<InformationMovWrapper> mov;
    private List<InformationMovSrWrapper> movSrPack;

    public String getFechaProbable() {
        return fechaProbable;
    }

    public void setFechaProbable(String fechaProbable) {
        this.fechaProbable = fechaProbable;
    }

    public String getNumGui() {
        return numGui;
    }

    public void setNumGui(String numGui) {
        this.numGui = numGui;
    }

    public String getFecEnv() {
        return fecEnv;
    }

    public void setFecEnv(String fecEnv) {
        this.fecEnv = fecEnv;
    }

    public int getNumPie() {
        return numPie;
    }

    public void setNumPie(int numPie) {
        this.numPie = numPie;
    }

    public String getCiuRem() {
        return ciuRem;
    }

    public void setCiuRem(String ciuRem) {
        this.ciuRem = ciuRem;
    }

    public String getNomRem() {
        return nomRem;
    }

    public void setNomRem(String nomRem) {
        this.nomRem = nomRem;
    }

    public String getDirRem() {
        return dirRem;
    }

    public void setDirRem(String dirRem) {
        this.dirRem = dirRem;
    }

    public String getCiuDes() {
        return ciuDes;
    }

    public void setCiuDes(String ciuDes) {
        this.ciuDes = ciuDes;
    }

    public String getNomDes() {
        return nomDes;
    }

    public void setNomDes(String nomDes) {
        this.nomDes = nomDes;
    }

    public String getDirDes() {
        return dirDes;
    }

    public void setDirDes(String dirDes) {
        this.dirDes = dirDes;
    }

    public int getIdEstAct() {
        return idEstAct;
    }

    public void setIdEstAct(int idEstAct) {
        this.idEstAct = idEstAct;
    }

    public String getEstAct() {
        return estAct;
    }

    public void setEstAct(String estAct) {
        this.estAct = estAct;
    }

    public String getFecEst() {
        return fecEst;
    }

    public void setFecEst(String fecEst) {
        this.fecEst = fecEst;
    }

    public String getNomRec() {
        return nomRec;
    }

    public void setNomRec(String nomRec) {
        this.nomRec = nomRec;
    }

    public String getNumCun() {
        return numCun;
    }

    public void setNumCun(String numCun) {
        this.numCun = numCun;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public byte[] getBytesImagen() {
        return bytesImagen;
    }

    public void setBytesImagen(byte[] bytesImagen) {
        this.bytesImagen = bytesImagen;
    }

    public String getIdDocumentoCliente() {
        return idDocumentoCliente;
    }

    public void setIdDocumentoCliente(String idDocumentoCliente) {
        this.idDocumentoCliente = idDocumentoCliente;
    }

    public String getIdDocumentoCliente2() {
        return idDocumentoCliente2;
    }

    public void setIdDocumentoCliente2(String idDocumentoCliente2) {
        this.idDocumentoCliente2 = idDocumentoCliente2;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getIdGPS() {
        return idGPS;
    }

    public void setIdGPS(int idGPS) {
        this.idGPS = idGPS;
    }

    public String getFormPago() {
        return formPago;
    }

    public void setFormPago(String formPago) {
        this.formPago = formPago;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public String getMensajeBuscarImagen() {
        return mensajeBuscarImagen;
    }

    public void setMensajeBuscarImagen(String mensajeBuscarImagen) {
        this.mensajeBuscarImagen = mensajeBuscarImagen;
    }

    public String getMotivoDevolucion() {
        return motivoDevolucion;
    }

    public void setMotivoDevolucion(String motivoDevolucion) {
        this.motivoDevolucion = motivoDevolucion;
    }

    public String getMensajesValidacion() {
        return mensajesValidacion;
    }

    public void setMensajesValidacion(String mensajesValidacion) {
        this.mensajesValidacion = mensajesValidacion;
    }

    public List<InformationMovWrapper> getMov() {
        return mov;
    }

    public void setMov(List<InformationMovWrapper> mov) {
        this.mov = mov;
    }

    public List<InformationMovSrWrapper> getMovSrPack() {
        return movSrPack;
    }

    public void setMovSrPack(List<InformationMovSrWrapper> movSrPack) {
        this.movSrPack = movSrPack;
    }

    
}
