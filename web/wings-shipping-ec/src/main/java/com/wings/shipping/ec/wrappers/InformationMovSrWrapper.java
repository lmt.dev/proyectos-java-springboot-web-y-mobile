/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wings.shipping.ec.wrappers;

/**
 *
 * @author seba
 */
public class InformationMovSrWrapper {
 
    private String descMov;
    private String oriMov;
    private String destMov;
    private String fechMov;

    public String getDescMov() {
        return descMov;
    }

    public void setDescMov(String descMov) {
        this.descMov = descMov;
    }

    public String getOriMov() {
        return oriMov;
    }

    public void setOriMov(String oriMov) {
        this.oriMov = oriMov;
    }

    public String getDestMov() {
        return destMov;
    }

    public void setDestMov(String destMov) {
        this.destMov = destMov;
    }

    public String getFechMov() {
        return fechMov;
    }

    public void setFechMov(String fechMov) {
        this.fechMov = fechMov;
    }
 
    
}
